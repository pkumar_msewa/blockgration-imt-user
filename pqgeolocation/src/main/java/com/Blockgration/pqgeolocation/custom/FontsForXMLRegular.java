package com.Blockgration.pqgeolocation.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontsForXMLRegular extends TextView{

	public FontsForXMLRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public FontsForXMLRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FontsForXMLRegular(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                                               "Roboto-Regular.ttf");
        setTypeface(tf);
    }

}


