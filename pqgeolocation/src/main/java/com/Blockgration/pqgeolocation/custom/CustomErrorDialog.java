package com.Blockgration.pqgeolocation.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.Blockgration.pqgeolocation.R;
import com.Blockgration.pqgeolocation.activity.PQApplication;


/**
 * Created by Kashif-PC on 1/21/2017.
 */
public class CustomErrorDialog extends AlertDialog.Builder {

    public CustomErrorDialog(Context context, String title, CharSequence message) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_custom_error, null, false);

        TextView titleTextView = (TextView) viewDialog.findViewById(R.id.cadTiltle);
        titleTextView.setText(title);

        ImageView ivpqlogo = (ImageView) viewDialog.findViewById(R.id.ivpqlogo);
        ivpqlogo.setImageResource(PQApplication.bitmap);

        TextView messageTextView = (TextView) viewDialog.findViewById(R.id.cadMessage);
        messageTextView.setText(message);

        this.setCancelable(false);

        this.setView(viewDialog);


    }
}

