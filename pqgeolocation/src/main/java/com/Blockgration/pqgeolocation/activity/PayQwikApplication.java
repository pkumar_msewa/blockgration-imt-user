package com.Blockgration.pqgeolocation.activity;

import android.app.Application;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ConnectionQuality;
import com.androidnetworking.interfaces.ConnectionQualityChangeListener;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;


/**
 * Created by Ksf on 7/14/2016.
 */
public class PayQwikApplication extends Application {

    public static final String TAG = PayQwikApplication.class.getSimpleName();
    private static PayQwikApplication mInstance;




    @Override
    public void onCreate() {
        super.onCreate();
      OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
        .connectTimeout(18000, TimeUnit.SECONDS)
        .readTimeout(18000, TimeUnit.SECONDS)
        .writeTimeout(18000, TimeUnit.SECONDS)
        .build();

      AndroidNetworking.initialize(getApplicationContext(), okHttpClient);
      BitmapFactory.Options options = new BitmapFactory.Options();
      options.inPurgeable = true;
      AndroidNetworking.setBitmapDecodeOptions(options);
      AndroidNetworking.enableLogging();
      AndroidNetworking.setConnectionQualityChangeListener(new ConnectionQualityChangeListener() {
        @Override
        public void onChange(ConnectionQuality currentConnectionQuality, int currentBandwidth) {
          Log.d(TAG, "onChange: currentConnectionQuality : " + currentConnectionQuality + " currentBandwidth : " + currentBandwidth);
        }
      });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
