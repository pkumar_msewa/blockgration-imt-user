package com.Blockgration.pqgeolocation.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.Blockgration.pqgeolocation.service.SensorService;

//import com.msewa.pqgeolocation.service.LocationService;

/**
 * Created by kashifimam on 29/06/17.
 */

public class PQApplication {

    public static final String TAG = PQApplication.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 65;
    public static String mainUrl;
    public static String secretKey;
    public static String tokenKey;
    public static int bitmap;
    public static String appName;
    private static PQApplication mInstance = null;
    private static SensorService mSensorService;
    private static Intent mServiceIntent;
    private  Context context;

    private PQApplication(Context context) {
        this.context = context;
    }


    public static void init(Context context) {
        mInstance = new PQApplication(context);
        mSensorService = new SensorService(context);
        mServiceIntent = new Intent(context, mSensorService.getClass());
//        if (!isMyServiceRunning(mSensorService.getClass(), context)) {
            context.startService(mServiceIntent);
//        }

    }

    private static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    public static synchronized PQApplication getInstance() {
        return mInstance;
    }

}
