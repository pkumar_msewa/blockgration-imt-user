package com.Blockgration.pqgeolocation.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.Blockgration.pqgeolocation.BuildConfig;
import com.Blockgration.pqgeolocation.R;
import com.Blockgration.pqgeolocation.custom.CustomErrorDialog;
import com.Blockgration.pqgeolocation.custom.LoadingDialog;
import com.Blockgration.pqgeolocation.metadata.ApiUrl;
import com.Blockgration.pqgeolocation.model.CountryModel;
import com.Blockgration.pqgeolocation.util.NetworkErrorHandler;
import com.Blockgration.pqgeolocation.util.SecurityUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//import com.msewa.pqgeolocation.service.LocationService;

/**
 * Created by kashifimam on 29/06/17.
 */

public class RegisterActivity extends AppCompatActivity {

  private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
  private LinearLayout llPassword, llCountry;
  private EditText etPassword, etCountry;
  private Button btnDone;
  private LoadingDialog loadingDialog;

  private String tag_json_obj = "json_mobile_topup";

  private ArrayList<CountryModel> countryArray;
  private String selectedCode = "";
  private String mobileNo = "";

  private boolean isStartService = false;


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    countryArray = new ArrayList<>();
    mobileNo = getIntent().getStringExtra("mobileNo");
    setContentView(R.layout.activity_register);
    btnDone = (Button) findViewById(R.id.btnDone);
    llPassword = (LinearLayout) findViewById(R.id.llPassword);
    llCountry = (LinearLayout) findViewById(R.id.llCountry);
    etPassword = (EditText) findViewById(R.id.etPassword);
    etCountry = (EditText) findViewById(R.id.etCountry);

    attemptCheck();

    btnDone.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (checkPermissions()) {
          checkField();
        } else {
          requestPermissions();
        }

      }
    });

    etCountry.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (countryArray.size() == 0) {
          loadCountries();
        } else {
          showCountryDialog();
        }
      }
    });

    llCountry.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (countryArray.size() == 0) {
          loadCountries();
        } else {
          showCountryDialog();
        }
      }
    });


  }


  private void attemptCheck() {
    loadingDialog = new LoadingDialog(RegisterActivity.this, "Please Wait");
    loadingDialog.show();
    HashMap<String, String> headers = new HashMap<String, String>();
    headers.put("Content-Type", "application/json");
    headers.put("token", PQApplication.tokenKey);
    headers.put("key", PQApplication.secretKey);
    AndroidNetworking.get(ApiUrl.URL_LIST_NOTIFICATION)
      // posting json
      .setTag("test")
      .addHeaders(headers)
      .setPriority(Priority.HIGH)
      .build()
      .getAsString(new StringRequestListener() {
        @Override
        public void onResponse(String res) {

          Log.i("Check No", res.toString());

          try {
            JSONObject response = new JSONObject(res);
            String code = response.getString("code");
            if (code != null && code.equals("U01")) {
              loadingDialog.dismiss();

              boolean isUser = response.getBoolean("user");

              if (isUser) {
                if (checkPermissions()) {

                  Intent notificationIntent = new Intent(RegisterActivity.this, NotificationListActivity.class);
                  notificationIntent.putExtra("UserName", mobileNo);
                  startActivity(notificationIntent);
                  finish();


                } else {
                  isStartService = true;
                  requestPermissions();
                }
              } else {
                if (response.has("message")) {
                  showErrorDialog(response.getString("message"));
                } else {
                  showErrorDialog("User Name already taken as user\n Please try new.");
                }
              }
            } else if (code != null && code.equals("S00")) {
              loadingDialog.dismiss();

              //Ready to register
            } else {

              showErrorDialog("An error occurred\nplease try again");
              loadingDialog.dismiss();


            }

          } catch (JSONException e) {
            loadingDialog.dismiss();
            showErrorDialog("Json Exception\nplease try again");
            e.printStackTrace();
          }


        }

        @Override
        public void onError(ANError anError) {


          loadingDialog.dismiss();
          showErrorDialog(getResources().getString(R.string.server_error_server_error));

        }
      });


  }


  private void showCountryDialog() {
    ArrayList<String> countryVal = new ArrayList<>();
    for (CountryModel countryModels : countryArray) {
      countryVal.add(countryModels.getCountryName());
    }

    android.support.v7.app.AlertDialog.Builder currencyDialog =
      new android.support.v7.app.AlertDialog.Builder(RegisterActivity.this, R.style.AppCompatAlertDialogStyle);
    currencyDialog.setTitle("Select Service Provider");

    currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      }
    });


    currencyDialog.setItems(countryVal.toArray(new String[countryArray.size()]),
      new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface arg0, int i) {
          etCountry.setText(countryArray.get(i).getCountryName());
          selectedCode = countryArray.get(i).getCountryCode();
        }
      });

    currencyDialog.show();
  }


  private void loadCountries() {
    loadingDialog = new LoadingDialog(RegisterActivity.this, "Please Wait");
    loadingDialog.show();
    HashMap<String, String> headers = new HashMap<String, String>();
    headers.put("Content-Type", "application/json");
    headers.put("token", PQApplication.tokenKey);
    headers.put("key", PQApplication.secretKey);
    AndroidNetworking.get(ApiUrl.URL_LOAD_COUNTRIES)
      // posting json
      .setTag("test")
      .addHeaders(headers)
      .setPriority(Priority.HIGH)
      .build()
      .getAsString(new StringRequestListener() {
        @Override
        public void onResponse(String res) {

          try {
            JSONObject response = new JSONObject(res);
            String code = response.getString("code");
            if (code != null && code.equals("S00")) {

              JSONArray operatorArray = response.getJSONArray("details");
              for (int i = 0; i < operatorArray.length(); i++) {
                JSONObject c = operatorArray.getJSONObject(i);
                String cCode = c.getString("code");
                String cName = c.getString("name");

                CountryModel countryModel = new CountryModel(cCode, cName);
                countryArray.add(countryModel);
              }
              loadingDialog.dismiss();
              showCountryDialog();
            } else {
              loadingDialog.dismiss();
              showErrorMessage("Error Loading countires");
            }

          } catch (JSONException e) {
            loadingDialog.dismiss();
            e.printStackTrace();
          }


        }

        @Override
        public void onError(ANError anError) {
          loadingDialog.dismiss();
        }
      });

  }

  private void checkField() {
    clearErrorBackGround();

    if (etPassword.getText().toString().isEmpty()) {
      llPassword.setBackground(ContextCompat.getDrawable(RegisterActivity.this, R.drawable.et_click_stork_background));
      showErrorMessage("Required password to continue.");
    } else if (etPassword.getText().toString().length() < 6) {
      llPassword.setBackground(ContextCompat.getDrawable(RegisterActivity.this, R.drawable.et_click_stork_background));
      showErrorMessage("Password should be at least 6 digit.");
    } else if (etCountry.getText().toString().isEmpty()) {
      llCountry.setBackground(ContextCompat.getDrawable(RegisterActivity.this, R.drawable.et_click_stork_background));
      showErrorMessage("Required country to continue.");
    } else {
      //Done Now


      promoteRegistration();
    }
  }


  public void promoteRegistration() {
    loadingDialog.show();
    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("username", mobileNo);
      jsonRequest.put("password", etPassword.getText().toString());
      jsonRequest.put("userType", "User");
      jsonRequest.put("code", selectedCode);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      HashMap<String, String> headers = new HashMap<String, String>();
      headers.put("Content-Type", "application/json");
      headers.put("token", PQApplication.tokenKey);
      headers.put("key", PQApplication.secretKey);
      AndroidNetworking.post(ApiUrl.URL_REQ_REG)
        // posting json
        .setTag("test")
        .addHeaders(headers)
        .addJSONObjectBody(jsonRequest)
        .setPriority(Priority.HIGH)
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject response) {
            Log.i("GEO Reg", response.toString());
            try {
              String code = response.getString("code");
              if (code != null && code.equals("S00")) {
                //Register the device now
                promoteSaveDevice();

              } else {
                loadingDialog.dismiss();
                if (response.has("message") && response.getString("message") != null) {
                  String message = response.getString("message");
                  showErrorMessage(message);
                }
              }
            } catch (JSONException e) {
              loadingDialog.dismiss();
              showErrorMessage(getResources().getString(R.string.server_exception2));
              e.printStackTrace();
            }
          }

          @Override
          public void onError(ANError anError) {
            loadingDialog.dismiss();
            showErrorMessage(getResources().getString(R.string.server_error_server_error));

          }
        });
    }
  }

  public void promoteSaveDevice() {
    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("username", mobileNo);
      jsonRequest.put("deviceId", SecurityUtil.getAndroidId(RegisterActivity.this));
      jsonRequest.put("imei", SecurityUtil.getIMEI(RegisterActivity.this));
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());

      HashMap<String, String> headers = new HashMap<String, String>();
      headers.put("Content-Type", "application/json");
      headers.put("token", PQApplication.tokenKey);
      headers.put("key", PQApplication.secretKey);
      AndroidNetworking.post(ApiUrl.URL_REQ_SAVE_DEVICE)
        // posting json
        .setTag("test")
        .addHeaders(headers)
        .addJSONObjectBody(jsonRequest)
        .setPriority(Priority.HIGH)
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject response) {
            Log.i("GEO Reg", response.toString());
            try {
              String code = response.getString("code");
              if (code != null && code.equals("S00")) {
                //Register the device now
                loadingDialog.dismiss();

                Intent notificationIntent = new Intent(RegisterActivity.this, NotificationListActivity.class);
                notificationIntent.putExtra("UserName", mobileNo);
                startActivity(notificationIntent);
                finish();

              } else {
                loadingDialog.dismiss();
                if (response.has("message") && response.getString("message") != null) {
                  String message = response.getString("message");
                  showErrorMessage(message);
                }
              }
            } catch (JSONException e) {
              loadingDialog.dismiss();
              showErrorMessage(getResources().getString(R.string.server_exception2));
              e.printStackTrace();
            }
          }

          @Override
          public void onError(ANError anError) {
            loadingDialog.dismiss();
            showErrorMessage(getResources().getString(R.string.server_error_server_error));

          }
        });
    }
  }

  private void clearErrorBackGround() {
    llPassword.setBackground(ContextCompat.getDrawable(RegisterActivity.this, R.drawable.et_click_background));
    llPassword.setBackground(ContextCompat.getDrawable(RegisterActivity.this, R.drawable.et_click_background));
  }


  private void showErrorMessage(String message) {
    Snackbar snackbar = Snackbar
      .make(llCountry, message, Snackbar.LENGTH_SHORT);
    View view = snackbar.getView();
    view.setBackgroundColor(ContextCompat.getColor(RegisterActivity.this, R.color.sb_view));
    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
    tv.setTextColor(ContextCompat.getColor(RegisterActivity.this, R.color.sb_title));

    snackbar.show();
  }


  public Context getContext() {
    return RegisterActivity.this;
  }


  private boolean checkPermissions() {
    int permissionState = ActivityCompat.checkSelfPermission(this,
      Manifest.permission.ACCESS_COARSE_LOCATION);
    int phoneResult = ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.READ_PHONE_STATE);

    if (phoneResult == PackageManager.PERMISSION_GRANTED && permissionState == PackageManager.PERMISSION_GRANTED) {
      return true;
    } else {
      return false;
    }

  }

  private void startLocationPermissionRequest() {
    ActivityCompat.requestPermissions(RegisterActivity.this,
      new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE},
      REQUEST_PERMISSIONS_REQUEST_CODE);
  }

  private void requestPermissions() {
    boolean shouldProvideRationale =
      ActivityCompat.shouldShowRequestPermissionRationale(this,
        Manifest.permission.ACCESS_FINE_LOCATION);
    if (shouldProvideRationale) {
      showSnackbar(R.string.permission_rationale, android.R.string.ok,
        new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            startLocationPermissionRequest();
          }
        });

    } else {
      startLocationPermissionRequest();
    }
  }

  /**
   * Callback received when a permissions request has been completed.
   */
  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                         int[] grantResults) {
    if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
      if (grantResults.length <= 0) {

      } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
        // Permission granted.
        if (isStartService) {
          Intent notificationIntent = new Intent(RegisterActivity.this, NotificationListActivity.class);
          notificationIntent.putExtra("UserName", mobileNo);
          startActivity(notificationIntent);
          finish();

        } else {
          checkField();
        }
      } else {
        showSnackbar(R.string.permission_denied_explanation, R.string.settings,
          new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              // Build intent that displays the App settings screen.
              Intent intent = new Intent();
              intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
              Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
              intent.setData(uri);
              intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
              startActivity(intent);
            }
          });
      }
    }
  }


  private void showSnackbar(final int mainTextStringId, final int actionStringId,
                            View.OnClickListener listener) {
    Snackbar.make(findViewById(android.R.id.content),
      getString(mainTextStringId),
      Snackbar.LENGTH_INDEFINITE)
      .setAction(getString(actionStringId), listener).show();
  }

  private void showErrorDialog(String message) {
    CustomErrorDialog builder = new CustomErrorDialog(RegisterActivity.this, "Oops! Error occurred.", message);
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        finish();

      }
    });
    builder.show();
  }


}
