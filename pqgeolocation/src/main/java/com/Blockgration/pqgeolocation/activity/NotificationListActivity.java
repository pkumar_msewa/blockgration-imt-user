package com.Blockgration.pqgeolocation.activity;


import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.Blockgration.pqgeolocation.R;
import com.Blockgration.pqgeolocation.adapter.NotificationAdapter;
import com.Blockgration.pqgeolocation.custom.LoadMoreListView;
import com.Blockgration.pqgeolocation.metadata.ApiUrl;
import com.Blockgration.pqgeolocation.model.NotificationModel;
import com.Blockgration.pqgeolocation.util.NetworkErrorHandler;
import com.Blockgration.pqgeolocation.util.SecurityUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

//import com.msewa.pqgeolocation.service.LocationService;

/**
 * Created by kashifimam on 29/06/17.
 */

public class NotificationListActivity extends AppCompatActivity {
  //Share preference
  public static final String PHONE = "userName";
  private static final int MY_PERMISSIONS_REQUEST_LOCATION = 65;
  SharedPreferences phonePreferences;
  private LoadMoreListView lvNotification;
  private ArrayList<NotificationModel> notificationList;
  private ProgressBar pbNotification;
  private LinearLayout llNoNotification;
  private String tag_json_obj = "json_mobile_topup";
  private TextView tvNoNotification;
  private String userName;
  private NotificationAdapter notificationAdapter;
  //Pagination
  private int currentPage = 0;
  private int totalPage = 0;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    phonePreferences = getSharedPreferences(PHONE, Context.MODE_PRIVATE);

    setContentView(R.layout.activity_notification_list);
    lvNotification = (LoadMoreListView) findViewById(R.id.lvNotification);
    pbNotification = (ProgressBar) findViewById(R.id.pbNotification);
    llNoNotification = (LinearLayout) findViewById(R.id.llNoNotification);
    tvNoNotification = (TextView) findViewById(R.id.tvNoNotification);

    userName = getIntent().getStringExtra("UserName");

    SharedPreferences.Editor editor = phonePreferences.edit();
    editor.clear();
    editor.putString("userId", userName);
    editor.apply();


    pbNotification.setVisibility(View.GONE);
    lvNotification.setVisibility(View.GONE);
    llNoNotification.setVisibility(View.VISIBLE);


//        if(checkLocationPermission(NotificationListActivity.this)) {
//            stopService(new Intent(NotificationListActivity.this, LocationService.class));
//            Intent serviceIntent = new Intent(NotificationListActivity.this, LocationService.class);
//            startService(serviceIntent);
//        }

//        }else{
//            Intent serviceIntent = new Intent(NotificationListActivity.this, LocationService.class);
//            startService(serviceIntent);
//        }

    lvNotification.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
      @Override
      public void onLoadMore() {
        if (currentPage < totalPage) {
          loadMoreNotification();
        } else {
          lvNotification.onLoadMoreComplete();
        }
      }
    });

    loadNotification();

  }

  public boolean checkLocationPermission(Activity activity) {
    if (ContextCompat.checkSelfPermission(this,
      Manifest.permission.ACCESS_FINE_LOCATION)
      != PackageManager.PERMISSION_GRANTED) {

      // Should we show an explanation?
      if (ActivityCompat.shouldShowRequestPermissionRationale(NotificationListActivity.this,
        Manifest.permission.ACCESS_FINE_LOCATION)) {

        // Show an explanation to the user *asynchronously* -- don't block
        // this thread waiting for the user's response! After the user
        // sees the explanation, try again to request the permission.
        ActivityCompat.requestPermissions(activity,
          new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
          MY_PERMISSIONS_REQUEST_LOCATION);


      } else {
        // No explanation needed, we can request the permission.
        ActivityCompat.requestPermissions(activity,
          new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
          MY_PERMISSIONS_REQUEST_LOCATION);
      }
      return false;
    } else {
      return true;
    }
  }


  private void loadNotification() {
    pbNotification.setVisibility(View.VISIBLE);
    lvNotification.setVisibility(View.GONE);
    llNoNotification.setVisibility(View.GONE);
    notificationList = new ArrayList<>();

    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("deviceId", SecurityUtil.getAndroidId(NotificationListActivity.this));
      jsonRequest.put("page", currentPage);
      jsonRequest.put("size", 5);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      HashMap<String, String> headers = new HashMap<>();
      headers.put("token", "MTUwNzcxNTkxNDAwMw==");
      headers.put("key", "2DFF462E1DC9D398D456C8BC2B8AD518CF2F95FF");

      AndroidNetworking.post(ApiUrl.URL_LIST_NOTIFICATION)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .addHeaders(headers)
        .setPriority(Priority.HIGH)
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject JsonObj) {
            try {
              String message = JsonObj.getString("message");
              String code = JsonObj.getString("code");

              if (code != null && code.equals("S00")) {

                JSONObject jsonDetails = JsonObj.getJSONObject("details");
                totalPage = jsonDetails.getInt("totalPages");

                long totalElements = jsonDetails.getLong("totalElements");
                if (totalElements == 0) {
                  pbNotification.setVisibility(View.GONE);
                  lvNotification.setVisibility(View.GONE);
                  llNoNotification.setVisibility(View.VISIBLE);
                  tvNoNotification.setText("No notification found.");

                } else {
                  JSONArray operatorArray = jsonDetails.getJSONArray("content");
                  for (int i = 0; i < operatorArray.length(); i++) {
                    JSONObject c = operatorArray.getJSONObject(i);
                    long notID = c.getLong("id");
                    String notContent = c.getString("content");
                    String notImage = c.getString("image");
                    long notRadius = c.getLong("radius");
                    String notSubject = c.getString("subject");
                    String notTitle = c.getString("title");
                    String validFrom = c.getString("validFrom");
                    String validTo = c.getString("validTo");
                    String validFromFormat = c.getString("validFromFormat");
                    String validToFormat = c.getString("validToFormat");

                    NotificationModel statementModel = new NotificationModel(notID, notContent, notImage, notRadius, notSubject, notTitle, validFrom, validTo, validFromFormat, validToFormat);
                    notificationList.add(statementModel);
                  }
                  if (notificationList != null && notificationList.size() != 0) {
                    notificationAdapter = new NotificationAdapter(NotificationListActivity.this, notificationList);
                    lvNotification.setAdapter(notificationAdapter);
                    pbNotification.setVisibility(View.GONE);
                    lvNotification.setVisibility(View.VISIBLE);
                    llNoNotification.setVisibility(View.GONE);
                  } else {
                    pbNotification.setVisibility(View.GONE);
                    lvNotification.setVisibility(View.GONE);
                    llNoNotification.setVisibility(View.VISIBLE);
                    if (message.equalsIgnoreCase("New Device Detected") || message.equalsIgnoreCase("Device not exists")) {
                      promoteSaveDevice();
                    } else {
                      tvNoNotification.setText(message);
                    }
                  }

                }
              } else {
                pbNotification.setVisibility(View.GONE);
                lvNotification.setVisibility(View.GONE);
                llNoNotification.setVisibility(View.VISIBLE);
                if (message.equalsIgnoreCase("New Device Detected") || message.equalsIgnoreCase("Device not exists")) {
                  promoteSaveDevice();
                } else {
                  tvNoNotification.setText(message);
                }
              }

            } catch (JSONException e) {
              e.printStackTrace();
              pbNotification.setVisibility(View.GONE);
              lvNotification.setVisibility(View.GONE);
              llNoNotification.setVisibility(View.VISIBLE);
              tvNoNotification.setText(getResources().getString(R.string.server_exception2));

            }

          }

          @Override
          public void onError(ANError anError) {
//                .dismiss();
            Log.i("valus", anError.getErrorDetail());
            pbNotification.setVisibility(View.GONE);
            lvNotification.setVisibility(View.GONE);
            llNoNotification.setVisibility(View.VISIBLE);
            tvNoNotification.setText(getResources().getString(R.string.server_exception2));
//                CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
          }
        });
    }


  }


  private void loadMoreNotification() {
    currentPage = currentPage + 1;
    notificationList = new ArrayList<>();

    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("deviceId", SecurityUtil.getAndroidId(NotificationListActivity.this));
      jsonRequest.put("page", currentPage);
      jsonRequest.put("size", 5);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("NotificationJsonRequest", jsonRequest.toString());
      Log.i("Receipt Url", ApiUrl.URL_LIST_NOTIFICATION);
      HashMap<String, String> headers = new HashMap<String, String>();
      headers.put("token", PQApplication.tokenKey);
      headers.put("key", PQApplication.secretKey);

      AndroidNetworking.post(ApiUrl.URL_LIST_NOTIFICATION)
        .addJSONObjectBody(jsonRequest) // posting json
        .addHeaders(headers)
        .setTag("test")
        .setPriority(Priority.HIGH)
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject JsonObj) {
            try {

              Log.i("Receipts Response", JsonObj.toString());
              String message = JsonObj.getString("message");
              String code = JsonObj.getString("code");

              if (code != null && code.equals("S00")) {

                JSONObject jsonDetails = JsonObj.getJSONObject("details");
                JSONArray operatorArray = jsonDetails.getJSONArray("content");
                for (int i = 0; i < operatorArray.length(); i++) {
                  JSONObject c = operatorArray.getJSONObject(i);
                  long notID = c.getLong("id");
                  String notContent = c.getString("content");
                  String notImage = c.getString("image");
                  long notRadius = c.getLong("radius");
                  String notSubject = c.getString("subject");
                  String notTitle = c.getString("title");
                  String validFrom = c.getString("validFrom");
                  String validTo = c.getString("validTo");
                  String validFromFormat = c.getString("validFromFormat");
                  String validToFormat = c.getString("validToFormat");


                  NotificationModel statementModel = new NotificationModel(notID, notContent, notImage, notRadius, notSubject, notTitle, validFrom, validTo, validFromFormat, validToFormat);
                  notificationList.add(statementModel);
                }
                notificationAdapter.notifyDataSetChanged();


              } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
              }

            } catch (JSONException e) {
              e.printStackTrace();
              Toast.makeText(getApplicationContext(), "Error, Json Exception", Toast.LENGTH_SHORT).show();

            }
          }

          @Override
          public void onError(ANError anError) {
            pbNotification.setVisibility(View.GONE);
            try {
              Toast.makeText(getApplicationContext(), R.string.server_error_no_connection, Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
              e.printStackTrace();
            }
          }
        });
    }

  }

  private boolean isMyServiceRunning(Class<?> serviceClass) {
    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
    for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
      if (serviceClass.getName().equals(service.service.getClassName())) {
        return true;
      }
    }
    return false;
  }

  public void promoteSaveDevice() {
    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("username", userName);
      jsonRequest.put("deviceId", SecurityUtil.getAndroidId(NotificationListActivity.this));
      jsonRequest.put("imei", SecurityUtil.getIMEI(NotificationListActivity.this));
    } catch (JSONException e) {

      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      HashMap<String, String> headers = new HashMap<String, String>();
      headers.put("token", PQApplication.tokenKey);
      headers.put("key", PQApplication.secretKey);

      AndroidNetworking.post(ApiUrl.URL_REQ_SAVE_DEVICE)
        .addJSONObjectBody(jsonRequest) // posting json
        .addHeaders(headers)
        .setTag("test")
        .setPriority(Priority.HIGH)
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject response) {
            Log.i("SaveDevice", response.toString());

            try {
              String code = response.getString("code");
              if (code != null && code.equals("S00")) {

                loadNotification();

              } else {

                if (response.has("message") && response.getString("message") != null) {
                  String message = response.getString("message");
                  showErrorMessage(message);
                }
              }
            } catch (JSONException e) {
              showErrorMessage(getResources().getString(R.string.server_exception2));
              e.printStackTrace();
            }
          }

          @Override
          public void onError(ANError anError) {

            showErrorMessage(getResources().getString(R.string.server_error_server_error));
          }
        });

    }
  }

  private void showErrorMessage(String message) {
    Snackbar snackbar = Snackbar
      .make(llNoNotification, message, Snackbar.LENGTH_SHORT);
    View view = snackbar.getView();
    view.setBackgroundColor(ContextCompat.getColor(NotificationListActivity.this, R.color.sb_view));
    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
    tv.setTextColor(ContextCompat.getColor(NotificationListActivity.this, R.color.sb_title));

    snackbar.show();
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
  }
}
