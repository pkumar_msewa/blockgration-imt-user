package com.Blockgration.pqgeolocation.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.Blockgration.pqgeolocation.R;
import com.Blockgration.pqgeolocation.model.NotificationModel;

import java.util.ArrayList;



/**
 * Created by Ksf on 5/8/2016.
 */
public class NotificationAdapter extends BaseAdapter {

    private ViewHolder viewHolder;
    private Context context;
    private ArrayList<NotificationModel> notificationList;

    public NotificationAdapter(Context context, ArrayList<NotificationModel> notificationList) {
        this.context = context;
        this.notificationList = notificationList;
    }

    @Override
    public int getCount() {
        return notificationList.size();
    }

    @Override
    public Object getItem(int position) {
        return notificationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parentView) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_notification, null);
            viewHolder = new ViewHolder();
            viewHolder.tvNotificationTitle = (TextView) convertView.findViewById(R.id.tvNotificationTitle);
            viewHolder.tvNotificationDate = (TextView) convertView.findViewById(R.id.tvNotificationDate);
            viewHolder.tvNotificationDetails = (TextView) convertView.findViewById(R.id.tvNotificationDetails);
            viewHolder.tvNotificationSubject = (TextView) convertView.findViewById(R.id.tvNotificationSubject);

            viewHolder.ivNotification = (ImageView) convertView.findViewById(R.id.ivNotification);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvNotificationTitle.setText(notificationList.get(position).getNotTitle());
        viewHolder.tvNotificationDate.setText("Validity: "+notificationList.get(position).getValidToFormat());
        viewHolder.tvNotificationDetails.setText(notificationList.get(position).getNotContent());
        viewHolder.tvNotificationSubject.setText(notificationList.get(position).getNotSubject());


        if(notificationList.get(position).getNotImage()==null || notificationList.get(position).getNotImage().equals("null")||notificationList.get(position).getNotImage().isEmpty()){
            viewHolder.ivNotification.setVisibility(View.GONE);
        }else{
            viewHolder.ivNotification.setVisibility(View.VISIBLE);
          AQuery aQuery=new AQuery(context);
          aQuery.id(viewHolder.ivNotification).background(R.drawable.loading_image).image(notificationList.get(position).getNotImage(),true,true);

        }

        return convertView;
    }

    static class ViewHolder {
        TextView tvNotificationTitle, tvNotificationDate, tvNotificationDetails,tvNotificationSubject;
        ImageView ivNotification;
    }
}
