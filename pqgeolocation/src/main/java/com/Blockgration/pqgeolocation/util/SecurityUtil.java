package com.Blockgration.pqgeolocation.util;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;

/**
 * Created by Ksf on 5/7/2016.
 */
public class SecurityUtil {

    public static String getAndroidId(Context context){
        // TODO Encryption Algorithm
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
//        return "121231231231";
        return android_id;
    }

    public static String getIMEI(Context context){
        // TODO Encryption Algorithm
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = mTelephonyMgr.getDeviceId();
        return imei;
    }

}
