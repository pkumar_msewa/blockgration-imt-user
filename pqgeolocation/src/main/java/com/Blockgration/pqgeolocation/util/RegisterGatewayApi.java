package com.Blockgration.pqgeolocation.util;


import android.content.Intent;

import com.Blockgration.pqgeolocation.activity.RegisterActivity;
import com.Blockgration.pqgeolocation.model.PayQwikInit;


/**
 * Created by Ksf on 3/10/2016.
 */
public class RegisterGatewayApi implements IRegisterGatewayApi {

    private PayQwikInit init;

    public RegisterGatewayApi(PayQwikInit init) {
        this.init = init;
    }

    @Override
    public void register(String mobileNumber) {
        Intent i = new Intent(init.getContext(), RegisterActivity.class);
        i.putExtra("mobileNo", mobileNumber);
        init.getContext().startActivity(i);

    }
}
