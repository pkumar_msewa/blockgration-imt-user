package com.Blockgration.pqgeolocation.model;

import android.content.Context;

/**
 * Created by Ksf on 3/10/2016.
 */
public class PayQwikInit {

    private String machineId;
    private Context context;

    public PayQwikInit(Context context) {
        this.context = context;
        this.machineId = getDeviceMachineId();
    }

    public String getMachineId() {
        return machineId;
    }

    private String getDeviceMachineId() {
        return null;
    }

    public Context getContext() {
        return context;
    }
}
