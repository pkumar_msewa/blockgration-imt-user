package com.Blockgration.pqgeolocation.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kashifimam on 03/07/17.
 */

public class NotificationModel implements Parcelable {
    public static final Creator<NotificationModel> CREATOR = new Creator<NotificationModel>() {
        @Override
        public NotificationModel createFromParcel(Parcel source) {
            return new NotificationModel(source);
        }

        @Override
        public NotificationModel[] newArray(int size) {
            return new NotificationModel[size];
        }
    };
    private long notID;
    private String notContent;
    private String notImage;
    private long notRadius;
    private String notSubject;
    private String notTitle;
    private String validFrom;
    private String validTo;
    private String validFromFormat;
    private String validToFormat;

    public NotificationModel(long notID, String notContent, String notImage, long notRadius, String notSubject, String notTitle, String validFrom, String validTo, String validFromFormat, String validToFormat){
        this.notID = notID;
        this.notContent = notContent;
        this.notImage = notImage;
        this.notSubject = notSubject;
        this.notTitle = notTitle;
        this.notRadius = notRadius;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.validFromFormat = validFromFormat;
        this.validToFormat = validToFormat;
    }

    protected NotificationModel(Parcel in) {
        this.notID = in.readLong();
        this.notContent = in.readString();
        this.notImage = in.readString();
        this.notRadius = in.readLong();
        this.notSubject = in.readString();
        this.notTitle = in.readString();
        this.validFrom = in.readString();
        this.validTo = in.readString();
        this.validFromFormat = in.readString();
        this.validToFormat = in.readString();
    }

    public long getNotID() {
        return notID;
    }

    public void setNotID(long notID) {
        this.notID = notID;
    }

    public String getNotContent() {
        return notContent;
    }

    public void setNotContent(String notContent) {
        this.notContent = notContent;
    }

    public String getNotImage() {
        return notImage;
    }

    public void setNotImage(String notImage) {
        this.notImage = notImage;
    }

    public long getNotRadius() {
        return notRadius;
    }

    public void setNotRadius(long notRadius) {
        this.notRadius = notRadius;
    }

    public String getNotSubject() {
        return notSubject;
    }

    public void setNotSubject(String notSubject) {
        this.notSubject = notSubject;
    }

    public String getNotTitle() {
        return notTitle;
    }

    public void setNotTitle(String notTitle) {
        this.notTitle = notTitle;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getValidFromFormat() {
        return validFromFormat;
    }

    public void setValidFromFormat(String validFromFormat) {
        this.validFromFormat = validFromFormat;
    }

    public String getValidToFormat() {
        return validToFormat;
    }

    public void setValidToFormat(String validToFormat) {
        this.validToFormat = validToFormat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.notID);
        dest.writeString(this.notContent);
        dest.writeString(this.notImage);
        dest.writeLong(this.notRadius);
        dest.writeString(this.notSubject);
        dest.writeString(this.notTitle);
        dest.writeString(this.validFrom);
        dest.writeString(this.validTo);
        dest.writeString(this.validFromFormat);
        dest.writeString(this.validToFormat);
    }
}
