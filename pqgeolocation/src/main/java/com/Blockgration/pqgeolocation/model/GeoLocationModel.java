package com.Blockgration.pqgeolocation.model;

/**
 * Created by user on 9/10/2015.
 */
public class GeoLocationModel {
    private long geoID;
    private String geoName;
    private String geoDetails;
    private double geoLat;
    private double geoLon;
    private String geoImage;

    public GeoLocationModel(long geoID, String geoName, String geoDetails, double geoLat, double geoLon, String geoImage){
        this.geoID = geoID;
        this.geoName = geoName;
        this.geoDetails = geoDetails;
        this.geoLat = geoLat;
        this.geoLon = geoLon;
        this.geoImage = geoImage;

    }

    public GeoLocationModel(){

    }

    public long getGeoID() {
        return geoID;
    }

    public void setGeoID(long geoID) {
        this.geoID = geoID;
    }

    public String getGeoName() {
        return geoName;
    }

    public void setGeoName(String geoName) {
        this.geoName = geoName;
    }

    public String getGeoDetails() {
        return geoDetails;
    }

    public void setGeoDetails(String geoDetails) {
        this.geoDetails = geoDetails;
    }

    public double getGeoLat() {
        return geoLat;
    }

    public void setGeoLat(double geoLat) {
        this.geoLat = geoLat;
    }

    public double getGeoLon() {
        return geoLon;
    }

    public void setGeoLon(double geoLon) {
        this.geoLon = geoLon;
    }

    public String getGeoImage() {
        return geoImage;
    }

    public void setGeoImage(String geoImage) {
        this.geoImage = geoImage;
    }
}
