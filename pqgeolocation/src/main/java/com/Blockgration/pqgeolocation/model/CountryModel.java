package com.Blockgration.pqgeolocation.model;

/**
 * Created by kashifimam on 30/06/17.
 */

public class CountryModel {

    private String countryName;
    private String countryCode;



    public CountryModel(String countryCode, String countryName){
        this.countryCode = countryCode;
        this.countryName = countryName;
    }


    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
