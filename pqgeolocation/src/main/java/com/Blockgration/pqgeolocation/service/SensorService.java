package com.Blockgration.pqgeolocation.service;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.Blockgration.pqgeolocation.activity.NotificationListActivity;
import com.Blockgration.pqgeolocation.activity.PQApplication;
import com.Blockgration.pqgeolocation.metadata.ApiUrl;
import com.Blockgration.pqgeolocation.util.SecurityUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by fabio on 30/01/2016.
 */
public class SensorService extends Service implements LocationListener {
  public static final int NOTIFICATION_ID = 1;
  //Share preference
  public static final String PHONE = "userName";
  private static final int MY_PERMISSIONS_REQUEST_LOCATION = 76;
  private static final String TAG = "BookingTrackingService";
  public static String str_receiver = "servicetutorial.service.receiver";
  public int counter = 0;
  public double track_lat = 0.0;
  public double track_lng = 0.0;
  Timer timer = new Timer();
  boolean isGPSEnable = false;
  boolean isNetworkEnable = false;
  double latitude, longitude;
  LocationManager locationManager;
  Location location;
  long notify_interval = 30000;
  Intent intent;
  SharedPreferences phonePreferences;
  long oldTime = 0;
  private LocationManager lm;
  private NotificationManager mNotificationManager;
  private Context context;
  private Handler mHandler = new Handler();
  private Timer mTimer = null;
  private String userName = "";
  private android.os.Handler handlerList = new android.os.Handler();
  Runnable runnable = new Runnable() {

    @Override
    public void run() {
      // TODO Auto-generated method stub
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
      getLocation("user");
//            }

      handlerList.postDelayed(this, 2000); // 1000 - Milliseconds
    }
  };
  private BroadcastReceiver mReceiver = null;
  private Runnable mUpdateListTask = new Runnable() {
    public void run() {  //will make a toast notification every 1 minute.
      Calendar cal = Calendar.getInstance();
      getLocation("user");
      handlerList.postDelayed(this, 1000 * 60);
    }
  };
  private TimerTask timerTask;

  public SensorService(Context applicationContext) {
    super();
  }

  public SensorService() {
  }

  public void updateLists() {
    handlerList.postDelayed(mUpdateListTask, 1000 * 60);
  }

  @Override
  public void onCreate() {
    super.onCreate();
//        updateLists();
//        handlerList.postDelayed(runnable, 1000);
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                getLocation("user");
//            }
//        }, 0, 5000);

    handlerList.postDelayed(runnable, 2000);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    super.onStartCommand(intent, flags, startId);
//        startTimer();
//        updateLists();
    handlerList.postDelayed(runnable, 2000);
    return START_STICKY;
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    Log.i("EXIT", "ondestroy!");
    stopService(new Intent(this, SensorService.class));

//        Intent intent = new Intent("SensorRestarterBroadcastReceiver");
//        sendBroadcast(intent);
    stoptimertask();
  }

  public void startTimer() {
    //set a new Timer
    timer = new Timer();

    //initialize the TimerTask's job
    initializeTimerTask();

    //schedule the timer, to wake up every 1 second
    timer.schedule(timerTask, 2000, 2000); //
  }

  /*
   * it sets the timer to print the counter every x seconds
   */
  public void initializeTimerTask() {
    timerTask = new TimerTask() {
      public void run() {
        Log.i("in timer", "in timer ++++  " + (counter++));
//                if(checkLocationPermission(SensorService.this)) {

        getLocation("user");
//                }

      }
    };
  }

  /**
   * not needed
   */
  public void stoptimertask() {
    //stop the timer, if it's not already null
    if (timer != null) {
      timer.cancel();
      timer = null;
    }
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  @SuppressWarnings("MissingPermission")
  private void getLocation(final String userName) {

//        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//
//            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
////            promoteUpdateLocation(String.valueOf(location.getLatitude()) + "", String.valueOf(location.getLongitude()) + "", userName);
//            new Thread(new Runnable() {
//
//                @Override
//                public void run() {
//                    Looper.getMainLooper().prepare();
//                    lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 0.3f, new android.location.LocationListener() {
//                        @Override
//                        public void onLocationChanged(Location location) {
//
//                            promoteUpdateLocation(String.valueOf(location.getLatitude()) + "", String.valueOf(location.getLongitude()) + "", userName);
////                sendNotification("location changed"+ location.getLatitude()+"-"+location.getLongitude(),"","");
//                        }
//
//                        @Override
//                        public void onStatusChanged(String provider, int status, Bundle extras) {
//
//                        }
//
//                        @Override
//                        public void onProviderEnabled(String provider) {
//
//                        }
//
//                        @Override
//                        public void onProviderDisabled(String provider) {
//
//                        }
//                    }, Looper.getMainLooper());
//                }
//            }).start();


    locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
    isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

    if (!isGPSEnable && !isNetworkEnable) {
      buildAlertMessageNoGps();
    } else {
      if (isNetworkEnable) {
        location = null;
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 0, this);
        if (locationManager != null) {
          location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
          if (location != null) {

            latitude = location.getLatitude();
            longitude = location.getLongitude();
            track_lat = latitude;
            track_lng = longitude;
            promoteRegisterLocation(String.valueOf(location.getLatitude()) + "", String.valueOf(location.getLongitude()) + "", "user");
//                        fn_update(location);
          }
        }
      }

      if (isGPSEnable) {
        location = null;
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
        if (locationManager != null) {
          location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
          if (location != null) {
            Log.e(TAG, "isGPSEnable latitude" + location.getLatitude() + "\nlongitude" + location.getLongitude() + "");
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            track_lat = latitude;
            track_lng = longitude;
            promoteRegisterLocation(String.valueOf(location.getLatitude()) + "", String.valueOf(location.getLongitude()) + "", "user");
//                        fn_update(location);
          }
        }
      }

    }
  }


  private void sendNotification(String msg, String image, String details) {

  }

  public Bitmap getBitmapFromUrl(String imageUrl) {
    try {
      URL url = new URL(imageUrl);
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setDoInput(true);
      connection.connect();
      StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
      StrictMode.setThreadPolicy(policy);
      InputStream input = connection.getInputStream();
      Bitmap bitmap = BitmapFactory.decodeStream(input);
      return bitmap;

    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;

    }
  }


  public void promoteUpdateLocation(String currentLat, String currentLong, String userName) {
    JSONObject jsonRequest;
    try {
      jsonRequest = new JSONObject();
      jsonRequest.put("userType", "User");
      jsonRequest.put("deviceId", SecurityUtil.getAndroidId(SensorService.this));
      jsonRequest.put("latitude", currentLat);
      jsonRequest.put("longitude", currentLong);
    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      HashMap<String, String> headers = new HashMap<>();
      headers.put("token", "MTUwNzcxNTkxNDAwMw==");
      headers.put("key", "2DFF462E1DC9D398D456C8BC2B8AD518CF2F95FF");
      AndroidNetworking.post(ApiUrl.URL_UPDATE_LOCATION)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .addHeaders(headers)
        .setPriority(Priority.HIGH)
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject JsonObj) {
            Log.i("Location", JsonObj.toString());

            try {
              Log.i("Location Response", JsonObj.toString());

              String code = JsonObj.getString("code");

              if (code != null && code.equals("S00")) {

                JSONObject jsonDetails = JsonObj.getJSONObject("details");

                long totalElements = jsonDetails.getLong("totalElements");
                if (totalElements == 0) {

                } else {
                  JSONArray operatorArray = jsonDetails.getJSONArray("content");
                  for (int i = 0; i < operatorArray.length(); i++) {
                    JSONObject c = operatorArray.getJSONObject(i);
                    long notID = c.getLong("id");
                    String notContent = c.getString("content");
                    String notImage = c.getString("image");
                    long notRadius = c.getLong("radius");
                    String notSubject = c.getString("subject");
                    String notTitle = c.getString("title");
                    String validFrom = c.getString("validFrom");
                    String validTo = c.getString("validTo");
                    String validFromFormat = c.getString("validFromFormat");
                    String validToFormat = c.getString("validToFormat");
                    sendNotification sendNotification = new sendNotification(SensorService.this, notTitle, notContent);
                    sendNotification.execute(notTitle, notContent, notImage);
                    sendNotification(notTitle, notImage, notContent);
//                                    NotificationModel statementModel = new NotificationModel( notID,  notContent,  notImage, notRadius,  notSubject,  notTitle,  validFrom, validTo, validFromFormat, validToFormat);
//                                    notificationList.add(statementModel);
                  }


                }
              } else {
                String message = JsonObj.getString("message");
                sendNotification(message, message, message);
              }

            } catch (JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(ANError anError) {

          }
        });
    }

  }

  public boolean checkLocationPermission(Context activity) {
    if (ContextCompat.checkSelfPermission(activity,
      Manifest.permission.ACCESS_FINE_LOCATION)
      != PackageManager.PERMISSION_GRANTED) {

      // Should we show an explanation?
      if (ActivityCompat.shouldShowRequestPermissionRationale((AppCompatActivity) activity,
        Manifest.permission.ACCESS_FINE_LOCATION)) {

        // Show an explanation to the user *asynchronously* -- don't block
        // this thread waiting for the user's response! After the user
        // sees the explanation, try again to request the permission.
        ActivityCompat.requestPermissions((Activity) activity,
          new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
          MY_PERMISSIONS_REQUEST_LOCATION);


      } else {
        // No explanation needed, we can request the permission.
        ActivityCompat.requestPermissions((Activity) activity,
          new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
          MY_PERMISSIONS_REQUEST_LOCATION);
      }
      return false;
    } else {
      return true;
    }
  }

  public void onTaskRemoved(Intent rootIntent) {

    Calendar cal = Calendar.getInstance();

    Intent intent = new Intent(this, SensorService.class);
    PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);

    AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
// Start every minute
    alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 1000, pintent);
    // stopSelf();
//        super.onTaskRemoved(rootIntent);
  }


  @Override
  public void onLocationChanged(Location location) {
    promoteRegisterLocation(String.valueOf(location.getLatitude()) + "", String.valueOf(location.getLongitude()) + "", "user");
  }

  @Override
  public void onStatusChanged(String s, int i, Bundle bundle) {

  }

  @Override
  public void onProviderEnabled(String s) {

  }

  @Override
  public void onProviderDisabled(String s) {

  }

  private void buildAlertMessageNoGps() {
    final AlertDialog.Builder builder = new AlertDialog.Builder(SensorService.this);
    builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
      .setCancelable(false)
      .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
        public void onClick(final DialogInterface dialog, final int id) {
          startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
      })
      .setNegativeButton("No", new DialogInterface.OnClickListener() {
        public void onClick(final DialogInterface dialog, final int id) {
          dialog.cancel();
        }
      });
    final AlertDialog alert = builder.create();
    alert.show();
  }

  public void promoteRegisterLocation(final String currentLat, final String currentLong, String userName) {
    JSONObject jsonRequest;
    try {
      jsonRequest = new JSONObject();
      jsonRequest.put("userType", "User");
      jsonRequest.put("device", SecurityUtil.getAndroidId(SensorService.this));

    } catch (JSONException e) {
      e.printStackTrace();
      jsonRequest = null;
    }

    if (jsonRequest != null) {
      Log.i("JsonRequest", jsonRequest.toString());
      HashMap<String, String> headers = new HashMap<>();
      headers.put("token", "MTUwNzcxNTkxNDAwMw==");
      headers.put("key", "2DFF462E1DC9D398D456C8BC2B8AD518CF2F95FF");
      AndroidNetworking.post(ApiUrl.URL_REQ_REG)
        .addJSONObjectBody(jsonRequest) // posting json
        .setTag("test")
        .addHeaders(headers)
        .setPriority(Priority.HIGH)
        .build()
        .getAsJSONObject(new JSONObjectRequestListener() {
          @Override
          public void onResponse(JSONObject JsonObj) {
            Log.i("SaveDevice", JsonObj.toString());

            try {
              Log.i("Receipts Response", JsonObj.toString());

              String code = JsonObj.getString("code");

              if (code != null && code.equals("S00")) {
                try {
                  promoteUpdateLocation(currentLat + "", currentLong + "", "user");
                } catch (NullPointerException e) {
                  e.printStackTrace();
                }
              } else {
                String message = JsonObj.getString("message");
                sendNotification(message, message, message);
              }

            } catch (JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(ANError anError) {

          }

        });
    }


  }

  private class sendNotification extends AsyncTask<String, Void, Bitmap> {

    Context ctx;
    String message, details;

    public sendNotification(Context context, String msg, String details) {
      super();
      this.ctx = context;
      this.message = msg;
      this.details = details;
    }

    @Override
    protected Bitmap doInBackground(String... params) {

      InputStream in;
      try {

        URL url = new URL(params[2]);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoInput(true);
        connection.connect();
        in = connection.getInputStream();
        Bitmap myBitmap = BitmapFactory.decodeStream(in);
        return myBitmap;


      } catch (MalformedURLException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }
      return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {

      super.onPostExecute(result);
      try {

        Intent myIntent = new Intent(SensorService.this, NotificationListActivity.class);
        myIntent.putExtra("UserName", userName);
        PendingIntent contentIntent = PendingIntent.getActivity(SensorService.this, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intent = new Intent(SensorService.this, NotificationListActivity.class);
        myIntent.putExtra("UserName", userName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(SensorService.this, 0 /* Request code */, intent,
          PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(SensorService.this)
          .setLargeIcon(result)
          .setSmallIcon(PQApplication.bitmap)
          .setContentTitle(message)
          .setStyle(new NotificationCompat.BigPictureStyle()
            .bigPicture(result))
          .setAutoCancel(true)
          .setPriority(Notification.PRIORITY_MAX)
          .setSound(defaultSoundUri)
          .setContentText(details)
          .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
          (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1 /* ID of notification */, notificationBuilder.build());

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
