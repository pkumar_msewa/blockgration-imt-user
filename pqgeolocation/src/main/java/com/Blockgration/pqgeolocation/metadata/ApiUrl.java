package com.Blockgration.pqgeolocation.metadata;

import com.Blockgration.pqgeolocation.activity.PQApplication;

/**
 * Created by Ksf on 3/11/2016.
 */
public class ApiUrl {


    private static final String URL_MAIN = PQApplication.mainUrl;

    public static final String URL_CHECK_REG = URL_MAIN+ "checkUsername/";
    public static final String URL_LOAD_COUNTRIES = URL_MAIN+ "getCountries";

    public static final String URL_REQ_REG = URL_MAIN+ "register";
    public static final String URL_REQ_SAVE_DEVICE = URL_MAIN+ "updateDevice";

    public static final String URL_LIST_NOTIFICATION = URL_MAIN + "userNotifications";

    public static final String URL_UPDATE_LOCATION = URL_MAIN + "updateLocation";





}