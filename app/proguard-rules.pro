-optimizationpasses 5
-verbose
-useuniqueclassmembernames
-keepattributes SourceFile,LineNumberTable
-allowaccessmodification
-dontwarn org.npci.upi.security.services.**

#Keep classes that are referenced on the AndroidManifest
-keep public class * extends android.support.v7.app.AppCompatActivity
-keep public class * extends android.app.Application
-keep class com.Blockgration.model.** { *; }
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends Request<String>

-keep interface android.support.v4.app.** { *; }
-keep class android.support.v4.app.** { *; }
-keep class org.apache.http.** { *; }


-dontwarn org.apache.commons.**
-dontwarn com.squareup.**
-dontwarn org.apache.http.**
-dontwarn com.google.**
-dontwarn com.github.**
-dontwarn com.android.volley.**
-keep public class * extends com.orm.SugarRecord
-keep class com.orm.** { *; }
-dontwarn com.google.common.**
-keepattributes Signature,InnerClasses

# Keep Picasso
-keep class com.squareup.picasso.** { *; }
-keepclasseswithmembers class * {
    @com.squareup.picasso.** *;
}
-keepclassmembers class * {
    @com.squareup.picasso.** *;
}

-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}
#To remove debug logs:
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}


#Maintain java native methods
-keepclasseswithmembernames class * {
    native <methods>;
}

#To maintain custom components names that are used on layouts XML.
#Uncomment if having any problem with the approach below
#-keep public class custom.components.package.and.name.**

#To maintain custom components names that are used on layouts XML:
-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-adaptresourcefilenames    **.properties,**.gif,**.jpg
-adaptresourcefilecontents **.properties,META-INF/MANIFEST.MF


#To keep parcelable classes (to serialize - deserialize objects to sent through Intents)
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

#Keep the R
-keepclassmembers class **.R$* {
    public static <fields>;
}
