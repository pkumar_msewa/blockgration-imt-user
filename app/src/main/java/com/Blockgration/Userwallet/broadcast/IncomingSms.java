package com.Blockgration.Userwallet.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by Ksf on 3/13/2016.
 */
public class IncomingSms extends BroadcastReceiver {


    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();
        Log.i("PAYQWIK INFO", "PAYQWIK INFO");
        try {

            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                Log.i("PAYQWIK INFO", "PAYQWIK INFO 1");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage;
                    Log.i("PAYQWIK INFO", "PAYQWIK INFO 2");
//                    if (Build.VERSION.SDK_INT >= 23) {
//                        String format = bundle.getString("format");
//                        currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i], format);
//                    }
//                    else {
                        currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
//                    }

                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

//                    String[] messageSplit = message.split("-");
                    String[] messageOTP = message.split(" ");


                    if(senderNum.equals("MD-PAYQWK")){
                        Intent smsIntent = new Intent("broadCastName");
                        smsIntent.putExtra("message", messageOTP[4].trim());
                        context.sendBroadcast(smsIntent);
                    }

                }
            }

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }
}
