package com.Blockgration.Userwallet.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.model.SmOfferDetailsModel;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.UserModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SmOfferDetailsActivity extends AppCompatActivity {

    private LoadingDialog loadingDialog;
    private String offer_key;
    private TextView tvSmOffer, tvSmDiscount, tvSmOfferExp, tvSmOfferTc, tvSmOfferMinAmt;
    private ImageView ivSmOfferImg;
    private LinearLayout smOfferll;

    //Volley
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;

    private UserModel session = UserModel.getInstance();

    private List<SmOfferDetailsModel> offerDetailsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sm_offer_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        loadingDialog = new LoadingDialog(SmOfferDetailsActivity.this);
        offerDetailsModel = new ArrayList<>();

        offer_key = getIntent().getStringExtra("offer_key");

        tvSmOffer = (TextView) findViewById(R.id.tvSmOffer);
        tvSmDiscount = (TextView) findViewById(R.id.tvSmDiscount);
        tvSmOfferExp = (TextView) findViewById(R.id.tvSmOfferExp);
        tvSmOfferTc = (TextView) findViewById(R.id.tvSmOfferTc);
        tvSmOfferMinAmt = (TextView) findViewById(R.id.tvSmOfferMinAmt);
        ivSmOfferImg = (ImageView) findViewById(R.id.ivSmOfferImg);
        smOfferll = (LinearLayout) findViewById(R.id.smOfferll);
        smOfferll.setVisibility(View.GONE);

        getOfferDetails(offer_key);
//        tvSmOffer.setText(offer_key);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.Blockgration.Userwallet.R.menu.menu_main, menu);
        return true;
    }

    private void getOfferDetails(final String id) {
        loadingDialog.show();
        final JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("OfferDetails Url", ApiURLNew.URL_SM_OFFER_DETAIL + id);
            Log.i("OfferDetails Request", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SM_OFFER_DETAIL + id, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    loadingDialog.dismiss();
                    try {
                        Log.i("OfferDetails Response", jsonObj.toString());
                        boolean success = jsonObj.getBoolean("success");
                        String message = jsonObj.getString("message");

                        String details = jsonObj.getString("details");
                        JSONObject detailsObj = new JSONObject(details);

                        if (success) {
//                            String logo_url = null;
                            String offers = detailsObj.getString("offers");
                            JSONArray offersArray = new JSONArray(offers);
                            for (int i = 0; i < offersArray.length(); i++) {
                                JSONObject obj1 = offersArray.getJSONObject(i);
                                String minimum_purchase = obj1.getString("minimum_purchase");
                                String savings_amount = obj1.getString("savings_amount");
                                String logo_url = obj1.getString("logo_url");
                                Log.i("SMOfferDetails", logo_url);
                                boolean promotion_code = obj1.getBoolean("promotion_code");
                                String expires_on = obj1.getString("expires_on");
                                String discount_value = obj1.getString("discount_value");
                                String terms_of_use = obj1.getString("terms_of_use");

                                String offer_store = obj1.getString("offer_store");
                                JSONObject obj2 = new JSONObject(offer_store);
                                String name = obj2.getString("name");
                                String description = obj2.getString("description");
                                String store_key = obj2.getString("store_key");

                                String physical_location = obj2.getString("physical_location");
                                JSONObject obj3 = new JSONObject(physical_location);
                                SmOfferDetailsModel smOfferDetailsModel = new SmOfferDetailsModel(savings_amount, logo_url, promotion_code, expires_on, discount_value, terms_of_use, name, description, store_key, minimum_purchase);
                                offerDetailsModel.add(smOfferDetailsModel);
                            }
                            if (!offerDetailsModel.get(0).getLogo_url().isEmpty()) {
                                smOfferll.setVisibility(View.VISIBLE);
                                Picasso.with(SmOfferDetailsActivity.this).load(offerDetailsModel.get(0).getLogo_url()).placeholder(R.drawable.loading_image).into(ivSmOfferImg);
                                tvSmOffer.setText(offerDetailsModel.get(0).getDescription());
                                tvSmDiscount.setText(offerDetailsModel.get(0).getSavings_amount() + " off");
                                tvSmOfferExp.setText(offerDetailsModel.get(0).getExpires_on());
                                tvSmOfferTc.setText(offerDetailsModel.get(0).getTerms_of_use());
                                tvSmOfferMinAmt.setText(offerDetailsModel.get(0).getMinimum_purchase());
                            }
                        } else if (message.equalsIgnoreCase("Invalid Session")) {
                            showInvalidSessionDialog();
                        } else {
                            CustomToast.showMessage(SmOfferDetailsActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    loadingDialog.dismiss();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(SmOfferDetailsActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(SmOfferDetailsActivity.this).sendBroadcast(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        offerDetailsModel.clear();
        finish();
    }
}
