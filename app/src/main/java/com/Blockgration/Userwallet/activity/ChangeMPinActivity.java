package com.Blockgration.Userwallet.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.NetworkErrorHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ksf on 5/8/2016.
 */
public class ChangeMPinActivity extends AppCompatActivity {
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();

    private EditText etChangeNewRePin, etChangeNewPin, etChangeCurrentPin;
    private TextInputLayout ilChangeCurrentPin, ilChangeNewPin, ilChangeNewRePin;
    private Button btnChangeMin, btnChangeMinDismiss;

    //Volley Tag
    private String tag_json_obj = "json_user";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_mpin);

        loadDlg = new LoadingDialog(ChangeMPinActivity.this);

        etChangeNewRePin = (EditText) findViewById(R.id.etChangeNewRePin);
        etChangeNewPin = (EditText) findViewById(R.id.etChangeNewPin);
        etChangeCurrentPin = (EditText) findViewById(R.id.etChangeCurrentPin);

        ilChangeCurrentPin = (TextInputLayout) findViewById(R.id.ilChangeCurrentPin);
        ilChangeNewPin = (TextInputLayout) findViewById(R.id.ilChangeNewPin);
        ilChangeNewRePin = (TextInputLayout) findViewById(R.id.ilChangeNewRePin);


        btnChangeMin = (Button) findViewById(R.id.btnChangeMin);
        btnChangeMinDismiss = (Button) findViewById(R.id.btnChangeMinDismiss);

        btnChangeMinDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnChangeMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitQuery();

            }
        });
    }

    private void submitQuery() {
        if (!validateCurrentMPin()) {
            return;
        }
        if (!validateNewMPin()) {
            return;
        }
        changeMpin();
    }

    private boolean validateCurrentMPin() {
        if (etChangeCurrentPin.getText().toString().trim().isEmpty() || etChangeCurrentPin.getText().toString().trim().length() < 4) {
            ilChangeCurrentPin.setError("Enter 4 digit pin");
            requestFocus(etChangeCurrentPin);
            return false;
        } else {
            ilChangeCurrentPin.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateNewMPin() {
        if (etChangeNewPin.getText().toString().trim().length() < 4) {
            ilChangeNewPin.setError("Enter 4 digit MPIN");
            requestFocus(etChangeNewPin);
            return false;

        } else if (etChangeNewRePin.getText().toString().trim().length() < 4) {
            ilChangeNewRePin.setError("Enter 4 digit MPIN");
            requestFocus(etChangeNewRePin);
            return false;

        } else if (!etChangeNewRePin.getText().toString().trim().equals(etChangeNewPin.getText().toString().trim())) {
            ilChangeNewRePin.setError("MPIN din't match");
            requestFocus(etChangeNewRePin);
            return false;

        } else {
            ilChangeNewPin.setErrorEnabled(false);
            ilChangeNewRePin.setErrorEnabled(false);
        }
        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void changeMpin() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
//            jsonRequest.put("username", session.getUserEmail());
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("mpin", etChangeCurrentPin.getText().toString());
            jsonRequest.put("newMpin", etChangeNewPin.getText().toString());
            jsonRequest.put("confirmMpin", etChangeNewRePin.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_CHANGE_M_PIN, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Change PIN RESPONSE", response.toString());
                        String code = response.getString("code");
                        String jsonString = response.getString("response");
                        JSONObject jsonObject = new JSONObject(jsonString);
                        String detailMessage = jsonObject.getString("message");
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            Toast.makeText(getApplicationContext(), detailMessage, Toast.LENGTH_SHORT).show();
                            finish();
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(getApplicationContext(), detailMessage, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(ChangeMPinActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

}
