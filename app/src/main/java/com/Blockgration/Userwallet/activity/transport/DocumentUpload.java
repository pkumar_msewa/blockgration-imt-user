package com.Blockgration.Userwallet.activity.transport;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.MainActivity;
import com.Blockgration.Userwallet.activity.shopping.CustomSuccessDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.model.DocumentModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.MultipartRequest;
import com.Blockgration.util.NetworkErrorHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


/**
 * Created by Ksf on 4/12/2016.
 */
public class DocumentUpload extends AppCompatActivity {


    private static final int REQUEST_CODE_DOC = 1;
    private Spinner document_type;
    private EditText issue_date, issue_expire;
    private TextView upload_text;
    private Button upload_document;
    private JSONObject totalvalue;
    private String docFilePath, radio;
    private HashMap<String, String> uploadHeaderParams;
    private ArrayList<DocumentModel> documentModels;
    LoadingDialog loadingDialog;
    private UserModel session = UserModel.getInstance();
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_custom_document);
        overridePendingTransition(R.anim.trans_left_out, R.anim.trans_left_in);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.trans_right_out, R.anim.trans_right_in);
            }
        });

        document_type = (Spinner) findViewById(R.id.document_type);
        issue_date = (EditText) findViewById(R.id.issue_date);
        issue_expire = (EditText) findViewById(R.id.issue_expire);
        issue_date.setFocusable(false);
        issue_date.setFocusableInTouchMode(false);
        issue_expire.setFocusable(false);
        issue_expire.setFocusableInTouchMode(false);
        upload_text = (TextView) findViewById(R.id.upload_text);
        upload_document = (Button) findViewById(R.id.upload_document);
        loadingDialog = new LoadingDialog(DocumentUpload.this);
        documentModels = getIntent().getParcelableArrayListExtra("Document");
        radio = getIntent().getStringExtra("radio");
        try {
            totalvalue = new JSONObject(getIntent().getStringExtra("parmters"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<String> strings = new ArrayList<>();
        for (DocumentModel documentModel : documentModels) {
            strings.add(documentModel.getName());
        }
        if (strings.size() != 0) {
            ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(DocumentUpload.this, android.R.layout.simple_spinner_dropdown_item, strings);
            document_type.setAdapter(stringArrayAdapter);
        }
        document_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        issue_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateOfBirthDialog(issue_date);
            }
        });
        issue_expire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateOfBirthDialog(issue_expire);
            }
        });

        upload_document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDocument();

            }
        });

        Button submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String valiue = document_type.getSelectedItem().toString();
                if (valiue != null && !valiue.isEmpty()) {
                    if (issue_expire != null && !issue_expire.getText().toString().isEmpty()) {
                        if (issue_date != null && !issue_date.getText().toString().isEmpty()) {
                            if (docFilePath != null && !docFilePath.isEmpty()) {
                                for (DocumentModel documentUpload : documentModels) {
                                    if (document_type.getSelectedItem().equals(documentUpload.getName())) {
                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
                                        Date newDate = null, expire = null;
                                        try {
                                            newDate = format.parse(issue_date.getText().toString());
                                            expire = format.parse(issue_date.getText().toString());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                                        String date = format.format(newDate);
                                        String expiredate = format.format(expire);
                                        getTranctionCreateAPi();
//     uploadImage(docFilePath, documentUpload.getDocId(), documentUpload.getName(), "1", date, expiredate, "4");
                                    }
                                }
                            }
                        } else {
                            issue_date.setError("Flied Required");
                        }
                    } else {
                        issue_expire.setError("Flied Required");
                    }
                } else {
                    CustomToast.showMessage(DocumentUpload.this, "Select Document Type");
                }
            }
        });


    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_out, R.anim.trans_right_in);
    }

//    void getCheckValidDoc() {
//
//        try {
//            JSONObject jsonObject = new JSONObject().put("id", totalvalue.getString("destination_country_id"));
//            Log.i("valueJSON", jsonObject.toString());
//
//            final LoadingDialog loadingDialog = new LoadingDialog(DocumentUpload.this);
//            loadingDialog.show();
//            Log.i("URLSLS", ApiURLNew.URL_SEND_MONEY_CHECK_DOCUMENT_TYPE);
//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SEND_MONEY_CHECK_DOCUMENT_TYPE, jsonObject, new Response.Listener<JSONObject>() {
//                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
//                @Override
//                public void onResponse(JSONObject response) {
//                    loadingDialog.dismiss();
//                    Log.i("Response", response.toString());
//                    try {
//
//                        if (response.getString("status_code").equals("1000")) {
//                            Object o = response.get("results");
//                            if ((o instanceof JSONArray)) {
//                                Log.i("JSONLENGTH", String.valueOf(((JSONArray) o).length()));
//                                for (int i = 0; i < ((JSONArray) o).length(); i++) {
//
//                                    agentModel.add(((JSONArray) o).getJSONObject(i).getString("sub_agent_id") + "," + ((JSONArray) o).getJSONObject(i).getString("name"));
//                                }
//                                Log.i("valueLength", String.valueOf(agentModels.size()));
//                                ViewGroup hourButtonLayout = (ViewGroup) findViewById(R.id.rgRadiGroup);  // This is the id of the RadioGroup we defined
//                                for (int i = 0; i < agentModel.size(); i++) {
//                                    radioButton = new RadioButton(AgentLoactionActivity.this);
//                                    radioButton.setId(i);
//                                    String[] g = agentModel.get(i).split(",");
//                                    totalvalue.put(g[1], g[0]);
//                                    radioButton.setText(g[1]);
////                                    button.setChecked(i == currentHours); // Only select button with same index as currently selected number of hours// This is a custom button drawable, defined in XML
//                                    hourButtonLayout.addView(radioButton);
//                                }
//
//                            }
//                        }
//                    } catch (JSONException e) {
//                        loadingDialog.dismiss();
//                        CustomToast.showMessage(AgentLoactionActivity.this, getResources().getString(R.string.server_exception2));
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadingDialog.dismiss();
//                    CustomToast.showMessage(AgentLoactionActivity.this, NetworkErrorHandler.getMessage(error, AgentLoactionActivity.this));
//                }
//            });
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, "jsonrequest");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }
//

    public void dateOfBirthDialog(final EditText date) {
        Calendar mcurrentDate = Calendar.getInstance();

        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        int month = mcurrentDate.get(Calendar.MONTH);
        int year = mcurrentDate.get(Calendar.YEAR);
        DatePickerDialog dialog = new DatePickerDialog(DocumentUpload.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String formattedDay, formattedMonth;
                if (dayOfMonth < 10) {
                    formattedDay = "0" + dayOfMonth;
                } else {
                    formattedDay = dayOfMonth + "";
                }

                if ((monthOfYear + 1) < 10) {
                    formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                } else {
                    formattedMonth = String.valueOf(monthOfYear + 1) + "";
                }
                date.setText(String.valueOf(year) + "-" + String.valueOf(formattedMonth) + "-" + String.valueOf(formattedDay));
//                @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                String format = formatter.format(date.getText().toString());
//                Log.i("dateValue", format);
            }
        }, year, month, day);

        dialog.show();


    }


    private void getDocument() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        // Only the system receives the ACTION_OPEN_DOCUMENT, so no need to test.
        startActivityForResult(intent, REQUEST_CODE_DOC);
    }


    @Override
    protected void onActivityResult(int req, int result, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(req, result, data);
        if (result == RESULT_OK) {
            Uri fileuri = data.getData();
            docFilePath = getFileNameByUri(this, fileuri);

            String valiue = document_type.getSelectedItem().toString();
            if (valiue != null && !valiue.isEmpty()) {
                if (issue_expire != null && !issue_expire.getText().toString().isEmpty()) {
                    if (issue_date != null && !issue_date.getText().toString().isEmpty()) {
                        if (docFilePath != null && !docFilePath.isEmpty()) {
                            for (DocumentModel documentUpload : documentModels) {
                                if (document_type.getSelectedItem().equals(documentUpload.getName())) {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
                                    Date newDate = null, expire = null;
                                    try {
                                        newDate = format.parse(issue_date.getText().toString());
                                        expire = format.parse(issue_date.getText().toString());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                                    String date = format.format(newDate);
                                    String expiredate = format.format(expire);
                                    uploadImage(docFilePath, documentUpload.getDocId(), documentUpload.getName(), "1", date, expiredate, "4");
                                }
                            }
                        }
                    } else {
                        issue_date.setError("Flied Required");
                    }
                } else {
                    issue_expire.setError("Flied Required");
                }
            } else {
                CustomToast.showMessage(DocumentUpload.this, "Select Document Type");
            }
            upload_text.setText(docFilePath);

        }
    }

// get file path

    private String getFileNameByUri(Context context, Uri uri) {
        String filepath = "";//default fileName
        //Uri filePathUri = uri;
        File file;
        if (uri.getScheme().toString().compareTo("content") == 0) {
            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns.DATA, MediaStore.Images.Media.ORIENTATION}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            cursor.moveToFirst();

            String mImagePath = cursor.getString(column_index);
            cursor.close();
            filepath = mImagePath;

        } else if (uri.getScheme().compareTo("file") == 0) {
            try {
                file = new File(new URI(uri.toString()));
                if (file.exists())
                    filepath = file.getAbsolutePath();

            } catch (URISyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            filepath = uri.getPath();
        }
        return filepath;
    }

    public void uploadImage(String imagePath, String doc_id, String docName, String docNumber, String issueDate, String issuesExpire, String issues_country_id) {
        loadingDialog.show();
        File file = new File(imagePath);

        uploadHeaderParams = new HashMap<>();
        uploadHeaderParams.put("document_type_id", doc_id);
        uploadHeaderParams.put("document_name", docName);
        uploadHeaderParams.put("document_number", docNumber);
        uploadHeaderParams.put("issued_date", issueDate);
        uploadHeaderParams.put("expire_date", issuesExpire);
        uploadHeaderParams.put("issued_country_id", "4");
        Log.i("value", uploadHeaderParams.toString());

        MultipartRequest mr = new MultipartRequest(ApiURLNew.URL_SEND_MONEY_UPLOAD_DOCUMENT, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                loadingDialog.dismiss();
//                Picasso.with(getApplicationContext()).load(ApiUrl.URL_IMAGE_MAIN + session.getUserImage()).placeholder(R.mipmap.ic_user_load).into(ivUser);
                CustomToast.showMessage(DocumentUpload.this, NetworkErrorHandler.getMessage(volleyError, DocumentUpload.this));

            }
        },
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("REPOSNE", response);
                        try {
                            loadingDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(response);
                            String status_code = jsonObject.getString("status_code");

                            if (status_code.equals("1000")) {
                                CustomToast.showMessage(DocumentUpload.this, "Document Upload successfull");
                            } else {
                                String msg = jsonObject.getString("error");
                                CustomToast.showMessage(DocumentUpload.this, msg);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, file, uploadHeaderParams, "filereq"
        );
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        mr.setRetryPolicy(policy);
        PayQwikApp.getInstance().addToRequestQueue(mr);

    }

    void getTranctionCreateAPi() {
        loadingDialog.show();
        try {
            JSONObject jsonObject = new JSONObject().put("destination_country_id", totalvalue.getString("destination_country_id")).put("service_type", 0);
            jsonObject.put("payment_type", "0");
            jsonObject.put("sending_amount", totalvalue.getString("sending_amount"));
            jsonObject.put("received_amount", totalvalue.getString("sending_amount"));
            jsonObject.put("exchange_rate", totalvalue.getString("exchangerate"));
            jsonObject.put("transfer_fees", totalvalue.getString("transfer_fee"));
            jsonObject.put("transfer_fee_id", totalvalue.getString("transfer_fee_id"));
            jsonObject.put("user_id", totalvalue.getString("user_id"));
            jsonObject.put("sessionId", session.getUserSessionId());
//            jsonObject.put("")
            jsonObject.put("sub_agent_id", totalvalue.getString(radio));
            jsonObject.put("branch_id", "");
            Log.i("valueJSON", jsonObject.toString());

            final LoadingDialog loadingDialog = new LoadingDialog(DocumentUpload.this);
            loadingDialog.show();
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SEND_MONEY_GETCREATETRANSACTION, jsonObject, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onResponse(JSONObject response) {
                    loadingDialog.dismiss();
                    Log.i("Response", response.toString());
                    try {
                        if (response.getString("status_code").equals("1000")) {
                            CustomSuccessDialog customSuccessDialog = new CustomSuccessDialog(DocumentUpload.this, "Transaction Successful", "");
                            customSuccessDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    sendRefresh();
                                    Intent intent = new Intent(DocumentUpload.this, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    finishAffinity();
                                    startActivity(intent);
                                }
                            });
                            customSuccessDialog.show();
                        } else {
                            CustomToast.showMessage(DocumentUpload.this, "Transaction Failed");
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(DocumentUpload.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(DocumentUpload.this, NetworkErrorHandler.getMessage(error, DocumentUpload.this));
                }
            });
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, "json");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(DocumentUpload.this).sendBroadcast(intent);
    }
}



