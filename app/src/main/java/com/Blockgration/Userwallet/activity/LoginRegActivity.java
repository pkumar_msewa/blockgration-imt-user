package com.Blockgration.Userwallet.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.Blockgration.fragment.userfragment.LoginFragment;
import com.Blockgration.fragment.userfragment.RegisterFragment;
import com.Blockgration.model.UserModel;
import com.Blockgration.Userwallet.R;

import java.util.List;


/**
 * Created by Ksf on 3/27/2016.
 */
public class LoginRegActivity extends SuperActivity {

    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    TextView textView;

    int NumbOfTabs = 2;

    private UserModel session = UserModel.getInstance();
    private List<UserModel> userArray;
    private CharSequence[] TitlesEnglish;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentManager = getSupportFragmentManager();
        setContentView(R.layout.activity_tab_layout);
        TitlesEnglish = new CharSequence[]{getResources().getString(R.string.login), getResources().getString(R.string.register)};
        mainPager = (ViewPager) findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
        mSlidingTabLayout.setupWithViewPager(mainPager);

        textView = (TextView) findViewById(R.id.textView);

        textView.setVisibility(View.GONE);
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                LoginFragment tab1 = new LoginFragment();
                return tab1;
            } else {
                RegisterFragment tab2 = new RegisterFragment();
                return tab2;
            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }

}
