package com.Blockgration.Userwallet.activity.shopping;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.model.CurrentAccountModel;
import com.Blockgration.model.ShoppingModel;
import com.Blockgration.model.UserModel;

import com.Blockgration.util.NetworkErrorHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Ksf on 4/6/2016.
 */
public class InCartAdapter extends BaseAdapter {
    private PQCart cart = PQCart.getInstance();
    private Context context;
    private PQCart pqCart = PQCart.getInstance();
    private RequestQueue rq;
    private UserModel session = UserModel.getInstance();
    private JsonObjectRequest postReq;
    private String tag_json_obj = "json_events";
    boolean addresult, removeresult = false;
    long ticketQty = 1;
    long totalPrice = 0;
    private CurrentAccountModel currentAccountModel;

    LoadingDialog loadingDialog;

    private InCartListner inCartListner;

    public InCartAdapter(Context context, InCartListner inCartListner, CurrentAccountModel currentAccountModel) {
        this.context = context;
        this.inCartListner = inCartListner;
        this.currentAccountModel = currentAccountModel;
        loadingDialog = new LoadingDialog(context);
        try {
            rq = Volley.newRequestQueue(context);
        } catch (Exception e) {

        }
    }

    @Override
    public int getCount() {
        return cart.getProductsInCartArray().size();
    }

    @Override
    public Object getItem(int position) {
        return cart.getProductsInCartArray().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parentView) {
        final int currentPosition = position;
        final ShoppingModel product = cart.getProductsInCartArray().get(currentPosition);
        final ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_incart_product, null);
            viewHolder = new ViewHolder();
            viewHolder.ivProductImage = (ImageView) convertView.findViewById(R.id.ivInCartProduct);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvInCartTitle);
            viewHolder.tvPrice = (TextView) convertView.findViewById(R.id.tvInCartRupees);
            viewHolder.lLInCartBackGround = (LinearLayout) convertView.findViewById(R.id.lLIncartProduct);
            viewHolder.ivInCartRemove = (ImageView) convertView.findViewById(R.id.ivInCartRemove);
            viewHolder.tvProductQty = (TextView) convertView.findViewById(R.id.tvProductQty);
            viewHolder.btnAddItem = (Button) convertView.findViewById(R.id.btnAddItem);
            viewHolder.btnRemoveItem = (Button) convertView.findViewById(R.id.btnRemoveItem);
            viewHolder.tvProdPrice = (TextView) convertView.findViewById(R.id.tvProdPrice);
            viewHolder.tvProdDisc = (TextView) convertView.findViewById(R.id.tvProdDisc);
            viewHolder.tvProdShip = (TextView) convertView.findViewById(R.id.tvProdShip);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvTitle.setText(product.getpName());
        viewHolder.tvProdPrice.setText("Price: "+currentAccountModel.getCurrencyCode() + product.getpPrice());
//        viewHolder.tvPriceOld.setVisibility(View.GONE);
//        viewHolder.tvPriceOld.setPaintFlags(viewHolder.tvPriceOld.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        viewHolder.tvPriceOld.setText("\u20B9 " + "XXX");
        viewHolder.tvProductQty.setText(String.valueOf(product.getpQty()));
        int price = Integer.parseInt(viewHolder.tvProductQty.getText().toString()) * Integer.parseInt(viewHolder.tvProdPrice.getText().toString().replaceAll("\\D+",""));
        viewHolder.tvPrice.setText(currentAccountModel.getCurrencyCode() + String.valueOf(price));

        String[] resultArray = product.getpImage().trim().split("#");
        final String pImage1 = resultArray[0];
        if (pImage1 != null && pImage1.length() != 0) {
            AQuery aQuery = new AQuery(context);
            aQuery.id(viewHolder.ivProductImage).image(pImage1).background(R.drawable.ic_loading_image).getContext();
        }
        viewHolder.ivInCartRemove.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                final ShoppingModel product = cart.getProductsInCartArray().get(currentPosition);
                removeProduct(product, "removeall", viewHolder);

            }

        });
//        if(pqCart.isProductInCart(product)){
//            long qty = Long.parseLong(viewHolder.tvProductQty.getText().toString());
//            ticketQty = qty;
//            Log.i("NUMBER", "qty : " + qty);
//            qty++;
//            long price = product.getpPrice();
//            price = price * qty;
//            viewHolder.tvProductQty.setText(String.valueOf(price));
//            viewHolder.tvPrice.setText("\u20B9 " +" "+ String.valueOf(price));
//            totalPrice =  price;
//            ticketQty = qty;
//        }

        viewHolder.btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ShoppingModel product = cart.getProductsInCartArray().get(currentPosition);
                if(product.getpQty()<10){
                    addProduct(product, viewHolder);
                    Log.i("productid", String.valueOf(product.getpQty()));
                }

//                if(addProduct(product)){
//                Log.i("Postionm", String.valueOf(currentPosition));
//                    long qty = Long.parseLong(viewHolder.tvProductQty.getText().toString());
//                    ticketQty = qty;
//                    Log.i("NUMBER", "qty : " + qty);
//                    qty++;
//                    long price = product.getpPrice();
//                    price = price * qty;
//                    viewHolder.tvProductQty.setText(String.valueOf(qty));
//                    viewHolder.tvPrice.setText("\u20B9 " +" "+ String.valueOf(price));
//                    totalPrice =  price;
//                    ticketQty = qty;
//                }
            }
        });

        viewHolder.btnRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ShoppingModel product = cart.getProductsInCartArray().get(currentPosition);
                if (product.getpQty() != 1) {
                    removeProduct(product, "remove", viewHolder);
                }

            }
        });

//        if (currentPosition % 2 == 0) {
//            viewHolder.lLInCartBackGround.setBackgroundResource(android.R.color.white);
//        } else {
//            viewHolder.lLInCartBackGround.setBackgroundResource(R.color.mark_disselceted);
//        }
        return convertView;
    }

    public boolean addProduct(final ShoppingModel shopingModel, final  ViewHolder viewHolder) {
        loadingDialog.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobile", session.getUserMobileNo());
            jsonRequest.put("email", session.getUserEmail());
            jsonRequest.put("firstName", session.getUserFirstName());
            jsonRequest.put("lastName", "dawn");
            jsonRequest.put("country", "india");
            jsonRequest.put("productId", shopingModel.getPid());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("CARTADDONEURL", ApiURLNew.URL_SHOPPONG_ADD_ITEM_CART);
            Log.i("CARTADDONEREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SHOPPONG_ADD_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("CARTADDONERES", jsonObj.toString());
                        String code = jsonObj.getString("code");

                        if (code != null && code.equals("S00")) {

                            shopingModel.setpQty(shopingModel.getpQty() + 1);
                            shopingModel.update();
                            loadingDialog.dismiss();
                            inCartListner.taskCompleted();
                            int price = Integer.parseInt(viewHolder.tvProductQty.getText().toString()) * Integer.parseInt(viewHolder.tvProdPrice.getText().toString().replaceAll("\\D+",""));
                            viewHolder.tvPrice.setText(String.valueOf(price));
                            notifyDataSetChanged();
                            addresult = true;
                        } else {
                            loadingDialog.dismiss();
                            CustomToast.showMessage(context, "Item cannot be added");
                        }

                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(context, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 120000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
            return addresult;
        }
        return addresult;
    }

    public boolean removeProduct(final ShoppingModel shopingModel, final String urlShoppongRemoveItemCart, final ViewHolder viewHolder) {
        loadingDialog.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobile", session.getUserMobileNo());
            jsonRequest.put("productId", shopingModel.getPid());
            if (urlShoppongRemoveItemCart.equals("removeall")) {
                jsonRequest.put("quentity", "all");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (jsonRequest != null) {
            Log.i("CARTREMOVEONEURL", ApiURLNew.URL_SHOPPONG_REMOVE_ITEM_CART);
            Log.i("CARTREMOVEONEREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SHOPPONG_REMOVE_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        loadingDialog.dismiss();
                        Log.i("CARTREMOVEONERES", jsonObj.toString());
                        String code = jsonObj.getString("code");
                        if (code != null && code.equals("S00")) {
                            if (urlShoppongRemoveItemCart.equals("removeall")) {
                                pqCart.addRemoveProductsInCart(shopingModel, shopingModel.getpQty());
                                inCartListner.taskCompleted();
                                loadingDialog.dismiss();
                                Intent intent = new Intent("cart-clear");
                                intent.putExtra("updates", "2");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                                int price = Integer.parseInt(viewHolder.tvProductQty.getText().toString()) * Integer.parseInt(viewHolder.tvProdPrice.getText().toString().replaceAll("\\D+",""));
                                viewHolder.tvPrice.setText(String.valueOf(price));
                                notifyDataSetChanged();
                            } else {
                                shopingModel.setpQty(shopingModel.getpQty() - 1);
                                shopingModel.save();

//                            pqCart.addRemoveProductsInCart(shopingModel, shopingModel.getpQty());
                                loadingDialog.dismiss();
                                inCartListner.taskCompleted();
                                notifyDataSetChanged();
                                removeresult = true;
                            }

                            if(pqCart.getProductsInCartArray().isEmpty()){
                                inCartListner.closeCart();
                            }
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(context, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();

                    CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }
            };
            int socketTimeout = 120000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
        return removeresult;
    }

    static class ViewHolder {
        ImageView ivProductImage;
        LinearLayout lLInCartBackGround;
        ImageView ivInCartRemove;
        TextView tvTitle;
        TextView tvPrice, tvProductQty, tvProdPrice, tvProdShip, tvProdDisc;
        Button btnRemoveItem, btnAddItem;
    }

}
