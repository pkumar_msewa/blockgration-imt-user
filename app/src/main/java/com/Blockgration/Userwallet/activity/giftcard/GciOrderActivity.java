package com.Blockgration.Userwallet.activity.giftcard;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.MainActivity;
import com.Blockgration.Userwallet.activity.shopping.CustomSuccessDialog;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.UserModel;

import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.ServiceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;


public class GciOrderActivity extends AppCompatActivity {

    private EditText etName, etEmail, etAddress, etCity, etState, etCountry, etPersonalMsg;
    private Button btnRegisterSubmit, btnTerms;

    private String Name, email, address, city, state, country, personalMsg, date, hash, clientOrderId;

    private TextView tvAmount, tvQuantity;

    int quantity;

    double price;

    private LoadingDialog loadDlg;

    private TextInputLayout ilName, ilEmail, ilAddress, ilCity, ilState, ilCountry, ilPersonalMsg;
    private JSONObject jsonRequest;

    private final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final Pattern hasLowercase = Pattern.compile("[a-z]");
    private final Pattern hasNumber = Pattern.compile("\\d");
    private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");

    private UserModel session = UserModel.getInstance();

    Calendar c = Calendar.getInstance();

    //    private LoadingDialog orderLoadindDialog;
    //Volley Tag
    private String tag_json_obj = "json_user";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gci_order);
        loadDlg = new LoadingDialog(GciOrderActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        hash = getIntent().getStringExtra("Hash");
        price = getIntent().getDoubleExtra("Price", 0);
        quantity = getIntent().getIntExtra("Quantity", 1);

        etName = (EditText) findViewById(R.id.etGciName);
        etEmail = (EditText) findViewById(R.id.etGciEmail);


        etAddress = (EditText) findViewById(R.id.etGciAddress);
        etCity = (EditText) findViewById(R.id.etGciCity);
        etCountry = (EditText) findViewById(R.id.etGciCountry);
        etState = (EditText) findViewById(R.id.etGciState);
        etPersonalMsg = (EditText) findViewById(R.id.etGciPersonalMsg);
        tvAmount = (TextView) findViewById(R.id.tvAmount);
        tvQuantity = (TextView) findViewById(R.id.tvQty);

        tvAmount.setText("Total Amount : " + "\u20B9 " + " " + price);
        tvQuantity.setText("Quantity : " + quantity);


        btnRegisterSubmit = (Button) findViewById(R.id.btnRegisterSubmit);

        ilName = (TextInputLayout) findViewById(R.id.ilGciName);
        ilEmail = (TextInputLayout) findViewById(R.id.ilGciEmail);
        ilAddress = (TextInputLayout) findViewById(R.id.ilGciAddress);
        ilCity = (TextInputLayout) findViewById(R.id.ilGciCity);
        ilState = (TextInputLayout) findViewById(R.id.ilGciState);
        ilCountry = (TextInputLayout) findViewById(R.id.ilGciCountry);
        ilPersonalMsg = (TextInputLayout) findViewById(R.id.ilGciPersonalMsg);


//        firstName = getIntent().getStringExtra("NAME");
//        newPassword = getIntent().getStringExtra("PASSWORD");
//        email = getIntent().getStringExtra("EMAIL");
//        mobileNo = getIntent().getStringExtra("MOBILE");
//        address = getIntent().getStringExtra("ADDRESS");
//        city = getIntent().getStringExtra("CITY");
//        state = getIntent().getStringExtra("STATE");
//        country = getIntent().getStringExtra("COUNTRY");
//        pincode = getIntent().getStringExtra("PINCODE");


        btnRegisterSubmit.setTextColor(Color.parseColor("#ffffff"));
//        etEmail.addTextChangedListener(new MyTextWatcher(etEmail));


        btnRegisterSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                submitForm();
            }
        });

        //DONE CLICK ON VIRTUAL KEYPAD
        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submitForm();
                }
                return false;
            }
        });

        fillViews();

    }

    private void fillViews() {
        etName.setText(session.getUserFirstName());
        etEmail.setText(session.getUserEmail());

        etAddress.setText(session.getUserAddress());
        etCountry.setText("INDIA");
    }


    private void submitForm() {

        if (!validateFName()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }
        if (!validateAddress()) {
            return;
        }
        if (!validateCIty()) {
            return;
        }
        if (!validateState()) {
            return;
        }
        if (!validateCountry()) {
            return;
        }

        loadDlg.show();

        Name = etName.getText().toString();
        email = etEmail.getText().toString();
        address = etAddress.getText().toString();
        city = etCity.getText().toString();
        state = etState.getText().toString();
        country = etCountry.getText().toString();


        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        date = df.format(c.getTime());

        Log.i("DATE", "DATE : " + date);

        promotePay();
    }


    private boolean validateFName() {
        if (etName.getText().toString().trim().isEmpty()) {
            ilName.setError("Enter Name");
            requestFocus(etName);
            return false;
        } else {
            ilName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateAddress() {
        if (etAddress.getText().toString().trim().isEmpty()) {
            ilAddress.setError("Enter Address");
            requestFocus(etAddress);
            return false;
        } else {
            ilAddress.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCIty() {
        if (etCity.getText().toString().trim().isEmpty()) {
            ilCity.setError("Enter your City");
            requestFocus(etCity);
            return false;
        } else {
            ilCity.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateState() {
        if (etState.getText().toString().trim().isEmpty()) {
            ilState.setError("Enter your State");
            requestFocus(etState);
            return false;
        } else {
            ilState.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCountry() {
        if (etCountry.getText().toString().trim().isEmpty()) {
            ilCountry.setError("Enter your Country");
            requestFocus(etCountry);
            return false;
        } else {
            ilCountry.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePersonalMsg() {
        if (etPersonalMsg.getText().toString().trim().isEmpty()) {
            ilPersonalMsg.setError("Enter Personal Message");
            requestFocus(etPersonalMsg);
            return false;
        } else {
            ilPersonalMsg.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateEmail() {
        String email = etEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            ilEmail.setError("Enter valid email");
            requestFocus(etEmail);
            return false;
        } else {
            ilEmail.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private void promotePay() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("orderDate", date);
            jsonRequest.put("billingName", Name);
            jsonRequest.put("billingEmail", email);
            jsonRequest.put("billingAddressLine1", address);
            jsonRequest.put("billingCity", city);
            jsonRequest.put("billingState", state);
            jsonRequest.put("billingCountry", country);
            jsonRequest.put("receiversName", Name);
            jsonRequest.put("receiversEmail", email);
            jsonRequest.put("shippingName", Name);
            jsonRequest.put("shippingEmail", email);
            jsonRequest.put("shippingAddressLine1", address);
            jsonRequest.put("shippingCity", city);
            jsonRequest.put("shippingState", state);
            jsonRequest.put("shippingCountry", country);
            jsonRequest.put("shippingZip", "560068");
            jsonRequest.put("clientOrderId", ServiceUtility.createOrderId());
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("brandHash", hash);
            jsonRequest.put("denomination", price);
            jsonRequest.put("productType", "digital");
            jsonRequest.put("quantity", quantity);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("CardRequest", jsonRequest.toString());
            Log.i("Registration API", ApiURLNew.URL_GIFT_CARD_ORDER);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_GIFT_CARD_ORDER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Registration Response", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            sendRefresh();
                            showSuccessDialog("Congratulations Order Successful","We have sent you the card details at your email address. Please check your email account for purchased gift card details.");

                        }else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        }

                        else {
                            CustomToast.showMessage(getApplicationContext(), messageAPI);
                            loadDlg.dismiss();

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        Log.i("Type", "jSON");
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("Type", "ERro");
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    loadDlg.dismiss();


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showSuccessDialog(String message, String Title) {
        CustomSuccessDialog builder = new CustomSuccessDialog(GciOrderActivity.this, Title, message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(mainActivityIntent);
                finish();
            }
        });
        builder.show();
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(GciOrderActivity.this).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(GciOrderActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

}
