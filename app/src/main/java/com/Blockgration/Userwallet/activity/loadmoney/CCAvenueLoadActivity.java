package com.Blockgration.Userwallet.activity.loadmoney;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.MainActivity;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.AesCryptUtil;
import com.Blockgration.util.NetworkErrorHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ksf on 4/11/2016.
 */
public class CCAvenueLoadActivity extends AppCompatActivity {

    public static final String EXTRA = "name";
    String encVal;
    //HTTP Response Variables
    private int amountToLoad;
    private String responseCode;
    private LoadingDialog loadDlg;
    private String orderId;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private ImageView iconback;
    private String tag_json_obj = "load_money";

    UserModel userModel = UserModel.getInstance();

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_webview);
        amountToLoad = getIntent().getIntExtra(CCAvenueLoadActivity.EXTRA, 0);
        loadDlg = new LoadingDialog(CCAvenueLoadActivity.this);
        try {
            iconback = (ImageView) findViewById(R.id.iconback);
            iconback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            orderId = String.valueOf(System.currentTimeMillis());
            final LinearLayout llLoadMoneyWebViewLoading = (LinearLayout) findViewById(R.id.llLoadMoneyWebViewLoading);
            final WebView webview = (WebView) findViewById(R.id.webview);
            webview.getSettings().setJavaScriptEnabled(true);
            if (webview != null) {
                webview.setWebViewClient(new WebViewClient() {

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(webview, url);
                        llLoadMoneyWebViewLoading.setVisibility(View.GONE);
                        if (url.indexOf("ccavenueTranstionFailure") != -1) {
                            finish();
                            webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                        }
                    }

                    @Override
                    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                        Toast.makeText(getApplicationContext(), "Oh no!" + description, Toast.LENGTH_SHORT).show();
                        llLoadMoneyWebViewLoading.setVisibility(View.GONE);
                        webview.loadUrl("https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction&merchant_id=47281&encRequest=5b1b2f5903f1920d9bcc3566fae0815cfa634235691fdc3dd35fae4a2d971b8a01f507cf9ac8390ca4f0b42e368f2416ce4a4bf8dfce0324ecb3aa98c9c2ad1de5190c2287681bdb2fc8371c90d8f3f973c99f64807d07759b336c2e8a7e68a54eaaa98f0d57659acd23eeda44654ab7637bd6ff76fb8d98333549379a8b4c0df728c599de8ed5ff671d1c5d9767d45f96bb418acba832b05e9e827eb55ef3fc602cababc9a7ae79385d58d38566f029fae2884afff4ead3f77ebd34b240c607b1a483b354bce9bfc13f75652562d55f8a3c0f83043c804ad51ee0443353dd64f5af24f42c40e7384e6e0d43e43ddd5f6970ebc61b99e32fc5e40588f4de7aa10ed6c153082d60b982030a471a7746eb71ea7fd9cd0546a942f13a04a1d1ae40aebdb9b2b04732e1b5148d00420753a88ad4f0b8bb1b221f0705c6ed607d8cd9b40113cded63e559d38e4d25a1b1c701afe39f0a2cfa7a21590a60780b4095f008b88ad055a879148be2894f520b700f08c53f19f2b1eb23c6c59415d1b4f828b0ccc4195a5a4e86dcfb7d70aea293bb4f45f0054293bff2903163b4247cba889922f839c1702c3fa6f04ae22dbc77b4eb33672a173508b2fb344c426d4741661da696461dd1dceb1ac65cbbf268a0b66066e0ea4ace0ac226de5276945ec26c82db63b4f0c3b68476702178385894fb054db7492fd0c2492f4dd31a53631e5b5b5840a11faede4ad37608914cd668bb9810edafba3e533779493f79c0f74f5bcc4331ae05f2598542031f87e04ae47f11df2e15c39aebc8a865d565044abfd178bde871c5d05ebfba7112c93e1b848c423d6fc470fcf062c7232b27d11e27ab8402c7942257588f54f76d8483211d6d86dd00fca92172c67fe1ffa46eeac3e4cd6d575c1401437fdec228685c9d947725583c5c0cc33743c47ba13a7a49b2eee3cc758d3c1ecdd6b58289c85d08801bbcf82418f86ce322762ee76496a7718dd69ec01a6cba405f40685d1f8f88212fa40d55d1a5d1f43bbbde0e13f8ba12e0bb2305cb490a055ec1061840292001e85bd210415812c65ef4521e742547fd72b4ca7e6c3ac1fe8ae4ccbc6c65f95e15db3136918b62f9d4282b7219711b6bf5dfbd078ee002a51b377c97a0d709a5cf47f9d07177d01a0fd6477f4e5f10d163ef154a2bf9ae12dd5facd9cd07f9036aaa2170a30c0acf645c61b2a9104c1eba39b1ea53fa07d2a39ce79f78b0c222d956d7fdc09d90a9344d127f6b2ec1d7fe20e1e9d2c2cc3bc27d3b90f41ff4888d7991f30ee81c508efad3ca2643c5f7bb&access_code=AVXL02BI72CL24LXLC");
                    }

                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                        if (url.contains("http://119.18.52.107/ccavenueTranstionFailure")) {
                            view.stopLoading();
                            finish();
                        } else if (url.contains("http://119.18.52.107/ccavenueTranstionSuccessfull")) {
                            view.stopLoading();
                            getJSONLOAD();
                        } else if (url.contains("https://ubimpi.electracard.com/electraSECURE/vbv/MPIEntry.jsp")) {
                            view.stopLoading();
                            getJSONLOAD();
                        }
                    }
                });
            }
            String key = "promo_code=&integration_type=iframe_normal&billing_state=Tamilnadu&order_id="
                    + "8553926329" + "&delivery_name=PayQwik&billing_email=care@payqwik.in&customer_identifier="
                    + "8553926329" + "&currency=INR&delivery_state=Tamilnadu&delivery_tel=" + "8553926329"
                    + "&amount=" + amountToLoad + "&billing_country=India&delivery_country=India&successURL="
                    + "success" + "&billing_tel=" + "8553926329"
                    + "&delivery_city=Chennai&merchant_id=47281&redirect_url=http://119.18.52.107/ccavenueTranstionSuccessfull&cancel_url=http://119.18.52.107/ccavenueTranstionFailure&payqwikBal=0.00&billing_name=PayQwik&delivery_address=India Land Building&delivery_zip=600058&failureURL=http://wap.krayons.mobi/PayQuickRes&billing_zip=600058&sessionId=54225&billing_city=Chennai&merchant_param1="
                    + "8553926329" + "&language=EN&merchant_param5=" + "ASDFFDWWEWFFSDS12452" + "&mobileNumber="
                    + "8553926329" + "&merchant_param4=" + "skumar@firstgobaldata.com" + "&merchant_param3="
                    + "santosh" + "&merchant_param2=" + "2323134"
                    + "&billing_address=India Land Building";

            AesCryptUtil aesUtil = new AesCryptUtil("3FB85A32035FD55667BAC54C51463BE3");
            String encRequest = aesUtil.encrypt(key);
            webview.getSettings().setLoadsImagesAutomatically(true);
            webview.getSettings().setPluginState(WebSettings.PluginState.ON);
            webview.getSettings().setAllowFileAccess(true);
            webview.getSettings().setLoadWithOverviewMode(true);
            webview.getSettings().setUseWideViewPort(false);
            webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webview.loadUrl("https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction&merchant_id=47281&encRequest=" + encRequest + "&access_code=AVXL02BI72CL24LXLC");

        } catch (java.lang.NoSuchFieldError e) {
            e.printStackTrace();
        }
    }

    void getJSONLOAD() {
        loadDlg.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sessionId", userModel.getUserSessionId());
            jsonObject.put("amount", amountToLoad);
            jsonObject.put("transactionRefNo", orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_LOAD_MONEY, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    loadDlg.dismiss();
                    String message = response.getString("message");
                    String code = response.getString("code");
                    if (code != null & code.equals("S00")) {
                        CustomToast.showMessage(CCAvenueLoadActivity.this, "Load Money Successful");
                        sendRefresh();
                        finishAffinity();
                        Intent intent = new Intent(CCAvenueLoadActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                    } else {
                        finish();
                        CustomToast.showMessage(CCAvenueLoadActivity.this, message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    loadDlg.dismiss();
                    finish();
                    CustomToast.showMessage(CCAvenueLoadActivity.this, "Error connecting to server");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                loadDlg.dismiss();
                finish();
                CustomToast.showMessage(CCAvenueLoadActivity.this, NetworkErrorHandler.getMessage(volleyError, CCAvenueLoadActivity.this));
                volleyError.printStackTrace();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("hash", "123");
                return map;
            }

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        // Adding request to request queue
        PayQwikApp.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
    }

    private void sendRefresh() {

        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "3");
        LocalBroadcastManager.getInstance(CCAvenueLoadActivity.this).sendBroadcast(intent);
    }

//}


//package com.msewa.faspay.activity.loadmoney;
//
//import android.annotation.TargetApi;
//import android.app.Dialog;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.webkit.JavascriptInterface;
//import android.webkit.WebChromeClient;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.Toast;
//
//import LoadingDialog;
//import com.msewa.faspay.R;
//import com.serviceOntario.in.activity.status.StatusActivity;
//import com.serviceOntario.uitl.AesCryptUtil;
//import com.serviceOntario.uitl.LogCat;
//
//
///**
// * Created by admin on 4/30/2016.
// */
//public class CCAvenueLoadActivity extends AppCompatActivity {
//
//    public static final String EXTRA_AMOUNT = "amount";
//    private android.webkit.WebView wbview;
//    static String response = null;
//    private ImageView back;
//    private Dialog dialog;
//    private String sessionId;
//    private String amount;
//    private ProgressBar progressBar;
//    private LinearLayout frame;
//    private LoadingDialog loadingDialog;
//
//    @TargetApi(Build.VERSION_CODES.KITKAT)
//    @Override
//
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_movie_webview);
//        amount = getIntent().getStringExtra(CCAvenueLoadActivity.EXTRA_AMOUNT);
//        frame = (LinearLayout) findViewById(R.id.llLoadMoneyWebViewLoading);
//        loadingDialog = new LoadingDialog(CCAvenueLoadActivity.this);
//        back = (ImageView) findViewById(R.id.iconback);
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//        try {
//            class MyJavaScriptInterface {
//                @JavascriptInterface
//                public void processHTML(String html) {
//                    // process the html as needed by the app
//                    // TODO Process the HTML
//
//                    String status = null;
//                    if (html.indexOf("Source : ERROR") != -1) {
//                        status = "Transaction Declined!";
//                    } else {
//                        status = "Status Not Known!";
//                    }
//                    // Start Activity with service_status_activity put in extra
//                    final Intent intent = new Intent(getApplicationContext(), StatusActivity.class);
//                    intent.putExtra(StatusActivity.EXTRA_STATUS, status);
//                    Thread timer = new Thread() {
//                        public void run() {
//                            try {
//                                sleep(10);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            } finally {
//                                startActivity(intent);
//                                finish();
//                            }
//                        }
//                    };
//                    timer.start();
//                }
//            }
//
//            wbview = (WebView) findViewById(R.id.webview);
//            wbview.getSettings().setJavaScriptEnabled(true);
//            wbview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
//
//
//            if (wbview != null) {
//                wbview.setWebViewClient(new WebViewClient() {
//
//                    @Override
//                    public void onPageFinished(WebView view, String url) {
//                        super.onPageFinished(wbview, url);
//                        frame.setVisibility(View.GONE);
//                        if (url.indexOf("ccavenueTranstionFailure") != -1) {
//                            final Intent intent = new Intent(getApplicationContext(), StatusActivity.class);
//                            intent.putExtra(StatusActivity.EXTRA_STATUS, "Transaction Declined!");
//                            startActivity(intent);
//                            wbview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
//                        }
//                    }
//
//                    @Override
//                    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                        Toast.makeText(getApplicationContext(), "Oh no!" + description, Toast.LENGTH_SHORT).show();
//                        frame.setVisibility(View.GONE);
//                        wbview.loadUrl("https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction&merchant_id=47281&encRequest=5b1b2f5903f1920d9bcc3566fae0815cfa634235691fdc3dd35fae4a2d971b8a01f507cf9ac8390ca4f0b42e368f2416ce4a4bf8dfce0324ecb3aa98c9c2ad1de5190c2287681bdb2fc8371c90d8f3f973c99f64807d07759b336c2e8a7e68a54eaaa98f0d57659acd23eeda44654ab7637bd6ff76fb8d98333549379a8b4c0df728c599de8ed5ff671d1c5d9767d45f96bb418acba832b05e9e827eb55ef3fc602cababc9a7ae79385d58d38566f029fae2884afff4ead3f77ebd34b240c607b1a483b354bce9bfc13f75652562d55f8a3c0f83043c804ad51ee0443353dd64f5af24f42c40e7384e6e0d43e43ddd5f6970ebc61b99e32fc5e40588f4de7aa10ed6c153082d60b982030a471a7746eb71ea7fd9cd0546a942f13a04a1d1ae40aebdb9b2b04732e1b5148d00420753a88ad4f0b8bb1b221f0705c6ed607d8cd9b40113cded63e559d38e4d25a1b1c701afe39f0a2cfa7a21590a60780b4095f008b88ad055a879148be2894f520b700f08c53f19f2b1eb23c6c59415d1b4f828b0ccc4195a5a4e86dcfb7d70aea293bb4f45f0054293bff2903163b4247cba889922f839c1702c3fa6f04ae22dbc77b4eb33672a173508b2fb344c426d4741661da696461dd1dceb1ac65cbbf268a0b66066e0ea4ace0ac226de5276945ec26c82db63b4f0c3b68476702178385894fb054db7492fd0c2492f4dd31a53631e5b5b5840a11faede4ad37608914cd668bb9810edafba3e533779493f79c0f74f5bcc4331ae05f2598542031f87e04ae47f11df2e15c39aebc8a865d565044abfd178bde871c5d05ebfba7112c93e1b848c423d6fc470fcf062c7232b27d11e27ab8402c7942257588f54f76d8483211d6d86dd00fca92172c67fe1ffa46eeac3e4cd6d575c1401437fdec228685c9d947725583c5c0cc33743c47ba13a7a49b2eee3cc758d3c1ecdd6b58289c85d08801bbcf82418f86ce322762ee76496a7718dd69ec01a6cba405f40685d1f8f88212fa40d55d1a5d1f43bbbde0e13f8ba12e0bb2305cb490a055ec1061840292001e85bd210415812c65ef4521e742547fd72b4ca7e6c3ac1fe8ae4ccbc6c65f95e15db3136918b62f9d4282b7219711b6bf5dfbd078ee002a51b377c97a0d709a5cf47f9d07177d01a0fd6477f4e5f10d163ef154a2bf9ae12dd5facd9cd07f9036aaa2170a30c0acf645c61b2a9104c1eba39b1ea53fa07d2a39ce79f78b0c222d956d7fdc09d90a9344d127f6b2ec1d7fe20e1e9d2c2cc3bc27d3b90f41ff4888d7991f30ee81c508efad3ca2643c5f7bb&access_code=AVXL02BI72CL24LXLC");
//                    }
//
//                    @Override
//                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                        super.onPageStarted(view, url, favicon);
//
//
//                    }
//                });
//            }
//            wbview.getSettings().setJavaScriptEnabled(true);
//            wbview.getSettings().setDomStorageEnabled(true);
//            wbview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
//            wbview.getSettings().setUserAgentString("Android");
//
//            wbview.getSettings().setLoadsImagesAutomatically(true);
//            wbview.getSettings().setPluginState(WebSettings.PluginState.ON);
//            wbview.getSettings().setAllowFileAccess(true);
//            wbview.getSettings().setLoadWithOverviewMode(true);
//            wbview.getSettings().setUseWideViewPort(false);
////            wbview.getSettings().setBuiltInZoomControls(true);
////            wbview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//            String DESKTOP_USERAGENT = wbview.getSettings().getUserAgentString();
//            DESKTOP_USERAGENT = DESKTOP_USERAGENT.replace("Mobile ", "");
//            wbview.getSettings().setUserAgentString(DESKTOP_USERAGENT);
//            String key = "promo_code=&integration_type=iframe_normal&billing_state=Tamilnadu&order_id="
//                    + "8553926329" + "&delivery_name=PayQwik&billing_email=care@payqwik.in&customer_identifier="
//                    + "8553926329" + "&currency=INR&delivery_state=Tamilnadu&delivery_tel=" + "8553926329"
//                    + "&amount=" + amount.replaceAll("[-+.^:,$]", "") + "&billing_country=India&delivery_country=India&successURL="
//                    + "success" + "&billing_tel=" + "8553926329"
//                    + "&delivery_city=Chennai&merchant_id=47281&redirect_url=http://119.18.52.107/ccavenueTranstionSuccessfull&cancel_url=http://119.18.52.107/ccavenueTranstionFailure&payqwikBal=0.00&billing_name=PayQwik&delivery_address=India Land Building&delivery_zip=600058&failureURL=http://wap.krayons.mobi/PayQuickRes&billing_zip=600058&sessionId=54225&billing_city=Chennai&merchant_param1="
//                    + "8553926329" + "&language=EN&merchant_param5=" + "ASDFFDWWEWFFSDS12452" + "&mobileNumber="
//                    + "8553926329" + "&merchant_param4=" + "skumar@firstgobaldata.com" + "&merchant_param3="
//                    + "santosh" + "&merchant_param2=" + "2323134"
//                    + "&billing_address=India Land Building";
////
//            System.out.println("KEYSS" + key);
//            AesCryptUtil aesUtil = new AesCryptUtil("3FB85A32035FD55667BAC54C51463BE3");
//            String encRequest = aesUtil.encrypt(key);
//
//            wbview.loadUrl("https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction&merchant_id=47281&encRequest=" + encRequest + "&access_code=AVXL02BI72CL24LXLC");
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
