package com.Blockgration.Userwallet.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.R;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.ImageLoadingUtils;
import com.Blockgration.util.MultipartRequest;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.SecurityUtil;
import com.Blockgration.util.Utility;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Ksf on 4/13/2016.
 */
public class EditProfileActivity extends SuperActivity {
    private EditText etEditFirstName, etEditAddress, etEditEmail, etEditMobile, etEditLastName, etEditDob;
    private RadioButton rbEditMale, rbEditFeMale;
    private Button btnEditSubmit;
    private ImageView ivEditProfilePic;
    private String firstName, lastName, address, email, mobileNo, gender, dob;
    private LoadingDialog loadDlg;
    private TextInputLayout ilEditFName, ilEditLName, ilEditMobile, ilEditEmail, ilEditAddress, ilEditDob;
    private FloatingActionButton btnEditProfilePic;
    private RequestQueue rq;
    private UserModel session = UserModel.getInstance();

    private AlertDialog.Builder dialogPicture;
    public static final String IMAGE_DIRECTORY_NAME = "PayQwik";
    private final int CameraResult = 1;
    private final int GalleryResult = 2;
    private Uri fileUri;
    private String userImage;
    private boolean isImage = false;
    private CompressImageTask compressTask;
    private ImageLoadingUtils utils;

    private Map<String, String> uploadHeaderParams;

    private String updatedImageUrl = null;

    private JSONObject jsonRequest;

    private Toolbar toolbar;
    private ImageButton ivBackBtn;

    private Bitmap btpUserImage;
    private String imageName = "";

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    //API>=23 Permission
    private static final int PERMISSION_REQUEST_CODE = 1;
    private String userChoosenTask;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        utils = new ImageLoadingUtils(getApplicationContext());
        setContentView(R.layout.activity_edit_profile);
        loadDlg = new LoadingDialog(EditProfileActivity.this);
        rq = Volley.newRequestQueue(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etEditFirstName = (EditText) findViewById(R.id.etEditFirstName);
        etEditAddress = (EditText) findViewById(R.id.etEditAddress);
        etEditEmail = (EditText) findViewById(R.id.etEditEmail);
        etEditMobile = (EditText) findViewById(R.id.etEditMobile);
        etEditLastName = (EditText) findViewById(R.id.etEditLastName);
        btnEditProfilePic = (FloatingActionButton) findViewById(R.id.btnEditProfilePic);
        etEditDob = (EditText) findViewById(R.id.etEditDob);

        rbEditMale = (RadioButton) findViewById(R.id.rbEditMale);
        rbEditFeMale = (RadioButton) findViewById(R.id.rbEditFeMale);
        try {

            if (session.emailIsActive.equals("Active")) {
                etEditEmail.setFocusable(false);
                etEditEmail.setEnabled(false);
            } else {
                etEditEmail.setFocusable(true);
                etEditEmail.setEnabled(true);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        etEditMobile.setFocusable(false);
        etEditMobile.setEnabled(false);
//        etEditDob.setFocusable(false);

        btnEditSubmit = (Button) findViewById(R.id.btnEditSubmit);
        ilEditFName = (TextInputLayout) findViewById(R.id.ilEditFName);
        ilEditLName = (TextInputLayout) findViewById(R.id.ilEditLName);
        ilEditMobile = (TextInputLayout) findViewById(R.id.ilEditMobile);
        ilEditEmail = (TextInputLayout) findViewById(R.id.ilEditEmail);
        ilEditAddress = (TextInputLayout) findViewById(R.id.ilEditAddress);
        ivEditProfilePic = (ImageView) findViewById(R.id.ivEditProfilePic);
        ilEditDob = (TextInputLayout) findViewById(R.id.ilEditDob);

        fillViews();


        btnEditSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

//        btnEditProfilePic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(),"Sorry, service not available", Toast.LENGTH_SHORT).show();
//            }
//        });

        btnEditProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
//                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
//                if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
//                    checkPermission();
//                } else {
////                    fetchImageClick();
//                }
                selectImage();
            }
        });

    }

//    private void fetchImageClick() {
//        CharSequence[] types = {"Gallery", "Camera"};
//        dialogPicture = new AlertDialog.Builder(EditProfileActivity.this, R.style.AppCompatAlertDialogStyle);
//        dialogPicture.setTitle("Select to change picture");
//        dialogPicture.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//        dialogPicture.setItems(types, new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int position) {
//                switch (position) {
//                    case 0:
//                        takePicfromgallery();
//                        dialog.dismiss();
//                        return;
//                    case 1:
//                        takepicfromcamera();
//                        dialog.dismiss();
//                        return;
//                }
//            }
//
//            private void takePicfromgallery() {
//                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                intent.setType("image/*");
//                startActivityForResult(intent, GalleryResult);
//            }
//
//            private void takepicfromcamera() {
//                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                fileUri = getOutputMediaFileUri();
//                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//                startActivityForResult(intent, CameraResult);
//            }
//
//            public Uri getOutputMediaFileUri() {
//                return Uri.fromFile(getOutputMediaFile());
//            }
//
//            private File getOutputMediaFile() {
//                File mediaStorageDir = new File(
//                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
//
//                if (!mediaStorageDir.exists()) {
//                    mediaStorageDir.mkdirs();
//
//                }
//
//                File mediaFile;
//                mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + ".jpg");
//
//                return mediaFile;
//            }
//
//        });
//
//        dialogPicture.show();
//    }

    private void fillViews() {
        etEditFirstName.setText(session.getUserFirstName());
        etEditLastName.setText(session.getUserLastName());
        etEditMobile.setText(session.getUserMobileNo());
        etEditEmail.setText(session.getUserEmail());
        etEditAddress.setText(session.getUserAddress());
        etEditDob.setText(session.getUserDob());

        if (session.getUserGender() != null) {
            if (session.getUserGender().equals("M")) {
                rbEditMale.setChecked(true);
                Log.i("Edit Gender", "M");
            } else {
                rbEditFeMale.setChecked(true);
                Log.i("Edit Gender", "M");
            }
        } else {
            rbEditMale.setChecked(true);
        }


        /*if (session.getUserImage() != null && !session.getUserImage().isEmpty()) {
            Picasso.with(getApplicationContext()).load(ApiURLNew.URL_IMAGE_MAIN + session.getUserImage()).placeholder(R.drawable.ic_user_load).into(ivEditProfilePic);
        } else {
            Picasso.with(getApplicationContext()).load(R.drawable.ic_user_load).into(ivEditProfilePic);
        }*/

        AQuery aq = new AQuery(EditProfileActivity.this);

        if (session.getUserImage() != null) {
            aq.id(ivEditProfilePic).image(decodeFromBase64ToBitmap(session.getUserImage())).getContext();
        } else {
            Picasso.with(getApplicationContext()).load(R.drawable.ic_user_load).into(ivEditProfilePic);
        }

    }

    private Bitmap decodeFromBase64ToBitmap(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }


    private void submitForm() {
        if (!validateFName()) {
            return;
        }
        if (!validateLName()) {
            return;
        }
        if (!validateAddress()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }


        firstName = etEditFirstName.getText().toString();
        lastName = etEditLastName.getText().toString();
        address = etEditAddress.getText().toString();
        dob = etEditDob.getText().toString();
        email = etEditEmail.getText().toString();

        if (rbEditMale.isChecked()) {
            gender = "M";
        } else {
            gender = "F";
        }

        /*if (isImage && userImage != null && !userImage.isEmpty()) {
            loadDlg.show();

            compressTask = new CompressImageTask(userImage);
            compressTask.execute();
            uploadStringImage();
        } else {
            loadDlg.show();
            editProfileNew();
        }*/

        editProfileNew();

    }

    private boolean validateFName() {
        if (etEditFirstName.getText().toString().trim().isEmpty()) {
            ilEditFName.setError("Enter first name");
            requestFocus(ilEditFName);
            return false;
        } else {
            ilEditFName.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateLName() {
        if (etEditLastName.getText().toString().trim().isEmpty()) {
            ilEditLName.setError("Enter last name");
            requestFocus(etEditLastName);
            return false;
        } else {
            ilEditLName.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateAddress() {
        if (etEditAddress.getText().toString().trim().isEmpty()) {
            ilEditAddress.setError("Enter address");
            requestFocus(etEditAddress);
            return false;
        } else {
            ilEditAddress.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateDob() {
        if (etEditDob.getText().toString().trim().isEmpty()) {
            ilEditDob.setError("Enter Date of birth");
            requestFocus(etEditDob);
            return false;
        } else {
            ilEditDob.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {

        if (etEditEmail.getText().toString().trim().isEmpty() || !isValidEmail(etEditEmail.getText().toString().trim())) {
            ilEditEmail.setError("Enter valid email");
            requestFocus(ilEditEmail);
            return false;
        } else {
            ilEditEmail.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private void editProfileNew() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("firstName", firstName);
//            jsonRequest.put("lastName", lastName);
//            jsonRequest.put("gender", session.getUserGender());
            jsonRequest.put("gender", gender);
            jsonRequest.put("address", address);
            jsonRequest.put("state", "");
            jsonRequest.put("city", "");

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("Edit Request", jsonRequest.toString());
            Log.i("Edit URL", ApiURLNew.URL_EDIT_USER);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_EDIT_USER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("Edit Response", jsonObj.toString());
                        String code = jsonObj.getString("code");
                        String message = jsonObj.getString("message");

                        if (code != null & code.equals("S00")) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            session.setUserFirstName(etEditFirstName.getText().toString());
                            session.setUserLastName(etEditLastName.getText().toString());
                            loadDlg.dismiss();
                            sendRefresh();
                            finish();
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", SecurityUtil.getSecurityKey(jsonRequest.toString()));
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            rq.add(postReq);
        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (resultCode == AppCompatActivity.RESULT_OK) {
//            if (requestCode == CameraResult) {
//                if (isDeviceSupportCamera() == true) {
//                    userImage = fileUri.getPath();
//                    BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
//                    btmapOptions.inSampleSize = 2;
//                    btpUserImage = BitmapFactory.decodeFile(userImage, btmapOptions);
//                    ivEditProfilePic.setImageBitmap(btpUserImage);
//                    Log.i("Image path::Camera", userImage);
//                    isImage = true;
//
//                    String[] imageArray = userImage.split("/");
//                    int i = imageArray.length;
//                    if (i != 0)
//                        imageName = imageArray[i - 1];
//                    Log.i("Camera Image Name", imageName);
//                } else {
//                    Toast.makeText(getApplicationContext(),
//                            "No camera was found", Toast.LENGTH_SHORT).show();
//                }
//
//            } else if (requestCode == GalleryResult) {
//                Uri selectedImageUri = data.getData();
//                String tempPath = getPath(selectedImageUri);
//                BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
//                btmapOptions.inSampleSize = 2;
//                btpUserImage = BitmapFactory.decodeFile(tempPath, btmapOptions);
//                ivEditProfilePic.setImageBitmap(btpUserImage);
//                userImage = tempPath;
//                isImage = true;
//                Log.i("Gallery path::Camera", userImage);
//                String[] imageArray = userImage.split("/");
//                int i = imageArray.length;
//                if (i != 0)
//                    imageName = imageArray[i - 1];
//                Log.i("Gallery Image Name", imageName);
//            }
//        }
//    }


    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor.getString(column_index);
    }

    private class CompressImageTask extends AsyncTask<Void, Void, String> {

        private String pathImage;

        public CompressImageTask(String pathImage) {
            this.pathImage = pathImage;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... arg0) {
            String filePath = compressImage(pathImage);
            return filePath;

        }

        public String compressImage(String imageUri) {

            String filePath = getRealPathFromURI(imageUri);
            Bitmap scaledBitmap = null;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;
            float maxHeight = 816.0f;
            float maxWidth = 612.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }

            options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            /*
             * Deprecated
			 */
            // options.inPurgeable = true;
            // options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
                bmp = BitmapFactory.decodeFile(filePath, options);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();

            }
            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                        Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                    middleY - bmp.getHeight() / 2, new Paint(
                            Paint.FILTER_BITMAP_FLAG));

            ExifInterface exif;
            try {
                exif = new ExifInterface(filePath);

                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION, 0);
                Log.d("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                    Log.d("EXIF", "Exif: " + orientation);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                        scaledBitmap.getWidth(), scaledBitmap.getHeight(),
                        matrix, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileOutputStream out = null;
            String filename = getFilename();
            try {
                out = new FileOutputStream(filename);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            return filename;

        }

        private String getRealPathFromURI(String contentURI) {
            Uri contentUri = Uri.parse(contentURI);
            Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
            if (cursor == null) {
                return contentUri.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor
                        .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                return cursor.getString(idx);
            }
        }

        public String getFilename() {
            File file = new File(Environment.getExternalStorageDirectory().getPath(), "PayQwik/Images");
            if (!file.exists()) {
                file.mkdirs();
            }
            String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");

            return uriSting;

        }

//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            //Compression complete new path received as result in  String
//            uploadImage(result);
//            compressTask = null;
//
//        }
//
//        @Override
//        protected void onCancelled() {
//            super.onCancelled();
//            compressTask = null;
//
//        }

    }


    public void uploadStringImage() {
        loadDlg.show();
        String userImageFinal = getStringImage(btpUserImage);

        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("s", session.getUserSessionId());
            jsonRequest.put("f", imageName);
            jsonRequest.put("p", userImageFinal);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_UPLOAD_IMAGE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("PIC EDIT Response", jsonObj.toString());
                        String code = jsonObj.getString("code");
                        String message = jsonObj.getString("message");

                        if (code != null & code.equals("S00")) {
                            sendRefresh();
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        } else {
                            loadDlg.dismiss();
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            rq.add(postReq);
        }


    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


    public void uploadImage(String imagePath) {
        File file = new File(imagePath);

        uploadHeaderParams = new HashMap<String, String>();
        uploadHeaderParams.put("sessionId", session.getUserSessionId());
        Log.i("Upload", uploadHeaderParams.toString());
        MultipartRequest mr = new MultipartRequest(ApiURLNew.URL_UPLOAD_IMAGE, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                loadDlg.dismiss();
//                Toast.makeText(getApplicationContext(), NetworkErrorHandler.getMessage(volleyError, EditProfileActivity.this), Toast.LENGTH_SHORT).show();

            }
        },
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("Upload Complete", response);
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            String code = jsonObj.getString("code");
                            String message = jsonObj.getString("message");
                            if (code != null & code.equals("S00")) {
                                updatedImageUrl = jsonObj.getString("details");
                                sendRefresh();
                                CustomToast.showMessage(EditProfileActivity.this, message);
//                                editProfileNew();
                            } else {
                                loadDlg.dismiss();
                                Toast.makeText(getApplicationContext(), jsonObj.getString("details"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            loadDlg.dismiss();
                            Toast.makeText(getApplicationContext(), "Exception caused", Toast.LENGTH_SHORT).show();

                        }

                    }
                }, file, uploadHeaderParams, "profilePicture");
        rq.add(mr);

    }

    private void sendRefresh() {
        loadDlg.dismiss();
        Log.i("EditProfile", "Editprofile sendrefresh");
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(EditProfileActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public String compressImage(String filePath) {


        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;


        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;


        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }


        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");

    }


    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        builder.setTitle("Upload Profile Image!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(EditProfileActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask = "Choose from Gallery";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);//
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        assert thumbnail != null;
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        Uri tempUri = getImageUri(EditProfileActivity.this, thumbnail);
        String filePath = getRealPathFromURI(tempUri);
        uploadImage(compressImage(filePath));

        ivEditProfilePic.setImageBitmap(thumbnail);

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                Uri selectedImageUri = data.getData();
                InputStream imageStream = getContentResolver().openInputStream(selectedImageUri);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                ivEditProfilePic.setImageBitmap(getResizedBitmap(selectedImage, 100));
                uploadImage(compressImage(getPath(EditProfileActivity.this, selectedImageUri)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };

    @Override
    public void onBackPressed() {
        android.support.v7.app.AlertDialog.Builder exitDialog =
                new android.support.v7.app.AlertDialog.Builder(EditProfileActivity.this, R.style.AppCompatAlertDialogStyle);
        exitDialog.setTitle(getResources().getString(R.string.exit1));
        exitDialog.setPositiveButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        exitDialog.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                finish();
            }
        });
        exitDialog.show();
    }
}
