package com.Blockgration.Userwallet.activity.shopping;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.Blockgration.Userwallet.R;


import java.util.List;


/**
 * Created by kashifimam on 21/02/17.
 */

public class ShopMyOrderAdapter extends RecyclerView.Adapter<ShopMyOrderAdapter.RecyclerViewHolders> {

    private List<ShoppingMyOrderModel> itemList;
    private Context context;


    public ShopMyOrderAdapter(Context context, List<ShoppingMyOrderModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_order, null);
        return new RecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, int position) {
//        if (itemList.get(position) instanceof ShoppingMyOrderModel) {
        ShoppingMyOrderModel shoppingMyOrderModel =  itemList.get(position);
        holder.tvTotalPrice.setText("Total Price: "+"\u20B9 "+" " + shoppingMyOrderModel.getTotAmount()+"/-");
        holder.tvOrderDate.setText(shoppingMyOrderModel.getDate());
        holder.tvTranxId.setText("Transaction ref: " + shoppingMyOrderModel.getTranxNo());
        holder.tvOrderId.setText("Order ID: "+shoppingMyOrderModel.getOrderId());
        holder.tvCgst.setText("CGST : "+shoppingMyOrderModel.getpCgst()+"/-");
        holder.tvSgst.setText("SGST : "+shoppingMyOrderModel.getpSgst()+"/-");
        holder.tvTotalPrice1.setText("Total Price: "+shoppingMyOrderModel.getpPrice()+"/-");
        holder.tvDeliveryPrice.setText("Delivery Charges: 0/-");
//        }
//        if (itemList.get(position)instanceof ShoppingMyOrderModel.Product) {
//            ShoppingMyOrderModel.Product product = (ShoppingMyOrderModel.Product)itemList.get(position);
        holder.tvProdName.setText(shoppingMyOrderModel.getpName());
        holder.tvProdAmount.setVisibility(View.GONE);
        holder.tvProdAmount.setText(shoppingMyOrderModel.getpPrice());

        holder.btnStd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.llNoTax.setVisibility(View.GONE);
                holder.llTax.setVisibility(View.VISIBLE);
            }
        });

        holder.btnHtd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.llTax.setVisibility(View.GONE);
                holder.llNoTax.setVisibility(View.VISIBLE);
            }
        });
        AQuery aq = new AQuery(context);
        if (shoppingMyOrderModel.getpImage() != null && !shoppingMyOrderModel.getpImage().isEmpty()) {
            aq.id(holder.ivProductImage).image(shoppingMyOrderModel.getpImage(), true, true);
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTranxId;
        TextView tvOrderDate, tvDeliveryPrice, tvCgst, tvSgst, tvTotalPrice1;
        TextView tvTotalPrice, tvProdName, tvProdAmount, tvOrderId;
        ImageView ivProductImage;
        LinearLayout llProducts, llTax, llNoTax;
        Button btnStd, btnHtd;


        public RecyclerViewHolders(View itemView) {
            super(itemView);
            llNoTax = (LinearLayout) itemView.findViewById(R.id.llNoTax);
            llTax = (LinearLayout) itemView.findViewById(R.id.llTax);
            tvOrderDate = (TextView) itemView.findViewById(R.id.tvOrderDate);
            tvTotalPrice = (TextView) itemView.findViewById(R.id.tvTotalPrice);
            tvTranxId = (TextView) itemView.findViewById(R.id.tvTranxId);
            llProducts = (LinearLayout) itemView.findViewById(R.id.llProducts);
            tvProdAmount = (TextView) itemView.findViewById(R.id.tvProdAmount);
            tvProdName = (TextView) itemView.findViewById(R.id.tvProdName);
            ivProductImage = (ImageView) itemView.findViewById(R.id.ivProductImage);
            tvOrderId = (TextView) itemView.findViewById(R.id.tvOrderId);
            tvDeliveryPrice = (TextView) itemView.findViewById(R.id.tvDeliveryPrice);
            tvCgst = (TextView) itemView.findViewById(R.id.tvCgst);
            tvSgst = (TextView) itemView.findViewById(R.id.tvSgst);
            tvTotalPrice1 = (TextView) itemView.findViewById(R.id.tvTotalPrice1);
            btnStd = (Button) itemView.findViewById(R.id.btnStd);
            btnHtd = (Button) itemView.findViewById(R.id.btnHtd);
        }

        @Override
        public void onClick(final View view) {
            int i = getAdapterPosition();
        }
    }

}

