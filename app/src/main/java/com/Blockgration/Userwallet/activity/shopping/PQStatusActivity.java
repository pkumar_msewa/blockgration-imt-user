package com.Blockgration.Userwallet.activity.shopping;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.MainActivity;


public class PQStatusActivity extends AppCompatActivity {

    public static final String EXTRA_STATUS = "mmsn";
    private TextView tvTransactionStatus;
    private TextView tvCongratulationTransactionStatus;

    private PQCart cart = PQCart.getInstance();
    private Toolbar toolbar;
    private Button btnTransactionOK;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_status);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvTransactionStatus = (TextView) findViewById(R.id.tvTransactionStatus);
        tvCongratulationTransactionStatus = (TextView) findViewById(R.id.tvCongratulationTransactionStatus);

//        final String transactionStatus = getIntent().getStringExtra("transStatus");
//        final String html = getIntent().getStringExtra("html");
        btnTransactionOK = (Button) findViewById(R.id.btnTransactionOK);
        btnTransactionOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PQStatusActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });


    }



}