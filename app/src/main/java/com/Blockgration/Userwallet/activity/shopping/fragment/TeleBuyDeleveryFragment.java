package com.Blockgration.Userwallet.activity.shopping.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.shopping.AddressAdapter;
import com.Blockgration.Userwallet.activity.shopping.InCartListner;
import com.Blockgration.Userwallet.activity.shopping.LoadingDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.model.ShoppingAddressModel;
import com.Blockgration.model.UserModel;

import com.Blockgration.util.CheckLog;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.PayingDetailsValidation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Ksf on 4/6/2016.
 */
public class TeleBuyDeleveryFragment extends Fragment implements InCartListner {

    private UserModel session = UserModel.getInstance();
    private List<ShoppingAddressModel> shoppingAddressArray;
    private LinearLayout lladdresslist;
    private LinearLayout llAddAddress;
    private LoadingDialog loadDlg;
    private View rootView;
    private Button btndeliveryProduct;
    private EditText etdeliveryFName, etdeliveryLName, etdeliveryAddressOne, etdeliveryAddressTwo, etdeliveryAddressThree, etdeliveryPinCode, etdeliveryMobile, etCity, etCountry;
    private Boolean cancel;
    private View focusView = null;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private JsonObjectRequest postReq;
    private String tag_json_obj = "json_events";
    private AddressAdapter addressAdapter;
    private String addressID = "";


    private String url;
    private Button AddressNext;
    private TextView tvBuyTotalRupess, tvBuyTotalTax;
    private ImageView ivBuyNext;

    private RequestQueue rq;
    private String orderId, mobile;
    private ListView lvAddList;
    private Button addAddress, btn_deleivery_cancel;
    private String toggleEdit = "add";
    private boolean value = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            rq = Volley.newRequestQueue(getActivity());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_incart_delivery, container, false);
        loadDlg = new LoadingDialog(getActivity());
        btndeliveryProduct = (Button) rootView.findViewById(R.id.btn_deleivery_ok);
        etdeliveryAddressOne = (EditText) rootView.findViewById(R.id.edt_delivery_address1);
        etdeliveryAddressTwo = (EditText) rootView.findViewById(R.id.etLandmark);
//        etdeliveryMobile = (EditText) rootView.findViewById(R.id.edt_delivery_mobile_number);
        etdeliveryPinCode = (EditText) rootView.findViewById(R.id.edt_deleivery_pin_code);
        etdeliveryFName = (EditText) rootView.findViewById(R.id.etFirstName);
        etdeliveryLName = (EditText) rootView.findViewById(R.id.etLastName);
        AddressNext = (Button) rootView.findViewById(R.id.AddressNext);
        etCountry = (EditText) rootView.findViewById(R.id.etCountry);
        etCity = (EditText) rootView.findViewById(R.id.etCity);
        addAddress = (Button) rootView.findViewById(R.id.addAddress);
//        etdeliveryMobile.setText(session.getUserMobileNo());
        llAddAddress = (LinearLayout) rootView.findViewById(R.id.llAddAddress);
        lladdresslist = (LinearLayout) rootView.findViewById(R.id.lladdresslist);
        shoppingAddressArray = new ArrayList<>();
        etdeliveryFName.setText(session.getUserFirstName());
        if (session.getUserLastName() != null && !session.getUserLastName().equals(" ")) {
            etdeliveryLName.setText(session.getUserLastName());
            Log.i("LNAME", session.getUserLastName());
        }
        btn_deleivery_cancel = (Button) rootView.findViewById(R.id.btn_deleivery_cancel);
//        tvBuyTotalRupess = (TextView) rootView.findViewById(R.id.tvBuyTotalRupess);
//        tvBuyTotalTax = (TextView) rootView.findViewById(R.id.tvBuyTotalTax);
//        ivBuyNext = (ImageView) rootView.findViewById()
        lvAddList = (ListView) rootView.findViewById(R.id.lvAddList);
        lvAddList.setFastScrollEnabled(true);
        checkOutProcess();
        btndeliveryProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPayment();
            }
        });
//        if (session.getUserMobileNo().length() == 11) {
//            mobile = session.getUserMobileNo().substring(1);
//        }
//        if (session.getUserMobileNo().length() == 12) {
//            mobile = session.getUserMobileNo().substring(2);
//        }
//        if (session.getUserMobileNo().length() == 13) {
//            mobile = session.getUserMobileNo().substring(3);
//        }
//        if (session.getUserMobileNo().length() == 14) {
//            mobile = session.getUserMobileNo().substring(4);
//        }
//        if (session.getUserMobileNo().length() == 15) {
//            mobile = session.getUserMobileNo().substring(5);
//        }
//        if (session.getUserMobileNo().length() == 10) {
//            mobile = session.getUserMobileNo();
//        }

        mobile = session.getUserMobileNo();
        AddressNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderId != null && orderId.length() != 0) {
                    gofordeliveryProcess(orderId);
                } else {
                    Toast.makeText(getActivity(), "Please select Address", Toast.LENGTH_SHORT).show();
                }
            }
        });
        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llAddAddress.setVisibility(View.VISIBLE);
                lladdresslist.setVisibility(view.GONE);
            }
        });

        btn_deleivery_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddAddress.setVisibility(View.GONE);
                lladdresslist.setVisibility(View.VISIBLE);
                btn_deleivery_cancel.setVisibility(View.GONE);
            }
        });

        return rootView;
    }

    private void attemptPayment() {

        etdeliveryFName.setError(null);
        etdeliveryLName.setError(null);
        etdeliveryPinCode.setError(null);
        etCountry.setError(null);
        etCity.setError(null);
        etdeliveryAddressOne.setError(null);
        etdeliveryAddressTwo.setError(null);
//        etdeliveryMobile.setError(null);

        cancel = false;


        checkFName(etdeliveryFName.getText().toString());
        checkLName(etdeliveryLName.getText().toString());
        checkPin(etdeliveryPinCode.getText().toString());
        checkCountry(etCountry.getText().toString());
        checkCity(etCity.getText().toString());
        checkAddress1(etdeliveryAddressOne.getText().toString());
        checkAddress2(etdeliveryAddressTwo.getText().toString());
//        checkMobile(etdeliveryMobile.getText().toString());


        if (cancel) {
            focusView.requestFocus();
        } else {
//            gotoPay();
            if (toggleEdit.equals("add")) {
                addDeliveryAddress();
            } else {
                editDeliveryAddress();
            }
        }
    }

    private void checkPin(String pin) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkPinCode(pin);
        if (!gasCheckLog.isValid) {
            etdeliveryPinCode.setError(getString(gasCheckLog.msg));
            focusView = etdeliveryPinCode;
            cancel = true;
        }
    }

    private void checkAddress1(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etdeliveryAddressOne.setError(getString(gasCheckLog.msg));
            focusView = etdeliveryAddressOne;
            cancel = true;
        }
    }

    private void checkFName(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etdeliveryFName.setError(getString(gasCheckLog.msg));
            focusView = etdeliveryLName;
            cancel = true;
        }
    }

    private void checkLName(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etdeliveryLName.setError(getString(gasCheckLog.msg));
            focusView = etdeliveryLName;
            cancel = true;
        }
    }

    private void checkAddress2(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etdeliveryAddressTwo.setError(getString(gasCheckLog.msg));
            focusView = etdeliveryAddressTwo;
            cancel = true;
        }
    }

    private void checkCity(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etCity.setError(getString(gasCheckLog.msg));
            focusView = etCity;
            cancel = true;
        }
    }

    private void checkCountry(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etCountry.setError(getString(gasCheckLog.msg));
            focusView = etCountry;
            cancel = true;
        }
    }

    public void addDeliveryAddress() {
        loadDlg.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("deliveryMobileNo", session.getUserMobileNo());
            jsonRequest.put("firstName", etdeliveryFName.getText().toString());
            jsonRequest.put("lastName", etdeliveryLName.getText().toString());
            jsonRequest.put("address", etdeliveryAddressOne.getText().toString());
            jsonRequest.put("landMark", etdeliveryAddressTwo.getText().toString());
            jsonRequest.put("city", etCity.getText().toString());
            jsonRequest.put("mobileNo", session.getUserMobileNo());
            jsonRequest.put("zipCode", etdeliveryPinCode.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("ADDDELIVERYURL", ApiURLNew.URL_SHOPPONG_ADD_DELIVERY_ADDRESS);
            Log.i("ADDDELIVERYREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SHOPPONG_ADD_DELIVERY_ADDRESS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("ADDDELIVERYRES", jsonObj.toString());
                        String code = jsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            JSONObject response = jsonObj.getJSONObject("response");
                            String id = response.getString("Id");
                            loadDlg.dismiss();
//                            TeleBuyPaymentFragment deliveryFragment = new TeleBuyPaymentFragment();
//                            Bundle args = new Bundle();
//                            deliveryFragment.setArguments(args);
//                            FragmentManager fragmentManager = getFragmentManager();
//                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                            fragmentTransaction.addToBackStack(getTag());
//                            fragmentTransaction.replace(R.id.frameInCart, deliveryFragment);
//                            fragmentTransaction.commit();
                            gofordeliveryProcess(id);


                        } else {
                            CustomToast.showMessage(getActivity(), jsonObj.getString("message"));
                            loadDlg.dismiss();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 120000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void editDeliveryAddress() {
        loadDlg.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("deliveryMobileNo", mobile);
            jsonRequest.put("firstName", etdeliveryFName.getText().toString());
            jsonRequest.put("lastName", etdeliveryLName.getText().toString());
            jsonRequest.put("address", etdeliveryAddressOne.getText().toString());
            jsonRequest.put("landMark", etdeliveryAddressTwo.getText().toString());
            jsonRequest.put("city", etCity.getText().toString());
            jsonRequest.put("mobileNo", session.getUserMobileNo());
            jsonRequest.put("zipCode", etdeliveryPinCode.getText().toString());
            jsonRequest.put("id", addressID);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("EDITDELIVERYURL", ApiURLNew.URL_SHOPPONG_EDIT_DELIVERY_ADDRESS);
            Log.i("EDITDELIVERYREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SHOPPONG_EDIT_DELIVERY_ADDRESS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("EDITDELIVERYRES", jsonObj.toString());
                        String code = jsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            llAddAddress.setVisibility(View.GONE);
                            lvAddList.setVisibility(View.VISIBLE);
                            checkOutProcess();
                        } else {
                            CustomToast.showMessage(getActivity(), jsonObj.getString("message"));
                            loadDlg.dismiss();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 120000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void deleteDeliveryAddress(String id, final int pos) {
        loadDlg.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobileNo", mobile);
            jsonRequest.put("id", id);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("DELDELIVERYURL", ApiURLNew.URL_SHOPPONG_DELETE_DELIVERY_ADDRESS);
            Log.i("DELDELIVERYREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SHOPPONG_DELETE_DELIVERY_ADDRESS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("DELIVERYRES", jsonObj.toString());
                        String code = jsonObj.getString("code");
                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            shoppingAddressArray.remove(pos);
                            addressAdapter.notifyDataSetChanged();
                        } else {
                            CustomToast.showMessage(getActivity(), jsonObj.getString("message"));
                            loadDlg.dismiss();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 120000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public void checkOutProcess() {
        loadDlg.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobileNo", session.getUserMobileNo());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("CHECKOUTURL", ApiURLNew.URL_SHOPPONG_CHECKOUT_PROCESS);
            Log.i("CHECKOUTREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SHOPPONG_CHECKOUT_PROCESS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        shoppingAddressArray.clear();
                        Log.i("CHECKOUTRES", jsonObj.toString());
                        String code = jsonObj.getString("code");
                        loadDlg.dismiss();
                        if (code != null && code.equals("S00")) {
                            JSONArray addArray = jsonObj.getJSONArray("response");
                            for (int i = 0; i < addArray.length(); i++) {
                                JSONObject c = addArray.getJSONObject(i);
                                long orderid = c.getLong("id");
                                String firstname = c.getString("firstName");
                                String lastname = c.getString("lastName");
                                String streetaddress = c.getString("address");
                                String email = session.getUserEmail();
                                String landmark = c.getString("landMark");
                                String city = c.getString("city");
                                String country = "India";
                                String mobilenumber = c.getString("deliveryMobileNo");
                                long zipcode = c.getLong("zipCode");
                                long userid = c.getLong("id");

                                ShoppingAddressModel shoppingAddressModel = new ShoppingAddressModel(orderid, firstname, lastname, streetaddress, email, landmark, city, country, mobilenumber, zipcode, userid);
                                shoppingAddressArray.add(shoppingAddressModel);
                            }
                            if (shoppingAddressArray != null && shoppingAddressArray.size() != 0) {
                                lladdresslist.setVisibility(View.VISIBLE);
                                llAddAddress.setVisibility(View.GONE);
                                addressAdapter = new AddressAdapter(getActivity(), shoppingAddressArray, TeleBuyDeleveryFragment.this);
                                lvAddList.setAdapter(addressAdapter);
                            } else {
                                lladdresslist.setVisibility(View.GONE);
                                llAddAddress.setVisibility(View.VISIBLE);
                            }
                            loadDlg.dismiss();

                        } else {
                            CustomToast.showMessage(getActivity(), jsonObj.getString("message"));
                            loadDlg.dismiss();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 120000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void gofordeliveryProcess(String o) {
        loadDlg.show();
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobileNo", session.getUserMobileNo());
            jsonRequest.put("id", o);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("DELIVERYURL", ApiURLNew.URL_SHOPPONG_GO_FOR_DELIVERY);
            Log.i("DELIVERYREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SHOPPONG_GO_FOR_DELIVERY, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("DELIVERYRES", jsonObj.toString());
                        String code = jsonObj.getString("code");
                        loadDlg.dismiss();
                        if (code != null && code.equals("S00")) {
                            JSONObject response = jsonObj.getJSONObject("response");
                            String QwikrTxnRefNo = response.getString("QwikrTxnRefNo");
                            Log.i("TRXNO", QwikrTxnRefNo);
//                            JSONObject resposne = jsonObj.getJSONObject("response");
//                            JSONArray carDetails = resposne.getJSONArray("Cart Details");
//                            for (int i = 0; i < carDetails.length(); i++) {
//                                JSONObject c = carDetails.getJSONObject(i);
//                                long orderid = c.getLong("orderid");
//                                String firstname = c.getString("firstname");
//                                String lastname = c.getString("lastname");
//                                String streetaddress = c.getString("streetaddress");
//                                String email = c.getString("email");
//                                String landmark = c.getString("landmark");
//                                String city = c.getString("city");
//                                String country = c.getString("country");
//                                String mobilenumber = c.getString("mobilenumber");
//                                long zipcode = c.getLong("zipcode");
//                                long userid = c.getLong("userid");
//
//                                ShoppingAddressModel shoppingAddressModel = new ShoppingAddressModel(orderid, firstname, lastname, streetaddress, email, landmark, city, country, mobilenumber, zipcode, userid);
//                                shoppingAddressArray.add(shoppingAddressModel);
//                            }
//
//                            if (shoppingAddressArray != null && shoppingAddressArray.size() != 0) {
                            TeleBuyPaymentFragment deliveryFragment = new TeleBuyPaymentFragment();
                            Bundle args = new Bundle();
                            args.putString("TRXREF", QwikrTxnRefNo);
                            deliveryFragment.setArguments(args);
                            FragmentManager fragmentManager = getFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.addToBackStack(getTag());
                            fragmentTransaction.replace(R.id.frameInCart, deliveryFragment);
                            fragmentTransaction.commit();
//                            }

                        } else {
                            CustomToast.showMessage(getActivity(), jsonObj.getString("message"));
                            loadDlg.dismiss();
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 120000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    @Override
    public void taskCompleted() {

    }

    @Override
    public void selectAddress(String s) {
        orderId = s;
        if (orderId.length() != 0) {
            gofordeliveryProcess(orderId);
        } else {
            Toast.makeText(getActivity(), "Please select Address", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void closeCart() {

    }

    @Override
    public void deleteAddress(String addId, int pos) {
        deleteDeliveryAddress(addId, pos);


    }


    @Override
    public void editAddress(String addId, int pos) {
        llAddAddress.setVisibility(View.VISIBLE);
        lladdresslist.setVisibility(View.GONE);
        btn_deleivery_cancel.setVisibility(View.VISIBLE);
        toggleEdit = "edit";
        value = true;
        etdeliveryFName.setText(shoppingAddressArray.get(pos).getFirstname());
        etdeliveryLName.setText(shoppingAddressArray.get(pos).getLastname());
        etdeliveryPinCode.setText(String.valueOf(shoppingAddressArray.get(pos).getZipcode()));
        etCountry.setText(shoppingAddressArray.get(pos).getCountry());
        etCity.setText(shoppingAddressArray.get(pos).getCity());
        etdeliveryAddressOne.setText(shoppingAddressArray.get(pos).getStreetaddress());
        etdeliveryAddressTwo.setText(shoppingAddressArray.get(pos).getLandmark());
        addressID = String.valueOf(shoppingAddressArray.get(pos).getUserid());
    }


}
