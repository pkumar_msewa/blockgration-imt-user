package com.Blockgration.Userwallet.activity.transport;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;

import com.Blockgration.adapter.BenficryListAdapter;
import com.Blockgration.adapter.CountryListAdapter;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomToast;

import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.MenuMetadata;
import com.Blockgration.model.BenfercyModel;
import com.Blockgration.model.CountryModel;
import com.Blockgration.model.UserModel;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddUpdateBeneficiary extends AppCompatActivity {

    public static final String EXTRAJSON = "json";
    ArrayList<CountryModel> countryModels, stateModel, cityModel;
    private RadioButton addbeneficiary, rbAddBeneficiary;
    private RadioGroup radioGroup;
    private UserModel userModel = UserModel.getInstance();
    private ScrollView svBeneficiaryDetails;
    private MaterialEditText metselectBeneficiary, metFirstName, metMiddleName, metLastName, metMaidenName, metGender;
    private MaterialEditText metMobile, metEmail, metCountry, metState, metCity, metStreet, metPincode;
    private JSONObject totalvalue;
    private ArrayList<BenfercyModel> benficyList;
    private String CityID;
    private String iD;
    private String transferNo;
    private Toolbar toolbar;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String va = intent.getStringExtra("updates");
            if (va != null && !va.equals("")) {
                try {
                    CustomToast.showMessage(AddUpdateBeneficiary.this, va);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            } else if (intent.getStringExtra("json") != null && !intent.getStringExtra("json").equals("")) {
                transferNo = intent.getStringExtra("json");
                getResponse();
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_update_beneficiary);
        overridePendingTransition(R.anim.trans_left_out, R.anim.trans_left_in);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.trans_right_out, R.anim.trans_right_in);
            }
        });

        LocalBroadcastManager.getInstance(AddUpdateBeneficiary.this).registerReceiver(mMessageReceiver, new IntentFilter("setting"));
        LocalBroadcastManager.getInstance(AddUpdateBeneficiary.this).registerReceiver(fMessageReceiver, new IntentFilter("action-done"));

        //Radio group
        addbeneficiary = (RadioButton) findViewById(R.id.beneficiary);
//        addbeneficiary.setChecked(true);
        rbAddBeneficiary = (RadioButton) findViewById(R.id.rbAddBeneficiary);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        //linearlayout and edittext
        metselectBeneficiary = (MaterialEditText) findViewById(R.id.metselectBeneficiary);
        svBeneficiaryDetails = (ScrollView) findViewById(R.id.svBeneficiaryDetails);
        metFirstName = (MaterialEditText) findViewById(R.id.metFirstName);
        metMiddleName = (MaterialEditText) findViewById(R.id.metMiddleName);
        metLastName = (MaterialEditText) findViewById(R.id.metLastName);
        metMaidenName = (MaterialEditText) findViewById(R.id.metMaidenName);
        metGender = (MaterialEditText) findViewById(R.id.metGender);
        metMobile = (MaterialEditText) findViewById(R.id.metMobile);
        metEmail = (MaterialEditText) findViewById(R.id.metEmail);
        metCountry = (MaterialEditText) findViewById(R.id.metCountry);
        metState = (MaterialEditText) findViewById(R.id.metState);
        metCity = (MaterialEditText) findViewById(R.id.metCity);
        metStreet = (MaterialEditText) findViewById(R.id.metStreet);
        metPincode = (MaterialEditText) findViewById(R.id.metPincode);
        metCountry.setFocusable(false);
        metCountry.setFocusableInTouchMode(false);
        metCity.setFocusable(false);
        metCity.setFocusableInTouchMode(false);
        metGender.setFocusable(false);
        metGender.setFocusableInTouchMode(false);
        metState.setFocusable(false);
        metState.setFocusableInTouchMode(false);
        MenuMetadata.getResponse(AddUpdateBeneficiary.this, ApiURLNew.URL_SEND_MONEY_COUNTRY, new JSONObject());

        try {
            totalvalue = new JSONObject(getIntent().getStringExtra(AddUpdateBeneficiary.EXTRAJSON));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (addbeneficiary.isChecked()) {
            metselectBeneficiary.setVisibility(View.VISIBLE);
            svBeneficiaryDetails.setVisibility(View.GONE);
            rbAddBeneficiary.setChecked(false);
            JSONObject value = new JSONObject();
            try {
                value.put("destination_country_id", totalvalue.getString("destination_country_id"));
                value.put("payment_method", totalvalue.getString("payment_method_id"));
                MenuMetadata.getResponse(AddUpdateBeneficiary.this, ApiURLNew.URL_SEND_MONEY_GETBENCFY, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        countryModels = new ArrayList<>();
        stateModel = new ArrayList<>();
        cityModel = new ArrayList<>();
        addbeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addbeneficiary.isChecked()) {
                    metselectBeneficiary.getText().clear();
                    metselectBeneficiary.setVisibility(View.VISIBLE);
                    svBeneficiaryDetails.setVisibility(View.GONE);
                    rbAddBeneficiary.setChecked(false);
                    JSONObject value = new JSONObject();
                    try {
                        value.put("destination_country_id", totalvalue.getString("destination_country_id"));
                        value.put("payment_method", totalvalue.getString("payment_method_id"));
                        Log.i("benfi", value.toString());
                        MenuMetadata.getResponse(AddUpdateBeneficiary.this, ApiURLNew.URL_SEND_MONEY_GETBENCFY, value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });


        metCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CountryListAdapter addCurrencySpinnerAdapter = new CountryListAdapter(AddUpdateBeneficiary.this, countryModels);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddUpdateBeneficiary.this);
                alertDialog.setTitle("Select Sender Country");
                alertDialog.setAdapter(addCurrencySpinnerAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        metCountry.setText(countryModels.get(i).getCountryName());
                        try {
                            MenuMetadata.getResponse(AddUpdateBeneficiary.this, ApiURLNew.URL_SEND_MONEY_STATE, new JSONObject().put("id", countryModels.get(i).getIsoCode()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();

            }
        });

        rbAddBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rbAddBeneficiary.isChecked()) {
                    metselectBeneficiary.setVisibility(View.GONE);
                    svBeneficiaryDetails.setVisibility(View.VISIBLE);
                    addbeneficiary.setChecked(false);
                    getAddbeneficiary();
                }


            }
        });

        metState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (metCountry.getText().toString().isEmpty()) {
                    CustomToast.showMessage(AddUpdateBeneficiary.this, "Please select Country first");
                    return;
                }
                CountryListAdapter addCurrencySpinnerAdapter = new CountryListAdapter(AddUpdateBeneficiary.this, stateModel);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddUpdateBeneficiary.this);
                alertDialog.setTitle("Select State");
                alertDialog.setAdapter(addCurrencySpinnerAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        metState.setText(stateModel.get(i).getCountryName());
                        try {
                            MenuMetadata.getResponse(AddUpdateBeneficiary.this, ApiURLNew.URL_SEND_MONEY_CITY, new JSONObject().put("id", stateModel.get(i).getIsoCode()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
        metCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (metState.getText().toString().isEmpty()) {
                    CustomToast.showMessage(AddUpdateBeneficiary.this, "Please select State first");
                    return;
                }
                CountryListAdapter addCurrencySpinnerAdapter = new CountryListAdapter(AddUpdateBeneficiary.this, cityModel);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddUpdateBeneficiary.this);
                alertDialog.setTitle("Select City");
                alertDialog.setAdapter(addCurrencySpinnerAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        metCity.setText(cityModel.get(i).getCountryName());
                        CityID = cityModel.get(i).getIsoCode();
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();
            }
        });

        metGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] charSequence = new CharSequence[]{"Male", "Female"};
                AlertDialog.Builder builder = new AlertDialog.Builder(AddUpdateBeneficiary.this);
                builder.setTitle("Select Gender");
                builder.setItems(charSequence, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        metGender.setText(charSequence[i]);

                    }
                });
                builder.show();
            }
        });
        benficyList = new ArrayList<>();
        metselectBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (benficyList.size() != 0) {

                    BenficryListAdapter addCurrencySpinnerAdapter = new BenficryListAdapter(AddUpdateBeneficiary.this, benficyList);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddUpdateBeneficiary.this);
                    alertDialog.setTitle("Select Beneficiary");
                    alertDialog.setAdapter(addCurrencySpinnerAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            metselectBeneficiary.setText(benficyList.get(i).getFirstname());
                            metselectBeneficiary.setVisibility(View.GONE);
                            svBeneficiaryDetails.setVisibility(View.VISIBLE);
                            metFirstName.setText(benficyList.get(i).getFirstname());
                            metLastName.setText(benficyList.get(i).getLastname());
                            metMiddleName.setText(benficyList.get(i).getMiddlename());
                            metMaidenName.setText(benficyList.get(i).getMaidenname());
                            metMobile.setText(benficyList.get(i).getPhone());
//                            metGender.setText(benficyList.get(i).getGender());
                            metGender.setText("Male");
                            metPincode.setText(benficyList.get(i).getZipcode());
                            metEmail.setText(benficyList.get(i).getEmail());
                            metCity.setText(benficyList.get(i).getCity_name());
                            metStreet.setText(benficyList.get(i).getStreet());
                            metCountry.setText(benficyList.get(i).getCountry_name());
                            metState.setText(benficyList.get(i).getState_name());
                            dialogInterface.dismiss();
                        }
                    });
                    alertDialog.show();
                } else {
                    CustomToast.showMessage(AddUpdateBeneficiary.this, "Please Add Beneficiary details.");
                }
            }
        });


        Button btnFundTransfer = (Button) findViewById(R.id.btnFundTransfer);
        btnFundTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitCheck();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private BroadcastReceiver fMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            if (action.equals("1")) {
                finish();
            }

        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_out, R.anim.trans_right_in);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fMessageReceiver);
    }

    public void getAddbeneficiary() {
        metFirstName.setText(userModel.getUserFirstName());
        if (!userModel.getUserLastName().equals("") && userModel.getUserLastName() != null) {
            metLastName.setText("");
        } else {
            metLastName.setText(userModel.getUserLastName());
        }
        metMiddleName.setText("");
        metMaidenName.setText("");
        metMobile.setText(userModel.getUserMobileNo());
        metGender.setText(userModel.getUserGender());
        metPincode.setText("");
        metEmail.setText(userModel.getUserEmail());
        metStreet.setText("");
        metCity.setText("");
        metCountry.setText("");
        metState.setText("");
    }

    private void getResponse() {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(transferNo);
            Object o = jsonObject.get("results");
            if (o instanceof JSONArray) {
                Log.i("test1", String.valueOf(((JSONArray) o).length()));
                benficyList.clear();
                for (int i = 0; i < ((JSONArray) o).length(); i++) {
                    if (((JSONArray) o).getJSONObject(i).has("dialingCode")) {
                        CountryModel countryModel = new CountryModel(((JSONArray) o).getJSONObject(i).getString("name"), ((JSONArray) o).getJSONObject(i).getString("iso3"), ((JSONArray) o).getJSONObject(i).getString("flag_url"), ((JSONArray) o).getJSONObject(i).getString("id"), "", "");
                        countryModels.add(countryModel);
                    } else if (((JSONArray) o).getJSONObject(i).has("shortname")) {
                        CountryModel countryModel = new CountryModel(((JSONArray) o).getJSONObject(i).getString("name"), ((JSONArray) o).getJSONObject(i).getString("country_id"), ((JSONArray) o).getJSONObject(i).getString("shortname"), ((JSONArray) o).getJSONObject(i).getString("id"), ((JSONArray) o).getJSONObject(i).getString("shortname"), "");
                        stateModel.add(countryModel);
                    } else if (((JSONArray) o).getJSONObject(i).has("occupation")) {
                        BenfercyModel benfercyModel = new BenfercyModel(AddUpdateBeneficiary.this);
                        benfercyModel.setFirstname(((JSONArray) o).getJSONObject(i).getString("firstname"));
                        benfercyModel.setMiddlename(((JSONArray) o).getJSONObject(i).getString("middlename"));
                        benfercyModel.setLastname(((JSONArray) o).getJSONObject(i).getString("lastname"));
                        benfercyModel.setMaidenname(((JSONArray) o).getJSONObject(i).getString("maidenname"));
                        benfercyModel.setPhone(((JSONArray) o).getJSONObject(i).getString("mobile"));
                        benfercyModel.setGender(((JSONArray) o).getJSONObject(i).getString("gender"));
                        benfercyModel.setZipcode(((JSONArray) o).getJSONObject(i).getString("zipcode"));
                        benfercyModel.setEmail(((JSONArray) o).getJSONObject(i).getString("email"));
                        benfercyModel.setCity_name(((JSONArray) o).getJSONObject(i).getString("city_name"));
                        benfercyModel.setCountry_name(((JSONArray) o).getJSONObject(i).getString("country_name"));
                        benfercyModel.setState_name(((JSONArray) o).getJSONObject(i).getString("state_name"));
                        benfercyModel.setStreet(((JSONArray) o).getJSONObject(i).getString("street"));
                        benficyList.add(benfercyModel);
                        CityID = ((JSONArray) o).getJSONObject(i).getString("city_id");
                        totalvalue.remove("city_id");
                        totalvalue.put("city_id", CityID);
                        iD = ((JSONArray) o).getJSONObject(i).getString("id");
                        totalvalue.remove("user_id");
                        totalvalue.put("user_id", iD);
                    } else if (((JSONArray) o).getJSONObject(i).has("state_id")) {
                        CountryModel countryModel = new CountryModel(((JSONArray) o).getJSONObject(i).getString("name"), ((JSONArray) o).getJSONObject(i).getString("state_id"), ((JSONArray) o).getJSONObject(i).getString("state_id"), ((JSONArray) o).getJSONObject(i).getString("id"), "", "");
                        cityModel.add(countryModel);
                    }
                }
                Log.i("test2", "" + benficyList.size());
            }
            if (o instanceof JSONObject) {
                if (((JSONObject) o).has("msg")) {
                    if (addbeneficiary.isChecked()) {

                    } else {
                        totalvalue.remove("user_id");
                        totalvalue.put("user_id", ((JSONObject) o).getString("customer_id"));
                    }
                    CustomToast.showMessage(AddUpdateBeneficiary.this, ((JSONObject) o).getString("msg"));
                    Intent i = new Intent(AddUpdateBeneficiary.this, AgentLoactionActivity.class);
                    i.putExtra(AgentLoactionActivity.EXTRA_VALUE, totalvalue.toString());
                    startActivity(i);

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void submitCheck() {
        if (addbeneficiary.isChecked()) {
            if (!validateSelectBeneficiary()) {
                Log.i("submitCheck1", String.valueOf(validateSelectBeneficiary()));
                return;
            }
        }
        if (!validateFirstName()) {
            Log.i("submitCheck2", String.valueOf(validateFirstName()));
            return;
        }
        if (!validateMiddleName()) {
            Log.i("submitCheck12", String.valueOf(validateMiddleName()));
            return;
        }
        if (!validateLastName()) {
            Log.i("submitCheck3", String.valueOf(validateLastName()));
            return;
        }
        if (!validateMaidenName()) {
            Log.i("submitCheck13", String.valueOf(validateMaidenName()));
            return;
        }
        if (!validateGender()) {
            Log.i("submitCheck4", String.valueOf(validateGender()));
            return;
        }
        if (!validateMobile()) {
            Log.i("submitCheck5", String.valueOf(validateMobile()));
            return;
        }
        if (!validateEmail()) {
            Log.i("submitCheck6", String.valueOf(validateEmail()));
            return;
        }
        if (!validateCountry()) {
            Log.i("submitCheck7", String.valueOf(validateCountry()));
            return;
        }
        if (!validateState()) {
            Log.i("submitCheck8", String.valueOf(validateState()));
            return;
        }
        if (!validateCity()) {
            Log.i("submitCheck9", String.valueOf(validateCity()));
            return;
        }
        if (!validateStreet()) {
            Log.i("submitCheck10", String.valueOf(validateStreet()));
            return;
        }
        if (!validatePincode()) {
            Log.i("submitCheck11", String.valueOf(validatePincode()));
            return;
        }
        JSONObject jsonObject = new JSONObject();

        try {
            if (addbeneficiary.isChecked()) {
                jsonObject.put("id", iD);
            } else {
                jsonObject.put("id", "");
            }
            jsonObject.put("email", metEmail.getText().toString());
            jsonObject.put("first_name", metFirstName.getText().toString());
            jsonObject.put("last_name", metLastName.getText().toString());
            jsonObject.put("middle_name", metMiddleName.getText().toString());
            jsonObject.put("maiden_name", metMaidenName.getText().toString());
            jsonObject.put("city_id", CityID);
            jsonObject.put("mobile", metMobile.getText().toString());
            jsonObject.put("phone", metMobile.getText().toString());
            jsonObject.put("gender", metGender.getText().toString());
            jsonObject.put("street", metStreet.getText().toString());
            jsonObject.put("zipcode", metPincode.getText().toString());
            Log.i("test3", jsonObject.toString());
            MenuMetadata.getResponse(AddUpdateBeneficiary.this, ApiURLNew.URL_SEND_MONEY_ADDBENEFICIARY, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean validateSelectBeneficiary() {
        if (metselectBeneficiary.getText().toString().isEmpty()) {
            metselectBeneficiary.setError("This field is required");
            metselectBeneficiary.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateFirstName() {
        if (metFirstName.getText().toString().isEmpty() || metFirstName.getText().toString().length() < 3) {
            metFirstName.setError("Enter Valid First Name");
            metFirstName.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateMiddleName() {
        if (metMiddleName.getText().toString().isEmpty() || metMiddleName.getText().toString().length() < 3) {
            metMiddleName.setError("Enter Valid Middle Name");
            metMiddleName.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateLastName() {
        if (metLastName.getText().toString().isEmpty() || metLastName.getText().toString().length() < 3) {
            metLastName.setError("Enter Valid Last Name");
            metLastName.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateMaidenName() {
        if (metMaidenName.getText().toString().isEmpty() || metMaidenName.getText().toString().length() < 3) {
            metMaidenName.setError("Enter Valid Maiden Name");
            metMaidenName.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateGender() {
        if (metGender.getText().toString().isEmpty()) {
            metGender.setError("Select Gender");
            metGender.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateMobile() {
        if (metMobile.getText().toString().isEmpty() || metMobile.getText().toString().length() < 10) {
            metMobile.setError("Enter valid mobile no.");
            metMobile.requestFocus();
            return false;
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            AddUpdateBeneficiary.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validateEmail() {
        String email = metEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            metEmail.setError("Enter valid email");
            metEmail.requestFocus();
            return false;
        }
        return true;
    }


    private boolean validateCountry() {
        if (metCountry.getText().toString().isEmpty()) {
            metCountry.setError("Select Country");
            metCountry.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateState() {
        if (metState.getText().toString().isEmpty()) {
            metState.setError("Select State");
            metState.requestFocus();
            return false;
        }
        return true;
    }


    private boolean validateCity() {
        if (metCity.getText().toString().isEmpty()) {
            metCity.setError("Select City");
            metCity.requestFocus();
            return false;
        }
        return true;
    }


    private boolean validateStreet() {
        if (metStreet.getText().toString().isEmpty() || metStreet.getText().toString().length() < 4) {
            metStreet.setError("Enter valid street name");
            metStreet.requestFocus();
            return false;
        }
        return true;
    }


    private boolean validatePincode() {
        if (metPincode.getText().toString().isEmpty()) {
            metPincode.setError("Enter pincode");
            metPincode.requestFocus();
            return false;
        }
        return true;
    }


}



