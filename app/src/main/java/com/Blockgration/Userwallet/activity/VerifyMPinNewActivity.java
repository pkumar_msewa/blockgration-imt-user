package com.Blockgration.Userwallet.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.UserModel;
import com.Blockgration.Userwallet.R;

import me.philio.pinentry.PinEntryView;

/**
 * Created by Ksf on 7/26/2016.
 */
public class VerifyMPinNewActivity extends AppCompatActivity {
    private PinEntryView etVerifyPin;

    private Button btnForgetPin;
    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();

    private LoadingDialog loadDlg;
    private TextWatcher textWatcher;
    private boolean isRunning = false;

    private String detailMessage;

    //Volley Tag
    private String tag_json_obj = "json_user";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("Logout"));


        setContentView(R.layout.activity_verify_m_pin_new);
        loadDlg = new LoadingDialog(VerifyMPinNewActivity.this);

        etVerifyPin = (PinEntryView) findViewById(R.id.etVerifyPin);
        btnForgetPin = (Button) findViewById(R.id.btnForgetPin);
        etVerifyPin.requestFocus();


        btnForgetPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(VerifyMPinNewActivity.this, ForgetMPinActivity.class));
            }
        });


        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 4) {
                    verifyMPin();
                }
            }
        };

        etVerifyPin.addTextChangedListener(textWatcher);
    }

    private void verifyMPin() {
        isRunning = true;
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("mpin", etVerifyPin.getText().toString());
//            jsonRequest.put("username", session.getUserMobileNo());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_VERIFY_M_PIN, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Verify PIN RESPONSE", response.toString());
                        String code = response.getString("code");
                        String jsonString = response.getString("response");

                        JSONObject jsonObject = new JSONObject(jsonString);

                        if (jsonObject.has("details")) {
                            detailMessage = jsonObject.getString("details");
                        }

                        if (code != null && code.equals("S00")) {
                            //Verified
                            Intent mainIntent = new Intent(VerifyMPinNewActivity.this, MainActivity.class);
                            loadDlg.dismiss();
                            startActivity(mainIntent);
                            finish();
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else if (code != null && code.equals("F00")) {
                            etVerifyPin.getText().clear();
                            detailMessage = response.getString("message");
                            loadDlg.dismiss();
                            CustomToast.showMessage(getApplicationContext(), detailMessage);
                        } else {
                            etVerifyPin.getText().clear();
                            loadDlg.dismiss();
                            CustomToast.showMessage(getApplicationContext(), detailMessage);
                        }


                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception));


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(VerifyMPinNewActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                promoteLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void promoteLogout() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_LOGOUT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    loadDlg.show();
                    try {
                        String code = jsonObj.getString("code");
                        if (code != null && code.equals("S00")) {
                            UserModel.deleteAll(UserModel.class);
                            startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
                            finish();
                        } else {
                            UserModel.deleteAll(UserModel.class);
                            startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
                            finish();
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };


}
