package com.Blockgration.Userwallet.activity;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Blockgration.Userwallet.activity.shopping.ShoppingActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.Blockgration.Userwallet.MyService;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.fragment.billpayment.BillPaymentsFragment;
import com.Blockgration.fragment.billpayment.PNationBillPayFragment;
import com.Blockgration.fragment.mainmenufragment.ExchangeMoneyFragment;
import com.Blockgration.fragment.mainmenufragment.FundTransferFragment;
import com.Blockgration.fragment.mainmenufragment.HomeFragment;

import com.Blockgration.fragment.mainmenufragment.QwikPaymentFragment;
import com.Blockgration.fragment.mainmenufragment.ReceiptFragment;
import com.Blockgration.fragment.mainmenufragment.RequestMoneyFragment;
import com.Blockgration.fragment.mainmenufragment.TelcoServiceFragment;
import com.Blockgration.fragment.mainmenufragment.WebViewFragment;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.AccountModel;
import com.Blockgration.model.CurrentAccountModel;
import com.Blockgration.model.MobileRechargeModel;
import com.Blockgration.model.UserModel;
//import com.msewa.pqgeolocation.activity.PQApplication;
//import com.msewa.pqgeolocation.model.PayQwikInit;
//import com.msewa.pqgeolocation.util.RegisterGatewayApi;
import com.Blockgration.pqgeolocation.activity.NotificationListActivity;
import com.Blockgration.pqgeolocation.metadata.ApiUrl;
import com.Blockgration.pqgeolocation.model.PayQwikInit;
import com.Blockgration.pqgeolocation.util.RegisterGatewayApi;
import com.Blockgration.util.ColorUtil;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.SecurityUtil;
import com.orm.query.Select;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends SuperActivity implements NavigationView.OnNavigationItemSelectedListener {
    private UserModel session = UserModel.getInstance();
    private DrawerLayout drawer;
    //    private RequestQueue rq;
    private LoadingDialog loadDlg;
    private boolean mRecentlyBackPressed;
    private Runnable mExitRunnable = new Runnable() {

        @Override
        public void run() {
            mRecentlyBackPressed = false;
        }
    };
    //Language Shared Preference.
    public static final String LANG = "lang";
    SharedPreferences langPreferences;
    private String language;
    private JSONObject jsonRequest;
    private String mPinCreated = "";
    private Handler mExitHandler = new Handler();
    private String action = null;
    //Volley Tag
    private String tag_json_obj = "json_obj_req";
    private boolean sm;
    private static final long delay = 2000L;
    private boolean isChanged = false;

    private RequestQueue rq;

    private String accBalance;
    private String accNo;
    private String balanceLimit;
    private String accType;
    private String dailyLimit;
    private String accCountry;
    private String monthlyLimit;
    private String currencyName;
    private String currencyCode;
    private String currencySymbol;
    private String userEmailStatus = "";

    private TextView tvNavUserName, tvNavUserAccNo, tvNavUserAccName;
    private Button btnUpgradeWallet;
    private CircleImageView ivNavProfilPic;
    private List<CurrentAccountModel> currentAccountArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        langPreferences = getSharedPreferences(LANG, Context.MODE_PRIVATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("setting-change"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mThemeReceiver, new IntentFilter("theme-change"));

        language = langPreferences.getString("language", "");
        checkLangPref();

        setContentView(com.Blockgration.Userwallet.R.layout.activity_main);
        rq = Volley.newRequestQueue(MainActivity.this);
        loadDlg = new LoadingDialog(this);
        Toolbar toolbar = (Toolbar) findViewById(com.Blockgration.Userwallet.R.id.toolbar);
        ImageView ivPQLogo = (ImageView) findViewById(com.Blockgration.Userwallet.R.id.ivPQLogo);
        setSupportActionBar(toolbar);


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);

        tvNavUserName = (TextView) header.findViewById(R.id.tvNavUserName);
        tvNavUserAccNo = (TextView) header.findViewById(R.id.tvNavUserAccNo);
        tvNavUserAccName = (TextView) header.findViewById(R.id.tvNavUserAccName);
        btnUpgradeWallet = (Button) header.findViewById(R.id.btnUpgradeWallet);
        ivNavProfilPic = (CircleImageView) header.findViewById(R.id.ivNavProfilPic);
        setViews();
        setNavigationHeader();

        drawer = (DrawerLayout) findViewById(com.Blockgration.Userwallet.R.id.drawer_layout);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });


        toolbar.setTitle("");

        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean("preSwitchTheme", false)) {
            toolbar.setBackgroundColor(ColorUtil.getColor(MainActivity.this, com.Blockgration.Userwallet.R.color.white_text));
            toolbar.setNavigationIcon(com.Blockgration.Userwallet.R.drawable.ic_navigation);

        } else {
            int color = Color.parseColor("#ffffff");
            ivPQLogo.setColorFilter(color);
            toolbar.setBackgroundColor(ColorUtil.getColor(MainActivity.this, com.Blockgration.Userwallet.R.color.dark_grey));
            toolbar.setNavigationIcon(com.Blockgration.Userwallet.R.drawable.ic_navigation_white);
        }

//        GetRSSDataTask task = new GetRSSDataTask();
//        task.execute("");

        //New Thread
        Handler mHandler = new Handler();
        mHandler.postDelayed(navigationTask, 200);
        getUserDetail();

    }

    private void setNavigationHeader() {
        AQuery aq = new AQuery(MainActivity.this);
        if (session.getUserImage() != null && !session.getUserImage().equals("")) {
            aq.id(ivNavProfilPic).background(R.drawable.ic_user_load).image(decodeFromBase64ToBitmap(session.getUserImage())).getContext();
            Log.i("MAME", "1" + session.getUserImage());
        } else {
            Picasso.with(MainActivity.this).load(R.drawable.ic_user_load).placeholder(R.drawable.ic_user_load).into(ivNavProfilPic);
            Log.i("MAME", "2" + session.getUserImage());
        }

        tvNavUserName.setText(session.getUserFirstName());
        currentAccountArray = Select.from(CurrentAccountModel.class).list();
        tvNavUserAccNo.setText("Acc No: " + currentAccountArray.get(0).getAccNo());
        tvNavUserAccName.setText("Balance:  " + currentAccountArray.get(0).getAccBalance());
        btnUpgradeWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(), HowToUpgradeActivity.class));
            }
        });

        /*if (session.getUserAcName() != null && session.getUserAcName().trim().equals("KYC")) {
            tvNavUserAccName.setText("Wallet:  " + session.getUserAcName());
            btnUpgradeWallet.setVisibility(View.GONE);
        } else {
            tvNavUserAccName.setText("Wallet:  " + session.getUserAcName());
            btnUpgradeWallet.setVisibility(View.VISIBLE);
        }*/
    }

    private Bitmap decodeFromBase64ToBitmap(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public void showLangDialog() {
        final String[] items = {getResources().getString(com.Blockgration.Userwallet.R.string.lang_english), getResources().getString(com.Blockgration.Userwallet.R.string.lang_spanish)};
        android.support.v7.app.AlertDialog.Builder langDialog =
                new android.support.v7.app.AlertDialog.Builder(MainActivity.this, com.Blockgration.Userwallet.R.style.AppCompatAlertDialogStyle);
        langDialog.setTitle(getResources().getString(com.Blockgration.Userwallet.R.string.lang));
        langDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SharedPreferences.Editor editor = langPreferences.edit();
                editor.clear();
                editor.putString("language", "English");
                editor.apply();
                dialog.dismiss();
            }
        });
        langDialog.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int position) {
                if (position == 0) {
                    SharedPreferences.Editor editor = langPreferences.edit();
                    editor.clear();
                    editor.putString("language", "English");
                    editor.apply();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);

                } else if (position == 1) {
                    SharedPreferences.Editor editor = langPreferences.edit();
                    editor.clear();
                    editor.putString("language", "Spanish");
                    editor.apply();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);

                }
            }

        });

        langDialog.show();
    }

    public void setViews() {
        NavigationView navigationView = (NavigationView) findViewById(com.Blockgration.Userwallet.R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        displayView(1);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.Blockgration.Userwallet.R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == com.Blockgration.Userwallet.R.id.menuSetting) {
            Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
            startActivity(intent);
            return true;
        } else if (id == com.Blockgration.Userwallet.R.id.menuSignOut) {
            loadDlg.show();
            promoteLogout();
            return true;
        } else if (id == com.Blockgration.Userwallet.R.id.menuLanguage) {
            showLangDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(com.Blockgration.Userwallet.R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (mRecentlyBackPressed) {
            android.support.v7.app.AlertDialog.Builder exitDialog =
                    new android.support.v7.app.AlertDialog.Builder(MainActivity.this, com.Blockgration.Userwallet.R.style.AppCompatAlertDialogStyle);
            exitDialog.setTitle(getResources().getString(com.Blockgration.Userwallet.R.string.exit));
            exitDialog.setPositiveButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
//            getResources().getString(com.msewa.claro.R.string.yes)
            exitDialog.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finishAffinity();
                }
            });
            exitDialog.show();
        } else {
            Intent intent = new Intent();
            intent.putExtra("postion", sm);
            if (getSupportFragmentManager().popBackStackImmediate()) {
                System.out.println("ppp");
                getFragmentManager().popBackStack();
            }
            mRecentlyBackPressed = true;
            mExitHandler.postDelayed(mExitRunnable, delay);
        }


    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == com.Blockgration.Userwallet.R.id.nav_home) {
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(1);
                }
            }, 300);
        } else if (id == com.Blockgration.Userwallet.R.id.nav_send_money) {
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(3);
                }
            }, 300);
        } else if (id == com.Blockgration.Userwallet.R.id.nav_mobile_topup) {
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(2);
                }
            }, 300);
        } else if (id == com.Blockgration.Userwallet.R.id.nav_bill_pay) {
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(4);
                }
            }, 300);
//        } else if (id == com.msewa.claro.R.id.nav_exchange_money) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(5);
//                }
//            }, 300);
        } else if (id == com.Blockgration.Userwallet.R.id.nav_travel) {
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(6);
                }
            }, 300);
        } else if (id == com.Blockgration.Userwallet.R.id.nav_statement) {
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(7);
                }
            }, 300);
        } else if (id == com.Blockgration.Userwallet.R.id.nav_quick_payment) {
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(8);
                }
            }, 300);
//        } else if (id == com.msewa.claro.R.id.nav_req_money) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(9);
//                }
//            }, 300);
        } else if (id == R.id.nav_geoloacation) {
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(10);
                }
            }, 300);
        }
//        else if (id == R.id.nav_invite) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(10);
//                }
//            }, 300);
//        }
//        else if (id == com.msewa.claro.R.id.nav_near) {
//            PQApplication.mainUrl = "http://66.207.206.54:8041/GeoNotify/ws/api/v1/wallet/en/";
//            PQApplication.secretKey = "2DFF462E1DC9D398D456C8BC2B8AD518CF2F95FF";
//            PQApplication.tokenKey = "MTQ5OTgzODc3NDQzNg==";
//            PQApplication.appName = "Vpayqwik";
//            PQApplication.init(this);
//            RegisterGatewayApi api = new RegisterGatewayApi(new PayQwikInit(MainActivity.this));
//            api.register(session.getUserMobileNo());
//        }


        else if (id == R.id.nav_shopping) {
            drawer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(5);
                }
            }, 300);

        }
// 300);
//
//        }   else if (id == R.id.nav_travel) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(6);
//                }
//            }, 300);
//        } else if (id == R.id.nav_merchant_payment) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(7);
//                }
//            }, 300);
//        } else if (id == R.id.nav_quick_payment) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(8);
//                }
//            }, 300);
//        } else if (id == R.id.nav_qr_payment) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(9);
//                }
//            }, 300);
//        } else if (id == R.id.nav_redeem_coupen) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(10);
//                }
//            }, 300);
//        } else if (id == R.id.nav_invite) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(11);
//                }
//            }, 300);
//        } else if (id == R.id.nav_statement) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(12);
//                }
//            }, 300);
//        } else if (id == R.id.nav_customer_phone) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(13);
//                }
//            }, 300);
//        } else if (id == R.id.nav_customer_email) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(14);
//                }
//            }, 300);
//        } else if (id == R.id.nav_currency_converter) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(15);
//                }
//            }, 300);
//        } else if (id == R.id.nav_exchange_money) {
//            drawer.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    displayView(16);
//                }
//            }, 300);
//        }


        DrawerLayout drawer = (DrawerLayout) findViewById(com.Blockgration.Userwallet.R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void displayView(int position) {
        Fragment fragment = null;
        Bundle navBundle = new Bundle();
        navBundle.putString(AppMetadata.FRAGMENT_TYPE, AppMetadata.FRAGMENT_TYPE);

        if (position == 1) {
            try {
//                stopService(new Intent(MainActivity.this, MyService.class));
//                startService(new Intent(MainActivity.this, MyService.class));
                fragment = new HomeFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(com.Blockgration.Userwallet.R.id.container_body, fragment);
                fragmentTransaction.commit();
            } catch (IllegalStateException e) {
                isChanged = true;
            }
        } else if (position == 2) {
            fragment = new TelcoServiceFragment();
        } else if (position == 3) {
            fragment = new FundTransferFragment();
        } else if (position == 4) {
//            CustomToast.showMessage(MainActivity.this, getResources().getString(R.string.soon));
//            fragment = new BillPaymentsFragment();
            fragment = new PNationBillPayFragment();
            fragment.setReenterTransition(true);
        } else if (position == 5) {
            Intent shoppingIntent = new Intent(MainActivity.this, ShoppingActivity.class);
            startActivity(shoppingIntent);
            finish();
//            fragment = new ExchangeMoneyFragment();
        } else if (position == 6) {
            fragment = new WebViewFragment();
            Bundle bType = new Bundle();
            bType.putString("URL", "http://travel.vpayqwik.com/");
            fragment.setArguments(bType);
        } else if (position == 7) {
            fragment = new ReceiptFragment();
        } else if (position == 8) {
            fragment = new QwikPaymentFragment();
        }
//        else if (position == 10) {
//            fragment = new InviteFreindFragment();
//        }
        else if (position == 9) {
            fragment = new RequestMoneyFragment();
        } else if (position == 10) {
            Intent notificationIntent = new Intent(MainActivity.this, NotificationListActivity.class);
            notificationIntent.putExtra("UserName", "8553926329");
            startActivity(notificationIntent);
//            finish();
        }

        if (fragment != null) {
            try {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(com.Blockgration.Userwallet.R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);

                fragmentTransaction.commit();
            } catch (IllegalStateException e) {
                isChanged = true;
            }
        }
    }

    private void promoteLogout() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("JsonRequest URL", ApiURLNew.URL_LOGOUT);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_LOGOUT, jsonRequest, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onResponse(JSONObject jsonObj) {
                    Log.i("JsonRequest", jsonObj.toString());
                    try {
                        String code = jsonObj.getString("code");
                        if (code != null && code.equals("S00")) {
                            UserModel.deleteAll(UserModel.class);
                            AccountModel.deleteAll(AccountModel.class);
                            CurrentAccountModel.deleteAll(CurrentAccountModel.class);
                            loadDlg.dismiss();
                            finish();
                            startActivity(new Intent(getApplicationContext(), LoginRegActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        } else {
                            UserModel.deleteAll(UserModel.class);
                            AccountModel.deleteAll(AccountModel.class);
                            CurrentAccountModel.deleteAll(CurrentAccountModel.class);
                            loadDlg.dismiss();
                            finishAffinity();
                            startActivity(new Intent(getApplicationContext(), LoginRegActivity.class));
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(com.Blockgration.Userwallet.R.string.server_exception));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", SecurityUtil.getSecurityKey(jsonRequest.toString()));
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    private void checkLangPref() {
        switch (language) {
            case "English":
                setLocale("en");
                break;
            case "Spanish":
                setLocale("es");
                break;
            default:
                setLocale("en");
                showLangDialog();
                break;
        }

    }

    public void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mThemeReceiver);
        super.onDestroy();
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            action = intent.getStringExtra("updates");
            if (action.equals("1")) {
                getUserDetail();
            } else if (action.equals("2")) {
                loadDlg.show();
                promoteLogout();
            } else if (action.equals("3")) {
                UpdateUserDetailsTask task = new UpdateUserDetailsTask();
                task.execute();
            }

        }
    };

    private class UpdateUserDetailsTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i("Step1", "execute");
            if (action != null && action.equals("3")) {
                setViews();
                loadDlg.dismiss();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getUserDetail();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (action != null && action.equals("3")) {
                setViews();
                loadDlg.dismiss();
            } else {
                Log.i("Step1", "three");
                isChanged = true;
            }

        }
    }

//    private class GetRSSDataTask extends AsyncTask<String, Void, List<MobileRechargeModel>> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected List<MobileRechargeModel> doInBackground(String... urls) {
//            try {
//                DefaultHttpClient httpclient = new DefaultHttpClient();
//
//                StringEntity params = new StringEntity(MainActivity.getSkuList());
//                String url = "https://qa.valuetopup.com/posaservice/servicemanager.asmx?WSDL";
//
//                HttpPost httpPost = new HttpPost(url);
//
//                httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");
//                httpPost.setEntity(params);
//                HttpResponse response12 = httpclient.execute(httpPost);
//                String json_string = EntityUtils.toString(response12.getEntity());
//                Log.i("JSONREQUEST", json_string);
//                JSONObject jsonObj = null;
//                try {
//
//                    jsonObj = XML.toJSONObject(json_string);
////                    XMLSerializer xmlSerializer = new XMLSerializer();
////                    JSON json = xmlSerializer.read(json_string);
//
//                    //Converted XML to Json
//                    JSONObject mainEnvelop = jsonObj.getJSONObject("soap:Envelope");
//                    JSONObject mainBody = mainEnvelop.getJSONObject("soap:Body");
//                    JSONObject skuResponse = mainBody.getJSONObject("GetSkuListResponse");
//                    JSONObject skuListResult = skuResponse.getJSONObject("GetSkuListResult");
//                    JSONObject skuJson = skuListResult.getJSONObject("skus");
//                    JSONArray skuList = skuJson.getJSONArray("sku");
//
//                    MobileRechargeModel.deleteAll(MobileRechargeModel.class);
//
//                    for (int i = 0; i < skuList.length(); i++) {
//                        JSONObject c = skuList.getJSONObject(i);
//                        String countryCode = c.getString("countryCode");
//                        String operatorName = c.getString("carrierName");
//                        String minAmount = c.getString("minAmount");
//                        String maxAmount = c.getString("maxAmount");
//                        String skuId = c.getString("skuId");
//                        String category = c.getString("category");
//                        if (category != null && category.equals("Rtr")) {
//                            MobileRechargeModel cModel = new MobileRechargeModel(countryCode, operatorName, minAmount, maxAmount, skuId, category);
//                            cModel.save();
//                        }
//
//                    }
//                } catch (JSONException e) {
//                    Log.e("JSON exception", e.getMessage());
//                    e.printStackTrace();
//                } catch (NoClassDefFoundError e) {
//                    e.printStackTrace();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(List<MobileRechargeModel> result) {
//
//        }
//    }

    public static String getSkuList() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder;
            docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            //Envelope
            Element envelope = doc.createElement("soap:Envelope");
            doc.appendChild(envelope);
            envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
            envelope.setAttribute("xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");
            //Header
            Element header = doc.createElement("soap:Header");
            Element authHeader = doc.createElement("AuthenticationHeader");
            authHeader.setAttribute("xmlns", "http://www.pininteract.com");

            Element userId = doc.createElement("userId");
            userId.appendChild(doc.createTextNode("globaltest"));

            Element password = doc.createElement("password");
            password.appendChild(doc.createTextNode("welcome1"));

            authHeader.appendChild(userId);
            authHeader.appendChild(password);
            header.appendChild(authHeader);
            envelope.appendChild(header);
            //Body tag
            Element body = doc.createElement("soap:Body");
            Element skuList = doc.createElement("GetSkuList");
            skuList.setAttribute("xmlns", "http://www.pininteract.com");

            Element version = doc.createElement("version");
            version.appendChild(doc.createTextNode("1.0"));

            skuList.appendChild(version);
            body.appendChild(skuList);
            envelope.appendChild(body);

            String payload = toString(doc);

            return payload;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }


    public static String toString(Document doc) {

        try {
            StringWriter sw = new StringWriter();
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");

            transformer.transform(new DOMSource(doc), new StreamResult(sw));
            return sw.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private BroadcastReceiver mThemeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            intent = getIntent();
            finish();
            startActivity(intent);

        }
    };

    @Override
    protected void onPause() {
        Log.i("ONPAuse", "kisk");
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("REPOSNE", "value" + isChanged);
        if (isChanged) {
            setViews();
        }
    }

    private Runnable navigationTask = new Runnable() {
        @Override
        public void run() {
            try {
                setViews();
            } catch (IllegalStateException e) {
                isChanged = true;
            }
        }
    };

    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getAssets().open("getUserDetails.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void getUserDetail() {
        final LoadingDialog loadDlg1 = new LoadingDialog(MainActivity.this);
        loadDlg1.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("userDetails Request", jsonRequest.toString());
            Log.i("userDetails URL", ApiURLNew.URL_USER_DETAILS);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_USER_DETAILS, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject res) {
                    if ((loadDlg1 != null) && loadDlg1.isShowing()) {
                        loadDlg1.dismiss();
                    }

                    try {
                        Log.i("userDetails Response", res.toString());
//                        Log.i("GetUserDetails Response", loadJSONFromAsset().toString());
//                        JSONObject res = new JSONObject(loadJSONFromAsset());
                        String resp = res.getString("response");
                        JSONObject obj = new JSONObject(resp);
                        String code = obj.getString("code");
                        if (code != null & code.equals("S00")) {
//                            String rep = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(rep);
//                            JSONObject jsonDetail = res.getJSONObject("details");
                            JSONObject jsonDetail = obj.getJSONObject("details");
                            JSONObject jsonCurrentAcc = jsonDetail.getJSONObject("activeAccount");
//                            JSONObject jsonCurrentAcc = jsonObject.getJSONObject("activeAccount");
                            CurrentAccountModel.deleteAll(CurrentAccountModel.class);

                            String currentAccNo = jsonCurrentAcc.getString("accountNumber");
                            String currentAccBalance = jsonCurrentAcc.getString("balance");

                            String currentBalanceLimit = null;
                            String currentAccType = null;
                            String currentDailyLimit = null;
                            String currentMonthlyLimit = null;

//                            JSONArray accountDetail = jsonObject.getJSONArray("accountDetail");
//                            for(int i =0;i<accountDetail.length();i++){
//                                JSONObject arrayObj = accountDetail.getJSONObject(i);
//                                JSONObject jCurrentType = arrayObj.getJSONObject("type");
//                                currentBalanceLimit = jCurrentType.getString("balanceLimit");
//                                currentAccType = jCurrentType.getString("code");
//                                currentDailyLimit = jCurrentType.getString("dailyLimit");
//                                currentMonthlyLimit = jCurrentType.getString("monthlyLimit");
//                            }

                            JSONObject jCurrentType = jsonCurrentAcc.getJSONObject("accountType");
                            currentBalanceLimit = jCurrentType.getString("balanceLimit");
                            currentAccType = jCurrentType.getString("code");
                            currentDailyLimit = jCurrentType.getString("dailyLimit");
                            currentMonthlyLimit = jCurrentType.getString("monthlyLimit");

                            JSONObject jCurrentCountry = jsonCurrentAcc.getJSONObject("country");
                            String currentAccCountry = jCurrentCountry.getString("countryName");
                            String currentCurrencySymbol = jCurrentCountry.getString("countryISO2");

                            JSONObject jCurrentCurrency = jCurrentCountry.getJSONObject("currency");
                            String currentCurrencyName = jCurrentCurrency.getString("currencyName");
                            String currentCurrencyCode = jCurrentCurrency.getString("currencyISO3");
//                            String currentCurrencySymbol = jCurrentCurrency.getString("currencySymbol");

                            CurrentAccountModel currentAccountModel = new CurrentAccountModel(currentAccBalance, currentAccNo, currentBalanceLimit, currentAccType, currentDailyLimit, currentAccCountry
                                    , currentMonthlyLimit, currentCurrencyName, currentCurrencyCode, currentCurrencySymbol);
                            currentAccountModel.save();

                            AccountModel.deleteAll(AccountModel.class);
                            Object o = jsonDetail.get("accountDetail");
                            if (o instanceof JSONObject) {
                                accNo = ((JSONObject) o).getString("accountNumber");
//                                accBalance = ((JSONObject) o).getString("balance");

                                String bal = ((JSONObject) o).getString("balance");

                                double newBal = Double.parseDouble(bal);
                                DecimalFormat decimalFormat = new DecimalFormat("#.##");
                                String accBalance = decimalFormat.format(newBal);

                                Log.i("BALANCE", accBalance);

                                JSONObject jType = ((JSONObject) o).getJSONObject("accountType");
                                balanceLimit = jType.getString("balanceLimit");
                                accType = jType.getString("code");
                                dailyLimit = jType.getString("dailyLimit");
                                monthlyLimit = jType.getString("monthlyLimit");

                                JSONObject jCountry = ((JSONObject) o).getJSONObject("country");
                                accCountry = jCountry.getString("countryName");

                                JSONObject jCurrency = ((JSONObject) o).getJSONObject("currency");
                                currencyName = jCurrency.getString("currencyName");
                                currencyCode = jCurrency.getString("currencyISO3");
                                currencySymbol = jCurrency.getString("currencySymbol");

                                AccountModel accountModel = new AccountModel(accBalance, accNo, balanceLimit, accType, dailyLimit, accCountry
                                        , monthlyLimit, currencyName, currencyCode, currencySymbol);
                                accountModel.save();
                            } else if (o instanceof JSONArray) {
                                JSONArray accArray = jsonDetail.getJSONArray("accountDetail");
                                for (int i = 0; i < accArray.length(); i++) {
                                    JSONObject c = accArray.getJSONObject(i);

                                    accNo = c.getString("accountNumber");
                                    accBalance = c.getString("balance");

                                    JSONObject jType = c.getJSONObject("type");
                                    balanceLimit = jType.getString("balanceLimit");
                                    accType = jType.getString("code");
                                    dailyLimit = jType.getString("dailyLimit");
                                    monthlyLimit = jType.getString("monthlyLimit");

                                    JSONObject jCountry = c.getJSONObject("country");
                                    accCountry = jCountry.getString("countryName");

//                                    JSONObject jCurrency = c.getJSONObject("currency");
//                                    currencyName = jCurrency.getString("currencyName");
//                                    currencyCode = jCurrency.getString("currencyISO3");
//                                    currencySymbol = jCurrency.getString("currencySymbol");

                                    currencyName = jCountry.getString("currencyName");
                                    currencyCode = jCountry.getString("countryCode");
                                    currencySymbol = jCountry.getString("currencyCode");


                                    AccountModel accountModel = new AccountModel(accBalance, accNo, balanceLimit, accType, dailyLimit, accCountry
                                            , monthlyLimit, currencyName, currencyCode, currencySymbol);
                                    accountModel.save();
                                }
                            }
                            JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");
                            String userFName = jsonUserDetail.getString("firstName");
                            String userLName = jsonUserDetail.getString("lastName");
                            String userMobile = jsonUserDetail.getString("contactNo");
                            String userEmail = jsonUserDetail.getString("email");
                            String userAddress = jsonUserDetail.getString("address");
                            String userImage = jsonUserDetail.getString("image");
//                            String userAddress = res.getString("address");
//                            String userImage = res.getString("image");
                            userEmailStatus = jsonUserDetail.getString("emailStatus");

//                            boolean isMPin = false;
                            boolean isMPin = jsonUserDetail.getBoolean("pin");
                            String userDob = jsonUserDetail.getString("dateOfBirth");
                            String userGender = jsonUserDetail.getString("gender");

                            UserModel.deleteAll(UserModel.class);
                            UserModel userModel = new UserModel(session.getUserSessionId(), userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAddress, userImage, userDob, userGender, isMPin);
                            userModel.save();
                            session.setUserFirstName(userFName);
                            session.setUserLastName(userLName);
                            session.setUserMobileNo(userMobile);
                            session.setUserEmail(userEmail);
                            session.setEmailIsActive(userEmailStatus);
                            session.setUserImage(userImage);
                            session.setUserAddress(userAddress);
                            session.setUserGender(userGender);
                            session.setUserDob(userDob);
                            session.setMPin(isMPin);
                            session.setIsValid(1);
                            setViews();

                        } else if (code != null & code.equals("F03")) {
                            promoteLogout();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg1.dismiss();
                    Toast.makeText(MainActivity.this, NetworkErrorHandler.getMessage(error, MainActivity.this), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            rq.add(postReq);
        }
    }

//    private void sendLogout() {
//        Intent intent = new Intent("setting-change");
//        intent.putExtra("updates", "2");
//        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
//    }
}
