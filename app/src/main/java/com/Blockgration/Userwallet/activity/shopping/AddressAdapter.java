package com.Blockgration.Userwallet.activity.shopping;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.model.ShoppingAddressModel;


import java.util.List;


/**
 * Created by Kashif-PC on 1/3/2017.
 */
public class AddressAdapter extends BaseAdapter {

    private Context context;
    private List<ShoppingAddressModel> addressArray;
    private ViewHolder viewHolder;
    private InCartListner inCartListner;

    public AddressAdapter(Context context, List<ShoppingAddressModel> addressArray, InCartListner inCartListner) {
        this.context = context;
        this.inCartListner = inCartListner;
        this.addressArray = addressArray;
    }

    @Override
    public int getCount() {
        return addressArray.size();
    }

    @Override
    public Object getItem(int index) {
        return addressArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_address, null);
            viewHolder = new ViewHolder();
            viewHolder.cvAdd = (CardView) convertView.findViewById(R.id.cvAdd);
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.tvMobile = (TextView) convertView.findViewById(R.id.tvMobile);
            viewHolder.tvAdress = (TextView) convertView.findViewById(R.id.tvAdress);
            viewHolder.tvCItyCountry = (TextView) convertView.findViewById(R.id.tvCItyCountry);
            viewHolder.tvPostalCode = (TextView) convertView.findViewById(R.id.tvPostalCode);
            viewHolder.tvAddressTitle = (TextView) convertView.findViewById(R.id.tvAddressTitle);
            viewHolder.ivAddRemove = (ImageView) convertView.findViewById(R.id.ivAddRemove);
            viewHolder.ivAddEdit = (ImageView) convertView.findViewById(R.id.ivAddEdit);
//            viewHolder.rbSelectAdd = (RadioButton) convertView.findViewById(R.id.rbSelectAdd);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvMobile.setText("Address " + (position+1));
        viewHolder.tvName.setText(addressArray.get(position).getFirstname() + " " + addressArray.get(position).getLastname());
        viewHolder.tvMobile.setText("Contact No: " + addressArray.get(position).getMobilenumber());
        viewHolder.tvAdress.setText(addressArray.get(position).getStreetaddress() + ", " + addressArray.get(position).getLandmark());
        viewHolder.tvCItyCountry.setText(addressArray.get(position).getCity() + ", " + addressArray.get(position).getCountry());
        viewHolder.tvPostalCode.setText("Postal Code: " + String.valueOf(addressArray.get(position).getZipcode()));
//        viewHolder.rbSelectAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                addressArray.get(position).getOrderid();
//                inCartListner.selectAddress(String.valueOf(addressArray.get(position).getOrderid()));
//            }
//        });
        viewHolder.cvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addressArray.get(position).getOrderid();
                inCartListner.selectAddress(String.valueOf(addressArray.get(position).getOrderid()));
            }
        });

        viewHolder.ivAddRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String orderID = String.valueOf(addressArray.get(position).getOrderid());
                inCartListner.deleteAddress(orderID,position);
//                CustomToast.showMessage(context, "close selected for Address "+position);

            }
        });

        viewHolder.ivAddEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String orderID = String.valueOf(addressArray.get(position).getOrderid());
                inCartListner.editAddress(orderID,position);
//                CustomToast.showMessage(context, "close selected for Address "+position);

            }
        });

        return convertView;
    }


    static class ViewHolder {
        TextView tvName;
        TextView tvMobile;
        TextView tvAdress;
        TextView tvCItyCountry;
        TextView tvPostalCode;
        TextView tvAddressTitle;
        CardView cvAdd;
        ImageView ivAddRemove, ivAddEdit;
//        RadioButton rbSelectAdd;


    }

}
