package com.Blockgration.Userwallet.activity.shopping;//package in.msewa.vpayqwiktest.activity.shopping;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.content.LocalBroadcastManager;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.HurlStack;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.Volley;
//import com.google.android.youtube.player.YouTubePlayerView;
//import com.squareup.picasso.Picasso;
//
//
//import org.json.JSONException;
//import org.json.JSONObject;
//import java.util.HashMap;
//import java.util.Map;
//
//
//import in.msewa.custom.CustomToast;
//
//import in.msewa.custom.LoadingDialog;
//import in.msewa.metadata.ApiURLNew;
//import in.msewa.model.UserModel;
//import in.msewa.vpayqwiktest.PayQwikApp;
//import in.msewa.vpayqwiktest.R;
//import in.msewa.util.NetworkErrorHandler;
//import in.msewa.vpayqwiktest.activity.LoginRegActivity;
//
////import in.rupease.Animations.DescriptionAnimation;
//
//
///**
// * Created by Ksf on 4/6/2016.
// */
//public class ProductDetailActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, AddRemoveCartListner {
//    private ShoppingModel shoppingModel;
//
//    private Button addToCart, btn_shopping_details_cancel;
//    private TextView shoppingTitle, shoppingprice, tvDescr;
//    private ImageView shoppingImage;
//    private YouTubePlayerView youTubeView;
//    private YouTubePlayer ytp;
//    private UserModel session = UserModel.getInstance();
//    private PQCart pqCart = PQCart.getInstance();
//    LoadingDialog loadingDialog;
//    private String cartValue = "0";
//
//    private String vCode;
//
//    private RequestQueue rq;
//    private SliderLayout mDemoSlider;
//    private TextView tvshopping_product_selling_price;
//    private ImageView ivToolBarback;
//    private JsonObjectRequest postReq;
//    private String tag_json_obj = "json_events";
//    private Button btn_shopping_details_addTocart;
//    private ShopAdapter shoppingAdapter;
//    private Button btnGoCart;
//    private TextView tvcart_point;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_product_detail);
//        loadingDialog = new LoadingDialog(ProductDetailActivity.this);
//        ivToolBarback = (ImageView) findViewById(R.id.ivToolBarback);
//        ivToolBarback.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//        shoppingModel = getIntent().getParcelableExtra("ShoppingData");
//        mDemoSlider = (SliderLayout) findViewById(R.id.productImageSlider);
//        btn_shopping_details_addTocart = (Button) findViewById(R.id.btn_shopping_details_addTocart);
//        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("cart-clear"));
//
//        try {
//            rq = Volley.newRequestQueue(getApplicationContext());
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//
//        declare();
//
//
////        getVideo();
//
//
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//
//    }
//
//    private void getImageSlider() {
//
//        HashMap<String, String> file_maps = new HashMap<>();
//        file_maps.put("Hannibal", shoppingModel.getpImage());
//        file_maps.put("Big Bang Theory", shoppingModel.getpImage());
//        file_maps.put("House of Cards", shoppingModel.getpImage());
//
//        for (String name : file_maps.keySet()) {
//            DefaultSliderView textSliderView = new DefaultSliderView(this);
//            // initialize a SliderLayout
//            textSliderView.image(file_maps.get(name))
//                    .setScaleType()
//                    .setOnSliderClickListener(this);
//
//            //add your extra information
//            textSliderView.bundle(new Bundle());
//            textSliderView.getBundle()
//                    .putString("extra", name);
//
//            mDemoSlider.addSlider(textSliderView);
//        }
//
//        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.RotateDown);
//        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//        mDemoSlider.setCustomIndicator((PagerIndicator) findViewById(R.id.custom_indicator));
////        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
////        mDemoSlider.setDuration(4000);
//        mDemoSlider.addOnPageChangeListener(this);
//    }
//
//    private void declare() {
//        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
//        addToCart = (Button) findViewById(R.id.btn_shopping_details_addTocart);
//        shoppingTitle = (TextView) findViewById(R.id.tvshopping_details_title);
//        shoppingprice = (TextView) findViewById(R.id.tvshopping_product_price);
//        tvshopping_product_selling_price = (TextView) findViewById(R.id.tvshopping_product_selling_price);
//        shoppingImage = (ImageView) findViewById(R.id.ivshopping_details_image);
//        tvDescr = (TextView) findViewById(R.id.tvDescr);
//        btn_shopping_details_cancel = (Button) findViewById(R.id.btn_shopping_details_cancel);
//        btnGoCart = (Button) findViewById(R.id.btnGoCart);
//        tvcart_point = (TextView) findViewById(R.id.tvshopping_cart_value);
//
//        btn_shopping_details_cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        tvDescr.setText(shoppingModel.getpDesc());
//        shoppingTitle.setText(shoppingModel.getpName());
//        shoppingprice.setText("RS :  " + getString(R.string.rupease) + " " + shoppingModel.getpPrice());
//        tvshopping_product_selling_price.setText("Brand - " + shoppingModel.getpBrand());
//        if (shoppingModel.getpImage().length() != 0) {
//            getImageSlider();
//            Picasso.with(getApplicationContext()).load(shoppingModel.getpImage())
//                    .placeholder(R.drawable.loading_image).into(shoppingImage);
//        } else {
//            shoppingImage.setVisibility(View.GONE);
//        }
//
//        if (pqCart.isProductInCart(shoppingModel)) {
//            addToCart.setText("Remove");
//            addToCart.setBackgroundResource(R.drawable.bg_button_pressed);
//
//        } else {
//            addToCart.setText("Add");
//            addToCart.setBackgroundResource(R.drawable.bg_red_btn);
//        }
//
//        addToCart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (session.getIsValid() == 1) {
//                    if (pqCart.isProductInCart(shoppingModel)) {
//                        removeProduct(shoppingModel);
//                        addToCart.setBackgroundResource(R.drawable.bg_red_btn);
//                    } else {
//                        addProduct(shoppingModel);
//                        addToCart.setBackgroundResource(R.drawable.bg_button);
//                    }
//                } else {
//                    Intent loginIntent = new Intent(getApplicationContext(), LoginRegActivity.class);
//                    startActivity(loginIntent);
//                }
//            }
//        });
//
//        btnGoCart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.i("CARTVALUE", cartValue);
//                if (session.getIsValid() == 1) {
//                    if (cartValue.equals("0")) {
//                        CustomToast.showMessage(getApplicationContext(), "You have no products in cart.");
//                    } else {
//                        Intent goToCartIntent = new Intent(getApplicationContext(), TeleBuyPaymentActivity.class);
//                        startActivity(goToCartIntent);
//                    }
//                } else {
//                    Intent loginIntent = new Intent(getApplicationContext(), LoginRegActivity.class);
//                    startActivity(loginIntent);
//                }
//            }
//        });
//    }
//
//    private String getMessage() {
//        String message = "<!DOCTYPE html>\n" +
//                "<html>\n" +
//                "<body>\n" +
//                "<p> <strike>" + getString(R.string.rupease) + shoppingModel.getpPrice() + "</strike> </p>\n" +
//                "</body>\n" +
//                "</html>";
//        return message;
//    }
//
////    private void getVideo() {
////        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, ApiUrl.URL_GET_SHOPPING_VIDEO + shoppingModel.getPid(), (String) null,
////                new Response.Listener<JSONObject>() {
////                    @Override
////                    public void onResponse(JSONObject response) {
////                        try {
////                            Log.i("Video Response", response.toString());
////                            JSONArray videoJArray = response.getJSONArray("youtubeurl");
////                            for (int i = 0; i < videoJArray.length(); i++) {
////                                JSONObject c = videoJArray.getJSONObject(i);
////                                if (c.has("url")) {
////                                    String url = c.getString("url");
////                                    if (url != null && !url.isEmpty()) {
////                                        String[] videoCo = url.substring(url.lastIndexOf("/")).split("/");
////                                        vCode = videoCo[1];
////                                        Log.i("Video Code", vCode);
////
////                                        try {
////                                            Log.i("Video Code", "Sucess");
////                                            youTubeView.initialize(AppMetadata.YOUTUBE_DEVELOPER_KEY, ProductDetailActivity.this);
////                                        } catch (Exception e) {
////                                            Log.i("Video", "Failed");
////                                            e.printStackTrace();
////                                        }
////                                    }
////                                }
////                            }
////                        } catch (JSONException e) {
////                            e.printStackTrace();
////                        }
////
////                    }
////                }, new Response.ErrorListener() {
////
////            @Override
////            public void onErrorResponse(VolleyError error) {
////            }
////        }) {
////
////        };
////        rq.add(jsonObjReq);
////
////    }
//    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getStringExtra("updates");
//            if (action.equals("1")) {
//                pqCart.clearCart();
////                ShoppingModel.delete(ShoppingModel.class);
////                shoppingArray.clear();
//                tvcart_point.setText("0");
////                shoppingAdapter.notifyDataSetChanged();
//
//            }else if(action.equals("2")){
//                cartValue= String.valueOf(pqCart.getProductsInCartArray().size());
//                tvcart_point.setText(cartValue);
//            }
//        }
//    };
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        cartValue = String.valueOf(pqCart.getProductsInCart().size());
//        Log.i("CARTONRESUME", cartValue);
//        tvcart_point.setText(cartValue);
//        if (pqCart.isProductInCart(shoppingModel)) {
//            addToCart.setText("Remove");
//            addToCart.setBackgroundResource(R.drawable.bg_button_pressed);
//
//        } else {
//            addToCart.setText("Add");
//            addToCart.setBackgroundResource(R.drawable.bg_red_btn);
//        }
//    }
//
//
//    @Override
//    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
//        if (!b) {
//            youTubePlayer.cueVideo(vCode);
//        }
//    }
//
//    @Override
//    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
//        Toast.makeText(this, "Initialization Fail", Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    protected void onStop() {
//        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
//        mDemoSlider.stopAutoCycle();
//        super.onStop();
//    }
//
//    @Override
//    public void onSliderClick(BaseSliderView slider) {
//    }
//
//    @Override
//    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//    }
//
//    @Override
//    public void onPageSelected(int position) {
//        Log.d("Slider Demo", "Page Changed: " + position);
//    }
//
//    @Override
//    public void onPageScrollStateChanged(int state) {
//    }
//
//    public void removeProduct(final ShoppingModel shoppingModel) {
//        loadingDialog.show();
//        JSONObject jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("mobile", session.getUserMobileNo());
//            jsonRequest.put("productId", shoppingModel.getPid());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        if (jsonRequest != null) {
//            Log.i("CARTREMOVEONEURL", ApiURLNew.URL_SHOPPONG_REMOVE_ITEM_CART);
//            Log.i("CARTREMOVEONEREQ", jsonRequest.toString());
//            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SHOPPONG_REMOVE_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject jsonObj) {
//                    try {
//                        Log.i("CARTREMOVEONERES", jsonObj.toString());
//                        String code = jsonObj.getString("code");
//
//                        loadingDialog.dismiss();
//                        if (code != null && code.equals("S00")) {
//                            pqCart.addRemoveProductsInCart(shoppingModel, shoppingModel.getpQty());
//                            Intent intent = new Intent("cart-clear");
//                            intent.putExtra("updates", "2");
//                            LocalBroadcastManager.getInstance(ProductDetailActivity.this).sendBroadcast(intent);
//                            addToCart.setText("Add");
//
//                        } else {
//                            loadingDialog.dismiss();
//                            CustomToast.showMessage(ProductDetailActivity.this, "Item cannot be removed");
//                        }
//
//                    } catch (JSONException e) {
//                        loadingDialog.dismiss();
//                        e.printStackTrace();
//                        Toast.makeText(ProductDetailActivity.this, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadingDialog.dismiss();
//                    error.printStackTrace();
//                    CustomToast.showMessage(ProductDetailActivity.this, NetworkErrorHandler.getMessage(error, ProductDetailActivity.this));
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 120000;
//
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//    }
//
//    public void addProduct(final ShoppingModel shoppingModel) {
//        loadingDialog.show();
//        JSONObject jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("mobile", session.getUserMobileNo());
//            jsonRequest.put("email", session.getUserEmail());
//            jsonRequest.put("firstName", session.getUserFirstName());
//            jsonRequest.put("lastName", session.getUserFirstName());
//            jsonRequest.put("country", "india");
//            jsonRequest.put("productId", shoppingModel.getPid());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        if (jsonRequest != null) {
//            Log.i("CARTADDONEURL", ApiURLNew.URL_SHOPPONG_ADD_ITEM_CART);
//            Log.i("CARTADDONEREQ", jsonRequest.toString());
//            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SHOPPONG_ADD_ITEM_CART, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject jsonObj) {
//                    try {
//                        Log.i("CARTADDONERES", jsonObj.toString());
//                        String code = jsonObj.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            pqCart.addRemoveProductsInCart(shoppingModel, shoppingModel.getpQty());
//                            Intent intent = new Intent("cart-clear");
//                            intent.putExtra("updates", "2");
//                            LocalBroadcastManager.getInstance(ProductDetailActivity.this).sendBroadcast(intent);
//                            addToCart.setText("Remove");
//                            loadingDialog.dismiss();
//                        } else {
//                            loadingDialog.dismiss();
//                            CustomToast.showMessage(ProductDetailActivity.this, "Item cannot be added");
//                        }
//
//                    } catch (JSONException e) {
//                        loadingDialog.dismiss();
//                        e.printStackTrace();
//                        Toast.makeText(ProductDetailActivity.this, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadingDialog.dismiss();
//                    error.printStackTrace();
//                    CustomToast.showMessage(ProductDetailActivity.this, NetworkErrorHandler.getMessage(error, ProductDetailActivity.this));
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 120000;
//
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//    }
//
//    @Override
//    public void taskCompleted(String response) {
//        if (response.equals("Add")) {
//            cartValue = String.valueOf(Integer.parseInt(cartValue) + 1);
//            tvcart_point.setText(cartValue);
//            shoppingAdapter.notifyDataSetChanged();
//        } else {
//            cartValue = String.valueOf(Integer.parseInt(cartValue) - 1);
//            shoppingAdapter.notifyDataSetChanged();
//            tvcart_point.setText(cartValue);
//        }
//    }
//}
