package com.Blockgration.Userwallet.activity.flightinneracitivty;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.Blockgration.adapter.DomesticCityListAdapter;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.model.DomesticFlightModel;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


/**
 * Created by Ksf on 10/6/2016.
 */
public class DomesticFlightSearchActivity extends AppCompatActivity {
    private ListView lvSearchFlightCity;
    private MaterialEditText etSearchFlightCity;
    private LoadingDialog loadingDialog;
    private ArrayList<DomesticFlightModel> flightCityModelList;
    private DomesticCityListAdapter domesticCityListAdapter;
    private FetchCityTask fetchCityTask;
    private String searchType;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        searchType = getIntent().getStringExtra("searchType");
        setContentView(R.layout.activity_flight_airport_search);
        //press back button in toolbar
        loadingDialog = new LoadingDialog(DomesticFlightSearchActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etSearchFlightCity = (MaterialEditText) findViewById(R.id.etSearchFlightCity);
        lvSearchFlightCity = (ListView) findViewById(R.id.lvSearchFlightCity);

        flightCityModelList = new ArrayList<>();
        domesticCityListAdapter = new DomesticCityListAdapter(getApplicationContext(), flightCityModelList);
        lvSearchFlightCity.setAdapter(domesticCityListAdapter);

        lvSearchFlightCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent("flight-search-complete");
                intent.putExtra("selectedCityModel", flightCityModelList.get(i));
                intent.putExtra("searchType", searchType);
                LocalBroadcastManager.getInstance(DomesticFlightSearchActivity.this).sendBroadcast(intent);
                loadingDialog.dismiss();
                finish();
            }
        });

        etSearchFlightCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if (cs.length() > 2) {
                    searchCity(String.valueOf(cs));
                } else {
                    flightCityModelList.clear();
                    domesticCityListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

    }

    private class FetchCityTask extends AsyncTask<String, Void, ArrayList<DomesticFlightModel>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog.show();
            flightCityModelList.clear();
        }

        @Override
        protected ArrayList<DomesticFlightModel> doInBackground(String... selectedCityString) {
            String mainJson = loadJSONFromAsset();
            try {
                JSONArray response = new JSONArray(mainJson);
                for (int i = 0; i < response.length(); i++) {
                    JSONObject c = response.getJSONObject(i);
                    String airportCode = c.getString("AirportCode");
                    String cityName = c.getString("City");
                    String countryName = c.getString("Country");
                    String airportDesc = c.getString("AirportDesc");
                    String airportType = c.getString("Type");

                    if ((cityName.toUpperCase()).contains(selectedCityString[0].toUpperCase()) || (airportCode.toUpperCase()).contains(selectedCityString[0].toUpperCase())) {
                        DomesticFlightModel busModel = new DomesticFlightModel(airportCode, cityName, countryName, airportDesc, airportType);
                        flightCityModelList.add(busModel);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return flightCityModelList;
        }

        @Override
        protected void onPostExecute(ArrayList<DomesticFlightModel> result) {
            loadingDialog.dismiss();
            if (result != null && result.size() != 0) {
                domesticCityListAdapter.notifyDataSetChanged();
            } else {
                flightCityModelList.clear();
                domesticCityListAdapter.notifyDataSetChanged();
            }

        }
    }

    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getAssets().open("airport.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void searchCity(String cityValue) {
        fetchCityTask = new FetchCityTask();
        fetchCityTask.execute(cityValue);
    }

    @Override
    protected void onDestroy() {
        if (fetchCityTask != null && !fetchCityTask.isCancelled()) {
            fetchCityTask.cancel(true);
        }
        super.onDestroy();

    }
}

