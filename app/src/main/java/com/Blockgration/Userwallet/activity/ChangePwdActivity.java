package com.Blockgration.Userwallet.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.Userwallet.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Ksf on 3/12/2016.
 */
public class ChangePwdActivity extends AppCompatActivity {

    public static final String EXTRA_EMAIL = "mobile";
    public static final String EXTRA_MSG = "message";
    private EditText etCPNewPwd;
    private EditText etCPReNewPwd;
    private Button btnChangePwd;

    private TextInputLayout ilChangePassword , ilChangeRePassword;
    private TextInputLayout ilOTP;
    private EditText etCPOTP;
    private TextView tvCPMessage;


    private String mobileNo, msg;

    private LoadingDialog loadDlg;
    private RequestQueue rq;
    private Toolbar tbChangePwd;

    private JSONObject jsonRequest;
    private ImageButton ivBackBtn;
    private Button btnResendOtp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerReceiver(broadcastReceiver, new IntentFilter("broadCastName"));

        setContentView(R.layout.activity_change_password);
        rq = Volley.newRequestQueue(getApplicationContext());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        etCPNewPwd = (EditText) findViewById(R.id.etCPNewPwd);
        etCPReNewPwd = (EditText) findViewById(R.id.etCPReNewPwd);
        etCPOTP = (EditText) findViewById(R.id.etCPOTP);
        ilChangePassword = (TextInputLayout) findViewById(R.id.ilChangePassword);
        ilChangeRePassword = (TextInputLayout) findViewById(R.id.ilChangeRePassword);
        tvCPMessage = (TextView) findViewById(R.id.tvCPMessage);
        ilOTP = (TextInputLayout) findViewById(R.id.ilOTP);
        btnChangePwd = (Button) findViewById(R.id.btnChangePwd);
        btnResendOtp = (Button) findViewById(R.id.btnResendOtp);

        //press back button in toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//        tbChangePwd.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

//        btnChangePwd.setTextColor(Color.parseColor("#ffffff"));

        mobileNo = getIntent().getStringExtra(ChangePwdActivity.EXTRA_EMAIL);
        msg = getIntent().getStringExtra(ChangePwdActivity.EXTRA_MSG);

//        tvCPMessage.setText("An OTP has send to " + mobileNo + ", Please check your Email.");
        tvCPMessage.setText(msg);

        loadDlg = new LoadingDialog(this);
        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        btnChangePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateOTP()) {
                    return;
                }
                if (!validatePassword()) {
                    return;
                }

                loadDlg.show();
                promoteChangePwd();
            }
        });

    }

    private boolean validatePassword() {
        if (etCPReNewPwd.getText().toString().length() < 6 && etCPNewPwd.getText().toString().length() < 6) {
            ilChangeRePassword.setError("Password Should be 6 digits");
            requestFocus(etCPNewPwd);
            return false;
        } else if (!etCPNewPwd.getText().toString().equals(etCPReNewPwd.getText().toString())) {
            ilChangePassword.setError("Password dint match");
            requestFocus(ilChangePassword);
            return false;
        } else {
            ilChangePassword.setErrorEnabled(false);

        }

        return true;
    }

    private boolean validateOTP() {
        if (etCPOTP.getText().toString().trim().isEmpty() || etCPOTP.getText().toString().length() < 6) {
            etCPOTP.setError("Enter valid OTP");
            requestFocus(etCPOTP);
            return false;
        } else {
            return true;

        }
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    //    private void promoteResendOtpPwd() {
//        jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("username", mobileNo);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        }
//
//        if (jsonRequest != null) {
//            Log.i("JsonRequest", jsonRequest.toString());
//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_CHANGE_PWD, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    try {
//                        String code = response.getString("code");
//                        System.out.println("RESPONSE" + response);
//                        String message = response.getString("message");
//                        if (code != null & code.equals("S00")) {
//                            loadDlg.dismiss();
//
//                        } else if (code != null & code.equals("F04")) {
//                            loadDlg.dismiss();
//                            Toast.makeText(getApplicationContext(), "Invalid OTP", Toast.LENGTH_SHORT).show();
//                        }
//                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
//                    } catch (JSONException e) {
//                        loadDlg.dismiss();
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
//                    error.printStackTrace();
//
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<String, String>();
//                    map.put("hash", "123456");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            rq.add(postReq);
//        }
//    }
    private void promoteChangePwd() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("username", mobileNo);
            jsonRequest.put("newPassword", etCPNewPwd.getText().toString());
            jsonRequest.put("confirmPassword", etCPReNewPwd.getText().toString());
            jsonRequest.put("key", etCPOTP.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_CHANGE_PWD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loadDlg.dismiss();
                    try {
                        String code = response.getString("code");
                        System.out.println("RESPONSE" + response);
                        String message = response.getString("message");
                        if (code != null & code.equals("S00")) {
                            Intent loginIntent = new Intent(ChangePwdActivity.this, LoginRegActivity.class);
                            startActivity(loginIntent);
                            finish();
                        } else if (code != null & code.equals("F04")) {
//                            Toast.makeText(getApplicationContext(), "Invalid OTP", Toast.LENGTH_SHORT).show();
                            CustomToast.showMessage(ChangePwdActivity.this, message);
                        }
//                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123456");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            rq.add(postReq);
        }
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            String message = b.getString("message");
            etCPOTP.setText(message);
            unregisterReceiver(broadcastReceiver);

        }
    };

}
