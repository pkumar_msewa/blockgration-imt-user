package com.Blockgration.Userwallet.activity.shopping;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.shopping.fragment.ShopProdListFragment;
import com.Blockgration.model.CategoryListModel;
import com.Blockgration.model.UserModel;


import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by Kashif-PC on 2/21/2017.
 */
public class CatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CategoryListModel> shoppingArray;
    //    private Context context;
    private AppCompatActivity act;
    private LayoutInflater layoutInflater;
    private RecyclerView.ViewHolder viewHolder;
    private PQCart pqCart = PQCart.getInstance();
    private RequestQueue rq;
    private UserModel session = UserModel.getInstance();
    private AddRemoveCartListner adpListner;
    private Context context;
    private JsonObjectRequest postReq;
    private String tag_json_obj = "json_events";
    private LoadingDialog loadingDialog;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    public static final int header = 0;
    public static final int Normal = 1;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private int[] IMAGES;
    private FragmentManager supportFragmentManager;


    public CatAdapter(Context context, List<CategoryListModel> shoppingArray, FragmentManager supportFragmentManager) {
        this.shoppingArray = shoppingArray;
        this.context = context;
        this.supportFragmentManager = supportFragmentManager;
        loadingDialog = new LoadingDialog(context);

        try {
            rq = Volley.newRequestQueue(context);
            layoutInflater = LayoutInflater.from(context);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_shopping, parent, false);
            //inflate your layout and pass it to view holder
            return new VHItem(layoutView);
        } else if (viewType == TYPE_HEADER) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_shopping_homepage, parent, false);
            //inflate your layout and pass it to view holder
            return new VHHeader(layoutView);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

    }

//    @Override
//    public void onBindViewHolder(final RecyclerViewHolders.CatAdapter holder, final int currentPostion) {
//        viewHolder = holder;
//        viewHolder.tvShoppingPrice.setText("Rs. " + shoppingArray.get(currentPostion).getpPrice());
//        viewHolder.tvShoppingName.setText(shoppingArray.get(currentPostion).getpName());
//        viewHolder.tvShoppingBrand.setText(shoppingArray.get(currentPostion).getpBrand());
//        AQuery aQuery = new AQuery(context);
//        aQuery.id(viewHolder.ivShoppingItems).image(shoppingArray.get(currentPostion).getpImage()).background(R.drawable.loading_image).getContext();
////        Picasso.with(act).load(shoppingArray.get(currentPostion).getpImage()).placeholder(R.drawable.loading_image).into(viewHolder.ivShoppingItems);
//
//        holder.cvShoppingItem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent detailIntent = new Intent(act, ProductDetailActivity.class);
//                try {
//
//                    ShoppingModel shoppingModel = shoppingArray.get(currentPostion);
//                    detailIntent.putExtra("ShoppingData", shoppingModel);
//                    detailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    act.startActivity(detailIntent);
////            BillPaymentFragment fragment = new BillPaymentFragment();
//    FragmentTransaction fragmentTransaction = context.gef().beginTransaction();
//            fragmentTransaction.replace(R.id.frameInCart,fragment);
//            fragmentTransaction.commit();
//                } catch (IndexOutOfBoundsException e) {
//                    e.printStackTrace();
//                }
//            }
//        });


    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof VHItem) {

            ((VHItem) holder).tvShoppingPrice.setVisibility(View.GONE);
            ((VHItem) holder).tvShoppingName.setText(shoppingArray.get(position - 1).getcategoryName());
            ((VHItem) holder).tvShoppingBrand.setVisibility(View.GONE);
            AQuery aQuery = new AQuery(context);
            Log.i("IMAGEURL", shoppingArray.get(position - 1).getGetCategoryImage());
            aQuery.id(((VHItem) holder).ivShoppingItems).image(shoppingArray.get(position - 1).getGetCategoryImage()).background(R.drawable.ic_loading_image).getContext();
//        Picasso.with(act).load(shoppingArray.get(currentPostion).getpImage()).placeholder(R.drawable.loading_image).into(viewHolder.ivShoppingItems);

            ((VHItem) holder).cvShoppingItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ShopProdListFragment shopProdListFragment = new ShopProdListFragment();
                        Bundle bundle = new Bundle();
                        Log.i("CATEGORYID",String.valueOf(shoppingArray.get(position-1).getcategoryCode()));
                        bundle.putString("CATID", String.valueOf(shoppingArray.get(position-1).getcategoryCode()));
                        shopProdListFragment.setArguments(bundle);
                        supportFragmentManager.beginTransaction().replace(R.id.frameInShop,shopProdListFragment).addToBackStack("details").commit();
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
            });
            //cast holder to VHItem and set data
        } else if (holder instanceof VHHeader) {
            //cast holder to VHHeader and set data for header.

//            HashMap<String, String> file_maps = new HashMap<>();
//            file_maps.put(shoppingArray.get(0).getpName(), shoppingArray.get(0).getpImage());
//            file_maps.put(shoppingArray.get(1).getpName(), shoppingArray.get(1).getpImage());
//            file_maps.put(shoppingArray.get(2).getpName(), shoppingArray.get(2).getpImage());
//
//            for (String name : file_maps.keySet()) {
//                DefaultSliderView textSliderView = new DefaultSliderView(context);
//                // initialize a SliderLayout
//                textSliderView.image(file_maps.get(name))
//                        .setScaleType()
//                        .setOnSliderClickListener((BaseSliderView.OnSliderClickListener) context);
//
//                //add your extra information
//                textSliderView.bundle(new Bundle());
//                textSliderView.getBundle()
//                        .putString("extra", name);
//
////                ((VHHeader) holder).productHeaderImageSlider.addSlider(textSliderView);
//            }
//
////            ((VHHeader) holder).productHeaderImageSlider.setPresetTransformer(SliderLayout.Transformer.RotateDown);
////            ((VHHeader) holder).productHeaderImageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
////            ((VHHeader) holder).productHeaderImageSlider.setCustomIndicator(((VHHeader) holder).custom_indicator);
////        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
////        mDemoSlider.setDuration(4000);
////            ((VHHeader) holder).productHeaderImageSlider.addOnPageChangeListener((ViewPagerEx.OnPageChangeListener) context);
            final VHHeader holders = (VHHeader) holder;
            IMAGES = new int[]{R.drawable.img_slider1, R.drawable.img_slider2, R.drawable.img_slider3};
            holders.mPager.setAdapter(new HomeSliderAdapter(context, IMAGES));

            holders.indicator.setViewPager(holders.mPager);

            final float density = context.getResources().getDisplayMetrics().density;

            holders.indicator.setRadius(5 * density);

            NUM_PAGES = IMAGES.length;

            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    holders.mPager.setCurrentItem(currentPage++, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 3000, 3000);

            // Pager listener over indicator
            holders.indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;

                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int pos) {

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.shoppingArray.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }


    class VHItem extends RecyclerView.ViewHolder {
        ImageView ivShoppingItems;
        TextView tvShoppingPrice, tvShoppingName, tvShoppingBrand;
        CardView cvShoppingItem;

        public VHItem(View convertView) {
            super(convertView);
            ivShoppingItems = (ImageView) convertView.findViewById(R.id.ivShoppingItems);
            tvShoppingPrice = (TextView) convertView.findViewById(R.id.tvShoppingPrice);
            tvShoppingName = (TextView) convertView.findViewById(R.id.tvShoppingName);
            tvShoppingBrand = (TextView) convertView.findViewById(R.id.tvShoppingBrand);
            cvShoppingItem = (CardView) convertView.findViewById(R.id.cvShoppingItem);
        }
    }

    public class VHHeader extends RecyclerView.ViewHolder {
        public TextView tvMenuItems;
        public ImageView ivMenuItems;
        public ViewPager mPager;
        public CirclePageIndicator indicator;


        public VHHeader(View itemView) {
            super(itemView);
            mPager = (ViewPager) itemView.findViewById(R.id.pagerHome);

            indicator = (CirclePageIndicator) itemView.findViewById(R.id.productHeaderImageSlider);

        }
    }


//    private String getItem(int position) {
//        return data[position - 1];
//    }
//    @Override
//    public int getItemCount() {
//        return this.shoppingArray.size();
//    }

//    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
//        ImageView ivShoppingItems;
//        TextView tvShoppingPrice, tvShoppingName, tvShoppingBrand;
//        CardView cvShoppingItem;
//
//        public RecyclerViewHolders(View convertView) {
//            super(convertView);
//            ivShoppingItems = (ImageView) convertView.findViewById(R.id.ivShoppingItems);
//            tvShoppingPrice = (TextView) convertView.findViewById(R.id.tvShoppingPrice);
//            tvShoppingName = (TextView) convertView.findViewById(R.id.tvShoppingName);
//            tvShoppingBrand = (TextView) convertView.findViewById(R.id.tvShoppingBrand);
//            cvShoppingItem = (CardView) convertView.findViewById(R.id.cvShoppingItem);
////            btnShoppingDetails = (Button) convertView.findViewById(R.id.btnShoppingDetails);
////            btnShoppingRemove = (Button) convertView.findViewById(R.id.btnShoppingRemove);
//
//
//        }
//
//
//    }


    public boolean isPositionHeader(int position) {
        return position == 0;
    }

}
