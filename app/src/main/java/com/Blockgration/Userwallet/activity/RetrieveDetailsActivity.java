package com.Blockgration.Userwallet.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonIOException;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.util.NetworkErrorHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RetrieveDetailsActivity extends AppCompatActivity {

    private LoadingDialog loadDlg;
    private String rrn;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrieve_details);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        loadDlg = new LoadingDialog(RetrieveDetailsActivity.this);
        rrn = getIntent().getExtras().getString("rrn");
        loadDlg.show();
        retrieveDetails(rrn);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.Blockgration.Userwallet.R.menu.menu_main, menu);
        return true;
    }

    private void retrieveDetails(String r) {
        Log.i("Retrieve URL", ApiURLNew.URL_RETRIEVE_DETAILS + r);
        StringRequest req = new StringRequest(Request.Method.GET, ApiURLNew.URL_RETRIEVE_DETAILS + r, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("Retrieve Details", s);
                loadDlg.dismiss();
                try {
                    JSONObject obj = new JSONObject(s);
                    int status_code = obj.getInt("status_code");
                    if (status_code != 0 && status_code == 1000) {
                        JSONObject results = obj.getJSONObject("results");
                        JSONObject transaction = results.getJSONObject("transaction");
                        String tracking_number = transaction.getString("tracking_number");
                        String remitting_amount = transaction.getString("remitting_amount");
                        String service_charge = transaction.getString("service_charge");
                        String payout_amount = transaction.getString("payout_amount");
                        String exchange_rate = transaction.getString("exchange_rate");
                    }
                } catch (JSONException e) {
                    loadDlg.dismiss();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                loadDlg.dismiss();
                CustomToast.showMessage(RetrieveDetailsActivity.this, NetworkErrorHandler.getMessage(volleyError, RetrieveDetailsActivity.this));
                volleyError.printStackTrace();
            }
        });
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        PayQwikApp.getInstance().addToRequestQueue(req);
    }

}
