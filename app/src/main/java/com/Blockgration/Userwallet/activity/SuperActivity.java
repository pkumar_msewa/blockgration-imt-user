package com.Blockgration.Userwallet.activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.Blockgration.Userwallet.R;

/**
 * Created by Ksf on 8/12/2016.
 */
public class SuperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {


        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean("preSwitchTheme",false)){
            setTheme(R.style.AppThemeLight);
        }else{
            setTheme(R.style.AppThemeDark);
        }
        super.onCreate(savedInstanceState);

    }


}
