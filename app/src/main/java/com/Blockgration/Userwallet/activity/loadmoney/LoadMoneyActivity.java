package com.Blockgration.Userwallet.activity.loadmoney;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.Blockgration.Userwallet.activity.MainMenuDetailActivity;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomLoadMoneyDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.fragment.CustomDisclaimerDialog;
import com.Blockgration.fragment.billpayment.TravelHealthFragment;
import com.Blockgration.fragment.mainmenufragment.WebViewFragment;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.BanksListModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.NetworkErrorHandler;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONString;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ksf on 10/28/2016.
 */
public class LoadMoneyActivity extends AppCompatActivity {
    private UserModel session = UserModel.getInstance();
    private LoadingDialog loadDlg;
    private boolean isVBank = true;
    private String inValidMessage = "";
    private String tag_json_obj = "load_money";
    private double amount;
    private String URL_VERIFY_TRANSACTION;
    private RequestQueue requestQueue;
    private Button submit, btnAmountOne, btnAmountTwo, btnAmountThree, btnAmountFoure;
    private Toolbar toolbar;
    private RadioGroup radioGroup;
    private RadioButton rbAchPse;
    private RadioButton rbVoucher;
    private String paymentValue;
    private EditText etAmount;
    private MaterialEditText etACHbanklist, etACHAmount;
    private LinearLayout layoutACH;
    private List<BanksListModel> banklistArray;
    String bank_code, bank_name;
    private String orderId;
    private boolean cancel;
    private View focusView = null;

    private String voucherCode, bankName;
    private JSONObject jsonRequest;
    String trazabilityCode;
    String amt;

    ScrollView scrollView;
    FrameLayout frameLayout;
    private AlertDialog alertDialog;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_load);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        hideSoftKeyboard();
        loadDlg = new LoadingDialog(LoadMoneyActivity.this);
        orderId = String.valueOf(System.currentTimeMillis());

        requestQueue = Volley.newRequestQueue(LoadMoneyActivity.this);
        etAmount = (EditText) findViewById(R.id.etAmount);
        banklistArray = new ArrayList<>();

        etACHAmount = (MaterialEditText) findViewById(R.id.etACHAmount);
        submit = (Button) findViewById(R.id.submit);
//        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        rbAchPse = (RadioButton) findViewById(R.id.rbAchPse);
        rbVoucher = (RadioButton) findViewById(R.id.rbVoucher);

        layoutACH = (LinearLayout) findViewById(R.id.layoutACH);

        etAmount.setVisibility(View.GONE);
        layoutACH.setVisibility(View.GONE);
//        submit.setVisibility(View.GONE);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        frameLayout = (FrameLayout) findViewById(R.id.achContainer);

        if (rbAchPse.isChecked()) {
            paymentValue = "";
            paymentValue = "achpse";
            etACHAmount.setVisibility(View.VISIBLE);
        }

        rbVoucher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    Log.i("value", String.valueOf(b));
                    paymentValue = "";
                    paymentValue = "voucher";
                    rbAchPse.setChecked(false);
                    etACHAmount.setVisibility(View.GONE);
                }
            }
        });

        rbAchPse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    paymentValue = "";
                    paymentValue = "achpse";
                    rbVoucher.setChecked(false);
                    etACHAmount.setVisibility(View.VISIBLE);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (paymentValue.equals("voucher")) {
                    showSortingDialog();
                } else if (paymentValue.equals("achpse")) {
                    if (!validateAmount()) {
                        return;
                    } else {
                        amt = etACHAmount.getText().toString();
                        getBanksList();
                    }
                }
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    private void getBanksList() {
        try {
            JSONObject resp = new JSONObject(loadBanksListFromAsset());
            JSONArray banksArray = resp.getJSONArray("banks");
            for (int i = 0; i < banksArray.length(); i++) {
                JSONObject banksObj = banksArray.getJSONObject(i);
                String bankCode = banksObj.getString("bankCode");
                String bankName = banksObj.getString("bankName");
                BanksListModel bankListModel = new BanksListModel(bankCode, bankName);
                banklistArray.add(bankListModel);
            }
            showSortingDialog();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*private void getBanksList() {
        loadDlg.show();
        StringRequest postReq = new StringRequest(Request.Method.POST, ApiURLNew.URL_BANK_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String resp) {
                loadDlg.dismiss();
                try {
                    Log.i("BanksList Response", resp.toString());
                    Log.i("BanksList URL", ApiURLNew.URL_BANK_LIST);
                    JSONObject obj = new JSONObject(resp);
                    boolean success = obj.getBoolean("success");
                    if (success) {
                        JSONArray banksArray = obj.getJSONArray("banks");
                        for (int i = 0; i < banksArray.length(); i++) {
                            JSONObject banksObj = banksArray.getJSONObject(i);
                            String bankCode = banksObj.getString("bankCode");
                            String bankName = banksObj.getString("bankName");
                            BanksListModel bankListModel = new BanksListModel(bankCode, bankName);
                            banklistArray.add(bankListModel);
                        }
                        if (banklistArray.size() != 0) {
                            showSortingDialog();
                        } else {
                            CustomToast.showMessage(LoadMoneyActivity.this, "Please try again");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                loadDlg.dismiss();
                CustomToast.showMessage(LoadMoneyActivity.this, NetworkErrorHandler.getMessage(volleyError, LoadMoneyActivity.this));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("hash", "123");
                map.put("Content-Type", "application/x-www-form-urlencoded");
                return map;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("sessionId", session.getUserSessionId());
                return map;
            }
        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);
//             Adding request to request queue
        requestQueue.add(postReq);
    }*/

    private void showBankListDialog() {

        final ArrayList<String> banklistVal = new ArrayList<>();

        for (BanksListModel banklist : banklistArray) {
            banklistVal.add(banklist.getBankName());
        }

        AlertDialog.Builder banklistDialog =
                new AlertDialog.Builder(LoadMoneyActivity.this, R.style.AppCompatAlertDialogStyle);

        banklistDialog.setTitle("Select Bank");
        banklistDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        banklistDialog.setItems(banklistVal.toArray(new String[banklistVal.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                etACHbanklist.setText(banklistVal.get(i));
                bank_name = banklistArray.get(i).getBankName();
                bank_code = banklistArray.get(i).getBankCode();
                Log.i("bank_name", bank_name);
                Log.i("bank_code", bank_code);
            }
        });

        banklistDialog.show();

    }


    private void hideSoftKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusedView = LoadMoneyActivity.this.getCurrentFocus();
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    private void sendRefresh() {
        finish();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(LoadMoneyActivity.this).sendBroadcast(intent);
    }

    private void sendLogout() {
        finish();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(LoadMoneyActivity.this).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(LoadMoneyActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void submitForm() {
        etAmount.setError(null);
        cancel = false;
        if (!validateVoucher()) {
            CustomToast.showMessage(LoadMoneyActivity.this, "voucher not valid");
            return;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            loadDlg.show();
//            hideSoftKeyboard();
            redeemVoucher();
            Log.i("PaymentValue", paymentValue);
        }
    }

    private void submitACH() {
        cancel = false;
        if (!validateBank()) {
            return;
        }
//        if (!validateAmount()) {
//            return;
//        }
        else {
            showCustomDisclaimerDialog();
            Log.i("PaymentValue", paymentValue);
        }
    }


    public String loadBanksListFromAsset() {
        String json;
        try {
            InputStream is = getAssets().open("banksList.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public String loadACHResponseFromAsset() {
        String json;
        try {
            InputStream is = getAssets().open("achLoadMoney.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void loadACH() {
        loadDlg.dismiss();
        try {
            JSONObject obj = new JSONObject(loadACHResponseFromAsset());
            Log.i("LoadACH URL", ApiURLNew.URL_PSE_PROCESS);
            boolean success = obj.getBoolean("success");
            trazabilityCode = obj.getString("trazabilityCode");
            String returnCode = obj.getString("returnCode");
            String bankUrl = obj.getString("bankUrl");
            String trasactionCycle = obj.getString("trasactionCycle");
            String message = obj.getString("message");
            if (success && returnCode.equals("SUCCESS")) {
                scrollView.setVisibility(View.GONE);
                alertDialog.dismiss();
                Intent menuIntent = new Intent(LoadMoneyActivity.this, MainMenuDetailActivity.class);
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "ACH");
                menuIntent.putExtra("URL", bankUrl);
                menuIntent.putExtra("TrazCode", trazabilityCode);
                startActivity(menuIntent);
                finish();
            } else {
                CustomToast.showMessage(LoadMoneyActivity.this, "No value found");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    private void loadACH() {
//        StringRequest postReq = new StringRequest(Request.Method.POST, ApiURLNew.URL_PSE_PROCESS, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String resp) {
//                loadDlg.dismiss();
//                try {
//                    JSONObject obj = new JSONObject(resp);
//                    Log.i("LoadACH Response", resp.toString());
//                    Log.i("LoadACH URL", ApiURLNew.URL_PSE_PROCESS);
//                    boolean success = obj.getBoolean("success");
//                    trazabilityCode = obj.getString("trazabilityCode");
//                    String returnCode = obj.getString("returnCode");
//                    String bankUrl = obj.getString("bankUrl");
//                    String trasactionCycle = obj.getString("trasactionCycle");
//                    String message = obj.getString("message");
//                    if (success && returnCode.equals("SUCCESS")) {
//                        scrollView.setVisibility(View.GONE);
////                        frameLayout.setVisibility(View.VISIBLE);
//                        alertDialog.dismiss();
//                        Intent menuIntent = new Intent(LoadMoneyActivity.this, MainMenuDetailActivity.class);
//                        menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "ACH");
//                        menuIntent.putExtra("URL", bankUrl);
//                        menuIntent.putExtra("TrazCode", trazabilityCode);
//                        startActivity(menuIntent);
//                        finish();
//                    } else {
//                        CustomToast.showMessage(LoadMoneyActivity.this, "No value found");
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                loadDlg.dismiss();
//                alertDialog.dismiss();
//                CustomToast.showMessage(LoadMoneyActivity.this, NetworkErrorHandler.getMessage(volleyError, LoadMoneyActivity.this));
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> map = new HashMap<String, String>();
//                map.put("hash", "123");
//                map.put("Content-Type", "application/json");
//                return map;
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> map = new HashMap<>();
//                map.put("sessionId", session.getUserSessionId());
//                map.put("amount", amt);
//                map.put("bankCode", bank_code);
//                map.put("bankName", bank_name);
//                Log.i("MAP", map.toString());
//                return map;
//            }
//        };
//        int socketTimeout = 60000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        postReq.setRetryPolicy(policy);
////             Adding request to request queue
//        requestQueue.add(postReq);
//
//    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validateVoucher() {
        if (etAmount.getText().toString() == null || etAmount.getText().toString().trim().isEmpty() || etAmount.getText().length() < 16) {
            etAmount.setError("Enter 16 digit code ");
            focusView = etAmount;
            requestFocus(etAmount);
            cancel = true;
            return false;
        }
        return true;
    }

    private boolean validateBank() {
        if (etACHbanklist.getText().toString().isEmpty()) {
            etACHbanklist.setError("Select your bank");
            requestFocus(etACHbanklist);
            return false;
        }
        return true;
    }

    private boolean validateAmount() {
        if (etACHAmount.getText().toString().isEmpty() || etACHAmount.getText().toString() == null ||
                Integer.parseInt(etACHAmount.getText().toString()) < 10) {
            etACHAmount.setError("Enter min. amount 10");
            requestFocus(etACHAmount);
            return false;
        }
        return true;
    }


    public void redeemVoucher() {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("voucherNumber", voucherCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("LMV JsonRequest", jsonRequest.toString());
            Log.i("LMV URL", ApiURLNew.URL_REEDEM_VOUCHER);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_REEDEM_VOUCHER, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("LMV JsonResponse", response.toString());
                    try {
                        loadDlg.dismiss();
                        String message = response.getString("message");
                        String code = response.getString("code");
                        if (code != null & code.equals("S00")) {
                            alertDialog.dismiss();
                            String amount = response.getString("amount");
                            String msg = null;
                            if (!amount.equals("null")) {
                                msg = amount + " added to your account.";
                            } else {
                                msg = "Money added successfully to your account.";
                            }
//                        response
                            CustomAlertDialog customAlertDialog = new CustomAlertDialog(LoadMoneyActivity.this, R.string.load_success, msg);
                            customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    sendRefresh();
                                }
                            });
                            customAlertDialog.setCancelable(false);
                            customAlertDialog.show();
//                            CustomToast.showMessage(LoadMoneyActivity.this, message);
                        } else if (code != null & code.equals("F03")) {
                            showInvalidSessionDialog();
                        } else if (code != null & code.equals("F00")) {
                            CustomToast.showMessage(LoadMoneyActivity.this, message);
                        } else if (code != null & code.equals("F02")) {
                            CustomToast.showMessage(LoadMoneyActivity.this, message);
                        } else if (code != null & code.equals("F04")) {
//                            dialog.show();
                            CustomToast.showMessage(LoadMoneyActivity.this, message);
                        } else if (code != null & code.equals("F05")) {
                            CustomToast.showMessage(LoadMoneyActivity.this, message);
                        } else if (code != null & code.equals("F06")) {
                            CustomToast.showMessage(LoadMoneyActivity.this, message);
                        } else {
                            CustomToast.showMessage(LoadMoneyActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadDlg.dismiss();
                        CustomToast.showMessage(LoadMoneyActivity.this, "Error connecting to server");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(LoadMoneyActivity.this, NetworkErrorHandler.getMessage(volleyError, LoadMoneyActivity.this));
                    volleyError.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjectRequest.setRetryPolicy(policy);
            // Adding request to request queue
            PayQwikApp.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
        }
    }


    public void showSortingDialog() {
        LayoutInflater myLayout = LayoutInflater.from(LoadMoneyActivity.this);

        Button cancell, pay;

        View dialogView = myLayout.inflate(R.layout.dialog_custom_load_money, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoadMoneyActivity.this);

        alertDialogBuilder.setView(dialogView);
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);

        TextView titleTextView = (TextView) dialogView.findViewById(R.id.cadTiltle);
        titleTextView.setText(R.string.dialog_title2);

        etAmount = (MaterialEditText) dialogView.findViewById(R.id.etAmount);
        etACHbanklist = (MaterialEditText) dialogView.findViewById(R.id.etACHbanklist);
        cancell = (Button) dialogView.findViewById(R.id.cancel);
        pay = (Button) dialogView.findViewById(R.id.submit);

        etAmount.setVisibility(View.GONE);
        etACHbanklist.setVisibility(View.GONE);

        if (paymentValue.equals("voucher")) {
            etAmount.setVisibility(View.VISIBLE);
            etACHbanklist.setVisibility(View.GONE);
        } else if (paymentValue.equals("achpse")) {
            etAmount.setVisibility(View.GONE);
            etACHbanklist.setVisibility(View.VISIBLE);
            etACHbanklist.setFocusable(false);
            pay.setText("NEXT");
        }

        etACHbanklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBankListDialog();
            }
        });

//        Button pay = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (paymentValue.equals("voucher")) {
                    Log.i("PaymentValue", paymentValue);
                    voucherCode = etAmount.getText().toString();
                    submitForm();
                } else if (paymentValue.equals("achpse")) {
                    bankName = etACHbanklist.getText().toString();
                    Log.i("PaymentValue", paymentValue);
                    submitACH();
//                    showCustomDisclaimerDialog();
                }
//                CustomToast.showMessage(LoadMoneyActivity.this, "PAYING......");
            }
        });

        cancell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }

    public void showCustomDisclaimerDialog() {
        CustomDisclaimerDialog builder = new CustomDisclaimerDialog(LoadMoneyActivity.this);
        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                loadACH();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                alertDialog.dismiss();
            }
        });
        builder.show();
    }


    @Override
    public void onBackPressed() {
        loadDlg.dismiss();
        super.onBackPressed();
    }
}