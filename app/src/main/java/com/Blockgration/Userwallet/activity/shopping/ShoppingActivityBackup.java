package com.Blockgration.Userwallet.activity.shopping;//package in.msewa.vpayqwiktest.activity.shopping;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.graphics.Bitmap;
//import android.graphics.Rect;
//import android.graphics.drawable.BitmapDrawable;
//import android.graphics.drawable.Drawable;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.content.LocalBroadcastManager;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.GridLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.HurlStack;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.Volley;
//import com.readystatesoftware.viewbadger.BadgeView;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.security.KeyManagementException;
//import java.security.NoSuchAlgorithmException;
//import java.util.ArrayList;
//import java.util.Comparator;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.net.ssl.SSLSocketFactory;
//
//import in.msewa.custom.BaseSliderView;
//import in.msewa.custom.CustomToast;
//import in.msewa.custom.LoadingDialog;
//import in.msewa.custom.ViewPagerEx;
//import in.msewa.metadata.ApiUrl;
//import in.msewa.model.CategoryListModel;
//import in.msewa.model.PQCart;
//import in.msewa.model.ShoppingModel;
//import in.msewa.model.UserModel;
//import in.msewa.util.AddRemoveCartListner;
//import in.msewa.util.NetworkErrorHandler;
//import in.msewa.util.TLSSocketFactory;
//import in.msewa.vpayqwiktest.PayQwikApplication;
//import in.msewa.vpayqwiktest.R;
//import in.msewa.vpayqwiktest.activity.LoginRegActivity;
//import in.msewa.vpayqwiktest.activity.shopping.filter.FiltterActivity;
//
///**
// * Created by Ksf on 4/29/2016.
// */
//public class ShoppingActivityBackup extends AppCompatActivity implements AddRemoveCartListner, View.OnClickListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
//
//    private RecyclerView rvShopping;
//    private TextView tvcart_point;
//
//    private List<CategoryListModel> categoryArray;
//    private RequestQueue rq;
//    private String cartValue = "0";
//    private PQCart pqCart = PQCart.getInstance();
//    private CatAdapter catAdapter;
//    private UserModel session = UserModel.getInstance();
//    private Button btnGoCart;
//
//    private LoadingDialog loadDlg;
//    private Toolbar toolbar;
//    private TextView itemOne;
//    private TextView itemTwo;
//    private TextView itemThree;
//    private TextView itemFive;
//    private TextView itemFour;
//    private ImageView ivclose;
//    private Button submit;
//    private View view1, view2;
//    private static String selectValue;
//    private JsonObjectRequest postReq;
//    private String tag_json_obj = "json_events";
//    private JSONObject jsonRequest;
//    private String action;
//    private ImageView ivFab;
//    private BadgeView badge;
//
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        view1 = getLayoutInflater().inflate(R.layout.activity_shopping, null);
//        view2 = getLayoutInflater().inflate(R.layout.shoppingpoplate, null);
//        setContentView(view1);
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        loadDlg = new LoadingDialog(ShoppingActivityBackup.this);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                sendRefresh();
//                onBackPressed();
//            }
//        });
//        try {
//            final SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
//            rq = Volley.newRequestQueue(getApplicationContext(), new HurlStack(null, sslSocketFactory));
//        } catch (KeyManagementException | NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//
//        ivFab = (ImageView)  findViewById(R.id.ivFab);
//
////        shoppingArray = Select.from(CategoryListModel.class).list();
//
//        rvShopping = (RecyclerView) findViewById(R.id.rvShopping);
//        rvShopping.addItemDecoration(new SpacesItemDecoration(8));
//        final GridLayoutManager manager = new GridLayoutManager(ShoppingActivityBackup.this, 3);
//        rvShopping.setLayoutManager(manager);
//        rvShopping.setHasFixedSize(true);
//        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(int position) {
//                return catAdapter.isPositionHeader(position) ? manager.getSpanCount() : 1;
//            }
//        });
//
////        rvShopping.setAdapter(new ShopAdapter());
////        btnGoCart = (Button) findViewById(R.id.btnGoCart);
//        categoryArray = new ArrayList<>();
////        tvcart_point = (TextView) findViewById(R.id.tvshopping_cart_value);
//        View target = findViewById(R.id.ivFab);
//        badge = new BadgeView(this, target);
//
//        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("cart-clear"));
////        LocalBroadcastManager.getInstance(this).registerReceiver(fMessageReceiver, new IntentFilter("filter-activated"));
////        if (shoppingArray != null && shoppingArray.size() != 0) {
////            Collections.sort(shoppingArray, new compPopulation());
////            shoppingAdapter = new ShopAdapter(ShoppingActivity.this, shoppingArray, ShoppingActivity.this, ShoppingActivity.this);
////            rvShopping.setAdapter(shoppingAdapter);
////        } else {
////            getShoppingItems("all", "", "", "");
////
////        }
//        getCatList();
//
//
//        ivFab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (session.getIsValid() == 1) {
//                    if (cartValue.equals("0")) {
//                        CustomToast.showMessage(getApplicationContext(), "You have no products in cart.");
//                    } else {
//                        Intent goToCartIntent = new Intent(ShoppingActivityBackup.this, TeleBuyPaymentActivity.class);
//                        startActivity(goToCartIntent);
//                    }
//                } else {
//                    Intent loginIntent = new Intent(getApplicationContext(), LoginRegActivity.class);
//                    startActivity(loginIntent);
//                }
//            }
//        });
//
//
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_order_filter) {
//            setContentView(view2);
//            itemOne = (TextView) findViewById(R.id.itemOne);
//            itemTwo = (TextView) findViewById(R.id.itemTwo);
//            itemThree = (TextView) findViewById(R.id.itemThree);
//            itemFive = (TextView) findViewById(R.id.itemFive);
//            itemFour = (TextView) findViewById(R.id.itemFour);
//            ivclose = (ImageView) findViewById(R.id.ivclose);
//            submit = (Button) findViewById(R.id.submit);
//            itemOne.setOnClickListener(this);
//            itemTwo.setOnClickListener(this);
//            itemThree.setOnClickListener(this);
//            itemFive.setOnClickListener(this);
//            itemFour.setOnClickListener(this);
//            ivclose.setColorFilter(getResources().getColor(R.color.colorPrimary));
//            ivclose.setOnClickListener(this);
//            submit.setOnClickListener(this);
//
//            return true;
//        } else if (id == R.id.action_filter) {
//            startActivityForResult(new Intent(ShoppingActivityBackup.this, FiltterActivity.class), 100);
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//    }
//
////    public void getShoppingItems(String type, String catId, String subCatId, String brandID) {
////        loadDlg.show();
////        jsonRequest = new JSONObject();
////        try {
////            if (type.equals("SUBCAT")) {
////                jsonRequest.put("categoryId", catId);
////                jsonRequest.put("subCategoryId", subCatId);
////            } else if (type.equals("BRAND")) {
////                jsonRequest.put("brandId", brandID);
////            }
////            jsonRequest.put("mobile", session.getUserMobileNo());
////            jsonRequest.put("country", "india");
////
////
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
////
////        if (jsonRequest != null) {
////            Log.i("SHOPPINGURL", ApiUrl.URL_GET_ALL_SHOPPING_PRODUCTS);
////            Log.i("SHOPPINGREQ", jsonRequest.toString());
////            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GET_ALL_SHOPPING_PRODUCTS, jsonRequest, new Response.Listener<JSONObject>() {
////                @Override
////                public void onResponse(JSONObject jsonObj) {
////                    loadDlg.dismiss();
////                    try {
////                        Log.i("SHOPPINGRES", jsonObj.toString());
////                        String code = jsonObj.getString("code");
////
////                        if (code != null && code.equals("S00")) {
////                            try {
////                                if (!jsonObj.isNull("response")) {
////                                    shoppingArray.clear();
////                                    ShoppingModel.deleteAll(ShoppingModel.class);
////
////                                    JSONArray productJArray = jsonObj.getJSONArray("response");
////                                    if (productJArray.length() != 0) {
////
////                                        for (int i = 0; i < productJArray.length(); i++) {
////                                            JSONObject c = productJArray.getJSONObject(i);
////                                            long pId = c.getLong("id");
////                                            String pImage = c.getString("productImage");
////                                            String pName = c.getString("productName");
////                                            String pDesc = c.getString("description");
////                                            long pPrice = c.getLong("unitPrice");
//////                                String pStatus;
//////                                if (!c.getString("status").isEmpty()) {
//////                                    pStatus = c.getString("status");
//////                                } else {
//////                                    pStatus = "no status";
//////                                }
////                                            long pWeight = c.getLong("weight");
////                                            String pCat = c.getString("category");
////                                            String pSubCat = c.getString("subCategory");
////                                            String pBrand = c.getString("brand");
////                                            ShoppingModel sModel = new ShoppingModel(pId, pName, pDesc, pPrice, "http://www.qwikrpay.com" + pImage, "success", pWeight, pCat, pSubCat, pBrand, 0);
////                                            sModel.save();
////                                            shoppingArray.add((sModel));
////
////                                        }
////                                        if (shoppingArray.size() != 0) {
////                                            Collections.sort(shoppingArray, new compPopulation());
////                                            shoppingAdapter = new ShopAdapter(ShoppingActivity.this, shoppingArray, ShoppingActivity.this, ShoppingActivity.this);
////                                            rvShopping.setAdapter(shoppingAdapter);
////                                        }
////                                        getLowToHigh(shoppingArray);
////                                    } else {
////                                        if (shoppingArray.size() != 0) {
////                                            Collections.sort(shoppingArray, new compPopulation());
////                                            shoppingAdapter = new ShopAdapter(ShoppingActivity.this, shoppingArray, ShoppingActivity.this, ShoppingActivity.this);
////                                            rvShopping.setAdapter(shoppingAdapter);
////                                        }
////                                    }
////                                }
////                            } catch (IndexOutOfBoundsException e) {
////                                e.printStackTrace();
////                            }
//////                            getHighToLow(shoppingArray);
////                            loadDlg.dismiss();
////
////                        }
////
////                    } catch (JSONException e) {
////                        loadDlg.dismiss();
////                        e.printStackTrace();
////                        Toast.makeText(getApplicationContext(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
////                    }
////                }
////            }, new Response.ErrorListener() {
////                @Override
////                public void onErrorResponse(VolleyError error) {
////                    loadDlg.dismiss();
////                    CustomToast.showMessage(ShoppingActivity.this, NetworkErrorHandler.getMessage(error, ShoppingActivity.this));
////                    error.printStackTrace();
////                }
////            }) {
////                @Override
////                public Map<String, String> getHeaders() throws AuthFailureError {
////                    HashMap<String, String> map = new HashMap<>();
////                    map.put("hash", "1234");
////                    return map;
////                }
////
////            };
////            int socketTimeout = 60000;
////            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
////            postReq.setRetryPolicy(policy);
////            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
////        }
////    }
//
//    public void getCatList() {
//        loadDlg.show();
//        jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("mobile", session.getUserMobileNo());
//            jsonRequest.put("countryName", "india");
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        if (jsonRequest != null) {
//            Log.i("CATLIST", ApiUrl.URL_SHOPPING_CATEGORY_LIST);
//            Log.i("CATREQ", jsonRequest.toString());
//            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_SHOPPING_CATEGORY_LIST, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject jsonObj) {
//                    loadDlg.dismiss();
//                    try {
//                        Log.i("CATRES", jsonObj.toString());
//                        String code = jsonObj.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            try {
//                                if (!jsonObj.isNull("response")) {
//                                    categoryArray.clear();
//                                    ShoppingModel.deleteAll(ShoppingModel.class);
//
//                                    JSONArray productJArray = jsonObj.getJSONArray("response");
//                                    if (productJArray.length() != 0) {
//
//                                        for (int i = 0; i < productJArray.length(); i++) {
//                                            JSONObject c = productJArray.getJSONObject(i);
//                                            JSONObject catId = c.getJSONObject("country");
//                                            long cId = catId.getLong("id");
//                                            String cImage = c.getString("categoryImage");
//                                            String cName = c.getString("categoryName");
//                                            CategoryListModel categoryListModel = new CategoryListModel(String.valueOf(cId), cName,"http://qwikrpay.com" + cImage);
//                                            categoryArray.add((categoryListModel));
//
//                                        }
//                                        if (categoryArray.size() != 0) {
//                                            catAdapter = new CatAdapter(ShoppingActivityBackup.this, categoryArray, ShoppingActivityBackup.this, ShoppingActivityBackup.this);
//                                            rvShopping.setAdapter(catAdapter);
//                                        }
//                                    } else {
////                                        if (shoppingArray.size() != 0) {
////                                            Collections.sort(shoppingArray, new compPopulation());
////                                            shoppingAdapter = new ShopAdapter(ShoppingActivity.this, shoppingArray, ShoppingActivity.this, ShoppingActivity.this);
////                                            rvShopping.setAdapter(shoppingAdapter);
////                                        }
//                                        CustomToast.showMessage(ShoppingActivityBackup.this,"No category Found");
//                                        finish();
//                                    }
//                                }
//                            } catch (IndexOutOfBoundsException e) {
//                                e.printStackTrace();
//                            }
////                            getHighToLow(shoppingArray);
//                            loadDlg.dismiss();
//
//                        }
//
//                    } catch (JSONException e) {
//                        loadDlg.dismiss();
//                        e.printStackTrace();
//                        Toast.makeText(getApplicationContext(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
//                    CustomToast.showMessage(ShoppingActivityBackup.this, NetworkErrorHandler.getMessage(error, ShoppingActivityBackup.this));
//                    error.printStackTrace();
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//    }
////    private Drawable buildCounterDrawable(int count, int backgroundImageId) {
////        LayoutInflater inflater = LayoutInflater.from(this);
////        View view = inflater.inflate(R.layout.activity_shopping_add_cart, null);
////        view.setBackgroundResource(backgroundImageId);
////
////        if (count == 0) {
////            View counterTextPanel = view.findViewById(R.id.cartCount);
////            counterTextPanel.setVisibility(View.GONE);
////        } else {
////            TextView textView = (TextView) view.findViewById(R.id.cartCount);
////            textView.setText("" + count);
////        }
////
////        view.measure(
////                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
////                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
////        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
////
////        view.setDrawingCacheEnabled(true);
////        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
////        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
////        view.setDrawingCacheEnabled(false);
////
////        return new BitmapDrawable(getResources(), bitmap);
////    }
//    @Override
//    public void taskCompleted(String response) {
//        if (response.equals("Add")) {
//            cartValue = String.valueOf(Integer.parseInt(cartValue) + 1);
//            tvcart_point.setText(cartValue);
//            if(Integer.parseInt(cartValue)>0){
//                badge.setText(cartValue);
//                badge.show();
//            }
////            shoppingAdapter.notifyDataSetChanged();
//        } else {
//            cartValue = String.valueOf(Integer.parseInt(cartValue) - 1);
////            shoppingAdapter.notifyDataSetChanged();
////            tvcart_point.setText(cartValue);
//            if(Integer.parseInt(cartValue)>0){
//                badge.setText(cartValue);
//                badge.show();
//            }
//        }
//
//    }
//
////    public void getLowToHigh(List<ShoppingModel> shoppingArray) {
////        if (shoppingArray != null && shoppingArray.size() != 0) {
////            Collections.sort(shoppingArray, new Comparator<ShoppingModel>() {
////                @Override
////                public int compare(ShoppingModel lhs, ShoppingModel rhs) {
////                    // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
////                    return lhs.getpPrice() > rhs.getpPrice() ? -1 : (lhs.getpPrice() < rhs.getpPrice()) ? 1 : 0;
////                }
////            });
////            shoppingAdapter.notifyDataSetChanged();
////        }
////    }
////
////    public void getHighToLow(List<ShoppingModel> shoppingArray) {
////        if (shoppingArray != null && shoppingArray.size() != 0) {
////            Collections.sort(shoppingArray, Collections.reverseOrder());
////            shoppingAdapter = new ShopAdapter(ShoppingActivity.this, shoppingArray, ShoppingActivity.this, ShoppingActivity.this);
////            rvShopping.setAdapter(shoppingAdapter);
////        }
////    }
//
//    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getStringExtra("updates");
//            if (action.equals("1")) {
//                pqCart.clearCart();
////                ShoppingModel.delete(ShoppingModel.class);
////                shoppingArray.clear();
//////                tvcart_point.setText("0");
////                shoppingAdapter.notifyDataSetChanged();
//                badge.hide();
//
//            } else if (action.equals("2")) {
//                cartValue = String.valueOf(pqCart.getProductsInCartArray().size());
////                tvcart_point.setText(cartValue);
//                if(Integer.parseInt(cartValue)>0){
//                    badge.setText(cartValue);
//                    badge.show();
//                }
////                buildCounterDrawable(pqCart.getProductsInCartArray().size(), R.drawable.shopping_cart);
//            }
//        }
//    };
//
////    private BroadcastReceiver fMessageReceiver = new BroadcastReceiver() {
////        @Override
////        public void onReceive(Context context, Intent intent) {
////            action = intent.getStringExtra("TYPE");
////            if (action.equals("SUBCAT")) {
////                String catCode = intent.getStringExtra("CATID");
////                String subCatCode = intent.getStringExtra("SUBCATID");
////                getShoppingItems("SUBCAT", catCode, subCatCode, "");
////            } else if (action.equals("all")) {
////                getShoppingItems("all", "", "", "");
////            } else {
////                String brandCode = intent.getStringExtra("BRANDID");
////                getShoppingItems("BRAND", "", "", brandCode);
////            }
////        }
////    };
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        cartValue = String.valueOf(pqCart.getProductsInCart().size());
//        Log.i("CARTONRESUME", cartValue);
////        tvcart_point.setText(cartValue);
//        if(Integer.parseInt(cartValue)>0){
//            badge.setText(cartValue);
//            badge.show();
//        }
////        if (selectValue != null && selectValue.length() != 0) {
////            if (selectValue.equalsIgnoreCase("Price - high to low")) {
////                getHighToLow(shoppingArray);
////                selectValue = "";
////            } else if (selectValue.equalsIgnoreCase("Price - low to high")) {
////                getLowToHigh(shoppingArray);
////                selectValue = "";
////            }
////        }
//    }
//
//    @Override
//    public void onClick(View v) {
//        Intent i = getIntent();
//        switch (v.getId()) {
//            case R.id.itemOne:
//                itemOne.setTextColor(getResources().getColor(R.color.colorPrimary));
//                itemTwo.setTextColor(getResources().getColor(R.color.white_text));
//                itemFour.setTextColor(getResources().getColor(R.color.white_text));
//                itemThree.setTextColor(getResources().getColor(R.color.white_text));
//                itemFive.setTextColor(getResources().getColor(R.color.white_text));
//                selectValue = itemOne.getText().toString();
//                break;
//            case R.id.itemTwo:
//                itemOne.setTextColor(getResources().getColor(R.color.white_text));
//                itemTwo.setTextColor(getResources().getColor(R.color.colorPrimary));
//                itemFour.setTextColor(getResources().getColor(R.color.white_text));
//                itemThree.setTextColor(getResources().getColor(R.color.white_text));
//                itemFive.setTextColor(getResources().getColor(R.color.white_text));
//                selectValue = itemTwo.getText().toString();
//                break;
//            case R.id.itemThree:
//                itemOne.setTextColor(getResources().getColor(R.color.white_text));
//                itemTwo.setTextColor(getResources().getColor(R.color.white_text));
//                itemFour.setTextColor(getResources().getColor(R.color.white_text));
//                itemThree.setTextColor(getResources().getColor(R.color.colorPrimary));
//                itemFive.setTextColor(getResources().getColor(R.color.white_text));
//                selectValue = itemThree.getText().toString();
//                break;
//            case R.id.itemFour:
//                itemOne.setTextColor(getResources().getColor(R.color.white_text));
//                itemTwo.setTextColor(getResources().getColor(R.color.white_text));
//                itemFour.setTextColor(getResources().getColor(R.color.colorPrimary));
//                itemThree.setTextColor(getResources().getColor(R.color.white_text));
//                itemFive.setTextColor(getResources().getColor(R.color.white_text));
//                selectValue = itemFour.getText().toString();
//                break;
//            case R.id.itemFive:
//                itemOne.setTextColor(getResources().getColor(R.color.white_text));
//                itemTwo.setTextColor(getResources().getColor(R.color.white_text));
//                itemFour.setTextColor(getResources().getColor(R.color.white_text));
//                itemThree.setTextColor(getResources().getColor(R.color.white_text));
//                itemFive.setTextColor(getResources().getColor(R.color.colorPrimary));
//                selectValue = itemFive.getText().toString();
//                break;
//            case R.id.ivclose:
//                finish();
//                startActivity(i);
//                break;
//            case R.id.submit:
//                finish();
//                startActivity(i);
//
//                break;
//        }
//    }
//
//    @Override
//    public void onSliderClick(BaseSliderView slider) {
//
//    }
//
//    @Override
//    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//    }
//
//    @Override
//    public void onPageSelected(int position) {
//
//    }
//
//    @Override
//    public void onPageScrollStateChanged(int state) {
//
//    }
//
//
//    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
//        private int space;
//
//        public SpacesItemDecoration(int space) {
//            this.space = space;
//        }
//
//        @Override
//        public void getItemOffsets(Rect outRect, View view,
//                                   RecyclerView parent, RecyclerView.State state) {
//            outRect.left = space;
//            outRect.right = space;
//            outRect.bottom = space;
//            outRect.top = space;
//        }
//    }
//
//    @Override
//    public void onBackPressed() {
//        sendRefresh();
//        super.onBackPressed();
//    }
//
//    private void sendRefresh() {
//        Intent intent = new Intent("setting-change");
//        intent.putExtra("updates", "6");
//        LocalBroadcastManager.getInstance(ShoppingActivityBackup.this).sendBroadcast(intent);
//    }
//
//    @Override
//    protected void onDestroy() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
////        LocalBroadcastManager.getInstance(this).unregisterReceiver(fMessageReceiver);
//        super.onDestroy();
//    }
//
//    public class compPopulation implements Comparator<ShoppingModel> {
//
//        public int compare(ShoppingModel a, ShoppingModel b) {
//            Log.i("MAx", "value");
//            if (a.getpPrice() > b.getpPrice())
//                return -1; // highest value first
//            if (a.getpPrice() == b.getpPrice())
//                return 0;
//            return 1;
//        }
//    }
//
//}
