package com.Blockgration.Userwallet.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;

import com.Blockgration.custom.CustomToast;
import com.Blockgration.Userwallet.R;
import com.Blockgration.fragment.billpayment.BillPaymentsFragment;
import com.Blockgration.fragment.billpayment.PNationBillPayFragment;
import com.Blockgration.fragment.fragmentgiftcard.GiftCardCatFragment;
import com.Blockgration.fragment.fragmentsmartmobile.SmartMobileAllOffersFragment;
import com.Blockgration.fragment.mainmenufragment.ExchangeMoneyFragment;
import com.Blockgration.fragment.mainmenufragment.FundTransferFragment;
import com.Blockgration.fragment.mainmenufragment.ReceiptFragment;
import com.Blockgration.fragment.mainmenufragment.SendMoneyFragment;
import com.Blockgration.fragment.mainmenufragment.TelcoServiceFragment;
import com.Blockgration.fragment.mainmenufragment.TravelFragment;
import com.Blockgration.fragment.mainmenufragment.WebViewFragment;
import com.Blockgration.fragment.mainmenufragment.WebViewTravelFragment;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.Userwallet.activity.shopping.ShoppingActivity;
import com.Blockgration.model.UserModel;
import com.movieapp.in.activity.HomePageActivity;
import com.niki.config.NikiConfig;
import com.niki.config.SessionConfig;
import com.serviceOntario.in.activity.Home;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Ksf on 4/9/2016.
 */
public class MainMenuDetailActivity extends SuperActivity {
    private String type;
    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private UserModel session = UserModel.getInstance();
    //FIREBASE
    public static final String PROPERTY_REG_ID = "registration_id";
    private String regId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telebuy_pay);

        //press back button in toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
//        setSupportActionBar(toolbar);
//        ivBackBtn.setVisibility(View.VISIBLE);
//        ivBackBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });

        type = getIntent().getStringExtra(AppMetadata.FRAGMENT_TYPE);
        if (type.equals("MobileTopUp")) {
            TelcoServiceFragment fragment = new TelcoServiceFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("BillPayment")) {
//            BillPaymentsFragment fragment = new BillPaymentsFragment();
            PNationBillPayFragment fragment = new PNationBillPayFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("FundTransfer")) {
            FundTransferFragment fragment = new FundTransferFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("ExchangeMoney")) {
            ExchangeMoneyFragment fragment = new ExchangeMoneyFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("Travel")) {
            TravelFragment fragment = new TravelFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("sendmoney")) {
            SendMoneyFragment fragment = new SendMoneyFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("Shopping")) {
            Intent shoppingIntent = new Intent(MainMenuDetailActivity.this, ShoppingActivity.class);
            startActivity(shoppingIntent);
            finish();
        } else if (type.equals("QRScan")) {
            Intent shoppingIntent = new Intent(MainMenuDetailActivity.this, MerchantPaymentActivity.class);
            startActivity(shoppingIntent);
            finish();
        } else if (type.equals("Entertainment")) {
            Intent shoppingIntent = new Intent(MainMenuDetailActivity.this, HomePageActivity.class);
            startActivity(shoppingIntent);
            finish();
        } else if (type.equals("EGovermment")) {
            Intent shoppingIntent = new Intent(MainMenuDetailActivity.this, Home.class);
            startActivity(shoppingIntent);
            finish();
        } else if (type.equals("ACH")) {
            String url = getIntent().getStringExtra("URL");
            String trazCode = getIntent().getStringExtra("TrazCode");
            WebViewFragment fragment = new WebViewFragment();
            Bundle bType = new Bundle();
            bType.putString("URL", url);
            bType.putString("TrazCode", trazCode);
            fragment.setArguments(bType);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        } else if (type.equals("Reciepts")) {
            Fragment fragment = new ReceiptFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        }
        else if (type.equals("Gift Cards")) {
            GiftCardCatFragment fragment = new GiftCardCatFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        }
        else if (type.equals("SmartMobile")) {
            SmartMobileAllOffersFragment fragment = new SmartMobileAllOffersFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameInCart, fragment);
            fragmentTransaction.commit();
        }
        else if (type.equals("ChatBot")) {
            startNikkiChat();
        } else {
            CustomToast.showMessage(MainMenuDetailActivity.this, getResources().getString(R.string.soon));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.Blockgration.Userwallet.R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void startNikkiChat() {
        if (NikiConfig.isNikiInitialized()) {
            NikiConfig.getInstance().startNikiChat(MainMenuDetailActivity.this);
            finish();
        } else {
            try {
                Log.i("MAINTHREAD", "Main Thread : " + Thread.currentThread().getId());
//                    startService(new Intent(getBaseContext(), SessionCheck.class));
                JSONObject nikkiJson = new JSONObject();
                nikkiJson.put("name", session.getUserFirstName());
                nikkiJson.put("email", session.getUserEmail());
                nikkiJson.put("phoneNumber", session.getUserMobileNo());
                regId = getRegistrationId();
                if (regId != null && !regId.isEmpty()) {
                    Log.i("REGID", regId);
                    Log.i("registrationId", regId);
                    nikkiJson.put("regId", regId);
                }

                SessionConfig config = new SessionConfig.Builder()
                        .setAccessKey(AppMetadata.appKey)//Mandatory
                        .setSecret(AppMetadata.appSecret)//Mandatory
                        .setParentTheme(false)//Optional
                        .setBackIconResourceId(R.drawable.ic_back)//Optional
                        .setTitleColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null))//Optional
                        .setMerchantTitle("Claro")//Optional
                        .setLogoResourceId(R.drawable.claro_niki)//Optional (To set logo in the header)
                        .setLogoGravity(Gravity.CENTER) //Optional (To be used in case of horizontal logo to center align it)
                        .setUserData(nikkiJson)//Optional, See Configuring SDK section on how to create this
                        .build();
                NikiConfig.initNiki(MainMenuDetailActivity.this, config);
                finish();
                NikiConfig.getInstance().startNikiChat(MainMenuDetailActivity.this);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences() {
        return getSharedPreferences("FIREBASE",
                Context.MODE_PRIVATE);
    }
}
