package com.Blockgration.Userwallet.activity.loadmoney;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.MainActivity;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.NetworkErrorHandler;
import com.serviceOntario.in.activity.status.StatusActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by admin on 4/30/2016.
 */
public class MonerisViewActivity extends AppCompatActivity {

    public static final String EXTRA_AMOUNT = "amount";
    private WebView wbview;
    static String response = null;
    private ImageView back;
    UserModel userModel = UserModel.getInstance();
    private Dialog dialog;
    private String sessionId;
    private int amount;
    private ProgressBar progressBar;
    private LinearLayout frame;
    private LoadingDialog loadingDialog;
    private LoadingDialog loadDlg;
    private String orderId;
    private String tag_json_obj = "load_money";

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_webview);
        amount = getIntent().getIntExtra(MonerisViewActivity.EXTRA_AMOUNT, 0);
        frame = (LinearLayout) findViewById(R.id.llLoadMoneyWebViewLoading);
        loadingDialog = new LoadingDialog(MonerisViewActivity.this);
        back = (ImageView) findViewById(R.id.iconback);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loadDlg = new LoadingDialog(MonerisViewActivity.this);
        orderId = String.valueOf(System.currentTimeMillis());
        try {
            class MyJavaScriptInterface {
                @JavascriptInterface
                public void processHTML(String html) {
                    // process the html as needed by the app
                    // TODO Process the HTML

                    String status = null;
                    if (html.indexOf("Source : ERROR") != -1) {
                        status = "Transaction Declined!";
                    } else {
                        status = "Status Not Known!";
                    }
                    // Start Activity with service_status_activity put in extra
                    final Intent intent = new Intent(getApplicationContext(), StatusActivity.class);
                    intent.putExtra("status", status);
                    Thread timer = new Thread() {
                        public void run() {
                            try {
                                sleep(10);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } finally {
                                startActivity(intent);
                                finish();
                            }
                        }
                    };
                    timer.start();
                }
            }

            wbview = (android.webkit.WebView) findViewById(R.id.webview);
            wbview.getSettings().setJavaScriptEnabled(true);
            wbview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
            wbview.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    if (progress == 100) {
                        loadingDialog.dismiss();
                    } else {
                        loadingDialog.show();
                    }
                }
            });

            wbview.setWebViewClient(new WebViewClient() {

                @Override
                public void onPageFinished(android.webkit.WebView view, String url) {
                    super.onPageFinished(wbview, url);
                    frame.setVisibility(View.GONE);
                    Log.i("URL", url);
                    if (url.contains("cancelTXN=Cancel+Transaction")) {
                        finish();
//                        wbview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                    }
                }

                @Override
                public void onReceivedError(android.webkit.WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(getApplicationContext(), "Oh no!" + description, Toast.LENGTH_SHORT).show();
                    frame.setVisibility(View.GONE);
                }

                @Override
                public void onPageStarted(android.webkit.WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    Log.i("URL_NEW", url);
                    if (url.contains("https://its-robdavis09.ws.its.uwo.ca:8181/paymentbroker/moneris/receipt")) {
                        view.stopLoading();
                        getJSONLOAD();
                    }

                }
            });


            wbview.getSettings().setLoadsImagesAutomatically(true);
            wbview.getSettings().setPluginState(WebSettings.PluginState.ON);
            wbview.getSettings().setAllowFileAccess(true);
            wbview.getSettings().setLoadWithOverviewMode(true);
            wbview.getSettings().setUseWideViewPort(false);
            wbview.getSettings().setBuiltInZoomControls(true);
            wbview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            String DESKTOP_USERAGENT = wbview.getSettings().getUserAgentString();
            DESKTOP_USERAGENT = DESKTOP_USERAGENT.replace("Mobile ", "");
            wbview.getSettings().setUserAgentString(DESKTOP_USERAGENT);
            String params = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\n" +
                    "</head>\n" +
                    "<body onLoad=\"document.pay.submit()\">\n" +
                    " <form method=\"post\" id=\"pay\" name=\"pay\" action=\"https://esqa.moneris.com/HPPDP/index.php\">\n" +
                    "  <input type=\"hidden\" name=\"ps_store_id\" class=\"form-control\"  value=\"A4RS8tore3\" />\n" +
                    " <input type=\"hidden\" name=\"hpp_key\" class=\"form-control\"  value=\"hpIZB3HA5L5K\"/>\n" +
                    " <input type=\"hidden\" class=\"form-control\" name=\"charge_total\" value=\"" + amount + "\" />\n" +
                    " \n" +
                    "  <!-- <input type=\"submit\" value=\"submit\" /> -->\n" +
                    " </form>\n" +
                    " <script>document.getElementById('pay').submit();</script>\n" +
                    "</body>\n" +
                    "</html>";

            wbview.loadData(params, "text/Html", "Base64");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    void getJSONLOAD() {
        loadDlg.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sessionId", userModel.getUserSessionId());
            jsonObject.put("amount", amount);
            jsonObject.put("transactionRefNo", orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_LOAD_MONEY, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    loadDlg.dismiss();
                    String message = response.getString("message");
                    String code = response.getString("code");
                    if (code != null & code.equals("S00")) {
                        CustomToast.showMessage(MonerisViewActivity.this, "Load Money Successful");
                        sendRefresh();
                        finishAffinity();
                        Intent intent = new Intent(MonerisViewActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                    } else {
                        finish();
                        CustomToast.showMessage(MonerisViewActivity.this, message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    loadDlg.dismiss();
                    finish();
                    CustomToast.showMessage(MonerisViewActivity.this, "Error connecting to server");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                loadDlg.dismiss();
                finish();
                CustomToast.showMessage(MonerisViewActivity.this, NetworkErrorHandler.getMessage(volleyError, MonerisViewActivity.this));
                volleyError.printStackTrace();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("hash", "123");
                return map;
            }

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        // Adding request to request queue
        PayQwikApp.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
    }

    private void sendRefresh() {

        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "3");
        LocalBroadcastManager.getInstance(MonerisViewActivity.this).sendBroadcast(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
