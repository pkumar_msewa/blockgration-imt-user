package com.Blockgration.Userwallet.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.model.MobileRechargeCountryModel;
import com.orm.query.Select;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.model.CountryModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.Userwallet.R;
import com.Blockgration.util.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SplashActivity extends SuperActivity {

    private UserModel session = UserModel.getInstance();
    private List<UserModel> userArray;

    //For  now set it to true
    private boolean mPinActive;
    private ConnectionDetector connectionDetector;
    Boolean isInternetPresent = false;
    Boolean isResumed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        List<CountryModel> countryStoredArray = Select.from(CountryModel.class).list();

//        if (countryStoredArray.size() == 0) {
//            loadCountry();
//        } else {
//            workLoad(3000);
//        }
        workLoad(3000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("Stop", "stop");
    }

    private boolean checkSession() {

        userArray = Select.from(UserModel.class).list();
        if (userArray.size() != 0) {
            session.setUserSessionId(userArray.get(0).getUserSessionId());
            session.setUserFirstName(userArray.get(0).getUserFirstName());
            session.setUserLastName(userArray.get(0).getUserLastName());
            session.setUserMobileNo(userArray.get(0).getUserMobileNo());
            session.setUserEmail(userArray.get(0).getUserEmail());
            session.setUserAddress(userArray.get(0).getUserAddress());
            session.setEmailIsActive(userArray.get(0).getEmailIsActive());
            session.setUserImage(userArray.get(0).getUserImage());
            session.setMPin(userArray.get(0).isMPin());
            mPinActive = session.isMPin;
            session.setIsValid(1);
            return true;
        } else {
            session.setIsValid(0);
            return false;
        }
    }


    public void showCustomDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(SplashActivity.this, R.string.dialog_title2, Html.fromHtml(generateMessage()));

        builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                isResumed = true;
                dialog.dismiss();
                startActivity(new Intent(Settings.ACTION_SETTINGS));

            }
        });

        builder.setNegativeButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                workLoad(1000);
            }
        });
        builder.show();
    }

    public String generateMessage() {
        String source = "<b><font color=#ff0000> No internet connection.</font></b>" +
                "<br><br><b><font color=#000000> Please check your internet connection and try again.</font></b><br></br>";
        return source;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isResumed) {
            workLoad(1000);
        }

    }

    private void workLoad(final int time) {
        //Checking internet Status
        connectionDetector = new ConnectionDetector(getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      if (isInternetPresent) {
                                          if (!checkSession()) {
                                              Intent mainIntent = new Intent(SplashActivity.this, LoginRegActivity.class);
                                              startActivity(mainIntent);
                                              finish();
                                          } else {
                                              if (mPinActive) {
                                                  Intent mainIntent = new Intent(SplashActivity.this, VerifyMPinNewActivity.class);
                                                  startActivity(mainIntent);
                                                  finish();
                                              } else {
                                                  Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                                                  startActivity(mainIntent);
                                                  finish();
                                              }

                                          }
                                      } else {
                                          showCustomDialog();
                                      }
                                  }

                              }
                );
            }
        };
        thread.start();
    }

    public String loadCountryJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("country.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void loadCountry() {
        try {
            CountryModel.deleteAll(CountryModel.class);
            JSONObject obj = new JSONObject(loadCountryJSONFromAsset());
            JSONArray country_array = obj.getJSONArray("country");
            for (int i = 0; i < country_array.length(); i++) {
                JSONObject jo_inside = country_array.getJSONObject(i);
                String symbol = "";
                String countryName = jo_inside.getString("countryName");
                String countryCode = jo_inside.getString("countryCode");
                String currencyCode = jo_inside.getString("currencyCode");
                String isoAlpha3 = jo_inside.getString("isoAlpha3");
                String currencyName = jo_inside.getString("Currency Name");
                if (jo_inside.has("currencySymbol")) {
                    symbol = jo_inside.getString("currencySymbol");
                }

                CountryModel countryModel = new CountryModel(countryName, countryCode, currencyCode, isoAlpha3, symbol, currencyName);
                countryModel.save();
            }
            workLoad(3000);

        } catch (JSONException e) {
            e.printStackTrace();
            workLoad(3000);
        }
    }

    @Override
    protected void onPause() {
        Log.i("pasuse", "pas");
        super.onPause();

    }
}
