package com.Blockgration.Userwallet.activity.flightinneracitivty;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.shopping.CustomSuccessDialog;
import com.Blockgration.Userwallet.activity.shopping.LoadingDialog;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.FlightListModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.CheckLog;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.PayingDetailsValidation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by kashifimam on 05/08/17.
 */

public class FlightBookingFormReturnActivity extends AppCompatActivity {


    private EditText etBookFlightMobile, etBookFlightDob, etBookFlightCitizen, etBookFlightEmail;
    private RadioButton rbBookFlightMale, rbBookFlightFeMale;
    private LinearLayout llFlightAdult,llFlightChild,llFlightInfant;

    private Button btnBookFlightSubmit;


    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private LoadingDialog loadDlg;

    private String gender = "MALE";

    private String sourceCode, destinationCode, dateOfJourney;
    private int adultNo, childNo, infantNo;
    private String flightClass;


    private FlightListModel flightListModelUp, flightListModelDown;
    private UserModel session = UserModel.getInstance();

    private JSONObject segmentObject ;
    private JSONObject segmentObjectDown ;

    private JSONObject jsonRequest;
    private String tag_json_obj = "json_flight";

    private String engineId;



    private boolean cancel;
    private View focusView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_booking_form);
        loadDlg = new LoadingDialog(FlightBookingFormReturnActivity.this);

        //Intent Data
        flightListModelUp = getIntent().getParcelableExtra("FlightModelUp");
        flightListModelDown = getIntent().getParcelableExtra("FlightModelDown");

        try {
            segmentObject = new JSONObject(flightListModelUp.getFlightJsonString());
            segmentObjectDown = new JSONObject(flightListModelDown.getFlightJsonString());

            engineId = segmentObject.getString("engineID");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sourceCode = getIntent().getStringExtra("sourceCode");
        destinationCode = getIntent().getStringExtra("destinationCode");
        dateOfJourney = getIntent().getStringExtra("dateOfJourney");
        flightClass = getIntent().getStringExtra("flightClass");

        adultNo = getIntent().getIntExtra("adultNo", 0);
        childNo = getIntent().getIntExtra("childNo", 0);
        infantNo = getIntent().getIntExtra("infantNo", 0);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivBackBtn = (ImageButton)findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        etBookFlightMobile = (EditText) findViewById(R.id.etBookFlightMobile);
        etBookFlightDob = (EditText) findViewById(R.id.etBookFlightDob);
        etBookFlightCitizen = (EditText) findViewById(R.id.etBookFlightCitizen);
        etBookFlightEmail = (EditText) findViewById(R.id.etBookFlightEmail);


        rbBookFlightMale = (RadioButton) findViewById(R.id.rbBookFlightMale);
        rbBookFlightFeMale = (RadioButton) findViewById(R.id.rbBookFlightFeMale);

        btnBookFlightSubmit = (Button) findViewById(R.id.btnBookFlightSubmit);

        llFlightAdult = (LinearLayout) findViewById(R.id.llFlightAdult);
        llFlightChild = (LinearLayout) findViewById(R.id.llFlightChild);
        llFlightInfant = (LinearLayout) findViewById(R.id.llFlightInfant);

        if(childNo == 0){
            llFlightChild.setVisibility(View.GONE);
        }

        if(adultNo == 0){
            llFlightAdult.setVisibility(View.GONE);
        }

        if(infantNo == 0){
            llFlightInfant.setVisibility(View.GONE);
        }



        etBookFlightDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                new DatePickerDialog(FlightBookingFormReturnActivity.this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String formattedDay, formattedMonth;
                        if (dayOfMonth < 10) {
                            formattedDay = "0" + dayOfMonth;
                        } else {
                            formattedDay = dayOfMonth + "";
                        }

                        if ((monthOfYear + 1) < 10) {
                            formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                        } else {
                            formattedMonth = String.valueOf(monthOfYear + 1) + "";
                        }
                        etBookFlightDob.setText(String.valueOf(year) + "-"
                                + String.valueOf(formattedMonth) + "-"
                                + String.valueOf(formattedDay));
                    }
                }, 1990, 01, 01).show();
            }
        });



        btnBookFlightSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });

        for (int i = 0; i < adultNo; i++) {
            ViewGroup.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                    ,ViewGroup.LayoutParams.WRAP_CONTENT);

            final EditText etFName = new EditText(FlightBookingFormReturnActivity.this);
            etFName.setHint("Enter adult first name");
            final EditText etLName = new EditText(FlightBookingFormReturnActivity.this);
            etLName.setHint("Enter adult  last name");

            etFName.setLayoutParams(lp);
            etLName.setLayoutParams(lp);

            llFlightAdult.addView(etFName);
            llFlightAdult.addView(etLName);
        }

        for (int i = 0; i < childNo; i++) {
            ViewGroup.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                    ,ViewGroup.LayoutParams.WRAP_CONTENT);

            final EditText etFName = new EditText(FlightBookingFormReturnActivity.this);
            etFName.setHint("Enter child first name");
            final EditText etLName = new EditText(FlightBookingFormReturnActivity.this);
            etLName.setHint("Enter child last name");

            etFName.setLayoutParams(lp);
            etLName.setLayoutParams(lp);

            llFlightChild.addView(etFName);
            llFlightChild.addView(etLName);
        }

        for (int i = 0; i < infantNo; i++) {
            ViewGroup.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                    ,ViewGroup.LayoutParams.WRAP_CONTENT);

            final EditText etFName = new EditText(FlightBookingFormReturnActivity.this);
            etFName.setHint("Enter infant first name");
            final EditText etLName = new EditText(FlightBookingFormReturnActivity.this);
            etLName.setHint("Enter infant last name");

            etFName.setLayoutParams(lp);
            etLName.setLayoutParams(lp);

            llFlightInfant.addView(etFName);
            llFlightInfant.addView(etLName);
        }




    }


    private void submitForm() {
        validateDynamicAdultField();
        validateDynamicChildField();
        validateDynamicInfantField();
        etBookFlightCitizen.setError(null);
        etBookFlightDob.setError(null);
        etBookFlightMobile.setError(null);
        etBookFlightEmail.setError(null);

        cancel = false;

        checkDynamicAdultField();
        checkDynamicChildField();
        checkDynamicInfantField();
        checkDob(etBookFlightDob.getText().toString());
        checkCitizenship(etBookFlightCitizen.getText().toString());
        checkEmail(etBookFlightEmail.getText().toString());
        checkMobile(etBookFlightMobile.getText().toString());


        if (rbBookFlightMale.isChecked()) {
            gender = "MALE";
        } else{
            gender = "FEMALE";
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            loadDlg.show();
            attemptCheckOut();
        }



    }

    private void validateDynamicAdultField() {
        int count = llFlightAdult.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightAdult.getChildAt(i);
            EditText etDynamic = (EditText) view;
            etDynamic.setError(null);
        }


    }


    private void checkDynamicAdultField() {
        int count = llFlightAdult.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightAdult.getChildAt(i);
            EditText etDynamic = (EditText) view;

            if (etDynamic.getText().toString().trim().isEmpty()) {
                etDynamic.setError("This field is required");
                focusView = etDynamic;
                cancel = true;
            }
        }

    }

    private void validateDynamicChildField() {
        int count = llFlightChild.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightChild.getChildAt(i);
            EditText etDynamic = (EditText) view;
            etDynamic.setError(null);
        }


    }


    private void checkDynamicChildField() {
        int count = llFlightChild.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightChild.getChildAt(i);
            EditText etDynamic = (EditText) view;

            if (etDynamic.getText().toString().trim().isEmpty()) {
                etDynamic.setError("This field is required");
                focusView = etDynamic;
                cancel = true;
            }
        }

    }

    private void validateDynamicInfantField() {
        int count = llFlightInfant.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightInfant.getChildAt(i);
            EditText etDynamic = (EditText) view;
            etDynamic.setError(null);
        }


    }


    private void checkDynamicInfantField() {
        int count = llFlightInfant.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = llFlightInfant.getChildAt(i);
            EditText etDynamic = (EditText) view;

            if (etDynamic.getText().toString().trim().isEmpty()) {
                etDynamic.setError("This field is required");
                focusView = etDynamic;
                cancel = true;
            }
        }

    }


    private void checkEmail(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkEmail(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etBookFlightEmail.setError(getString(inviteNameCheckLog.msg));
            focusView = etBookFlightEmail;
            cancel = true;
        }

    }

    private void checkMobile(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkMobileTenDigit(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etBookFlightMobile.setError(getString(inviteNameCheckLog.msg));
            focusView = etBookFlightMobile;
            cancel = true;
        }

    }

    private void checkCitizenship(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etBookFlightCitizen.setError(getString(inviteNameCheckLog.msg));
            focusView = etBookFlightCitizen;
            cancel = true;
        }

    }

    private void checkDob(String inviteName) {
        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(inviteName);
        if (!inviteNameCheckLog.isValid) {
            etBookFlightDob.setError(getString(inviteNameCheckLog.msg));
            focusView = etBookFlightDob;
            cancel = true;
        }

    }


    public void attemptCheckOut() {
        jsonRequest = new JSONObject();

        try {


            //Adult
            String adultFName = "";
            String adultLName = "";

            for (int i = 0; i < llFlightAdult.getChildCount(); i++) {
                View view = llFlightAdult.getChildAt(i);
                EditText etDynamic = (EditText) view;
                if((i % 2) == 0){
                    adultFName = adultFName+"~"+etDynamic.getText().toString();
                }else{
                    adultLName = adultLName+"@"+etDynamic.getText().toString();
                }

            }

            //Child
            String childFName = "";
            String childLName = "";
            for (int i = 0; i < llFlightChild.getChildCount(); i++) {
                View view = llFlightChild.getChildAt(i);
                EditText etDynamic = (EditText) view;
                if((i % 2) == 0){
                    childFName = childFName+"~"+etDynamic.getText().toString();
                }else{
                    childLName = childLName+ "@"+etDynamic.getText().toString();
                }

            }

            //Infant
            String infantFName = "";
            String infantLName = "";

            for (int i = 0; i < llFlightInfant.getChildCount(); i++) {
                View view = llFlightInfant.getChildAt(i);
                EditText etDynamic = (EditText) view;
                if((i % 2) == 0){
                    infantFName = infantFName+"~"+etDynamic.getText().toString();
                }else{
                    infantLName =infantLName+ "@"+etDynamic.getText().toString();
                }

            }

            if(childNo !=0){
                jsonRequest.put("firstNamechild", childFName);
                jsonRequest.put("lastNamechild", childLName);
            }

            if(adultNo!=0){
                jsonRequest.put("firstName", adultFName);
                jsonRequest.put("lastName", adultLName);

            }

            if(infantNo!=0){
                jsonRequest.put("firstNameinfant", infantFName);
                jsonRequest.put("lastNameinfant", infantLName);

            }


            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("grandtotal", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("totalFareWithOutMarkUp")+segmentObjectDown.getJSONArray("fare").getJSONObject(0).getDouble("totalFareWithOutMarkUp"));
            jsonRequest.put("boundType", "");
            jsonRequest.put("itineraryKey", segmentObject.getString("itineraryKey"));
            jsonRequest.put("baggageFare", segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("isBaggageFare"));
            jsonRequest.put("ssrFare", segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("isSSR"));
            jsonRequest.put("journeyTime", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));
            jsonRequest.put("addOnDetail", "");
            jsonRequest.put("aircraftCode", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("aircraftCode"));
            jsonRequest.put("aircraftType", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("aircraftType"));
            jsonRequest.put("airlineName", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("airlineName"));
            jsonRequest.put("amount", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getDouble("amount"));
            jsonRequest.put("arrivalDate", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalDate"));
            jsonRequest.put("arrivalTerminal", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalTerminal"));
            jsonRequest.put("arrivalTime", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalTime"));
            jsonRequest.put("availableSeat", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("availableSeat"));
            jsonRequest.put("baggageUnit", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("baggageUnit"));
            jsonRequest.put("baggageWeight", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("baggageWeight"));
            jsonRequest.put("boundTypes", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("boundType"));
            jsonRequest.put("cabin", flightClass);
            jsonRequest.put("capacity", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("capacity"));
            jsonRequest.put("carrierCode", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("carrierCode"));
            jsonRequest.put("currencyCode", "INR");
            jsonRequest.put("departureDate", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureDate"));
            jsonRequest.put("departureTerminal", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureTerminal"));
            jsonRequest.put("departureTime", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureTime"));
            jsonRequest.put("destination", destinationCode);
            jsonRequest.put("duration", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));
            jsonRequest.put("fareBasisCode", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("fareBasisCode"));
            jsonRequest.put("fareClassOfService", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("fareClassOfService"));
            jsonRequest.put("flightDesignator", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightDesignator"));

            jsonRequest.put("flightDetailRefKey", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightDetailRefKey"));
            jsonRequest.put("flightName", segmentObject.getString("engineID"));
            jsonRequest.put("flightNumber", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightNumber"));
            jsonRequest.put("group", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("group"));
            jsonRequest.put("connecting", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getBoolean("isConnecting"));
            jsonRequest.put("numberOfStops", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("numberOfStops"));
            jsonRequest.put("origin", sourceCode);
            jsonRequest.put("providerCode", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("providerCode"));
            jsonRequest.put("remarks", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("remarks"));
            jsonRequest.put("sold", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getInt("sold"));
            jsonRequest.put("status", segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("status"));
            jsonRequest.put("deeplink", segmentObject.getString("deeplink"));
            jsonRequest.put("fareIndicator", 0);
            jsonRequest.put("fareRule", segmentObject.getString("fareRule"));
            jsonRequest.put("cache", segmentObject.getBoolean("isCache"));
            jsonRequest.put("holdBooking", segmentObject.getBoolean("isHoldBooking"));
            jsonRequest.put("international", segmentObject.getBoolean("isInternational"));
            jsonRequest.put("roundTrip", segmentObject.getBoolean("isRoundTrip"));
            jsonRequest.put("special", segmentObject.getBoolean("isSpecial"));
            jsonRequest.put("specialId", segmentObject.getBoolean("isSpecialId"));
            jsonRequest.put("journeyIndex", segmentObject.getInt("journeyIndex"));
            jsonRequest.put("memoryCreationTime", "/Date(1499158249483+0530)/");
            jsonRequest.put("nearByAirport", segmentObject.getBoolean("nearByAirport"));
            jsonRequest.put("remark", segmentObject.getString("remark"));
            jsonRequest.put("searchId", segmentObject.getString("searchId"));
            jsonRequest.put("engineID", segmentObject.getString("engineID"));

            jsonRequest.put("basicFare", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("basicFare"));
            jsonRequest.put("exchangeRate", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("exchangeRate"));
            jsonRequest.put("totalFareWithOutMarkUp", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("totalFareWithOutMarkUp"));
            jsonRequest.put("totalTaxWithOutMarkUp", segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("totalTaxWithOutMarkUp"));
            jsonRequest.put("baseTransactionAmount", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("baseTransactionAmount"));
            jsonRequest.put("cancelPenalty", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("cancelPenalty"));
            jsonRequest.put("changePenalty", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("changePenalty"));
            jsonRequest.put("equivCurrencyCode", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("equivCurrencyCode"));
            jsonRequest.put("fareInfoKey", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("fareInfoKey"));
            jsonRequest.put("fareInfoValue", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("fareInfoValue"));

            jsonRequest.put("markUP", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("markUP"));
            jsonRequest.put("paxType", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("paxType"));
            jsonRequest.put("refundable", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getBoolean("refundable"));
            jsonRequest.put("totalFare", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalFare"));
            jsonRequest.put("totalTax", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalTax"));
            jsonRequest.put("transactionAmount", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("transactionAmount"));
            jsonRequest.put("beginDate", dateOfJourney);
            jsonRequest.put("endDate", dateOfJourney);

            jsonRequest.put("bookingCurrencyCode", "INR");
            jsonRequest.put("address1", "BTM");
            jsonRequest.put("address2", "BTM");
            jsonRequest.put("city", "Banglore");
            jsonRequest.put("countryCode", "IN");
            jsonRequest.put("cultureCode", "String content");

            jsonRequest.put("emailAddress", etBookFlightEmail.getText().toString());
            jsonRequest.put("middleName", "");
            jsonRequest.put("homePhone", "");
            jsonRequest.put("gender", gender);
            jsonRequest.put("frequentFlierNumber", "");
            jsonRequest.put("mobileNumber", etBookFlightMobile.getText().toString());
            jsonRequest.put("nationality", etBookFlightCitizen.getText().toString());
            jsonRequest.put("passportExpiryDate", "/Date(928129800000+0530)/");
            jsonRequest.put("passportNo", "");
            jsonRequest.put("provisionState", "Karnataka");
            jsonRequest.put("residentCountry", "India");

            if(gender.equals("MALE")){
                jsonRequest.put("title", "Mr");
            }else{
                jsonRequest.put("title", "Miss");
            }

            jsonRequest.put("visatype", "Employee Visa");
            jsonRequest.put("traceId", "AYTM00011111111110001");
            jsonRequest.put("androidBooking", true);
            jsonRequest.put("domestic", true);
            jsonRequest.put("bookFares", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getJSONArray("fares"));

            //Date Fomat
            jsonRequest.put("bookingAmount",segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("totalFareWithOutMarkUp"));
            jsonRequest.put("dateofBirth", etBookFlightDob.getText().toString());
            jsonRequest.put("meal", "");




            //Reutrn
            jsonRequest.put("boundTypereturn", "");
            jsonRequest.put("itineraryKeyreturn", segmentObjectDown.getString("itineraryKey"));
            jsonRequest.put("baggageFarereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getBoolean("isBaggageFare"));
            jsonRequest.put("ssrFare", segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("isSSR"));
            jsonRequest.put("journeyTimereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));
            jsonRequest.put("addOnDetailreturn", "");
            jsonRequest.put("aircraftCodereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("aircraftCode"));
            jsonRequest.put("aircraftTypereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("aircraftType"));
            jsonRequest.put("airlineNamereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("airlineName"));
            jsonRequest.put("amountreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getDouble("amount"));
            jsonRequest.put("arrivalDatereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalDate"));
            jsonRequest.put("arrivalTerminalreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalTerminal"));
            jsonRequest.put("arrivalTimereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("arrivalTime"));
            jsonRequest.put("availableSeatreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("availableSeat"));
            jsonRequest.put("baggageUnitreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("baggageUnit"));
            jsonRequest.put("baggageWeightreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("baggageWeight"));
            jsonRequest.put("boundTypesreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("boundType"));
            jsonRequest.put("cabinreturn", flightClass);
            jsonRequest.put("capacityreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("capacity"));
            jsonRequest.put("carrierCodereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("carrierCode"));
            jsonRequest.put("currencyCodereturn", "INR");
            jsonRequest.put("departureDatereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureDate"));
            jsonRequest.put("departureTerminalreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureTerminal"));
            jsonRequest.put("departureTimereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("departureTime"));
            jsonRequest.put("destinationreturn", sourceCode);
            jsonRequest.put("durationreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));
            jsonRequest.put("fareBasisCodereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("fareBasisCode"));
            jsonRequest.put("fareClassOfServicereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("fareClassOfService"));
            jsonRequest.put("flightDesignatorreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightDesignator"));

            jsonRequest.put("flightDetailRefKeyreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightDetailRefKey"));
            jsonRequest.put("flightNamereturn", segmentObjectDown.getString("engineID"));
            jsonRequest.put("flightNumberreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("flightNumber"));
            jsonRequest.put("groupreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("group"));
            jsonRequest.put("connectingreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getBoolean("isConnecting"));
            jsonRequest.put("numberOfStopsreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("numberOfStops"));
            jsonRequest.put("originreturn", destinationCode);
            jsonRequest.put("providerCodereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("providerCode"));
            jsonRequest.put("remarksreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("remarks"));
            jsonRequest.put("soldreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getInt("sold"));
            jsonRequest.put("statusreturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("status"));
            jsonRequest.put("deeplinkreturn", segmentObjectDown.getString("deeplink"));
            jsonRequest.put("engineIDreturn", segmentObjectDown.getString("engineID"));
            jsonRequest.put("ssrFarereturn", segmentObjectDown.getJSONArray("bonds").getJSONObject(0).getBoolean("isSSR"));
            jsonRequest.put("cachereturn", segmentObjectDown.getBoolean("isCache"));
            jsonRequest.put("fareIndicatorreturn", 0);
            jsonRequest.put("basicFarereturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getDouble("basicFare"));
            jsonRequest.put("exchangeRatereturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getDouble("exchangeRate"));
            jsonRequest.put("baseTransactionAmountreturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("baseTransactionAmount"));
            jsonRequest.put("bookFaresreturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getJSONArray("fares"));
            jsonRequest.put("cancelPenaltyreturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("cancelPenalty"));
            jsonRequest.put("changePenaltyreturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("changePenalty"));
            jsonRequest.put("equivCurrencyCodereturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("equivCurrencyCode"));
            jsonRequest.put("fareInfoKeyreturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("fareInfoKey"));
            jsonRequest.put("fareInfoValuereturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("fareInfoValue"));
            jsonRequest.put("totalFareWithOutMarkUpreturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getDouble("totalFareWithOutMarkUp"));
            jsonRequest.put("totalTaxWithOutMarkUpreturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getDouble("totalTaxWithOutMarkUp"));
            jsonRequest.put("fareRulereturn", segmentObjectDown.getString("fareRule"));
            jsonRequest.put("holdBookingreturn", segmentObjectDown.getBoolean("isHoldBooking"));
            jsonRequest.put("internationalreturn", segmentObjectDown.getBoolean("isInternational"));
            jsonRequest.put("roundTripreturn", segmentObjectDown.getBoolean("isRoundTrip"));
            jsonRequest.put("specialreturn", segmentObjectDown.getBoolean("isSpecial"));
            jsonRequest.put("searchIdreturn", segmentObjectDown.getBoolean("isSpecialId"));
            jsonRequest.put("journeyIndexreturn", segmentObjectDown.getInt("journeyIndex"));
            jsonRequest.put("memoryCreationTimereturn", "/Date(1499158249483+0530)/");

            jsonRequest.put("markUPreturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("markUP"));
            jsonRequest.put("paxTypereturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("paxType"));
            jsonRequest.put("refundablereturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getBoolean("refundable"));
            jsonRequest.put("totalFarereturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalFare"));
            jsonRequest.put("totalTaxreturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalTax"));
            jsonRequest.put("transactionAmountreturn", segmentObjectDown.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("transactionAmount"));

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("Request",jsonRequest.toString());
            Log.i("URL", ApiURLNew.URL_FLIGHT_CHECKOUT);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_FLIGHT_CHECKOUT_ROUND, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("CheckOut Resonse", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            sendRefresh();
                            loadDlg.dismiss();
                            showSuccessDialog();
                            Toast.makeText(getApplicationContext(),"S00",Toast.LENGTH_SHORT).show();

                        }else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        }

                        else {
                            String message = response.getString("message");
                            loadDlg.dismiss();
                            CustomToast.showMessage(FlightBookingFormReturnActivity.this, message);

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(FlightBookingFormReturnActivity.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(FlightBookingFormReturnActivity.this, NetworkErrorHandler.getMessage(error, FlightBookingFormReturnActivity.this));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(FlightBookingFormReturnActivity.this).sendBroadcast(intent);
    }


    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(FlightBookingFormReturnActivity.this).sendBroadcast(intent);
    }




    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(FlightBookingFormReturnActivity.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public void showSuccessDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(getSuccessMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(getSuccessMessage());
        }
        CustomSuccessDialog builder = new CustomSuccessDialog(FlightBookingFormReturnActivity.this, "Booked Successful", result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                finish();

            }
        });
        builder.show();
    }

    public String getSuccessMessage() {
        String source =
                "<b><font color=#000000> Airline Name </font></b>" + "<font color=#000000>" + engineId + "</font><br>" +
                        "<b><font color=#000000> Date: </font></b>" + "<font>" + dateOfJourney + "</font><br>" +
                        "<b><font color=#000000> Please check provided email for detail. </font></b>" + "<font>";
        return source;
    }


}
