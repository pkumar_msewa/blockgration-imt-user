package com.Blockgration.Userwallet.activity;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.R;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.CheckLog;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.PayingDetailsValidation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MerchantPaymentActivity extends AppCompatActivity {

    private View rootView;
    private Button btnQRCodeScan, btnQRPay;
    private TextView tvPleaseScan, tvScanTitle;
    private TableLayout tlQRScannedResult;
    private ImageView ivQRCode;
    private EditText etQrScanAmount;
    private RequestQueue rq;
    private UserModel session = UserModel.getInstance();
    private View focusView = null;
    private boolean cancel;

    private String merchantId = "";
    private JSONObject jsonRequest;

    private LoadingDialog loadDlg;
    //Volley Tag
    private String tag_json_obj = "json_merchant_pay";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_merchant_pay);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setTitle("");
        loadDlg = new LoadingDialog(MerchantPaymentActivity.this);
        btnQRCodeScan = (Button) findViewById(R.id.btnQRCodeScan);
        btnQRPay = (Button) findViewById(R.id.btnQRPay);
        tvScanTitle = (TextView) findViewById(R.id.tvScanTitle);
        tvScanTitle.setText(getResources().getString(R.string.merchant_tittle));
        etQrScanAmount = (EditText) findViewById(R.id.etQrScanAmount);
        tvPleaseScan = (TextView) findViewById(R.id.tvPleaseScan);
        tvPleaseScan.setText(getResources().getString(R.string.qr_merchant));
        tlQRScannedResult = (TableLayout) findViewById(R.id.tlQRScannedResult);
        ivQRCode = (ImageView) findViewById(R.id.ivQRCode);
        rq = Volley.newRequestQueue(MerchantPaymentActivity.this);
        btnQRCodeScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator intentIntegrator = new IntentIntegrator(MerchantPaymentActivity.this);
                intentIntegrator
                        .setOrientationLocked(false)
                        .setBeepEnabled(true)
                        .addExtra(getResources().getString(R.string.pro_code), getResources().getString(R.string.scan_qr_code))
                        .initiateScan(IntentIntegrator.QR_CODE_TYPES);
            }
        });

        btnQRPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPayment();
            }
        });

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.Blockgration.Userwallet.R.menu.menu_main, menu);
        return true;
    }

    private void attemptPayment() {
        gethideKeyboard();
        etQrScanAmount.setError(null);
        cancel = false;
        checkPayAmount(etQrScanAmount.getText().toString());
        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteMerchantPay();
        }
    }

    public void gethideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etQrScanAmount.setError(getString(gasCheckLog.msg));
            focusView = etQrScanAmount;
            cancel = true;
        } else if (Integer.valueOf(etQrScanAmount.getText().toString()) < 10) {
            etQrScanAmount.setError("Enter min amount 10");
            focusView = etQrScanAmount;
            cancel = true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {
                CustomToast.showMessage(MerchantPaymentActivity.this, "Cancelled");
            } else {
                try {
                    String[] splitResult = result.toString().split(":");
                    Log.i("valueqrcode", result.getContents());
                    String encryptValue = splitResult[2].trim();
                    String finalResult = "";

                    if (encryptValue != null) {
                        encryptValue = encryptValue.replaceAll("\t", "");
                        encryptValue = encryptValue.replaceAll("\\n", "");
                        encryptValue = encryptValue.replaceAll("\r", "");
                        encryptValue = encryptValue.replace("Raw bytes", "");
                        Log.i("EncryptValue", encryptValue);

                        try {
                            finalResult = encryptValue;
                            addingResultToTableView(finalResult);
                        } catch (Exception e) {
                            CustomToast.showMessage(this, "Invalid QR");
                        }

                    } else {
                        CustomToast.showMessage(this, "Unsupported QR Format");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            CustomToast.showMessage(this, "Result is null");
        }
    }

    private void addingResultToTableView(String result) {
        tlQRScannedResult.removeAllViews();

        String[] resultArray = result.split("-");
        ArrayList<String> arrayListHead = new ArrayList<>();
        arrayListHead.add("Receiver Name");
        arrayListHead.add("Receiver Mobile");
        arrayListHead.add("Receiver Email");

        ArrayList<String> arrayListResult = new ArrayList<>();
        merchantId = resultArray[4].trim();
        arrayListResult.add(resultArray[1]);
        arrayListResult.add(resultArray[2]);
        arrayListResult.add(resultArray[3]);

        for (int i = 0; i < arrayListHead.size(); i++) {
            TableRow row = new TableRow(this);

            TextView textH = new TextView(this);
            TextView textC = new TextView(this);
            TextView textV = new TextView(this);

            textH.setText(arrayListHead.get(i));
            textC.setText(":  ");
            textV.setText(arrayListResult.get(i));
            textV.setTypeface(null, Typeface.BOLD);

            row.addView(textH);
            row.addView(textC);
            row.addView(textV);

            tlQRScannedResult.addView(row);
        }
        tlQRScannedResult.setVisibility(View.VISIBLE);
        ivQRCode.setVisibility(View.GONE);
        btnQRCodeScan.setText("Re-Scan");
        tvPleaseScan.setText("Are you sure you want to proceed to pay?");
        tvScanTitle.setText("Scanned successfully");
        etQrScanAmount.setVisibility(View.VISIBLE);
        btnQRPay.setVisibility(View.VISIBLE);
    }


    public void promoteMerchantPay() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("id", merchantId);
            jsonRequest.put("netAmount", etQrScanAmount.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("URLS", ApiURLNew.URL_PAY_AT_STORE);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_PAY_AT_STORE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loadDlg.dismiss();
                    Log.i("Send Money", response.toString());
                    try {
                        Log.i("Merchant Pay Response", response.toString());
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);
                            String successMessage = response.getString("message");
                            CustomToast.showMessage(MerchantPaymentActivity.this, successMessage);
                            etQrScanAmount.getText().clear();
                            sendRefresh();
                            finish();
                        } else if (code != null && code.equals("F03")) {
//                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
//                            loadDlg.dismiss();
//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);
                            String errorMessage = response.getString("message");
                            CustomToast.showMessage(MerchantPaymentActivity.this, errorMessage);
                        }

                    } catch (JSONException e) {
//                        loadDlg.dismiss();
                        CustomToast.showMessage(MerchantPaymentActivity.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
                    CustomToast.showMessage(MerchantPaymentActivity.this, NetworkErrorHandler.getMessage(error,MerchantPaymentActivity.this));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "123");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            rq.add(postReq);
        }

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

}
