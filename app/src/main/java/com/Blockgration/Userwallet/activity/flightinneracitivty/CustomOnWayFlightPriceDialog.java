package com.Blockgration.Userwallet.activity.flightinneracitivty;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;


/**
 * Created by Kashif-PC on 1/21/2017.
 */
public class CustomOnWayFlightPriceDialog extends AlertDialog.Builder {

    public CustomOnWayFlightPriceDialog(Context context, String fare) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_custom_oneway_price, null, false);


        TextView cadTotalFare = (TextView) viewDialog.findViewById(R.id.cadTotalFare);
        cadTotalFare.setText("Total Fare   : " + fare);

        this.setCancelable(false);

        this.setView(viewDialog);


    }
}

