package com.Blockgration.Userwallet.activity.shopping.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.shopping.InCartAdapter;
import com.Blockgration.Userwallet.activity.shopping.InCartListner;
import com.Blockgration.Userwallet.activity.shopping.PQCart;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.model.CurrentAccountModel;

import com.orm.query.Select;

import java.util.List;


/**
 * Created by Ksf on 4/6/2016.
 */
public class TeleBuyInCartFragment extends Fragment implements InCartListner {
    private PQCart cart = PQCart.getInstance();
    private ListView lvInCartItem;
    private ImageView imBuyNext;
    private TextView tvTotalRupees;
    private View rootView;
    private double totalCost = 0.0;
    private TextView tvBuyTotalTax;
    private String currencyCode;
    private List<CurrentAccountModel> currentAccountArray;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentAccountArray = Select.from(CurrentAccountModel.class).list();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_incart_products, container, false);
        tvTotalRupees = (TextView) rootView.findViewById(R.id.tvBuyTotalRupess);
        imBuyNext = (ImageView) rootView.findViewById(R.id.imBuyNext);
        lvInCartItem = (ListView) rootView.findViewById(R.id.hlvInCart);
        tvBuyTotalTax = (TextView) rootView.findViewById(R.id.tvBuyTotalTax);
        for (CurrentAccountModel model : currentAccountArray) {
            currencyCode = model.getCurrencyCode();

        }
        Log.i("curreny", currentAccountArray.get(0).getCurrencyCode());
        lvInCartItem.setAdapter(new InCartAdapter(getActivity(),this, currentAccountArray.get(0) ));
        updateView();

        tvBuyTotalTax.setText("Applicable Taxes -"+currentAccountArray.get(0).getCurrencyCode()+" 0/-");

        imBuyNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (cart.getProductsInCart().size() == 0) {
                    CustomToast.showMessage(getActivity(), "You have no products in cart.");
                }
                else{
                    TeleBuyDeleveryFragment deliveryFragment = new TeleBuyDeleveryFragment();
                    Bundle args = new Bundle();
                    args.putDouble("total_amount", Double.parseDouble(tvTotalRupees.getText().toString().replace(",","")));
                    deliveryFragment.setArguments(args);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(getTag());
                    fragmentTransaction.replace(R.id.frameInCart, deliveryFragment);
                    fragmentTransaction.commit();
                }

            }
        });
        return rootView;
    }

    private void updateView() {
        tvTotalRupees.setText(cart.getTotalCost());
    }

    private void clearCartBadge() {
        Intent intent = new Intent("cart-clear");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void taskCompleted() {
        updateView();
    }

    @Override
    public void selectAddress(String s) {

    }

    @Override
    public void closeCart() {

        clearCartBadge();
        getActivity().finish();
    }

    @Override
    public void deleteAddress(String addID, int pos) {

    }

    @Override
    public void editAddress(String addId, int pos) {

    }

}
