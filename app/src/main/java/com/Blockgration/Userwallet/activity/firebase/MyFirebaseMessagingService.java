package com.Blockgration.Userwallet.activity.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.SplashActivity;
//import in.msewa.vpayqwiktest.activity.SplashActivity;

/**
 * Created by Ksf on 10/24/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    private Bitmap remote_picture;

    Bitmap bitmap;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getFrom().equals("158229869873")) {

            String message = remoteMessage.getData().get("message");
            String imageUri = remoteMessage.getData().get("image");
            String title = remoteMessage.getData().get("title");
            String source = "";
            if (remoteMessage.getData().containsKey("notification_source")) {
                source = remoteMessage.getData().get("notification_source");
            }
            if (source != null && source.equals("niki")) {
            } else {
                if (imageUri != null && !imageUri.isEmpty()) {
                    bitmap = getBitmapFromUrl(imageUri);
                    sendNotificationImage(message, bitmap, title);
                }else
                {
                    sendNotification(message, title);
                }
            }


        }

    }

    private void sendNotificationImage(String messageBody, Bitmap image, String title) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(image)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(image))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentText(messageBody)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    /*
    *To get a Bitmap image from the URL received
    * */
    public Bitmap getBitmapFromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }


    private void sendNotification(String msg, String title) {
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent myIntent = new Intent(this, SplashActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setAutoCancel(true)
                .setContentText(msg);
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private void showNotification(String mes, String url, String title) {
        try {
            NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
            notiStyle.setSummaryText(mes);

            try {
                remote_picture = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
            } catch (IOException e) {
                e.printStackTrace();
            }

            notiStyle.bigPicture(remote_picture);

            NotificationManager notificationManager = (NotificationManager) MyFirebaseMessagingService.this
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            PendingIntent contentIntent = null;

            Intent gotoIntent = new Intent(getApplicationContext(), SplashActivity.class);
            contentIntent = PendingIntent.getActivity(getApplicationContext(),
                    (int) (Math.random() * 100), gotoIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                    getApplicationContext());

            Notification notification = mBuilder.setSmallIcon(R.drawable.ic_notification).setTicker(getResources().getString(R.string.app_name)).setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(mes))
                    .setContentIntent(contentIntent)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentText(mes)
                    .setStyle(notiStyle).build();

            notification.flags = Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(100, notification);//This will generate seperate notification each time server sends.

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
