package com.Blockgration.Userwallet.activity.flightinneracitivty;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.adapter.FlightListAdapter;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.FlightListModel;
import com.Blockgration.model.FlightModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.NetworkErrorHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Ksf on 9/30/2016.
 */
public class FlightListOnWayActivity extends AppCompatActivity {

    private RecyclerView lvListBus;
    private LoadingDialog loadingDialog;
    private String destinationCode, sourceCode;
    private String dateOfDep, flightClass;
    private int noOfAdult, noOfChild, noOfInfant;
    private int flightType;

    //Volley
    private String tag_json_obj = "json_travel";
    private LinearLayout llNoBus;
    private ArrayList<FlightListModel> flightListItem;

    //header
    private TextView tvHeaderBusListDate;
    private ImageView ivHeaderListDate;
    private JSONObject jsonRequest;

    private UserModel session = UserModel.getInstance();

    private String jsonToSend = "";
    private String jsonFareToSend = "";




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oneway_flight_list);
        loadingDialog = new LoadingDialog(FlightListOnWayActivity.this);
        llNoBus = (LinearLayout) findViewById(R.id.llNoBus);
        TextView tvNoBus = (TextView) findViewById(R.id.tvNoBus);
        ImageView ivNoBus = (ImageView) findViewById(R.id.ivNoBus);

        tvHeaderBusListDate = (TextView) findViewById(R.id.tvHeaderBusListDate);
        ivHeaderListDate = (ImageView) findViewById(R.id.ivHeaderListDate);
        ivHeaderListDate.setImageResource(R.drawable.ic_no_flight);
        ivHeaderListDate.setVisibility(View.INVISIBLE);

        //Setting for flight in bus xml file
        tvNoBus.setText(getResources().getString(R.string.no_flight));
        ivNoBus.setImageResource(R.drawable.ic_no_flight);


        noOfAdult = getIntent().getIntExtra("Adult", 0);
        noOfChild = getIntent().getIntExtra("Child", 0);
        noOfInfant = getIntent().getIntExtra("Infant", 0);
        flightClass = getIntent().getStringExtra("Class");
        destinationCode = getIntent().getStringExtra("destination");
        sourceCode = getIntent().getStringExtra("source");
        dateOfDep = getIntent().getStringExtra("date");
        flightType = getIntent().getIntExtra("flightType",0);
        lvListBus = (RecyclerView) findViewById(R.id.lvListBus);
        //press back button in toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getFlightList();

    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

    private void getFlightList() {
        loadingDialog.show();
        flightListItem = new ArrayList<>();
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("origin", sourceCode);
            jsonRequest.put("destination", destinationCode);
            jsonRequest.put("tripType", "OneWay");
            jsonRequest.put("cabin", flightClass);
            jsonRequest.put("adults", noOfAdult);
            jsonRequest.put("childs", noOfChild);
            jsonRequest.put("infants", noOfInfant);
            jsonRequest.put("traceId", "AYTM00011111111110002");
            jsonRequest.put("beginDate",dateOfDep);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }


        if (jsonRequest != null) {
            Log.i("jsonRequest", jsonRequest.toString());
            Log.i("URL", ApiURLNew.URL_GET_LIST_FLIGHT_ONE_WAY);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_GET_LIST_FLIGHT_ONE_WAY, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Flight List Response", response.toString());
                    try {

                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {

                            JSONObject jsonDetails = response.getJSONObject("details");

                            JSONArray jsonJourney = jsonDetails.getJSONArray("journeys");
                            JSONObject jsonJrnyObj = jsonJourney.getJSONObject(0);
                            JSONArray jsonSegmentArray = jsonJrnyObj.getJSONArray("segments");


                            if (jsonSegmentArray.length() != 0) {

                                for (int k = 0; k < jsonSegmentArray.length(); k++) {

                                    String jsonSegmentString = jsonSegmentArray.getJSONObject(k).toString();

                                    ArrayList<FlightModel> flightArray = new ArrayList<>();
                                    JSONObject segmentObj = jsonSegmentArray.getJSONObject(k);
                                    JSONArray bondArray = segmentObj.getJSONArray("bonds");

                                    JSONObject bonObj = bondArray.getJSONObject(0);
                                    JSONArray legArray = bonObj.getJSONArray("legs");

                                    for (int j = 0; j < legArray.length(); j++) {
                                        JSONObject d = legArray.getJSONObject(j);
                                        jsonToSend = legArray.getJSONObject(0).toString();

                                        String depAirportCode = d.getString("origin");
                                        String depTime = d.getString("departureTime");
                                        String arrivalAirportCode = d.getString("destination");
                                        String arrivalTime = d.getString("arrivalTime");

                                        String flightDuration = d.getString("duration");
                                        String flightName = d.getString("airlineName");
                                        String flightNo = d.getString("flightNumber");
                                        String flightImage = d.getString("airlineName");
                                        String flightCode = d.getString("aircraftCode");


                                        String flightArrivalTimeZone =  d.getString("arrivalDate");
                                        String flightDepartureTimeZone = d.getString("departureDate");

                                        String flightArrivalAirportName = d.getString("destination");
                                        String flightDepartureAirportName = d.getString("origin");

                                        String flightRule = "";

                                        FlightModel flightModel = new FlightModel(depAirportCode, depTime, arrivalAirportCode, arrivalTime, flightDuration, flightName,
                                                flightNo, flightImage, flightRule, flightCode, flightArrivalTimeZone, flightDepartureTimeZone, flightArrivalAirportName, flightDepartureAirportName,1);
                                        flightArray.add(flightModel);


                                    }
                                    JSONArray fareArray = segmentObj.getJSONArray("fare");
                                    JSONObject fareObj = fareArray.getJSONObject(0);
                                    JSONArray paxFaresArray = fareObj.getJSONArray("paxFares");
                                    JSONObject paxFaresObj = paxFaresArray.getJSONObject(0);

                                    jsonFareToSend = paxFaresArray.getJSONObject(0).toString();


                                    double flightActualBaseFare = paxFaresObj.getDouble("basicFare");
                                    double flightTaxFare = 0.0;
                                    double totalFare = 0.0;
                                    if(paxFaresObj.has("totalTax")){
                                        flightTaxFare = paxFaresObj.getDouble("totalTax") ;
                                    }
                                    if(paxFaresObj.has("totalFare")){
                                        totalFare = paxFaresObj.getDouble("totalFare");
                                    }

                                    FlightListModel flightModel = new FlightListModel(flightActualBaseFare, flightTaxFare, 0.0, 0.0, 0.0,
                                            totalFare, totalFare, "", flightArray, jsonSegmentString);
                                    flightListItem.add(flightModel);
                                }

                                ivHeaderListDate.setVisibility(View.VISIBLE);
                                tvHeaderBusListDate.setText(sourceCode + " " + getResources().getString(R.string.arrow) + " " + destinationCode + "\n" + dateOfDep);

                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
                                lvListBus.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
                                GridLayoutManager manager = new GridLayoutManager(FlightListOnWayActivity.this, 1);
                                lvListBus.setLayoutManager(manager);
                                FlightListAdapter flightListAdapter = new FlightListAdapter(FlightListOnWayActivity.this, flightListItem, sourceCode, destinationCode, "\n" +  dateOfDep, noOfAdult, noOfChild, noOfInfant, flightClass, jsonToSend,jsonFareToSend);
                                lvListBus.setAdapter(flightListAdapter);
                                loadingDialog.dismiss();
                            } else {
                                loadingDialog.dismiss();
                                llNoBus.setVisibility(View.VISIBLE);
                                lvListBus.setVisibility(View.GONE);
                            }
                        }
                        else if (code != null && code.equals("F03")) {
                            loadingDialog.dismiss();
                            showInvalidSessionDialog();
                        }
                        else{
                            loadingDialog.dismiss();
                            llNoBus.setVisibility(View.VISIBLE);
                            lvListBus.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        CustomToast.showMessage(getApplicationContext(), "Unexpected response from server");
                        loadingDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    loadingDialog.dismiss();
                }
            }) {


            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        }
    }

    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(FlightListOnWayActivity.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(FlightListOnWayActivity.this).sendBroadcast(intent);
    }

}
