package com.Blockgration.Userwallet.activity.shopping;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.androidquery.AQuery;

import com.Blockgration.Userwallet.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


/**
 * Created by payqwik on 2/2/2017.
 */
public class HomeSliderAdapter extends PagerAdapter {

    private Context context;
    private int[] banner;
    private LayoutInflater inflater;

    public HomeSliderAdapter(Context context, int[] banner) {
        this.banner = banner;
        this.context = context;
    }

    @Override
    public int getCount() {
        return banner.length;
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final ImageView ivBannerAdpter;
        inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View viewLayout = inflater.inflate(R.layout.home_slider_banner, container, false);
        container.addView(viewLayout, 0);
        ivBannerAdpter = (ImageView) viewLayout.findViewById(R.id.ivBannerAdapter);

//        Bitmap bitmap = BitmapHelper.decodeSampledBitmapFromResource(context.getResources(), banner[position], ivBannerAdpter.getMaxWidth(), ivBannerAdpter.getMaxHeight();
//        ivBannerAdpter.setImageBitmap(BitmapHelper.decodeSampledBitmapFromResource(context.getResources(), banner[position], ivBannerAdpter.getMaxWidth(), ivBannerAdpter.getMaxHeight()));

        Target mBackgroundTarget = new Target() {
            Bitmap mBitmap;

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                if (bitmap == null || bitmap.isRecycled())
                    return;

                mBitmap = bitmap;
                ivBannerAdpter.setImageBitmap(bitmap);

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                recycle();
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }

            /**
             * Recycle bitmap to free memory
             */
            private void recycle() {
                if (mBitmap != null && !mBitmap.isRecycled()) {
                    mBitmap.recycle();
                    mBitmap = null;
                    System.gc();
                }
            }
        };
        AQuery aq = new AQuery(context);
        aq.id(ivBannerAdpter).background(R.drawable.loading_image).image(banner[position]).getContext();
//        Picasso.with(context).load(banner[position]).priority(Picasso.Priority.HIGH).into(mBackgroundTarget);


        return viewLayout;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
