package com.Blockgration.Userwallet.activity.flightinneracitivty;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.adapter.FlightDetailAdapter;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.custom.NonScrollListView;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.FlightListModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.NetworkErrorHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Ksf on 10/13/2016.
 */

public class FlightPriceOneWayActivity extends AppCompatActivity {
    private FlightListModel flightListModel;
    private String sourceCode, destinationCode, dateOfJourney;
    private int adultNo, childNo, infantNo;
    private String flightClass;

    private Toolbar toolbar;
    private ImageButton ivBackBtn;

    private JSONObject segmentObject ;

    private NonScrollListView lvListFlightDetail;
    private Button btnBookFlight;

    private TextView tvflightDetailsTotalFare, tvflightDetailsTaxFees, tvflightDetailsBaseFare, tvflightDetailsPassengers, tvflightDetailsFareRule;
    private TextView tvFlightTotalTime;



    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;

    private JSONObject jsonReceived;
    private  JSONObject jsonFareReceived;

    //Volley Tag
    private String tag_json_obj = "json_flight";

    private UserModel session = UserModel.getInstance();


    private String jsonToSend = "";
    private String jsonFareToSend="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_price_oneway);
        loadDlg = new LoadingDialog(FlightPriceOneWayActivity.this);
        //press back button in toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        lvListFlightDetail = (NonScrollListView) findViewById(R.id.lvListFlightDetail);
        tvflightDetailsTotalFare = (TextView) findViewById(R.id.tvflightDetailsTotalFare);
        tvflightDetailsTaxFees = (TextView) findViewById(R.id.tvflightDetailsTaxFees);
        tvflightDetailsBaseFare = (TextView) findViewById(R.id.tvflightDetailsBaseFare);
        tvflightDetailsPassengers = (TextView) findViewById(R.id.tvflightDetailsPassengers);
        tvflightDetailsFareRule = (TextView) findViewById(R.id.tvflightDetailsFareRule);
        tvFlightTotalTime = (TextView) findViewById(R.id.tvFlightTotalTime);
        btnBookFlight = (Button) findViewById(R.id.btnBookFlight);

        //Should update this.

        flightListModel = getIntent().getParcelableExtra("FlightArray");

        try {
            segmentObject = new JSONObject(flightListModel.getFlightJsonString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        sourceCode = getIntent().getStringExtra("sourceCode");
        destinationCode = getIntent().getStringExtra("destinationCode");
        dateOfJourney = getIntent().getStringExtra("dateOfJourney");
        flightClass = getIntent().getStringExtra("flightClass");
        jsonToSend = getIntent().getStringExtra("jsonToSend");
        jsonFareToSend = getIntent().getStringExtra("jsonFareToSend");

        adultNo = getIntent().getIntExtra("adultNo", 0);
        childNo = getIntent().getIntExtra("childNo", 0);
        infantNo = getIntent().getIntExtra("infantNo", 0);

        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        View dateView = layoutInflater.inflate(R.layout.header_flight_detail, null);
        TextView tvHeaderBusListDate = (TextView) dateView.findViewById(R.id.tvHeaderBusListDate);
        ImageView ivHeaderListDate = (ImageView) dateView.findViewById(R.id.ivHeaderListDate);
        ImageView ivHeaderFlight = (ImageView) dateView.findViewById(R.id.ivHeaderFlight);
        TextView tvHeaderFlightName= (TextView) dateView.findViewById(R.id.tvHeaderFlightName);
        ivHeaderListDate.setImageResource(R.drawable.ic_no_flight);
        tvHeaderBusListDate.setText(sourceCode + " " + getResources().getString(R.string.arrow) + " " + destinationCode + dateOfJourney);
        tvHeaderFlightName.setText(flightListModel.getFlightListArray().get(0).getFlightName());
        ivHeaderFlight.setImageResource(AppMetadata.getFLightImage(flightListModel.getFlightListArray().get(0).getFlightCode()));
        lvListFlightDetail.addHeaderView(dateView);
        FlightDetailAdapter flightDetailAdapter = new FlightDetailAdapter(getApplicationContext(), flightListModel.getFlightListArray());
        lvListFlightDetail.setAdapter(flightDetailAdapter);

        tvflightDetailsTotalFare.setText(getResources().getString(R.string.rupease) + " " + flightListModel.getTotalFare());
        tvflightDetailsTaxFees.setText(getResources().getString(R.string.rupease) + " " + flightListModel.getFlightTaxFare() + "");
        tvflightDetailsBaseFare.setText(getResources().getString(R.string.rupease) + " " + flightListModel.getFlightActualBaseFare() + "");

        tvflightDetailsFareRule.setText(flightListModel.getFlightListArray().get(0).getFlightRule());

        tvFlightTotalTime.setText("");

        //ALL THREE
        if (adultNo != 0 && childNo != 0 && infantNo != 0) {
            tvflightDetailsPassengers.setText(adultNo + " Adults, " + childNo + " Children, " + infantNo + " Infant");
        }
        //ADULT AND CHILD
        else if (adultNo != 0 && childNo != 0 && infantNo == 0) {
            tvflightDetailsPassengers.setText(adultNo + " Adults, " + childNo + " Children");
        }
        //ADULT AND INFANT
        else if (adultNo != 0 && infantNo != 0 && childNo == 0) {
            tvflightDetailsPassengers.setText(adultNo + " Adults, " + infantNo + " Infant");
        }
        //CHILD AND INFANT
        else if (adultNo == 0 && childNo != 0 && infantNo != 0) {
            tvflightDetailsPassengers.setText(adultNo + " Children, " + infantNo + " Infant");
        }

        //ONLY ADULT
        else if (adultNo != 0 && childNo == 0 && infantNo == 0) {
            tvflightDetailsPassengers.setText(adultNo + " Adults ");
        }
        //ONLY INFANT
        else if (adultNo == 0 && childNo == 0 && infantNo != 0) {
            tvflightDetailsPassengers.setText(adultNo + " Adults, " + infantNo + " Infant");
        }
        //ONLY CHILD
        else if (adultNo == 0 && infantNo == 0 && childNo != 0) {
            tvflightDetailsPassengers.setText(childNo + " Children");
        }
        else {
            tvflightDetailsPassengers.setText("");
        }

        btnBookFlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptBook();
            }
        });


    }


    public void attemptBook() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("origin", sourceCode);
            jsonRequest.put("destination", destinationCode);
            jsonRequest.put("beginDate", dateOfJourney);
            jsonRequest.put("tripType", "OneWay");
            jsonRequest.put("cabin", flightClass);
            jsonRequest.put("adults", adultNo);
            jsonRequest.put("childs", childNo);
            jsonRequest.put("infants", infantNo);
            jsonRequest.put("traceId", "AYTM00011111111110002");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {


            Log.i("URL LOGIN", ApiURLNew.URL_FLIGHT_BOOK_ONEWAY);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_FLIGHT_BOOK_ONEWAY, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("LOgin Resonse", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {
                            loadDlg.dismiss();
                            fetchLatestPrice();

                        }else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        }

                        else {
                            String message = response.getString("message");
                            loadDlg.dismiss();
                            CustomToast.showMessage(getApplicationContext(), message);

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(FlightPriceOneWayActivity.this, NetworkErrorHandler.getMessage(error, FlightPriceOneWayActivity.this));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "23704F92F9C89AC418079642E45C9EBE351F3F280395C084D78BE8461F7683B2");
                    Log.i("Header", map.get("hash"));
                    return map;
                }

            };

            int socketTimeout = 600000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public void fetchLatestPrice() {
        loadDlg.show();


        try {
            jsonReceived = new JSONObject(jsonToSend);
            jsonFareReceived = new JSONObject(jsonFareToSend);
            jsonReceived.remove("cabinClasses");
            jsonReceived.remove("ssrDetails");
            jsonReceived.remove("aircraftCode");
            jsonReceived.remove("aircraftType");
            jsonReceived.remove("availableSeat");
            jsonReceived.remove("fareBasisCode");
            jsonReceived.remove("flightDesignator");
            jsonReceived.remove("flightDetailRefKey");
            jsonReceived.remove("group");
            jsonReceived.remove("remarks");
            jsonReceived.remove("providerCode");
            jsonReceived.remove("numberOfStops");

            jsonReceived.put("sessionId", session.getUserSessionId());



            jsonReceived.put("engineID",segmentObject.getString("engineID"));
            jsonReceived.put("basicFare",segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("basicFare"));
            jsonReceived.put("exchangeRate",segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("exchangeRate"));
            jsonReceived.put("baseTransactionAmount",segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("baseTransactionAmount"));
            jsonReceived.put("cancelPenalty",segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("cancelPenalty"));
            jsonReceived.put("changePenalty",segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("changePenalty"));
            jsonReceived.put("chargeCode","");
            jsonReceived.put("chargeType","FarePrice");
            jsonReceived.put("markUP",segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("markUP"));
            jsonReceived.put("paxType",segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getString("paxType"));
            jsonReceived.put("refundable", segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getBoolean("refundable"));

            jsonReceived.put("totalFare",segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalFare"));
            jsonReceived.put("totalTax",segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getDouble("totalTax"));
            jsonReceived.put("totalFareWithOutMarkUp",segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("totalFareWithOutMarkUp"));
            jsonReceived.put("totalTaxWithOutMarkUp",segmentObject.getJSONArray("fare").getJSONObject(0).getDouble("totalTaxWithOutMarkUp"));
            jsonReceived.put("isCache",segmentObject.getBoolean("isCache"));

            jsonReceived.put("isHoldBooking",segmentObject.getBoolean("isHoldBooking"));
            jsonReceived.put("isInternational",segmentObject.getBoolean("isInternational"));
            jsonReceived.put("isRoundTrip", segmentObject.getBoolean("isRoundTrip"));

            jsonReceived.put("isSpecial",segmentObject.getBoolean("isSpecial"));
            jsonReceived.put("isSpecialId",segmentObject.getBoolean("isSpecialId"));
            jsonReceived.put("journeyIndex",segmentObject.getInt("journeyIndex"));
            jsonReceived.put("nearByAirport",segmentObject.getBoolean("nearByAirport"));
            jsonReceived.put("searchId", segmentObject.getString("searchId"));

            jsonReceived.put("traceId","AYTM00011111111110002");
            jsonReceived.put("tripType","OneWay");
            jsonReceived.put("transactionAmount",segmentObject.getJSONArray("fare").getJSONObject(0).getJSONArray("paxFares").getJSONObject(0).getInt("transactionAmount"));

            jsonReceived.put("childs",childNo);
            jsonReceived.put("infants",infantNo);
            jsonReceived.put("adults",adultNo);


            jsonReceived.put("beginDate",dateOfJourney);
            jsonReceived.put("bondType",segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("boundType"));
            jsonReceived.put("fareRule",segmentObject.getString("fareRule"));

            jsonReceived.put("isBaggageFare", segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("isBaggageFare"));

            jsonReceived.put("isSSR",segmentObject.getJSONArray("bonds").getJSONObject(0).getBoolean("isSSR"));

            jsonReceived.put("itineraryKey",segmentObject.getString("itineraryKey"));
            jsonReceived.put("flightName",segmentObject.getString("engineID"));

            jsonReceived.put("journeyTime",segmentObject.getJSONArray("bonds").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("duration"));



        } catch (JSONException e) {
            e.printStackTrace();
            jsonReceived = null;
        }

        if (jsonReceived != null) {


            Log.i("URL", ApiURLNew.URL_FETCH_ONE_WAY_PRICE);
            Log.i("jsonReceived",jsonReceived.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_FETCH_ONE_WAY_PRICE, jsonReceived, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Price Resonse", response.toString());
                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {

                            JSONObject jsonDetails = response.getJSONObject("details");

                            JSONArray jsonJourney = jsonDetails.getJSONArray("journeys");
                            JSONObject jsonJrnyObj = jsonJourney.getJSONObject(0);
                            JSONArray jsonSegmentArray = jsonJrnyObj.getJSONArray("segments");

                            JSONObject jsonSegment = jsonSegmentArray.getJSONObject(0);
                            JSONArray fareArray = jsonSegment.getJSONArray("fare");
                            JSONObject fareObj = fareArray.getJSONObject(0);

                            Double totalPrice = fareObj.getDouble("totalFareWithOutMarkUp");
                            loadDlg.dismiss();
                            showPriceDialog(totalPrice+"");

                        }
                        else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        }
                        else{
                            loadDlg.dismiss();
                            if(response.has("message")){
                                String message = response.getString("message");
                                CustomToast.showMessage(FlightPriceOneWayActivity.this, message);
                            }else{
                                CustomToast.showMessage(FlightPriceOneWayActivity.this, "Error occurred");
                            }

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getApplicationContext(), getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(FlightPriceOneWayActivity.this, NetworkErrorHandler.getMessage(error, FlightPriceOneWayActivity.this));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "23704F92F9C89AC418079642E45C9EBE351F3F280395C084D78BE8461F7683B2");
                    Log.i("Header", map.get("hash"));
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(FlightPriceOneWayActivity.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(FlightPriceOneWayActivity.this).sendBroadcast(intent);
    }


    private void showPriceDialog(String price){

        CustomOnWayFlightPriceDialog builder = new CustomOnWayFlightPriceDialog(FlightPriceOneWayActivity.this, price);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent bookFormIntent = new Intent(getApplicationContext(),FlightBookingFormActivity.class);
                bookFormIntent.putExtra("FlightModel",flightListModel);
                bookFormIntent.putExtra("sourceCode", sourceCode);
                bookFormIntent.putExtra("destinationCode", destinationCode);
                bookFormIntent.putExtra("dateOfJourney", dateOfJourney);
                bookFormIntent.putExtra("adultNo", adultNo);
                bookFormIntent.putExtra("childNo", childNo);
                bookFormIntent.putExtra("infantNo", infantNo);
                bookFormIntent.putExtra("flightClass",flightClass);
                startActivity(bookFormIntent);

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });

        builder.show();

    }


}
