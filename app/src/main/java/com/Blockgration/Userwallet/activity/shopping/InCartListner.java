package com.Blockgration.Userwallet.activity.shopping;


public interface InCartListner {

	public void taskCompleted();
	public void selectAddress(String s);
	public void  closeCart();
	public void deleteAddress(String addId, int pos);
	public void editAddress(String addId, int pos);
}
