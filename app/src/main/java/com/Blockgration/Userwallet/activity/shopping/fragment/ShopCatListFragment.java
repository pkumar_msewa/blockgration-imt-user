package com.Blockgration.Userwallet.activity.shopping.fragment;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.shopping.CatAdapter;
import com.Blockgration.Userwallet.activity.shopping.LoadingDialog;
import com.Blockgration.Userwallet.activity.shopping.PQCart;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.model.CategoryListModel;
import com.Blockgration.model.ShoppingModel;
import com.Blockgration.model.UserModel;

import com.Blockgration.util.NetworkErrorHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Ksf on 3/27/2016.
 */
public class ShopCatListFragment extends Fragment{

    private View rootView;
    private TextView tvcart_point;
    private List<CategoryListModel> categoryArray;

    private List<ShoppingModel> shoppingArray;
    private RequestQueue rq;

    private String cartValue = "0";
    private CatAdapter catAdapter;
    private PQCart pqCart = PQCart.getInstance();


    //    private ShoppingAdapter shoppingAdapter;
    private UserModel session = UserModel.getInstance();

    private Button btnGoCart;
    private RecyclerView rvShopping;
    private LoadingDialog loadDlg;

    private static String selectValue;
    private JsonObjectRequest postReq;
    private String tag_json_obj = "json_events";
    private JSONObject jsonRequest;
    private String action;
    private ImageView ivFab;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            rq = Volley.newRequestQueue(getActivity());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        loadDlg = new LoadingDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_category_list, container, false);
        btnGoCart = (Button) rootView.findViewById(R.id.btnGoCart);
        ivFab = (ImageView)  rootView.findViewById(R.id.ivFab);
//        badge = new BadgeView(getActivity(), ivFab);
//        tvcart_point = (TextView) rootView.findViewById(R.id.tvshopping_cart_value);
//        tvcart_point.setText(cartValue);

        rvShopping = (RecyclerView) rootView.findViewById(R.id.rvShoppingCat);
        rvShopping.addItemDecoration(new SpacesItemDecoration(8));
        final GridLayoutManager manager = new GridLayoutManager(getActivity(), 3);
        rvShopping.setLayoutManager(manager);
        rvShopping.setHasFixedSize(true);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return catAdapter.isPositionHeader(position) ? manager.getSpanCount() : 1;
            }
        });

        categoryArray = new ArrayList<>();
        getCatList();

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
//        PayQwikApplication.getInstance().trackScreenView("Shopping");
        cartValue = String.valueOf(pqCart.getProductsInCart().size());
        Log.i("CARTONRESUME", cartValue);
//        tvcart_point.setText(cartValue);
        if(Integer.parseInt(cartValue)>0){
//            badge.setText(cartValue);
//            badge.show();
        }
    }
    public FragmentManager getHostFragmentManager() {
        FragmentManager fm = getFragmentManager();
        if (fm == null && isAdded() && getActivity()!=null) {
            fm = (getActivity()).getSupportFragmentManager();
        }
        return fm;
    }

    public void getCatList() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobile", session.getUserMobileNo());
            jsonRequest.put("countryName", "india");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("CATLIST", ApiURLNew.URL_SHOPPING_CATEGORY_LIST);
            Log.i("CATREQ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SHOPPING_CATEGORY_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    loadDlg.dismiss();
                    try {
                        Log.i("CATRES", jsonObj.toString());
                        String code = jsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            try {
                                if (!jsonObj.isNull("response")) {
                                    categoryArray.clear();
                                    CategoryListModel.deleteAll(CategoryListModel.class);

                                    JSONArray productJArray = jsonObj.getJSONArray("response");
                                    if (productJArray.length() != 0) {

                                        for (int i = 0; i < productJArray.length(); i++) {
                                            JSONObject c = productJArray.getJSONObject(i);
                                            JSONObject catId = c.getJSONObject("country");
                                            long cId = c.getLong("id");
                                            String cImage = c.getString("categoryImage");
                                            String cName = c.getString("categoryName");
                                            URL url=new URL(cImage.replaceAll(" ","%20"));
                                            cImage = url.toString();
                                            CategoryListModel categoryListModel = new CategoryListModel(String.valueOf(cId), cName, cImage);
                                            categoryArray.add((categoryListModel));

                                        }
                                        if (categoryArray.size() != 0) {
                                            try {
                                                catAdapter = new CatAdapter(getActivity(), categoryArray,getHostFragmentManager() );
                                                rvShopping.setAdapter(catAdapter);
                                            } catch (NullPointerException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    } else {
//                                        if (shoppingArray.size() != 0) {
//                                            Collections.sort(shoppingArray, new compPopulation());
//                                            shoppingAdapter = new ShopAdapter(ShoppingActivity.this, shoppingArray, ShoppingActivity.this, ShoppingActivity.this);
//                                            rvShopping.setAdapter(shoppingAdapter);
//                                        }
                                        CustomToast.showMessage(getActivity(), "No category Found");
                                        getActivity().finish();
                                    }
                                }
                            } catch (IndexOutOfBoundsException e) {
                                e.printStackTrace();
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
//                            getHighToLow(shoppingArray);
                            loadDlg.dismiss();

                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }


    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "6");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    public class compPopulation implements Comparator<ShoppingModel> {

        public int compare(ShoppingModel a, ShoppingModel b) {
            Log.i("MAx", "value");
            if (a.getpPrice() > b.getpPrice())
                return -1; // highest value first
            if (a.getpPrice() == b.getpPrice())
                return 0;
            return 1;
        }
    }

}
