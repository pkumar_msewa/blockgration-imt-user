package com.Blockgration.Userwallet.activity.transport;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.adapter.AgentCountryAdapter;
import com.Blockgration.adapter.PlacesAutoCompleteAdapter;
import com.Blockgration.Userwallet.activity.shopping.CustomSuccessDialog;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.MainActivity;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.model.AgentCityModel;
import com.Blockgration.model.AgentModel;
import com.Blockgration.model.DocumentModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.NetworkErrorHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AgentLoactionActivity extends AppCompatActivity {

    public static final String EXTRA_VALUE = "json";
    private JSONObject totalvalue;
    private ArrayList<AgentModel> agentModels;
    private ArrayList<AgentCityModel> agentCityModels;
    private GridView my_linear_layout;
    private TextView tvAgentCityLocation;
    private String tag_json_obj = "vakie";
    private AutoCompleteTextView metSelectCityAgent;
    private String agentID;
    private RadioButton radioButton;
    private AgentCityModel agentCityModel;
    private String AgentCity = "null";
    private PlacesAutoCompleteAdapter placesAutoCompleteAdapter;
    private RadioGroup rgRadiGroup;
    private ArrayList<String> agentModel;
    private UserModel session = UserModel.getInstance();
    private String radio = "";

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_loaction);
        overridePendingTransition(R.anim.trans_left_out, R.anim.trans_left_in);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.trans_right_out, R.anim.trans_right_in);
            }
        });

        try {
            totalvalue = new JSONObject(getIntent().getStringExtra(AgentLoactionActivity.EXTRA_VALUE));
            getJSonResponse();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        agentModel = new ArrayList<>();
        agentModels = new ArrayList<>();
        agentCityModels = new ArrayList<>();
        Button btnFundTransfer = (Button) findViewById(R.id.btnFundTransfer);
        my_linear_layout = (GridView) findViewById(R.id.agentGridView);
        tvAgentCityLocation = (TextView) findViewById(R.id.tvAgentCityLocation);
        metSelectCityAgent = (AutoCompleteTextView) findViewById(R.id.metSelectCityAgent);
        my_linear_layout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                metSelectCityAgent.setVisibility(View.VISIBLE);
                tvAgentCityLocation.setVisibility(View.VISIBLE);
                agentID = "";
                agentID = agentModels.get(i).getAgentId();
                metSelectCityAgent.setText("");
            }
        });
        btnFundTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (metSelectCityAgent.getText().toString() != null && !metSelectCityAgent.getText().toString().isEmpty()) {

                    try {
                        String amt = totalvalue.getString("sending_amount");
                        if (Double.parseDouble(amt) >= 1000) {
                            getCheckValidDoc();
                        } else {
                            checkThreshHoldAPi();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                if (radioButton == null) {
                    radio = "1234";
                } else {
                    radio = radioButton.getText().toString();
                }


//                final LoadingDialog loadingDialog = new LoadingDialog(AgentLoactionActivity.this);
//                loadingDialog.show();
//                Thread thread = new Thread() {
//                    @Override
//                    public void run() {
//                        try {
//                            Thread.sleep(2000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//
//                        runOnUiThread(new Runnable() {
//                                          @Override
//                                          public void run() {
//                                              loadingDialog.dismiss();
//                                              CustomAlertDialog customAlertDialog = new CustomAlertDialog(AgentLoactionActivity.this, R.string.dialog_title, "Send Money Successful");
//                                              customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//                                                  @Override
//                                                  public void onClick(DialogInterface dialogInterface, int i) {
//                                                      Intent intent = new Intent(AgentLoactionActivity.this, MainActivity.class);
//                                                      startActivity(intent);
//                                                  }
//                                              });
//                                              customAlertDialog.show();
//                                          }
//
//                                      }
//                        );
//                    }
//                };
//                thread.start();

            }
        });
        my_linear_layout.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                metSelectCityAgent.setVisibility(View.VISIBLE);
                tvAgentCityLocation.setVisibility(View.VISIBLE);
                agentID = "";
                metSelectCityAgent.setText("");
                agentID = agentModels.get(i).getAgentId();
                Log.i("AGENYLOCTION", agentID);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        rgRadiGroup = (RadioGroup)

                findViewById(R.id.rgRadiGroup);
        metSelectCityAgent.setOnTouchListener(new View.OnTouchListener()

        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                try {
                    Log.i("AGENT", agentID);
                    if (metSelectCityAgent.getText() != null && agentID != null) {
                        agentModel.clear();
                        placesAutoCompleteAdapter = new PlacesAutoCompleteAdapter(AgentLoactionActivity.this, android.R.layout.simple_list_item_1, new JSONObject().put("agent_id", agentID).put("destination_currency_id", totalvalue.getString("des_country_currency_id")), metSelectCityAgent.getText().toString());
                        metSelectCityAgent.setAdapter(placesAutoCompleteAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return false;
            }
        });
        metSelectCityAgent.setOnItemClickListener(new AdapterView.OnItemClickListener()

        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    totalvalue.remove("sub_agent_id");

                    totalvalue.put("sub_agent_id", placesAutoCompleteAdapter.getArrayStringValue().get(i).getAgent_City_Sub_Id());
                    getGetCityAgentOfficeAPi();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_out, R.anim.trans_right_in);
    }


    void getJSonResponse() {
        try {
            JSONObject jsonObject = new JSONObject().put("destination_country_id", totalvalue.getString("destination_country_id")).put("destination_currency_id", totalvalue.getString("des_country_currency_id"));
            final LoadingDialog loadingDialog = new LoadingDialog(AgentLoactionActivity.this);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SEND_MONEY_AGENT, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loadingDialog.dismiss();
                    Log.i("JSONRESPONSE", response.toString());
                    try {
                        if (response.getString("status_code").equals("1000")) {
                            Object o = response.get("results");
                            if ((o instanceof JSONArray)) {
                                Log.i("JSONLENGTH", String.valueOf(((JSONArray) o).length()));
                                for (int i = 0; i < ((JSONArray) o).length(); i++) {
                                    AgentModel agentModel = new AgentModel(((JSONArray) o).getJSONObject(i).getString("id"), ((JSONArray) o).getJSONObject(i).getString("name"), ((JSONArray) o).getJSONObject(i).getString("slug"), ((JSONArray) o).getJSONObject(i).getString("creditlimit"), ((JSONArray) o).getJSONObject(i).getString("legalName"), ((JSONArray) o).getJSONObject(i).getString("owner"), ((JSONArray) o).getJSONObject(i).getString("shortcode"), ((JSONArray) o).getJSONObject(i).getString("logo"), ((JSONArray) o).getJSONObject(i).getBoolean("agentOpen"));
                                    agentModels.add(agentModel);
                                }
                                Log.i("valueLength", String.valueOf(agentModels.size()));
                                AgentCountryAdapter countryAdapter = new AgentCountryAdapter(AgentLoactionActivity.this, agentModels);
                                my_linear_layout.setAdapter(countryAdapter);
                                countryAdapter.notifyDataSetChanged();
                            }
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(AgentLoactionActivity.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(AgentLoactionActivity.this, NetworkErrorHandler.getMessage(error, AgentLoactionActivity.this));
                }
            });
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    void getGetCityAgentOfficeAPi() {
        rgRadiGroup.setVisibility(View.VISIBLE);
        try {
            JSONObject jsonObject = new JSONObject().put("agent_id", agentID).put("destination_currency_id", totalvalue.getString("des_country_currency_id")).put("city_name", totalvalue.getString("sub_agent_id"));
            Log.i("valueJSON", jsonObject.toString());

            final LoadingDialog loadingDialog = new LoadingDialog(AgentLoactionActivity.this);
            loadingDialog.show();
            Log.i("URLSLS", ApiURLNew.URL_SEND_MONEY_AGENT_CITY_LOCATION);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SEND_MONEY_AGENT_CITY_LOCATION, jsonObject, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onResponse(JSONObject response) {
                    loadingDialog.dismiss();
                    Log.i("Response", response.toString());
                    try {

                        if (response.getString("status_code").equals("1000")) {
                            Object o = response.get("results");
                            if ((o instanceof JSONArray)) {
                                Log.i("JSONLENGTH", String.valueOf(((JSONArray) o).length()));
                                for (int i = 0; i < ((JSONArray) o).length(); i++) {

                                    agentModel.add(((JSONArray) o).getJSONObject(i).getString("sub_agent_id") + "," + ((JSONArray) o).getJSONObject(i).getString("name"));
                                }
                                Log.i("valueLength", String.valueOf(agentModels.size()));
                                ViewGroup hourButtonLayout = (ViewGroup) findViewById(R.id.rgRadiGroup);  // This is the id of the RadioGroup we defined
                                for (int i = 0; i < agentModel.size(); i++) {
                                    radioButton = new RadioButton(AgentLoactionActivity.this);
                                    radioButton.setId(i);
                                    String[] g = agentModel.get(i).split(",");
                                    totalvalue.put(g[1], g[0]);
                                    radioButton.setText(g[1]);
//                                    button.setChecked(i == currentHours); // Only select button with same index as currently selected number of hours// This is a custom button drawable, defined in XML
                                    hourButtonLayout.addView(radioButton);
                                }

                            }
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(AgentLoactionActivity.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(AgentLoactionActivity.this, NetworkErrorHandler.getMessage(error, AgentLoactionActivity.this));
                }
            });
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    void getCheckValidDoc() {

        try {
            JSONObject jsonObject = new JSONObject().put("id", totalvalue.getString("destination_country_id"));
            Log.i("valueJSON", jsonObject.toString());

            final LoadingDialog loadingDialog = new LoadingDialog(AgentLoactionActivity.this);
            loadingDialog.show();
            Log.i("URLSLS", ApiURLNew.URL_SEND_MONEY_CHECK_DOCUMENT_TYPE);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SEND_MONEY_CHECK_DOCUMENT_TYPE, jsonObject, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onResponse(JSONObject response) {
                    loadingDialog.dismiss();
                    Log.i("Response", response.toString());
                    try {

                        if (response.getString("status_code").equals("1000")) {

                            Object o = response.get("results");
                            if ((o instanceof JSONArray)) {
                                Log.i("JSONLENGTH", String.valueOf(((JSONArray) o).length()));
                                ArrayList<DocumentModel> jsonObjects = new ArrayList<>();
                                JSONObject jsonObject1 = new JSONObject();
                                for (int i = 0; i < ((JSONArray) o).length(); i++) {
                                    String id = ((JSONArray) o).getJSONObject(i).getString("id");
                                    String name = ((JSONArray) o).getJSONObject(i).getString("name");
                                    String issuedDate = ((JSONArray) o).getJSONObject(i).getString("issuedDate");
                                    String expiryDate = ((JSONArray) o).getJSONObject(i).getString("expiryDate");
                                    String issuedBy = ((JSONArray) o).getJSONObject(i).getString("issuedBy");
                                    DocumentModel documentModel = new DocumentModel(id, name, issuedDate, expiryDate, issuedBy);
                                    jsonObjects.add(documentModel);
                                }
                                if (jsonObjects.size() != 0) {
                                    Intent intenti = new Intent(AgentLoactionActivity.this, DocumentUpload.class);
                                    intenti.putExtra("Document", jsonObjects);
                                    intenti.putExtra("parmters", totalvalue.toString());
                                    intenti.putExtra("radio", radioButton.getText().toString());
                                    startActivity(intenti);
                                }

                            }
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(AgentLoactionActivity.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(AgentLoactionActivity.this, NetworkErrorHandler.getMessage(error, AgentLoactionActivity.this));
                }
            });
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    void getTranctionCreateAPi() {
        final LoadingDialog loadingDialog1 = new LoadingDialog(AgentLoactionActivity.this);
        loadingDialog1.show();
        try {
            JSONObject jsonObject = new JSONObject().put("destination_country_id", totalvalue.getString("destination_country_id")).put("service_type", 0);
            jsonObject.put("payment_type", "0");
            jsonObject.put("sending_amount", totalvalue.getString("sending_amount"));
            jsonObject.put("received_amount", totalvalue.getString("sending_amount"));
            jsonObject.put("exchange_rate", totalvalue.getString("exchangerate"));
            jsonObject.put("transfer_fees", totalvalue.getString("transfer_fee"));
            jsonObject.put("transfer_fee_id", totalvalue.getString("transfer_fee_id"));
            jsonObject.put("user_id", totalvalue.getString("user_id"));
            jsonObject.put("sessionId", session.getUserSessionId());
//            jsonObject.put("")
            if (radio.equals("")) {
                jsonObject.put("sub_agent_id", "1111");
            } else {
                jsonObject.put("sub_agent_id", totalvalue.getString(radio));
            }
            jsonObject.put("branch_id", "");
            Log.i("TRXNJSON", jsonObject.toString());

            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SEND_MONEY_GETCREATETRANSACTION, jsonObject, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onResponse(JSONObject response) {
                    loadingDialog1.dismiss();
                    Log.i("TRXNResponse", response.toString());
                    try {
                        if (response.getString("status_code").equals("1000")) {
                            CustomSuccessDialog customSuccessDialog = new CustomSuccessDialog(AgentLoactionActivity.this, "Transaction Successful", "");
                            customSuccessDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    sendRefresh();
                                    finishAllActivity();
//                                    Intent intent = new Intent(AgentLoactionActivity.this, MainActivity.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    finishAffinity();
//                                    startActivity(intent);
                                }
                            });
                            customSuccessDialog.show();
                        } else {
                            CustomToast.showMessage(AgentLoactionActivity.this, "Transaction Failed");
                        }
                    } catch (JSONException e) {
                        CustomToast.showMessage(AgentLoactionActivity.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog1.dismiss();
                    CustomToast.showMessage(AgentLoactionActivity.this, NetworkErrorHandler.getMessage(error, AgentLoactionActivity.this));
                }
            });
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, "json");
        } catch (JSONException e) {
            e.printStackTrace();
            loadingDialog1.dismiss();
        }

    }

    void checkThreshHoldAPi() {
        final LoadingDialog loadingDialog = new LoadingDialog(AgentLoactionActivity.this);
        loadingDialog.show();
        try {
            JSONObject jsonObject = new JSONObject().put("destination_country_id", totalvalue.getString("destination_country_id"));
            jsonObject.put("transaction_source", "Mobile");
            jsonObject.put("source_country_id", totalvalue.getString("source_country_id"));
            jsonObject.put("source_currency_id", totalvalue.getString("source_country_currency_id"));
            jsonObject.put("amount", totalvalue.getString("sending_amount"));
            jsonObject.put("beneficiary_id", totalvalue.getString("user_id"));
            jsonObject.put("destination_currency_id",totalvalue.getString("des_country_currency_id"));

            Log.i("THRESHJSON", jsonObject.toString());


            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SEND_MONEY_CHECK_THRESHOLD, jsonObject, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onResponse(JSONObject response) {
                    loadingDialog.dismiss();
                    Log.i("THRESHResponse", response.toString());
                    try {
                        if (response.getString("status_code").equals("1000")) {
                            getTranctionCreateAPi();
                        } else {
                            CustomToast.showMessage(AgentLoactionActivity.this, "Transaction Threshold Failed");
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(AgentLoactionActivity.this, getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(AgentLoactionActivity.this, NetworkErrorHandler.getMessage(error, AgentLoactionActivity.this));
                }
            });
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, "json");
        } catch (JSONException e) {
            e.printStackTrace();
            loadingDialog.dismiss();
        }

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(AgentLoactionActivity.this).sendBroadcast(intent);
    }

    private void finishAllActivity() {
        Intent intent = new Intent("action-done");
        intent.putExtra("action", "1");
        LocalBroadcastManager.getInstance(AgentLoactionActivity.this).sendBroadcast(intent);
        finish();
    }
}
