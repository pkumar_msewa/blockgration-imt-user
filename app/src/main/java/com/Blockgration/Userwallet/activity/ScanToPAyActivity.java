package com.Blockgration.Userwallet.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.Blockgration.Userwallet.R;
import com.Blockgration.fragment.mainmenufragment.FundTransferFragment;


/**
 * Created by Kashif-PC on 12/7/2016.
 */
public class ScanToPAyActivity extends AppCompatActivity {
    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    CharSequence TitlesEnglish[];
    int NumbOfTabs = 2;

    private View rootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        fragmentManager = getSupportFragmentManager();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TitlesEnglish = new CharSequence[]{"Email/Mobile"};
        mainPager = (ViewPager) findViewById(R.id.mainPager);
        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
        mSlidingTabLayout.setVisibility(View.GONE);
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs));
        mSlidingTabLayout.setupWithViewPager(mainPager);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.Blockgration.Userwallet.R.menu.menu_main, menu);
        return true;
    }


    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when HomePagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the HomePagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new FundTransferFragment();
            } else{
                return new FundTransferFragment();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return Titles.length;
    }

}

    @Override
    public void onResume() {
        super.onResume();
//        App.getInstance().trackScreenView("Mobile TopUp");
    }
}

