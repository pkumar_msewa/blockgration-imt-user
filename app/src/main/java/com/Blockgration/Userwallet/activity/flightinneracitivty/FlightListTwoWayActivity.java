package com.Blockgration.Userwallet.activity.flightinneracitivty;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.adapter.FlightRoundTripListAdapter;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.FlightListModel;
import com.Blockgration.model.FlightModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.NetworkErrorHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Ksf on 9/30/2016.
 */
public class FlightListTwoWayActivity extends AppCompatActivity {

    private RecyclerView rvFlightUp, rvFlightDown;
    private LoadingDialog loadingDialog;
    private String destinationCode, sourceCode;
    private String dateOfDep, flightClass, dateOfReturn;
    private int flightType;

    //Volley
    private String tag_json_obj = "json_travel";
    private LinearLayout llNoBus;
    private ArrayList<FlightListModel> flightListItem;

    //header
    private TextView tvHeadOnWardDate, tvHeadFlightReturnDate;
    private JSONObject jsonRequest;

    private TextView tvFlightRoundFinalPrice;
    private Button btnFlightRoundBook;

    private double upPrice = 0, downPrice = 0;
    private FlightListModel flightModelUp, flightModelDown;

    private String jSonUp, jSonDown;
    private int postUp = 0, postDown = 0;

    private int noOfAdult, noOfChild, noOfInfant;

    private UserModel session = UserModel.getInstance();


    private String jsonToSendUp = "";
    private String jsonFareToSendUp="";

    private String jsonToSendDown = "";
    private String jsonFareToSendDown="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(FlightListTwoWayActivity.this);
        setContentView(R.layout.activity_roundtrip_flight_list);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("flight-price"));
        llNoBus = (LinearLayout) findViewById(R.id.llNoBus);
        TextView tvNoBus = (TextView) findViewById(R.id.tvNoBus);
        ImageView ivNoBus = (ImageView) findViewById(R.id.ivNoBus);

        tvHeadOnWardDate = (TextView) findViewById(R.id.tvHeadOnWardDate);
        tvHeadFlightReturnDate = (TextView) findViewById(R.id.tvHeadFlightReturnDate);
        btnFlightRoundBook = (Button) findViewById(R.id.btnFlightRoundBook);
        tvFlightRoundFinalPrice = (TextView) findViewById(R.id.tvFlightRoundFinalPrice);
        //Setting for flight in bus xml file
        tvNoBus.setText(getResources().getString(R.string.no_flight));
        ivNoBus.setImageResource(R.drawable.ic_no_flight);

        noOfAdult = getIntent().getIntExtra("Adult", 0);
        noOfChild = getIntent().getIntExtra("Child", 0);
        noOfInfant = getIntent().getIntExtra("Infant", 0);
        flightClass = getIntent().getStringExtra("Class");
        destinationCode = getIntent().getStringExtra("destination");
        sourceCode = getIntent().getStringExtra("source");
        dateOfDep = getIntent().getStringExtra("date");
        flightType = getIntent().getIntExtra("flightType", 0);
        dateOfReturn = getIntent().getStringExtra("dateOfReturn");
        rvFlightDown = (RecyclerView) findViewById(R.id.rvFlightDown);
        rvFlightUp = (RecyclerView) findViewById(R.id.rvFlightUp);

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing_round);
        rvFlightUp.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        rvFlightDown.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        GridLayoutManager manager = new GridLayoutManager(FlightListTwoWayActivity.this, 1);
        GridLayoutManager manager2 = new GridLayoutManager(FlightListTwoWayActivity.this, 1);
        rvFlightUp.setLayoutManager(manager);
        rvFlightDown.setLayoutManager(manager2);


        //press back button in toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        setSupportActionBar(toolbar);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Button btnFlightRoundBook = (Button) findViewById(R.id.btnFlightRoundBook);
        btnFlightRoundBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flightModelUp != null && flightModelDown != null) {
                    FlightPriceTwoWayActivity.jsonUpString = jSonUp;
                    FlightPriceTwoWayActivity.jsonDownString= jSonDown;

                    Intent flightPriceIntent = new Intent(FlightListTwoWayActivity.this, FlightPriceTwoWayActivity.class);
                    flightPriceIntent.putExtra("flightUp", flightModelUp);
                    flightPriceIntent.putExtra("flightDown", flightModelDown);
                    flightPriceIntent.putExtra("postUp", postUp);
                    flightPriceIntent.putExtra("postDown", postDown);
                    flightPriceIntent.putExtra("dateOfDep", dateOfDep);
                    flightPriceIntent.putExtra("dateOfReturn", dateOfReturn);

                    flightPriceIntent.putExtra("sourceCode", sourceCode);
                    flightPriceIntent.putExtra("destinationCode", destinationCode);

                    flightPriceIntent.putExtra("adultNo", noOfAdult);
                    flightPriceIntent.putExtra("childNo", noOfChild);
                    flightPriceIntent.putExtra("infantNo", noOfInfant);

                    flightPriceIntent.putExtra("infantNo", noOfInfant);
                    flightPriceIntent.putExtra("flightClass", flightClass);



                    flightPriceIntent.putExtra("jsonFareToSendUp", jsonFareToSendUp);
                    flightPriceIntent.putExtra("jsonFareToSendDown", jsonFareToSendDown);

                    flightPriceIntent.putExtra("jsonToSendUp", jsonToSendUp);
                    flightPriceIntent.putExtra("jsonToSendDown", jsonToSendDown);




                    startActivity(flightPriceIntent);
                } else {
                    Toast.makeText(getApplicationContext(), "Flight not found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        getFlightList();

    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }


    private void getFlightList() {
        loadingDialog.show();

        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("origin", sourceCode);
            jsonRequest.put("destination", destinationCode);
            jsonRequest.put("tripType", "RoundTrip");
            jsonRequest.put("cabin", flightClass);
            jsonRequest.put("adults", noOfAdult);
            jsonRequest.put("childs", noOfChild);
            jsonRequest.put("infants", noOfInfant);
            jsonRequest.put("traceId", "AYTM00011111111110002");
            jsonRequest.put("endDate",dateOfReturn);
            jsonRequest.put("beginDate",dateOfDep);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }


        if (jsonRequest != null) {
            Log.i("jsonRequest", jsonRequest.toString());
            Log.i("URL", ApiURLNew.URL_GET_LIST_FLIGHT_ONE_WAY);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_GET_LIST_FLIGHT_ONE_WAY, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Flight List Response", response.toString());
                    try {

                        String code = response.getString("code");

                        if (code != null && code.equals("S00")) {

                            JSONObject jsonDetails = response.getJSONObject("details");

                            JSONArray jsonJourney = jsonDetails.getJSONArray("journeys");

                            Log.i("Journey Array size",jsonJourney.length()+"");
                            //OnWard Ticket
                            JSONObject jsonFirstJrnyObj = jsonJourney.getJSONObject(0);
                            JSONArray jsonSegmentFirstArray = jsonFirstJrnyObj.getJSONArray("segments");
                            if (jsonSegmentFirstArray.length() != 0) {
                                flightListItem = new ArrayList<>();

                                for (int k = 0; k < jsonSegmentFirstArray.length(); k++) {
                                    String jsonSegmentFirstString = jsonSegmentFirstArray.getJSONObject(k).toString();
                                    ArrayList<FlightModel> flightArray = new ArrayList<>();
                                    JSONObject segmentObj = jsonSegmentFirstArray.getJSONObject(k);
                                    JSONArray bondArray = segmentObj.getJSONArray("bonds");

                                    JSONObject bonObj = bondArray.getJSONObject(0);
                                    JSONArray legArray = bonObj.getJSONArray("legs");

                                    for (int j = 0; j < legArray.length(); j++) {
                                        JSONObject d = legArray.getJSONObject(j);
                                        jsonToSendUp = legArray.getJSONObject(0).toString();

                                        String depAirportCode = d.getString("origin");
                                        String depTime = d.getString("departureTime");
                                        String arrivalAirportCode = d.getString("destination");
                                        String arrivalTime = d.getString("arrivalTime");

                                        String flightDuration = d.getString("duration");
                                        String flightName = d.getString("airlineName");
                                        String flightNo = d.getString("flightNumber");
                                        String flightImage = d.getString("airlineName");
                                        String flightCode = d.getString("aircraftCode");

                                        String flightArrivalTimeZone =  d.getString("arrivalDate");

                                        String flightDepartureTimeZone = d.getString("departureDate");

                                        String flightArrivalAirportName = d.getString("destination");
                                        String flightDepartureAirportName = d.getString("origin");

                                        String flightRule = "";

                                        FlightModel flightModel = new FlightModel(depAirportCode, depTime, arrivalAirportCode, arrivalTime, flightDuration, flightName,
                                                flightNo, flightImage, flightRule, flightCode, flightArrivalTimeZone, flightDepartureTimeZone, flightArrivalAirportName, flightDepartureAirportName,1);
                                        flightArray.add(flightModel);


                                    }
                                    JSONArray fareArray = segmentObj.getJSONArray("fare");
                                    JSONObject fareObj = fareArray.getJSONObject(0);
                                    JSONArray paxFaresArray = fareObj.getJSONArray("paxFares");
                                    JSONObject paxFaresObj = paxFaresArray.getJSONObject(0);

                                    jsonFareToSendUp = paxFaresArray.getJSONObject(0).toString();

                                    double flightActualBaseFare = paxFaresObj.getDouble("basicFare");
                                    double flightTaxFare = 0.0;
                                    double totalFare = 0.0;
                                    if(paxFaresObj.has("totalTax")){
                                        flightTaxFare = paxFaresObj.getDouble("totalTax") ;
                                    }
                                    if(paxFaresObj.has("totalFare")){
                                        totalFare = paxFaresObj.getDouble("totalFare");
                                    }

                                    FlightListModel flightModel = new FlightListModel(flightActualBaseFare, flightTaxFare, 0.0, 0.0, 0.0,
                                            totalFare, totalFare, "", flightArray, jsonSegmentFirstString);
                                    flightListItem.add(flightModel);

                                    flightModelUp = flightListItem.get(0);
                                    upPrice = flightModelUp.getFlightNetFare();

                                }

//                                ivHeaderListDate.setVisibility(View.VISIBLE);
                                tvHeadOnWardDate.setText(sourceCode + " " + getResources().getString(R.string.arrow) + " " + destinationCode + "\n" + dateOfDep);

                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
                                rvFlightUp.addItemDecoration(new FlightListTwoWayActivity.SpacesItemDecoration(spacingInPixels));

                                GridLayoutManager manager = new GridLayoutManager(FlightListTwoWayActivity.this, 1);
                                rvFlightUp.setLayoutManager(manager);
                                FlightRoundTripListAdapter flightListAdapter = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListItem, sourceCode, destinationCode, "\n" +  dateOfDep, noOfAdult, noOfChild, noOfInfant);
                                rvFlightUp.setAdapter(flightListAdapter);
                                loadingDialog.dismiss();
                            } else {
                                loadingDialog.dismiss();
                                llNoBus.setVisibility(View.VISIBLE);
                                rvFlightUp.setVisibility(View.GONE);
                            }



                            //Return Ticket
                            JSONObject jsonSecondJrnyObj = jsonJourney.getJSONObject(1);
                            JSONArray jsonSecondSegmentArray = jsonSecondJrnyObj.getJSONArray("segments");

                            if (jsonSecondSegmentArray.length() != 0) {
                                flightListItem = new ArrayList<>();
                                for (int k = 0; k < jsonSecondSegmentArray.length(); k++) {
                                    String jsonSegmentSecondString = jsonSecondSegmentArray.getJSONObject(k).toString();
                                    ArrayList<FlightModel> flightArray = new ArrayList<>();
                                    JSONObject segmentObj = jsonSecondSegmentArray.getJSONObject(k);
                                    JSONArray bondArray = segmentObj.getJSONArray("bonds");

                                    JSONObject bonObj = bondArray.getJSONObject(0);
                                    JSONArray legArray = bonObj.getJSONArray("legs");

                                    for (int j = 0; j < legArray.length(); j++) {
                                        JSONObject d = legArray.getJSONObject(j);
                                        jsonToSendDown = legArray.getJSONObject(0).toString();

                                        String depAirportCode = d.getString("origin");
                                        String depTime = d.getString("departureTime");
                                        String arrivalAirportCode = d.getString("destination");
                                        String arrivalTime = d.getString("arrivalTime");

                                        String flightDuration = d.getString("duration");
                                        String flightName = d.getString("airlineName");
                                        String flightNo = d.getString("flightNumber");
                                        String flightImage = d.getString("airlineName");
                                        String flightCode = d.getString("aircraftCode");

                                        String flightArrivalTimeZone =  d.getString("arrivalDate");

                                        String flightDepartureTimeZone = d.getString("departureDate");


                                        String flightArrivalAirportName = d.getString("destination");
                                        String flightDepartureAirportName = d.getString("origin");

                                        String flightRule = "";

                                        FlightModel flightModel = new FlightModel(depAirportCode, depTime, arrivalAirportCode, arrivalTime, flightDuration, flightName,
                                                flightNo, flightImage, flightRule, flightCode, flightArrivalTimeZone, flightDepartureTimeZone, flightArrivalAirportName, flightDepartureAirportName,2);
                                        flightArray.add(flightModel);


                                    }
                                    JSONArray fareArray = segmentObj.getJSONArray("fare");
                                    JSONObject fareObj = fareArray.getJSONObject(0);
                                    JSONArray paxFaresArray = fareObj.getJSONArray("paxFares");
                                    JSONObject paxFaresObj = paxFaresArray.getJSONObject(0);

                                    jsonFareToSendDown = paxFaresArray.getJSONObject(0).toString();

                                    double flightActualBaseFare = paxFaresObj.getDouble("basicFare");
                                    double flightTaxFare = 0.0;
                                    double totalFare = 0.0;
                                    if(paxFaresObj.has("totalTax")){
                                        flightTaxFare = paxFaresObj.getDouble("totalTax") ;
                                    }
                                    if(paxFaresObj.has("totalFare")){
                                        totalFare = paxFaresObj.getDouble("totalFare");
                                    }

                                    FlightListModel flightModel = new FlightListModel(flightActualBaseFare, flightTaxFare, 0.0, 0.0, 0.0,
                                            totalFare, totalFare, "", flightArray, jsonSegmentSecondString);
                                    flightListItem.add(flightModel);

                                    flightModelDown = flightListItem.get(0);
                                    downPrice = flightModelDown.getFlightNetFare();
                                    updatePrice();


                                }

//                                ivHeaderListDate.setVisibility(View.VISIBLE);
                                tvHeadFlightReturnDate.setText(destinationCode + " " + getResources().getString(R.string.arrow) + " " + sourceCode + "\n" + dateOfReturn);

                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
                                rvFlightDown.addItemDecoration(new FlightListTwoWayActivity.SpacesItemDecoration(spacingInPixels));

                                GridLayoutManager manager = new GridLayoutManager(FlightListTwoWayActivity.this, 1);
                                rvFlightDown.setLayoutManager(manager);
                                FlightRoundTripListAdapter flightListAdapter = new FlightRoundTripListAdapter(FlightListTwoWayActivity.this, flightListItem, sourceCode, destinationCode, "\n" +  dateOfDep, noOfAdult, noOfChild, noOfInfant);
                                rvFlightDown.setAdapter(flightListAdapter);
                                loadingDialog.dismiss();
                            }else if (code != null && code.equals("F03")) {
                                loadingDialog.dismiss();
                                showInvalidSessionDialog();
                            }
                            else {
                                loadingDialog.dismiss();
                                llNoBus.setVisibility(View.VISIBLE);
                                rvFlightDown.setVisibility(View.GONE);
                            }
                        }else{
                            loadingDialog.dismiss();
                            llNoBus.setVisibility(View.VISIBLE);
                            rvFlightDown.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        CustomToast.showMessage(getApplicationContext(), "Unexpected response from server");
                        loadingDialog.dismiss();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    CustomToast.showMessage(getApplicationContext(), NetworkErrorHandler.getMessage(error, getApplicationContext()));
                    loadingDialog.dismiss();
                }
            }) {


            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int tripType = intent.getIntExtra("tripType", 0);
            if (tripType == 1) {
                upPrice = intent.getDoubleExtra("price-update", 0);
                flightModelUp = intent.getParcelableExtra("flightArray");
                postUp = intent.getIntExtra("position",0);
            } else {
                downPrice = intent.getDoubleExtra("price-update", 0);
                flightModelDown = intent.getParcelableExtra("flightArray");
                postDown = intent.getIntExtra("position",0);
            }
            updatePrice();

        }
    };


    private void updatePrice() {
        double finalPrice = upPrice + downPrice;
        tvFlightRoundFinalPrice.setText(getResources().getString(R.string.rupease) + " " + finalPrice);
    }


    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


    public void showInvalidSessionDialog() {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
        }
        CustomAlertDialog builder = new CustomAlertDialog(FlightListTwoWayActivity.this, R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(FlightListTwoWayActivity.this).sendBroadcast(intent);
    }

}
