package com.Blockgration.Userwallet.activity.giftcard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.fragment.fragmentgiftcard.DescriptionFragment;
import com.Blockgration.fragment.fragmentgiftcard.PurchaseFragment;
import com.Blockgration.fragment.fragmentgiftcard.TermAndConditionFragment;
import com.Blockgration.model.GiftCardCatModel;

import com.squareup.picasso.Picasso;


/**
 * Created by kashifimam on 21/02/17.
 */

public class GiftCardListActivity extends AppCompatActivity {


    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    ImageView ivGiftCardItem;
    CharSequence TitlesEnglish[] = {"Description", "Terms & Condition","Purchase"};
    int NumbOfTabs = 3;
    private GiftCardCatModel model;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getSupportFragmentManager();
        setContentView(R.layout.activity_tabs_layout);
        mainPager = (ViewPager) findViewById(R.id.mainPager);
        model = getIntent().getParcelableExtra("Model");
        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs_home);
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager, TitlesEnglish, NumbOfTabs, model));
        mSlidingTabLayout.setupWithViewPager(mainPager);
        ivGiftCardItem = (ImageView) findViewById(R.id.ivGiftCardItem);

        Log.i("GEGE", String.valueOf(model));
        Picasso.with(GiftCardListActivity.this).load(model.getCardImage()).into(ivGiftCardItem);

        android.support.v7.widget.Toolbar issuentoolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(issuentoolbar);
        ImageButton issueIb = (ImageButton) findViewById(R.id.ivBackBtn);
        issueIb.setVisibility(View.VISIBLE);
        issueIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //LocalBroadcastManager.getInstance(this).registerReceiver(mSignUpReceiver, new IntentFilter("sign-up"));
    }


    private BroadcastReceiver mSignUpReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mainPager.setCurrentItem(1);

        }
    };

    public static class ViewPagerAdapter extends FragmentPagerAdapter {
        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created

        GiftCardCatModel model;

        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, GiftCardCatModel model) {
            super(fm);
            this.model = model;
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 2) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("model", model);
                PurchaseFragment tab1 = new PurchaseFragment();
                tab1.setArguments(bundle);
                return tab1;
            } else if (position == 0) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("model", model);
                DescriptionFragment tab2 = new DescriptionFragment();
                tab2.setArguments(bundle);
                return tab2;
            } else {
                Bundle bundle = new Bundle();
                bundle.putParcelable("model", model);
                TermAndConditionFragment tab3 = new TermAndConditionFragment();
                tab3.setArguments(bundle);
                return tab3;
            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }
}





   /* private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private LoadingDialog loadDlg;
    private RecyclerView rvGiftCarditem;
    private LinearLayout llpbGiftCarditem;
    private String itemhash, itemImage, brandName;
    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;
    private AQuery aq;


    private GiftCardItemAdapter itemAdp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       LocalBroadcastManager.getInstance(this).registerReceiver(mCartReceiver, new IntentFilter("gift-cart-changed"));

        setContentView(R.layout.activity_gift_card_item);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        loadDlg = new LoadingDialog(this);
        setSupportActionBar(toolbar);
        rvGiftCarditem = (RecyclerView) findViewById(R.id.rvGiftCarditem);
        llpbGiftCarditem = (LinearLayout) findViewById(R.id.llpbGiftCarditem);
        itemhash = getIntent().getStringExtra("itemHash");
        itemImage = getIntent().getStringExtra("itemImage");
        brandName = getIntent().getStringExtra("brandName");
        ivBackBtn = (ImageButton) findViewById(R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getCardCategory();
    }

    public void getCardCategory() {
        llpbGiftCarditem.setVisibility(View.VISIBLE);
        rvGiftCarditem.setVisibility(View.GONE);
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("hash", itemhash);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("CardCatUrl", ApiUrl.URL_GIFT_CARD_LIST);
            Log.i("Request URL : ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiUrl.URL_GIFT_CARD_LIST, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("EventsCatResponse", jsonObj.toString());
                        String code = jsonObj.getString("success");
                        String message = jsonObj.getString("message");

                        if (code != null && code.equals("true")) {
                            List<GiftCardItemModel> cardListItemArray = new ArrayList<>();

                            JSONArray cardItemArray = jsonObj.getJSONArray("denominationObject");

                            for (int i = 0; i < cardItemArray.length(); i++) {
                                JSONObject c = cardItemArray.getJSONObject(i);
                                long itemID = c.getLong("denominationId");
                                int itemName = c.getInt("denominationName");
                                String itemSkuID = c.getString("skuId");
                                String itemType = c.getString("type");
                                String itemValueType = c.getString("valueType");


                                GiftCardItemModel giftCardItemModel = new GiftCardItemModel(itemID, itemName, itemSkuID, itemType, itemValueType);
                                cardListItemArray.add(giftCardItemModel);

                            }
                            if (cardListItemArray != null && cardListItemArray.size() != 0) {
                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_space_double);
                                rvGiftCarditem.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
                                GridLayoutManager manager = new GridLayoutManager(getApplicationContext(), 1);
                                rvGiftCarditem.setLayoutManager(manager);
                                rvGiftCarditem.setHasFixedSize(true);

                                itemAdp = new GiftCardItemAdapter(cardListItemArray, GiftCardListActivity.this, itemImage, itemhash, brandName);
                                rvGiftCarditem.setAdapter(itemAdp);
                                llpbGiftCarditem.setVisibility(View.GONE);
                                rvGiftCarditem.setVisibility(View.VISIBLE);

                            } else {
                                Toast.makeText(getApplicationContext(), "Empty Array", Toast.LENGTH_SHORT).show();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    CustomToast.showMessage(GiftCardListActivity.this, NetworkErrorHandler.getMessage(error, GiftCardListActivity.this));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.shopping_menu, menu);
//        MenuItem menuItem = menu.findItem(R.id.menuCart);
//
//        menuItem.setIcon(buildCounterDrawable(gIftCart.getProductsInCartArray().size(), R.drawable.shopping_cart));
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.menuCart) {
//            startActivity(new Intent(GiftCardListActivity.this, GiftCartListActivity.class));
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    private Drawable buildCounterDrawable(int count, int backgroundImageId) {
//        LayoutInflater inflater = LayoutInflater.from(this);
//        View view = inflater.inflate(R.layout.layout_cart_icon, null);
//        view.setBackgroundResource(backgroundImageId);
//
//        if (count == 0) {
//            View counterTextPanel = view.findViewById(R.id.cartCount);
//            counterTextPanel.setVisibility(View.GONE);
//        } else {
//            TextView textView = (TextView) view.findViewById(R.id.cartCount);
//            textView.setText("" + count);
//        }
//
//        view.measure(
//                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
//                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
//        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
//
//        view.setDrawingCacheEnabled(true);
//        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
//        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
//        view.setDrawingCacheEnabled(false);
//
//        return new BitmapDrawable(getResources(), bitmap);
//    }
//
//    private BroadcastReceiver mCartReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            invalidateOptionsMenu();
//
//        }
//    };
//
//
//    @Override
//    protected void onDestroy() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mCartReceiver);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        getCardCategory();
 }*/

