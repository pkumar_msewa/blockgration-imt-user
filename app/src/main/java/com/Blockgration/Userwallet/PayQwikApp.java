package com.Blockgration.Userwallet;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.Blockgration.model.BillPayOperatorModel;
import com.Blockgration.pqgeolocation.activity.PQApplication;
import com.orm.SugarContext;

import java.util.List;


/**
 * Created by Ksf on 7/14/2016.
 */
public class PayQwikApp extends Application {

    public static final String TAG = PayQwikApp.class.getSimpleName();
    private RequestQueue mRequestQueue;
    private static PayQwikApp mInstance;

    private List<BillPayOperatorModel> operatorData;


    public void storeData(List<BillPayOperatorModel> operatorData) {
        this.operatorData = operatorData;
    }

    public List<BillPayOperatorModel> getData() {
        return operatorData;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
        mInstance = this;
        PQApplication.mainUrl = "http://173.240.8.56:8034/NotifyMe/ws/api/v1/wallet/en/";
        PQApplication.secretKey = "2DFF462E1DC9D398D456C8BC2B8AD518CF2F95FF";
        PQApplication.tokenKey = "MTUwNzcxNTkxNDAwMw==";
        PQApplication.bitmap = R.mipmap.ic_launcher;
        PQApplication.appName = "Vpayqwik-Wallet";
        PQApplication.init(this);

    }


    public static synchronized PayQwikApp getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}