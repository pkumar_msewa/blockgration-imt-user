package com.Blockgration.beacon;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;

/**
 * Created by Ksf on 3/27/2016.
 */
public class BeaconWebview extends AppCompatActivity {
    private View rootView;
    private WebView webview;
    private String url = "";
    private ProgressBar pbWebView;
    private LinearLayout llWebError;
    private TextView tvWebError;
    private Toolbar toolbar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_travel);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                finish();
                onBackPressed();
            }
        });
        url = getIntent().getStringExtra("URL");
        webview = (WebView) findViewById(R.id.wvMain);
        pbWebView = (ProgressBar) findViewById(R.id.pbWebView);
        llWebError = (LinearLayout) findViewById(R.id.llWebError);
        tvWebError = (TextView) findViewById(R.id.tvWebError);

        webview.setVisibility(View.GONE);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
        webview.getSettings().setDomStorageEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        webview.setWebChromeClient(new WebChromeClient() {

            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    webview.setVisibility(View.VISIBLE);
                    pbWebView.setVisibility(View.GONE);
                } else {
                    webview.setVisibility(View.GONE);
                    pbWebView.setVisibility(View.VISIBLE);
                }
            }
        });

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    view.loadUrl(request.getUrl().toString());
                } else {
                    view.loadUrl(url);
                }
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                webview.setVisibility(View.VISIBLE);
                pbWebView.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                 Handle the error
                view.loadData("", "text/html", "UTF-8");
                pbWebView.setVisibility(View.GONE);
                llWebError.setVisibility(View.VISIBLE);
                tvWebError.setText(description);
            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                view.loadData("", "text/html", "UTF-8");
                pbWebView.setVisibility(View.GONE);
                llWebError.setVisibility(View.VISIBLE);
                tvWebError.setText(rerr.getDescription());
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }


        });
        webview.loadUrl(url);
    }
}
