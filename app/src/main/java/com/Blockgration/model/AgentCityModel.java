package com.Blockgration.model;

import com.orm.SugarRecord;

/**
 * Created by Amrutha-PC on 10-Mar-17.
 */

public class AgentCityModel extends SugarRecord {
    private String agent_Name;
    private String agent_City_Sub_Id;

    public AgentCityModel(String agent_Name, String agent_City_Sub_Id) {
        this.agent_Name = agent_Name;
        this.agent_City_Sub_Id = agent_City_Sub_Id;
    }

    public String getAgent_Name() {
        return agent_Name;
    }

    public void setAgent_Name(String agent_Name) {
        this.agent_Name = agent_Name;
    }

    public String getAgent_City_Sub_Id() {
        return agent_City_Sub_Id;
    }

    public void setAgent_City_Sub_Id(String agent_City_Sub_Id) {
        this.agent_City_Sub_Id = agent_City_Sub_Id;
    }
}
