package com.Blockgration.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ksf on 10/11/2016.
 */
public class FlightModel implements Parcelable {

    //Arrival and Departure
    private String depAirportCode;
    private String depTime;
    private String arrivalAirportCode;
    private String arrivalTime;
    private String flightDuration;

    private String flightArrivalTimeZone;
    private String flightDepartureTimeZone;

    private String flightArrivalAirportName;
    private String flightDepartureAirportName;

    //Flight name and no
    private String flightName;
    private String flightNo;
    private String flightImage;
    private String flightRule;
    private String flightCode;

    private int tripType;


    public FlightModel(String depAirportCode, String depTime, String arrivalAirportCode, String arrivalTime, String flightDuration, String flightName,
                       String flightNo, String flightImage, String flightRule, String flightCode, String flightArrivalTimeZone, String flightDepartureTimeZone,String flightArrivalAirportName,String flightDepartureAirportName,int tripType) {

        this.depAirportCode = depAirportCode;
        this.depTime = depTime;
        this.arrivalAirportCode = arrivalAirportCode;
        this.arrivalTime = arrivalTime;
        this.flightDuration = flightDuration;
        this.flightName = flightName;
        this.flightArrivalTimeZone = flightArrivalTimeZone;
        this.flightDepartureTimeZone = flightDepartureTimeZone;

        this.flightNo = flightNo;
        this.flightImage = flightImage;
        this.flightRule = flightRule;
        this.flightCode = flightCode;

        this.flightArrivalAirportName = flightArrivalAirportName;
        this.flightDepartureAirportName = flightDepartureAirportName;
        this.tripType = tripType;
    }

    public String getDepAirportCode() {
        return depAirportCode;
    }

    public void setDepAirportCode(String depAirportCode) {
        this.depAirportCode = depAirportCode;
    }

    public String getDepTime() {
        return depTime;
    }

    public void setDepTime(String depTime) {
        this.depTime = depTime;
    }

    public String getArrivalAirportCode() {
        return arrivalAirportCode;
    }

    public void setArrivalAirportCode(String arrivalAirportCode) {
        this.arrivalAirportCode = arrivalAirportCode;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getFlightDuration() {
        return flightDuration;
    }

    public void setFlightDuration(String flightDuration) {
        this.flightDuration = flightDuration;
    }

    public String getFlightArrivalTimeZone() {
        return flightArrivalTimeZone;
    }

    public void setFlightArrivalTimeZone(String flightArrivalTimeZone) {
        this.flightArrivalTimeZone = flightArrivalTimeZone;
    }

    public String getFlightDepartureTimeZone() {
        return flightDepartureTimeZone;
    }

    public void setFlightDepartureTimeZone(String flightDepartureTimeZone) {
        this.flightDepartureTimeZone = flightDepartureTimeZone;
    }

    public String getFlightArrivalAirportName() {
        return flightArrivalAirportName;
    }

    public void setFlightArrivalAirportName(String flightArrivalAirportName) {
        this.flightArrivalAirportName = flightArrivalAirportName;
    }

    public String getFlightDepartureAirportName() {
        return flightDepartureAirportName;
    }

    public void setFlightDepartureAirportName(String flightDepartureAirportName) {
        this.flightDepartureAirportName = flightDepartureAirportName;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getFlightImage() {
        return flightImage;
    }

    public void setFlightImage(String flightImage) {
        this.flightImage = flightImage;
    }

    public String getFlightRule() {
        return flightRule;
    }

    public void setFlightRule(String flightRule) {
        this.flightRule = flightRule;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }

    public int getTripType() {
        return tripType;
    }

    public void setTripType(int tripType) {
        this.tripType = tripType;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.depAirportCode);
        dest.writeString(this.depTime);
        dest.writeString(this.arrivalAirportCode);
        dest.writeString(this.arrivalTime);
        dest.writeString(this.flightDuration);
        dest.writeString(this.flightArrivalTimeZone);
        dest.writeString(this.flightDepartureTimeZone);
        dest.writeString(this.flightArrivalAirportName);
        dest.writeString(this.flightDepartureAirportName);
        dest.writeString(this.flightName);
        dest.writeString(this.flightNo);
        dest.writeString(this.flightImage);
        dest.writeString(this.flightRule);
        dest.writeString(this.flightCode);
        dest.writeInt(this.tripType);
    }

    protected FlightModel(Parcel in) {
        this.depAirportCode = in.readString();
        this.depTime = in.readString();
        this.arrivalAirportCode = in.readString();
        this.arrivalTime = in.readString();
        this.flightDuration = in.readString();
        this.flightArrivalTimeZone = in.readString();
        this.flightDepartureTimeZone = in.readString();
        this.flightArrivalAirportName = in.readString();
        this.flightDepartureAirportName = in.readString();
        this.flightName = in.readString();
        this.flightNo = in.readString();
        this.flightImage = in.readString();
        this.flightRule = in.readString();
        this.flightCode = in.readString();
        this.tripType = in.readInt();
    }

    public static final Creator<FlightModel> CREATOR = new Creator<FlightModel>() {
        @Override
        public FlightModel createFromParcel(Parcel source) {
            return new FlightModel(source);
        }

        @Override
        public FlightModel[] newArray(int size) {
            return new FlightModel[size];
        }
    };
}
