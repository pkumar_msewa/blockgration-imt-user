package com.Blockgration.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 9/8/2016.
 */
public class CategoryListModel extends SugarRecord{
    private String categoryCode;
    private String categoryName;
    private String categoryImage;


    public CategoryListModel(String categoryCode, String categoryName, String categoryImage){
        this.categoryCode = categoryCode;
        this.categoryName = categoryName;
        this.categoryImage = categoryImage;

    }

    public String getcategoryCode() {
        return categoryCode;
    }

    public void setcategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getcategoryName() {
        return categoryName;
    }

    public void setcategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getGetCategoryImage() {
        return categoryImage;
    }

    public void setGetCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }
}
