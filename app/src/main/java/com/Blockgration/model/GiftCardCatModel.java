package com.Blockgration.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kashifimam on 21/02/17.
 */

public class GiftCardCatModel implements Parcelable {
    private String cardHash;
    private String cardName;
    private String cardImage;
    private String cardDescription;
    private String cardTerms;
    private String cardMessage;
    private String cardWebsite;
    private boolean cardActive;
    private boolean cardOnline;
    private boolean cardSuccess;

    public GiftCardCatModel(String cardHash, String cardName, String cardImage, String cardDescription, String cardTerms, String cardMessage, String cardWebsite, boolean cardActive, boolean cardOnline, boolean cardSuccess) {
        this.cardHash = cardHash;
        this.cardName = cardName;
        this.cardImage = cardImage;
        this.cardDescription = cardDescription;
        this.cardTerms = cardTerms;
        this.cardMessage = cardMessage;
        this.cardWebsite = cardWebsite;
        this.cardActive = cardActive;
        this.cardOnline = cardOnline;
        this.cardSuccess = cardSuccess;
    }

    protected GiftCardCatModel(Parcel in) {
        cardHash = in.readString();
        cardName = in.readString();
        cardImage = in.readString();
        cardDescription = in.readString();
        cardTerms = in.readString();
        cardMessage = in.readString();
        cardWebsite = in.readString();
        cardActive = in.readByte() != 0;
        cardOnline = in.readByte() != 0;
        cardSuccess = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cardHash);
        dest.writeString(cardName);
        dest.writeString(cardImage);
        dest.writeString(cardDescription);
        dest.writeString(cardTerms);
        dest.writeString(cardMessage);
        dest.writeString(cardWebsite);
        dest.writeByte((byte) (cardActive ? 1 : 0));
        dest.writeByte((byte) (cardOnline ? 1 : 0));
        dest.writeByte((byte) (cardSuccess ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GiftCardCatModel> CREATOR = new Creator<GiftCardCatModel>() {
        @Override
        public GiftCardCatModel createFromParcel(Parcel in) {
            return new GiftCardCatModel(in);
        }

        @Override
        public GiftCardCatModel[] newArray(int size) {
            return new GiftCardCatModel[size];
        }
    };

    public String getCardHash() {
        return cardHash;
    }

    public void setCardHash(String cardHash) {
        this.cardHash = cardHash;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardImage() {
        return cardImage;
    }

    public void setCardImage(String cardImage) {
        this.cardImage = cardImage;
    }

    public String getCardDescription() {
        return cardDescription;
    }

    public void setCardDescription(String cardDescription) {
        this.cardDescription = cardDescription;
    }

    public String getCardTerms() {
        return cardTerms;
    }

    public void setCardTerms(String cardTerms) {
        this.cardTerms = cardTerms;
    }

    public String getCardMessage() {
        return cardMessage;
    }

    public void setCardMessage(String cardMessage) {
        this.cardMessage = cardMessage;
    }

    public String getCardWebsite() {
        return cardWebsite;
    }

    public void setCardWebsite(String cardWebsite) {
        this.cardWebsite = cardWebsite;
    }

    public boolean isCardActive() {
        return cardActive;
    }

    public void setCardActive(boolean cardActive) {
        this.cardActive = cardActive;
    }

    public boolean isCardOnline() {
        return cardOnline;
    }

    public void setCardOnline(boolean cardOnline) {
        this.cardOnline = cardOnline;
    }

    public boolean isCardSuccess() {
        return cardSuccess;
    }

    public void setCardSuccess(boolean cardSuccess) {
        this.cardSuccess = cardSuccess;
    }
}
