package com.Blockgration.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Hitesh on 22-Sep-17.
 */

public class SmSCategoriesModel extends SugarRecord implements Parcelable{

    String c_category_key;
    String c_category_name;
    String c_parent_key;

    public SmSCategoriesModel(String c_category_key, String c_category_name, String c_parent_key) {
        this.c_category_key = c_category_key;
        this.c_category_name = c_category_name;
        this.c_parent_key = c_parent_key;
    }

    protected SmSCategoriesModel(Parcel in) {
        c_category_key = in.readString();
        c_category_name = in.readString();
        c_parent_key = in.readString();
    }

    public static final Creator<SmSCategoriesModel> CREATOR = new Creator<SmSCategoriesModel>() {
        @Override
        public SmSCategoriesModel createFromParcel(Parcel in) {
            return new SmSCategoriesModel(in);
        }

        @Override
        public SmSCategoriesModel[] newArray(int size) {
            return new SmSCategoriesModel[size];
        }
    };

    public String getC_category_key() {
        return c_category_key;
    }

    public void setC_category_key(String c_category_key) {
        this.c_category_key = c_category_key;
    }

    public String getC_category_name() {
        return c_category_name;
    }

    public void setC_category_name(String c_category_name) {
        this.c_category_name = c_category_name;
    }

    public String getC_parent_key() {
        return c_parent_key;
    }

    public void setC_parent_key(String c_parent_key) {
        this.c_parent_key = c_parent_key;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(c_category_key);
        dest.writeString(c_category_name);
        dest.writeString(c_parent_key);
    }
}
