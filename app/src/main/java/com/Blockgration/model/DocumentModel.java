package com.Blockgration.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Amrutha-PC on 10-Mar-17.
 */

public class DocumentModel implements Parcelable {
    private String docId;
    private String name;
    private String issuedDate;
    private String expiryDate;
    private String issuedBy;

    public DocumentModel(String docId, String name, String issuedDate, String expiryDate, String issuedBy) {
        this.docId = docId;
        this.name = name;
        this.issuedDate = issuedDate;
        this.expiryDate = expiryDate;
        this.issuedBy = issuedBy;
    }

    protected DocumentModel(Parcel in) {
        docId = in.readString();
        name = in.readString();
        issuedDate = in.readString();
        expiryDate = in.readString();
        issuedBy = in.readString();
    }

    public static final Creator<DocumentModel> CREATOR = new Creator<DocumentModel>() {
        @Override
        public DocumentModel createFromParcel(Parcel in) {
            return new DocumentModel(in);
        }

        @Override
        public DocumentModel[] newArray(int size) {
            return new DocumentModel[size];
        }
    };

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(String issuedDate) {
        this.issuedDate = issuedDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(docId);
        parcel.writeString(name);
        parcel.writeString(issuedDate);
        parcel.writeString(expiryDate);
        parcel.writeString(issuedBy);
    }
}
