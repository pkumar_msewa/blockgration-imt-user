package com.Blockgration.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 5/5/2016.
 */
public class QwikPayModel extends SugarRecord {
    public String payName;
    public String payDate;
    public String payDes;
    public boolean isfav;
    public String payAmount;
    public String payRef;


    public QwikPayModel(){

    }

    public QwikPayModel(String payName, String payDate, String payDes, String payAmount, boolean isfav, String payRef){
        this.payName = payName;
        this.isfav = isfav;
        this.payDate = payDate;
        this.payAmount = payAmount;
        this.payDes = payDes;
        this.payRef = payRef;
    }

    public String getPayRef() {
        return payRef;
    }

    public void setPayRef(String payRef) {
        this.payRef = payRef;
    }

    public String getPayName() {
        return payName;
    }

    public void setPayName(String payName) {
        this.payName = payName;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getPayDes() {
        return payDes;
    }

    public void setPayDes(String payDes) {
        this.payDes = payDes;
    }

    public boolean isfav() {
        return isfav;
    }

    public void setIsfav(boolean isfav) {
        this.isfav = isfav;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }
}
