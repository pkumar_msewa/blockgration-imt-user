package com.Blockgration.model;

/**
 * Created by Ksf on 3/16/2016.
 */
public class MainMenuModel {
    private String menuTitle;
    private int menuImage;
    private int colorCode;

    public MainMenuModel(String menuTitle, int menuImage, int colorCode){
        this.menuImage = menuImage;
        this.menuTitle = menuTitle;
        this.colorCode = colorCode;

    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public int getMenuImage() {
        return menuImage;
    }

    public void setMenuImage(int menuImage) {
        this.menuImage = menuImage;
    }

    public int getColorCode() {
        return colorCode;
    }

    public void setColorCode(int colorCode) {
        this.colorCode = colorCode;
    }
}
