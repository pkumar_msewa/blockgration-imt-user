package com.Blockgration.model;

/**
 * Created by Ksf on 8/14/2016.
 */
public class PrepaidOperatorModel {
    private String operatorCode;
    private String operatorName;

    public PrepaidOperatorModel(){

    }

    public PrepaidOperatorModel(String operatorCode, String operatorName){
        this.operatorCode = operatorCode;
        this.operatorName = operatorName;

    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
}
