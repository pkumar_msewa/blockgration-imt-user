package com.Blockgration.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Ksf on 10/11/2016.
 */
public class FlightListModel implements Parcelable {

    //Fares
    private double flightActualBaseFare;
    private double flightTaxFare;
    private double serviceTax;
    private double serviceCharge;
    private double convenienceCharge;
    private double flightNetFare;
    private double totalFare;
    private String flightProviderCode;

    private String flightJsonString;

    private ArrayList<FlightModel> flightListArray;


    public FlightListModel(double flightActualBaseFare, double flightTaxFare, double serviceTax, double serviceCharge, double convenienceCharge, double flightNetFare, double totalFare, String flightProviderCode, ArrayList<FlightModel> flightListArray, String flightJsonString) {
        this.flightActualBaseFare = flightActualBaseFare;
        this.flightTaxFare = flightTaxFare;
        this.serviceTax = serviceTax;
        this.serviceCharge = serviceCharge;
        this.convenienceCharge = convenienceCharge;
        this.flightNetFare = flightNetFare;
        this.totalFare = totalFare;
        this.flightProviderCode = flightProviderCode;
        this.flightListArray = flightListArray;
        this.flightJsonString  =  flightJsonString;
    }

    public double getFlightActualBaseFare() {
        return flightActualBaseFare;
    }

    public void setFlightActualBaseFare(double flightActualBaseFare) {
        this.flightActualBaseFare = flightActualBaseFare;
    }

    public double getFlightTaxFare() {
        return flightTaxFare;
    }

    public void setFlightTaxFare(double flightTaxFare) {
        this.flightTaxFare = flightTaxFare;
    }

    public double getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(double serviceTax) {
        this.serviceTax = serviceTax;
    }

    public double getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(double serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public double getConvenienceCharge() {
        return convenienceCharge;
    }

    public void setConvenienceCharge(double convenienceCharge) {
        this.convenienceCharge = convenienceCharge;
    }

    public double getFlightNetFare() {
        return flightNetFare;
    }

    public void setFlightNetFare(double flightNetFare) {
        this.flightNetFare = flightNetFare;
    }

    public double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(double totalFare) {
        this.totalFare = totalFare;
    }

    public String getFlightProviderCode() {
        return flightProviderCode;
    }

    public void setFlightProviderCode(String flightProviderCode) {
        this.flightProviderCode = flightProviderCode;
    }

    public String getFlightJsonString() {
        return flightJsonString;
    }

    public void setFlightJsonString(String flightJsonString) {
        this.flightJsonString = flightJsonString;
    }

    public ArrayList<FlightModel> getFlightListArray() {
        return flightListArray;
    }

    public void setFlightListArray(ArrayList<FlightModel> flightListArray) {
        this.flightListArray = flightListArray;
    }




    //    public double getFlightActualBaseFare() {
//        return flightActualBaseFare;
//    }
//
//    public void setFlightActualBaseFare(double flightActualBaseFare) {
//        this.flightActualBaseFare = flightActualBaseFare;
//    }
//
//    public double getFlightTaxFare() {
//        return flightTaxFare;
//    }
//
//    public void setFlightTaxFare(double flightTaxFare) {
//        this.flightTaxFare = flightTaxFare;
//    }
//
//    public double getServiceTax() {
//        return serviceTax;
//    }
//
//    public void setServiceTax(double serviceTax) {
//        this.serviceTax = serviceTax;
//    }
//
//    public double getServiceCharge() {
//        return serviceCharge;
//    }
//
//    public void setServiceCharge(double serviceCharge) {
//        this.serviceCharge = serviceCharge;
//    }
//
//    public double getConvenienceCharge() {
//        return convenienceCharge;
//    }
//
//    public void setConvenienceCharge(double convenienceCharge) {
//        this.convenienceCharge = convenienceCharge;
//    }
//
//    public double getFlightNetFare() {
//        return flightNetFare;
//    }
//
//    public void setFlightNetFare(double flightNetFare) {
//        this.flightNetFare = flightNetFare;
//    }
//
//    public double getTotalFare() {
//        return totalFare;
//    }
//
//    public void setTotalFare(double totalFare) {
//        this.totalFare = totalFare;
//    }
//
//    public String getFlightProviderCode() {
//        return flightProviderCode;
//    }
//
//    public void setFlightProviderCode(String flightProviderCode) {
//        this.flightProviderCode = flightProviderCode;
//    }
//
//    public ArrayList<FlightModel> getFlightListArray() {
//        return flightListArray;
//    }
//
//    public void setFlightListArray(ArrayList<FlightModel> flightListArray) {
//        this.flightListArray = flightListArray;
//    }
//
//
//
//    //    protected FlightListModel(Parcel in) {
////        this.flightActualBaseFare = in.readLong();
////        this.flightTaxFare = in.readLong();
////        this.serviceTax = in.readLong();
////        this.serviceCharge = in.readLong();
////        this.convenienceCharge = in.readLong();
////        this.flightNetFare = in.readLong();
////        this.totalFare = in.readLong();
////        this.flightProviderCode = in.readString();
////        this.flightListArray = new ArrayList<>();
////        in.readList(this.flightListArray, FlightModel.class.getClassLoader());
////    }
////
////    public static final Parcelable.Creator<FlightListModel> CREATOR = new Parcelable.Creator<FlightListModel>() {
////        @Override
////        public FlightListModel createFromParcel(Parcel source) {
////            return new FlightListModel(source);
////        }
////
////        @Override
////        public FlightListModel[] newArray(int size) {
////            return new FlightListModel[size];
////        }
////    };
//
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeDouble(this.flightActualBaseFare);
//        dest.writeDouble(this.flightTaxFare);
//        dest.writeDouble(this.serviceTax);
//        dest.writeDouble(this.serviceCharge);
//        dest.writeDouble(this.convenienceCharge);
//        dest.writeDouble(this.flightNetFare);
//        dest.writeDouble(this.totalFare);
//        dest.writeString(this.flightProviderCode);
//        dest.writeTypedList(flightListArray);
//    }
//
//    protected FlightListModel(Parcel in) {
//        this.flightActualBaseFare = in.readDouble();
//        this.flightTaxFare = in.readDouble();
//        this.serviceTax = in.readDouble();
//        this.serviceCharge = in.readDouble();
//        this.convenienceCharge = in.readDouble();
//        this.flightNetFare = in.readDouble();
//        this.totalFare = in.readDouble();
//        this.flightProviderCode = in.readString();
//        this.flightListArray = in.createTypedArrayList(FlightModel.CREATOR);
//    }
//
//    public static final Parcelable.Creator<FlightListModel> CREATOR = new Parcelable.Creator<FlightListModel>() {
//        @Override
//        public FlightListModel createFromParcel(Parcel source) {
//            return new FlightListModel(source);
//        }
//
//        @Override
//        public FlightListModel[] newArray(int size) {
//            return new FlightListModel[size];
//        }
//    };
//
//
//    @Override
//    public int compare(FlightListModel model1, FlightListModel model2) {
//        return model1.totalFare > model2.totalFare ? 1 : (model1.totalFare < model2.totalFare ? -1 : 0);
//    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.flightActualBaseFare);
        dest.writeDouble(this.flightTaxFare);
        dest.writeDouble(this.serviceTax);
        dest.writeDouble(this.serviceCharge);
        dest.writeDouble(this.convenienceCharge);
        dest.writeDouble(this.flightNetFare);
        dest.writeDouble(this.totalFare);
        dest.writeString(this.flightProviderCode);
        dest.writeString(this.flightJsonString);
        dest.writeTypedList(this.flightListArray);
    }

    protected FlightListModel(Parcel in) {
        this.flightActualBaseFare = in.readDouble();
        this.flightTaxFare = in.readDouble();
        this.serviceTax = in.readDouble();
        this.serviceCharge = in.readDouble();
        this.convenienceCharge = in.readDouble();
        this.flightNetFare = in.readDouble();
        this.totalFare = in.readDouble();
        this.flightProviderCode = in.readString();
        this.flightJsonString = in.readString();
        this.flightListArray = in.createTypedArrayList(FlightModel.CREATOR);
    }

    public static final Creator<FlightListModel> CREATOR = new Creator<FlightListModel>() {
        @Override
        public FlightListModel createFromParcel(Parcel source) {
            return new FlightListModel(source);
        }

        @Override
        public FlightListModel[] newArray(int size) {
            return new FlightListModel[size];
        }
    };
}
