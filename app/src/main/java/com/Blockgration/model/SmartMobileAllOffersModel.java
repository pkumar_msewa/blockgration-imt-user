package com.Blockgration.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Hitesh on 18-Sep-17.
 */

public class SmartMobileAllOffersModel extends SugarRecord implements Parcelable {

    String offer_key;
    String logo_url;
    String title;

    public SmartMobileAllOffersModel(String offer_key, String logo_url, String title) {
        this.offer_key = offer_key;
        this.logo_url = logo_url;
        this.title = title;
    }

    protected SmartMobileAllOffersModel(Parcel in) {
        offer_key = in.readString();
        logo_url = in.readString();
        title = in.readString();
    }

    public static final Creator<SmartMobileAllOffersModel> CREATOR = new Creator<SmartMobileAllOffersModel>() {
        @Override
        public SmartMobileAllOffersModel createFromParcel(Parcel in) {
            return new SmartMobileAllOffersModel(in);
        }

        @Override
        public SmartMobileAllOffersModel[] newArray(int size) {
            return new SmartMobileAllOffersModel[size];
        }
    };

    public String getOffer_key() {
        return offer_key;
    }

    public void setOffer_key(String offer_key) {
        this.offer_key = offer_key;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(offer_key);
        dest.writeString(logo_url);
        dest.writeString(title);
    }
}
