package com.Blockgration.model;

/**
 * Created by Msewa-Admin on 6/6/2017.
 */

public class ShoppingAddressModel {
private long orderid;
    private String firstname;
    private String lastname;
    private String streetaddress;
    private String email;
    private String landmark;
    private String city;
    private String country;
    private String mobilenumber;
    private long zipcode;
    private long userid;

    public ShoppingAddressModel(long orderid, String firstname, String lastname, String streetaddress, String email, String landmark, String city, String country, String mobilenumber, long zipcode, long userid) {
        this.orderid = orderid;
        this.firstname = firstname;
        this.lastname = lastname;
        this.streetaddress = streetaddress;
        this.email = email;
        this.landmark = landmark;
        this.city = city;
        this.country = country;
        this.mobilenumber = mobilenumber;
        this.zipcode = zipcode;
        this.userid = userid;
    }

    public long getOrderid() {
        return orderid;
    }

    public void setOrderid(long orderid) {
        this.orderid = orderid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStreetaddress() {
        return streetaddress;
    }

    public void setStreetaddress(String streetaddress) {
        this.streetaddress = streetaddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public long getZipcode() {
        return zipcode;
    }

    public void setZipcode(long zipcode) {
        this.zipcode = zipcode;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }
}
