package com.Blockgration.model;

import com.orm.SugarRecord;

/**
 * Created by Hitesh on 20-Sep-17.
 */

public class SmCategoriesListModel extends SugarRecord {

    String p_category_key;
    String p_category_name;

    String c_category_key;
    String c_category_name;
    String c_parent_key;

    public SmCategoriesListModel(String p_category_key, String p_category_name, String c_category_key, String c_category_name, String c_parent_key) {
        this.p_category_key = p_category_key;
        this.p_category_name = p_category_name;
        this.c_category_key = c_category_key;
        this.c_category_name = c_category_name;
        this.c_parent_key = c_parent_key;
    }

    public String getP_category_key() {
        return p_category_key;
    }

    public void setP_category_key(String p_category_key) {
        this.p_category_key = p_category_key;
    }

    public String getP_category_name() {
        return p_category_name;
    }

    public void setP_category_name(String p_category_name) {
        this.p_category_name = p_category_name;
    }

    public String getC_category_key() {
        return c_category_key;
    }

    public void setC_category_key(String c_category_key) {
        this.c_category_key = c_category_key;
    }

    public String getC_category_name() {
        return c_category_name;
    }

    public void setC_category_name(String c_category_name) {
        this.c_category_name = c_category_name;
    }

    public String getC_parent_key() {
        return c_parent_key;
    }

    public void setC_parent_key(String c_parent_key) {
        this.c_parent_key = c_parent_key;
    }
}
