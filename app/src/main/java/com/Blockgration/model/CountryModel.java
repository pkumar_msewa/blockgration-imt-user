package com.Blockgration.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 7/14/2016.
 */
public class CountryModel extends SugarRecord {
    private String countryName;
    private String countryCode;
    private String currencyCode;
    private String isoCode;
    private String currencySymbol;
    private String currencyName;

    public CountryModel(){

    }

    public CountryModel(String countryName, String countryCode, String currencyCode, String isoCode, String currencySymbol,String currencyName){
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.isoCode = isoCode;
        this.currencySymbol = currencySymbol;
        this.currencyName = currencyName;
        this.currencyCode = currencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
}
