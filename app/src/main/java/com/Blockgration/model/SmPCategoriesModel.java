package com.Blockgration.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hitesh on 22-Sep-17.
 */

public class SmPCategoriesModel extends SugarRecord implements Parcelable {

    String p_category_key;
    String p_category_name;

    public SmPCategoriesModel(String p_category_key, String p_category_name) {
        this.p_category_key = p_category_key;
        this.p_category_name = p_category_name;
    }

    protected SmPCategoriesModel(Parcel in) {
        p_category_key = in.readString();
        p_category_name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(p_category_key);
        dest.writeString(p_category_name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SmPCategoriesModel> CREATOR = new Creator<SmPCategoriesModel>() {
        @Override
        public SmPCategoriesModel createFromParcel(Parcel in) {
            return new SmPCategoriesModel(in);
        }

        @Override
        public SmPCategoriesModel[] newArray(int size) {
            return new SmPCategoriesModel[size];
        }
    };

    public String getP_category_key() {
        return p_category_key;
    }

    public void setP_category_key(String p_category_key) {
        this.p_category_key = p_category_key;
    }

    public String getP_category_name() {
        return p_category_name;
    }

    public void setP_category_name(String p_category_name) {
        this.p_category_name = p_category_name;
    }
}
