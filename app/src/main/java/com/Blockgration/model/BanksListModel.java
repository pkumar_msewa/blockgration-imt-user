package com.Blockgration.model;

/**
 * Created by Hitesh on 16-08-2017.
 */

public class BanksListModel {

    private String bankCode;
    private String bankName;

    public BanksListModel(String bankCode, String bankName) {
        this.bankCode = bankCode;
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
