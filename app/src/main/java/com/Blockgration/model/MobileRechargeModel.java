package com.Blockgration.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 8/8/2016.
 */
public class MobileRechargeModel extends SugarRecord {

    private String countrycode;
    private String operatorName;
    private String minAmount;
    private String maxAmount;
    private String skuId;
    private String category;


    public MobileRechargeModel(){

    }

    public MobileRechargeModel(String countrycode, String operatorName, String minAmount, String maxAmount, String skuId, String category){
        this.countrycode= countrycode;
        this.operatorName = operatorName;
        this.minAmount= minAmount;
        this.maxAmount = maxAmount;
        this.skuId = skuId;
        this.category = category;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(String minAmount) {
        this.minAmount = minAmount;
    }

    public String getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(String maxAmount) {
        this.maxAmount = maxAmount;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
