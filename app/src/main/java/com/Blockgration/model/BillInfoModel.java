package com.Blockgration.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Hitesh on 01-09-2017.
 */

public class BillInfoModel extends SugarRecord implements Parcelable {

    String accountNo ;
    String address ;
    String balanceDue ;
    String customerName ;
    String state ;
    String payType ;

    public BillInfoModel() {
    }

    public BillInfoModel(String accountNo, String address, String balanceDue, String customerName, String state, String payType) {
        this.accountNo = accountNo;
        this.address = address;
        this.balanceDue = balanceDue;
        this.customerName = customerName;
        this.state = state;
        this.payType = payType;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBalanceDue() {
        return balanceDue;
    }

    public void setBalanceDue(String balanceDue) {
        this.balanceDue = balanceDue;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    protected BillInfoModel(Parcel in) {
        accountNo = in.readString();
        address = in.readString();
        balanceDue = in.readString();
        customerName = in.readString();
        state = in.readString();
        payType = in.readString();
    }

    public static final Creator<BillInfoModel> CREATOR = new Creator<BillInfoModel>() {
        @Override
        public BillInfoModel createFromParcel(Parcel in) {
            return new BillInfoModel(in);
        }

        @Override
        public BillInfoModel[] newArray(int size) {
            return new BillInfoModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(accountNo);
        dest.writeString(address);
        dest.writeString(balanceDue);
        dest.writeString(customerName);
        dest.writeString(state);
        dest.writeString(payType);
    }
}
