package com.Blockgration.model;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by Ksf on 8/9/2016.
 */
public class BillPayOperatorModel extends SugarRecord implements Serializable {
    private String serviceid;
    private String servicename;
    private String servicecountry;
    private String billtype;
    private String servicecurrency;
    private String operatorname;


    public BillPayOperatorModel() {

    }

    public BillPayOperatorModel(String serviceid, String servicename, String servicecountry, String billtype, String servicecurrency,String operatorname){
        this.serviceid = serviceid;
        this.servicename = servicename;
        this.servicecountry = servicecountry;
        this.billtype =billtype;
        this.servicecurrency = servicecurrency;
        this.operatorname = operatorname;
    }

    public String getServiceid() {
        return serviceid;
    }

    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
    }

    public String getServicename() {
        return servicename;
    }

    public void setServicename(String servicename) {
        this.servicename = servicename;
    }

    public String getServicecountry() {
        return servicecountry;
    }

    public void setServicecountry(String servicecountry) {
        this.servicecountry = servicecountry;
    }

    public String getBilltype() {
        return billtype;
    }

    public void setBilltype(String billtype) {
        this.billtype = billtype;
    }

    public String getServicecurrency() {
        return servicecurrency;
    }

    public void setServicecurrency(String servicecurrency) {
        this.servicecurrency = servicecurrency;
    }

    public String getOperatorname() {
        return operatorname;
    }

    public void setOperatorname(String operatorname) {
        this.operatorname = operatorname;
    }
}
