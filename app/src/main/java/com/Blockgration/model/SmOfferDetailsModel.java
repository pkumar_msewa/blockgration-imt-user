package com.Blockgration.model;

/**
 * Created by Hitesh on 20-Sep-17.
 */

public class SmOfferDetailsModel {

    /*String savings_amount;
    String logo_url;
    boolean promotion_code;
    String expires_on;
    String discount_value;
    String terms_of_use;
    String offer_store;
    String name;
    String description;
    String store_key;
    String country;
    String street_address;
    String state_region;
    String location_name;
    String city_locality;
    String phone_number;
    String postal_code;
    String location_key;
    String lon;
    String lat;
    String discount_type;
    String offer_key;
    String minimum_purchase;
    String started_on;
    String maximum_award;
    String uses_per_period;
    String offer_value;


    boolean online_exclusive;
    boolean national_offer;*/


    private String savings_amount;
    private String logo_url;
    private boolean promotion_code;
    private String expires_on;
    private String discount_value;
    private String terms_of_use;
    private String name;
    private String description;
    private String store_key;
    private String minimum_purchase;

    public SmOfferDetailsModel(String savings_amount, String logo_url, boolean promotion_code, String expires_on, String discount_value, String terms_of_use, String name, String description, String store_key, String minimum_purchase) {
        this.savings_amount = savings_amount;
        this.logo_url = logo_url;
        this.promotion_code = promotion_code;
        this.expires_on = expires_on;
        this.discount_value = discount_value;
        this.terms_of_use = terms_of_use;
        this.name = name;
        this.description = description;
        this.store_key = store_key;
        this.minimum_purchase = minimum_purchase;
    }

    public String getSavings_amount() {
        return savings_amount;
    }

    public void setSavings_amount(String savings_amount) {
        this.savings_amount = savings_amount;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public boolean isPromotion_code() {
        return promotion_code;
    }

    public void setPromotion_code(boolean promotion_code) {
        this.promotion_code = promotion_code;
    }

    public String getExpires_on() {
        return expires_on;
    }

    public void setExpires_on(String expires_on) {
        this.expires_on = expires_on;
    }

    public String getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(String discount_value) {
        this.discount_value = discount_value;
    }

    public String getTerms_of_use() {
        return terms_of_use;
    }

    public void setTerms_of_use(String terms_of_use) {
        this.terms_of_use = terms_of_use;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStore_key() {
        return store_key;
    }

    public void setStore_key(String store_key) {
        this.store_key = store_key;
    }

    public String getMinimum_purchase() {
        return minimum_purchase;
    }

    public void setMinimum_purchase(String minimum_purchase) {
        this.minimum_purchase = minimum_purchase;
    }
}