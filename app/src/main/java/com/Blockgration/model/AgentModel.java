package com.Blockgration.model;

import android.content.Context;

import com.orm.SugarRecord;

/**
 * Created by Amrutha-PC on 10-Mar-17.
 */

public class AgentModel extends SugarRecord {
    private String agentId;
    private String agentName;
    private String agentSlug;
    private String agentCreditLimit;
    private String agentLegalName;
    private String agentOwner;
    private String agentShortCode;
    private String agentLogo;
    private boolean agentOpen;
    private Context context;

    public AgentModel(String agentId, String agentName, String agentSlug, String agentCreditLimit, String agentLegalName, String agentOwner, String agentShortCode, String agentLogo, boolean agentOpen) {
        this.agentId = agentId;
        this.agentName = agentName;
        this.agentSlug = agentSlug;
        this.agentCreditLimit = agentCreditLimit;
        this.agentLegalName = agentLegalName;
        this.agentOwner = agentOwner;
        this.agentShortCode = agentShortCode;
        this.agentLogo = agentLogo;
        this.agentOpen = agentOpen;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentSlug() {
        return agentSlug;
    }

    public void setAgentSlug(String agentSlug) {
        this.agentSlug = agentSlug;
    }

    public String getAgentCreditLimit() {
        return agentCreditLimit;
    }

    public void setAgentCreditLimit(String agentCreditLimit) {
        this.agentCreditLimit = agentCreditLimit;
    }

    public String getAgentLegalName() {
        return agentLegalName;
    }

    public void setAgentLegalName(String agentLegalName) {
        this.agentLegalName = agentLegalName;
    }

    public String getAgentOwner() {
        return agentOwner;
    }

    public void setAgentOwner(String agentOwner) {
        this.agentOwner = agentOwner;
    }

    public String getAgentShortCode() {
        return agentShortCode;
    }

    public void setAgentShortCode(String agentShortCode) {
        this.agentShortCode = agentShortCode;
    }

    public String getAgentLogo() {
        return agentLogo;
    }

    public void setAgentLogo(String agentLogo) {
        this.agentLogo = agentLogo;
    }

    public boolean isAgentOpen() {
        return agentOpen;
    }

    public void setAgentOpen(boolean agentOpen) {
        this.agentOpen = agentOpen;
    }
}
