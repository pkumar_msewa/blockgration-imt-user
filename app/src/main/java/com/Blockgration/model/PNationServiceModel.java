package com.Blockgration.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Hitesh on 19-08-2017.
 */

public class PNationServiceModel extends SugarRecord implements Parcelable {

    private String name ;
    private String code ;
    private String minAmount ;
    private String maxAmount ;
    private String status ;
    private String carrierId ;
    private String type ;
    private String fee ;
    private String currency ;
    private String country ;

    public PNationServiceModel() {
    }

    public PNationServiceModel(String name, String code, String minAmount, String maxAmount, String status, String carrierId, String type, String fee, String currency, String country) {
        this.name = name;
        this.code = code;
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
        this.status = status;
        this.carrierId = carrierId;
        this.type = type;
        this.fee = fee;
        this.currency = currency;
        this.country = country;
    }

    protected PNationServiceModel(Parcel in) {
        name = in.readString();
        code = in.readString();
        minAmount = in.readString();
        maxAmount = in.readString();
        status = in.readString();
        carrierId = in.readString();
        type = in.readString();
        fee = in.readString();
        currency = in.readString();
        country = in.readString();
    }

    public static final Creator<PNationServiceModel> CREATOR = new Creator<PNationServiceModel>() {
        @Override
        public PNationServiceModel createFromParcel(Parcel in) {
            return new PNationServiceModel(in);
        }

        @Override
        public PNationServiceModel[] newArray(int size) {
            return new PNationServiceModel[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(String minAmount) {
        this.minAmount = minAmount;
    }

    public String getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(String maxAmount) {
        this.maxAmount = maxAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(code);
        dest.writeString(minAmount);
        dest.writeString(maxAmount);
        dest.writeString(status);
        dest.writeString(carrierId);
        dest.writeString(type);
        dest.writeString(fee);
        dest.writeString(currency);
        dest.writeString(country);
    }
}
