package com.Blockgration.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 3/16/2016.
 */
public class UserModel extends SugarRecord {
    public String userSessionId;
    public String userFirstName;
    public String userLastName;
    public String userMobileNo;
    public String userEmail;
    public String userAddress;
    public String emailIsActive;
    public String userAcNo;
    public String userBalance;
    public String userDailyLimit;
    public String userMonthlyLimit;
    public String userBalanceLimit;
    public String userAcName;
    public String userAcCode;
    public String userImage;
    public String userPoints;
    public int isValid;

    public String userDob;
    public String userGender;
    public boolean isMPin;

    private volatile static UserModel uniqueInstance;


    public UserModel() {

    }

    /*
     * Singleton getInstance()
	 */
    public static UserModel getInstance() {
        if (uniqueInstance == null) {
            synchronized (UserModel.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new UserModel();
                }
            }
        }
        return uniqueInstance;
    }

    public UserModel(String userSessionId, String userFirstName, String userLastName, String userMobileNo, String userEmail, String emailIsActive, int isValid, String userAddress, String userImage, String userDob, String userGender, boolean isMPin) {
        this.userSessionId = userSessionId;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userMobileNo = userMobileNo;
        this.userEmail = userEmail;
        this.emailIsActive = emailIsActive;
        this.isValid = isValid;
        this.isMPin = isMPin;
        this.userDob = userDob;
        this.userGender = userGender;

        this.userAddress = userAddress;
        this.userImage = userImage;
    }

    public String getUserAcNo() {
        return userAcNo;
    }

    public void setUserAcNo(String userAcNo) {
        this.userAcNo = userAcNo;
    }

    public String getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(String userBalance) {
        this.userBalance = userBalance;
    }

    public String getUserDailyLimit() {
        return userDailyLimit;
    }

    public void setUserDailyLimit(String userDailyLimit) {
        this.userDailyLimit = userDailyLimit;
    }

    public String getUserMonthlyLimit() {
        return userMonthlyLimit;
    }

    public void setUserMonthlyLimit(String userMonthlyLimit) {
        this.userMonthlyLimit = userMonthlyLimit;
    }

    public String getUserBalanceLimit() {
        return userBalanceLimit;
    }

    public void setUserBalanceLimit(String userBalanceLimit) {
        this.userBalanceLimit = userBalanceLimit;
    }

    public String getUserAcName() {
        return userAcName;
    }

    public void setUserAcName(String userAcName) {
        this.userAcName = userAcName;
    }

    public String getUserAcCode() {
        return userAcCode;
    }

    public void setUserAcCode(String userAcCode) {
        this.userAcCode = userAcCode;
    }

    public String getUserPoints() {
        return userPoints;
    }

    public void setUserPoints(String userPoints) {
        this.userPoints = userPoints;
    }

    public String getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(String userSessionId) {
        this.userSessionId = userSessionId;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserMobileNo() {
        return userMobileNo;
    }

    public void setUserMobileNo(String userMobileNo) {
        this.userMobileNo = userMobileNo;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getEmailIsActive() {
        return emailIsActive;
    }

    public void setEmailIsActive(String emailIsActive) {
        this.emailIsActive = emailIsActive;
    }


    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public int getIsValid() {
        return isValid;
    }

    public void setIsValid(int isValid) {
        this.isValid = isValid;
    }

    public String getUserDob() {
        return userDob;
    }

    public void setUserDob(String userDob) {
        this.userDob = userDob;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public boolean isMPin() {
        return isMPin;
    }

    public void setMPin(boolean MPin) {
        isMPin = MPin;
    }

}
