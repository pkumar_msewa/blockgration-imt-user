package com.Blockgration.model;

import com.orm.SugarRecord;

/**
 * Created by Hitesh on 21-Sep-17.
 */

public class SuggestionsModel extends SugarRecord {

    String logo_url;
    String store_name;
    String store_key;

    public SuggestionsModel(String logo_url, String store_name, String store_key) {
        this.logo_url = logo_url;
        this.store_name = store_name;
        this.store_key = store_key;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getStore_key() {
        return store_key;
    }

    public void setStore_key(String store_key) {
        this.store_key = store_key;
    }
}
