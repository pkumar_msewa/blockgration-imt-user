package com.Blockgration.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 8/9/2016.
 */
public class MobileRechargeCountryModel extends SugarRecord implements Parcelable {
    private String countryName;
    private String countryCode;

    public MobileRechargeCountryModel(String countryName, String countryCode) {
        this.countryName = countryName;
        this.countryCode = countryCode;
    }

    protected MobileRechargeCountryModel(Parcel in) {
        countryName = in.readString();
        countryCode = in.readString();
    }

    public static final Creator<MobileRechargeCountryModel> CREATOR = new Creator<MobileRechargeCountryModel>() {
        @Override
        public MobileRechargeCountryModel createFromParcel(Parcel in) {
            return new MobileRechargeCountryModel(in);
        }

        @Override
        public MobileRechargeCountryModel[] newArray(int size) {
            return new MobileRechargeCountryModel[size];
        }
    };

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(countryName);
        dest.writeString(countryCode);
    }
}
