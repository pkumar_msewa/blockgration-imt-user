package com.Blockgration.model;

import com.orm.SugarRecord;

/**
 * Created by Ksf on 7/14/2016.
 */
public class AccountModel extends SugarRecord {
    private String accBalance;
    private String accNo;
    private String balanceLimit;
    private String accType;
    private String dailyLimit;
    private String accCountry;
    private String monthlyLimit;
    private String currencyName;
    private String currencyCode;
    private String currencySymbol;

    private volatile static AccountModel uniqueInstance;

    public AccountModel(){

    }

    public AccountModel(String accBalance, String accNo, String balanceLimit, String accType, String dailyLimit,
                        String accCountry, String monthlyLimit, String currencyName, String currencyCode, String currencySymbol){
        this.accBalance = accBalance;
        this.accNo = accNo;
        this.balanceLimit = balanceLimit;
        this.accType = accType;
        this.dailyLimit = dailyLimit;
        this.accCountry = accCountry;
        this.monthlyLimit = monthlyLimit;
        this.currencyName = currencyName;
        this.currencyCode = currencyCode;
        this.currencySymbol = currencySymbol;
    }
    /*
    * Singleton getInstance()
    */

    public static AccountModel getInstance() {
        if (uniqueInstance == null) {
            synchronized (AccountModel.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new AccountModel();
                }
            }
        }
        return uniqueInstance;
    }


    public String getAccBalance() {
        return accBalance;
    }

    public void setAccBalance(String accBalance) {
        this.accBalance = accBalance;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }


    public String getBalanceLimit() {
        return balanceLimit;
    }

    public void setBalanceLimit(String balanceLimit) {
        this.balanceLimit = balanceLimit;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getDailyLimit() {
        return dailyLimit;
    }

    public void setDailyLimit(String dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public String getAccCountry() {
        return accCountry;
    }

    public void setAccCountry(String accCountry) {
        this.accCountry = accCountry;
    }

    public String getMonthlyLimit() {
        return monthlyLimit;
    }

    public void setMonthlyLimit(String monthlyLimit) {
        this.monthlyLimit = monthlyLimit;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
}
