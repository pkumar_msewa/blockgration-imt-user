package com.Blockgration.metadata;

import com.Blockgration.Userwallet.R;
import com.Blockgration.model.MobileRechargeCountryModel;
import com.Blockgration.model.PrepaidOperatorModel;

import java.util.ArrayList;

/**
 * Created by Ksf on 3/27/2016.
 */
public class AppMetadata {
    public static String FRAGMENT_TYPE = "NAVIGATION";
    public static String appKey = "1478673778";
    public static String appSecret = "H/79xKHJBWCubOl9vjkpEBkcpDlVGPNssD6FP6sVY4c=";

    public static String getInvalidSession() {
        return "<b><font color=#000000> Session Expired.</font></b>" +
                "<br><br><b><font color=#ff0000> Please login again to use Blockgration.</font></b><br></br>";
    }

    public static String getSuccessMPinReset() {
        String source = "<b><font color=#000000> MPIN Reset Successfully.</font></b>" +
                "<br><br><b><font color=#ff0000> Please create new MPIN in next step.</font></b><br></br>";
        return source;
    }

    public static ArrayList<MobileRechargeCountryModel> getCountry() {
        ArrayList<MobileRechargeCountryModel> countryItems = new ArrayList<MobileRechargeCountryModel>();
        countryItems.add(new MobileRechargeCountryModel("Antigua and Barbuda", ""));
        countryItems.add(new MobileRechargeCountryModel("Barbados", ""));
        countryItems.add(new MobileRechargeCountryModel("Belize", ""));
        countryItems.add(new MobileRechargeCountryModel("Colombia", ""));
        countryItems.add(new MobileRechargeCountryModel("Costa Rica", ""));
        countryItems.add(new MobileRechargeCountryModel("Cuba", ""));
        countryItems.add(new MobileRechargeCountryModel("Dominica", ""));
        countryItems.add(new MobileRechargeCountryModel("El Salvador", ""));
        countryItems.add(new MobileRechargeCountryModel("France", ""));
        countryItems.add(new MobileRechargeCountryModel("Martinique", ""));

        countryItems.add(new MobileRechargeCountryModel("Grenada", ""));
        countryItems.add(new MobileRechargeCountryModel("Guatemala", ""));
        countryItems.add(new MobileRechargeCountryModel("Guyana", ""));
        countryItems.add(new MobileRechargeCountryModel("Haiti", ""));
        countryItems.add(new MobileRechargeCountryModel("Kingdom of the Netherlands", ""));
        countryItems.add(new MobileRechargeCountryModel("Nicaragua", ""));
        countryItems.add(new MobileRechargeCountryModel("Panama", ""));
        countryItems.add(new MobileRechargeCountryModel("Saint Kitts and Nevis", ""));
        countryItems.add(new MobileRechargeCountryModel("Saint Lucia", ""));
        countryItems.add(new MobileRechargeCountryModel("Saint Vincent and the Grenadines", ""));
        countryItems.add(new MobileRechargeCountryModel("Honduras", ""));
        countryItems.add(new MobileRechargeCountryModel("United Kingdom", ""));
        countryItems.add(new MobileRechargeCountryModel("United States of America", ""));
        countryItems.add(new MobileRechargeCountryModel("United States Virgin Islands", ""));
        countryItems.add(new MobileRechargeCountryModel("Lucayan Archipelago", ""));
        countryItems.add(new MobileRechargeCountryModel("Jamaica", ""));
        countryItems.add(new MobileRechargeCountryModel("Trinidad and Tobago", ""));
        countryItems.add(new MobileRechargeCountryModel("Turks and Caicos Islands", ""));

        return countryItems;
    }
    public static ArrayList<MobileRechargeCountryModel> getCountryNew() {
        ArrayList<MobileRechargeCountryModel> countryItems = new ArrayList<MobileRechargeCountryModel>();
        countryItems.add(new MobileRechargeCountryModel("Mexico","MX"));
        countryItems.add(new MobileRechargeCountryModel("India","IN"));
        countryItems.add(new MobileRechargeCountryModel("Nigeria","NG"));
        countryItems.add(new MobileRechargeCountryModel("Ecuador","EC"));
        countryItems.add(new MobileRechargeCountryModel("United States","US"));
        countryItems.add(new MobileRechargeCountryModel("Trinidad and Tobago","TT"));
        countryItems.add(new MobileRechargeCountryModel("Dominican Republic","DO"));
        countryItems.add(new MobileRechargeCountryModel("Guatemala","GT"));
        countryItems.add(new MobileRechargeCountryModel("El Salvador","SV"));
        countryItems.add(new MobileRechargeCountryModel("Honduras","HN"));
        countryItems.add(new MobileRechargeCountryModel("Brazil","BR"));
        countryItems.add(new MobileRechargeCountryModel("Colombia","CO"));
        countryItems.add(new MobileRechargeCountryModel("Jamaica","JM"));
        countryItems.add(new MobileRechargeCountryModel("Nicaragua","NI"));
        countryItems.add(new MobileRechargeCountryModel("Peru","PE"));
        countryItems.add(new MobileRechargeCountryModel("Puerto Rico","PR"));
        countryItems.add(new MobileRechargeCountryModel("Cuba","CU"));
        countryItems.add(new MobileRechargeCountryModel("Antigua and Barbuda","AG"));
        countryItems.add(new MobileRechargeCountryModel("Aruba","AW"));
        countryItems.add(new MobileRechargeCountryModel("Barbados","BB"));
        countryItems.add(new MobileRechargeCountryModel("Grenada","GD"));
        countryItems.add(new MobileRechargeCountryModel("Guyana","GY"));
        countryItems.add(new MobileRechargeCountryModel("Haiti","HT"));
        countryItems.add(new MobileRechargeCountryModel("Saint Kitts and Nevis","KN"));
        countryItems.add(new MobileRechargeCountryModel("Saint Lucia","LC"));
        countryItems.add(new MobileRechargeCountryModel("Saint Vincent and the Grenadines","VC"));
        countryItems.add(new MobileRechargeCountryModel("Suriname","SR"));
        countryItems.add(new MobileRechargeCountryModel("Turks and Caicos Islands","TC"));
        countryItems.add(new MobileRechargeCountryModel("Vanuatu","VU"));
        countryItems.add(new MobileRechargeCountryModel("Bolivia","BO"));
        countryItems.add(new MobileRechargeCountryModel("Costa Rica","CR"));
        countryItems.add(new MobileRechargeCountryModel("Philippines","PH"));
        countryItems.add(new MobileRechargeCountryModel("Anguilla","AI"));
        countryItems.add(new MobileRechargeCountryModel("Cayman Islands","KY"));
        countryItems.add(new MobileRechargeCountryModel("Dominica","DM"));
        countryItems.add(new MobileRechargeCountryModel("Montserrat","MS"));
        countryItems.add(new MobileRechargeCountryModel("British Virgin Islands","VG"));
        countryItems.add(new MobileRechargeCountryModel("Argentina","AR"));
        countryItems.add(new MobileRechargeCountryModel("Panama","PA"));
        countryItems.add(new MobileRechargeCountryModel("Uruguay","UY"));
        countryItems.add(new MobileRechargeCountryModel("Ghana","GH"));
        countryItems.add(new MobileRechargeCountryModel("Pakistan","PK"));
        countryItems.add(new MobileRechargeCountryModel("Cameroon","CM"));
        countryItems.add(new MobileRechargeCountryModel("Gambia","GM"));
        countryItems.add(new MobileRechargeCountryModel("Togo","TG"));
        return countryItems;
    }
    public static ArrayList<PrepaidOperatorModel> getOperator() {
        ArrayList<PrepaidOperatorModel> operatorItems = new ArrayList<PrepaidOperatorModel>();
        operatorItems.add(new PrepaidOperatorModel("jps", "Jamaica Public Service"));
        operatorItems.add(new PrepaidOperatorModel("lime", "LIME"));
        operatorItems.add(new PrepaidOperatorModel("flow", "Flow Jamaica"));
        operatorItems.add(new PrepaidOperatorModel("digc", "Digicel Jamaica"));
        operatorItems.add(new PrepaidOperatorModel("nwc", "National Water Commiss"));
        operatorItems.add(new PrepaidOperatorModel("kalm", "King Alarm"));
        operatorItems.add(new PrepaidOperatorModel("sagi", "Sagicor"));
        operatorItems.add(new PrepaidOperatorModel("lasc", "Lasco"));

        return operatorItems;
    }


    public static ArrayList<MobileRechargeCountryModel> getCurrencyNew() {
        ArrayList<MobileRechargeCountryModel> countryItems = new ArrayList<MobileRechargeCountryModel>();
        countryItems.add(new MobileRechargeCountryModel("United States","USD,US"));
        countryItems.add(new MobileRechargeCountryModel("Canada","CAD,CA"));
        countryItems.add(new MobileRechargeCountryModel("Colombia","PSO,CO"));
        countryItems.add(new MobileRechargeCountryModel("United Arab Emirates","AED,AED"));
        countryItems.add(new MobileRechargeCountryModel("Panama","PAB,PAB"));
        countryItems.add(new MobileRechargeCountryModel("India","INR,₹"));
        return countryItems;
    }

    //Flight And Images

    public static int getFLightImage(String imageName) {
        switch (imageName){
            case "SG":
                return R.drawable.flight_spice_jet;
            case "9W":
                return R.drawable.flight_jet_airways;
            case "6E":
                return R.drawable.flight_indigo;
            case "AI":
                return R.drawable.flight_air_india;
            case "I5":
                return R.drawable.flight_air_asia;
            case "UK":
                return R.drawable.flight_vistara;
            case "G8":
                return R.drawable.flight_go_air;
            default:
                return R.drawable.ic_no_flight;

        }
    }



}
