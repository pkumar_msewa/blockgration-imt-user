package com.Blockgration.metadata;

public class ApiURLNew {

    //    public static final String URL_DOMAIN = " http://192.170.1.112:8083/FasPayweb/Api/v1/";
//    public static final String URL_DOMAIN = " http://192.170.1.112:8083/FasPayweb/Api/v1/";
//    public static final String URL_DOMAIN = "http://66.207.206.54:8043/FasPayweb/Api/v1/";
//    public static final String URL_DOMAIN = "http://66.207.206.54:8035/Api/";
//    public static final String URL_DOMAIN = "http://173.240.8.56:8034/ClaroWeb/";
    public static final String URL_DOMAIN = "http://159.203.10.34/";
//    public static final String URL_DOMAIN = "http://192.170.1.113:8080/";


    //    public static final String URL_DOMAIN = "http://192.168.2.15:8081/FasPayApp/Api/v1/";
    public static final String CLIENT = "Api/";
    public static final String VERSION = "v1/";
    public static final String USER = "User/";
    public static final String DEVICE = "Android/";
    public static final String LANGUAGE = "en/";

    public static final String URL_MAIN_SEND = URL_DOMAIN + "V1/" + "Mobile/" + USER;

//    http://66.207.206.54:8035/Api/v1/User/Android/en/
//    http://66.207.206.54:8035/Api/v1/User/Android/en/Login/Process
//    http://66.207.206.54:8035/Api/v1/User/Android/en/Registration/Process
//    http://66.207.206.54:8035/Api/v1/User/Android/en/LoadMoney/Process
//    http://66.207.206.54:8035/Api/v1/User/Android/en/User/GetUserDetails
//    http://66.207.206.54:8035/Api/v1/User/Android/en/User/GetReceipts
//    http://66.207.206.54:8035/Api/v1/User/Android/en/SendMoney/Username
//    http://66.207.206.54:8035/Api/v1/User/Android/en/SendMoney/Store
//    http://66.207.206.54:8035/Api/v1/User/Android/en/Login/ForgotPassword
//    http://66.207.206.54:8035/Api/v1/User/Android/en/Login/ChangePasswordWithOTP

//    http://173.240.8.56:8034/ClaroWeb/Api/v1/User/Android/en/
//    http://173.240.8.56:8034/ClaroWeb/Api/v1/User/Android/en/SendMoney/Store

    public static final String URL_MAIN = URL_DOMAIN + CLIENT + VERSION + USER + DEVICE + LANGUAGE;

    public static final String URL_REGISTRATION = URL_MAIN + "Registration/Process";
    public static final String URL_LOGIN = URL_MAIN + "Login/Process";
    public static final String URL_REEDEM_VOUCHER = URL_MAIN + "LoadMoney/Process";
    public static final String URL_USER_DETAILS = URL_MAIN + "User/GetUserDetails";
    public static final String URL_FUND_TRANSFER_EMAIL = URL_MAIN + "SendMoney/Username";
    public static final String URL_RECEIPT = URL_MAIN + "User/GetReceipts";
    public static final String URL_PAY_AT_STORE = URL_MAIN + "SendMoney/Store";

    //  ACH
    public static final String URL_BANK_LIST = URL_MAIN + "LoadMoney/PSE";
    public static final String URL_PSE_PROCESS = URL_MAIN + "LoadMoney/PSEProcess";
    public static final String URL_FINAL_PROCESS = URL_DOMAIN + USER + "/LoadMoney/PSERedirectMobileFinal?code=";

    //  Registration
    public static final String URL_REGISTER_COUNTRIES = URL_MAIN + "Registration/GetCountries";
    public static final String URL_REGISTER_DETAILS = URL_MAIN + "Registration/Country/";

    //  PrePayNation
    public static final String URL_COUNTRIES = URL_MAIN + "User/AllCountry";
    public static final String URL_PNATION = URL_MAIN + "Topup/";
    public static final String URL_PNATION_ALL_SERVICES = URL_PNATION + "AllService";
    public static final String URL_SLIST_TYPE = URL_PNATION + "ServiceType";
    public static final String URL_PURCHASE_PIN = URL_PNATION + "PurchansePin";
    public static final String URL_PURCHASE_TOPUP = URL_PNATION + "PurchaseTopup";
    public static final String URL_ACTIVATE_SIM = URL_PNATION + "ActivateSim";
    public static final String URL_FETCH_BILL = URL_PNATION + "FetchBills";
    public static final String URL_PAY_BILL = URL_PNATION + "PayBill";


    // MPIN
    public static final String URL_GENERATE_M_PIN = URL_MAIN + "User/SetMPIN";
    public static final String URL_CHANGE_M_PIN = URL_MAIN + "User/ChangeMPIN";
    public static final String URL_FORGET_MPIN = URL_MAIN + "User/ForgotMPIN";
    public static final String URL_VERIFY_M_PIN = URL_MAIN + "User/VerifyMPIN";
    public static final String URL_DELETE_M_PIN = URL_MAIN + "";

    //get Reciept info
    public static final String URL_RETRIEVE_DETAILS = URL_DOMAIN + "V1/Mobile/User/getReceipt/";

    public static final String URL_INVITE_FRIENDS = URL_MAIN + "User/Invite/Mobile";

    public static final String URL_UPDATE_FAV = "";
    public static final String URL_QUICK_POST_PAY = "";
    public static final String URL_CHECK_BALANCE_FOR_QWIKPAY = "";

    public static final String URL_QUICK_GET_PAY_LIST = "";
//

    public static final String URL_SEND_FREE_SMS = "https://www.vpayqwik.com/SendSMS";

    public static final String URL_ADD_CURRENCY = URL_MAIN + "User/AddCurrencyReq";
    public static final String URL_EXCHANGE_CURRENCY = URL_MAIN + "User/Exchange_Currency";
    public static final String URL_SWITCH_ACCOUNT = URL_MAIN + "Login/switchAccount";


    public static final String URL_FORGET_PWD = URL_MAIN + "Login/ForgotPassword";
    public static final String URL_LOGOUT = URL_MAIN + "Logout/Process";

    public static final String URL_FUND_TRANSFER_MOBILE = URL_MAIN + "SendMoney/Mobile";
//    public static final String URL_FUND_TRANSFER_EMAIL = URL_MAIN + "SendMoney/Email";

    public static final String URL_IMAGE_MAIN = "";
    public static final String URL_EDIT_USER = URL_MAIN + "User/EditProfile";
    public static final String URL_UPLOAD_IMAGE = URL_MAIN + "User/UploadPicture";
    public static final String URL_CHANGE_CURRENT_PWD = URL_MAIN + "User/ChangePassword";

    public static final String URL_BILL_PAY_ELECTRICITY = URL_MAIN + USER + "Mobile_Electercity";
    public static final String URL_BILL_PAY_DTH = URL_MAIN + USER + "MDth_Topup";
    public static final String URL_BILL_PAY_LAND_LINE = URL_MAIN + USER + "mobile_Landline";
    public static final String URL_BILL_PAY_GAS = URL_MAIN + USER + "mobile_Landline";
    public static final String URL_BILL_PAY_INSURANCE = URL_MAIN + USER + "mobile_Landline";
    public static final String URL_BILL_PAY_OTHER = URL_MAIN + USER + "mobile_otherfeatures";
    public static final String URL_BILL_PAY_LOAN = URL_MAIN + USER + "mobile_Loan";
    public static final String URL_BILL_PAY_INTERNET = URL_MAIN + USER + "mobile_internet";
    public static final String URL_BILL_PAY_CARD = URL_MAIN + USER + "mobile_creaditCardPayment";
    public static final String URL_BILL_PAY_WATER = URL_MAIN + USER + "mobile_water";
    public static final String URL_BILL_PAY_MONTAGE = URL_MAIN + USER + "Mobile_Mortgage";
    public static final String URL_BILL_PAY_TV = URL_MAIN + USER + "mobile_Television";
    public static final String URL_BILL_PAY_CABLE = URL_MAIN + USER + "Mobile_Cabel";


    public static final String URL_PREPAID = URL_MAIN + "User/ProcessPrepaidReq";

    public static final String URL_POSTPAID = URL_MAIN + "User/ProcessPostpaidReq ";


    public static final String URL_CHANGE_PWD = URL_MAIN + "Registration/RenewPassword";
    public static final String URL_CHANGE_PWD_RESEND = URL_MAIN + "RenewPassword";
    //    public static final String URL_PAY_AT_STORE = URL_MAIN + "SendMoney/Store";
//    public static final String URL_PAY_AT_STORE = URL_MAIN + "PayAtStore";

//    public static final String URL_FUND_TRANSFER = URL_MAIN + "SendMoney/Mobile";

    //shopping
//    public static final String URL_GET_ALL_SHOPPING_PRODUCTS = "http://qwikrpay.com/Api/v1/User/Android/en/ShowProduct";
//    public static final String URL_SHOPPONG_REMOVE_ITEM_CART = "http://qwikrpay.com/Api/v1/User/Android/en/removeCartItem";
//    public static final String URL_SHOPPONG_ADD_ITEM_CART = "http://qwikrpay.com/Api/v1/User/Android/en/itemAddToCart";
//    public static final String URL_SHOPPONG_SHOW_CART = "http://qwikrpay.com/Api/v1/User/Android/en/showCartData";
//    public static final String URL_SHOPPONG_ADD_DELIVERY_ADDRESS = "http://qwikrpay.com/Api/v1/User/Android/en/addAddress";
//    public static final String URL_SHOPPONG_DELETE_DELIVERY_ADDRESS = "http://qwikrpay.com/Api/v1/User/Android/en/deleteAddress";
//    public static final String URL_SHOPPONG_CHECKOUT_PROCESS = "http://qwikrpay.com/Api/v1/User/Android/en/checkOutProcess";
//    public static final String URL_EXPENSE_TRACKER = URL_MAIN + "User/UpdateReceipt";
//    public static final String URL_FETCH_NOTIFICATIONS = "http://66.207.206.54:8043/Api/v1/User/Android/en/User/getNotifications";
//    public static final String URL_SHOPPING_CATEGORY_LIST = "http://qwikrpay.com/Api/v1/User/Android/en/country/categoryList";
//    public static final String URL_SHOPPING_SUB_CATEGORY_LIST = "http://qwikrpay.com/Api/v1/User/Android/en/category/SubcategoryList";
//    public static final String URL_SHOPPING_BRAND_LIST = "http://qwikrpay.com/Api/v1/User/Android/en/subCategory/brandList";
//    public static final String URL_SHOPPONG_GO_FOR_DELIVERY = "http://qwikrpay.com/Api/v1/User/Android/en/gofordelivery";
//    public static final String URL_YUP_TV_REGISTER = URL_MAIN + "YuppTV/Register";
//    public static final String URL_GET_ALL_SHOPPING_PAYMENT = "http://qwikrpay.com/Api/v1/User/Android/en/Payment";
//    public static final String URL_SHOPPONG_EDIT_DELIVERY_ADDRESS = "http://qwikrpay.com/Api/v1/User/Android/en/updateAddress";
//    public static final String URL_SHOPPING_MY_ORDER = "http://qwikrpay.com/Api/v1/User/Android/en/myOrder";

    public static final String URL_GET_ALL_SHOPPING_PRODUCTS = "http://www.qwikrpay.com/Api/v1/User/Android/en/ShowProduct";
    public static final String URL_SHOPPONG_REMOVE_ITEM_CART = "http://www.qwikrpay.com/Api/v1/User/Android/en/removeCartItem";
    public static final String URL_SHOPPONG_ADD_ITEM_CART = "http://www.qwikrpay.com/Api/v1/User/Android/en/itemAddToCart";
    public static final String URL_SHOPPONG_SHOW_CART = "http://www.qwikrpay.com/Api/v1/User/Android/en/showCartData";
    public static final String URL_SHOPPONG_ADD_DELIVERY_ADDRESS = "http://www.qwikrpay.com/Api/v1/User/Android/en/addAddress";
    public static final String URL_SHOPPONG_DELETE_DELIVERY_ADDRESS = "http://www.qwikrpay.com/Api/v1/User/Android/en/deleteAddress";
    public static final String URL_SHOPPONG_CHECKOUT_PROCESS = "http://www.qwikrpay.com/Api/v1/User/Android/en/checkOutProcess";
    public static final String URL_EXPENSE_TRACKER = URL_MAIN + "User/UpdateReceipt";
    public static final String URL_FETCH_NOTIFICATIONS = "http://66.207.206.54:8043/Api/v1/User/Android/en/User/getNotifications";
    public static final String URL_SHOPPING_CATEGORY_LIST = "http://www.qwikrpay.com/Api/v1/User/Android/en/country/categoryList";
    public static final String URL_SHOPPING_SUB_CATEGORY_LIST = "http://www.qwikrpay.com/Api/v1/User/Android/en/category/SubcategoryList";
    public static final String URL_SHOPPING_BRAND_LIST = "http://www.qwikrpay.com/Api/v1/User/Android/en/subCategory/brandList";
    public static final String URL_SHOPPONG_GO_FOR_DELIVERY = "http://www.qwikrpay.com/Api/v1/User/Android/en/gofordelivery";
    public static final String URL_YUP_TV_REGISTER = URL_MAIN + "YuppTV/Register";
    public static final String URL_GET_ALL_SHOPPING_PAYMENT = "http://www.qwikrpay.com/Api/v1/User/Android/en/Payment";
    public static final String URL_SHOPPONG_EDIT_DELIVERY_ADDRESS = "http://www.qwikrpay.com/Api/v1/User/Android/en/updateAddress";
    public static final String URL_SHOPPING_MY_ORDER = "http://www.qwikrpay.com/Api/v1/User/Android/en/myOrder";


    //shopping end

//    public static final String URL_SEND_MONEY_TRANSFER_COUNTRY = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "GetRemtingCountry";
//    public static final String URL_SEND_MONEY_PAYMENT_METHOD = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "TransborderPaymentMethods";
//    public static final String URL_SEND_MONEY_EXCHANGE_RATE = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "GetExchangeRate";
//    public static final String URL_SEND_MONEY_GETBENCFY = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "GetRecipientsByCountry";
//    public static final String URL_SEND_MONEY_ADDBENEFICIARY = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "AddUpdateBeneficiary";
//    public static final String URL_SEND_MONEY_COUNTRY = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "GetCountry";
//    public static final String URL_SEND_MONEY_STATE = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "GetState";
//    public static final String URL_SEND_MONEY_CITY = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "GetCity";
//    public static final String URL_SEND_MONEY_AGENT = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "getPayoutAgentByCountry";
//    public static final String URL_SEND_MONEY_AGENT_CITY = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "getCitiesByAgent";
//    public static final String URL_SEND_MONEY_GETCREATETRANSACTION = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "getCreateTransaction";
//    public static final String URL_SEND_MONEY_AGENT_CITY_LOCATION = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "getSubagentsByCity";

//    public static final String URL_RECEIPT = "http://66.207.206.54:8043/FasPayweb/V1/Mobile/" + USER + "getTransactionHistory";

    public static final String URL_LOAD_MONEY = "http://66.207.206.54:8043/FasPayweb/Api/v1/User/Android/en/LoadMoney/sampleLoadMoney";

    //

    public static final String URL_SEND_MONEY_TRANSFER_COUNTRY = URL_MAIN_SEND + "GetRemtingCountry";
    public static final String URL_SEND_MONEY_PAYMENT_METHOD = URL_MAIN_SEND + "TransborderPaymentMethods";
    public static final String URL_SEND_MONEY_EXCHANGE_RATE = URL_MAIN_SEND + "GetExchangeRate";
    public static final String URL_SEND_MONEY_GETBENCFY = URL_MAIN_SEND + "GetRecipientsByCountry";
    public static final String URL_SEND_MONEY_ADDBENEFICIARY = URL_MAIN_SEND + "AddUpdateBeneficiary";
    public static final String URL_SEND_MONEY_COUNTRY = URL_MAIN_SEND + "GetCountry";
    public static final String URL_SEND_MONEY_STATE = URL_MAIN_SEND + "GetState";
    public static final String URL_SEND_MONEY_CITY = URL_MAIN_SEND + "GetCity";
    public static final String URL_SEND_MONEY_AGENT = URL_MAIN_SEND + "getPayoutAgentByCountry";
    public static final String URL_SEND_MONEY_AGENT_CITY = URL_MAIN_SEND + "getCitiesByAgent";
    public static final String URL_SEND_MONEY_GETCREATETRANSACTION = URL_MAIN_SEND + "getCreateTransaction";
    public static final String URL_SEND_MONEY_AGENT_CITY_LOCATION = URL_MAIN_SEND + "getSubagentsByCity";
    public static final String URL_SEND_MONEY_CHECK_DOCUMENT_TYPE = URL_MAIN_SEND + "get_document_type_list";
    public static final String URL_SEND_MONEY_CHECK_THRESHOLD = URL_MAIN_SEND + "check_threshold";
    public static final String URL_SEND_MONEY_UPLOAD_DOCUMENT = URL_MAIN_SEND + "UploadCustomerDocumentRequest";

    //GCI- Gift Card
    public static final String URL_GIFT_CARD_CAT = URL_MAIN + "GiftCard/GetBrands";
    public static final String URL_GIFT_CARD_LIST = URL_MAIN + "GiftCard/GetBrandDenominations";
    public static final String URL_GIFT_CARD_ORDER = URL_MAIN + "GiftCard/AddOrder";

    //Flight
    public static final String URL_GET_FLIGHT_AIRPORT = "http://www.rupease.com/Api/v1/User/Mobile/en/ThirdParty/Travel/Flight/Getflightdeatail";
    public static final String URL_GET_LIST_FLIGHT_ONE_WAY = URL_MAIN + "Treval/Flight/OneWay";
    public static final String URL_GET_FLIGHT_LATEST_PRICE = "http://52.66.169.133/Api/v1/User/Mobile/en/ThirdParty/Travel/Flight/Getonewayallflighttaxdeatail";
    public static final String URL_FLIGHT_BOOK_ONEWAY = URL_MAIN + "Treval/Flight/OneWay";

    public static final String URL_FETCH_ONE_WAY_PRICE = URL_MAIN + "Treval/Flight/ShowPriceDetail";
    public static final String URL_FLIGHT_CHECKOUT = URL_MAIN + "Treval/Flight/CheckOut";
    public static final String URL_FLIGHT_CHECKOUT_ROUND = URL_MAIN + "Treval/Flight/RoundwayCheckOut";


    //Smart Mobile
    public static final String URL_SM_ALL_OFFERS = URL_MAIN + "User/Offers?&page=";
    public static final String URL_SM_OFFER_DETAIL = URL_MAIN + "User/Offer?&key=";
    public static final String URL_SM_OFFER_SUGGESTION = URL_MAIN + "User/Suggestions?&query=";
    public static final String URL_SM_OFFER_BY_STORE = URL_MAIN + "User/OffersByStore?&storeKey=";
    public static final String URL_SM_GET_CATEGORIES = URL_MAIN + "User/Categories?&page=";
    public static final String URL_SM_OFFER_BY_CATEGORIES = URL_MAIN + "User/OffersByCategory?categoryKey=";

}