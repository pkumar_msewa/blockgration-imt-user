package com.Blockgration.metadata;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.model.MainMenuModel;
import com.Blockgration.util.NetworkErrorHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Ksf on 3/16/2016.
 */
public class MenuMetadata {
    private static String tag_json_obj = "tab";
    public static ArrayList<MainMenuModel> getMenu(Context context) {
        ArrayList<MainMenuModel> catItems = new ArrayList<MainMenuModel>();
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu1) + "", R.drawable.menu_new_mobile_topup, R.color.menuB));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu2) + "", R.drawable.menu_new_pay_bills, R.color.menuG));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu3) + "", R.drawable.menu_ft, R.color.menuR));

//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu16) + "", R.drawable.menu_new_currency, R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu19) + "", R.drawable.sendmoney, R.color.menuB));

        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu23) + "", R.drawable.menu_new_travel, R.color.menuR));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu19) + "", R.drawable.menu_sm, R.color.menu7));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu18) + "", R.drawable.menu_new_bank_transfer, R.color.menu5));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu12) + "", R.drawable.menu_new_shopping, R.color.menu2));

//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu9) + "", R.drawable.menu_new_pay_store, R.color.menu9));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu11) + "", R.drawable.menu_new_health_care, R.color.menu1));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu8) + "", R.drawable.menu_new_qr_scan, R.color.menu8));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu12)+"", R.drawable.menu_new_shopping,R.color.menu2));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu13) + "", R.drawable.menu_new_entertainment, R.color.menu3));

        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu15) + "", R.drawable.menu_new_e_gov, R.color.menuG));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu21) + "", R.drawable.menu_new_shopping_new, R.color.menuG));
//        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu17) + "", R.drawable.menu_new_coupns, R.color.menu7));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu22)+"", R.drawable.menu_new_gift,R.color.menu1));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu24) + "", R.drawable.menu_new_chat_new, R.color.menu7));
        catItems.add(new MainMenuModel(context.getResources().getString(R.string.menu25)+"", R.drawable.menu_smartmob,R.color.menu11));

        return catItems;

    }

    public static void getResponse(final Context context, String uri, final JSONObject parms) {
        RequestQueue rq = Volley.newRequestQueue(context);
        final LoadingDialog loadingDialog = new LoadingDialog(context);
        loadingDialog.show();
        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, uri, parms, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loadingDialog.dismiss();
//                try {
                Log.i("Response", response.toString());
                try {
                    if (response.getString("status_code").equals("1000")) {
                        jsonvalue(response, context);
                    } else {
                        value(response.getString("error"), context);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                } catch (JSONException e) {
//                    loadingDialog.dismiss();
//                    CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception2));
//                    e.printStackTrace();
//                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingDialog.dismiss();
                value(NetworkErrorHandler.getMessage(error, context), context);
                error.printStackTrace();
            }
        });
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);
        PayQwikApp.getInstance().addToRequestQueue(postReq,tag_json_obj);
    }

    private static void jsonvalue(JSONObject s, Context context) {
        Intent intent = new Intent("setting");
        intent.putExtra("json", s.toString());
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private static void value(String s, Context context) {
        Intent intent = new Intent("setting");
        intent.putExtra("updates", s);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

}
