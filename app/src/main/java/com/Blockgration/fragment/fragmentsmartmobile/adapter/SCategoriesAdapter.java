package com.Blockgration.fragment.fragmentsmartmobile.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.Blockgration.adapter.GiftCardCatAdapter;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.model.SmPCategoriesModel;
import com.Blockgration.model.SmSCategoriesModel;
import com.orm.query.Select;

import java.util.List;

/**
 * Created by Hitesh on 22-Sep-17.
 */

public class SCategoriesAdapter extends RecyclerView.Adapter<SCategoriesAdapter.RecyclerViewHolders> {

    private List<SmSCategoriesModel> itemList;
    private Context context;

    public SCategoriesAdapter(List<SmSCategoriesModel> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    public SCategoriesAdapter.RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_s_categories, null);
        return new SCategoriesAdapter.RecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(final SCategoriesAdapter.RecyclerViewHolders holder, int position) {
        holder.tvSmCatF2.setText(itemList.get(position).getC_category_name());
    }


    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvSmCatF2;
        public CardView cvSmCatF2;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            tvSmCatF2 = (TextView) itemView.findViewById(R.id.tvSmCatF2);
            cvSmCatF2 = (CardView) itemView.findViewById(R.id.cvSmCatF2);
            cvSmCatF2.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int i = getAdapterPosition();
            if (view.getId() == R.id.cvSmCatF2) {
//                CustomToast.showMessage(context, "SubCategory " + itemList.get(i).getC_category_key() + " clicked");
                Intent intent = new Intent("filter-query");
                intent.putExtra("subCatKey", itemList.get(i).getC_category_key());
                intent.putExtra("filter", false);
                intent.putExtra("subCatMsg", "finish");
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                ((Activity) context).finish();
            }
        }
    }
}
