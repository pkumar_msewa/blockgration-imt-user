package com.Blockgration.fragment.fragmentsmartmobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadMoreListView;
import com.Blockgration.fragment.fragmentsmartmobile.adapter.PCategoriesAdapter;
import com.Blockgration.model.SmCategoriesListModel;
import com.Blockgration.model.SmPCategoriesModel;
import com.Blockgration.model.SmSCategoriesModel;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SmFilter2Activity extends AppCompatActivity {

    private UserModel session = UserModel.getInstance();
    private LoadMoreListView lvReceipt;
    private ProgressBar pbSmartMobileF;

    //Pagination
    private int currentPage = 0;
    private int totalPage = 0;

    //Volley
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;

    //getCategories
    private PCategoriesAdapter pCategoriesAdapter;
    private List<SmCategoriesListModel> smCategoriesArray;
    private List<SmPCategoriesModel> smPCategoriesArray;
    private ArrayList<SmSCategoriesModel> smSCategoriesArray;
    private String url;
    private JSONObject subcategoty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sm_filter2);

        Toolbar toolbar = (Toolbar) findViewById(com.Blockgration.Userwallet.R.id.toolbar);
        ImageView ivBackBtn = (ImageView) findViewById(com.Blockgration.Userwallet.R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        LocalBroadcastManager.getInstance(SmFilter2Activity.this).registerReceiver(fMessageReceiver, new IntentFilter("filter-query"));

        lvReceipt = (LoadMoreListView) findViewById(R.id.lvReceipt);
        pbSmartMobileF = (ProgressBar) findViewById(R.id.pbSmartMobileF);

        subcategoty = new JSONObject();
        smSCategoriesArray = new ArrayList<>();

        url = ApiURLNew.URL_SM_GET_CATEGORIES;
        getSmartMobileCat(url);

        lvReceipt.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (currentPage < totalPage) {
                    getMoreSmartMobileCat(url);
                } else {
                    lvReceipt.onLoadMoreComplete();
                }
            }
        });

//        GetCategories gc = new GetCategories();
//        gc.execute();
    }

    public void getSmartMobileCat(final String url) {
        pbSmartMobileF.setVisibility(View.VISIBLE);
        lvReceipt.setVisibility(View.GONE);
        final JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("SmartMobile Url", url + currentPage);
            Log.i("SmartMobile Request : ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, url + currentPage, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("SmartMobile Response", jsonObj.toString());
                        boolean success = jsonObj.getBoolean("success");
                        String message = jsonObj.getString("message");
                        if (success) {
                            String details = jsonObj.getString("details");
                            JSONObject detailsObj = new JSONObject(details);
                            boolean status = detailsObj.getBoolean("success");
                            if (status) {
                                String p_category_key = null;
                                String p_category_name = null;
                                String c_category_key = null;
                                String c_category_name = null;
                                String c_parent_key = null;
                                String categories = detailsObj.getString("categories");
                                JSONArray categoriesArray = new JSONArray(categories);
                                smPCategoriesArray = new ArrayList<>();
                                for (int i = 0; i < categoriesArray.length(); i++) {
                                    JSONObject obj1 = categoriesArray.getJSONObject(i);
                                    p_category_key = obj1.getString("category_key");
                                    p_category_name = obj1.getString("category_name");
                                    String subcategories = obj1.getString("subcategories");
                                    JSONArray subcategoriesArray = new JSONArray(subcategories);
                                    subcategoty.put(p_category_key, subcategoriesArray);
                                    Log.i("SUBCATSIZE", String.valueOf(subcategoriesArray.length()));
                                    /*if (smSCategoriesArray != null) {
                                        smSCategoriesArray.clear();
                                    }
                                    for (int j = 0; j < subcategoriesArray.length(); j++) {
                                        JSONObject obj2 = subcategoriesArray.getJSONObject(j);
                                        c_category_key = obj2.getString("category_key");
                                        c_category_name = obj2.getString("category_name");
                                        c_parent_key = obj2.getString("category_parent_key");
                                        SmSCategoriesModel slModel = new SmSCategoriesModel(c_category_key, c_category_name, c_parent_key);
                                        smSCategoriesArray.add(slModel);
                                    }*/
//                                    SmPCategoriesModel clModel = new SmPCategoriesModel(p_category_key, p_category_name, smSCategoriesArray);
                                    SmPCategoriesModel clModel = new SmPCategoriesModel(p_category_key, p_category_name);
                                    Log.i("PRINTARRAY", String.valueOf(smSCategoriesArray.size()));
                                    smPCategoriesArray.add(clModel);
                                }
                                if (smPCategoriesArray != null && smPCategoriesArray.size() != 0) {
                                    lvReceipt.setVisibility(View.VISIBLE);
                                    pCategoriesAdapter = new PCategoriesAdapter(smPCategoriesArray, SmFilter2Activity.this, subcategoty);
                                    lvReceipt.setAdapter(pCategoriesAdapter);
                                    pbSmartMobileF.setVisibility(View.GONE);
                                } else {
                                    Toast.makeText(SmFilter2Activity.this, "Empty Array", Toast.LENGTH_SHORT).show();
                                    pbSmartMobileF.setVisibility(View.GONE);
                                }
                            } else if (message.equalsIgnoreCase("Invalid Session")) {
                                showInvalidSessionDialog();
                            }
                        } else {
                            Toast.makeText(SmFilter2Activity.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        pbSmartMobileF.setVisibility(View.GONE);
                        lvReceipt.setVisibility(View.GONE);
                        Toast.makeText(SmFilter2Activity.this, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(SmFilter2Activity.this, "Error connecting to server");
                    pbSmartMobileF.setVisibility(View.GONE);
                    lvReceipt.setVisibility(View.GONE);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public void getMoreSmartMobileCat(final String url) {
        currentPage = currentPage + 1;
        final JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("SmartMobile Url", url + currentPage);
            Log.i("SmartMobile Request : ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, url + currentPage, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("SmartMobile Response", jsonObj.toString());
                        boolean success = jsonObj.getBoolean("success");
                        String message = jsonObj.getString("message");
                        if (success) {
                            String details = jsonObj.getString("details");
                            JSONObject detailsObj = new JSONObject(details);
                            boolean status = detailsObj.getBoolean("success");
                            if (status) {
                                String p_category_key = null;
                                String p_category_name = null;
                                String c_category_key = null;
                                String c_category_name = null;
                                String c_parent_key = null;
                                String categories = detailsObj.getString("categories");
                                JSONArray categoriesArray = new JSONArray(categories);
                                for (int i = 0; i < categoriesArray.length(); i++) {
                                    JSONObject obj1 = categoriesArray.getJSONObject(i);
                                    p_category_key = obj1.getString("category_key");
                                    p_category_name = obj1.getString("category_name");
                                    String subcategories = obj1.getString("subcategories");
                                    JSONArray subcategoriesArray = new JSONArray(subcategories);
                                    Log.i("SUBCATSIZE", String.valueOf(subcategoriesArray.length()));
                                    subcategoty.put(p_category_key, subcategoriesArray);
                                    SmPCategoriesModel clModel = new SmPCategoriesModel(p_category_key, p_category_name);
                                    /*JSONArray subcategoriesArray = new JSONArray(subcategories);
                                    for (int j = 0; j < subcategoriesArray.length(); j++) {
                                        JSONObject obj2 = subcategoriesArray.getJSONObject(j);
                                        c_category_key = obj2.getString("category_key");
                                        c_category_name = obj2.getString("category_name");
                                        c_parent_key = obj2.getString("category_parent_key");
                                        SmSCategoriesModel slModel = new SmSCategoriesModel(c_category_key, c_category_name, c_parent_key);
                                        smSCategoriesArray.add(slModel);
                                    }
                                    SmPCategoriesModel clModel = new SmPCategoriesModel(p_category_key, p_category_name, smSCategoriesArray);*/
                                    smPCategoriesArray.add(clModel);
                                }
                                pCategoriesAdapter.notifyDataSetChanged();
                            } else if (message.equalsIgnoreCase("Invalid Session")) {
                                showInvalidSessionDialog();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        pbSmartMobileF.setVisibility(View.GONE);
                        lvReceipt.onLoadMoreComplete();
                        Toast.makeText(SmFilter2Activity.this, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(SmFilter2Activity.this, "Error connecting to server");
                    pbSmartMobileF.setVisibility(View.GONE);
                    lvReceipt.onLoadMoreComplete();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public class GetCategories extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            getCategories();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void getCategories() {
        final JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("GetCategories Url", ApiURLNew.URL_SM_GET_CATEGORIES);
            Log.i("GetCategories Request", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SM_GET_CATEGORIES, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("GetCategories Response", jsonObj.toString());
                        boolean success = jsonObj.getBoolean("success");
                        String message = jsonObj.getString("message");

                        if (success) {
                            String details = jsonObj.getString("details");
                            JSONObject detailsObj = new JSONObject(details);
                            boolean status = detailsObj.getBoolean("success");
                            if (status) {
                                String p_category_key = null;
                                String p_category_name = null;
                                String c_category_key = null;
                                String c_category_name = null;
                                String c_parent_key = null;
                                String categories = detailsObj.getString("categories");
                                JSONArray categoriesArray = new JSONArray(categories);
                                for (int i = 0; i < categoriesArray.length(); i++) {
                                    JSONObject obj1 = categoriesArray.getJSONObject(i);
                                    p_category_key = obj1.getString("category_key");
                                    p_category_name = obj1.getString("category_name");
                                    String subcategories = obj1.getString("subcategories");
                                    JSONArray subcategoriesArray = new JSONArray(subcategories);
                                    for (int j = 0; j < subcategoriesArray.length(); j++) {
                                        JSONObject obj2 = subcategoriesArray.getJSONObject(j);
                                        c_category_key = obj2.getString("category_key");
                                        c_category_name = obj2.getString("category_name");
                                        c_parent_key = obj2.getString("category_parent_key");
                                    }
//                                    SmCategoriesListModel clModel = new SmCategoriesListModel(p_category_key, p_category_name, c_category_key, c_category_name, c_parent_key);
//                                    smCategoriesArray.add(clModel);
                                }
//                                Log.i("GetCategories B", String.valueOf(smCategoriesArray.size()));
                            } else if (message.equalsIgnoreCase("Invalid Session")) {
                                showInvalidSessionDialog();
                            }
                        } else {
                            Toast.makeText(SmFilter2Activity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(SmFilter2Activity.this, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(SmFilter2Activity.this, "Error connecting to server");
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(SmFilter2Activity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(SmFilter2Activity.this).sendBroadcast(intent);
    }

    private BroadcastReceiver fMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String msg = intent.getStringExtra("subCatMsg");
            if (msg.equals("finish")) {
                finish();
            }
        }
    };

    public void onDestroy() {
        LocalBroadcastManager.getInstance(SmFilter2Activity.this).unregisterReceiver(fMessageReceiver);
        super.onDestroy();
    }

}
