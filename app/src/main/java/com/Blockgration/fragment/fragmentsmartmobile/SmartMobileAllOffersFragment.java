package com.Blockgration.fragment.fragmentsmartmobile;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.adapter.SmartMobAllOffersAdapterNew;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadMoreListView;
import com.Blockgration.model.SmCategoriesListModel;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.SmartMobileAllOffersModel;
import com.Blockgration.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class SmartMobileAllOffersFragment extends Fragment {

    private RecyclerView rvSmartMobile;
    private ProgressBar pbSmartMobile;
    private UserModel session = UserModel.getInstance();

    private SmartMobAllOffersAdapterNew smartMobAllOffersAdapter;
    private LoadMoreListView lvReceipt;
    private ArrayList<SmartMobileAllOffersModel> smartMobileArray;

    //Pagination
    private int currentPage = 0;
    private int totalPage = 0;

    //Volley
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;

    //getCategories
    private List<SmCategoriesListModel> smCategoriesArray;

    private FrameLayout smFiltersfl;
    private Button filter1, filter2;
    private boolean myFilter;
    private String action;
    private String subCatKey;
    private String dynamicUrl;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        smartMobileArray = new ArrayList<>();
        smCategoriesArray = new ArrayList<>();

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(fMessageReceiver, new IntentFilter("filter-query"));

    }

    public SmartMobileAllOffersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_smart_mobile_all_offers, container, false);
        lvReceipt = (LoadMoreListView) rootView.findViewById(R.id.lvReceipt);
//        rvSmartMobile = (RecyclerView) rootView.findViewById(R.id.rvSmartMobile);
        pbSmartMobile = (ProgressBar) rootView.findViewById(R.id.pbSmartMobile);
        smFiltersfl = (FrameLayout) rootView.findViewById(R.id.smFiltersfl);
        smFiltersfl.setVisibility(View.GONE);
        filter1 = (Button) rootView.findViewById(R.id.filter1);
        filter2 = (Button) rootView.findViewById(R.id.filter2);

//        myFilter = false;
        dynamicUrl = ApiURLNew.URL_SM_ALL_OFFERS;

        /*if (!myFilter) {
            dynamicUrl = ApiURLNew.URL_SM_ALL_OFFERS;
        } else {
            dynamicUrl = ApiURLNew.URL_SM_OFFER_BY_STORE + action + "&page=";
        }*/

        getSmartMobileOffers(dynamicUrl);

        lvReceipt.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (currentPage < totalPage) {
                    getMoreSmartMobileOffers(dynamicUrl);
                } else {
                    lvReceipt.onLoadMoreComplete();
                }
            }
        });


        filter1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inn = new Intent(getActivity(), SmFilterActivity.class);
                startActivity(inn);
            }
        });

        filter2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inn = new Intent(getActivity(), SmFilter2Activity.class);
                startActivity(inn);
            }
        });

        return rootView;
    }

    public void getSmartMobileOffers(final String url) {
        pbSmartMobile.setVisibility(View.VISIBLE);
        lvReceipt.setVisibility(View.GONE);
        final JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("SmartMobile Url", url + currentPage);
            Log.i("SmartMobile Request : ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, url + currentPage, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("SmartMobile Response", jsonObj.toString());
                        boolean success = jsonObj.getBoolean("success");
                        String message = jsonObj.getString("message");

                        if (success) {

                            String details = jsonObj.getString("details");
                            JSONObject detailsObj = new JSONObject(details);
                            JSONArray offersArray = null;
                            if (detailsObj.isNull("offers")) {
                                CustomToast.showMessage(getActivity(), "No results found!!!");
                                pbSmartMobile.setVisibility(View.GONE);
                                getActivity().finish();
                                return;
                            } else {
                                offersArray = new JSONArray(detailsObj.getString("offers"));
                            }

                            JSONObject infoObj = null;
                            if(detailsObj.isNull("info")){
                                CustomToast.showMessage(getActivity(), message);
                                getActivity().finish();
                                return;
                            }else{
                                infoObj = new JSONObject(detailsObj.getString("info"));
                            }

//                            String info = detailsObj.getString("info");
//                            JSONObject infoObj = new JSONObject(info);
                            totalPage = infoObj.getInt("total_pages");

                            for (int i = 0; i < offersArray.length(); i++) {
                                JSONObject c = offersArray.getJSONObject(i);
                                long offer_key_long = c.getLong("offer_key");
                                String offer_key = String.valueOf(offer_key_long);
                                String logo_url = c.getString("logo_url");
                                Log.i("GetCategories", logo_url);
                                String title = c.getString("title");
                                SmartMobileAllOffersModel smCatModel = new SmartMobileAllOffersModel(offer_key, logo_url, title);
                                smartMobileArray.add(smCatModel);
                            }
                            Log.i("ARRAYSIZE", String.valueOf(smartMobileArray.size()));
                            if (smartMobileArray != null && smartMobileArray.size() != 0) {
                                Log.i("SmartMobile", smartMobileArray.toString());
                                lvReceipt.setVisibility(View.VISIBLE);
                                smartMobAllOffersAdapter = new SmartMobAllOffersAdapterNew(getActivity(), smartMobileArray);
                                lvReceipt.setAdapter(smartMobAllOffersAdapter);
                                pbSmartMobile.setVisibility(View.GONE);
                                smFiltersfl.setVisibility(View.VISIBLE);
                            } else {
                                Toast.makeText(getActivity(), "Empty Array", Toast.LENGTH_SHORT).show();
                                pbSmartMobile.setVisibility(View.GONE);
                            }
                        } else if (message.equalsIgnoreCase("Invalid Session")) {
                            showInvalidSessionDialog();
                            pbSmartMobile.setVisibility(View.GONE);
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                            pbSmartMobile.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        pbSmartMobile.setVisibility(View.GONE);
                        lvReceipt.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), "Error connecting to server");
                    pbSmartMobile.setVisibility(View.GONE);
                    lvReceipt.setVisibility(View.GONE);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    public void getMoreSmartMobileOffers(final String url) {
        currentPage = currentPage + 1;
        final JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("SmartMobile Url", url + currentPage);
            Log.i("SmartMobile Request : ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, url + currentPage, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("SmartMobile Response", jsonObj.toString());
                        boolean success = jsonObj.getBoolean("success");
                        String message = jsonObj.getString("message");

                        if (success) {

                            String details = jsonObj.getString("details");
                            JSONObject detailsObj = new JSONObject(details);
//                            String offers = detailsObj.getString("offers");
//                            JSONArray offersArray = new JSONArray(offers);

                            JSONArray offersArray = null;
                            if (detailsObj.isNull("offers")) {
                                CustomToast.showMessage(getActivity(), "No results found!!!");
                                pbSmartMobile.setVisibility(View.GONE);
                                getActivity().finish();
                            } else {
                                offersArray = new JSONArray(detailsObj.getString("offers"));
                            }

                            for (int i = 0; i < offersArray.length(); i++) {
                                JSONObject c = offersArray.getJSONObject(i);
                                long offer_key_long = c.getLong("offer_key");
                                String offer_key = String.valueOf(offer_key_long);
                                String logo_url = c.getString("logo_url");
                                Log.i("GetCategories", logo_url);
                                String title = c.getString("title");
                                SmartMobileAllOffersModel smCatModel = new SmartMobileAllOffersModel(offer_key, logo_url, title);
                                smartMobileArray.add(smCatModel);
                            }
                            smartMobAllOffersAdapter.notifyDataSetChanged();
                        } else if (message.equalsIgnoreCase("Invalid Session")) {
                            showInvalidSessionDialog();
                            pbSmartMobile.setVisibility(View.GONE);
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                            pbSmartMobile.setVisibility(View.GONE);
                        }
                        lvReceipt.onLoadMoreComplete();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        pbSmartMobile.setVisibility(View.GONE);
                        lvReceipt.onLoadMoreComplete();
                        Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), "Error connecting to server");
                    pbSmartMobile.setVisibility(View.GONE);
                    lvReceipt.onLoadMoreComplete();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    @Override
    public void onDetach() {
        postReq.cancel();
        super.onDetach();
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private BroadcastReceiver fMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            smFiltersfl.setVisibility(View.GONE);
            smartMobileArray.clear();
            currentPage = 0;
            totalPage = 0;
            action = intent.getStringExtra("query");
            myFilter = intent.getBooleanExtra("filter", false);
            subCatKey = intent.getStringExtra("subCatKey");
//            CustomToast.showMessage(context, subCatKey);
            if (myFilter) {
                dynamicUrl = ApiURLNew.URL_SM_OFFER_BY_STORE + action + "&page=";
            } else {
                dynamicUrl = ApiURLNew.URL_SM_OFFER_BY_CATEGORIES + subCatKey + "&page=";
            }
            getSmartMobileOffers(dynamicUrl);
        }
    };

    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(fMessageReceiver);
        super.onDestroy();
    }

}
