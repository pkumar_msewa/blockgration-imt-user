package com.Blockgration.fragment.fragmentsmartmobile.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.fragment.fragmentsmartmobile.SMSubCategoriesActivity;
import com.Blockgration.model.SmPCategoriesModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Hitesh on 22-Sep-17.
 */

public class PCategoriesAdapter extends BaseAdapter {

    private List<SmPCategoriesModel> itemList;
    private Context context;
    private ViewHolder viewHolder;
    private JSONObject subCategory;

    public PCategoriesAdapter(List<SmPCategoriesModel> itemList, Context context, JSONObject subCategory) {
        this.itemList = itemList;
        this.context = context;
        this.subCategory = subCategory;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_p_categories, null);
            viewHolder = new PCategoriesAdapter.ViewHolder();
            viewHolder.cvSmCatF = (CardView) convertView.findViewById(R.id.cvSmCatF);
            viewHolder.tvSmCatF = (TextView) convertView.findViewById(R.id.tvSmCatF);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (PCategoriesAdapter.ViewHolder) convertView.getTag();
        }

        viewHolder.tvSmCatF.setText(itemList.get(position).getP_category_name());

        viewHolder.cvSmCatF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CustomToast.showMessage(context, "Parent Adapter Item " + itemList.get(position).getP_category_key() + " clicked");
                Intent in = new Intent(context, SMSubCategoriesActivity.class);
                try {
                    String jspnArrys = subCategory.getJSONArray(itemList.get(position).getP_category_key()).toString();
                    in.putExtra("SubCatArray", jspnArrys);
                    context.startActivity(in);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        return convertView;
    }

    static class ViewHolder {
        CardView cvSmCatF;
        TextView tvSmCatF;
    }

}
