package com.Blockgration.fragment.fragmentsmartmobile;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.fragment.fragmentsmartmobile.adapter.SuggestionsAdapter;
import com.Blockgration.model.SuggestionsModel;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.UserModel;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SmFilterActivity extends AppCompatActivity {

    LinearLayout smFilterll, smFilterll2;
    MaterialEditText etSmSearchStore;
    Button btnSmFilterGo;

    private RecyclerView rvGiftCardCat;
    private ProgressBar pbGiftCardCat;
    private UserModel session = UserModel.getInstance();

    private List<SuggestionsModel> suggestionsArray;

    //Volley
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sm_filter);

        Toolbar toolbar = (Toolbar) findViewById(com.Blockgration.Userwallet.R.id.toolbar);
        ImageView ivBackBtn = (ImageView) findViewById(com.Blockgration.Userwallet.R.id.ivBackBtn);
        ivBackBtn.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        smFilterll = (LinearLayout) findViewById(R.id.smFilterll);
        smFilterll2 = (LinearLayout) findViewById(R.id.smFilterll2);
        etSmSearchStore = (MaterialEditText) findViewById(R.id.etSmSearchStore);
        btnSmFilterGo = (Button) findViewById(R.id.btnSmFilterGo);
        rvGiftCardCat = (RecyclerView) findViewById(R.id.rvGiftCardCat);
        pbGiftCardCat = (ProgressBar) findViewById(R.id.pbGiftCardCat);

        suggestionsArray = new ArrayList<>();

        smFilterll.setVisibility(View.VISIBLE);
        smFilterll2.setVisibility(View.GONE);

        btnSmFilterGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query = etSmSearchStore.getText().toString().trim();
                if (query.isEmpty() && query.length() <= 3) {
                    CustomToast.showMessage(SmFilterActivity.this, "enter valid query to search");
                    return;
                }
                smFilterll.setVisibility(View.GONE);
                smFilterll2.setVisibility(View.VISIBLE);
                getSuggestions(query);
            }
        });

    }

    public void getSuggestions(final String word) {
        pbGiftCardCat.setVisibility(View.VISIBLE);
        rvGiftCardCat.setVisibility(View.GONE);
        final JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonRequest != null) {
            Log.i("SmFilter URL", ApiURLNew.URL_SM_OFFER_SUGGESTION + word);
            Log.i("SmFilter Request : ", jsonRequest.toString());
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SM_OFFER_SUGGESTION + word, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObj) {
                    try {
                        Log.i("SmFilter Response", jsonObj.toString());
                        String message = jsonObj.getString("message");
                        boolean success = jsonObj.getBoolean("success");
                        if (success) {
                            String details = jsonObj.getString("details");
                            JSONObject detailsObj = new JSONObject(details);
                            String suggestions = detailsObj.getString("suggestions");
                            JSONObject suggestionsObj = new JSONObject(suggestions);
                            String stores = suggestionsObj.getString("stores");
                            JSONArray storesArr = new JSONArray(stores);
                            for (int i = 0; i < storesArr.length(); i++) {
                                JSONObject obj = storesArr.getJSONObject(i);
                                String logo_url = obj.getString("logo_url");
                                String store_name = obj.getString("store_name");
                                String store_key = obj.getString("store_key");

                                SuggestionsModel suggestionsModel = new SuggestionsModel(logo_url, store_name, store_key);
                                suggestionsArray.add(suggestionsModel);

                            }

                            if (suggestionsArray != null && suggestionsArray.size() != 0) {
                                int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
                                rvGiftCardCat.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
                                GridLayoutManager manager = new GridLayoutManager(SmFilterActivity.this, 2);
                                rvGiftCardCat.setLayoutManager(manager);
                                rvGiftCardCat.setHasFixedSize(true);

                                SuggestionsAdapter eveCatAdp = new SuggestionsAdapter(suggestionsArray, SmFilterActivity.this);
                                rvGiftCardCat.setAdapter(eveCatAdp);
                                pbGiftCardCat.setVisibility(View.GONE);
                                rvGiftCardCat.setVisibility(View.VISIBLE);

                            }
                        } else if (message.equalsIgnoreCase("Invalid Session")) {
                            pbGiftCardCat.setVisibility(View.GONE);
                            showInvalidSessionDialog();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        pbGiftCardCat.setVisibility(View.GONE);
                        Toast.makeText(SmFilterActivity.this, "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(SmFilterActivity.this, "Error connecting to server");
                    pbGiftCardCat.setVisibility(View.GONE);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(SmFilterActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(SmFilterActivity.this).sendBroadcast(intent);
    }

}
