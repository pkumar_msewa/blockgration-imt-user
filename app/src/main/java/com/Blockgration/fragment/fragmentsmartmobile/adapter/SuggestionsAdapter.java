package com.Blockgration.fragment.fragmentsmartmobile.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.model.SuggestionsModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Hitesh on 21-Sep-17.
 */

public class SuggestionsAdapter extends RecyclerView.Adapter<SuggestionsAdapter.RecyclerViewHolders> {

    private List<SuggestionsModel> itemList;
    private Context context;

    public SuggestionsAdapter(List<SuggestionsModel> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_smart_mob_cat, null);
        return new RecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, int position) {
        if (itemList.get(position).getLogo_url() != null && !itemList.get(position).getLogo_url().isEmpty()) {
            Picasso.with(context).load(itemList.get(position).getLogo_url()).placeholder(R.drawable.loading_image).into(holder.ivGiftCardCat);
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView ivGiftCardCat;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            ivGiftCardCat = (ImageView) itemView.findViewById(R.id.ivGiftCardCat);
            ivGiftCardCat.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int i = getAdapterPosition();
            if (view.getId() == R.id.ivGiftCardCat) {
                Intent intent = new Intent("filter-query");
                intent.putExtra("query", itemList.get(i).getStore_key());
                intent.putExtra("filter", true);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                ((Activity) context).finish();
//                CustomToast.showMessage(context, itemList.get(i).getStore_key());
            }
        }
    }

}

