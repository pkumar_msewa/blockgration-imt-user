package com.Blockgration.fragment.fragmentsmartmobile;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.Blockgration.adapter.GiftCardCatAdapter;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.fragment.fragmentgiftcard.GiftCardCatFragment;
import com.Blockgration.fragment.fragmentsmartmobile.adapter.SCategoriesAdapter;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.SmSCategoriesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SMSubCategoriesActivity extends AppCompatActivity {

    private RecyclerView rvGiftCardCat;
    private ProgressBar pbGiftCardCat;
    private String smCatModel;
    private ArrayList<SmSCategoriesModel> smSCategoriesArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smsub_categories);
        rvGiftCardCat = (RecyclerView) findViewById(R.id.rvGiftCardCat);
        pbGiftCardCat = (ProgressBar) findViewById(R.id.pbGiftCardCat);
        smCatModel = getIntent().getStringExtra("SubCatArray");
        smSCategoriesArray = new ArrayList<>();
//        SmSCategoriesModel model = new SmSCategoriesModel(smCatModel.get(0).getC_category_key(),smCatModel.get(0).getC_category_name(),smCatModel.get(0).getC_parent_key());

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
        rvGiftCardCat.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        GridLayoutManager manager = new GridLayoutManager(SMSubCategoriesActivity.this, 2);
        rvGiftCardCat.setLayoutManager(manager);
        rvGiftCardCat.setHasFixedSize(true);

        ///  JSON Array Parse here\
        Log.i("subcategory", smCatModel);
        try {
            JSONArray subCategoryList = new JSONArray(smCatModel);
            for (int j = 0; j < subCategoryList.length(); j++) {
                JSONObject obj2 = subCategoryList.getJSONObject(j);
                String c_category_key = obj2.getString("category_key");
                String c_category_name = obj2.getString("category_name");
                String c_parent_key = obj2.getString("category_parent_key");
                SmSCategoriesModel slModel = new SmSCategoriesModel(c_category_key, c_category_name, c_parent_key);
                smSCategoriesArray.add(slModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SCategoriesAdapter sCatAdp = new SCategoriesAdapter(smSCategoriesArray, SMSubCategoriesActivity.this);
        rvGiftCardCat.setAdapter(sCatAdp);
        pbGiftCardCat.setVisibility(View.GONE);
        rvGiftCardCat.setVisibility(View.VISIBLE);

    }


    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(SMSubCategoriesActivity.this, R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(SMSubCategoriesActivity.this).sendBroadcast(intent);
    }

}
