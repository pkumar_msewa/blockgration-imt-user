package com.Blockgration.fragment.billpayment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.BillPayOperatorModel;
import com.Blockgration.model.CurrentAccountModel;
import com.Blockgration.model.MobileRechargeCountryModel;
import com.Blockgration.model.PNationServiceModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.CheckLog;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.PayingDetailsValidation;
import com.Blockgration.util.RandomNoGenerator;
import com.Blockgration.util.SecurityUtil;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Ksf on 8/9/2016.
 */
public class BillPaymentsFragment extends Fragment {
    //Views
    private View rootView;
    private MaterialEditText etBillPayAmount, etBillPayOperator, etBillPayCountry, etBillPayServiceType, etBillPayServiceNo;
    private ScrollView svBillPayment;
    private Button btnBillPay;
    private ProgressBar pbBillPayment;

    //Variables
    private List<BillPayOperatorModel> serviceTypeArray;
    ArrayList<MobileRechargeCountryModel> countryArray = AppMetadata.getCountryNew();
    private View focusView = null;
    private boolean cancel;
    private String selectedCurrencyCode = null, selectedCountryCode = null, selectedCurrentId = null;

    //Session Instance
    private UserModel session = UserModel.getInstance();

    //Task and loader
    private GetOperator getOperatorTask = null;
    private LoadingDialog loadingDialog;
    private JSONObject jsonRequest;

    //Volley Tag
    private String tag_json_obj = "json_obj_req";

    //KEYS
    private static final int KEY_ELECTRICITY = 14;
    private static final int KEY_LAND_LINE = 21;
    private static final int KEY_DTH = 31;
    private static final int KEY_GAS = 41;
    private static final int KEY_INSURANCE = 51;
    private static final int KEY_OTHER = 61;
    private static final int KEY_LOAN = 71;
    private static final int KEY_INTERNET = 81;
    private static final int KEY_CARD = 91;
    private static final int KEY_WATER = 10;
    private static final int KEY_MONTAGE = 11;
    private static final int KEY_TV = 12;
    private static final int KEY_CABLE = 13;
    private static final int KEY_PIN = 1;
    private static final int KEY_RTR = 2;
    private static final int KEY_SIM = 3;
    private static final int KEY_BILL = 4;

    private int current_type = 0;

    private String name;
    private String code;
    private String minAmount;
    private String maxAmount;
    private String status;
    private String carrierId;
    private String type;
    private String fee;
    private String currency;
    private String country;

    private List<CurrentAccountModel> currentAccountArray;
    private List<PNationServiceModel> serviceArray;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(getActivity());
        currentAccountArray = Select.from(CurrentAccountModel.class).list();
        serviceArray = new ArrayList<>();
//        if(serviceArray.size() == 0){
//            loadingDialog.show();
//            getAllPNationServices();
//        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_bill_payment, container, false);
        svBillPayment = (ScrollView) rootView.findViewById(R.id.svBillPayment);
//        pbBillPayment = (ProgressBar) rootView.findViewById(R.id.pbBillPayment);
        etBillPayServiceType = (MaterialEditText) rootView.findViewById(R.id.etBillPayServiceType);
        etBillPayCountry = (MaterialEditText) rootView.findViewById(R.id.etBillPayCountry);
        etBillPayOperator = (MaterialEditText) rootView.findViewById(R.id.etBillPayOperator);
        etBillPayAmount = (MaterialEditText) rootView.findViewById(R.id.etBillPayAmount);
        etBillPayServiceNo = (MaterialEditText) rootView.findViewById(R.id.etBillPayServiceNo);

        btnBillPay = (Button) rootView.findViewById(R.id.btnBillPay);
        etBillPayServiceType.setFocusable(false);
        etBillPayCountry.setFocusable(false);
        etBillPayOperator.setFocusable(false);

        serviceTypeArray = ((PayQwikApp) getActivity().getApplication()).getData();
        if (serviceTypeArray == null || serviceTypeArray.size() == 0) {
            getOperatorTask = new GetOperator();
            getOperatorTask.execute();
        } else {
            afterTaskComplete();
        }
        return rootView;
    }


    public void showCountryDialog() {
        Log.i("nameService,", etBillPayServiceType.getText().toString());
        final ArrayList<String> countryVal = new ArrayList<>();
        Select specificSpecificQueryGt = Select.from(BillPayOperatorModel.class).where(Condition.prop("servicename").eq(etBillPayServiceType.getText().toString()));
        serviceTypeArray.clear();
        serviceTypeArray = specificSpecificQueryGt.list();

        for (BillPayOperatorModel countryModel : serviceTypeArray) {
            int searchListLength = countryArray.size();
            for (int i = 0; i < searchListLength; i++) {
                if (countryArray.get(i).getCountryCode().contains(countryModel.getServicecountry())) {
                    countryVal.add(countryArray.get(i).getCountryName());
                    HashSet<String> hashSet = new HashSet<>();
                    hashSet.addAll(countryVal);
                    countryVal.clear();
                    countryVal.addAll(hashSet);
                }
            }
//            selectedCountryCode = countryModel.getServicecountry();
        }

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select your Service");

        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        currencyDialog.setItems(countryVal.toArray(new String[countryVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etBillPayCountry.setText(countryVal.get(i));
                        etBillPayOperator.getText().clear();

                        for (MobileRechargeCountryModel countryCode : countryArray) {
                            if (countryCode.getCountryName().contains(etBillPayCountry.getText().toString())) {
                                Log.i("CountryCode", countryCode.getCountryCode());
                                selectedCountryCode = countryCode.getCountryCode();
                            }
                        }
                    }
                });

        currencyDialog.show();

    }


    public void showOperatorDialog() {
        final ArrayList<String> operatorVal = new ArrayList<>();
        Select specificOptQuery = Select.from(BillPayOperatorModel.class).where(Condition.prop("servicename").eq(etBillPayServiceType.getText().toString())).and(Condition.prop("servicecountry").eq(selectedCountryCode));
//        serviceTypeArray.clear();
        serviceTypeArray = specificOptQuery.list();

        for (BillPayOperatorModel operatorModel : serviceTypeArray) {
            operatorVal.add(operatorModel.getOperatorname());
        }


        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select your Service");

        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        currencyDialog.setItems(operatorVal.toArray(new String[operatorVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etBillPayOperator.setText(operatorVal.get(i));
                        selectedCurrencyCode = serviceTypeArray.get(i).getServicecurrency();
                        selectedCurrentId = serviceTypeArray.get(i).getServiceid();

                    }
                });

        currencyDialog.show();

    }

    public void showServiceTypeDialog() {
        final ArrayList<String> serviceVal = new ArrayList<>();
//        serviceVal.add("Electricity");
//        serviceVal.add("Landline");
//        serviceVal.add("Satellite");
//        serviceVal.add("Gas");
//        serviceVal.add("Insurance");
//        serviceVal.add("Utilities");
//        serviceVal.add("Loan");
//        serviceVal.add("Internet");
//        serviceVal.add("Credit Cards");
//        serviceVal.add("Water");
//        serviceVal.add("Mortgage");
//        serviceVal.add("Television");
//        serviceVal.add("Cable");
        serviceVal.add("Bill Pay");
        serviceVal.add("Pin");
        serviceVal.add("Rtr");
        serviceVal.add("Sim");

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select your Service");

        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        currencyDialog.setItems(serviceVal.toArray(new String[serviceVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etBillPayCountry.getText().clear();
                        etBillPayOperator.getText().clear();
                        etBillPayServiceType.setText(serviceVal.get(i));
                        current_type = i + 1;
                        loadingDialog.show();
                        if (serviceVal.get(i).equals("Pin")) {
                            etBillPayServiceNo.setVisibility(View.GONE);
                        }
                        loadServiceType(serviceVal.get(i));

                    }
                });

        currencyDialog.show();
    }

    public void parseOperationData() {
        String mainJson = loadJSONFromAsset();
        try {
            JSONObject mainObj = new JSONObject(mainJson);
            JSONArray billArray = mainObj.getJSONArray("billers");
            serviceTypeArray.clear();
            BillPayOperatorModel.deleteAll(BillPayOperatorModel.class);

            for (int i = 0; i < billArray.length(); i++) {
                JSONObject c = billArray.getJSONObject(i);
                String serviceId = c.getString("id");
                String servicename = c.getString("biller_type");
                String servicecountry = c.getString("country");
                String billtype = c.getString("bill_type");
                String servicecurrency = c.getString("currency");
                String operatorname = c.getString("name");
                BillPayOperatorModel billPayOperatorModel = new BillPayOperatorModel(serviceId, servicename, servicecountry, billtype, servicecurrency, operatorname);
                billPayOperatorModel.save();
                serviceTypeArray.add(billPayOperatorModel);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getActivity().getAssets().open("bill.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private class GetOperator extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            pbBillPayment.setVisibility(View.VISIBLE);
            svBillPayment.setVisibility(View.GONE);

        }

        @Override
        protected Void doInBackground(Void... voids) {
            serviceTypeArray = Select.from(BillPayOperatorModel.class).list();
            if (serviceTypeArray.size() == 0) {
                parseOperationData();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ((PayQwikApp) getActivity().getApplication()).storeData(serviceTypeArray);
            afterTaskComplete();
        }
    }

    private void afterTaskComplete() {
//        pbBillPayment.setVisibility(View.GONE);
        svBillPayment.setVisibility(View.VISIBLE);
        etBillPayServiceType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showServiceTypeDialog();
//                showPNationServicesDialog();
            }
        });

        etBillPayCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etBillPayServiceType.getText().toString().isEmpty()) {
                    showCountryDialog();
                } else {
                    CustomToast.showMessage(getActivity(), "Select your service type first");
                }
            }
        });

        etBillPayOperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etBillPayServiceType.getText().toString().isEmpty()) {
//                    showOperatorDialog();
                    showPNationOperatorDialog();
                } else {
                    CustomToast.showMessage(getActivity(), "Select your service type first");
                }
            }
        });

        btnBillPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                attemptPay();
                puchasePin(1);
                CustomToast.showMessage(getActivity(), "Coming Soon");
            }
        });
    }

    private void attemptPay() {
//        etBillPayCountry.setError(null);
        etBillPayOperator.setError(null);
        etBillPayServiceType.setError(null);
        etBillPayAmount.setError(null);
//        etBillPayServiceNo.setError(null);
        cancel = false;
        checkServiceType(etBillPayServiceType.getText().toString());
//        checkCountry(etBillPayCountry.getText().toString());
        checkProvider(etBillPayOperator.getText().toString());
//        checkServiceNo(etBillPayServiceNo.getText().toString());
        checkAmount(etBillPayAmount.getText().toString());
        if (cancel) {
            focusView.requestFocus();
        } else {
            showPayDialog();
        }
    }

    private void checkCountry(String amount) {
        CheckLog countryCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!countryCheckLog.isValid) {
            etBillPayCountry.setError(getString(countryCheckLog.msg));
            focusView = etBillPayCountry;
            cancel = true;
        }
    }

    private void checkProvider(String amount) {
        CheckLog operatorCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!operatorCheckLog.isValid) {
            etBillPayOperator.setError(getString(operatorCheckLog.msg));
            focusView = etBillPayOperator;
            cancel = true;
        }
    }

    private void checkServiceType(String phNo) {
        CheckLog phoneCheckLog = PayingDetailsValidation.checkGasCustomerAc(phNo);
        if (!phoneCheckLog.isValid) {
            etBillPayServiceType.setError(getString(phoneCheckLog.msg));
            focusView = etBillPayServiceType;
            cancel = true;
        }
    }

    private void checkAmount(String amount) {
        CheckLog amountCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!amountCheckLog.isValid) {
            etBillPayAmount.setError(getString(amountCheckLog.msg));
            focusView = etBillPayAmount;
            cancel = true;
        }
    }

    private void checkServiceNo(String amount) {
        CheckLog amountCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!amountCheckLog.isValid) {
            etBillPayServiceNo.setError(getString(amountCheckLog.msg));
            focusView = etBillPayServiceNo;
            cancel = true;
        }
    }

    public void showPayDialog() {
        CharSequence result = "";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(generateMessage(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(generateMessage());
        }

        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (current_type != 0) {
                    promotePayBill(current_type);
                }
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private Map<String, String> generateParams(int type) {
        Map<String, String> params = new HashMap<>();
        switch (type) {
            case KEY_DTH:
                params.put("country_dth", selectedCountryCode);
                params.put("Dth_currency", selectedCurrencyCode);
                params.put("Dth_current_id", selectedCurrentId);
                params.put("dth_provider", etBillPayOperator.getText().toString());
                params.put("dthNo", etBillPayServiceNo.getText().toString());
                params.put("Dth_Amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                break;
            case KEY_ELECTRICITY:
                params.put("Electerciy_country", selectedCountryCode);
                params.put("Electerciy_currency", selectedCurrencyCode);
                params.put("Electerciy_current_id", selectedCurrentId);
                params.put("Electerciy_provider", etBillPayOperator.getText().toString());
                params.put("Electerciy_number", etBillPayServiceNo.getText().toString());
                params.put("Electerciy_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                break;
            case KEY_LAND_LINE:
                params.put("landline_country", selectedCountryCode);
                params.put("landline_currency", selectedCurrencyCode);
                params.put("landline_current_id", selectedCurrentId);
                params.put("landline_provider", etBillPayOperator.getText().toString());
                params.put("landline_number", etBillPayServiceNo.getText().toString());
                params.put("landline_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                break;
            case KEY_GAS:
                params.put("Gas_country", selectedCountryCode);
                params.put("Gas_currency", selectedCurrencyCode);
                params.put("Gas_current_id", selectedCurrentId);
                params.put("Gas_operator", etBillPayOperator.getText().toString());
                params.put("Gas_number", etBillPayServiceNo.getText().toString());
                params.put("Gas_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                break;
            case KEY_INSURANCE:
                params.put("Insurance_country", selectedCountryCode);
                params.put("Insurance_currency", selectedCurrencyCode);
                params.put("Insurance_current_id", selectedCurrentId);
                params.put("Insurance_operator", etBillPayOperator.getText().toString());
                params.put("Insurance_number", etBillPayServiceNo.getText().toString());
                params.put("Insurance_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                break;
            case KEY_OTHER:
                params.put("Other_country", selectedCountryCode);
                params.put("Other_currency", selectedCurrencyCode);
                params.put("Other_current_id", selectedCurrentId);
                params.put("Other_operator", etBillPayOperator.getText().toString());
                params.put("Other_number", etBillPayServiceNo.getText().toString());
                params.put("Other_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                params.put("transctionrefno", RandomNoGenerator.getRandomNoString());
                break;
            case KEY_LOAN:
                params.put("loan_country", selectedCountryCode);
                params.put("loan_currency", selectedCurrencyCode);
                params.put("loan_current_id", selectedCurrentId);
                params.put("loan_operator", etBillPayOperator.getText().toString());
                params.put("loan_number", etBillPayServiceNo.getText().toString());
                params.put("loan_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                params.put("transctionrefno", RandomNoGenerator.getRandomNoString());
                break;
            case KEY_INTERNET:
                params.put("Internet_country", selectedCountryCode);
                params.put("Internet_currency", selectedCurrencyCode);
                params.put("Internet_current_id", selectedCurrentId);
                params.put("Internet_operator", etBillPayOperator.getText().toString());
                params.put("Internet_number", etBillPayServiceNo.getText().toString());
                params.put("Internet_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                params.put("transctionrefno", RandomNoGenerator.getRandomNoString());
                break;
            case KEY_CARD:
                params.put("CreditCard_country", selectedCountryCode);
                params.put("CreditCard_currency", selectedCurrencyCode);
                params.put("CreditCard_current_id", selectedCurrentId);
                params.put("CreditCard_operator", etBillPayOperator.getText().toString());
                params.put("CreditCard_number", etBillPayServiceNo.getText().toString());
                params.put("CreditCard_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                params.put("transctionrefno", RandomNoGenerator.getRandomNoString());
                break;
            case KEY_WATER:
                params.put("Water_country", selectedCountryCode);
                params.put("Water_currency", selectedCurrencyCode);
                params.put("Water_current_id", selectedCurrentId);
                params.put("Water_operator", etBillPayOperator.getText().toString());
                params.put("Water_number", etBillPayServiceNo.getText().toString());
                params.put("Water_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                params.put("transctionrefno", RandomNoGenerator.getRandomNoString());
                break;
            case KEY_MONTAGE:
                params.put("Mortgage_country", selectedCountryCode);
                params.put("Mortgage_currency", selectedCurrencyCode);
                params.put("Mortgage_current_id", selectedCurrentId);
                params.put("Mortgage_operator", etBillPayOperator.getText().toString());
                params.put("Mortgage_number", etBillPayServiceNo.getText().toString());
                params.put("Mortgage_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                params.put("transctionrefno", RandomNoGenerator.getRandomNoString());
                break;
            case KEY_TV:
                params.put("television_country", selectedCountryCode);
                params.put("television_currency", selectedCurrencyCode);
                params.put("television_current_id", selectedCurrentId);
                params.put("television_operator", etBillPayOperator.getText().toString());
                params.put("television_number", etBillPayServiceNo.getText().toString());
                params.put("television_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                params.put("transctionrefno", RandomNoGenerator.getRandomNoString());
                break;
            case KEY_CABLE:
                params.put("Cabel_country", selectedCountryCode);
                params.put("Cabel_currency", selectedCurrencyCode);
                params.put("Cabel_current_id", selectedCurrentId);
                params.put("Cabel_operator", etBillPayOperator.getText().toString());
                params.put("Cabel_number", etBillPayServiceNo.getText().toString());
                params.put("Cabel_amount", etBillPayAmount.getText().toString());
                params.put("username", session.getUserEmail());
                params.put("sessionId", session.getUserSessionId());
                params.put("transctionrefno", RandomNoGenerator.getRandomNoString());
                break;
            default:
                break;
        }
        return params;
    }

    private String generateURL(int type) {
        String url;
        switch (type) {
            case KEY_PIN:
                url = ApiURLNew.URL_PURCHASE_PIN;
                break;
            case KEY_RTR:
                url = ApiURLNew.URL_PURCHASE_PIN;
                break;
            case KEY_SIM:
                url = ApiURLNew.URL_PURCHASE_PIN;
                break;
            case KEY_BILL:
                url = ApiURLNew.URL_PURCHASE_PIN;
                break;
//            case KEY_DTH:
//                url = ApiURLNew.URL_BILL_PAY_DTH;
//                break;
//            case KEY_ELECTRICITY:
//                url = ApiURLNew.URL_BILL_PAY_ELECTRICITY;
//                break;
//            case KEY_LAND_LINE:
//                url = ApiURLNew.URL_BILL_PAY_LAND_LINE;
//                break;
//            case KEY_GAS:
//                url = ApiURLNew.URL_BILL_PAY_GAS;
//                break;
//            case KEY_INSURANCE:
//                url = ApiURLNew.URL_BILL_PAY_INSURANCE;
//                break;
//            case KEY_OTHER:
//                url = ApiURLNew.URL_BILL_PAY_OTHER;
//                break;
//            case KEY_LOAN:
//                url = ApiURLNew.URL_BILL_PAY_LOAN;
//                break;
//            case KEY_INTERNET:
//                url = ApiURLNew.URL_BILL_PAY_INTERNET;
//                break;
//            case KEY_CARD:
//                url = ApiURLNew.URL_BILL_PAY_CARD;
//                break;
//            case KEY_WATER:
//                url = ApiURLNew.URL_BILL_PAY_WATER;
//                break;
//            case KEY_MONTAGE:
//                url = ApiURLNew.URL_BILL_PAY_MONTAGE;
//                break;
//            case KEY_TV:
//                url = ApiURLNew.URL_BILL_PAY_TV;
//                break;
//            case KEY_CABLE:
//                url = ApiURLNew.URL_BILL_PAY_CABLE;
//                break;
            default:
                url = "N/A";
                break;
        }
        return url;
    }


    public void promotePayBill(final int type) {
        loadingDialog.show();
        Log.i("Bill Pay URL", generateURL(type));
        StringRequest postReq = new StringRequest(Request.Method.POST, generateURL(type), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loadingDialog.dismiss();
                Log.i("Bill PayResponse", response.toString());
                try {
                    JSONObject resObj = new JSONObject(response);
                    String code = resObj.getString("code");
                    Log.i("CODE", code);
                    if (code != null && code.equals("F03")) {
                        loadingDialog.dismiss();
                        showInvalidSessionDialog();
                    } else if (code != null && code.equals("S00")) {
                        CustomToast.showMessage(getActivity(), "Success");
                    } else if (code != null && code.equals("F00")) {
                        if (resObj.has("details")) {
                            String details = resObj.getString("details");
                            CustomToast.showMessage(getActivity(), details);
                        }
                    }
                    loadingDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();

                    CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception2));
                    loadingDialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingDialog.dismiss();
                CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Log.i("Bill Pay PARAMS", generateParams(type).toString());
                return generateParams(type);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                map.put("hash", "123");
                return map;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postReq.setRetryPolicy(policy);
        PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }

    public void getAllPNationServices() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("countryCode", currentAccountArray.get(0).getCurrencySymbol());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("PNation Request", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_PNATION_ALL_SERVICES, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject res) {
                    loadingDialog.dismiss();
                    try {
                        Log.i("PNation URL", ApiURLNew.URL_PNATION_ALL_SERVICES);
                        Log.i("PNation Response", res.toString());
                        String codee = res.getString("code");
                        String message = res.getString("message");
                        if (codee != null && codee.equals("S00")) {
                            PNationServiceModel.deleteAll(PNationServiceModel.class);
                            String response = res.getString("response");
                            JSONObject respo = new JSONObject(response);
//                            JSONObject respo = res.getJSONObject("response");
                            JSONArray details = respo.getJSONArray("details");
                            for (int i = 0; i < details.length(); i++) {
                                JSONObject obj = details.getJSONObject(i);
                                name = obj.getString("name");
                                code = obj.getString("code");
                                minAmount = obj.getString("minAmount");
                                maxAmount = obj.getString("maxAmount");
                                status = obj.getString("status");
                                carrierId = obj.getString("carrierId");
                                type = obj.getString("type");
                                fee = obj.getString("fee");
                                currency = obj.getString("currency");
                                country = obj.getString("country");
                                PNationServiceModel pNationServiceModel = new PNationServiceModel(name, code, minAmount, maxAmount, status, carrierId, type, fee, currency, country);
                                pNationServiceModel.save();
                                serviceArray.add(pNationServiceModel);
                            }
                        } else if (codee != null && codee.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123456");
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq);
        }
    }

    public void puchasePin(final int sType) {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("countryCode", currentAccountArray.get(0).getCurrencySymbol());
            jsonRequest.put("skuid", code);
            jsonRequest.put("amount", etBillPayAmount.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("PurchasePin Request", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, generateURL(sType), jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject res) {
                    loadingDialog.dismiss();
                    try {
                        Log.i("PurchasePin URL", generateURL(sType));
                        Log.i("PurchasePin Response", res.toString());
                        String codee = res.getString("code");
                        String message = res.getString("message");
                        if (codee != null && codee.equals("S00")) {
                            JSONObject respo = res.getJSONObject("response");
                            JSONObject details = respo.getJSONObject("details");
                            boolean success = details.getBoolean("success");
                            if (success) {
                                String invoiceNumber = details.getString("invoiceNumber");
                                String carrierName = details.getString("carrierName");
                                String pinNumber = details.getString("pinNumber");
                                String controlNo = details.getString("controlNo");
                                String amount = details.getString("amount");
                                String transactionId = details.getString("transactionId");
                            }
                        } else if (codee != null && codee.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123456");
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq);
        }
    }

    public void loadServiceType(final String sType) {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("countryCode", currentAccountArray.get(0).getCurrencySymbol());
            jsonRequest.put("type", sType);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("PNsType Request", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SLIST_TYPE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject res) {
                    loadingDialog.dismiss();
                    serviceArray.clear();
                    try {
                        Log.i("PNsType URL", ApiURLNew.URL_SLIST_TYPE);
                        Log.i("PNsType Response", res.toString());
                        String codee = res.getString("code");
                        String message = res.getString("message");
                        if (codee != null && codee.equals("S00")) {
                            PNationServiceModel.deleteAll(PNationServiceModel.class);
                            String response = res.getString("response");
                            JSONObject respo = new JSONObject(response);
                            JSONArray details = respo.getJSONArray("details");
                            for (int i = 0; i < details.length(); i++) {
                                JSONObject obj = details.getJSONObject(i);
                                name = obj.getString("name");
                                code = obj.getString("code");
                                minAmount = obj.getString("minAmount");
                                maxAmount = obj.getString("maxAmount");
                                status = obj.getString("status");
                                carrierId = obj.getString("carrierId");
                                type = obj.getString("type");
                                fee = obj.getString("fee");
                                currency = obj.getString("currency");
                                country = obj.getString("country");
                                PNationServiceModel pNationServiceModel = new PNationServiceModel(name, code, minAmount, maxAmount, status, carrierId, type, fee, currency, country);
                                pNationServiceModel.save();
                                serviceArray.add(pNationServiceModel);
                            }
                        } else if (codee != null && codee.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123456");
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq);
        }
    }

    private void showPNationOperatorDialog() {
        final ArrayList<String> serviceVal = new ArrayList<>();

//        if (serviceVal.size() != 0) {
//            serviceVal.clear();
//        }

        for (PNationServiceModel serviceModel : serviceArray) {
            serviceVal.add(serviceModel.getName());
        }

        Log.i("serviceVal", serviceVal.toString());
        Log.i("serviceArray", serviceArray.toString());

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select operator");


        currencyDialog.setItems(serviceVal.toArray(new String[serviceVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etBillPayOperator.setText(serviceVal.get(i));
                        Log.i("Country selected", etBillPayOperator.getText().toString());
                        for (PNationServiceModel infoModel : serviceArray) {
                            if (infoModel.getName().equals(serviceVal.get(i))) {
                                name = serviceArray.get(i).getName();
                                code = serviceArray.get(i).getCode();
                                minAmount = serviceArray.get(i).getMinAmount();
                                maxAmount = serviceArray.get(i).getMaxAmount();
                                status = serviceArray.get(i).getStatus();
                                carrierId = serviceArray.get(i).getCarrierId();
                                type = serviceArray.get(i).getType();
                                fee = serviceArray.get(i).getFee();
                                currency = serviceArray.get(i).getCurrency();
                                country = serviceArray.get(i).getCountry();
                            }
                        }
                    }
                });

        currencyDialog.show();
    }


    public void showLoginDialog(final String msg) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = getActivity().getIntent();
                getActivity().finish();
                startActivity(intent);
            }
        });

        builder.show();
    }

    public String generateMessage() {
        String source =
                "<b><font color=#000000> Service Type: </font></b>" + "<font color=#000000>" + etBillPayServiceType.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Service Provide: </font></b>" + "<font color=#000000>" + etBillPayOperator.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Amount: </font></b>" + "<font>" + etBillPayAmount.getText().toString() + "</font><br><br>" +
                        "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
        return source;
    }

    public String generatePinPSuccess(String message, String carrierName, String pinNo) {
        String source =
                "<b><font color=#ff0000>" + message + "</font></b><br>" +
                        "<b><font color=#000000> Service Provide: </font></b>" + "<font color=#000000>" + carrierName + "</font><br>" +
                        "<b><font color=#000000> Your PIN: </font></b>" + "<font>" + pinNo + "</font><br><br>";
        return source;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getOperatorTask != null) {
            if (!getOperatorTask.isCancelled()) {
                getOperatorTask.cancel(true);
                getOperatorTask = null;
            }
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


}
