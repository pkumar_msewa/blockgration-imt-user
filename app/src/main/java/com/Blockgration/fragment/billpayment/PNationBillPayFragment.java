package com.Blockgration.fragment.billpayment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.loadmoney.LoadMoneyActivity;
import com.Blockgration.Userwallet.activity.transport.DocumentUpload;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.custom.SearchableSpinner;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.BillInfoModel;
import com.Blockgration.model.BillPayOperatorModel;
import com.Blockgration.model.CurrentAccountModel;
import com.Blockgration.model.MobileRechargeCountryModel;
import com.Blockgration.model.PNationServiceModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.CheckLog;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.PayingDetailsValidation;
import com.Blockgration.util.RandomNoGenerator;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.Blockgration.Userwallet.R.id.etACHbanklist;

/**
 * Created by Ksf on 8/9/2016.
 */
public class PNationBillPayFragment extends Fragment {
    //Views
    private View rootView;
    private MaterialEditText etBillPayAmount, etBillPayOperator, etBillPayCountry, etBillPayServiceType, etBillPayServiceNo;
    private ScrollView svBillPayment;
    private Button btnBillPay;
    private ProgressBar pbBillPayment;

    //Variables
    private List<BillPayOperatorModel> serviceTypeArray;
    //    private ArrayList<MobileRechargeCountryModel> countryArray;
    private View focusView = null;
    private boolean cancel;
    private String selectedCurrencyCode = null, selectedCountryCode = null, selectedCurrentId = null;

    private SearchableSpinner spCountry, spOperator;

    //searchable spinner country
    private ArrayList<MobileRechargeCountryModel> countryArray;
    private ArrayAdapter countryAdapter;
    private ArrayList<String> countries = new ArrayList<>();

    //searchable spinner operator
    private List<PNationServiceModel> serviceArray;
    private ArrayAdapter operatorAdapter;
    private ArrayList<String> operators = new ArrayList<>();

    //Session Instance
    private UserModel session = UserModel.getInstance();

    //Task and loader
//    private GetOperator getOperatorTask = null;
    private LoadingDialog loadingDialog;
    private JSONObject jsonRequest;

    private AlertDialog alertDialog;
    private MaterialEditText etChooseBill;
    private TextView tvAccoNo, tvAdr, tvBalDue, tvCustN, tvState, tvPType;
    private String sAccoNo, sAdr, sBalDue, sCustN, sState, sPType;
    private LinearLayout layoutBill;
    private TextView etBillSenderName, etBillSenderMob;

    //Volley Tag
    private String tag_json_obj = "json_obj_req";

    //KEYS
    private static final int KEY_PIN = 1;
    private static final int KEY_RTR = 2;
    private static final int KEY_SIM = 3;
    private static final int KEY_FETCH_BILL = 4;
    private static final int KEY_PAY_BILL = 5;

    private int current_type = 0;

    private String name;
    private String code;
    private String minAmount;
    private String maxAmount;
    private String status;
    private String carrierId;
    private String type;
    private String fee;
    private String currency;
    private String country;

    private List<CurrentAccountModel> currentAccountArray;

    private List<BillInfoModel> billArray;

    private MaterialEditText etMobNo, etSimNumber, etZipCode;
    private String service_type;
    private String billMob;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(getActivity());
        currentAccountArray = Select.from(CurrentAccountModel.class).list();
        serviceArray = new ArrayList<>();
        countryArray = new ArrayList<>();
        billArray = new ArrayList<>();
        loadCountryCode();
//        if(serviceArray.size() == 0){
//            loadingDialog.show();
//            getAllPNationServices();
//        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_bill_payment, container, false);
        svBillPayment = (ScrollView) rootView.findViewById(R.id.svBillPayment);
//        pbBillPayment = (ProgressBar) rootView.findViewById(R.id.pbBillPayment);
        etBillPayServiceType = (MaterialEditText) rootView.findViewById(R.id.etBillPayServiceType);
        etBillPayCountry = (MaterialEditText) rootView.findViewById(R.id.etBillPayCountry);
        etBillPayOperator = (MaterialEditText) rootView.findViewById(R.id.etBillPayOperator);
        etBillPayAmount = (MaterialEditText) rootView.findViewById(R.id.etBillPayAmount);
        etBillPayServiceNo = (MaterialEditText) rootView.findViewById(R.id.etBillPayServiceNo);
        etMobNo = (MaterialEditText) rootView.findViewById(R.id.etMobNo);
        etSimNumber = (MaterialEditText) rootView.findViewById(R.id.etSimNumber);
        etZipCode = (MaterialEditText) rootView.findViewById(R.id.etZipCode);

        btnBillPay = (Button) rootView.findViewById(R.id.btnBillPay);
        etBillPayCountry.setFocusable(false);
        etBillPayServiceType.setFocusable(false);
        etBillPayOperator.setFocusable(false);

        etBillPayAmount.setVisibility(View.GONE);
        etBillPayServiceNo.setVisibility(View.GONE);
        etMobNo.setVisibility(View.GONE);
        etSimNumber.setVisibility(View.GONE);
        etZipCode.setVisibility(View.GONE);

        //new hitesh
        spOperator = (SearchableSpinner) rootView.findViewById(R.id.spOperator);
        spCountry = (SearchableSpinner) rootView.findViewById(R.id.spCountry);

        spCountry.setTitle("Select Country");
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                etBillPayServiceType.getText().clear();
                selectedCountryCode = "";
                selectedCountryCode = countryArray.get(position).getCountryCode();
                Log.i("selectedCountryCode", selectedCountryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spOperator.setTitle("Select Operator");
        spOperator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!etBillPayServiceType.getText().toString().isEmpty()) {
                    name = "";
                    code = "";
                    minAmount = "";
                    maxAmount = "";
                    status = "";
                    carrierId = "";
                    type = "";
                    fee = "";
                    currency = "";
                    country = "";
                    name = serviceArray.get(position).getName();
                    code = serviceArray.get(position).getCode();
                    minAmount = serviceArray.get(position).getMinAmount();
                    maxAmount = serviceArray.get(position).getMaxAmount();
                    status = serviceArray.get(position).getStatus();
                    carrierId = serviceArray.get(position).getCarrierId();
                    type = serviceArray.get(position).getType();
                    fee = serviceArray.get(position).getFee();
                    currency = serviceArray.get(position).getCurrency();
                    country = serviceArray.get(position).getCountry();
                    Log.i("#serviceArray name", name);
                    Log.i("#serviceArray code", code);
                    Log.i("#serviceArray status", status);
                    Log.i("#serviceArray carrierId", carrierId);
                    Log.i("#serviceArray type", type);
                    Log.i("#serviceArray fee", fee);
                    CustomToast.showMessage(getActivity(), "C " + code + " : Min." + minAmount + " : Max. " + maxAmount);
                } else {
                    CustomToast.showMessage(getActivity(), "Select your service type first");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //

        etBillPayCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spCountry.getSelectedItem().toString() != null) {
                    showCountryDialog();
                } else {
                    CustomToast.showMessage(getActivity(), "Select Country first");
                }
            }
        });

        etBillPayServiceType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (!etBillPayCountry.getText().toString().isEmpty()) {
//                    showServiceTypeDialog();
//                } else {
//                    CustomToast.showMessage(getActivity(), "Select Country first");
//                }
                if (spCountry.getSelectedItem() != null) {
                    showServiceTypeDialog();
                } else {
                    CustomToast.showMessage(getActivity(), "Select Country first");
                }

            }
        });

        etBillPayOperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etBillPayServiceType.getText().toString().isEmpty()) {
                    showPNationOperatorDialog();
                } else {
                    CustomToast.showMessage(getActivity(), "Select your service type first");
                }
            }
        });

        btnBillPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validatePrimary();
            }
        });

        return rootView;
    }

    private void validatePrimary() {
//        if (!validateCountry()) {
//            return;
//        }
        if (!validateSpCountry()) {
            return;
        }
        if (!validateService()) {
            return;
        }
        if (!validateSpOperator()) {
            return;
        }
//        if (!validateOperator()) {
//            return;
//        }
        validatePay(service_type);
    }

    private boolean validateSpCountry() {
        if (spCountry.getSelectedItem() == null) {
            CustomToast.showMessage(getActivity(), "Select Country");
            requestFocus(spCountry);
            return false;
        }
        return true;
    }

    private boolean validateSpOperator() {
        if (spOperator.getSelectedItem() == null) {
            CustomToast.showMessage(getActivity(), "Select Operator");
            requestFocus(spOperator);
            return false;
        }
        return true;
    }

    private void loadCountryCode() {
        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }
        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_COUNTRIES, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("JsonRequest", response.toString());
                    loadingDialog.dismiss();
                    try {
                        String message = response.getString("message");
                        String code = response.getString("code");
                        if (code != null & code.equals("S00")) {
                            try {
                                String resp = response.getString("response");
                                JSONObject obj = new JSONObject(resp);
                                String codee = obj.getString("code");
                                if (!codee.equals("null") && codee.equals("S00")) {
                                    JSONArray details = obj.getJSONArray("details");
                                    for (int i = 0; i < details.length(); i++) {
                                        JSONObject jo_inside = details.getJSONObject(i);
                                        String countryName = jo_inside.getString("countryName");
                                        String countryCode = jo_inside.getString("countryCode");
                                        MobileRechargeCountryModel model = new MobileRechargeCountryModel(countryName, countryCode);
                                        countryArray.add(model);
                                    }
                                    if (countryArray.size() != 0) {
                                        for (int p = 0; p < countryArray.size(); p++) {
                                            /*Collections.sort(countryArray, new Comparator<MobileRechargeCountryModel>() {
                                                @Override
                                                public int compare(MobileRechargeCountryModel o1, MobileRechargeCountryModel o2) {
                                                    return o1.getCountryName().compareToIgnoreCase(o2.getCountryName());
                                                }
                                            });*/
                                            countries.add(countryArray.get(p).getCountryName());
                                        }
                                        countryAdapter = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, countries);
                                        spCountry.setAdapter(countryAdapter);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        if (code != null & code.equals("F03")) {
                            showInvalidSessionDialog();
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    Toast.makeText(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123");
                    map.put("Content-Type", "application/json");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showCountryDialog() {
        final ArrayList<String> countryVal = new ArrayList<>();
//        serviceTypeArray.clear();

//        for (MobileRechargeCountryModel countryModel : countryArray) {
//            int searchListLength = countryArray.size();
//            for (int i = 0; i < searchListLength; i++) {
//                if (countryArray.get(i).getCountryCode().contains(countryModel.getCountryName())) {
//                    countryVal.add(countryArray.get(i).getCountryName());
//                    Map<String,String> hashSet = new HashMap<>();
//                    hashSet.put("Countries",countryVal.toString());
//                    if(hashSet.containsValue(countryVal.get(i))){
//                        continue;
//                    }
////                    Set<String> hashSet = new HashSet<>();
////                    hashSet.addAll(countryVal);
////                    countryVal.clear();
////                    countryVal.addAll(hashSet);
////                    Log.i("HASHSET",hashSet.toString());
//                }
//            }
//        }

        for (MobileRechargeCountryModel currencyModel : countryArray) {

            Collections.sort(countryArray, new Comparator<MobileRechargeCountryModel>() {
                @Override
                public int compare(MobileRechargeCountryModel o1, MobileRechargeCountryModel o2) {
                    return o1.getCountryName().compareToIgnoreCase(o2.getCountryName());
                }
            });

            countryVal.add(currencyModel.getCountryName());
        }

        android.support.v7.app.AlertDialog.Builder countryDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        countryDialog.setTitle("Select Country");

        countryDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        countryDialog.setItems(countryVal.toArray(new String[countryVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etBillPayCountry.setText(countryVal.get(i));
                        etBillPayOperator.getText().clear();
                        etBillPayServiceType.getText().clear();
                        for (MobileRechargeCountryModel countryCode : countryArray) {
                            if (countryCode.getCountryName().contains(etBillPayCountry.getText().toString())) {
                                Log.i("CountryCode", countryCode.getCountryCode());
                                selectedCountryCode = "";
                                selectedCountryCode = countryCode.getCountryCode();
                            }
                        }
                    }
                });

        countryDialog.show();

    }

    public void showBillDialog() {
        final ArrayList<String> billVal = new ArrayList<>();

        for (BillInfoModel bim : billArray) {
            billVal.add(bim.getAccountNo());
        }

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);

        currencyDialog.setTitle("Select your Account no.");

        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        currencyDialog.setItems(billVal.toArray(new String[billVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
//                        etChooseBill.getText().clear();
                        etChooseBill.setText(billVal.get(i));
                        layoutBill.setVisibility(View.VISIBLE);
                        Log.i("Bim Array", String.valueOf(billArray.size()));
                        Log.i("Bim Val", String.valueOf(billVal.size()));
                        for (BillInfoModel bim : billArray) {
                            if (bim.getAccountNo().equals(billArray.get(i).getAccountNo())) {
                                sAccoNo = billArray.get(i).getAccountNo();
                                sAdr = billArray.get(i).getAddress();
                                sBalDue = billArray.get(i).getBalanceDue();
                                sCustN = billArray.get(i).getCustomerName();
                                sState = billArray.get(i).getState();
                                sPType = billArray.get(i).getPayType();

                                Log.i("Bim", sAccoNo);
                                Log.i("Bim", sAdr);
                                Log.i("Bim", sBalDue);
                                Log.i("Bim", sCustN);
                                Log.i("Bim", sState);
                                Log.i("Bim", sPType);

                                tvAccoNo.setText(Html.fromHtml("<font color=#000000>Acc. No. : </font>" + sAccoNo));
                                tvCustN.setText(Html.fromHtml("<font color=#000000>Cust. Name : </font>" + sCustN));
                                tvBalDue.setText(Html.fromHtml("<font color=#000000>Bal. Due : </font>" + sBalDue));
                                tvPType.setText(Html.fromHtml("<font color=#000000>Pay Type : </font>" + sPType));
                                tvAdr.setText(Html.fromHtml("<font color=#000000>Address : </font>" + sAdr));
                                tvState.setText(Html.fromHtml("<font color=#000000>State : </font>" + sState));
                            }
                        }
//                        layoutBill.setVisibility(View.VISIBLE);
                    }
                });

        currencyDialog.show();

    }

    public void showServiceTypeDialog() {
        final ArrayList<String> serviceVal = new ArrayList<>();
        serviceVal.add("Bill Pay");
        serviceVal.add("Purchase Pin");
        serviceVal.add("Topup");
        serviceVal.add("Activate Sim");

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select your Service");

        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        currencyDialog.setItems(serviceVal.toArray(new String[serviceVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etBillPayServiceType.setText(serviceVal.get(i));
                        etBillPayOperator.getText().clear();
                        etMobNo.getText().clear();
                        etBillPayServiceNo.getText().clear();
                        etSimNumber.getText().clear();
                        etZipCode.getText().clear();
                        etBillPayAmount.getText().clear();
                        service_type = null;
                        service_type = serviceVal.get(i);
                        current_type = i + 1;
                        loadingDialog.show();
                        if (service_type.equals("Purchase Pin")) {
                            service_type = "Pin";
                            etBillPayAmount.setVisibility(View.VISIBLE);
                            etMobNo.setVisibility(View.GONE);
                            etBillPayServiceNo.setVisibility(View.GONE);
                            etSimNumber.setVisibility(View.GONE);
                            etZipCode.setVisibility(View.GONE);
                        } else if (service_type.equals("Topup")) {
                            service_type = "Rtr";
                            etMobNo.setVisibility(View.VISIBLE);
                            etBillPayServiceNo.setVisibility(View.GONE);
                            etBillPayAmount.setVisibility(View.VISIBLE);
                            etSimNumber.setVisibility(View.GONE);
                            etZipCode.setVisibility(View.GONE);
                        } else if (service_type.equals("Activate Sim")) {
                            service_type = "Sim";
                            etSimNumber.setVisibility(View.VISIBLE);
                            etZipCode.setVisibility(View.VISIBLE);
                            etMobNo.setVisibility(View.GONE);
                            etBillPayServiceNo.setVisibility(View.GONE);
                            etBillPayAmount.setVisibility(View.VISIBLE);
                        } else if (service_type.equals("Bill Pay")) {
                            service_type = "Bill Pay";
                            etMobNo.setVisibility(View.GONE);
                            etBillPayServiceNo.setVisibility(View.VISIBLE);
                            etBillPayAmount.setVisibility(View.GONE);
                            etSimNumber.setVisibility(View.GONE);
                            etZipCode.setVisibility(View.GONE);
                        }

                        loadServiceType(service_type);

                    }
                });

        currencyDialog.show();
    }

    private String generateURL(int type) {
        String url;
        switch (type) {
            case KEY_PIN:
                url = ApiURLNew.URL_PURCHASE_PIN;
                break;
            case KEY_RTR:
                url = ApiURLNew.URL_PURCHASE_TOPUP;
                break;
            case KEY_SIM:
                url = ApiURLNew.URL_ACTIVATE_SIM;
                break;
            case KEY_FETCH_BILL:
                url = ApiURLNew.URL_FETCH_BILL;
                break;
            case KEY_PAY_BILL:
                url = ApiURLNew.URL_PAY_BILL;
                break;
            default:
                url = "N/A";
                break;
        }
        return url;
    }

    public void loadServiceType(final String sType) {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
//            jsonRequest.put("countryCode", currentAccountArray.get(0).getCurrencySymbol());
            jsonRequest.put("countryCode", selectedCountryCode);
            jsonRequest.put("type", sType);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("PNsType Request", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SLIST_TYPE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject res) {
                    loadingDialog.dismiss();
                    serviceArray.clear();
                    operators.clear();
                    try {
                        Log.i("PNsType URL", ApiURLNew.URL_SLIST_TYPE);
                        Log.i("PNsType Response", res.toString());
                        String codee = res.getString("code");
                        String message = res.getString("message");
                        if (codee != null && codee.equals("S00")) {
                            PNationServiceModel.deleteAll(PNationServiceModel.class);
                            String response = res.getString("response");
                            JSONObject respo = new JSONObject(response);
                            JSONArray details = respo.getJSONArray("details");
                            for (int i = 0; i < details.length(); i++) {
                                JSONObject obj = details.getJSONObject(i);
                                name = obj.getString("name");
                                code = obj.getString("code");
                                minAmount = obj.getString("minAmount");
                                maxAmount = obj.getString("maxAmount");
                                status = obj.getString("status");
                                carrierId = obj.getString("carrierId");
                                type = obj.getString("type");
                                fee = obj.getString("fee");
                                currency = obj.getString("currency");
                                country = obj.getString("country");
                                PNationServiceModel pNationServiceModel = new PNationServiceModel(name, code, minAmount, maxAmount, status, carrierId, type, fee, currency, country);
                                pNationServiceModel.save();
                                serviceArray.add(pNationServiceModel);
                            }
                            if (serviceArray.size() != 0) {
                                for (int p = 0; p < serviceArray.size(); p++) {
                                    operators.add(serviceArray.get(p).getName() + " - " + serviceArray.get(p).getCode());
                                }
//                                if (!operatorAdapter.isEmpty()) {
//                                    operatorAdapter.clear();
//                                }
                                operatorAdapter = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, operators);
                                spOperator.setAdapter(operatorAdapter);
                            }
                        } else if (codee != null && codee.equals("F03")) {
                            showInvalidSessionDialog();
                        } else if (codee != null && codee.equals("F00")) {
                            CustomToast.showMessage(getActivity(), message);
                        } else if (codee != null && codee.equals("F04")) {
                            CustomToast.showMessage(getActivity(), message);
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123456");
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq);
        }
    }

    private void showPNationOperatorDialog() {
        final ArrayList<String> serviceVal = new ArrayList<>();

        for (PNationServiceModel serviceModel : serviceArray) {
            serviceVal.add(serviceModel.getName());
        }

        Log.i("serviceVal", serviceVal.toString());
        Log.i("serviceArray", serviceArray.toString());

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select operator");


        currencyDialog.setItems(serviceVal.toArray(new String[serviceVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etBillPayOperator.setText(serviceVal.get(i));
                        Log.i("Operator selected", etBillPayOperator.getText().toString());
                        for (PNationServiceModel infoModel : serviceArray) {
                            if (infoModel.getName().equals(serviceVal.get(i))) {
                                name = serviceArray.get(i).getName();
                                code = serviceArray.get(i).getCode();
                                minAmount = serviceArray.get(i).getMinAmount();
                                maxAmount = serviceArray.get(i).getMaxAmount();
                                status = serviceArray.get(i).getStatus();
                                carrierId = serviceArray.get(i).getCarrierId();
                                type = serviceArray.get(i).getType();
                                fee = serviceArray.get(i).getFee();
                                currency = serviceArray.get(i).getCurrency();
                                country = serviceArray.get(i).getCountry();
                            }
                        }
                        Log.i("serviceArray name", name);
                        Log.i("serviceArray code", code);
                        Log.i("serviceArray status", status);
                        Log.i("serviceArray carrierId", carrierId);
                        Log.i("serviceArray type", type);
                        Log.i("serviceArray fee", fee);
                        etBillPayOperator.setErrorColor(R.color.colorAcentDark);
                        etBillPayOperator.setError("C" + code + " : Min. " + minAmount + " : Max. " + maxAmount);
                    }
                });

        currencyDialog.show();
    }


    public void puchasePin(final int sType) {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("countryCode", selectedCountryCode);
            jsonRequest.put("skuid", code);
            jsonRequest.put("amount", etBillPayAmount.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("PurchasePin Request", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, generateURL(sType), jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject res) {
                    loadingDialog.dismiss();
                    try {
                        Log.i("PurchasePin URL", generateURL(sType));
                        Log.i("PurchasePin Response", res.toString());
                        String codee = res.getString("code");
                        String message = res.getString("message");
                        if (codee != null && codee.equals("S00")) {
                            String ress = res.getString("response");
                            JSONObject respo = new JSONObject(ress);
                            String det = respo.getString("details");
                            JSONObject details = new JSONObject(det);
                            boolean success = details.getBoolean("success");
                            if (success) {
                                String invoiceNumber = details.getString("invoiceNumber");
                                String carrierName = details.getString("carrierName");
                                String pinNumber = details.getString("pinNumber");
                                String controlNo = details.getString("controlNo");
                                String amount = details.getString("amount");
                                String transactionId = details.getString("transactionId");
//                                showResponseDialog(message);
                                showSuccessDialogForPin(message, carrierName, pinNumber);
                            }
                        } else if (codee != null && codee.equals("F00")) {
                            showResponseDialog(message);
                        } else if (codee != null && codee.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123456");
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq);
        }
    }

    public void puchaseTopUp(final int sType) {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("countryCode", selectedCountryCode);
            jsonRequest.put("skuid", code);
            jsonRequest.put("mobile", etMobNo.getText().toString());
            jsonRequest.put("amount", etBillPayAmount.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("puchaseTopUp Request", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, generateURL(sType), jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject res) {
                    loadingDialog.dismiss();
                    try {
                        Log.i("puchaseTopUp URL", generateURL(sType));
                        Log.i("puchaseTopUp Response", res.toString());
                        String codee = res.getString("code");
                        String message = res.getString("message");
                        if (codee != null && codee.equals("S00")) {
                            String ress = res.getString("response");
                            JSONObject respo = new JSONObject(ress);
                            String det = respo.getString("details");
                            JSONObject details = new JSONObject(det);
                            boolean success = details.getBoolean("success");
                            if (success) {
                                String invoiceNo = details.getString("invoiceNo");
                                String operatorId = details.getString("operatorId");
                                String authCode = details.getString("authCode");
                                String amount = details.getString("amount");
                                String transactionId = details.getString("transactionId");
//                                generateTopUpPSuccess(message, invoiceNo, operatorId, authCode, amount, transactionId);
                                showResponseDialog(message);
                            }
                        } else if (codee != null && codee.equals("F00")) {
                            showResponseDialog(message);
                        } else if (codee != null && codee.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123456");
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq);
        }
    }

    public void activateSim(final int sType) {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("countryCode", selectedCountryCode);
            jsonRequest.put("skuid", code);
            jsonRequest.put("amount", etBillPayAmount.getText().toString());
            jsonRequest.put("simNumber", etSimNumber.getText().toString());
            jsonRequest.put("zipCode", etZipCode.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("activateSim Request", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, generateURL(sType), jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject res) {
                    loadingDialog.dismiss();
                    try {
                        Log.i("activateSim URL", generateURL(sType));
                        Log.i("activateSim Response", res.toString());
                        String codee = res.getString("code");
                        String message = res.getString("message");
                        if (codee != null && codee.equals("S00")) {
                            JSONObject respo = res.getJSONObject("response");
                            JSONObject details = respo.getJSONObject("details");
                            boolean success = details.getBoolean("success");
                            if (success) {
                                String invoiceNo = details.getString("invoiceNo");
                                String operatorId = details.getString("operatorId");
                                String authCode = details.getString("pinNumber");
                                String amount = details.getString("amount");
                                String transactionId = details.getString("transactionId");
//                                generateTopUpPSuccess(message, invoiceNo, operatorId, authCode, amount, transactionId);
                                showResponseDialog(message);
                            }
                        } else if (codee != null && codee.equals("F00")) {
                            showResponseDialog(message);
                        } else if (codee != null && codee.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123456");
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq);
        }
    }


    public void fetchBillDetails(final int sType) {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("countryCode", selectedCountryCode);
            jsonRequest.put("skuId", code);
            jsonRequest.put("accountNo", etBillPayServiceNo.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("fetchBillD Request", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, generateURL(sType), jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject res) {
                    loadingDialog.dismiss();
                    billArray.clear();
                    try {
                        Log.i("fetchBillD URL", generateURL(sType));
                        Log.i("fetchBillD Response", res.toString());
                        String codee = res.getString("code");
                        String message = res.getString("message");
                        if (codee != null && codee.equals("S00")) {
                            String ress = res.getString("response");
                            JSONObject respo = new JSONObject(ress);
                            String det = respo.getString("details");
                            JSONObject details = new JSONObject(det);
                            boolean success = details.getBoolean("success");
                            if (success) {
                                JSONArray billsArr = details.getJSONArray("bills");
                                for (int l = 0; l < billsArr.length(); l++) {
                                    JSONObject bills = billsArr.getJSONObject(l);
                                    String accountNo = bills.getString("accountNo");
                                    String address = bills.getString("address");
                                    String balanceDue = bills.getString("balanceDue");
                                    String customerName = bills.getString("customerName");
                                    String state = bills.getString("state");
                                    String payType = bills.getString("payType");
                                    BillInfoModel biModel = new BillInfoModel(accountNo, address, balanceDue, customerName, state, payType);
                                    biModel.save();
                                    billArray.add(biModel);
                                }
                                showBillPDialog();
                            }
                        } else if (codee != null && codee.equals("F00")) {
                            showInvalidSessionDialog();
//                            showResponseDialog(message);
                        } else if (codee != null && codee.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123456");
                    map.put("Content-Type", "application/json");
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq);
        }
    }

    public void proceedPayBill(final int sType, final AlertDialog adialog) {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("countryCode", selectedCountryCode);
            jsonRequest.put("skuid", code);
            jsonRequest.put("mobileNo", etBillPayServiceNo.getText().toString());
            jsonRequest.put("accountNo", sAccoNo);
            jsonRequest.put("senderName", session.getUserFirstName());
            jsonRequest.put("senderMobile", session.getUserMobileNo());
            jsonRequest.put("accountType", sPType);
            jsonRequest.put("amount", sBalDue);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("ProceedBill Request", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, generateURL(sType), jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject res) {
                    loadingDialog.dismiss();
                    adialog.dismiss();
                    billArray.clear();
                    try {
                        Log.i("ProceedBill URL", generateURL(sType));
                        Log.i("ProceedBill Response", res.toString());
                        String codee = res.getString("code");
                        String message = res.getString("message");
                        String respo = res.getString("response");
                        JSONObject response = new JSONObject(respo);
                        JSONObject details = null;
                        String errMsg = "";
                        if (codee != null && codee.equals("S00")) {
                            showResponseDialog(message);
                        } else if (codee != null && codee.equals("F00")) {
                            showResponseDialog(message);
                        } else if (codee != null && codee.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
                            if (response.has("details")) {
                                String detail = response.getString("details");
                                details = new JSONObject(detail);
                            }
                            if (details != null && details.has("errMsg")) {
                                errMsg = details.getString("errMsg");
                            }
//                            showResponseDialog(errMsg);
                            CustomToast.showMessage(getActivity(), errMsg);
                            adialog.dismiss();
                        }
                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener()

            {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123456");
                    map.put("Content-Type", "application/json");
                    return map;
                }
            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().

                    addToRequestQueue(postReq);
        }
    }


    public void showConfirmDialogForPin() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessageForPin()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                CustomToast.showMessage(getActivity(), "Done");
                loadingDialog.show();
                puchasePin(1);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showConfirmDialogForRtr() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessageForRtr()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                CustomToast.showMessage(getActivity(), "Done");
                loadingDialog.show();
                puchaseTopUp(2);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showConfirmDialogForSim() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessageForSim()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                CustomToast.showMessage(getActivity(), "Done");
                loadingDialog.show();
                activateSim(3);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showConfirmDialogForBill() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessageForFBill()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                CustomToast.showMessage(getActivity(), "Done");
                loadingDialog.show();
                fetchBillDetails(4);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showResponseDialog(String msg) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, msg);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendRefresh();
            }
        });
        builder.show();
    }

    public String generateMessageForPin() {
        String source =
                "<b><font color=#000000> Service Type: </font></b>" + "<font color=#000000>" + etBillPayServiceType.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + etBillPayOperator.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Amount: </font></b>" + "<font>" + etBillPayAmount.getText().toString() + "</font><br><br>" +
                        "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
        return source;
    }

    public String generateMessageForSim() {
        String source =
                "<b><font color=#000000> Service Type: </font></b>" + "<font color=#000000>" + etBillPayServiceType.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + etBillPayOperator.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Sim Number: </font></b>" + "<font color=#000000>" + etSimNumber.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Amount: </font></b>" + "<font>" + etBillPayAmount.getText().toString() + "</font><br><br>" +
                        "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
        return source;
    }

    public String generateMessageForRtr() {
        String source =
                "<b><font color=#000000> Service Type: </font></b>" + "<font color=#000000>" + etBillPayServiceType.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + etBillPayOperator.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Mobile No.: </font></b>" + "<font color=#000000>" + etMobNo.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Amount: </font></b>" + "<font>" + etBillPayAmount.getText().toString() + "</font><br><br>" +
                        "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
        return source;
    }

    public String generateMessageForFBill() {
        String source =
                "<b><font color=#000000> Service Type: </font></b>" + "<font color=#000000>" + etBillPayServiceType.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + etBillPayOperator.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Account No.: </font></b>" + "<font color=#000000>" + etBillPayServiceNo.getText().toString() + "</font><br>" +
                        "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
        return source;
    }

    public String generatePinPSuccess(String message, String carrierName, String pinNo) {
        String source =
                "<b><font color=#ff0000>" + message + "</font></b><br>" +
                        "<b><font color=#000000> Service Provider: </font></b>" + "<font color=#000000>" + carrierName + "</font><br>" +
                        "<b><font color=#000000> Your PIN: </font></b>" + "<font>" + pinNo + "</font><br><br>";
        return source;
    }

    public void showSuccessDialogForPin(String m, String cN, String pN) {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generatePinPSuccess(m, cN, pN)));
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendRefresh();
            }
        });
        builder.show();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validateCountry() {
        if (etBillPayCountry.getText().toString().trim().isEmpty()) {
            etBillPayCountry.setError("Please select Country");
            requestFocus(etBillPayCountry);
            return false;
        }
        return true;
    }

    private boolean validateService() {
        if (etBillPayServiceType.getText().toString().trim().isEmpty()) {
            etBillPayServiceType.setError("Please select Service Type");
            requestFocus(etBillPayServiceType);
            return false;
        }
        return true;
    }

    private boolean validateOperator() {
        if (etBillPayOperator.getText().toString().trim().isEmpty()) {
            etBillPayOperator.setError("Please select Operator");
            requestFocus(etBillPayOperator);
            return false;
        }
        return true;
    }

    private boolean validateMobileNo() {
        if (etMobNo.getText().toString().trim().isEmpty()) {
            etMobNo.setError("Enter mobile no.");
            requestFocus(etMobNo);
            return false;
        }
        return true;
    }

    private boolean validateAccountNo() {
        if (etBillPayServiceNo.getText().toString().trim().isEmpty() || etBillPayServiceNo.getText().toString().length() < 10) {
            etBillPayServiceNo.setError("Enter valid mob. no.");
            requestFocus(etBillPayServiceNo);
            return false;
        }
        return true;
    }

    private boolean validateAmount() {
        if (etBillPayAmount.getText().toString().trim().isEmpty() || Integer.parseInt(etBillPayAmount.getText().toString().trim()) < 10) {
            etBillPayAmount.setError("Enter min. amount 10");
            requestFocus(etBillPayAmount);
            return false;
        }
        return true;
    }

    private boolean validateSimNo() {
        if (etSimNumber.getText().toString().trim().isEmpty()) {
            etSimNumber.setError("Enter valid amount");
            requestFocus(etSimNumber);
            return false;
        }
        return true;
    }

    private boolean validateZipCode() {
        if (etZipCode.getText().toString().trim().isEmpty()) {
            etZipCode.setError("Enter valid amount");
            requestFocus(etZipCode);
            return false;
        }
        return true;
    }

    private void validForPin() {
        if (!validateAmount()) {
            return;
        }
//        CustomToast.showMessage(getActivity(), "validForPin()");
//        loadingDialog.show();
//        puchasePin(1);
        showConfirmDialogForPin();
    }

    private void validForRtr() {
        if (!validateMobileNo()) {
            return;
        }
        if (!validateAmount()) {
            return;
        }
//        CustomToast.showMessage(getActivity(), "validForPin()");
//        loadingDialog.show();
        showConfirmDialogForRtr();
    }

    private void validForSim() {
        if (!validateSimNo()) {
            return;
        }
        if (!validateZipCode()) {
            return;
        }
        if (!validateAmount()) {
            return;
        }
//        CustomToast.showMessage(getActivity(), "validForSim()");
        showConfirmDialogForSim();
    }

    private void validForPBill() {
        if (!validateAccountNo()) {
            return;
        }
//        CustomToast.showMessage(getActivity(), "validForPayBill()");

        billMob = etBillPayServiceNo.getText().toString();
        loadingDialog.show();
//        showConfirmDialogForBill();
        fetchBillDetails(4);
    }

    private void validatePay(final String s) {
        switch (s) {
            case "Pin":
                validForPin();
                break;
            case "Rtr":
                validForRtr();
                break;
            case "Sim":
                validForSim();
                break;
            case "Bill Pay":
                validForPBill();
                break;
            default:
                CustomToast.showMessage(getActivity(), "N/A");
                break;
        }
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private boolean validateBillInfo() {
        if (etChooseBill.getText().toString().isEmpty()) {
            etChooseBill.setError("Select your account");
            requestFocus(etChooseBill);
            return false;
        }
        return true;
    }

    private boolean validateSenderN() {
        if (etBillSenderName.getText().toString().isEmpty() || etBillSenderName.getText().toString().length() < 3) {
            etBillSenderName.setError("Enter name of min. 3 character");
            requestFocus(etBillSenderName);
            return false;
        }
        return true;
    }

    private boolean validateSenderNo() {
        if (etBillSenderMob.getText().toString().isEmpty() || etBillSenderMob.getText().toString().length() < 10) {
            etBillSenderMob.setError("Enter valid mobile no.");
            requestFocus(etBillSenderMob);
            return false;
        }
        return true;
    }

    public void showBillPDialog() {
        LayoutInflater myLayout = LayoutInflater.from(getActivity());

        Button btSubmit, btCancel;

        View dialogView = myLayout.inflate(R.layout.dialog_custom_bill_info, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);

        layoutBill = (LinearLayout) dialogView.findViewById(R.id.layoutBill);
        etChooseBill = (MaterialEditText) dialogView.findViewById(R.id.etChooseBill);
        etBillSenderName = (TextView) dialogView.findViewById(R.id.etBillSenderName);
        etBillSenderMob = (TextView) dialogView.findViewById(R.id.etBillSenderMob);
        tvAccoNo = (TextView) dialogView.findViewById(R.id.tvAccoNo);
        tvAdr = (TextView) dialogView.findViewById(R.id.tvAdr);
        tvBalDue = (TextView) dialogView.findViewById(R.id.tvBalDue);
        tvCustN = (TextView) dialogView.findViewById(R.id.tvCustN);
        tvState = (TextView) dialogView.findViewById(R.id.tvState);
        tvPType = (TextView) dialogView.findViewById(R.id.tvPType);
        btCancel = (Button) dialogView.findViewById(R.id.btCancel);
        btSubmit = (Button) dialogView.findViewById(R.id.btSubmit);

        etChooseBill.setFocusable(false);
        layoutBill.setVisibility(View.GONE);

//        etBillSenderName.setText(Html.fromHtml("<font color=#000000>Payee Name : </font>" + session.getUserFirstName()));
//        etBillSenderMob.setText(Html.fromHtml("<font color=#000000>Payee Mob. : </font>" + String.valueOf(session.getUserMobileNo())));

        etChooseBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBillDialog();
            }
        });

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateBillInfo()) {
                    return;
                } else {
                    loadingDialog.show();
                    proceedPayBill(5, alertDialog);
                    /*if (!validateSenderN()) {
                        return;
                    }
                    if (!validateSenderNo()) {
                        return;
                    } else {
                        loadingDialog.show();
                        proceedPayBill(5, alertDialog);
//                        CustomToast.showMessage(getActivity(), "Bill Payment Done");
                    }*/
                }
            }
        });

        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutBill.setVisibility(View.GONE);
                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendRefresh() {
        getActivity().finish();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PNationServiceModel.deleteAll(PNationServiceModel.class);
        BillInfoModel.deleteAll(BillInfoModel.class);
    }

    @Override
    public void onStop() {

        super.onStop();

        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }

    }
}
