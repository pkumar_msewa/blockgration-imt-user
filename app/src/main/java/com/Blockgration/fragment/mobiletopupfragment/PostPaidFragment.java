package com.Blockgration.fragment.mobiletopupfragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.MobileRechargeCountryModel;
import com.Blockgration.model.PrepaidOperatorModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.util.CheckLog;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.PayingDetailsValidation;
import com.Blockgration.util.SecurityUtil;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ksf on 8/8/2016.
 */
public class PostPaidFragment extends Fragment {
    private View rootView;
    private Button btnPrepaidPay;
    private MaterialEditText etPrepaidAmount, etPrepaidPhoneNo, etPrepaidProvider, etPrepaidCountry;

    private View focusView = null;
    private boolean cancel;

    //Variables
    private String amount, provider, countryCode, mobileNo;
    private String maxAmount = null, minAmount = null;
    private LoadingDialog loadingDialog;
    //    private List<MobileRechargeModel> operatorList;
    private List<MobileRechargeCountryModel> countryList;
    private List<PrepaidOperatorModel> operatorList;
    private TextView tvPrepaidAmount;
    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();

    private String tag_json_obj = "json_obj_req";
    private String operatorCode;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = new LoadingDialog(getActivity());
        countryList = AppMetadata.getCountry();
        operatorList = AppMetadata.getOperator();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_prepaid, container, false);
        etPrepaidProvider = (MaterialEditText) rootView.findViewById(R.id.etPrepaidProvider);
        etPrepaidPhoneNo = (MaterialEditText) rootView.findViewById(R.id.etPrepaidPhoneNo);
        etPrepaidAmount = (MaterialEditText) rootView.findViewById(R.id.etPrepaidAmount);
        etPrepaidCountry = (MaterialEditText) rootView.findViewById(R.id.etPrepaidCountry);
        btnPrepaidPay = (Button) rootView.findViewById(R.id.btnPrepaidPay);
        tvPrepaidAmount = (TextView) rootView.findViewById(R.id.tvPrepaidAmount);

        etPrepaidCountry.setFocusable(false);
        etPrepaidProvider.setFocusable(false);


        etPrepaidCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPrepaidProvider.setText("");
                showCountryDialog();
            }
        });

        etPrepaidProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etPrepaidCountry.getText().toString().equals("Jamaica")) {
                    if (etPrepaidCountry.getText().toString() != null && !etPrepaidCountry.getText().toString().isEmpty()) {
                        if (operatorList.size() != 0) {
                            showOperatorDialog();
                        }
                    } else {
                        CustomToast.showMessage(getActivity(), "Sorry no operator available for this country");
                    }
                } else {
                    CustomToast.showMessage(getActivity(), "Select your country first");
                }

            }
        });


        btnPrepaidPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPay();
            }
        });
        return rootView;
    }


    private void attemptPay() {
        etPrepaidAmount.setError(null);
        etPrepaidPhoneNo.setError(null);
        etPrepaidProvider.setError(null);
        etPrepaidCountry.setError(null);

        cancel = false;

        amount = etPrepaidAmount.getText().toString();
        provider = etPrepaidProvider.getText().toString();
        mobileNo = etPrepaidPhoneNo.getText().toString();

        checkPayAmount(amount);
        checkPhone(mobileNo);
        checkCountry(etPrepaidCountry.getText().toString());
        checkProvider(provider);

        if (cancel) {
            focusView.requestFocus();
        } else {
            showPayDialog();
        }
    }


    public void showPayDialog() {

        CharSequence result = "";
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            result = Html.fromHtml(generateMessage(),Html.FROM_HTML_MODE_LEGACY);
//        } else {
        result = Html.fromHtml(generateMessage());
//        }

        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                promotePayBill();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public String generateMessage() {
        String source =
                "<b><font color=#000000> Mobile no: </font></b>" + "<font color=#000000>" + etPrepaidPhoneNo.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Service Provide: </font></b>" + "<font color=#000000>" + etPrepaidProvider.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Country: </font></b>" + "<font>" + etPrepaidCountry.getText().toString() + "</font><br>" +
                        "<b><font color=#000000> Amount: </font></b>" + "<font>" + etPrepaidAmount.getText().toString() + "</font><br><br>" +
                        "<b><font color=#03467b> Are you sure you want to proceed?</font></b><br>";
        return source;
    }

    private void checkPayAmount(String amount) {
        CheckLog amountCheckLog = PayingDetailsValidation.checkAmount(amount);
        if (!amountCheckLog.isValid) {
            etPrepaidAmount.setError(getString(amountCheckLog.msg));
            focusView = etPrepaidAmount;
            cancel = true;
        }
//        }else if (Double.parseDouble(amount)>Double.parseDouble(maxAmount)||Double.parseDouble(amount)<Double.parseDouble(minAmount)){
////            etPrepaidAmount.setError("Amount cant be less than "+minAmount+" and more than "+ maxAmount);
//            etPrepaidAmount.setError("Please enter valid amount");
//            focusView = etPrepaidAmount;
//            cancel = true;
//        }
    }

    private void checkCountry(String amount) {
        CheckLog amountCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!amountCheckLog.isValid) {
            etPrepaidCountry.setError(getString(amountCheckLog.msg));
            focusView = etPrepaidCountry;
            cancel = true;
        }
    }

    private void checkProvider(String amount) {
        CheckLog amountCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!amountCheckLog.isValid) {
            etPrepaidProvider.setError(getString(amountCheckLog.msg));
            focusView = etPrepaidProvider;
            cancel = true;
        }
    }

    private void checkPhone(String phNo) {
        CheckLog phoneCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
        if (!phoneCheckLog.isValid) {
            etPrepaidPhoneNo.setError(getString(phoneCheckLog.msg));
            focusView = etPrepaidPhoneNo;
            cancel = true;
        }
    }

    public void showCountryDialog() {
        final ArrayList<String> countryVal = new ArrayList<>();

        for (MobileRechargeCountryModel accModel : countryList) {
            countryVal.add(accModel.getCountryName());
        }

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select your Country");

        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        currencyDialog.setItems(countryVal.toArray(new String[countryVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etPrepaidCountry.setText(countryList.get(i).getCountryName());
                        countryCode = countryList.get(i).getCountryCode();
//                        Select specificSpecificQueryGt = Select.from(MobileRechargeModel.class).where(Condition.prop("countrycode").eq(countryCode));
//                        operatorList = specificSpecificQueryGt.list();
//                        etPrepaidProvider.getText().clear();
                    }
                });

        currencyDialog.show();
    }

    public void showOperatorDialog() {
        final ArrayList<String> countryVal = new ArrayList<>();

        for (PrepaidOperatorModel opModel : operatorList) {
            countryVal.add(opModel.getOperatorName());
        }

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select your Operator");

        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        currencyDialog.setItems(countryVal.toArray(new String[countryVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etPrepaidProvider.setText(operatorList.get(i).getOperatorName());
                        operatorCode = operatorList.get(i).getOperatorCode();
                        tvPrepaidAmount.setText("Min rechargeable amount: " + minAmount + "\nMax rechargeable amount: " + maxAmount);
                        tvPrepaidAmount.setVisibility(View.GONE);
                    }
                });

        currencyDialog.show();
    }

    public void promotePayBill() {
        loadingDialog.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("mobileNo", etPrepaidPhoneNo.getText().toString());
            jsonRequest.put("amount", etPrepaidAmount.getText().toString());
            jsonRequest.put("area", etPrepaidCountry.getText().toString());
            jsonRequest.put("serviceProvider", operatorCode);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_POSTPAID, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Exchange Money", response.toString());
                    loadingDialog.dismiss();
                    try {
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            String jsonString = response.getString("response");
                            JSONObject jsonObject = new JSONObject(jsonString);
                            String sucessMessage = jsonObject.getString("details");
                            etPrepaidAmount.getText().clear();
                            etPrepaidCountry.getText().clear();
                            etPrepaidProvider.getText().clear();
                            etPrepaidPhoneNo.getText().clear();
                            loadingDialog.dismiss();
                            sendRefresh();
                            CustomToast.showMessage(getActivity(), sucessMessage);
                        } else if (code != null && code.equals("F03")) {
                            loadingDialog.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadingDialog.dismiss();
                            String errorMessage = response.getString("message");
                            CustomToast.showMessage(getActivity(), errorMessage);
                        }

                    } catch (JSONException e) {
                        loadingDialog.dismiss();
                        CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadingDialog.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", SecurityUtil.getSecurityKey(jsonRequest.toString()));
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }


}
