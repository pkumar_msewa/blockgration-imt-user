package com.Blockgration.fragment.fragmentgiftcard;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.adapter.GiftCardCatAdapter;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.GiftCardCatModel;
import com.Blockgration.model.UserModel;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by kashifimam on 21/02/17.
 */

public class GiftCardCatFragment extends Fragment {

    private RecyclerView rvGiftCardCat;
    private ProgressBar pbGiftCardCat;
    private UserModel session = UserModel.getInstance();
    String cardTerms;

    //Volley
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gift_card, container, false);
        rvGiftCardCat = (RecyclerView) rootView.findViewById(R.id.rvGiftCardCat);
        pbGiftCardCat = (ProgressBar) rootView.findViewById(R.id.pbGiftCardCat);
        getCardCategory();
        return rootView;
    }


    public void getCardCategory() {
        String resp = loadJSONFromAsset();

        try {
            JSONObject jsonObj = new JSONObject(resp);
            Log.i("EventsCatResponse", jsonObj.toString());
            String code = jsonObj.getString("success");
            String message = jsonObj.getString("message");

            if (code != null && code.equals("true")) {
                List<GiftCardCatModel> cardListCatArray = new ArrayList<>();
                String jsonDetail = jsonObj.getString("brandObject");

                JSONArray cardCatArray = new JSONArray(jsonDetail);

                for (int i = 0; i < cardCatArray.length(); i++) {
                    JSONObject c = cardCatArray.getJSONObject(i);
                    boolean cardSuccess = c.getBoolean("success");
                    String cardMessage = c.getString("message");
                    String cardHash = c.getString("hash");
                    String cardName = c.getString("name");
                    String cardDescription = c.getString("description");
                    String cardTerms = c.getString("terms");
                    String cardImage = c.getString("images");
                    String cardWebsite = c.getString("website");
                    boolean cardActive = c.getBoolean("active");
                    boolean cardOnline = c.getBoolean("online");
                    GiftCardCatModel eveCatModel = new GiftCardCatModel(cardHash, cardName, cardImage, cardDescription, cardTerms, cardMessage, cardWebsite, cardActive, cardOnline, cardSuccess);
                    cardListCatArray.add(eveCatModel);
                }
                if (cardListCatArray != null && cardListCatArray.size() != 0) {
                    int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
                    rvGiftCardCat.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
                    GridLayoutManager manager = new GridLayoutManager(getActivity(), 2);
                    rvGiftCardCat.setLayoutManager(manager);
                    rvGiftCardCat.setHasFixedSize(true);

                    GiftCardCatAdapter eveCatAdp = new GiftCardCatAdapter(getActivity(), cardListCatArray);
                    rvGiftCardCat.setAdapter(eveCatAdp);
                    pbGiftCardCat.setVisibility(View.GONE);
                    rvGiftCardCat.setVisibility(View.VISIBLE);

                } else {
                    Toast.makeText(getActivity(), "Empty Array", Toast.LENGTH_SHORT).show();
                    pbGiftCardCat.setVisibility(View.GONE);
                }
            } else if (code.equalsIgnoreCase("F03")) {
                showInvalidSessionDialog();
            } else {
                if (message.equalsIgnoreCase("Please login and try again.")) {
                    showInvalidSessionDialog();
                    pbGiftCardCat.setVisibility(View.GONE);
                } else {
                    CustomToast.showMessage(getActivity(), message);
                    pbGiftCardCat.setVisibility(View.GONE);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            pbGiftCardCat.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
        }


    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }


    @Override
    public void onDetach() {
//        postReq.cancel();
        super.onDetach();
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }


    public String loadJSONFromAsset() {
        pbGiftCardCat.setVisibility(View.VISIBLE);
        rvGiftCardCat.setVisibility(View.GONE);
        String json;
        try {
            InputStream is = getActivity().getAssets().open("GCICAT.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
