package com.Blockgration.fragment.fragmentgiftcard;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidquery.AQuery;
import com.Blockgration.adapter.AmountAdapter;
import com.Blockgration.adapter.GiftCardItemAdapter;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.MainActivity;
import com.Blockgration.Userwallet.activity.shopping.AddRemoveCartListner;
import com.Blockgration.Userwallet.activity.shopping.CustomSuccessDialog;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.GiftCardCatModel;
import com.Blockgration.model.GiftCardItemModel;
import com.Blockgration.model.UserModel;

import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.ServiceUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;




//import in.payqwik.payqwik.activity.AdventureActivity.VideoPlayerActivity;

/**
 * Created by kashifimam on 21/02/17.
 */

public class PurchaseFragment extends Fragment implements View.OnClickListener {

    private View rootView;

    private EditText etRegName, etRegMobile, etRegEmail, etmsg;
    private Button btnpayment;
    private TextView tvdenName;
    private TextInputLayout ilRegName, ilRegMobile, ilRegEmail, ilmsg;
    private String Name, email, address, city, state, country, personalMsg, date, hash, clientOrderId, mobileNum, price, quantity;

    private Toolbar toolbar;
    private ImageButton ivBackBtn;
    private LoadingDialog loadDlg;
    private RecyclerView rvGiftCarditem;
    private LinearLayout llpbGiftCarditem;
    private String itemhash, itemImage, brandName;
    private UserModel session = UserModel.getInstance();
    private String tag_json_obj = "json_events";
    private JsonObjectRequest postReq;
    private JSONObject jsonRequests;
    private AQuery aq;


    private GiftCardItemAdapter itemAdp;
    private GiftCardCatModel giftCardCatModel;
    private RecyclerView llAmount;

    private final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final Pattern hasLowercase = Pattern.compile("[a-z]");
    private final Pattern hasNumber = Pattern.compile("\\d");
    private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");
    Calendar cl = Calendar.getInstance();

    private HashMap<String, String> hashMap;

    private static String Ammount;
    int postion;
    private TextView[] myTextViews;
    private Button[] button;
    private AmountAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_purchases, container, false);
        hashMap = new HashMap<>();
        etRegName = (EditText) rootView.findViewById(R.id.etRegName);
        etRegMobile = (EditText) rootView.findViewById(R.id.etRegMobile);
        etRegEmail = (EditText) rootView.findViewById(R.id.etRegEmail);
        etmsg = (EditText) rootView.findViewById(R.id.etmsg);
        //tvdenName = (TextView) rootView.findViewById(R.id.tvdenName);

        btnpayment = (Button) rootView.findViewById(R.id.btnRegPay);

        ilRegName = (TextInputLayout) rootView.findViewById(R.id.ilRegName);
        ilRegMobile = (TextInputLayout) rootView.findViewById(R.id.ilRegMobile);
        ilRegEmail = (TextInputLayout) rootView.findViewById(R.id.ilRegEmail);
        ilmsg = (TextInputLayout) rootView.findViewById(R.id.ilmsg);

        llAmount = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        llAmount.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        llAmount.setLayoutManager(mLayoutManager);
        loadDlg = new LoadingDialog((getActivity()));

        rvGiftCarditem = (RecyclerView) rootView.findViewById(R.id.rvGiftCarditem);
        llpbGiftCarditem = (LinearLayout) rootView.findViewById(R.id.llpbGiftCarditem);
        Bundle bundle = getArguments();
        giftCardCatModel = bundle.getParcelable("model");

        getCardCategory();

        Button btnRegPay = (Button) rootView.findViewById(R.id.btnRegPay);
        btnRegPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //submitForm();

                CustomToast.showMessage(getActivity(), "COMMING SOON");
            }
        });
        return rootView;
    }

    private void submitForm() {

        if (!validateName()) {
            return;
        }


        if (!validateMobile()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }


//        loadDlg.show();
//
        Name = etRegName.getText().toString();
        email = etRegEmail.getText().toString();
        mobileNum = etRegMobile.getText().toString();
//        address = etRegMobile.getText().toString();
       /* city = etCity.getText().toString();
        state = etState.getText().toString();
        country = etCountry.getText().toString();*/


//        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//        date = df.format(cl.getTime());
//
//        Log.i("DATE", "DATE : " + date);
        Log.i("HAsoMap", hashMap.toString());
        if (Ammount != null && !Ammount.toString().isEmpty()) {
            String amout = Ammount.replace("₹", "");
            Log.i("amout", amout.trim());
            try {
                JSONObject jsonObject = new JSONObject(hashMap);
                promotePay(jsonObject.get(amout.trim()).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            String s = null;
//            for (String key : hashMap.keySet()) {
//                if (key.equalsIgnoreCase(amout)) {
//                    s = hashMap.get(key);

//
//                }
//            }
//            if(s!=null){
//                Log.i("amout1",s);
//                promotePay(s);
//            }
//            Log.i("gaspMap", hashMap.get(amout).toString());

        } else {
            CustomToast.showMessage(getActivity(), "please select Amount");
        }

        //promotePay();
    }

    private boolean validateName() {
        if (etRegName.getText().toString().trim().isEmpty()) {
            ilRegName.setError("Enter Name");
            requestFocus(etRegName);
            return false;
        } else {
            ilRegName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMobile() {
        if (etRegMobile.getText().toString().trim().isEmpty() || etRegMobile.getText().toString().trim().length() < 10) {
            ilRegMobile.setError("Enter 10 digit no");
            requestFocus(etRegMobile);
            return false;
        } else {
            ilRegMobile.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        String email = etRegEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            ilRegEmail.setError("Enter valid email");
            requestFocus(etRegEmail);
            return false;
        } else {
            ilRegEmail.setErrorEnabled(false);
        }

        return true;
    }


    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    public void getCardCategory() {
        String jsonObj = loadJSONFromAsset();

        try {
            Log.i("EventsCatResponse", jsonObj.toString());
            JSONObject jsonObject=new JSONObject(jsonObj);
            String code = jsonObject.getString("success");
            String message = jsonObject.getString("message");
            loadDlg.dismiss();
            if (code != null && code.equals("true")) {
                final List<GiftCardItemModel> cardListItemArray = new ArrayList<>();

                JSONArray cardItemArray = jsonObject.getJSONArray("denominationObject");

                for (int i = 0; i < cardItemArray.length(); i++) {
                    JSONObject c = cardItemArray.getJSONObject(i);
                    long itemID = c.getLong("denominationId");
                    int itemName = c.getInt("denominationName");
                    String itemSkuID = c.getString("skuId");
                    String itemType = c.getString("type");
                    String itemValueType = c.getString("valueType");

                    hashMap.put(String.valueOf(itemName), itemType + "," + itemSkuID);


//                                GiftCardItemModel itemlist = new GiftCardItemModel();
//                                itemlist.setId(itemID);
//                                itemlist.setName(itemName);
//                                itemlist.setSkuId(itemSkuID);
//                                itemlist.setType(itemType);
//                                itemlist.setValueType(itemValueType);
//

                    GiftCardItemModel giftCardItemModel = new GiftCardItemModel(itemID, itemName, itemSkuID, itemType, itemValueType);
                    cardListItemArray.add(giftCardItemModel);

                }
                if (cardListItemArray.size() != 0) {
                    mAdapter = new AmountAdapter(getActivity(), cardListItemArray, new AddRemoveCartListner() {
                        @Override
                        public void taskCompleted(String response) {
                            Ammount = response;
                        }
                    });
                    llAmount.setAdapter(mAdapter);


                } else {
                    Toast.makeText(getActivity(), "Empty Array", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (message.equalsIgnoreCase("Please login and try again.")) {
                    showInvalidSessionDialog();

                } else {
                    CustomToast.showMessage(getActivity(), message);
                }
            }


        } catch (
                JSONException e)

        {
            loadDlg.dismiss();
            e.printStackTrace();
            Toast.makeText(getActivity(), "Exception caused while fetching data", Toast.LENGTH_SHORT).show();
        }
    }

//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    error.printStackTrace();
//                    loadDlg.dismiss();
//                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000 * 5;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//
//
//    }


    private void promotePay(String s) {
        loadDlg.show();


        jsonRequests = new JSONObject();
        try {
            Log.i("amountssss", Ammount);

            String[] demo = s.split(",");
            jsonRequests.put("sessionId", session.getUserSessionId());
            jsonRequests.put("denomination", Ammount.replace("₹", "").trim());
            jsonRequests.put("brandName", giftCardCatModel.getCardName());
            jsonRequests.put("billingName", session.getUserFirstName());
            jsonRequests.put("billingEmail", session.getUserEmail());


            jsonRequests.put("billingAddressLine1", "Kormangla");
            jsonRequests.put("billingCity", "Bangalore");
            jsonRequests.put("billingState", "Bangalore");
            jsonRequests.put("billingCountry", "India");
            jsonRequests.put("billingZip", "560036");


//            jsonRequests.put("receiversName", session.getUserFirstName());
//            jsonRequests.put("receiversMobileNumber", session.getUserMobileNo());
//            jsonRequests.put("receiversEmail", session.getUserEmail());


            jsonRequests.put("receiversName", Name);
            jsonRequests.put("receiversMobileNumber", mobileNum);
            jsonRequests.put("receiversEmail", email);

            Log.i("NAME", Name + " " + mobileNum + " " + " " + email);


            jsonRequests.put("clientOrderId", ServiceUtility.createOrderId());
            jsonRequests.put("brandHash", giftCardCatModel.getCardHash());
            jsonRequests.put("sessionId", session.getUserSessionId());
            jsonRequests.put("productType", demo[0]);
            jsonRequests.put("skuId", demo[1]);

//            jsonRequests.put("denomination", price);
//            jsonRequests.put("productType", "digital");
//            jsonRequests.put("quantity", quantity);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequests = null;
        }

        if (jsonRequests != null) {
            Log.i("CardRequest", jsonRequests.toString());
            Log.i("Registration API", ApiURLNew.URL_GIFT_CARD_ORDER);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_GIFT_CARD_ORDER, jsonRequests, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Registration Response", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        loadDlg.dismiss();
                        if (code != null && code.equals("S00")) {
                            showSuccessDialog("", "Congratulations Order Successful");


                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            if (messageAPI.equalsIgnoreCase("Please login and try again.")) {
                                showInvalidSessionDialog();

                            } else {
                                CustomToast.showMessage(getActivity(), messageAPI);
                            }
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                        Log.i("Type", "jSON");
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("Type", "ERro");
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    loadDlg.dismiss();


                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "12345");
                    return map;
                }

            };
            int socketTimeout = 60000 *5;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public void showSuccessDialog(String message, String Title) {
        CustomSuccessDialog builder = new CustomSuccessDialog(getActivity(), Title, message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent mainActivityIntent = new Intent(getActivity(), MainActivity.class);
                sendRefresh();
                startActivity(mainActivityIntent);
                getActivity().finish();
            }
        });
        builder.show();
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onClick(View v) {

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

//intent
//    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
//        private int space;
//
//        public SpacesItemDecoration(int space) {
//            this.space = space;
//        }
//
//        @Override
//        public void getItemOffsets(Rect outRect, View view,
//                                   RecyclerView parent, RecyclerView.State state) {
//            outRect.left = space;
//            outRect.right = space;
//            outRect.bottom = space;
//            outRect.top = space;
//        }
//    }
public String loadJSONFromAsset(){
        String json;
        try{
        InputStream is=getActivity().getAssets().open("denomination.json");
        int size=is.available();
        byte[]buffer=new byte[size];
        is.read(buffer);
        is.close();
        json=new String(buffer,"UTF-8");
        }catch(IOException ex){
        ex.printStackTrace();
        return null;
        }
        return json;
        }

}
