package com.Blockgration.fragment.userfragment;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.Blockgration.Userwallet.activity.loadmoney.LoadMoneyActivity;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.SearchableListDialog;
import com.Blockgration.custom.SearchableSpinner;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.AccountModel;
import com.Blockgration.model.CountryModel;
import com.Blockgration.model.MobileRechargeCountryModel;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.PlaceApi;
import com.orm.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.NotSerializableException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Ksf on 3/27/2016.
 */
public class RegisterFragment extends Fragment {
    private EditText etRegisterFirstName, etRegisterPassword, etRegisterEmail, etRegisterMobile, etRegisterDob;
    private Button btnRegisterSubmit;
    private String firstName, lastName, newPassword, rePassword, email, mobileNo;

    private String userAddress = null;
    private List<MobileRechargeCountryModel> countryArray;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    private String userCountry = null;
    private String userState = null;
    private String userCity = null;

    private LoadingDialog loadDlg;

    private TextInputLayout ilRegisterFName, ilRegisterMobile, ilRegisterPassword, ilRegisterEmail, ilRegisterDob, ilRegisterAddress;

    private View rootView;
    private JSONObject jsonRequest;

    //Volley Tag
    private String tag_json_obj = "json_obj_req";
    private AutoCompleteTextView autocompleteAddress;
    private List<AccountModel> accountArray;

    //API>=23 Permission
    private static final int PERMISSION_REQUEST_CODE = 1;
    public static final String POINTS = "points";
    private boolean isAddressSelected = false;
    private EditText etCountry;
    private String Country;
    private ImageButton ibtnShowPwd;
    private TextInputLayout ilRegisterCountry;
    private SharedPreferences pointPreferences;
    private String currency_name, country_ISO2, country_ISO3, currency_symbol, currency_ISO3;
    private String country_name, country_code, gender;
    private TextInputLayout ilRegisterMobileCode, ilGender;
    private EditText etRegisterMobileCode, etGender;
    private int extn;
    private String currencyName, countryCode, countryName, currencyCode;
    private boolean valid;
    private final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final Pattern hasLowercase = Pattern.compile("[a-z]");
    private final Pattern hasNumber = Pattern.compile("\\d");
    private final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");


    private SearchableSpinner spCountry;
    private List<MobileRechargeCountryModel> currencyArray;
    private ArrayAdapter countryAdapter;
    private ArrayList<String> countries = new ArrayList<>();
    private SearchableListDialog slDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
//        slDialog = new SearchableListDialog();
//        loadDlg.show();
        loadCountryApi();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_register_new, container, false);
        autocompleteAddress = (AutoCompleteTextView) rootView.findViewById(R.id.autocompleteAddress);
        autocompleteAddress.setAdapter(new PlacesAutoCompleteAdapter(getActivity(), R.layout.custom_place_items));
//        countryArray = AppMetadata.getCountry();
        ibtnShowPwd = (ImageButton) rootView.findViewById(R.id.ibtnShowPwd);
        pointPreferences = getActivity().getSharedPreferences(POINTS, Context.MODE_PRIVATE);
//        currencyArray = Select.from(CountryModel.class).list();

        currencyArray = new ArrayList<>();
        ilRegisterMobileCode = (TextInputLayout) rootView.findViewById(R.id.ilRegisterMobileCode);
        etRegisterMobile = (EditText) rootView.findViewById(R.id.etRegisterMobile);
        etRegisterMobileCode = (EditText) rootView.findViewById(R.id.etRegisterMobileCode);
//        ilRegisterMobileCode.setVisibility(View.GONE);
        etRegisterMobileCode.setFocusable(false);
        etRegisterMobileCode.setClickable(false);
        loadDlg.show();

        accountArray = Select.from(AccountModel.class).list();
//        currentAccountArray = Select.from(CurrentAccountModel.class).list();
        autocompleteAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String description = (String) parent.getItemAtPosition(position);
                userAddress = description + "";
                isAddressSelected = true;
            }
        });
        ibtnShowPwd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    valid = true;
                    etRegisterPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                    etRegisterPassword.setTransformationMethod(new HideReturnsTransformationMethod());
                    etRegisterPassword.setSelection(etRegisterPassword.length());
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    valid = true;
                    etRegisterPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                    etRegisterPassword.setTransformationMethod(new PasswordTransformationMethod());
                    etRegisterPassword.setSelection(etRegisterPassword.length());
                }
                return false;
            }
        });


        autocompleteAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isAddressSelected = false;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        etRegisterFirstName = (EditText) rootView.findViewById(R.id.etRegisterFirstName);
        etRegisterPassword = (EditText) rootView.findViewById(R.id.etRegisterPassword);
        etRegisterEmail = (EditText) rootView.findViewById(R.id.etRegisterEmail);
        etRegisterMobile = (EditText) rootView.findViewById(R.id.etRegisterMobile);
        etRegisterDob = (EditText) rootView.findViewById(R.id.etRegisterDob);
        etCountry = (EditText) rootView.findViewById(R.id.etCountry);
        etGender = (EditText) rootView.findViewById(R.id.etGender);

        spCountry = (SearchableSpinner) rootView.findViewById(R.id.spCountry);

        etRegisterEmail.setText("");
        etRegisterPassword.setText("");
        etRegisterFirstName.setText("");
        etRegisterDob.setText("");
        etCountry.setText("");

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;

        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            checkEmailPermission();
        } else {
            if (getEmail(getActivity()) != null && !getEmail(getActivity()).isEmpty()) {
                etRegisterEmail.setText(getEmail(getActivity()));
            }
        }


        btnRegisterSubmit = (Button) rootView.findViewById(R.id.btnRegisterSubmit);

        ilRegisterFName = (TextInputLayout) rootView.findViewById(R.id.ilRegisterFName);
        ilRegisterMobile = (TextInputLayout) rootView.findViewById(R.id.ilRegisterMobile);
        ilRegisterPassword = (TextInputLayout) rootView.findViewById(R.id.ilRegisterPassword);
        ilRegisterCountry = (TextInputLayout) rootView.findViewById(R.id.ilRegisterCountry);
        ilRegisterEmail = (TextInputLayout) rootView.findViewById(R.id.ilRegisterEmail);
        ilRegisterDob = (TextInputLayout) rootView.findViewById(R.id.ilRegisterDob);
        ilRegisterAddress = (TextInputLayout) rootView.findViewById(R.id.ilRegisterAddress);
        ilGender = (TextInputLayout) rootView.findViewById(R.id.ilGender);


        etRegisterMobile.addTextChangedListener(new MyTextWatcher(etRegisterMobile));
        etRegisterPassword.addTextChangedListener(new MyTextWatcher(etRegisterPassword));
        etRegisterEmail.addTextChangedListener(new MyTextWatcher(etRegisterEmail));
        etRegisterFirstName.addTextChangedListener(new MyTextWatcher(etRegisterFirstName));


        etRegisterDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dateOfBirthDialog();
            }
        });

        etGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final String[] items = {"Male", "Female"};
                android.support.v7.app.AlertDialog.Builder idDialog =
                        new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                idDialog.setTitle("Select your Title");
                idDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                idDialog.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int position) {
                        ((EditText) view).setText(items[position].trim());
                        if (items[position].equals("Male")) {
                            gender = "M";
                        } else {
                            gender = "F";
                        }
                        Log.i("GenderSelected", gender);
                    }

                });

                idDialog.show();
            }
        });

        etCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCountryDialog();
            }
        });

        spCountry.setTitle("Select Country");
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country_name = currencyArray.get(position).getCountryName();
                country_code = currencyArray.get(position).getCountryCode();
                loadCountryDetails(country_code);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnRegisterSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        return rootView;
    }

    private void submitForm() {

        if (!validateEmail()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        if (!validateFName()) {
            return;
        }

        if (!validateDob()) {
            return;
        }

        if (!validateGender()) {
            return;
        }

        if (!validateSpCountry()) {
            return;
        }

        if (!validateMobile()) {
            return;
        }

        loadDlg.show();

        firstName = etRegisterFirstName.getText().toString();
        newPassword = etRegisterPassword.getText().toString();
        email = etRegisterEmail.getText().toString();
        mobileNo = etRegisterMobile.getText().toString();

        promoteRegister();
    }


    private boolean validateFName() {
        if (etRegisterFirstName.getText().toString().trim().isEmpty()) {
            ilRegisterFName.setErrorEnabled(true);
            ilRegisterFName.setError("Enter first name");
            requestFocus(etRegisterFirstName);
            return false;
        } else {
            ilRegisterFName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateGender() {
        if (etGender.getText().toString().trim().isEmpty()) {
            ilGender.setErrorEnabled(true);
            ilGender.setError("Select Gender");
            requestFocus(etGender);
            return false;
        } else {
            ilGender.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateCountry() {
        if (etCountry.getText().toString().trim().isEmpty()) {
            ilRegisterCountry.setError("Enter Country name");
            requestFocus(etCountry);
            return false;
        } else {
            ilRegisterCountry.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateSpCountry() {
        if (spCountry.getSelectedItem() == null) {
            CustomToast.showMessage(getActivity(), "Select Country");
            requestFocus(spCountry);
            return false;
        }
        return true;
    }


    private boolean validateEmail() {
        String email = etRegisterEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            ilRegisterEmail.setError("Enter valid email");
            requestFocus(ilRegisterEmail);
            return false;
        } else {
            ilRegisterEmail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMobile() {
        if (etRegisterMobile.getText().toString().trim().isEmpty() || etRegisterMobile.getText().toString().trim().length() < 8) {
            ilRegisterMobile.setError("Enter valid mob. no.");
            requestFocus(etRegisterMobile);
            return false;
        } else {
            ilRegisterMobile.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateAddress() {
        if (isAddressSelected) {
            if (userAddress != null && !userAddress.isEmpty()) {
                String[] addressArray = userAddress.split(",");

                if (addressArray != null && addressArray.length != 0) {
                    Log.i("Size", addressArray.length + "");
                    if (addressArray.length == 3) {
                        userCity = addressArray[0];
                        userState = addressArray[1];
                        userCountry = addressArray[2];
                        Log.i("City", userCity + "");
                        Log.i("State", userState + "");
                        Log.i("Country", userCountry + "");
                        ilRegisterAddress.setErrorEnabled(false);
                        return true;
                    } else if (addressArray.length == 2) {
                        userState = addressArray[0];
                        userCountry = addressArray[1];
                        userCity = "N/A";
                        Log.i("City", userCity + "");
                        Log.i("State", userState + "");
                        ilRegisterAddress.setErrorEnabled(false);
                        return true;
                    } else {
                        ilRegisterAddress.setError("Enter your full address");
                        requestFocus(autocompleteAddress);
                        return false;
                    }
                } else {
                    ilRegisterAddress.setError("Enter your full address");
                    requestFocus(autocompleteAddress);
                    return false;
                }

            } else {
                ilRegisterAddress.setError("Enter your full address");
                requestFocus(autocompleteAddress);
            }
        } else {
            ilRegisterAddress.setError("Select address from the list");
            requestFocus(autocompleteAddress);
            return false;
        }
        return true;
    }

    private boolean validatePassword() {

        if (etRegisterPassword.getText().toString().isEmpty() || etRegisterPassword.getText().toString().length() < 6) {
            ilRegisterPassword.setErrorEnabled(true);
            ilRegisterPassword.setError("Enter at least 6 digit password");
            requestFocus(etRegisterPassword);
            return false;
        } else {
            ilRegisterPassword.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateDob() {
        if (etRegisterDob.getText().toString().trim().isEmpty()) {
            ilRegisterDob.setError("Enter Date of birth");
            requestFocus(etRegisterDob);
            return false;
        } else {
            ilRegisterDob.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.etRegisterEmail) {
                validateEmail();
            } else if (i == R.id.etRegisterMobile) {
                validateMobile();
            } else if (i == R.id.etRegisterPassword) {
                validatePassword();
                validatePasswordListner();
            } else if (i == R.id.etRegisterFirstName) {
                validateFName();
            }

        }
    }

    private void promoteRegister() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("contactNo", String.valueOf(extn) + etRegisterMobile.getText().toString());
            jsonRequest.put("password", etRegisterPassword.getText().toString());
            jsonRequest.put("confirmPassword", etRegisterPassword.getText().toString());
            jsonRequest.put("email", etRegisterEmail.getText().toString());
            jsonRequest.put("gender", gender);
            jsonRequest.put("dateOfBirth", etRegisterDob.getText().toString());
            jsonRequest.put("firstName", etRegisterFirstName.getText().toString());
            jsonRequest.put("lastName", " ");
            jsonRequest.put("countryCode", country_code);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Registration API", ApiURLNew.URL_REGISTRATION);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_REGISTRATION, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        loadDlg.dismiss();
                        Log.i("Response", response.toString());
                        String messageAPI = response.getString("message");
                        String code = response.getString("code");
                        if (code != null & code.equals("S00")) {
//                            etCountry.setText("");
//                            etRegisterFirstName.setText("");
//                            etRegisterDob.setText("");
//                            etRegisterEmail.setText("");
//                            etRegisterPassword.setText("");
//                            etRegisterMobile.setText("");
//                            etGender.setText("");
//                            etRegisterMobileCode.setText("");
                            showLoginDialog(messageAPI);
                        } else {
                            loadDlg.dismiss();
                            CustomToast.showMessage(getActivity(), messageAPI);
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }


    static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);
        if (account == null) {
            return null;
        } else {
            return account.name;

        }
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }


    public void dateOfBirthDialog() {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String formattedDay, formattedMonth;
                if (dayOfMonth < 10) {
                    formattedDay = "0" + dayOfMonth;
                } else {
                    formattedDay = dayOfMonth + "";
                }

                if ((monthOfYear + 1) < 10) {
                    formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                } else {
                    formattedMonth = String.valueOf(monthOfYear + 1) + "";
                }
                etRegisterDob.setText(String.valueOf(year) + "-"
                        + String.valueOf(formattedMonth) + "-"
                        + String.valueOf(formattedDay));
            }
        }, 1990, 01, 01);

        dialog.getDatePicker().setMaxDate(new Date().getTime());
        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "Done", dialog);
        dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Dismiss", dialog);
        dialog.show();


    }

    public void showLoginDialog(final String msg) {
//        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, msg);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = getActivity().getIntent();
                getActivity().finish();
                startActivity(intent);
            }
        });

        builder.show();
    }

    public String generateMessage() {
        String source = "<b><font color=#000000> Registration Successful.</font></b>" +
                "<br><br><b><font color=#ff0000> Please Login to use Claro. </font></b><br></br>";
        return source;
    }

    class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

        ArrayList<String> resultList;

        Context mContext;
        int mResource;

        PlaceApi mPlaceAPI = new PlaceApi();

        public PlacesAutoCompleteAdapter(Context context, int resource) {
            super(context, resource);

            mContext = context;
            mResource = resource;
        }

        @Override
        public int getCount() {
            // Last item will be the footer
            return resultList.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (position != (resultList.size() - 1))
                view = inflater.inflate(R.layout.custom_place_items, null);
            else
                view = inflater.inflate(R.layout.custom_place_google, null);


            if (position != (resultList.size() - 1)) {
                TextView autocompleteTextView = (TextView) view.findViewById(R.id.autocompleteText);
                autocompleteTextView.setText(resultList.get(position));
            } else {
                ImageView imageView = (ImageView) view.findViewById(R.id.ivPlaceGoogle);
                // not sure what to do <img draggable="false" class="emoji" alt="😀" src="https://s.w.org/images/core/emoji/72x72/1f600.png">
            }

            return view;
        }

        @Override
        public String getItem(int position) {
            return resultList.get(position);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected Filter.FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        resultList = mPlaceAPI.autocomplete(constraint.toString());
                        // Footer
                        resultList.add("footer");
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }

                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };

            return filter;
        }
    }

    private void checkEmailPermission() {
        int hasWriteContactsPermission = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            hasWriteContactsPermission = getActivity().checkSelfPermission(Manifest.permission.GET_ACCOUNTS);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS},
                        PERMISSION_REQUEST_CODE);
                return;
            } else {
                etRegisterEmail.setText(getEmail(getActivity()));
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    etRegisterEmail.setText(getEmail(getActivity()));

                } else {
                    // Permission Denied
                    Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void showCountryDialog() {

        final ArrayList<String> currencyVal = new ArrayList<>();

        for (MobileRechargeCountryModel currencyModel : currencyArray) {
            currencyVal.add(currencyModel.getCountryName());
        }

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select your Country");


        currencyDialog.setItems(currencyVal.toArray(new String[currencyVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etCountry.setText(currencyVal.get(i));
                        Log.i("Country selected", etCountry.getText().toString());
                        for (MobileRechargeCountryModel currencyModel : currencyArray) {
                            if (currencyModel.getCountryName().equals(currencyVal.get(i))) {
                                country_name = currencyArray.get(i).getCountryName();
                                country_code = currencyArray.get(i).getCountryCode();
                                loadDlg.show();
                                loadCountryDetails(country_code);
                                Log.i("Country name", country_name);
                                Log.i("Country code", country_code);
                            }
                        }
                    }
                });

        currencyDialog.show();
    }

    private void loadCountryApi() {
        StringRequest req = new StringRequest(Request.Method.GET, ApiURLNew.URL_REGISTER_COUNTRIES, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("Country URL", ApiURLNew.URL_REGISTER_COUNTRIES);
                Log.i("Country Response", s);
                loadDlg.dismiss();
                try {
                    JSONObject obj = new JSONObject(s);
                    String res = obj.getString("response");
                    JSONObject resp = new JSONObject(res);
                    String code = resp.getString("code");
                    if (!code.equals("null") && code.equals("S00")) {
                        JSONArray details = resp.getJSONArray("details");
                        for (int i = 0; i < details.length(); i++) {
                            JSONObject jo_inside = details.getJSONObject(i);
                            String countryName = jo_inside.getString("name");
                            String countryCode = jo_inside.getString("code");
                            MobileRechargeCountryModel model = new MobileRechargeCountryModel(countryName, countryCode);
                            currencyArray.add(model);
                        }
                        if (currencyArray.size() != 0) {
                            for (int p = 0; p < currencyArray.size(); p++) {
                                Collections.sort(currencyArray, new Comparator<MobileRechargeCountryModel>() {
                                    @Override
                                    public int compare(MobileRechargeCountryModel o1, MobileRechargeCountryModel o2) {
                                        return o1.getCountryName().compareToIgnoreCase(o2.getCountryName());
                                    }
                                });
                                countries.add(currencyArray.get(p).getCountryName());
                            }
                            try {
                                countryAdapter = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, countries);
                                spCountry.setAdapter(countryAdapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
//                    workLoad(3000);
                } catch (JSONException e) {
                    e.printStackTrace();
//                    workLoad(3000);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                loadDlg.dismiss();
                CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(volleyError, getActivity()));
                volleyError.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("hash", "1234");
                return map;
            }
        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        PayQwikApp.getInstance().addToRequestQueue(req);
    }

    private void loadCountryDetails(final String country_code) {
        loadDlg.show();
        Log.i("CountryDetails URL", ApiURLNew.URL_REGISTER_DETAILS + country_code);

        StringRequest req = new StringRequest(Request.Method.GET, ApiURLNew.URL_REGISTER_DETAILS + country_code, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("CountryDetails URL", ApiURLNew.URL_REGISTER_DETAILS + country_code);
                Log.i("CountryDetails Response", s);
                loadDlg.dismiss();
                try {
                    JSONObject obj = new JSONObject(s);
                    String res = obj.getString("response");
                    JSONObject resp = new JSONObject(res);
                    String code = resp.getString("code");
                    if (code != null && code.equals("S00")) {
                        JSONObject details = resp.getJSONObject("details");
                        extn = details.getInt("extn");
                        currencyName = details.getString("currencyName");
                        countryCode = details.getString("countryCode");
                        countryName = details.getString("countryName");
                        currencyCode = details.getString("currencyCode");
                    }
                    Log.i("extn", String.valueOf(extn));
                    Log.i("currencyName", currencyName);
                    Log.i("countryCode", countryCode);
                    Log.i("countryName", countryName);
                    Log.i("currencyCode", currencyCode);
                    etRegisterMobileCode.setText("+" + String.valueOf(extn));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                loadDlg.dismiss();
                CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(volleyError, getActivity()));
                volleyError.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("hash", "1234");
                return map;
            }
        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        PayQwikApp.getInstance().addToRequestQueue(req);
    }


    private boolean validatePasswordListner() {
        if (etRegisterPassword.getText().toString().trim().isEmpty() || etRegisterPassword.getText().toString().trim().length() < 6) {
            ilRegisterPassword.setError(getResources().getString(R.string.password_validity));
            requestFocus(etRegisterPassword);
            return false;
        } else {
            ilRegisterPassword.setErrorEnabled(false);
            checkPwdStr();
        }
        return true;
    }

    private String checkPwdStr() {
        if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            ilRegisterPassword.setError("Strong Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            ilRegisterPassword.setError("Strong Password");
        } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            ilRegisterPassword.setError("Strong Password");
        } else if (hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find() && hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            ilRegisterPassword.setError("Strong Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            ilRegisterPassword.setError("Good Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            ilRegisterPassword.setError("Good Password");
        } else if (hasUppercase.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            ilRegisterPassword.setError("Good Password");
        } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasSpecialChar.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            ilRegisterPassword.setError("Good Password");
        } else if (hasNumber.matcher(etRegisterPassword.getText().toString().trim()).find() && hasLowercase.matcher(etRegisterPassword.getText().toString().trim()).find()) {
            ilRegisterPassword.setError("Good Password");
        } else {
            ilRegisterPassword.setError("Weak Password");
        }
        return null;
    }

    @Override
    public void onPause() {
        super.onPause();
//        slDialog.getDialog().dismiss();
//        new SearchableListDialog().getDialog().dismiss();
        removeSearchableDialog();
    }

    @Override
    public void onStop() {
        super.onStop();
        removeSearchableDialog();
    }

    public void removeSearchableDialog() {
        Fragment searchableSpinnerDialog = getFragmentManager().findFragmentByTag("TAG");
        if (searchableSpinnerDialog != null && searchableSpinnerDialog.isAdded() && searchableSpinnerDialog.isVisible()) {
            getFragmentManager().beginTransaction().remove(searchableSpinnerDialog).commit();
        }
    }
}