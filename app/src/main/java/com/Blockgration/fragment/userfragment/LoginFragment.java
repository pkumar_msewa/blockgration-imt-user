package com.Blockgration.fragment.userfragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.Blockgration.Userwallet.activity.LoginRegActivity;
import com.Blockgration.Userwallet.activity.MainMenuDetailActivity;
import com.Blockgration.Userwallet.activity.VerifyMPinNewActivity;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.ChangePwdActivity;
import com.Blockgration.Userwallet.activity.MainActivity;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.model.AccountModel;
import com.Blockgration.model.CurrentAccountModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.SecurityUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.Blockgration.Userwallet.activity.MainMenuDetailActivity.PROPERTY_REG_ID;


/**
 * Created by Ksf on 3/27/2016.
 */
public class LoginFragment extends Fragment {
    private static final String POINTS = "POINTS";
    private View rootView;
    private EditText etLoginMobile;
    private EditText etLoginPassword;
    private TextInputLayout ilLoginMobile;
    private TextInputLayout ilLoginPassword;
    private Button btnLogin;
    private Button btnForget;
    private LoadingDialog loadDlg;
    private RequestQueue rq;
    SharedPreferences pointPreferences;
    private JSONObject jsonRequest;
    private UserModel session = UserModel.getInstance();
    private String accBalance;
    private String accNo;
    private String balanceLimit;
    private String accType;
    private String dailyLimit;
    private String accCountry;
    private String monthlyLimit;
    private String currencyName;
    private String currencyCode;
    private String currencySymbol;
    private String userEmailStatus = "";
    public static final String PHONE = "phone";
    SharedPreferences phonePreferences;

    //FIREBASE
    public static final String PROPERTY_REG_ID = "registration_id";
    private String regId = "";


    //Volley Tag
    private String tag_json_obj = "json_obj_req";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_login, container, false);
        etLoginMobile = (EditText) rootView.findViewById(R.id.etLoginMobile);
        etLoginMobile.setText(getStoredPhone());
        ArrayList<String> strings = new ArrayList<>();
        rq = Volley.newRequestQueue(getActivity());
        etLoginPassword = (EditText) rootView.findViewById(R.id.etLoginPassword);
        pointPreferences = getActivity().getSharedPreferences(POINTS, Context.MODE_PRIVATE);
        btnLogin = (Button) rootView.findViewById(R.id.btnLogin);
        btnForget = (Button) rootView.findViewById(R.id.btnForget);

        ilLoginPassword = (TextInputLayout) rootView.findViewById(R.id.ilLoginPassword);
        ilLoginMobile = (TextInputLayout) rootView.findViewById(R.id.ilLoginMobile);

//        etLoginMobile.addTextChangedListener(new MyTextWatcher(etLoginMobile));
//        etLoginPassword.addTextChangedListener(new MyTextWatcher(etLoginPassword));
        loadDlg = new LoadingDialog(getActivity());

        btnLogin.setOnClickListener(new View.OnClickListener()

                                    {
                                        @Override
                                        public void onClick(View view) {
                                            submitForm();

                                        }
                                    }

        );


        btnForget.setOnClickListener(new View.OnClickListener()

                                     {
                                         @Override
                                         public void onClick(View view) {
                                             if (!validateMobile()) {
                                                 return;
                                             }
                                             loadDlg.show();
                                             promoteForgetPwd();
                                         }
                                     }

        );


        return rootView;
    }

    private boolean validatePassword() {
        if (etLoginPassword.getText().toString().trim().isEmpty()) {
            ilLoginPassword.setError("Required password");
            requestFocus(etLoginPassword);
            return false;
        } else if (etLoginPassword.getText().toString().trim().length() < 6) {
            ilLoginPassword.setError("Enter 6 digit password");
            requestFocus(etLoginPassword);
            return false;
        } else {
            ilLoginPassword.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMobile() {
//        if (etLoginMobile.getText().toString().trim().isEmpty() || !(etLoginMobile.getText().toString().trim().contains("@") && etLoginMobile.getText().toString().trim().contains("."))) {
        if (etLoginMobile.getText().toString().trim().isEmpty()) {
            ilLoginMobile.setError("Enter valid id");
            requestFocus(etLoginMobile);
            return false;
        } else {
            ilLoginMobile.setErrorEnabled(false);
        }

        return true;
    }


    private void submitForm() {
        if (!validateMobile()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        attemptLoginNew();

    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int i = view.getId();
            if (i == R.id.etLoginMobile) {
//                validateMobile();
            } else if (i == R.id.etLoginPassword) {
//                validatePassword();

            }
            if (etLoginPassword.length() == 6) {
                submitForm();
            }
//            submitForm();
        }
    }

    public void attemptLoginNew() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("username", etLoginMobile.getText().toString());
            jsonRequest.put("password", etLoginPassword.getText().toString());
            regId = getRegistrationId();
            if (!regId.isEmpty()) {
                Log.i("registrationId", regId);
                jsonRequest.put("registrationId", regId);
            }
//            jsonRequest.put("ipAddress", "127.0.0.1");
            if (SecurityUtil.getIMEI(getActivity()) != null) {
                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(getActivity()) + "-" + SecurityUtil.getIMEI(getActivity()));
            } else {
                jsonRequest.put("ipAddress", SecurityUtil.getAndroidId(getActivity()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("Login Request", jsonRequest.toString());
            Log.i("Login URL", ApiURLNew.URL_LOGIN);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_LOGIN, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject res) {
                    try {
                        String userSessionId = null;
                        String resp = null;
                        String code = null;
                        JSONObject respo = null;
                        Log.i("Login Response", res.toString());
//                        Log.i("Login Response", loadJSONFromAsset().toString());
//                        JSONObject res = new JSONObject(loadJSONFromAsset());
                        boolean success = res.getBoolean("success");
//                        String code = res.getString("code");
                        String msg = res.getString("message");
                        if (success) {
                            resp = res.getString("response");
                            respo = new JSONObject(resp);
                            code = respo.getString("code");
                            userSessionId = res.getString("sessionId");
//                        }
                            if (code != null & code.equals("S00")) {
                                //New Implementation
//                            String details = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(details);
                                JSONObject jsonObject = respo.getJSONObject("details");
//                            JSONObject jsonDetail = jsonObject.getJSONObject("details");
//                            String userSessionId = jsonObject.getString("sessionId");
                                //Active Account
//                            JSONObject jsonCurrentAcc = jsonObject.getJSONObject("activeAccount");
                                JSONObject jsonCurrentAcc = jsonObject.getJSONObject("activeAccount");
                                CurrentAccountModel.deleteAll(CurrentAccountModel.class);

//                            JSONObject jsonCurrentCountry = jsonObject.getJSONObject("country");

                                String currentAccNo = jsonCurrentAcc.getString("accountNumber");
                                String currentAccBalance = jsonCurrentAcc.getString("balance");

                                JSONObject jCurrentType = jsonCurrentAcc.getJSONObject("type");
                                String currentBalanceLimit = jCurrentType.getString("balanceLimit");
                                String currentAccType = jCurrentType.getString("code");
                                String currentDailyLimit = jCurrentType.getString("dailyLimit");
                                String currentMonthlyLimit = jCurrentType.getString("monthlyLimit");

//                String currentMonthlyLimit ;
//                                String currentAccCountry = null;
//                                String currentCurrencyName = null;
//                                String currentCurrencyCode = null;
//                                String currentCurrencySymbol = null;

//                                JSONArray accDetail = jsonObject.getJSONArray("accountDetail");
//                                for (int i = 0; i < accDetail.length(); i++) {
//                                    JSONObject arrObj = accDetail.getJSONObject(i);
//                                    JSONObject jCurrentCountry = arrObj.getJSONObject("country");
//                                    currentAccCountry = jCurrentCountry.getString("countryName");
//                                    currentCurrencyName = jCurrentCountry.getString("currencyName");
//                                    currentCurrencyCode = jCurrentCountry.getString("currencyCode");
//                                    currentCurrencySymbol = jCurrentCountry.getString("countryCode");
//                                }

                                JSONObject jCurrentCountry = jsonCurrentAcc.getJSONObject("country");
                                String currentAccCountry = jCurrentCountry.getString("countryName");
//                            JSONObject jCurrentCurrency = jsonCurrentAcc.getJSONObject("countrycurrencyCode");
//                            JSONObject jCurrentCurrency = jsonCurrentAcc.getJSONObject("currency");
                                String currentCurrencyName = jCurrentCountry.getString("currencyName");
                                String currentCurrencyCode = jCurrentCountry.getString("currencyCode");
//                            String currentCurrencyCode = jCurrentCountry.getString("currencyISO3");
//                            String currentCurrencyCode = jCurrentCurrency.getString("currencyISO3");
                                String currentCurrencySymbol = jCurrentCountry.getString("countryCode");

                                CurrentAccountModel currentAccountModel = new CurrentAccountModel(currentAccBalance, currentAccNo, currentBalanceLimit, currentAccType, currentDailyLimit, currentAccCountry
                                        , currentMonthlyLimit, currentCurrencyName, currentCurrencyCode, currentCurrencySymbol);
                                currentAccountModel.save();


                                AccountModel.deleteAll(AccountModel.class);
                                Object o = jsonObject.get("accountDetail");
//                            if (o instanceof JSONObject) {
//
//
//                                accNo = ((JSONObject) o).getString("accountNo");
//                                accBalance = ((JSONObject) o).getString("balance");
//
//                                JSONObject jType = ((JSONObject) o).getJSONObject("type");
//                                balanceLimit = jType.getString("balanceLimit");
//                                accType = jType.getString("code");
//                                dailyLimit = jType.getString("dailyLimit");
//                                monthlyLimit = jType.getString("monthlyLimit");
//
//                                JSONObject jCountry = ((JSONObject) o).getJSONObject("country");
//                                accCountry = jCountry.getString("countryName");
//
//                                JSONObject jCurrency = ((JSONObject) o).getJSONObject("currency");
//                                currencyName = jCurrency.getString("currencyName");
//                                currencyCode = jCurrency.getString("currencyISO3");
//                                currencySymbol = jCurrency.getString("currencySymbol");
//
//
//                                AccountModel accountModel = new AccountModel(accBalance, accNo, balanceLimit, accType, dailyLimit, accCountry
//                                        , monthlyLimit, currencyName, currencyCode, currencySymbol);
//                                accountModel.save();
//                            }
                                if (o instanceof JSONObject) {


                                    accNo = ((JSONObject) o).getString("accountNumber");
                                    accBalance = ((JSONObject) o).getString("balance");

                                    JSONObject jType = ((JSONObject) o).getJSONObject("type");
                                    balanceLimit = jType.getString("balanceLimit");
                                    accType = jType.getString("code");
                                    dailyLimit = jType.getString("dailyLimit");
                                    monthlyLimit = jType.getString("monthlyLimit");

                                    JSONObject jCountry = ((JSONObject) o).getJSONObject("country");
                                    accCountry = jCountry.getString("countryName");

                                    JSONObject jCurrency = ((JSONObject) o).getJSONObject("currency");
                                    currencyName = jCurrency.getString("currencyName");
                                    currencyCode = jCurrency.getString("currencyISO3");
                                    currencySymbol = jCurrency.getString("currencySymbol");


                                    AccountModel accountModel = new AccountModel(accBalance, accNo, balanceLimit, accType, dailyLimit, accCountry
                                            , monthlyLimit, currencyName, currencyCode, currencySymbol);
                                    accountModel.save();
                                } else if (o instanceof JSONArray) {
                                    JSONArray accArray = jsonObject.getJSONArray("accountDetail");

                                    for (int i = 0; i < accArray.length(); i++) {
                                        JSONObject c = accArray.getJSONObject(i);

                                        accNo = c.getString("accountNumber");
                                        String bal = c.getString("balance");

                                        double newBal = Double.parseDouble(bal);
                                        DecimalFormat decimalFormat = new DecimalFormat("#.##");
                                        String accBalance = decimalFormat.format(newBal);

                                        Log.i("BALANCE", accBalance);

                                        JSONObject jType = c.getJSONObject("type");
                                        balanceLimit = jType.getString("balanceLimit");
                                        accType = jType.getString("code");
                                        dailyLimit = jType.getString("dailyLimit");
                                        monthlyLimit = jType.getString("monthlyLimit");

                                        JSONObject jCountry = c.getJSONObject("country");
                                        accCountry = jCountry.getString("countryName");

//                        JSONObject jCurrency = c.getJSONObject("currency");
//                        currencyName = jCurrency.getString("currencyName");
//                        currencyCode = jCurrency.getString("currencyISO3");
//                        currencySymbol = jCurrency.getString("currencySymbol");

                                        currencyName = jCountry.getString("currencyName");
                                        currencyCode = jCountry.getString("countryCode");
                                        currencySymbol = jCountry.getString("currencyCode");


                                        AccountModel accountModel = new AccountModel(accBalance, accNo, balanceLimit, accType, dailyLimit, accCountry
                                                , monthlyLimit, currencyName, currencyCode, currencySymbol);
                                        accountModel.save();
                                    }
                                }
                                JSONObject jsonUserDetail = jsonObject.getJSONObject("userDetail");
                                String userFName = jsonUserDetail.getString("firstName");
                                String userLName = jsonUserDetail.getString("lastName");
                                String userMobile = jsonUserDetail.getString("contactNo");
                                String userEmail = jsonUserDetail.getString("email");
                                String userAddress = res.getString("address");
//                                String userImage = res.getString("image");
//                String userAddress = jsonUserDetail.getString("address");
                                String userImage = jsonUserDetail.getString("image");
                                userEmailStatus = jsonUserDetail.getString("emailStatus");

                                boolean isMPin = jsonUserDetail.getBoolean("pin");
                                // To be changed after MPin is implemented

//                                boolean isMPin = false;
                                String userDob = jsonUserDetail.getString("dateOfBirth");
                                String userGender = jsonUserDetail.getString("gender");

                                UserModel.deleteAll(UserModel.class);
                                UserModel userModel = new UserModel(userSessionId, userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAddress, userImage, userDob, userGender, isMPin);
                                userModel.save();

                                session.setUserSessionId(userSessionId);
                                session.setUserFirstName(userFName);
                                session.setUserLastName(userLName);
                                session.setUserMobileNo(userMobile);
                                session.setUserEmail(userEmail);
                                session.setEmailIsActive(userEmailStatus);
                                session.setUserImage(userImage);
                                session.setUserAddress(userAddress);
                                session.setUserGender(userGender);
                                session.setUserDob(userDob);
                                session.setMPin(isMPin);
                                session.setIsValid(1);
                                loadDlg.dismiss();
//                            etLoginMobile.setText("");
//                            etLoginPassword.setText("");

                                SharedPreferences.Editor editor = phonePreferences.edit();
                                editor.clear();
                                editor.putString("phone", userEmail);
                                editor.apply();
                                startActivity(new Intent(getActivity()
                                        , MainActivity.class));

                                startActivity(new Intent(getActivity(), MainActivity.class));
                                getActivity().finish();

//                                if (isMPin) {
//                                    startActivity(new Intent(getActivity(), VerifyMPinNewActivity.class));
//                                    getActivity().finish();
//                                    loadDlg.dismiss();
//                                } else {
//                                    startActivity(new Intent(getActivity(), MainActivity.class).putExtra("IntentType", "Login"));
//                                    getActivity().finish();
//                                    loadDlg.dismiss();
//                                }


                            } else if (code != null & code.equals("F05")) {
                                CustomToast.showMessage(getActivity(), msg);
                                loadDlg.dismiss();
                            } else {
                                CustomToast.showMessage(getActivity(), msg);
                                loadDlg.dismiss();

                            }
                        } else {
                            CustomToast.showMessage(getActivity(), msg);
                            loadDlg.dismiss();

                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        Toast.makeText(getActivity(), getResources().getString(R.string.jsonException), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    loadDlg.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    Toast.makeText(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123");
                    return map;
                }

            };

            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
//             Adding request to request queue
            rq.add(postReq);
        }
    }


    private void promoteForgetPwd() {
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("username", etLoginMobile.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_FORGET_PWD, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loadDlg.dismiss();
                    try {
                        String message = response.getString("message");
                        String code = response.getString("code");
                        if (code != null & code.equals("S00")) {
                            showDialog(message);
//                            Intent i = new Intent(getActivity(), ChangePwdActivity.class);
//                            i.putExtra(ChangePwdActivity.EXTRA_EMAIL, etLoginMobile.getText().toString());
//                            i.putExtra(ChangePwdActivity.EXTRA_MSG, message);
//                            startActivity(i);
                            etLoginMobile.setText("");
                            etLoginMobile.requestFocus();
                        } else if (code != null & code.equals("F00")) {
                            showDialog(message);
                            etLoginMobile.setText("");
                            etLoginMobile.requestFocus();
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    Toast.makeText(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "123");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }
    }

    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getActivity().getAssets().open("login.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void showDialog(String msg) {
        final CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, msg);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences() {
        return getActivity().getSharedPreferences("FIREBASE",
                Context.MODE_PRIVATE);
    }

    private String getStoredPhone() {
        phonePreferences = getActivity().getSharedPreferences(PHONE, Context.MODE_PRIVATE);
        String phoneNo = phonePreferences.getString("phone", "");
        if (phoneNo != null && !phoneNo.isEmpty()) {
            return phoneNo;
        } else {
            return "";
        }
    }

}
