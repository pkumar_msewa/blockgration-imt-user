package com.Blockgration.fragment;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;


/**
 * Created by Ksf on 7/27/2016.
 */
public class CustomDisclaimerDialog extends AlertDialog.Builder {

    public CustomDisclaimerDialog(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(com.Blockgration.Userwallet.R.layout.dialog_custom_webview, null, false);


        WebView wvDisclaimer = (WebView) viewDialog.findViewById(com.Blockgration.Userwallet.R.id.wvDisclaimer);
        wvDisclaimer.setVerticalScrollBarEnabled(false);
        wvDisclaimer.loadData(context.getString(com.Blockgration.Userwallet.R.string.dis), "text/html; charset=utf-8", "utf-8");

        this.setCancelable(false);

        this.setView(viewDialog);


    }
}

