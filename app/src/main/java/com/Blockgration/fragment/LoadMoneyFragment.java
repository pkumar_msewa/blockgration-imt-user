//package com.msewa.fragment;
//
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.content.LocalBroadcastManager;
//import android.support.v7.app.AlertDialog;
//import android.text.Html;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.inputmethod.EditorInfo;
//import android.widget.Button;
//import android.widget.RadioButton;
//import android.widget.TextView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.ebs.android.sdk.Config.Encryption;
//import com.ebs.android.sdk.Config.Mode;
//import com.ebs.android.sdk.EBSPayment;
//import com.ebs.android.sdk.PaymentRequest;
//import CustomAlertDialog;
//import LoadingDialog;
//import com.msewa.faspay.R;
//import AppMetadata;
//import UserModel;
//import CheckLog;
//import PayingDetailsValidation;
//import com.rengwuxian.materialedittext.MaterialEditText;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//
//
///**
// * Created by Ksf on 4/11/2016.
// */
//public class LoadMoneyFragment extends Fragment {
//
//    private View rootView;
//    private MaterialEditText etLoadMoneyAmount;
//    private Button btnLoadMoney;
//
//    private RadioButton rbLoadMoneyVBank, rbLoadMoneyOther;
//    private View focusView = null;
//    private boolean cancel;
//
//    private String amount = null;
//
//    AlertDialog.Builder payDialog;
//
//    private UserModel session = UserModel.getInstance();
//    private LoadingDialog loadDlg;
//    private RequestQueue rq;
//    private boolean isVBank = true;
//    private String inValidMessage = "";
//    private String tag_json_obj = "load_money";
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        payDialog = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
//        loadDlg = new LoadingDialog(getActivity());
//        rq = Volley.newRequestQueue(getActivity());
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.fragment_ask_amount_to_load_money, container, false);
//        btnLoadMoney = (Button) rootView.findViewById(R.id.btnLoadMoney);
//        etLoadMoneyAmount = (MaterialEditText) rootView.findViewById(R.id.etLoadMoneyAmount);
//        rbLoadMoneyVBank = (RadioButton) rootView.findViewById(R.id.rbLoadMoneyVBank);
//        rbLoadMoneyOther = (RadioButton) rootView.findViewById(R.id.rbLoadMoneyOther);
//
//        btnLoadMoney.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (rbLoadMoneyVBank.isChecked()) {
//                    isVBank = true;
//                } else if (rbLoadMoneyOther.isChecked()) {
//                    isVBank = false;
//                } else {
//                    isVBank = true;
//                }
//                attemptLoad();
//
//            }
//        });
//
//
//        //DONE CLICK ON VIRTUAL KEYPAD
//        etLoadMoneyAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    if (rbLoadMoneyVBank.isChecked()) {
//                        isVBank = true;
//                    } else if (rbLoadMoneyOther.isChecked()) {
//                        isVBank = false;
//                    } else {
//                        isVBank = true;
//                    }
//                    attemptLoad();
//                }
//                return false;
//            }
//        });
//
//        return rootView;
//    }
//
//    private void attemptLoad() {
//        etLoadMoneyAmount.setError(null);
//        cancel = false;
//        amount = etLoadMoneyAmount.getText().toString();
//        checkPayAmount(amount);
////        checkUserType();
//
//        if (cancel) {
//            focusView.requestFocus();
//        } else {
////            if (isVBank) {
////                verifyTransaction(amount);
////            } else {
//                showCustomDisclaimerDialog();
////            }
//        }
//    }
//
//    public void showCustomDisclaimerDialog() {
//        CustomDisclaimerDialog builder = new CustomDisclaimerDialog(getActivity());
//        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                initialLoadMoneyEBS(amount);
//            }
//        });
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//        builder.show();
//    }
//
//    public void showNonKYCDialog() {
//        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(generateKYCMessage()));
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//        builder.show();
//    }
//
//
//    private void checkPayAmount(String amount) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        if (!gasCheckLog.isValid) {
//            etLoadMoneyAmount.setError(getString(gasCheckLog.msg));
//            focusView = etLoadMoneyAmount;
//            cancel = true;
//        } else if (Integer.valueOf(amount) < 10) {
//            etLoadMoneyAmount.setError("Amount cant be less than 10");
//            focusView = etLoadMoneyAmount;
//            cancel = true;
//        } else if (Integer.valueOf(amount) >= 10000) {
//            etLoadMoneyAmount.setError("Enter amount less than 10000");
//            focusView = etLoadMoneyAmount;
//            cancel = true;
//        }
//    }
//
//    private boolean checkUserType() {
//        if (amount != null && !amount.isEmpty()) {
//            if (Integer.valueOf(amount) > 10000) {
//                focusView = etLoadMoneyAmount;
//                cancel = true;
//                if (session.getUserAcName().equals("Non-KYC")) {
//                    showNonKYCDialog();
//                    return true;
//                } else {
//                    return false;
//                }
//            } else {
//                return false;
//            }
//        } else {
//            return false;
//        }
//
//    }
//
//    public String generateMessage() {
//        String source = "<b><font color=#000000> Amount to load: </font></b>" + "<font color=#000000>" + amount + "</font><br>" +
//                "<br><b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
//        return source;
//    }
//
//
//    public String generateKYCMessage() {
//        return "<b><font color=#000000> Amount to load: </font></b>" + "<font color=#000000>" + amount + "</font><br>" +
//                "<br><b><font color=#ff0000> Sorry you cannot load more than 10,000 at a time.</font></b><br>" +
//                "<br><b><font color=#ff0000> Please enter 10,000 or lesser amount to continue.</font></b><br>";
//    }
//
////    private void verifyTransaction(final String amount) {
////        loadDlg.show();
////        StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_VALIDATE_TRANSACTION, new Response.Listener<String>() {
////            @Override
////            public void onResponse(String responseString) {
////                try {
////                    Log.i("url Verify", ApiUrl.URL_VALIDATE_TRANSACTION);
////
////                    Log.i("JsonResponse Verify", responseString.toString());
////                    JSONObject response = new JSONObject(responseString);
////
////                    String code = response.getString("code");
////                    if (code != null && code.equals("S00")) {
////                        loadDlg.dismiss();
////                        String details = response.getString("details");
////                        JSONObject jsonDetails = new JSONObject(details);
////                        boolean isValid = jsonDetails.getBoolean("valid");
////                        if (isValid) {
////                            //If Want to implement webview instead of sdk just remove if else.
////                            if (isVBank) {
////                                Intent loadMoneyIntent = new Intent(getActivity(), LoadMoneyActivity.class);
////                                loadMoneyIntent.putExtra("amountToLoad", amount);
////                                loadMoneyIntent.putExtra("isVBank", isVBank);
////                                startActivity(loadMoneyIntent);
////                                getActivity().finish();
////                            } else {
////                                //Initiate and call EBS KIT
////                                loadDlg.show();
////                                initialLoadMoneyEBS(amount);
////                            }
////                        } else {
////                            inValidMessage = jsonDetails.getString("message");
////                            showInvalidTranDialog();
////                        }
////                    } else if (code != null && code.equals("F03")) {
////                        loadDlg.dismiss();
////                        showInvalidSessionDialog();
////                    } else {
////                        loadDlg.dismiss();
////                        CustomToast.showMessage(getActivity(), "Unexpected error code");
////                    }
////
////                } catch (JSONException e) {
////                    loadDlg.dismiss();
////                    e.printStackTrace();
////                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
////
////                }
////            }
////        }, new Response.ErrorListener() {
////            @Override
////            public void onErrorResponse(VolleyError error) {
////                loadDlg.dismiss();
////                error.printStackTrace();
////                CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
////
////            }
////        }) {
////            @Override
////            protected Map<String, String> getParams() {
////                Map<String, String> params = new HashMap<>();
////                params.put("sessionId", session.getUserSessionId());
////                params.put("amount", amount);
////                return params;
////            }
////
////        };
////        int socketTimeout = 60000;
////        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
////        postReq.setRetryPolicy(policy);
////        rq.add(postReq);
////
////    }
//
//    public void showInvalidSessionDialog() {
//        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                sendLogout();
//            }
//        });
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//
//            }
//        });
//        builder.show();
//    }
//
//    private void sendLogout() {
//        Intent intent = new Intent("setting-change");
//        intent.putExtra("updates", "4");
//        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
//    }
//
//    private void showInvalidTranDialog() {
//        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, inValidMessage);
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//        builder.show();
//    }
//
//    public void initialLoadMoneyEBS(final String amount) {
//        Log.i("INITIATE URL", ApiUrl.URL_INITIATE_LOAD_MONEY);
//        StringRequest postReq = new StringRequest(Request.Method.POST, ApiUrl.URL_INITIATE_LOAD_MONEY, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.i("Initiate Response", response.toString());
//                try {
//                    JSONObject resObj = new JSONObject(response);
//                    boolean successMsg = resObj.getBoolean("success");
//                    if (successMsg) {
//                        String referenceNo = resObj.getString("referenceNo");
//                        String currency = resObj.getString("currency");
//                        String description = resObj.getString("description");
//                        String name = resObj.getString("name");
//                        String email = resObj.getString("email");
//                        String address = resObj.getString("address");
//                        String cityName = resObj.getString("city");
//                        String stateName = resObj.getString("state");
//                        String countryName = resObj.getString("country");
//                        String postalCode = resObj.getString("postalCode");
//                        String shipName = resObj.getString("shipName");
//                        String shipAddress = resObj.getString("shipAddress");
//                        String shipCity = resObj.getString("shipCity");
//                        String shipState = resObj.getString("shipState");
//                        String shipCountry = resObj.getString("shipCountry");
//                        String shipPostalCode = resObj.getString("shipPostalCode");
//                        String shipPhone = resObj.getString("shipPhone");
//
//                        int ACC_ID = 20696;
//                        String SECRET_KEY = "6496e4db9ebf824ffe2269afee259447";
//                        String HOST_NAME = getResources().getString(R.string.hostname);
//
//                        PaymentRequest.getInstance().setTransactionAmount(amount);
//                        PaymentRequest.getInstance().setAccountId(ACC_ID);
//                        PaymentRequest.getInstance().setSecureKey(SECRET_KEY);
//
//                        PaymentRequest.getInstance().setReferenceNo(referenceNo);
//                        PaymentRequest.getInstance().setBillingEmail(email);
//                        PaymentRequest.getInstance().setFailureid("1");
//                        PaymentRequest.getInstance().setCurrency(currency);
//                        PaymentRequest.getInstance().setTransactionDescription(description);
//                        PaymentRequest.getInstance().setBillingName(name);
//                        PaymentRequest.getInstance().setBillingAddress(address);
//                        PaymentRequest.getInstance().setBillingCity(cityName);
//                        PaymentRequest.getInstance().setBillingPostalCode(postalCode);
//                        PaymentRequest.getInstance().setBillingState(stateName);
//                        PaymentRequest.getInstance().setBillingCountry(countryName);
//                        PaymentRequest.getInstance().setBillingPhone(session.getUserMobileNo());
//                        PaymentRequest.getInstance().setShippingName(shipName);
//                        PaymentRequest.getInstance().setShippingAddress(shipAddress);
//                        PaymentRequest.getInstance().setShippingCity(shipCity);
//                        PaymentRequest.getInstance().setShippingPostalCode(shipPostalCode);
//                        PaymentRequest.getInstance().setShippingState(shipState);
//                        PaymentRequest.getInstance().setShippingCountry(shipCountry);
//                        PaymentRequest.getInstance().setShippingEmail(email);
//                        PaymentRequest.getInstance().setShippingPhone(shipPhone);
//                        PaymentRequest.getInstance().setLogEnabled("1");
//
//                        PaymentRequest.getInstance().setHidePaymentOption(true);
//                        PaymentRequest.getInstance().setHideCashCardOption(true);
//                        PaymentRequest.getInstance().setHideCreditCardOption(true);
//                        PaymentRequest.getInstance().setHideDebitCardOption(false);
//                        PaymentRequest.getInstance().setHideNetBankingOption(false);
//                        PaymentRequest.getInstance().setHideStoredCardOption(true);
//
//                        ArrayList<HashMap<String, String>> custom_post_parameters = new ArrayList<>();
//
//                        HashMap<String, String> hashpostvalues = new HashMap<>();
//                        hashpostvalues.put("account_details", "saving");
//                        hashpostvalues.put("merchant_type", "vpayqwik");
//                        custom_post_parameters.add(hashpostvalues);
//
//                        PaymentRequest.getInstance().setCustomPostValues(custom_post_parameters);
//                        loadDlg.dismiss();
//                        EBSPayment.getInstance().init(getActivity(), ACC_ID, SECRET_KEY, Mode.ENV_LIVE, Encryption.ALGORITHM_MD5, HOST_NAME);
//
//                    } else {
//                        CustomToast.showMessage(getActivity(), "Unable to perform transaction. Please try again");
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
//
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//                params.put("sessionId", session.getUserSessionId());
//                params.put("amount", amount);
//                params.put("name", session.getUserFirstName());
//                params.put("email", session.getUserEmail());
//                params.put("phone", session.getUserMobileNo());
//
//                Log.d("LoadMoneyParam", "LoadParams: "+params);
//                return params;
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> map = new HashMap<>();
//                map.put("hash", "123");
//                return map;
//            }
//        };
//
//        int socketTimeout = 60000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        postReq.setRetryPolicy(policy);
//        PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//    }
//}
