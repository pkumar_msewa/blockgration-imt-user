package com.Blockgration.fragment.mainmenufragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.CheckLog;
import com.Blockgration.util.EncryptDecryptUserUtility;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.PayingDetailsValidation;
import com.Blockgration.util.SecurityUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Kashif-PC on 12/7/2016.
 */
public class FundTransferQrFragment extends Fragment {

    private View rootView;
    private Button btnQRCodeScan, btnQRPay;
    private TextView tvPleaseScan, tvScanTitle;
    private TableLayout tlQRScannedResult;
    private ImageView ivQRCode;
    private EditText etQrScanAmount;

    private UserModel session = UserModel.getInstance();
    private View focusView = null;
    private boolean cancel;

    private JSONObject jsonRequest;

    private LoadingDialog loadDlg;
    private RequestQueue rq;
    //Volley Tag
    private String tag_json_obj = "json_merchant_pay";

    private String receiverNo = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(com.Blockgration.Userwallet.R.layout.fragment_scan_to_pay, container, false);
        rq = Volley.newRequestQueue(getActivity());
        btnQRCodeScan = (Button) rootView.findViewById(com.Blockgration.Userwallet.R.id.btnQRCodeScan);
        btnQRPay = (Button) rootView.findViewById(com.Blockgration.Userwallet.R.id.btnQRPay);
        tvScanTitle = (TextView) rootView.findViewById(com.Blockgration.Userwallet.R.id.tvScanTitle);
        tvScanTitle.setText("FOR SEND MONEY TO YOUR FRIEND\nSCAN QR CODE");
        etQrScanAmount = (EditText) rootView.findViewById(com.Blockgration.Userwallet.R.id.etQrScanAmount);
        tvPleaseScan = (TextView) rootView.findViewById(com.Blockgration.Userwallet.R.id.tvPleaseScan);
        tvPleaseScan.setText("Please scan QR code of Friend.");
        tlQRScannedResult = (TableLayout) rootView.findViewById(com.Blockgration.Userwallet.R.id.tlQRScannedResult);
        ivQRCode = (ImageView) rootView.findViewById(com.Blockgration.Userwallet.R.id.ivQRCode);

        btnQRCodeScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator.forSupportFragment(FundTransferQrFragment.this)
                        .setOrientationLocked(false)
                        .setBeepEnabled(true)
                        .addExtra("PROMPT_MESSAGE", "Scan QR Code")
                        .initiateScan(IntentIntegrator.QR_CODE_TYPES);
            }
        });

        btnQRPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPayment();
            }
        });

        return rootView;
    }

    private void attemptPayment() {
        gethideKeyboard();
        etQrScanAmount.setError(null);
        cancel = false;
        checkPayAmount(etQrScanAmount.getText().toString());
        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteSendMoney();
        }
    }

    public void gethideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etQrScanAmount.setError(getString(gasCheckLog.msg));
            focusView = etQrScanAmount;
            cancel = true;
        } else if (Integer.valueOf(etQrScanAmount.getText().toString()) < 10) {
            etQrScanAmount.setError(getString(com.Blockgration.Userwallet.R.string.lessAmount));
            focusView = etQrScanAmount;
            cancel = true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {
                CustomToast.showMessage(getActivity(), "Cancelled");
            } else {
                try {
                    String[] splitResult = result.toString().split(":");
                    String encryptValue = splitResult[2].trim();
                    String finalResult = "";

                    encryptValue = encryptValue.replaceAll("\t", "");
                    encryptValue = encryptValue.replaceAll("\\n", "");
                    encryptValue = encryptValue.replaceAll("\r", "");
                    encryptValue = encryptValue.replace("Raw bytes", "");
                    Log.i("EncryptValue", encryptValue);
                    try {
                        finalResult = EncryptDecryptUserUtility.decrypt(encryptValue);
                        addingResultToTableView(finalResult);
                    } catch (Exception e) {
                        CustomToast.showMessage(getActivity(), "Invalid QR");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            CustomToast.showMessage(getActivity(), "Result is null");
        }
    }

    private void addingResultToTableView(String result) {
        tlQRScannedResult.removeAllViews();

        String[] resultArray = result.split("-");
        Log.i("resultSize", resultArray.length + "");

        Log.i("resultArray", resultArray[0]);
        Log.i("resultArray", resultArray[1]);


        ArrayList<String> arrayListHead = new ArrayList<>();
        arrayListHead.add("Receiver Name");
        arrayListHead.add("Receiver Mobile");

        ArrayList<String> arrayListResult = new ArrayList<>();
        arrayListResult.add(resultArray[0]);
        arrayListResult.add(resultArray[1]);
        receiverNo = resultArray[1];

        for (int i = 0; i < arrayListHead.size(); i++) {
            TableRow row = new TableRow(getActivity());

            TextView textH = new TextView(getActivity());
            TextView textC = new TextView(getActivity());
            TextView textV = new TextView(getActivity());


            textH.setText(arrayListHead.get(i));
            textC.setText(":  ");
            textV.setText(arrayListResult.get(i));


            row.addView(textH);
            row.addView(textC);
            row.addView(textV);


            tlQRScannedResult.addView(row);
        }
        tlQRScannedResult.setVisibility(View.VISIBLE);
        ivQRCode.setVisibility(View.GONE);
        btnQRCodeScan.setText("Re-Scan");
        tvPleaseScan.setText("Are you sure you want to proceed to pay?");
        tvScanTitle.setText("Scanned successfully");
        etQrScanAmount.setVisibility(View.VISIBLE);
        btnQRPay.setVisibility(View.VISIBLE);
    }


    public void promoteSendMoney() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("mobileNumber", receiverNo);
            jsonRequest.put("amount", etQrScanAmount.getText().toString());
            jsonRequest.put("message", " ");
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_PAY_AT_STORE, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Send Money", response.toString());
                    try {
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            String jsonString = response.getString("response");
                            JSONObject jsonObject = new JSONObject(jsonString);
                            String successMessage = jsonObject.getString("message");
                            CustomToast.showMessage(getActivity(), successMessage);
                            etQrScanAmount.getText().clear();
                            loadDlg.dismiss();
                            sendRefresh();

                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
                            if (response.has("message") && response.getString("message") != null) {
                                String message = response.getString("message");
                                CustomToast.showMessage(getActivity(), message);
                            }
                        }
                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getResources().getString(com.Blockgration.Userwallet.R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();

                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", SecurityUtil.getSecurityKey(jsonRequest.toString()));
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            rq.add(postReq);
        }

    }


    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        Spanned result;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            result = Html.fromHtml(AppMetadata.getInvalidSession(), Html.FROM_HTML_MODE_LEGACY);
//        } else {
            result = Html.fromHtml(AppMetadata.getInvalidSession());
//        }
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), com.Blockgration.Userwallet.R.string.dialog_title2, result);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

}
