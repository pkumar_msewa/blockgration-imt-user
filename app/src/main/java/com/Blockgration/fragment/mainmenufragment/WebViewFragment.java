package com.Blockgration.fragment.mainmenufragment;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.LoginRegActivity;
import com.Blockgration.Userwallet.activity.MainActivity;
import com.Blockgration.Userwallet.activity.MainMenuDetailActivity;
import com.Blockgration.Userwallet.activity.SplashActivity;
import com.Blockgration.Userwallet.activity.loadmoney.LoadMoneyActivity;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.util.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Ksf on 3/27/2016.
 */
public class WebViewFragment extends Fragment {
    private View rootView;
    private WebView webview;
    private String url = "";
    private String trazability = "";
    private ProgressBar pbWebView;
    private RequestQueue requestQueue;
    private ConnectionDetector connectionDetector;
    Boolean isInternetPresent = false;
    Boolean isResumed = false;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        workLoad(3000);
        url = getArguments().getString("URL");
        trazability = getArguments().getString("TrazCode");
        requestQueue = Volley.newRequestQueue(getActivity());
        Log.i("webViewURL", url);
        Log.i("trazCode", trazability);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(com.Blockgration.Userwallet.R.layout.fragment_web_view, container, false);
        webview = (WebView) rootView.findViewById(com.Blockgration.Userwallet.R.id.wvMain);
        pbWebView = (ProgressBar) rootView.findViewById(com.Blockgration.Userwallet.R.id.pbWebView);
        webview.setVisibility(View.VISIBLE);
        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        webview.clearCache(true);
        webview.clearHistory();
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
        webview.getSettings().setDomStorageEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        webview.setWebViewClient(new SSLTolerentWebViewClient());
//        webview.setWebViewClient(new WebViewClient() {
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//                return true;
//            }
//
//            public void onPageFinished(WebView view, String url) {
//                Log.i("url",url);
//                webview.setVisibility(View.VISIBLE);
//                pbWebView.setVisibility(View.GONE);
//            }
//
//            @SuppressWarnings("deprecation")
//            @Override
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Log.i("url",url);
//                view.loadData(description, "text/html", "UTF-8");
//            }
//
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                super.onPageStarted(view, url, favicon);
//
//            }
//
//            @TargetApi(android.os.Build.VERSION_CODES.M)
//            @Override
//            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
//                Log.i("url",url);
//                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
//            }
//        });
        webview.loadUrl(url);
        return rootView;
    }

    private class SSLTolerentWebViewClient extends WebViewClient {
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            handler.proceed();
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//            AlertDialog alertDialog = builder.create();
//            String message = "SSL Certificate error.";
//            switch (error.getPrimaryError()) {
//                case SslError.SSL_UNTRUSTED:
//                    message = "The certificate authority is not trusted.";
//                    break;
//                case SslError.SSL_EXPIRED:
//                    message = "The certificate has expired.";
//                    break;
//                case SslError.SSL_IDMISMATCH:
//                    message = "The certificate Hostname mismatch.";
//                    break;
//                case SslError.SSL_NOTYETVALID:
//                    message = "The certificate is not yet valid.";
//                    break;
//            }
//
//            message += " Do you want to continue anyway?";
//            alertDialog.setTitle("SSL Certificate Error");
//            alertDialog.setMessage(message);
//            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    // Ignore SSL certificate errors
//                    handler.proceed();
//                }
//            });
//
//            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//
//                    handler.cancel();
//                }
//            });
//            alertDialog.show();


        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.i("value", url);

        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);

        }

        @Override
        public void onPageFinished(WebView view, final String url) {
            super.onPageFinished(view, url);
            Log.i("PSEReMobile URL", url);
            pbWebView.setVisibility(View.GONE);

            if (url.contains("LoadMoney/PSERedirectMobile")) {
                StringRequest req = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        JSONObject obj = null;
                        Log.i("PSEReMobile Response", s);
                        try {
                            obj = new JSONObject(s);
                            boolean success = obj.getBoolean("success");
                            String message = obj.getString("message");
                            if (success) {
//                                pbWebView.setVisibility(View.VISIBLE);
                                loadFinalPayment();
                            } else {
                                getActivity().finish();
                                CustomToast.showMessage(getActivity(), message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });
                int socketTimeout = 60000;
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                req.setRetryPolicy(policy);
                requestQueue.add(req);
            }

        }
    }

    public void loadFinalPayment() {
        StringRequest req = new StringRequest(Request.Method.GET, ApiURLNew.URL_FINAL_PROCESS + trazability, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                JSONObject obj = null;
                Log.i("PSEReMobile URL", ApiURLNew.URL_FINAL_PROCESS + trazability);
                Log.i("FinalPay Response", s);
//                {"success":false,"message":"Balance Credited, \n Transaction Status OK",
//                  "sessionId":"CBD4BFF39C261FE55C26F4F83240EF22"}
                try {
                    obj = new JSONObject(s);
                    boolean success = obj.getBoolean("success");
                    String message = obj.getString("message");
//                    String sessionId = obj.getString("sessionId");
                    if (success) {
                        showACHpayDialog(message);
                    } else if (!success) {
                        showACHpayDialog(message);
                    } else {
                        getActivity().finish();
                        CustomToast.showMessage(getActivity(), message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        requestQueue.add(req);
    }

    private void sendRefresh() {
        getActivity().finish();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void loadReciepts() {
        pbWebView.setVisibility(View.GONE);
        Intent menuIntent = new Intent(getActivity(), MainMenuDetailActivity.class);
        menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Reciepts");
        startActivity(menuIntent);
        getActivity().finish();
    }

    private void workLoad(final int time) {
        //Checking internet Status
        connectionDetector = new ConnectionDetector(getActivity().getApplicationContext());
        isInternetPresent = connectionDetector.isConnectingToInternet();

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (getActivity() == null) {
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (isInternetPresent) {
                                                        isResumed = true;
                                                    } else {
                                                        showCustomDialog();
                                                    }
                                                }

                                            }
                );
            }
        };
        thread.start();
    }

    public void showCustomDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(generateMessage()));

        builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                isResumed = true;
                dialog.dismiss();
                startActivity(new Intent(Settings.ACTION_SETTINGS));

            }
        });

        builder.setNegativeButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                workLoad(1000);
            }
        });
        builder.show();
    }

    public String generateMessage() {
        String source = "<b><font color=#ff0000> No internet connection.</font></b>" +
                "<br><br><b><font color=#000000> Please check your internet connection and try again.</font></b><br></br>";
        return source;
    }

    private void showACHpayDialog(String msge) {
        CustomAlertDialog customAlertDialog = new CustomAlertDialog(getActivity(), R.string.dialog_title2, msge);
        customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                sendRefresh();
                loadReciepts();
            }
        });
        customAlertDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isResumed) {
            workLoad(1000);
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().finish();
    }
}
