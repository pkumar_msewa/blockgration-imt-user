package com.Blockgration.fragment.mainmenufragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.Blockgration.fragment.mobiletopupfragment.PostPaidFragment;
import com.Blockgration.fragment.mobiletopupfragment.PrepaidFragment;
import com.Blockgration.Userwallet.R;

/**
 * Created by Ksf on 8/8/2016.
 */
public class TelcoServiceFragment extends Fragment {
    private TabLayout mSlidingTabLayout;
    private ViewPager mainPager;
    private FragmentManager fragmentManager;
    CharSequence TitlesEnglish[]=  null;
    int NumbOfTabs =2;

    private View rootView;
    private String navType ="";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TitlesEnglish = new CharSequence[]{String.valueOf(getResources().getString(R.string.topup_prepaid)),String.valueOf(getResources().getString(R.string.topup_postpaid))};
        fragmentManager = getActivity().getSupportFragmentManager();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_tabs,container,false);
        mainPager = (ViewPager) rootView.findViewById(R.id.tabPager);
        mSlidingTabLayout = (TabLayout) rootView.findViewById(R.id.tabLayout);
        mainPager.setAdapter(new ViewPagerAdapter(fragmentManager,TitlesEnglish, NumbOfTabs));
        mSlidingTabLayout.setupWithViewPager(mainPager);
        return rootView;
    }


    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
        int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


        // Build a Constructor and assign the passed Values to appropriate values in the class
        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
            super(fm);
            this.Titles = mTitles;
            this.NumbOfTabs = mNumbOfTabsumb;

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0){
                PrepaidFragment tab1 = new PrepaidFragment();
                return tab1;
            }
            else {
                PostPaidFragment tab2 = new PostPaidFragment();
                return tab2;
            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return NumbOfTabs;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}

