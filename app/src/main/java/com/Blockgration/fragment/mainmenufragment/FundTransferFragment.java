package com.Blockgration.fragment.mainmenufragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RadioGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.CurrentAccountModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.CheckLog;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.PayingDetailsValidation;
import com.Blockgration.util.SecurityUtil;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ksf on 7/15/2016.
 */
public class FundTransferFragment extends Fragment {

    private Button btnFundTransfer;
    private ImageButton ibFundTransferPhoneBook;
    private MaterialEditText etFundTransferNo, etFundTransferEmail;
    private MaterialEditText etFundTransferName, etFundTransferAmount;
    private FrameLayout flFundTransferNo;

    public static final int PICK_CONTACT = 1;

    private UserModel session = UserModel.getInstance();

    private View focusView = null;
    private boolean cancel;
    private String transferNo, transferName, transferAmount, transferEmail;
    private RadioGroup rgFundTransferType;
    //Volley Tag
    private String tag_json_obj = "json_obj_req";


    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    private boolean isMobile = true;

    private String current_acc_bal ,current_country, current_currency, current_currency_symbol;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fund_transfer, container, false);
        etFundTransferAmount = (MaterialEditText) rootView.findViewById(R.id.etFundTransferAmount);
        etFundTransferName = (MaterialEditText) rootView.findViewById(R.id.etFundTransferName);

        etFundTransferNo = (MaterialEditText) rootView.findViewById(R.id.etFundTransferNo);
        etFundTransferEmail = (MaterialEditText) rootView.findViewById(R.id.etFundTransferEmail);
        flFundTransferNo = (FrameLayout) rootView.findViewById(R.id.flFundTransferNo);


//        rgFundTransferType = (RadioGroup) rootView.findViewById(R.id.rgFundTransferType);

        etFundTransferNo.setVisibility(View.GONE);
        etFundTransferEmail.setVisibility(View.VISIBLE);

        btnFundTransfer = (Button) rootView.findViewById(R.id.btnFundTransfer);
        ibFundTransferPhoneBook = (ImageButton) rootView.findViewById(R.id.ibFundTransferPhoneBook);

        ibFundTransferPhoneBook.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });


        btnFundTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transferEmail();
//                if (isMobile) {
//                    transferMobile();
//                } else {
//                    transferEmail();
//                }
            }
        });

        isMobile = false;
//        rgFundTransferType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                switch (i) {
//                    case R.id.rbFundTransferEmail:
//                        flFundTransferNo.setVisibility(View.GONE);
//                        etFundTransferEmail.setVisibility(View.VISIBLE);
//                        isMobile = false;
//                        break;
//                    case R.id.rbFundTransferMobile:
//                        flFundTransferNo.setVisibility(View.VISIBLE);
//                        etFundTransferEmail.setVisibility(View.GONE);
//                        etFundTransferNo.setVisibility(View.VISIBLE);
//                        isMobile = true;
//                        break;
//                }
//            }
//        });

        List<CurrentAccountModel> accountModels = Select.from(CurrentAccountModel.class).list();
        current_acc_bal = accountModels.get(0).getAccBalance();
        current_country = accountModels.get(0).getAccCountry();
        current_currency = accountModels.get(0).getCurrencyCode();
        current_currency_symbol = accountModels.get(0).getCurrencySymbol();

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactUri = data.getData();
                    Cursor c = getActivity().getContentResolver().query(contactUri, projection, null, null, null);
                    if (c.moveToFirst()) {
                        String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String finalNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
                        removeCountryCode(finalNumber);
                    }
                }
        }
    }

    private void transferEmail() {
        etFundTransferEmail.setError(null);
        etFundTransferName.setError(null);
        etFundTransferAmount.setError(null);

        cancel = false;

        transferAmount = etFundTransferAmount.getText().toString();
        transferEmail = etFundTransferEmail.getText().toString();
        transferName = etFundTransferName.getText().toString();

        boolean b = validateEmail();
        if(!b){
            focusView.requestFocus();
            return;
        }
        checkName(transferName);
        checkPayAmount(transferAmount);

        if (cancel) {
            focusView.requestFocus();
            return;
        } else {
            String msgc = "<b><font color=#000000>Sending From : </font></b>"+session.getUserEmail()+"<br>"+
                    "<b><font color=#000000>Sending To : </font></b>"+transferEmail+"<br>"+
                    "<b><font color=#000000>Sending Amount : </font></b>"+transferAmount+" "+current_currency_symbol+"<br>"+
                    "<b><font color=red>Are you sure you want to proceed?</font></b>";
            Spanned result;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                result = Html.fromHtml(msgc, Html.FROM_HTML_MODE_LEGACY);
            } else {
                result = Html.fromHtml(msgc);
            }
            showConfirmation(result);
        }
    }

    private void showConfirmation(Spanned putMsg) {
        CustomAlertDialog customAlertDialog = new CustomAlertDialog(getActivity(), R.string.dialog_title2, putMsg);
        customAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                loadDlg.show();
                promoteSendMoneyEmail();
            }
        });
        customAlertDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        customAlertDialog.show();
    }


//    private void transferMobile() {
//
//        etFundTransferNo.setError(null);
//        etFundTransferName.setError(null);
//        etFundTransferAmount.setError(null);
//
//        cancel = false;
//
//        transferAmount = etFundTransferAmount.getText().toString();
//        transferNo = etFundTransferNo.getText().toString();
//        transferName = etFundTransferName.getText().toString();
//
//        checkPayAmount(transferAmount);
//        checkPhone(transferNo);
//        checkName(transferName);
//
//        if (cancel) {
//            focusView.requestFocus();
//        } else {
//            promoteSendMoneyMobile();
//        }
//    }

//    private void checkPhone(String phNo) {
//        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
//        if (!gasCheckLog.isValid) {
//            etFundTransferNo.setError(getString(gasCheckLog.msg));
//            focusView = etFundTransferNo;
//            cancel = true;
//        }
//
//    }

    private boolean validateEmail() {
        String email = etFundTransferEmail.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            etFundTransferEmail.setError("Enter valid email");
            focusView = etFundTransferEmail;
            return false;
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etFundTransferAmount.setError(getString(gasCheckLog.msg));
            focusView = etFundTransferAmount;
            cancel = true;
        }
        else if (Integer.valueOf(etFundTransferAmount.getText().toString()) < 6) {
            etFundTransferAmount.setError(getString(R.string.lessAmount));
            focusView = etFundTransferAmount;
            cancel = true;
        }
    }

    private void checkName(String name) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(name);
        if (!gasCheckLog.isValid) {
            etFundTransferName.setError(getString(gasCheckLog.msg));
            focusView = etFundTransferName;
            cancel = true;
        }
    }


    private void removeCountryCode(String number) {
        if (hasCountryCode(number)) {
            int country_digits = number.length() - 10;
            number = number.substring(country_digits);
            etFundTransferNo.setText(number);
        } else {
            etFundTransferNo.setText(number);
        }
    }

    private boolean hasCountryCode(String number) {
        return number.charAt(0) == '+';
    }


//    public void promoteSendMoneyMobile() {
//        loadDlg.show();
//        jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("mobileNumber", etFundTransferNo.getText().toString());
//            jsonRequest.put("amount", etFundTransferAmount.getText().toString());
////            jsonRequest.put("message", etFundTransferName.getText().toString());
//            jsonRequest.put("sessionId", session.getUserSessionId());
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        }
//
//        if (jsonRequest != null) {
//            Log.i("JsonRequest", jsonRequest.toString());
//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_FUND_TRANSFER_MOBILE, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    Log.i("Send Money", response.toString());
//                    try {
//                        String code = response.getString("code");
//                        String msg = response.getString("message");
//                        if (code != null && code.equals("S00")) {
////                            String jsonString = response.getString("response");
////                            JSONObject jsonObject = new JSONObject(jsonString);
//                            String sucessMessage = response.getString("message");
//                            etFundTransferNo.getText().clear();
//                            etFundTransferAmount.getText().clear();
//                            etFundTransferName.getText().clear();
//
//                            loadDlg.dismiss();
//                            sendRefresh();
//                            CustomToast.showMessage(getActivity(), sucessMessage);
//                        } else if (code != null && code.equals("F03")) {
//                            loadDlg.dismiss();
//                            showInvalidSessionDialog();
//                        } else if (code != null && code.equals("F04")) {
//                            loadDlg.dismiss();
//                            JSONObject jsonObject = response.getJSONObject("details");
//                            if (jsonObject.has("email")) {
//                                String errorMessage = jsonObject.getString("email");
//                                CustomToast.showMessage(getActivity(), errorMessage);
//                            } else if (jsonObject.has("mobileNumber")) {
//                                String errorMessage = jsonObject.getString("mobileNumber");
//                                CustomToast.showMessage(getActivity(), errorMessage);
//                            }
//                        } else if (code != null && code.equals("F00")) {
//                            loadDlg.dismiss();
////                            String errorMessage = response.getString("details");
//                            CustomToast.showMessage(getActivity(), msg);
//                        }
//
//                    } catch (JSONException e) {
//                        loadDlg.dismiss();
//                        CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception2));
//
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
//                    CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception));
//
//                    error.printStackTrace();
//
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", SecurityUtil.getSecurityKey(jsonRequest.toString()));
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApplication.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//
//    }

    public void promoteSendMoneyEmail() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("username", etFundTransferEmail.getText().toString());
            jsonRequest.put("amount", etFundTransferAmount.getText().toString());
            jsonRequest.put("message", etFundTransferName.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_FUND_TRANSFER_EMAIL, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Send Money", response.toString());
                    try {
                        loadDlg.dismiss();
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);
                            String sucessMessage = response.getString("message");
                            etFundTransferNo.getText().clear();
                            etFundTransferAmount.getText().clear();
                            etFundTransferName.getText().clear();
                            etFundTransferEmail.getText().clear();
                            loadDlg.dismiss();
                            sendRefresh();
                            CustomToast.showMessage(getActivity(), sucessMessage);
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else if (code != null && code.equals("F04")) {
                            loadDlg.dismiss();
                            String jsonObject = response.getString("message");
//                            String errorMessage = jsonObject.getString("email");
                            CustomToast.showMessage(getActivity(), jsonObject);
                        } else if (code != null && code.equals("F00")) {
                            loadDlg.dismiss();
                            String errorMessage = response.getString("message");
                            CustomToast.showMessage(getActivity(), errorMessage);
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception2));
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));

                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", SecurityUtil.getSecurityKey(jsonRequest.toString()));
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }


    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        getActivity().finish();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


}

