//package com.msewa.fragment.mainmenufragment;
//
//import android.app.Activity;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.database.Cursor;
//import android.net.Uri;
//import android.os.Bundle;
//import android.provider.ContactsContract;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.content.LocalBroadcastManager;
//import android.text.Html;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.inputmethod.EditorInfo;
//import android.widget.Button;
//import android.widget.ImageButton;
//import android.widget.TextView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//
//
//import com.msewa.claro.PayQwikApp;
//import com.msewa.claro.R;
//import com.msewa.custom.CustomAlertDialog;
//import com.msewa.custom.CustomToast;
//import com.msewa.custom.LoadingDialog;
//import com.msewa.metadata.ApiURLNew;
//import com.msewa.metadata.AppMetadata;
//import com.msewa.model.UserModel;
//import com.msewa.util.CheckLog;
//import com.msewa.util.PayingDetailsValidation;
//import com.rengwuxian.materialedittext.MaterialEditText;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.HashMap;
//import java.util.Map;
//
//
//
//
//public class InviteFreindFragment extends Fragment {
//    public static final int PICK_CONTACT = 1;
//    private View rootView;
//    private Button btnInvite, btnShare;
//    private MaterialEditText etInviteName;
//    private MaterialEditText etInviteNumber;
//    private String inviteNumber;
//    private String inviteName;
//    private boolean cancel;
//    private View focusView;
//    private ImageButton ibFundTransferPhoneBook;
//    private UserModel session = UserModel.getInstance();
//
//    private JSONObject jsonRequest;
//    private LoadingDialog loadDlg;
//
//    //Volley Tag
//    private String tag_json_obj = "json_invite_freinds";
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        loadDlg = new LoadingDialog(getActivity());
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.fragment_invitefriend, container, false);
//        etInviteNumber = (MaterialEditText) rootView.findViewById(R.id.etInviteNumber);
//        etInviteName = (MaterialEditText) rootView.findViewById(R.id.etInviteName);
//        btnInvite = (Button) rootView.findViewById(R.id.btnInvite);
//        ibFundTransferPhoneBook = (ImageButton) rootView.findViewById(R.id.ibFundTransferPhoneBook);
//        btnShare = (Button) rootView.findViewById(R.id.btnShare);
//
//        btnInvite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                attemptInviteFriend();
//
//            }
//        });
//
//        //DONE CLICK ON VIRTUAL KEYPAD
//        etInviteName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    attemptInviteFriend();
//                }
//                return false;
//            }
//        });
//
//        ibFundTransferPhoneBook.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
//                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
//                startActivityForResult(intent, PICK_CONTACT);
//            }
//        });
//
//        btnShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent2 = new Intent(Intent.ACTION_SEND);
//                intent2.setType("text/plain");
//                intent2.putExtra(Intent.EXTRA_TEXT, "Hey!! I just started using Comet and its awesome. You should try using it as well! Click here to download the app: https://play.google.com/store/apps");
//                startActivity(Intent.createChooser(intent2, "Share via"));
//
//            }
//        });
//        return rootView;
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
//        switch (requestCode) {
//            case (PICK_CONTACT):
//                if (resultCode == Activity.RESULT_OK) {
//                    Uri contactUri = data.getData();
//                    Cursor c = getActivity().getContentResolver().query(
//                            contactUri, projection, null, null, null);
//                    if (c != null) {
//                        if (c.moveToFirst()) {
//                            String name = c.getString(c
//                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
//                            etInviteName.setText(name);
//                            String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                            String finalNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
//                            if (finalNumber != null && !finalNumber.isEmpty()) {
//                                removeCountryCode(finalNumber);
//                            }
//                        }
//                        c.close();
//                    }
//                }
//        }
//    }
//
//    private void attemptInviteFriend() {
//        etInviteNumber.setError(null);
//        etInviteName.setError(null);
//        cancel = false;
//
//        inviteNumber = etInviteNumber.getText().toString();
//        inviteName = etInviteName.getText().toString();
//
//
//        checkInviteName(inviteName);
//        checkInviteNumber(inviteNumber);
//
//
//        if (cancel) {
//            focusView.requestFocus();
//        } else {
//            promoteInviteFriend();
//
//        }
//    }
//
//    private void checkInviteNumber(String inviteNumber) {
//        CheckLog inviteNumberCheckLog = PayingDetailsValidation.checkMobileTenDigit(inviteNumber);
//        if (!inviteNumberCheckLog.isValid) {
//            etInviteNumber.setError(getString(inviteNumberCheckLog.msg));
//            focusView = etInviteNumber;
//            cancel = true;
//        }
//
//    }
//
//    private void checkInviteName(String inviteName) {
//        CheckLog inviteNameCheckLog = PayingDetailsValidation.checkFirstName(inviteName);
//        if (!inviteNameCheckLog.isValid) {
//            etInviteName.setError(getString(inviteNameCheckLog.msg));
//            focusView = etInviteName;
//            cancel = true;
//        }
//
//    }
//
//    private void removeCountryCode(String number) {
//        if (hasCountryCode(number)) {
//            int country_digits = number.length() - 10;
//            number = number.substring(country_digits);
//            etInviteNumber.setText(number);
//        } else if (hasZero(number)) {
//            if (number.length() >= 10) {
//                int country_digits = number.length() - 10;
//                number = number.substring(country_digits);
//                etInviteNumber.setText(number);
//            } else {
//                CustomToast.showMessage(getActivity(), "Please select 10 digit no");
//            }
//
//        } else {
//            etInviteNumber.setText(number);
//        }
//
//    }
//
//    private boolean hasZero(String number) {
//        return number.charAt(0) == '0';
//    }
//
//    private boolean hasCountryCode(String number) {
//        return number.charAt(0) == '+';
//    }
//
//    private void promoteInviteFriend() {
//        loadDlg.show();
//        jsonRequest = new JSONObject();
//        try {
//            jsonRequest.put("sessionId", session.getUserSessionId());
//            jsonRequest.put("receiversName", etInviteName.getText().toString());
//            jsonRequest.put("mobileNo", etInviteNumber.getText().toString());
//            jsonRequest.put("message", "Hi! I would like to recommend VPayQwik to you.");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonRequest = null;
//        }
//
//        if (jsonRequest != null) {
//            Log.i("JsonRequest", jsonRequest.toString());
//            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_INVITE_FRIENDS, jsonRequest, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    try {
//                        Log.i("Invite RESPONSE", response.toString());
//                        String code = response.getString("code");
//
//                        if (code != null && code.equals("S00")) {
//                            loadDlg.dismiss();
//                            etInviteName.getText().clear();
//                            etInviteNumber.getText().clear();
//                            if (response.has("message") && response.getString("message") != null) {
//                                String message = response.getString("message");
//                                CustomToast.showMessage(getActivity(), message);
//                            }
//
//                        } else if (code != null && code.equals("F03")) {
//                            loadDlg.dismiss();
//                            showInvalidSessionDialog();
//                        } else {
//                            if (response.has("message") && response.getString("message") != null) {
//                                String message = response.getString("message");
//                                CustomToast.showMessage(getActivity(), message);
//                            }
//                        }
//
//                    } catch (JSONException e) {
//                        loadDlg.dismiss();
//                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
//
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    loadDlg.dismiss();
//                    error.printStackTrace();
//                    CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception));
//
//
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("hash", "1234");
//                    return map;
//                }
//
//            };
//            int socketTimeout = 60000;
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            postReq.setRetryPolicy(policy);
//            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
//        }
//    }
//
//    private void sendLogout() {
//        Intent intent = new Intent("setting-change");
//        intent.putExtra("updates", "4");
//        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
//    }
//
//    public void showInvalidSessionDialog() {
//        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                sendLogout();
//            }
//        });
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//
//            }
//        });
//        builder.show();
//    }
//
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
