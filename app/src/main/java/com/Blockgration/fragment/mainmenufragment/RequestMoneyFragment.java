package com.Blockgration.fragment.mainmenufragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;
import java.util.Map;

import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.UserModel;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.util.CheckLog;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.PayingDetailsValidation;

/**
 * Created by Ksf on 6/21/2016.
 */
public class RequestMoneyFragment extends Fragment {

    public static final int PICK_CONTACT = 1;
    private static final int MOBILE_DIGITS = 10;
    private View rootView;
    private Button btnRequestMoney;
    private ImageButton ibFundTransferPhoneBook;
    private MaterialEditText etRequestMoneyNo;
    private MaterialEditText etRequestMoneyName, etRequestMoneyAmount, etRequestMoneyMessage;
    private UserModel session = UserModel.getInstance();
    private View focusView = null;
    private boolean cancel;
    private String transferNo, transferName, transferAmount, transferMessage;
    private LoadingDialog loadDlg;
    private String messageToSend = "";
    //Volley Tag
    private String tag_json_obj = "json_request_pay";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_request_money, container, false);
        etRequestMoneyAmount = (MaterialEditText) rootView.findViewById(R.id.etRequestMoneyAmount);
        etRequestMoneyName = (MaterialEditText) rootView.findViewById(R.id.etRequestMoneyName);
        etRequestMoneyNo = (MaterialEditText) rootView.findViewById(R.id.etRequestMoneyNo);
        etRequestMoneyMessage = (MaterialEditText) rootView.findViewById(R.id.etRequestMoneyMessage);
        btnRequestMoney = (Button) rootView.findViewById(R.id.btnRequestMoney);
        ibFundTransferPhoneBook = (ImageButton) rootView.findViewById(R.id.ibFundTransferPhoneBook);

        ibFundTransferPhoneBook.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });


        btnRequestMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPayment();
            }
        });

        //DONE CLICK ON VIRTUAL KEYPAD
        etRequestMoneyMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    attemptPayment();
                }
                return false;
            }
        });
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactUri = data.getData();
                    Cursor c = getActivity().getContentResolver().query(contactUri, projection, null, null, null);
                    if (c != null) {
                        if (c.moveToFirst()) {
                            String name = c.getString(c
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            etRequestMoneyName.setText(name);
                            String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String finalNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
                            removeCountryCode(finalNumber);
                        }
                        c.close();
                    }
                }
        }
    }

    private void attemptPayment() {
        etRequestMoneyNo.setError(null);
        etRequestMoneyName.setError(null);
        etRequestMoneyAmount.setError(null);
        etRequestMoneyMessage.setError(null);
        cancel = false;

        transferAmount = etRequestMoneyAmount.getText().toString();
        transferNo = etRequestMoneyNo.getText().toString();
        transferName = etRequestMoneyName.getText().toString();
        transferMessage = etRequestMoneyMessage.getText().toString();
        checkPayAmount(transferAmount);
        checkPhone(transferNo);
        checkName(transferName);
        checkMessage(transferMessage);
        if (cancel) {
            focusView.requestFocus();
        } else {
            promoteRequestMoney();
        }
    }

    private void checkPhone(String phNo) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkMobileTenDigit(phNo);
        if (!gasCheckLog.isValid) {
            etRequestMoneyNo.setError(getString(gasCheckLog.msg));
            focusView = etRequestMoneyNo;
            cancel = true;
        }
    }

    private void checkPayAmount(String amount) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
        if (!gasCheckLog.isValid) {
            etRequestMoneyAmount.setError(getString(gasCheckLog.msg));
            focusView = etRequestMoneyAmount;
            cancel = true;
        }
    }

    private void checkName(String name) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(name);
        if (!gasCheckLog.isValid) {
            etRequestMoneyName.setError(getString(gasCheckLog.msg));
            focusView = etRequestMoneyName;
            cancel = true;
        }
    }

    private void checkMessage(String name) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(name);
        if (!gasCheckLog.isValid) {
            etRequestMoneyMessage.setError(getString(gasCheckLog.msg));
            focusView = etRequestMoneyMessage;
            cancel = true;
        }
    }


    private void removeCountryCode(String number) {
        if (hasCountryCode(number)) {
            int country_digits = number.length() - MOBILE_DIGITS;
            number = number.substring(country_digits);
            etRequestMoneyNo.setText(number);
        } else if (hasZero(number)) {
            if (number.length() >= 10) {
                int country_digits = number.length() - MOBILE_DIGITS;
                number = number.substring(country_digits);
                etRequestMoneyNo.setText(number);
            } else {
                CustomToast.showMessage(getActivity(), "Please select 10 digit no");
            }

        } else {
            etRequestMoneyNo.setText(number);
        }

    }

    private boolean hasCountryCode(String number) {
        return number.charAt(0) == '+';
    }

    private boolean hasZero(String number) {
        return number.charAt(0) == '0';
    }


    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "4");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }


    public void promoteRequestMoney() {
        messageToSend = "Dear " + etRequestMoneyName.getText().toString() + ", " + session.getUserMobileNo() + " " + session.getUserFirstName() + " has requested Rs." + etRequestMoneyAmount.getText().toString() + " with the message, " + etRequestMoneyMessage.getText().toString() + "." + " Download VPayQwik: " + "https://play.google.com/store/apps/details?" + getActivity().getApplication().getPackageName() + " Thank you.";
        loadDlg.show();
        StringRequest postReq = new StringRequest(Request.Method.POST, ApiURLNew.URL_SEND_FREE_SMS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("RESPONSE", response);
                loadDlg.dismiss();
                CustomToast.showMessage(getActivity(), "Request sent to" + etRequestMoneyNo.getText().toString());
                etRequestMoneyMessage.getText().clear();
                etRequestMoneyAmount.getText().clear();
                etRequestMoneyName.getText().clear();
                etRequestMoneyNo.getText().clear();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadDlg.dismiss();
                CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                error.printStackTrace();

            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", "msewaScrub");
                params.put("password", "uuO2Qtgz");
                params.put("type", "0");
                params.put("dlr", "1");
                params.put("source", "VPYQWK");
                params.put("message", messageToSend);
                params.put("destination", etRequestMoneyNo.getText().toString());
                Log.i("msg",params.toString());
                return params;
            }
        };
        PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
    }


}

