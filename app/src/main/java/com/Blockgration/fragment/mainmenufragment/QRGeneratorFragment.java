package com.Blockgration.fragment.mainmenufragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.R;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.EncryptDecryptUserUtility;

import java.io.ByteArrayOutputStream;


/**
 * Created by Ksf on 4/9/2016.
 */
public class QRGeneratorFragment extends Fragment {
    private View rootView;
    private Button btnQRCodeGenerate;
    private ImageView ivGenerateQRCode;
    private TextView tvQrGeneratorMsg;
    private String qrCodeOutput = "";
    private int WIDTH = 800;
    private GetQRImageTask getImageTask;
    private LoadingDialog loadingDialog;
    private UserModel session = UserModel.getInstance();


    //QR Shared Preference.
    public static final String QR = "qr";
    SharedPreferences qrPreferences;
    private String storedAccID;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_qr_generator, container, false);


        loadingDialog = new

                LoadingDialog(getActivity());

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()

                                             {
                                                 @Override
                                                 public void onClick(View view) {
                                                     getActivity().onBackPressed();
                                                 }
                                             }

        );

        qrPreferences = getActivity().getSharedPreferences(QR, Context.MODE_PRIVATE);

        storedAccID = qrPreferences.getString("accNo", "");
        btnQRCodeGenerate = (Button) rootView.findViewById(R.id.btnQRCodeGenerate);

        ivGenerateQRCode = (ImageView)
                rootView.findViewById(R.id.ivGenerateQRCode);

        tvQrGeneratorMsg = (TextView)
                rootView.findViewById(R.id.tvQrGeneratorMsg);

        if (storedAccID != null && !storedAccID.isEmpty() && storedAccID.equals(session.getUserAcNo()))

        {
            Bitmap obtainedBitmap = stringToBitMap(qrPreferences.getString("qrImage", ""));
            if (obtainedBitmap != null) {
                ivGenerateQRCode.setImageBitmap(obtainedBitmap);
                btnQRCodeGenerate.setText("Re-Generate");
                tvQrGeneratorMsg.setText(getResources().getString(R.string.qr_generated));
            } else {
                ivGenerateQRCode.setImageResource(R.drawable.generate_qrcode);
                tvQrGeneratorMsg.setText(getResources().getString(R.string.qr_generator));
            }

        } else

        {
            btnQRCodeGenerate.setVisibility(View.VISIBLE);
            ivGenerateQRCode.setImageResource(R.drawable.generate_qrcode);
            tvQrGeneratorMsg.setText(getResources().getString(R.string.qr_generator));
        }

        btnQRCodeGenerate.setOnClickListener(new View.OnClickListener()

                                             {
                                                 @Override
                                                 public void onClick(View view) {
                                                     getImageTask = new GetQRImageTask();
                                                     getImageTask.execute();
                                                 }
                                             }

        );
        return rootView;
    }


    private class GetQRImageTask extends AsyncTask<String, Bitmap, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                qrCodeOutput = EncryptDecryptUserUtility.encrypt(session.getUserFirstName() + "-" + session.getUserMobileNo());
                Log.i("RedeemCode", qrCodeOutput);
            } catch (Exception e) {
                e.printStackTrace();
            }

            loadingDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... urls) {

            Bitmap bitmap = null;
            try {
                bitmap = encodeAsBitmap(qrCodeOutput.trim());
                if (bitMapToString(bitmap) != null && !bitMapToString(bitmap).isEmpty()) {
                    SharedPreferences.Editor editor = qrPreferences.edit();
                    editor.clear();
                    editor.putString("accNo", session.getUserAcNo());
                    editor.putString("qrImage", bitMapToString(bitmap));
                    editor.apply();
                    return bitmap;
                } else {
                    return bitmap;
                }

            } catch (WriterException e) {
                e.printStackTrace();
            } // end of catch block
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                ivGenerateQRCode.setImageBitmap(result);
                btnQRCodeGenerate.setText("Re-Generate");
                tvQrGeneratorMsg.setText(getResources().getString(R.string.qr_generated));
            } else {
                CustomToast.showMessage(getActivity(), "Error generating code");
            }
            loadingDialog.dismiss();
        }

    }

    Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, WIDTH, WIDTH, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? ContextCompat.getColor(getActivity(), R.color.menu6) : ContextCompat.getColor(getActivity(), R.color.white_text);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 800, 0, 0, w, h);
        return bitmap;
    }

    public String bitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public Bitmap stringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(encodeByte, 0,
                    encodeByte.length);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }


}
