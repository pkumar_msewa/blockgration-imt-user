//package com.msewa.fragment.mainmenufragment;
//
//import android.content.DialogInterface;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.text.Html;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ProgressBar;
//import android.widget.ScrollView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.orm.query.Condition;
//import com.orm.query.Select;
//import CustomAlertDialog;
//import CustomToast;
//import LoadingDialog;
//import ApiURLNew;
//import AppMetadata;
//import BillPayOperatorModel;
//import MobileRechargeCountryModel;
//import UserModel;
//import com.msewa.faspay.PayQwikApp;
//import com.msewa.faspay.R;
//import CheckLog;
//import PayingDetailsValidation;
//import com.rengwuxian.materialedittext.MaterialEditText;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by Ksf on 8/9/2016.
// */
//public class BillPaymentsFragment extends Fragment {
//
//    //Views
//    private View rootView;
//    private MaterialEditText etBillPayAmount, etBillPayOperator, etBillPayCountry, etBillPayServiceType, etBillPayServiceNo;
//    private ScrollView svBillPayment;
//    private Button btnBillPay;
//    private ProgressBar pbBillPayment;
//
//    //Variables
//    private List<BillPayOperatorModel> serviceTypeArray;
//    ArrayList<MobileRechargeCountryModel> countryArray = AppMetadata.getCountry();
//    private View focusView = null;
//    private boolean cancel;
//
//    private String selectedCurrencyCode = null, selectedCountryCode = null, selectedCurrentId = null;
//
//
//    //Session Instance
//    private UserModel session = UserModel.getInstance();
//
//    //Task and loader
//    private GetOperator getOperatorTask = null;
//    private LoadingDialog loadingDialog;
//
//    //Volley Tag
//    private String tag_json_obj = "json_obj_req";
//
//    private String billPayUrl = "";
//
//    private static final String KEY_ELECTRICITY = "Electricity";
//    private static final String KEY_LAND_LINE = "LandLine";
//    private static final String KEY_DTH = "DTH";
//
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        loadingDialog = new LoadingDialog(getActivity());
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.fragment_bill_payment, container, false);
//        svBillPayment = (ScrollView) rootView.findViewById(R.id.svBillPayment);
//        pbBillPayment = (ProgressBar) rootView.findViewById(R.id.pbBillPayment);
//        etBillPayServiceType = (MaterialEditText) rootView.findViewById(R.id.etBillPayServiceType);
//        etBillPayCountry = (MaterialEditText) rootView.findViewById(R.id.etBillPayCountry);
//        etBillPayOperator = (MaterialEditText) rootView.findViewById(R.id.etBillPayOperator);
//        etBillPayAmount = (MaterialEditText) rootView.findViewById(R.id.etBillPayAmount);
//        etBillPayServiceNo = (MaterialEditText) rootView.findViewById(R.id.etBillPayServiceNo);
//
//        btnBillPay = (Button) rootView.findViewById(R.id.btnBillPay);
//        etBillPayServiceType.setFocusable(false);
//        etBillPayCountry.setFocusable(false);
//        etBillPayOperator.setFocusable(false);
//
//        serviceTypeArray = ((PayQwikApp) getActivity().getApplication()).getData();
//
//        if (serviceTypeArray == null || serviceTypeArray.size() == 0) {
//            getOperatorTask = new GetOperator();
//            getOperatorTask.execute();
//        } else {
//            afterTaskComplete();
//        }
//
//
//        return rootView;
//    }
//
//
//    public void showCountryDialog() {
//        final ArrayList<String> countryVal = new ArrayList<>();
//        Select specificSpecificQueryGt = Select.from(BillPayOperatorModel.class).where(Condition.prop("servicename").eq(etBillPayServiceType.getText().toString()));
//        serviceTypeArray.clear();
//        serviceTypeArray = specificSpecificQueryGt.list();
//
//        for (BillPayOperatorModel countryModel : serviceTypeArray) {
//            int searchListLength = countryArray.size();
//            for (int i = 0; i < searchListLength; i++) {
//                if (countryArray.get(i).getCountryCode().contains(countryModel.getServicecountry())) {
//                    countryVal.add(countryArray.get(i).getCountryName());
//                    HashSet<String> hashSet = new HashSet<>();
//                    hashSet.addAll(countryVal);
//                    countryVal.clear();
//                    countryVal.addAll(hashSet);
//                }
//            }
////            selectedCountryCode = countryModel.getServicecountry();
//        }
//
//        android.support.v7.app.AlertDialog.Builder currencyDialog =
//                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
//        currencyDialog.setTitle("Select your Service");
//
//        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//
//        currencyDialog.setItems(countryVal.toArray(new String[countryVal.size()]),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface arg0, int i) {
//                        etBillPayCountry.setText(countryVal.get(i));
//                        etBillPayOperator.getText().clear();
//
//                        for (MobileRechargeCountryModel countryCode : countryArray) {
//                            if (countryCode.getCountryName().contains(etBillPayCountry.getText().toString())) {
//                                Log.i("CountryCode", countryCode.getCountryCode());
//                                selectedCountryCode = countryCode.getCountryCode();
//                            }
//                        }
//                    }
//                });
//
//        currencyDialog.show();
//
//    }
//
//
//    public void showOperatorDialog() {
//        final ArrayList<String> operatorVal = new ArrayList<>();
//        Select specificOptQuery = Select.from(BillPayOperatorModel.class).where(Condition.prop("servicename").eq(etBillPayServiceType.getText().toString())).and(Condition.prop("servicecountry").eq(selectedCountryCode));
//        serviceTypeArray.clear();
//        serviceTypeArray = specificOptQuery.list();
//
//        for (BillPayOperatorModel operatorModel : serviceTypeArray) {
//            operatorVal.add(operatorModel.getOperatorname());
//        }
//
//
//        android.support.v7.app.AlertDialog.Builder currencyDialog =
//                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
//        currencyDialog.setTitle("Select your Service");
//
//        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//
//        currencyDialog.setItems(operatorVal.toArray(new String[operatorVal.size()]),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface arg0, int i) {
//                        etBillPayOperator.setText(operatorVal.get(i));
//                        selectedCurrencyCode = serviceTypeArray.get(i).getServicecurrency();
//                        selectedCurrentId = serviceTypeArray.get(i).getServiceid();
//
//                    }
//                });
//
//        currencyDialog.show();
//
//    }
//
//    public void showServiceTypeDialog() {
//        final ArrayList<String> serviceVal = new ArrayList<>();
//        serviceVal.add("Landline");
//        serviceVal.add("Electricity");
//        serviceVal.add("Gas");
//        serviceVal.add("Insurance");
//        serviceVal.add("Loan");
//        serviceVal.add("Internet");
//        serviceVal.add("Credit Cards");
//        serviceVal.add("Cable");
//        serviceVal.add("Television");
//        serviceVal.add("Water");
//        serviceVal.add("Mortgage");
//        serviceVal.add("Satellite");
//        serviceVal.add("Utilities");
//
//        android.support.v7.app.AlertDialog.Builder currencyDialog =
//                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
//        currencyDialog.setTitle("Select your Service");
//
//        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//
//        currencyDialog.setItems(serviceVal.toArray(new String[serviceVal.size()]),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface arg0, int i) {
//                        etBillPayCountry.getText().clear();
//                        etBillPayOperator.getText().clear();
//                        etBillPayServiceType.setText(serviceVal.get(i));
//                    }
//                });
//
//        currencyDialog.show();
//    }
//
//
//    public void parseOperationData() {
//        String mainJson = loadJSONFromAsset();
//        try {
//            JSONObject mainObj = new JSONObject(mainJson);
//            JSONArray billArray = mainObj.getJSONArray("billers");
//            serviceTypeArray.clear();
//            BillPayOperatorModel.deleteAll(BillPayOperatorModel.class);
//
//            for (int i = 0; i < billArray.length(); i++) {
//                JSONObject c = billArray.getJSONObject(i);
//                String serviceId = c.getString("id");
//                String servicename = c.getString("biller_type");
//                String servicecountry = c.getString("country");
//                String billtype = c.getString("bill_type");
//                String servicecurrency = c.getString("currency");
//                String operatorname = c.getString("name");
//                BillPayOperatorModel billPayOperatorModel = new BillPayOperatorModel(serviceId, servicename, servicecountry, billtype, servicecurrency, operatorname);
//                billPayOperatorModel.save();
//                serviceTypeArray.add(billPayOperatorModel);
//
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    public String loadJSONFromAsset() {
//        String json;
//        try {
//            InputStream is = getActivity().getAssets().open("bill.json");
//            int size = is.available();
//            byte[] buffer = new byte[size];
//            is.read(buffer);
//            is.close();
//            json = new String(buffer, "UTF-8");
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            return null;
//        }
//        return json;
//    }
//
//    private class GetOperator extends AsyncTask<Void, Void, Void> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            pbBillPayment.setVisibility(View.VISIBLE);
//            svBillPayment.setVisibility(View.GONE);
//
//        }
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//            serviceTypeArray = Select.from(BillPayOperatorModel.class).list();
//            parseOperationData();
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            super.onPostExecute(aVoid);
//            ((PayQwikApp) getActivity().getApplication()).storeData(serviceTypeArray);
//            afterTaskComplete();
//
//        }
//    }
//
//    private void afterTaskComplete() {
//        pbBillPayment.setVisibility(View.GONE);
//        svBillPayment.setVisibility(View.VISIBLE);
//        etBillPayServiceType.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showServiceTypeDialog();
//            }
//        });
//
//        etBillPayCountry.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!etBillPayServiceType.getText().toString().isEmpty()) {
//                    showCountryDialog();
//                } else {
//                    CustomToast.showMessage(getActivity(), "Select your service type first");
//                }
//            }
//        });
//
//        etBillPayOperator.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!etBillPayServiceType.getText().toString().isEmpty()&& !etBillPayCountry.getText().toString().isEmpty()) {
//                    showOperatorDialog();
//                } else {
//                    CustomToast.showMessage(getActivity(), "Select your service type and country first");
//                }
//            }
//        });
//
//        btnBillPay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                attemptPay();
//            }
//        });
//    }
//
//    private void attemptPay() {
//        etBillPayCountry.setError(null);
//        etBillPayOperator.setError(null);
//        etBillPayServiceType.setError(null);
//        etBillPayAmount.setError(null);
//        etBillPayServiceNo.setError(null);
//        cancel = false;
//        checkServiceType(etBillPayServiceType.getText().toString());
//        checkCountry(etBillPayCountry.getText().toString());
//        checkProvider(etBillPayOperator.getText().toString());
//        checkServiceNo(etBillPayServiceNo.getText().toString());
//        checkAmount(etBillPayAmount.getText().toString());
//        if (cancel) {
//            focusView.requestFocus();
//        } else {
//            showPayDialog();
//        }
//    }
//
//    private void checkCountry(String amount) {
//        CheckLog countryCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        if (!countryCheckLog.isValid) {
//            etBillPayCountry.setError(getString(countryCheckLog.msg));
//            focusView = etBillPayCountry;
//            cancel = true;
//        }
//    }
//
//    private void checkProvider(String amount) {
//        CheckLog operatorCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        if (!operatorCheckLog.isValid) {
//            etBillPayOperator.setError(getString(operatorCheckLog.msg));
//            focusView = etBillPayOperator;
//            cancel = true;
//        }
//    }
//
//    private void checkServiceType(String phNo) {
//        CheckLog phoneCheckLog = PayingDetailsValidation.checkGasCustomerAc(phNo);
//        if (!phoneCheckLog.isValid) {
//            etBillPayServiceType.setError(getString(phoneCheckLog.msg));
//            focusView = etBillPayServiceType;
//            cancel = true;
//        }
//    }
//
//    private void checkAmount(String amount) {
//        CheckLog amountCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        if (!amountCheckLog.isValid) {
//            etBillPayAmount.setError(getString(amountCheckLog.msg));
//            focusView = etBillPayAmount;
//            cancel = true;
//        }
//    }
//
//    private void checkServiceNo(String amount) {
//        CheckLog amountCheckLog = PayingDetailsValidation.checkGasCustomerAc(amount);
//        if (!amountCheckLog.isValid) {
//            etBillPayServiceNo.setError(getString(amountCheckLog.msg));
//            focusView = etBillPayServiceNo;
//            cancel = true;
//        }
//    }
//
//    public void showPayDialog() {
//        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title, Html.fromHtml(generateMessage()));
//        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//                promotePayBill("");
//            }
//        });
//        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//        builder.show();
//    }
//
//    private Map<String, String> generateParams(String type) {
//        Map<String, String> params = new HashMap<>();
//        switch (type) {
//            case KEY_DTH:
//                params.put("country_dth", selectedCountryCode);
//                params.put("Dth_currency", selectedCurrencyCode);
//                params.put("Dth_current_id", selectedCurrentId);
//
//                //Taken from edit text
//                params.put("dth_provider", etBillPayOperator.getText().toString());
//                params.put("dthNo", etBillPayServiceNo.getText().toString());
//                params.put("Dth_Amount", etBillPayAmount.getText().toString());
//                params.put("username", session.getUserEmail());
//                params.put("sessionId", session.getUserSessionId());
//                break;
//            case KEY_ELECTRICITY:
//                params.put("country_dth", selectedCountryCode);
//                params.put("Dth_currency", selectedCurrencyCode);
//                params.put("Dth_current_id", selectedCurrentId);
//
//                //Taken from edit text
//                params.put("dth_provider", etBillPayOperator.getText().toString());
//                params.put("dthNo", etBillPayServiceNo.getText().toString());
//                params.put("Dth_Amount", etBillPayAmount.getText().toString());
//                params.put("username", session.getUserEmail());
//                params.put("sessionId", session.getUserSessionId());
//                break;
//            case KEY_LAND_LINE:
//                params.put("country_dth", selectedCountryCode);
//                params.put("Dth_currency", selectedCurrencyCode);
//                params.put("Dth_current_id", selectedCurrentId);
//
//                //Taken from edit text
//                params.put("dth_provider", etBillPayOperator.getText().toString());
//                params.put("dthNo", etBillPayServiceNo.getText().toString());
//                params.put("Dth_Amount", etBillPayAmount.getText().toString());
//                params.put("username", session.getUserEmail());
//                params.put("sessionId", session.getUserSessionId());
//                break;
//            default:
//                break;
//        }
//        return params;
//
//    }
//
//    private String generateURL(String type) {
//        String url;
//        switch (type) {
//            case KEY_DTH:
//                url = ApiURLNew.URL_BILL_PAY_DTH;
//                break;
//            case KEY_ELECTRICITY:
//                url = ApiURLNew.URL_BILL_PAY_ELECTRICITY;
//                break;
//            case KEY_LAND_LINE:
//                url = ApiURLNew.URL_BILL_PAY_LAND_LINE;
//                break;
//            default:
//                url = "N/A";
//                break;
//        }
//        return url;
//    }
//
//    public String generateMessage() {
//        String source =
//                "<b><font color=#000000> Service Type: </font></b>" + "<font color=#000000>" + etBillPayServiceType.getText().toString() + "</font><br>" +
//                        "<b><font color=#000000> Service Provide: </font></b>" + "<font color=#000000>" + etBillPayOperator.getText().toString() + "</font><br>" +
//                        "<b><font color=#000000> Country: </font></b>" + "<font>" + etBillPayCountry.getText().toString() + "</font><br>" +
//                        "<b><font color=#000000> Amount: </font></b>" + "<font>" + etBillPayAmount.getText().toString() + "</font><br><br>" +
//                        "<b><font color=#ff0000> Are you sure you want to proceed?</font></b><br>";
//        return source;
//    }
//
//
//    public void promotePayBill(final String type) {
//        loadingDialog.show();
//        StringRequest postReq = new StringRequest(Request.Method.POST, generateURL(type), new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                return generateParams(type);
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> map = new HashMap<>();
//                map.put("hash", "123");
//                return map;
//            }
//        };
//
//        int socketTimeout = 60000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        postReq.setRetryPolicy(policy);
//        PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if (getOperatorTask != null) {
//            if (!getOperatorTask.isCancelled()) {
//                getOperatorTask.cancel(true);
//                getOperatorTask = null;
//            }
//        }
//    }
//
//}
//
//
//
