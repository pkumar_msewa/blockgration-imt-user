package com.Blockgration.fragment.mainmenufragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Blockgration.Userwallet.activity.MainMenuDetailActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidquery.AQuery;
import com.Blockgration.adapter.HomeMenuAdapter;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.loadmoney.LoadMoneyActivity;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.metadata.MenuMetadata;
import com.Blockgration.model.AccountModel;
import com.Blockgration.model.CountryModel;
import com.Blockgration.model.CurrentAccountModel;
import com.Blockgration.model.MobileRechargeCountryModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.ColorUtil;
import com.orm.query.Select;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ksf on 7/14/2016.
 */
public class HomeFragment extends Fragment {
    private View rootView;
    private RecyclerView rvHome;
    private Button btnUserCurrency;
    private TextView tvUserBalance;
    private LinearLayout llHome;
    private UserModel session = UserModel.getInstance();
//    private AccountModel account = AccountModel.getInstance();

    ArrayList<String> currencyVal;

    //Add Currency
    private String currency_name = "", currency_symbol = "", currency_ISO3 = "", country_ISO2 = "";
    private String country_ISO3 = "", country_name = "";


    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    int[] Images = {R.drawable.creditcard_1, R.drawable.creditcard_2, R.drawable.creditcard_3};
    private ImageView ivPrv, ivCenter, ivNext;
    private int currentImage = 0;
    private TabLayout tabLayout;


//    private RequestQueue rq;

    //Reward Points Shared Preference.
    public static final String POINTS = "points";
    SharedPreferences pointPreferences;
    private int rewardPoints;
    private List<CountryModel> currencyArray;
    private List<AccountModel> accountArray;
    private ArrayList<MobileRechargeCountryModel> currency_Array;
    private List<CurrentAccountModel> currentAccountArray;

    //Volley Tag
    private String tag_json_obj = "json_obj_req";


    //Account
    private String accBalance;
    private String accNo;
    private String balanceLimit;
    private String accType;
    private String dailyLimit;
    private String accCountry;
    private String monthlyLimit;
    private String currencyName;
    private ViewPager viewPager;
    private String currencyCode;
    private String currencySymbol;
    private Button btnvaps, btnvabp;
    private ImageView ividea, ivairtel, ivvoda, ivjio, ivtata, ivdishtv, ividea1, ivvoda1, ivjio1, ivairtel1, ivbsnl1;

    private String userEmailStatus = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pointPreferences = getActivity().getSharedPreferences(POINTS, Context.MODE_PRIVATE);
        currencyArray = Select.from(CountryModel.class).list();
        accountArray = Select.from(AccountModel.class).list();
        currentAccountArray = Select.from(CurrentAccountModel.class).list();
        loadDlg = new LoadingDialog(getActivity());

    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        rvHome = (RecyclerView) rootView.findViewById(R.id.rvHome);
        llHome = (LinearLayout) rootView.findViewById(R.id.llHome);
        if (!PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("preSwitchTheme", false)) {
            llHome.setBackgroundColor(ColorUtil.getColor(getActivity(), R.color.white_text));
        } else {
            llHome.setBackgroundColor(ColorUtil.getColor(getActivity(), R.color.dark_grey));
        }

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.recycle_spacing);
        rvHome.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        GridLayoutManager manager = new GridLayoutManager(getActivity(), 3);
        rvHome.setLayoutManager(manager);
        rvHome.setHasFixedSize(true);
        HomeMenuAdapter rcAdapter = new HomeMenuAdapter(getActivity(), MenuMetadata.getMenu(getActivity()));
        rvHome.setAdapter(rcAdapter);


        rewardPoints = pointPreferences.getInt("reward", 0);

        ivCenter = (ImageView) rootView.findViewById(R.id.ivCenter);
        ivPrv = (ImageView) rootView.findViewById(R.id.ivPrv);
        ivNext = (ImageView) rootView.findViewById(R.id.ivNext);
        tvUserBalance = (TextView) rootView.findViewById(R.id.tvUserBalance);
        btnUserCurrency = (Button) rootView.findViewById(R.id.tvUserCurrency);
        Button btnUserLoadMoney = (Button) rootView.findViewById(R.id.tvUserLoadMoney);
        btnvaps = (Button) rootView.findViewById(R.id.btnvaps);
        btnvabp = (Button) rootView.findViewById(R.id.btnvabp);
        ividea = (ImageView) rootView.findViewById(R.id.ividea);
        ivvoda = (ImageView) rootView.findViewById(R.id.ivvoda);
        ivjio = (ImageView) rootView.findViewById(R.id.ivjio);
        ivairtel = (ImageView) rootView.findViewById(R.id.ivairtel);
        ivtata = (ImageView) rootView.findViewById(R.id.ivtata);
        ivdishtv = (ImageView) rootView.findViewById(R.id.ivdishtv);

        ividea1 = (ImageView) rootView.findViewById(R.id.ividea1);
        ivvoda1 = (ImageView) rootView.findViewById(R.id.ivvoda1);
        ivjio1 = (ImageView) rootView.findViewById(R.id.ivjio1);
        ivairtel1 = (ImageView) rootView.findViewById(R.id.ivairtel1);
        ivbsnl1 = (ImageView) rootView.findViewById(R.id.ivbsnl1);

        LinearLayout llBalanceDetails = (LinearLayout) rootView.findViewById(R.id.llBalanceDetails);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(screenWidth, screenWidth - (screenWidth / 3));


        if (session != null && session.getIsValid() == 1) {
            try {
                if (currentAccountArray.size() != 0) {
                    if (!currentAccountArray.get(0).getCurrencySymbol().isEmpty()) {
                        tvUserBalance.setText(getResources().getString(R.string.user_balance) + "\n" + currentAccountArray.get(0).getCurrencySymbol() + " " + currentAccountArray.get(0).getAccBalance());
                    } else {
                        tvUserBalance.setText(getResources().getString(R.string.user_balance) + "\n" + currentAccountArray.get(0).getCurrencyCode() + " " + currentAccountArray.get(0).getAccBalance());
                    }


                    btnUserCurrency.setText(getResources().getString(R.string.user_currency) + "\n" + currentAccountArray.get(0).getCurrencyCode());
                }


                AQuery aq = new AQuery(getActivity());


            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {
            sendLogout();

        }


        btnUserLoadMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), LoadMoneyActivity.class);
                startActivity(intent);
            }
        });

        btnUserCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCurrencyDialog();

            }
        });

        ivPrv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentImage++;
                ivPrv.setVisibility(View.VISIBLE);

                Animation slide_left_in = AnimationUtils.loadAnimation(getActivity(), R.anim.left_to_right_animation);
                ivCenter.startAnimation(slide_left_in);
                currentImage = currentImage % Images.length;
                ivCenter.setImageResource(Images[currentImage]);
            }
        });
        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentImage++;
                ivNext.setVisibility(View.VISIBLE);

                Animation slide_right_in = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left_animation);
                ivCenter.startAnimation(slide_right_in);
                currentImage = currentImage % Images.length;
                ivCenter.setImageResource(Images[currentImage]);
            }
        });
        viewPager = (ViewPager) rootView.findViewById(R.id.pagerHome);
        tabLayout = (TabLayout) rootView.findViewById(R.id.nav_tablayout);
        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.menu2)).setIcon(R.drawable.menu_new_pay_bills));
//        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.Recharge)).setIcon(R.drawable.ic_recharge));
        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.menu3)).setIcon(R.drawable.menu_ft));
//        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.menu23)).setIcon(R.drawable.menu_new_travel));

        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.menu19)).setIcon(R.drawable.menu_sm));
        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.menu8)).setIcon(R.drawable.menu_new_qr_scan));
//        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.Bharat_QR)).setIcon(R.drawable.ic_bharat_visa_01));
//        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.Gift_Card)).setIcon(R.drawable.ic_gift_cards));
        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.menu13)).setIcon(R.drawable.menu_new_entertainment));
        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.menu15)).setIcon(R.drawable.menu_new_e_gov));
//        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.menu21)).setIcon(R.drawable.menu_new_shopping_new));
//        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.menu22)).setIcon(R.drawable.menu_new_gift));
        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.menu24)).setIcon(R.drawable.menu_new_chat_new));
        tabLayout.addTab(tabLayout.newTab().setText(this.getResources().getString(R.string.menu21)).setIcon(R.drawable.menu_new_shopping));


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "BillPayment"));
                } else if (tab.getPosition() == 1) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "FundTransfer"));
                }
//                else if (tab.getPosition() == 2) {
//                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "Travel"));
//                }
                else if (tab.getPosition() == 2) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "sendmoney"));
                } else if (tab.getPosition() == 3) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "QRScan"));
                } else if (tab.getPosition() == 4) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "Entertainment"));
                } else if (tab.getPosition() == 5) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "EGovermment"));
                } else if (tab.getPosition() == 7) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "Shopping"));

//                } else if (tab.getPosition() == 7) {
//                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "Gift Cards"));
                } else if (tab.getPosition() == 6) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "ChatBot"));
                }
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "BillPayment"));
                } else if (tab.getPosition() == 1) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "FundTransfer"));
                }
//                else if (tab.getPosition() == 2) {
//                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "Travel"));
//                }
                else if (tab.getPosition() == 2) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "sendmoney"));
                } else if (tab.getPosition() == 3) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "QRScan"));
                } else if (tab.getPosition() == 4) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "Entertainment"));
                } else if (tab.getPosition() == 5) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "EGovermment"));
//                } else if (tab.getPosition() == 6) {
//                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "Shopping"));
//                } else if (tab.getPosition() == 7) {
//                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "Gift Cards"));
                } else if (tab.getPosition() == 6) {
                    startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "ChatBot"));
                }
            }
        });
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnvaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "BillPayment"));
            }
        });
        btnvabp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "sendmoney"));
            }
        });
        ivvoda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "BillPayment"));
            }
        });
        ivjio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "BillPayment"));
            }
        });
        ivairtel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "BillPayment"));
            }
        });

        ividea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "BillPayment"));
            }
        });
        ivdishtv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "sendmoney"));
            }
        });

        ivtata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "sendmoney"));
            }
        });


        ividea1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "ChatBot"));
            }
        });
        ivvoda1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "Entertainment"));
            }
        });

        ivjio1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "FundTransfer"));
            }
        });
        ivairtel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "BillPayment"));
            }
        });

        ivbsnl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainMenuDetailActivity.class).putExtra(AppMetadata.FRAGMENT_TYPE, "sendmoney"));
            }
        });


        return rootView;
    }

    private Bitmap decodeFromBase64ToBitmap(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showCurrencyDialog() {

        currencyVal = new ArrayList<>();

        currency_Array = AppMetadata.getCurrencyNew();

        for (MobileRechargeCountryModel currencyModel : currency_Array) {
            String[] name = currencyModel.getCountryName().split(",");
            currencyVal.add(name[0]);
        }

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle(getResources().getString(R.string.user_currency));


        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });


        currencyDialog.setItems(currencyVal.toArray(new String[currencyVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
//                        Log.i("Account number", accountArray.get(i).getAccCountry());
                        String[] new_name = currency_Array.get(i).getCountryCode().split(",");

                        btnUserCurrency.setText(getResources().getString(R.string.user_currency) + "\n" + new_name[0]);
//                        String symbol1 = currency_Array.get(i).getCountryCode();
//                        String symbol2 = symbol1.substring(symbol1.lastIndexOf(","+1),symbol1.length());
//                        String symbol3 = symbol1.substring(symbol1.indexOf(","+1),symbol1.lastIndexOf(","));
                        Log.i("latestSymbol", new_name[1]);
                        String currenbt = currentAccountArray.get(0).getAccCountry();
                        if (currencyVal.get(i).equalsIgnoreCase(currenbt)) {
                            tvUserBalance.setText(getResources().getString(R.string.user_balance) + "\n" + new_name[1] + " " + currentAccountArray.get(0).getAccBalance());
                        } else {
                            tvUserBalance.setText(getResources().getString(R.string.user_balance) + "\n" + new_name[1] + " " + currentAccountArray.get(0).getAccBalance());
                        }
//                        switchAccount(accountArray.get(i).getAccCountry(), accountArray.get(i).getAccNo());
                        CustomToast.showMessage(getActivity(), "Account switched successfully");
                    }
                });

        currencyDialog.show();
    }


    public void showAddCurrencyListDialog() {
        ArrayList<String> currencyVal = new ArrayList<>();
        for (CountryModel currencyModel : currencyArray) {
            currencyVal.add(currencyModel.getIsoCode() + ", " + currencyModel.getCountryName());
        }

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle(getResources().getString(R.string.country_request));

        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });


        currencyDialog.setItems(currencyVal.toArray(new String[currencyVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        currency_name = currencyArray.get(i).getCurrencyName();
                        currency_symbol = currencyArray.get(i).getCurrencySymbol();
                        currency_ISO3 = currencyArray.get(i).getCurrencyCode();
                        country_ISO2 = currencyArray.get(i).getCountryCode();
                        country_ISO3 = currencyArray.get(i).getIsoCode();
                        country_name = currencyArray.get(i).getCountryName();
                        promoteToAddCurrency();
                    }
                });

        currencyDialog.show();
    }


    public void promoteToAddCurrency() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("username", session.getUserEmail());
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("currency_name", currency_name);
            jsonRequest.put("currency_symbol", currency_symbol);
            jsonRequest.put("currency_ISO3", currency_ISO3);
            jsonRequest.put("country_ISO2", country_ISO2);
            jsonRequest.put("country_ISO3", country_ISO3);
            jsonRequest.put("country_name", country_name);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("URL", ApiURLNew.URL_ADD_CURRENCY);
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_ADD_CURRENCY, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Add Currency RESPONSE", response.toString());
                        String code = response.getString("code");
                        String message = "";
                        if (response.has("message")) {
                            message = response.getString("message");
                        }
                        if (code != null && code.equals("S00")) {
                            String reponse = response.getString("response");
                            JSONObject jsonObject = new JSONObject(reponse);
                            JSONObject jsonDetail = jsonObject.getJSONObject("details");
                            AccountModel.deleteAll(AccountModel.class);
//                            JSONArray accArray = jsonDetail.getJSONArray("accountDetail");
                            Object o = jsonDetail.get("accountDetail");
                            if (o instanceof JSONObject) {


                                accNo = ((JSONObject) o).getString("accountNumber");
                                accBalance = ((JSONObject) o).getString("balance");

                                JSONObject jType = ((JSONObject) o).getJSONObject("type");
                                balanceLimit = jType.getString("balanceLimit");
                                accType = jType.getString("code");
                                dailyLimit = jType.getString("dailyLimit");
                                monthlyLimit = jType.getString("monthlyLimit");

                                JSONObject jCountry = ((JSONObject) o).getJSONObject("country");
                                accCountry = jCountry.getString("countryName");

                                JSONObject jCurrency = ((JSONObject) o).getJSONObject("currency");
                                currencyName = jCurrency.getString("currencyName");
                                currencyCode = jCurrency.getString("currencyISO3");
                                currencySymbol = jCurrency.getString("currencySymbol");


                                AccountModel accountModel = new AccountModel(accBalance, accNo, balanceLimit, accType, dailyLimit, accCountry
                                        , monthlyLimit, currencyName, currencyCode, currencySymbol);
                                accountModel.save();

                            } else if (o instanceof JSONArray) {
                                JSONArray accArray = jsonDetail.getJSONArray("accountDetail");

                                for (int i = 0; i < accArray.length(); i++) {
                                    JSONObject c = accArray.getJSONObject(i);

                                    accNo = c.getString("accountNumber");
                                    accBalance = c.getString("balance");

                                    JSONObject jType = c.getJSONObject("type");
                                    balanceLimit = jType.getString("balanceLimit");
                                    accType = jType.getString("code");
                                    dailyLimit = jType.getString("dailyLimit");
                                    monthlyLimit = jType.getString("monthlyLimit");

                                    JSONObject jCountry = c.getJSONObject("country");
                                    accCountry = jCountry.getString("countryName");

                                    JSONObject jCurrency = c.getJSONObject("currency");
                                    currencyName = jCurrency.getString("currencyName");
                                    currencyCode = jCurrency.getString("currencyISO3");
                                    currencySymbol = jCurrency.getString("currencySymbol");


                                    AccountModel accountModel = new AccountModel(accBalance, accNo, balanceLimit, accType, dailyLimit, accCountry
                                            , monthlyLimit, currencyName, currencyCode, currencySymbol);
                                    accountModel.save();
                                }
                            }

                            String userSessionId = jsonDetail.getString("sessionId");

                            //Active Account
                            JSONObject jsonCurrentAcc = jsonDetail.getJSONObject("activeAccount");
                            CurrentAccountModel.deleteAll(CurrentAccountModel.class);

                            String currentAccNo = jsonCurrentAcc.getString("accountNumber");
                            String currentAccBalance = jsonCurrentAcc.getString("balance");

                            JSONObject jCurrentType = jsonCurrentAcc.getJSONObject("type");
                            String currentBalanceLimit = jCurrentType.getString("balanceLimit");
                            String currentAccType = jCurrentType.getString("code");
                            String currentDailyLimit = jCurrentType.getString("dailyLimit");
                            String currentMonthlyLimit = jCurrentType.getString("monthlyLimit");

                            JSONObject jCurrentCountry = jsonCurrentAcc.getJSONObject("country");
                            String currentAccCountry = jCurrentCountry.getString("countryName");

                            JSONObject jCurrentCurrency = jsonCurrentAcc.getJSONObject("currency");
                            String currentCurrencyName = jCurrentCurrency.getString("currencyName");
                            String currentCurrencyCode = jCurrentCurrency.getString("currencyISO3");
                            String currentCurrencySymbol = jCurrentCurrency.getString("currencySymbol");

                            CurrentAccountModel currentAccountModel = new CurrentAccountModel(currentAccBalance, currentAccNo, currentBalanceLimit, currentAccType, currentDailyLimit, currentAccCountry
                                    , currentMonthlyLimit, currentCurrencyName, currentCurrencyCode, currentCurrencySymbol);
                            currentAccountModel.save();

                            JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");
                            String userFName = jsonUserDetail.getString("firstName");
                            String userLName = jsonUserDetail.getString("lastName");
                            String userMobile = jsonUserDetail.getString("contactNo");
                            String userEmail = jsonUserDetail.getString("email");
                            String userAddress = jsonUserDetail.getString("address");
                            String userImage = jsonUserDetail.getString("image");
                            userEmailStatus = jsonUserDetail.getString("emailStatus");


                            boolean isMPin = false;
                            String userDob = jsonUserDetail.getString("dateOfBirth");
                            String userGender = jsonUserDetail.getString("gender");

                            UserModel.deleteAll(UserModel.class);
                            UserModel userModel = new UserModel(userSessionId, userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAddress, userImage, userDob, userGender, isMPin);
                            userModel.save();
                            CustomToast.showMessage(getActivity(), accCountry);
                            session.setUserSessionId(userSessionId);
                            session.setUserFirstName(userFName);
                            session.setUserLastName(userLName);
                            session.setUserMobileNo(userMobile);
                            session.setUserEmail(userEmail);
                            session.setEmailIsActive(userEmailStatus);
                            session.setUserImage(userImage);
                            session.setUserAddress(userAddress);
                            session.setUserGender(userGender);
                            session.setUserDob(userDob);
                            session.setMPin(isMPin);
                            session.setIsValid(1);


                            Handler mHandler = new Handler();
                            mHandler.postDelayed(refreshTask, 200);


                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            loadDlg.dismiss();
                        }


                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    Toast.makeText(getActivity(), "Error connecting to server", Toast.LENGTH_SHORT).show();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "112");
                    return map;
                }

            };
            int socketTimeout = 60000;

            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
//            rq.add(postReq);
        }


    }


    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

    public void switchAccount(final String country, String switchAccNo) {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("country", country);
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("username", session.getUserEmail());
            jsonRequest.put("accountNumber", switchAccNo);
        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            System.out.println("Switch acc request ::" + jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SWITCH_ACCOUNT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.i("Switch Account Response", response.toString());
                        String code = response.getString("code");
                        String message = response.getString("message");
                        if (code != null && code.equals("S00")) {
                            System.out.println("valuees" + response);
                            CustomToast.showMessage(getActivity(), message);
                            String rep = response.getString("response");
                            JSONObject jsonObject = new JSONObject(rep);
                            JSONObject jsonDetail = jsonObject.getJSONObject("details");
                            AccountModel.deleteAll(AccountModel.class);
                            JSONArray accArray = jsonDetail.getJSONArray("accountDetail");
                            String userSessionId = jsonDetail.getString("sessionId");

                            //Active Account
                            JSONObject jsonCurrentAcc = jsonDetail.getJSONObject("activeAccount");
                            CurrentAccountModel.deleteAll(CurrentAccountModel.class);

                            String currentAccNo = jsonCurrentAcc.getString("accountNumber");
                            String currentAccBalance = jsonCurrentAcc.getString("balance");

                            JSONObject jCurrentType = jsonCurrentAcc.getJSONObject("type");
                            String currentBalanceLimit = jCurrentType.getString("balanceLimit");
                            String currentAccType = jCurrentType.getString("code");
                            String currentDailyLimit = jCurrentType.getString("dailyLimit");
                            String currentMonthlyLimit = jCurrentType.getString("monthlyLimit");

                            JSONObject jCurrentCountry = jsonCurrentAcc.getJSONObject("country");
                            String currentAccCountry = jCurrentCountry.getString("countryName");

                            JSONObject jCurrentCurrency = jsonCurrentAcc.getJSONObject("currency");
                            String currentCurrencyName = jCurrentCurrency.getString("currencyName");
                            String currentCurrencyCode = jCurrentCurrency.getString("currencyISO3");
                            String currentCurrencySymbol = jCurrentCurrency.getString("currencySymbol");

                            CurrentAccountModel currentAccountModel = new CurrentAccountModel(currentAccBalance, currentAccNo, currentBalanceLimit, currentAccType, currentDailyLimit, currentAccCountry
                                    , currentMonthlyLimit, currentCurrencyName, currentCurrencyCode, currentCurrencySymbol);
                            currentAccountModel.save();

                            for (int i = 0; i < accArray.length(); i++) {
                                JSONObject c = accArray.getJSONObject(i);

                                accNo = c.getString("accountNumber");
                                accBalance = c.getString("balance");

                                JSONObject jType = c.getJSONObject("type");
                                balanceLimit = jType.getString("balanceLimit");
                                accType = jType.getString("code");
                                dailyLimit = jType.getString("dailyLimit");
                                monthlyLimit = jType.getString("monthlyLimit");

                                JSONObject jCountry = c.getJSONObject("country");
                                accCountry = jCountry.getString("countryName");

                                JSONObject jCurrency = c.getJSONObject("currency");
                                currencyName = jCurrency.getString("currencyName");
                                currencyCode = jCurrency.getString("currencyISO3");
                                currencySymbol = jCurrency.getString("currencySymbol");

                                AccountModel accountModel = new AccountModel(accBalance, accNo, balanceLimit, accType, dailyLimit, accCountry
                                        , monthlyLimit, currencyName, currencyCode, currencySymbol);
                                accountModel.save();
                            }

                            JSONObject jsonUserDetail = jsonDetail.getJSONObject("userDetail");
                            String userFName = jsonUserDetail.getString("firstName");
                            String userLName = jsonUserDetail.getString("lastName");
                            String userMobile = jsonUserDetail.getString("contactNo");
                            String userEmail = jsonUserDetail.getString("email");
                            String userAddress = jsonUserDetail.getString("address");
                            String userImage = jsonUserDetail.getString("image");
                            userEmailStatus = jsonUserDetail.getString("emailStatus");


                            boolean isMPin = false;
                            String userDob = jsonUserDetail.getString("dateOfBirth");
                            String userGender = jsonUserDetail.getString("gender");

                            UserModel.deleteAll(UserModel.class);
                            UserModel userModel = new UserModel(userSessionId, userFName, userLName, userMobile, userEmail, userEmailStatus, 1, userAddress, userImage, userDob, userGender, isMPin);
                            userModel.save();

                            session.setUserSessionId(userSessionId);
                            session.setUserFirstName(userFName);
                            session.setUserLastName(userLName);
                            session.setUserMobileNo(userMobile);
                            session.setUserEmail(userEmail);
                            session.setEmailIsActive(userEmailStatus);
                            session.setUserImage(userImage);
                            session.setUserAddress(userAddress);
                            session.setUserGender(userGender);
                            session.setUserDob(userDob);
                            session.setMPin(isMPin);
                            session.setIsValid(1);


                            Handler mHandler = new Handler();
                            mHandler.postDelayed(refreshTask, 200);
                        } else if (code != null && code.equals("F03")) {
                            CustomToast.showMessage(getActivity(), message);
                            loadDlg.dismiss();
                        } else {
                            CustomToast.showMessage(getActivity(), message);
                            loadDlg.dismiss();
                        }


                    } catch (JSONException e) {
                        loadDlg.dismiss();

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    Toast.makeText(getActivity(), "Error connecting to server", Toast.LENGTH_SHORT).show();
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", "112");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    private Runnable refreshTask = new Runnable() {
        @Override
        public void run() {
            sendRefresh();
        }
    };


    public void sendRefresh() {
        loadDlg.dismiss();
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "3");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}


