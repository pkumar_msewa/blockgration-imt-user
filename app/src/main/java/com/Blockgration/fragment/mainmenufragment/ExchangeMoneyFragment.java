package com.Blockgration.fragment.mainmenufragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.AccountModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.NetworkErrorHandler;
import com.Blockgration.util.SecurityUtil;
import com.orm.query.Select;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ksf on 8/3/2016.
 */
public class ExchangeMoneyFragment extends Fragment {
    private View rootView;
    private MaterialEditText etExchangeCodeFrom, etExchangeCodeTo, etExchangeAmount;
    private Button btnExchangeMoney;
    //Volley Tag
    private String tag_json_obj = "json_obj_req";

    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    private String countryAccFrom = null, countryACCTo = null;
    private UserModel session = UserModel.getInstance();
    private List<AccountModel> accArray;
    //Variables
    private String currencyName = null, currencySymbol = null, currencyISO = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());
        accArray = Select.from(AccountModel.class).list();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_exchange_money, container, false);
        etExchangeCodeFrom = (MaterialEditText) rootView.findViewById(R.id.etExchangeCodeFrom);
        etExchangeCodeTo = (MaterialEditText) rootView.findViewById(R.id.etExchangeCodeTo);
        etExchangeAmount = (MaterialEditText) rootView.findViewById(R.id.etExchangeAmount);
        btnExchangeMoney = (Button) rootView.findViewById(R.id.btnExchangeMoney);

        etExchangeCodeTo.setFocusable(false);
        etExchangeCodeFrom.setFocusable(false);
        etExchangeCodeFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (accArray.size() != 0) {
                    showFromCurrencyDialog();
                } else {
                    CustomToast.showMessage(getActivity(), "Please add account to exchange.");
                }

            }
        });

        etExchangeCodeTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showToCurrencyDialog();
            }
        });

        btnExchangeMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exchangeMoneyNow();
            }
        });

        return rootView;
    }


    public void exchangeMoneyNow() {
        loadDlg.show();
        jsonRequest = new JSONObject();
        try {
//            jsonRequest.put("fromAccount", etExchangeCodeFrom.getText().toString());
//            jsonRequest.put("toAccount", etExchangeCodeTo.getText().toString());
            jsonRequest.put("fromAccount", countryAccFrom);
            jsonRequest.put("toAccount", countryACCTo);
            jsonRequest.put("amount", etExchangeAmount.getText().toString());
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("username", session.getUserEmail());
            jsonRequest.put("currency_name", currencyName);
            jsonRequest.put("currency_symbol", currencySymbol);
            jsonRequest.put("currency_ISO3", currencyISO);


        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_EXCHANGE_CURRENCY, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("Exchange Money", response.toString());
                    try {
                        String code = response.getString("code");
                        if (code != null && code.equals("S00")) {
                            String sucessMessage = response.getString("message");
                            etExchangeAmount.getText().clear();
                            etExchangeCodeFrom.getText().clear();
                            etExchangeCodeTo.getText().clear();
                            loadDlg.dismiss();
                            sendRefresh();
                            CustomToast.showMessage(getActivity(), sucessMessage);
                        } else if (code != null && code.equals("F03")) {
                            loadDlg.dismiss();
                            showInvalidSessionDialog();
                        } else {
                            loadDlg.dismiss();
//                            String jsonString = response.getString("response");
//                            JSONObject jsonObject = new JSONObject(jsonString);
                            String errorMessage = response.getString("message");
                            CustomToast.showMessage(getActivity(), errorMessage);
                        }

                    } catch (JSONException e) {
                        loadDlg.dismiss();
                        CustomToast.showMessage(getActivity(), getActivity().getResources().getString(R.string.server_exception2));

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loadDlg.dismiss();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    error.printStackTrace();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("hash", SecurityUtil.getSecurityKey(jsonRequest.toString()));
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    public void showFromCurrencyDialog() {

        ArrayList<String> currencyVal = new ArrayList<>();

        for (AccountModel accModel : accArray) {
            currencyVal.add(accModel.getAccCountry());
        }

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select your Account");

        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        currencyDialog.setItems(currencyVal.toArray(new String[currencyVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etExchangeCodeFrom.setText(accArray.get(i).getCurrencyCode());
//                        countryCodeTo = accArray.get(i).getCurrencyCode();
                        countryAccFrom = accArray.get(i).getAccNo();
//                        currencyName = accArray.get(i).getCurrencyName();
//                        currencySymbol = accArray.get(i).getCurrencySymbol();
//                        currencyISO = accArray.get(i).getCurrencyCode();
                    }
                });

        currencyDialog.show();
    }

    public void showToCurrencyDialog() {
        ArrayList<String> currencyVal = new ArrayList<>();

        for (AccountModel accModel : accArray) {
            currencyVal.add(accModel.getAccCountry());
        }

        final android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select your Account");
        currencyDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        currencyDialog.setItems(currencyVal.toArray(new String[currencyVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        etExchangeCodeTo.setText(accArray.get(i).getCurrencyCode());
                        currencyName = accArray.get(i).getCurrencyName();
                        countryACCTo = accArray.get(i).getAccNo();
                        currencySymbol = accArray.get(i).getCurrencySymbol();
                        currencyISO = accArray.get(i).getCurrencyCode();
                        countryACCTo = accArray.get(i).getAccNo();

                    }
                });

        currencyDialog.show();
    }
}
