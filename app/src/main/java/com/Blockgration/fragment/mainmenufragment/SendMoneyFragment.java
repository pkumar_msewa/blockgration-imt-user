package com.Blockgration.fragment.mainmenufragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.adapter.CountryListAdapter;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.FontsForXMLLight;
import com.Blockgration.custom.LoadingDialog;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.transport.AddUpdateBeneficiary;
import com.Blockgration.custom.SearchableSpinner;
import com.Blockgration.imt.SendTransfer2;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.MenuMetadata;
import com.Blockgration.model.CountryModel;
import com.Blockgration.model.MobileRechargeCountryModel;
import com.Blockgration.util.NetworkErrorHandler;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Ksf on 7/15/2016.
 */
public class SendMoneyFragment extends Fragment {

    private View rootView;


    private MaterialEditText sender_country;
    private EditText meEnterAmount;
    private FontsForXMLLight moneyRecevied, moneyToPay;
    private JSONObject finalValue;

    ArrayList<CountryModel> countryModels;
    private LoadingDialog loadDlg;
    private JSONObject jsonRequest;
    private boolean isMobile = true;
    private TextView des_list, tvRecCurr;

    private String country_name, country_code;
    private SearchableSpinner spCountry;
    private List<MobileRechargeCountryModel> currencyArray;
    private ArrayAdapter countryAdapter;
    private ArrayList<String> countries = new ArrayList<>();
    private Button btnFundTransfer;

    //new design
    private MaterialEditText meRecAmount;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadDlg = new LoadingDialog(getActivity());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_send_transfer, container, false);
        sender_country = (MaterialEditText) rootView.findViewById(R.id.sender_country);
        btnFundTransfer = (Button) rootView.findViewById(R.id.btnFundTransfer);


//        llRecAmount = (LinearLayout) rootView.findViewById(R.id.llRecAmount);

        finalValue = new JSONObject();
        currencyArray = new ArrayList<>();
        countryModels = new ArrayList<>();


        loadCountryApi();


        sender_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCountryDialog();
            }
        });


        btnFundTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                submitForm();

                startActivity(new Intent(getContext(), SendTransfer2.class));

            }
        });


        return rootView;
    }

    private void submitForm() {


        if (!validateSpCountry()) {
            return;
        }


//        loadDlg.show();


//        promoteRegister();
    }


    public void showCountryDialog() {

        final ArrayList<String> currencyVal = new ArrayList<>();

        for (MobileRechargeCountryModel currencyModel : currencyArray) {
            currencyVal.add(currencyModel.getCountryName());
        }

        android.support.v7.app.AlertDialog.Builder currencyDialog =
                new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        currencyDialog.setTitle("Select your Country");


        currencyDialog.setItems(currencyVal.toArray(new String[currencyVal.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int i) {
                        sender_country.setText(currencyVal.get(i));
                        Log.i("Country selected", sender_country.getText().toString());
                        for (MobileRechargeCountryModel currencyModel : currencyArray) {
                            if (currencyModel.getCountryName().equals(currencyVal.get(i))) {
                                country_name = currencyArray.get(i).getCountryName();
                                country_code = currencyArray.get(i).getCountryCode();
//                                loadDlg.show();
                                Log.i("Country name", country_name);
                                Log.i("Country code", country_code);
                            }
                        }
                    }
                });

        currencyDialog.show();
    }

    private void loadCountryApi() {
        StringRequest req = new StringRequest(Request.Method.GET, ApiURLNew.URL_REGISTER_COUNTRIES, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("Country URL", ApiURLNew.URL_REGISTER_COUNTRIES);
                Log.i("Country Response", s);
                loadDlg.dismiss();
                try {
                    JSONObject obj = new JSONObject(s);
                    String res = obj.getString("response");
                    JSONObject resp = new JSONObject(res);
                    String code = resp.getString("code");
                    if (!code.equals("null") && code.equals("S00")) {
                        JSONArray details = resp.getJSONArray("details");
                        for (int i = 0; i < details.length(); i++) {
                            JSONObject jo_inside = details.getJSONObject(i);
                            String countryName = jo_inside.getString("name");
                            String countryCode = jo_inside.getString("code");
                            MobileRechargeCountryModel model = new MobileRechargeCountryModel(countryName, countryCode);
                            currencyArray.add(model);
                        }
                        if (currencyArray.size() != 0) {
                            for (int p = 0; p < currencyArray.size(); p++) {
                                Collections.sort(currencyArray, new Comparator<MobileRechargeCountryModel>() {
                                    @Override
                                    public int compare(MobileRechargeCountryModel o1, MobileRechargeCountryModel o2) {
                                        return o1.getCountryName().compareToIgnoreCase(o2.getCountryName());
                                    }
                                });
                                countries.add(currencyArray.get(p).getCountryName());
                            }
//                            try {
//                                countryAdapter = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, countries);
//                                country_name.setAdapter(countryAdapter);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
                        }
                    }
//                    workLoad(3000);
                } catch (JSONException e) {
                    e.printStackTrace();
//                    workLoad(3000);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                loadDlg.dismiss();
                CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(volleyError, getActivity()));
                volleyError.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("hash", "1234");
                return map;
            }
        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        PayQwikApp.getInstance().addToRequestQueue(req);
    }

    private boolean validateSpCountry() {
        if (sender_country.getText().toString().trim().isEmpty()) {
            CustomToast.showMessage(getActivity(), "Select Country");
            requestFocus(sender_country);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}



