package com.Blockgration.fragment.mainmenufragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.Blockgration.adapter.ReceiptAdapter;
import com.Blockgration.Userwallet.PayQwikApp;
import com.Blockgration.Userwallet.R;
import com.Blockgration.custom.CustomAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.custom.LoadMoreListView;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.StatementModel;
import com.Blockgration.model.UserModel;
import com.Blockgration.util.NetworkErrorHandler;

/**
 * Created by Ksf on 5/8/2016.
 */
public class ReceiptFragment extends Fragment {
    private View rootView;
    private ReceiptAdapter receiptAdapter;
    private LoadMoreListView lvReceipt;
    private UserModel session = UserModel.getInstance();
    private JSONObject jsonRequest;
    private ArrayList<StatementModel> receiptList;
    private ProgressBar pbReceipt;
    private LinearLayout llNoReceipt;
    private TextView tvNoReceipt;
    //Volley
    private String tag_json_obj = "json_receipt";
    private JsonObjectRequest postReq;

    //Pagination
    private int currentPage = 0;
    private int totalPage = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiptList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_receipt, container, false);
        lvReceipt = (LoadMoreListView) rootView.findViewById(R.id.lvReceipt);
        pbReceipt = (ProgressBar) rootView.findViewById(R.id.pbReceipt);
        tvNoReceipt = (TextView) rootView.findViewById(R.id.tvNoReceipt);
        llNoReceipt = (LinearLayout) rootView.findViewById(R.id.llNoReceipt);
        llNoReceipt.setVisibility(View.GONE);
        loadUserStatement();

        lvReceipt.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (currentPage < totalPage) {
                    loadMoreUserStatement();
                } else {
                    lvReceipt.onLoadMoreComplete();
                }
            }
        });

        return rootView;
    }
    public void loadUserStatement() {
        pbReceipt.setVisibility(View.VISIBLE);
        lvReceipt.setVisibility(View.GONE);
        jsonRequest = new JSONObject();
        try {

            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("page", currentPage);
            jsonRequest.put("size", 100);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Receipt Url", ApiURLNew.URL_RECEIPT);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_RECEIPT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Receipts Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
//                            sendRefresh();
                            String jsonString = JsonObj.getString("response");
                            JSONObject response = new JSONObject(jsonString);
                            JSONObject jsonDetails = response.getJSONObject("details");
//                            JSONArray arr = jsonDetails.getJSONArray("content");
//                            int totalElements = jsonDetails.getInt("totalElements");
                            int totalElements = JsonObj.getInt("numberOfElements");
                            totalPage = jsonDetails.getInt("totalPages");
                            if (totalElements == 0) {
                                llNoReceipt.setVisibility(View.VISIBLE);
                                pbReceipt.setVisibility(View.GONE);
                                lvReceipt.setVisibility(View.GONE);

                            } else {
//                            JSONArray operatorArray = response.getJSONArray("transaction");
                                JSONArray operatorArray = jsonDetails.getJSONArray("content");

                                for (int i = 0; i < operatorArray.length(); i++) {
                                    JSONObject c = operatorArray.getJSONObject(i);

                                    double currentBalanceD = c.getDouble("currentBalance");
                                    NumberFormat nf = new DecimalFormat("##.##");
                                    String currentBalance = String.valueOf(nf.format(currentBalanceD));

                                    double amountPaidD = c.getDouble("amount");
                                    String amountPaid = String.valueOf(amountPaidD);

                                    long createdD = c.getLong("created");
                                    String dateTime = String.valueOf(createdD);

                                    JSONObject serviceObj = c.getJSONObject("service");
                                    String sCode = serviceObj.getString("code");
                                    JSONObject serviceTypeObj = serviceObj.getJSONObject("serviceType");
                                    String servicesType = serviceTypeObj.getString("name");


//                                JSONObject serviceObj = c.getJSONObject("service");
//                                String serviceTypeStr = serviceObj.getString("serviceType");
//                                JSONObject serviceTypeObj = new JSONObject(serviceTypeStr);
//                                String servicesType = serviceTypeObj.getString("name");
//                                String servicesType = c.getString("service");
                                    String serviceStatus = c.getString("status");

                                    String refNo = c.getString("transactionRefNo");
                                    String description = c.getString("description");
                                    boolean isDebited = c.getBoolean("debit");

                                    String retrievRefNo = "";
                                    String authNo = "";

                                    if (c.has("retrivalReferenceNo")) {
                                        if (!c.isNull("retrivalReferenceNo")) {
                                            retrievRefNo = c.getString("retrivalReferenceNo");
                                        }
                                    }

                                    if (c.has("authReferenceNo")) {
                                        if (!c.isNull("authReferenceNo")) {
                                            authNo = c.getString("authReferenceNo");
                                        }
                                    }


                                    StatementModel statementModel = new StatementModel(currentBalance, amountPaid, dateTime, servicesType, serviceStatus, refNo, description, isDebited, authNo, retrievRefNo, sCode);
                                    receiptList.add(statementModel);
                                }
                                if (receiptList != null && receiptList.size() != 0) {

                                    receiptAdapter = new ReceiptAdapter(getActivity(), receiptList);
                                    lvReceipt.setAdapter(receiptAdapter);
                                    pbReceipt.setVisibility(View.GONE);
                                    lvReceipt.setVisibility(View.VISIBLE);
                                    llNoReceipt.setVisibility(View.GONE);
                                }
                            }

                        } else if (code != null && code.equals("F03")) {
                            pbReceipt.setVisibility(View.GONE);
                            lvReceipt.setVisibility(View.GONE);
                            showInvalidSessionDialog();
                        } else {
                            pbReceipt.setVisibility(View.GONE);
                            lvReceipt.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        pbReceipt.setVisibility(View.GONE);
                        lvReceipt.setVisibility(View.GONE);
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    llNoReceipt.setVisibility(View.VISIBLE);
                    pbReceipt.setVisibility(View.GONE);
                    lvReceipt.setVisibility(View.GONE);
                    try {
                        tvNoReceipt.setText(NetworkErrorHandler.getMessage(error, getActivity()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }

    public void loadMoreUserStatement() {
        currentPage = currentPage + 1;
        jsonRequest = new JSONObject();
        try {
            jsonRequest.put("sessionId", session.getUserSessionId());
            jsonRequest.put("page", currentPage);
            jsonRequest.put("size", 100);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonRequest = null;
        }

        if (jsonRequest != null) {
            Log.i("JsonRequest", jsonRequest.toString());
            Log.i("Receipt Url", ApiURLNew.URL_RECEIPT);
            postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_RECEIPT, jsonRequest, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject JsonObj) {
                    try {

                        Log.i("Receipts Response", JsonObj.toString());
                        String message = JsonObj.getString("message");
                        String code = JsonObj.getString("code");

                        if (code != null && code.equals("S00")) {
                            String jsonString = JsonObj.getString("response");
                            JSONObject response = new JSONObject(jsonString);
                            JSONObject jsonDetails = response.getJSONObject("details");
                            JSONArray operatorArray = jsonDetails.getJSONArray("content");
                            for (int i = 0; i < operatorArray.length(); i++) {
                                JSONObject c = operatorArray.getJSONObject(i);
                                double currentBalanceD = c.getDouble("currentBalance");
//                                String currentBalance = String.valueOf(currentBalanceD);
                                NumberFormat nf = new DecimalFormat("##.##");
                                String currentBalance = String.valueOf(nf.format(currentBalanceD));
                                double amountPaidD = c.getDouble("amount");
                                String amountPaid = String.valueOf(amountPaidD);

                                long createdD = c.getLong("created");
                                String dateTime = String.valueOf(createdD);

                                JSONObject serviceObj = c.getJSONObject("service");
                                String sCode = serviceObj.getString("code");
                                JSONObject serviceTypeObj = serviceObj.getJSONObject("serviceType");
                                String servicesType = serviceTypeObj.getString("name");

//                                String servicesType = c.getString("service");
                                String serviceStatus = c.getString("status");

                                String refNo = c.getString("transactionRefNo");
                                String description = c.getString("description");
                                boolean isDebited = c.getBoolean("debit");


                                String retrievRefNo = "";
                                String authNo = "";


                                if (c.has("retrivalReferenceNo")) {
                                    if (!c.isNull("retrivalReferenceNo")) {
                                        retrievRefNo = c.getString("retrivalReferenceNo");
                                    }
                                }

                                if (c.has("authReferenceNo")) {
                                    if (!c.isNull("authReferenceNo")) {
                                        authNo = c.getString("authReferenceNo");
                                    }
                                }

                                StatementModel statementModel = new StatementModel(currentBalance, amountPaid, dateTime, servicesType, serviceStatus, refNo, description, isDebited, authNo, retrievRefNo, sCode);
                                receiptList.add(statementModel);
                            }
                            receiptAdapter.notifyDataSetChanged();
                        } else if (code != null && code.equals("F03")) {
                            showInvalidSessionDialog();
                        } else {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                        lvReceipt.onLoadMoreComplete();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        lvReceipt.onLoadMoreComplete();
                        CustomToast.showMessage(getActivity(), getResources().getString(R.string.server_exception2));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    CustomToast.showMessage(getActivity(), NetworkErrorHandler.getMessage(error, getActivity()));
                    lvReceipt.onLoadMoreComplete();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("hash", "1234");
                    return map;
                }

            };
            int socketTimeout = 60000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postReq.setRetryPolicy(policy);
            PayQwikApp.getInstance().addToRequestQueue(postReq, tag_json_obj);
        }

    }


    public void showInvalidSessionDialog() {
        CustomAlertDialog builder = new CustomAlertDialog(getActivity(), R.string.dialog_title2, Html.fromHtml(AppMetadata.getInvalidSession()));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                sendLogout();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });
        builder.show();
    }

    private void sendLogout() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "2");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    private void sendRefresh() {
        Intent intent = new Intent("setting-change");
        intent.putExtra("updates", "1");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onDetach() {
        postReq.cancel();
        super.onDetach();
    }
}
