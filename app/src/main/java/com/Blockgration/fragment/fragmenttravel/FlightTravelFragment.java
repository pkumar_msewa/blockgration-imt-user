package com.Blockgration.fragment.fragmenttravel;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.flightinneracitivty.DomesticFlightSearchActivity;
import com.Blockgration.Userwallet.activity.flightinneracitivty.FlightListOnWayActivity;
import com.Blockgration.Userwallet.activity.flightinneracitivty.FlightListTwoWayActivity;
import com.Blockgration.custom.CustomTravellerPickerDialog;
import com.Blockgration.model.DomesticFlightModel;
import com.Blockgration.util.CheckLog;
import com.Blockgration.util.PayingDetailsValidation;
import com.Blockgration.util.TravellerSelectedListner;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.Calendar;

/**
 * Created by Ksf on 9/26/2016.
 */
public class FlightTravelFragment extends Fragment implements TravellerSelectedListner {
    private View rootView;
    private MaterialEditText etFlightFrom, etFlightTo, etFlightDepartDate, etFlightReturnDate, etFlightTraveller, etFlightClass;
    private Button btnSearchFlight;

    private RadioButton rbFlightRoundTrip, rbFlightOneWay;
    private RadioGroup rgFlightTripType;

    private RadioButton rbFlightDomestic, rbFlightInternational;
    private RadioGroup rgFlightType;


//    private Switch switchFLightType;

    private View focusView = null;
    private boolean cancel;

    private int adultNo, childNo, infantNo;
    private String desCode, sourceCode;
    private String flightClass;
    private int flightType;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_travel_flight, container, false);
        btnSearchFlight = (Button) rootView.findViewById(R.id.btnSearchFlight);
        etFlightFrom = (MaterialEditText) rootView.findViewById(R.id.etFlightFrom);
        etFlightTo = (MaterialEditText) rootView.findViewById(R.id.etFlightTo);
        etFlightDepartDate = (MaterialEditText) rootView.findViewById(R.id.etFlightDepartDate);
        etFlightReturnDate = (MaterialEditText) rootView.findViewById(R.id.etFlightReturnDate);
        etFlightTraveller = (MaterialEditText) rootView.findViewById(R.id.etFlightTraveller);
        etFlightClass = (MaterialEditText) rootView.findViewById(R.id.etFlightClass);
        rgFlightTripType = (RadioGroup) rootView.findViewById(R.id.rgFlightTripType);
        rbFlightRoundTrip = (RadioButton) rootView.findViewById(R.id.rbFlightRoundTrip);
        rbFlightOneWay = (RadioButton) rootView.findViewById(R.id.rbFlightOneWay);

        rgFlightType = (RadioGroup) rootView.findViewById(R.id.rgFlightType);
        rbFlightDomestic = (RadioButton) rootView.findViewById(R.id.rbFlightDomestic);
        rbFlightInternational = (RadioButton) rootView.findViewById(R.id.rbFlightInternational);


        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("flight-search-complete"));

        rgFlightTripType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                View radioButton = rgFlightTripType.findViewById(checkedId);
                int index = rgFlightTripType.indexOfChild(radioButton);

                switch (index) {
                    case 0:
                        etFlightReturnDate.setEnabled(false);
                        break;
                    case 1:
                        etFlightReturnDate.setEnabled(true);
                        break;
                }
            }
        });

        etFlightDepartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                    Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                    new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etFlightDepartDate.setText(
                                    String.valueOf(year)+"-"+String.valueOf(formattedMonth) + "-"+String.valueOf(formattedDay));
                        }
                    }, mYear, mMonth, mDay).show();

            }
        });


        etFlightReturnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                    Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                    new DatePickerDialog(getActivity(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String formattedDay, formattedMonth;
                            if (dayOfMonth < 10) {
                                formattedDay = "0" + dayOfMonth;
                            } else {
                                formattedDay = dayOfMonth + "";
                            }

                            if ((monthOfYear + 1) < 10) {
                                formattedMonth = "0" + String.valueOf(monthOfYear + 1);
                            } else {
                                formattedMonth = String.valueOf(monthOfYear + 1) + "";
                            }
                            etFlightReturnDate.setText(
                                    String.valueOf(year)+"-"+String.valueOf(formattedMonth) + "-"+String.valueOf(formattedDay));
                        }
                    }, mYear, mMonth, mDay).show();
                }
        });



        etFlightClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] items = {"Economy", "Business", "Premium Economy"};
                android.support.v7.app.AlertDialog.Builder callDialog = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                callDialog.setTitle("Select your flight class");
                callDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                callDialog.setItems(items, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int position) {
                        if (position == 0) {
                            flightClass = "Economy";
                        } else if (position == 1) {
                            flightClass = "Business";
                        } else {
                            flightClass = "Business";
                        }
                        etFlightClass.setText(items[position].toString());
                    }

                });

                callDialog.show();
            }
        });

        etFlightTraveller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTravellerDialog();
            }
        });

        btnSearchFlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rbFlightInternational.isChecked()) {
                    flightType = 2;
                } else {
                    flightType = 1;
                }
                attemptSearch();
            }
        });

        etFlightFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent searchCityIntent = new Intent(getActivity(), DomesticFlightSearchActivity.class);
                searchCityIntent.putExtra("searchType", "From");
                startActivity(searchCityIntent);

            }
        });
        etFlightTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent searchCityIntent = new Intent(getActivity(), DomesticFlightSearchActivity.class);
                searchCityIntent.putExtra("searchType", "To");
                startActivity(searchCityIntent);

            }
        });

        return rootView;
    }

    public void showTravellerDialog() {
        CustomTravellerPickerDialog builder = new CustomTravellerPickerDialog(getActivity(), this);
        builder.setCancelable(true);
    }

    @Override
    public void onSelected(int adultCount, int childCount, int infantCount) {
        adultNo = adultCount;
        childNo = childCount;
        infantNo = infantCount;
        //ALL THREE
        if (adultCount != 0 && childCount != 0 && infantCount != 0) {
            etFlightTraveller.setText(adultCount + " Adults, " + childCount + " Children, " + infantCount + " Infant");
        }
        //ADULT AND CHILD
        else if (adultCount != 0 && childCount != 0 && infantCount == 0) {
            etFlightTraveller.setText(adultCount + " Adults, " + childCount + " Children");
        }
        //ADULT AND INFANT
        else if (adultCount != 0 && infantCount != 0 && childCount == 0) {
            etFlightTraveller.setText(adultCount + " Adults, " + infantCount + " Infant");
        }
        //CHILD AND INFANT
        else if (adultCount == 0 && childCount != 0 && infantCount != 0) {
            etFlightTraveller.setText(childCount + " Children, " + infantCount + " Infant");
        }

        //ONLY ADULT
        else if (adultCount != 0 && childCount == 0 && infantCount == 0) {
            etFlightTraveller.setText(adultCount + " Adults, ");
        }
        //ONLY INFANT
        else if (adultCount == 0 && childCount == 0 && infantCount != 0) {
            etFlightTraveller.setText(adultCount + " Adults, " + infantCount + " Infant");
        }
        //ONLY CHILD
        else if (adultCount == 0 && infantCount == 0 && childCount != 0) {
            etFlightTraveller.setText(childCount + " Children");
        } else {
            etFlightTraveller.setText("");
        }

    }

    private void attemptSearch() {
        etFlightFrom.setError(null);
        etFlightTo.setError(null);
        etFlightDepartDate.setError(null);
        etFlightReturnDate.setError(null);
        etFlightTraveller.setError(null);
        etFlightClass.setError(null);

        cancel = false;


        checkBusFrom(etFlightFrom.getText().toString());
        checkBusTo(etFlightTo.getText().toString());
        checkDepDate(etFlightDepartDate.getText().toString());
        checkTraveller(etFlightTraveller.getText().toString());
        checkFlightClass(etFlightClass.getText().toString());

        if (rbFlightRoundTrip.isChecked()) {
            checkReturnDate(etFlightReturnDate.getText().toString());
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            if (rbFlightRoundTrip.isChecked()) {
                Intent flightSearchIntent = new Intent(getActivity(), FlightListTwoWayActivity.class);
                flightSearchIntent.putExtra("Adult", adultNo);
                flightSearchIntent.putExtra("Child", childNo);
                flightSearchIntent.putExtra("Infant", infantNo);
                flightSearchIntent.putExtra("Class", flightClass);
                flightSearchIntent.putExtra("destination", desCode);
                flightSearchIntent.putExtra("source", sourceCode);
                flightSearchIntent.putExtra("date", etFlightDepartDate.getText().toString());
                flightSearchIntent.putExtra("flightType", flightType);
                flightSearchIntent.putExtra("dateOfReturn", etFlightReturnDate.getText().toString());
                startActivity(flightSearchIntent);
            } else {
                Intent flightSearchIntent = new Intent(getActivity(), FlightListOnWayActivity.class);
                flightSearchIntent.putExtra("Adult", adultNo);
                flightSearchIntent.putExtra("Child", childNo);
                flightSearchIntent.putExtra("Infant", infantNo);
                flightSearchIntent.putExtra("Class", flightClass);
                flightSearchIntent.putExtra("destination", desCode);
                flightSearchIntent.putExtra("source", sourceCode);
                flightSearchIntent.putExtra("date", etFlightDepartDate.getText().toString());
                flightSearchIntent.putExtra("flightType", flightType);

                startActivity(flightSearchIntent);
            }
        }
    }


    private void checkBusFrom(String busFrom) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(busFrom);
        if (!gasCheckLog.isValid) {
            etFlightFrom.setError(getString(gasCheckLog.msg));
            focusView = etFlightFrom;
            cancel = true;
        }
    }

    private void checkBusTo(String busTo) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(busTo);
        if (!gasCheckLog.isValid) {
            etFlightTo.setError(getString(gasCheckLog.msg));
            focusView = etFlightTo;
            cancel = true;
        }
    }

    private void checkFlightClass(String flightClass) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(flightClass);
        if (!gasCheckLog.isValid) {
            etFlightClass.setError(getString(gasCheckLog.msg));
            focusView = etFlightClass;
            cancel = true;
        }
    }

    private void checkDepDate(String busDepDate) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(busDepDate);
        if (!gasCheckLog.isValid) {
            etFlightDepartDate.setError(getString(gasCheckLog.msg));
            focusView = etFlightDepartDate;
            cancel = true;
        }
    }

    private void checkReturnDate(String busReDate) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(busReDate);
        if (!gasCheckLog.isValid) {
            etFlightReturnDate.setError(getString(gasCheckLog.msg));
            focusView = etFlightReturnDate;
            cancel = true;
        }
    }

    private void checkTraveller(String travellerNo) {
        CheckLog gasCheckLog = PayingDetailsValidation.checkGasCustomerAc(travellerNo);
        if (!gasCheckLog.isValid) {
            etFlightTraveller.setError(getString(gasCheckLog.msg));
            focusView = etFlightTraveller;
            cancel = true;
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("searchType");
            if (action.equals("To")) {
                DomesticFlightModel domesticFlightModel = intent.getParcelableExtra("selectedCityModel");
                desCode = domesticFlightModel.getAirportCode();
                etFlightTo.setText(domesticFlightModel.getCityName() + ", " + domesticFlightModel.getAirportCode());
            } else {
                DomesticFlightModel domesticFlightModel = intent.getParcelableExtra("selectedCityModel");
                sourceCode = domesticFlightModel.getAirportCode();
                etFlightFrom.setText(domesticFlightModel.getCityName() + ", " + domesticFlightModel.getAirportCode());
            }
        }
    };

    @Override
    public void onDestroyView() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        super.onDestroyView();
    }
}