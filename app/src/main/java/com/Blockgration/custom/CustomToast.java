package com.Blockgration.custom;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.Blockgration.Userwallet.R;


public class CustomToast {

	private static final int CUSTOM_LAYOUT_MESSAGE = R.layout.custom_toast_message;

	public static void showMessage(Context context, String message) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View layout = inflater.inflate(CUSTOM_LAYOUT_MESSAGE, null);

		TextView text = (TextView) layout.findViewById(R.id.tvToastMessage);
		text.setText(message);

	 	Toast toast = new Toast(context);
		toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 50);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(layout);
		toast.show();
	}

}
