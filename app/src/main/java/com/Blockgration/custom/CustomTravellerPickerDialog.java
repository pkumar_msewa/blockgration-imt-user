package com.Blockgration.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.NumberPicker;

import com.Blockgration.Userwallet.R;
import com.Blockgration.util.TravellerSelectedListner;


/**
 * Created by Ksf on 9/26/2016.
 */
public class CustomTravellerPickerDialog extends AlertDialog.Builder {

    private TravellerSelectedListner travellerSelectedListner;
    private AlertDialog closeDialog;

    public CustomTravellerPickerDialog(Context context, final TravellerSelectedListner travellerSelectedListner) {
        super(context);
        this.travellerSelectedListner = travellerSelectedListner;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_travller_picker, null, false);
        Button btnTravellerDismiss = (Button) viewDialog.findViewById(R.id.btnTravellerDismiss);
        Button btnTravellerOk = (Button) viewDialog.findViewById(R.id.btnTravellerOk);


        final NumberPicker npAdult = (NumberPicker) viewDialog.findViewById(R.id.npAdult);
        final NumberPicker npChild = (NumberPicker) viewDialog.findViewById(R.id.npChild);
        final NumberPicker npInfant = (NumberPicker) viewDialog.findViewById(R.id.npInfant);
        npInfant.setMaxValue(9);
        npInfant.setMinValue(0);
        npAdult.setMaxValue(9);
        npAdult.setMinValue(0);
        npChild.setMaxValue(9);
        npChild.setMinValue(0);

        btnTravellerDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDialog.dismiss();
            }
        });

        btnTravellerOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                travellerSelectedListner.onSelected( npAdult.getValue(),npChild.getValue(),npInfant.getValue());
                closeDialog.dismiss();
            }
        });

        this.setView(viewDialog);
        this.setCancelable(true);
        closeDialog= this.create();
        closeDialog.show();
        closeDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }


}