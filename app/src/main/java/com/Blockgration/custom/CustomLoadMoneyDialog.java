package com.Blockgration.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;


/**
 * Created by Ksf on 4/12/2016.
 */
public class CustomLoadMoneyDialog extends AlertDialog.Builder {

    public CustomLoadMoneyDialog(Context context, int title) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewDialog = inflater.inflate(R.layout.dialog_custom_load_money, null, false);

        TextView titleTextView = (TextView) viewDialog.findViewById(R.id.cadTiltle);
//        titleTextView.setText(context.getText(title));
        titleTextView.setText(R.string.dialog_title);

//        TextView messageTextView = (TextView) viewDialog.findViewById(R.id.cadMessage);
//        messageTextView.setText(message);

        EditText etAmount = (EditText) viewDialog.findViewById(R.id.etAmount);
        EditText etACHbanklist = (EditText) viewDialog.findViewById(R.id.etACHbanklist);
        EditText etACHAmount = (EditText) viewDialog.findViewById(R.id.etACHAmount);
        Button cancel = (Button) viewDialog.findViewById(R.id.cancel);
        Button pay = (Button) viewDialog.findViewById(R.id.pay);


        this.setCancelable(false);
        this.setView(viewDialog);


    }


}
