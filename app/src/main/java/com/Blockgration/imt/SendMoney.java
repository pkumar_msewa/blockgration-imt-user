package com.Blockgration.imt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.Blockgration.Userwallet.R;

public class SendMoney extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.send_money);

    findViewById(R.id.next).setOnClickListener(
      new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          startActivity(new Intent(getApplicationContext(),BankInformation.class));
        }
      }
    );

  }
}
