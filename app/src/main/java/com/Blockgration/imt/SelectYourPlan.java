package com.Blockgration.imt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.Blockgration.Userwallet.R;

public class SelectYourPlan extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.select_your_plan);

    startActivity(new Intent(getApplicationContext(),SendMoney.class));

  }
}
