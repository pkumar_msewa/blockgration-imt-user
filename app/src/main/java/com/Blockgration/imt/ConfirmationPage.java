package com.Blockgration.imt;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;

public class ConfirmationPage extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.confirmation_page);
    TextView next,back;
    next = findViewById(R.id.viewReceipt);
    back = findViewById(R.id.cancelTransaction);
    next.setOnClickListener(
            new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ReceiptDetails.class));
              }
            }
    );
    back.setOnClickListener(
            new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                onBackPressed();
              }
            }
    );


  }
}
