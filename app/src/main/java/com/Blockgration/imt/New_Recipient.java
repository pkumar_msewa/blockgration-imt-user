package com.Blockgration.imt;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.Blockgration.Userwallet.R;

public class New_Recipient extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.new__recipient);
    Button next,back;
    next = findViewById(R.id.next);
    back = findViewById(R.id.back);
    next.setOnClickListener(
            new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),CashPickUpLocation.class));
              }
            }
    );
    back.setOnClickListener(
            new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                onBackPressed();
              }
            }
    );
  }
}
