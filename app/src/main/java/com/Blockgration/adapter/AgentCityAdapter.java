package com.Blockgration.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;


import com.Blockgration.Userwallet.R;
import com.Blockgration.model.AgentCityModel;

import java.util.ArrayList;


public class AgentCityAdapter extends ArrayAdapter<AgentCityModel> {
    // Your sent context
    private final String MY_DEBUG_TAG = "CustomerAdapter";
    private ArrayList<AgentCityModel> items;
    private ArrayList<AgentCityModel> itemsAll;
    private ArrayList<AgentCityModel> suggestions;
    private Context context;
    private int viewResourceId;

    public AgentCityAdapter(Context context, int textViewResourceId, ArrayList<AgentCityModel> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.items = values;
        this.itemsAll = (ArrayList<AgentCityModel>) items.clone();
        this.suggestions = new ArrayList<AgentCityModel>();
        this.viewResourceId = textViewResourceId;
    }

//    public int getCount() {
//        return values.size();
//    }

//    public long getItemId(int position) {
//        return position;
//    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
//        TextView label = new TextView(context);
//        label.setTextColor(Color.BLACK);
//        label.setPadding(16, 8, 8, 8);
//        label.setTextSize(16);
//        label.setText(values.get(position).getAgent_Name());
//
//        // And finally return your dynamic (or custom) view for each spinner item
//        return label;
//    }
//
//    // And here is when the "chooser" is popped up
//    // Normally is the same view, but you can customize it if you want
//    @Override
//    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//        TextView label = new TextView(context);
//        label.setTextColor(Color.BLACK);
//        label.setPadding(16, 8, 8, 8);
//        label.setTextSize(16);
//        label.setText(values.get(position).getAgent_Name());
//        return label;
//    }
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.list_item, null);
        }
        AgentCityModel customer = items.get(position);
        if (customer != null) {
            TextView customerNameLabel = (TextView) v.findViewById(R.id.title);
            if (customerNameLabel != null) {
//              Log.i(MY_DEBUG_TAG, "getView Customer Name:"+customer.getName());
                customerNameLabel.setText(customer.getAgent_Name());
            }
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((AgentCityModel) (resultValue)).getAgent_Name();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (AgentCityModel customer : itemsAll) {
                    if (customer.getAgent_Name().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(customer);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<AgentCityModel> filteredList = (ArrayList<AgentCityModel>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (AgentCityModel c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };

}


    


