package com.Blockgration.adapter;

import android.content.Context;
import android.util.Log;


import com.Blockgration.custom.CustomToast;
import com.Blockgration.Userwallet.R;
import com.Blockgration.metadata.ApiURLNew;
import com.Blockgration.model.AgentCityModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Amrutha-PC on 14-Mar-17.
 */

class AgentAPI {

    private StringBuilder stringBuilder;

    public ArrayList<AgentCityModel> autocomplete(JSONObject jsonObject, final Context context) {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(ApiURLNew.URL_SEND_MONEY_AGENT_CITY);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            OutputStreamWriter streamWriter = new OutputStreamWriter(connection.getOutputStream());
            Log.i("JSON value",jsonObject.toString());
            streamWriter.write(jsonObject.toString());
            streamWriter.flush();
            stringBuilder = new StringBuilder();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(streamReader);
                String response = null;
                while ((response = bufferedReader.readLine()) != null) {
                    stringBuilder.append(response + "\n");
                }
                Log.i("LEngthhhshshhasgdhasg", stringBuilder.toString());
                bufferedReader.close();

            } else {
                Log.e("test", connection.getResponseMessage());
                return null;
            }
        } catch (Exception exception) {
            Log.e("test", exception.toString());
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        ArrayList<AgentCityModel> arrayList = new ArrayList<>();
        try {

            JSONObject newResponse = new JSONObject(stringBuilder.toString());
            if (newResponse.getString("status_code").equals("1000")) {
                Object o = newResponse.get("results");

                if ((o instanceof JSONArray)) {
                    for (int i = 0; i < ((JSONArray) o).length(); i++) {

                        AgentCityModel agentModel = new AgentCityModel(((JSONArray) o).getJSONObject(i).getString("name"), ((JSONArray) o).getJSONObject(i).getString("id"));
                        arrayList.add(agentModel);
                    }
                    return arrayList;
                }
            } else {
//                            CustomToast.showMessage(context,response.getString());
                return arrayList;
            }
        } catch (JSONException e) {

            CustomToast.showMessage(context, context.getResources().getString(R.string.server_exception2));
            e.printStackTrace();
            return arrayList;
        }
        return arrayList;
    }

//
//        JsonObjectRequest postReq = new JsonObjectRequest(Request.Method.POST, ApiURLNew.URL_SEND_MONEY_AGENT_CITY, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
////                loadingDialog.dismiss();
//                Log.i("AgenRESPOSNE", response.toString());
//                try {
//                    arrayList = new ArrayList<>();
//                    if (response.getString("status_code").equals("1000")) {
//                        Object o = response.get("results");
//                        if ((o instanceof JSONArray)) {
//                            for (int i = 0; i < ((JSONArray) o).length(); i++) {
//                                AgentCityModel agentModel = new AgentCityModel(((JSONArray) o).getJSONObject(i).getString("name"), ((JSONArray) o).getJSONObject(i).getString("id"));
//                                arrayList.add(agentModel.getAgent_Name() + "" + agentModel.getAgent_City_Sub_Id());
//                            }
//                            Log.i("listValue", String.valueOf(arrayList.size()));
//                            callback.onSuccess(arrayList);
//                        }
//                    } else {
////                            CustomToast.showMessage(context,response.getString());
//                    }
//                } catch (JSONException e) {
////                    loadingDialog.dismiss();
//                    CustomToast.showMessage(context, context.getResources().getString(R.string.server_exception2));
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
////                loadingDialog.dismiss();
//                CustomToast.showMessage(context, NetworkErrorHandler.getMessage(error, context));
//            }
//        });
//        int socketTimeout = 60000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        postReq.setRetryPolicy(policy);
//        PayQwikApp.getInstance().addToRequestQueue(postReq, "tag");

}

