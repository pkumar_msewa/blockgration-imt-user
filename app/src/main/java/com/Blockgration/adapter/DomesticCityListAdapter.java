package com.Blockgration.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.model.DomesticFlightModel;

import java.util.List;

/**
 * Created by Ksf on 10/6/2016.
 */
public class DomesticCityListAdapter extends BaseAdapter{

    private Context context;
    private List<DomesticFlightModel> flightCityArray;
    private ViewHolder viewHolder;

    public DomesticCityListAdapter(Context context, List<DomesticFlightModel> flightCityArray) {
        this.context = context;
        this.flightCityArray = flightCityArray;
    }

    @Override
    public int getCount() {
        return flightCityArray.size();
    }

    @Override
    public Object getItem(int index) {
        return flightCityArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_flight_city_list, parent,false);
            viewHolder = new ViewHolder();
            viewHolder.tvFlightCity = (TextView) convertView.findViewById(R.id.tvFlightCity);
            viewHolder.tvFlightCode = (TextView) convertView.findViewById(R.id.tvFlightCode);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvFlightCity.setText(flightCityArray.get(position).getCityName());
        viewHolder.tvFlightCode.setText(flightCityArray.get(position).getAirportCode());
        return convertView;
    }


    static class ViewHolder {
        TextView tvFlightCity;
        TextView tvFlightCode;
    }
}
