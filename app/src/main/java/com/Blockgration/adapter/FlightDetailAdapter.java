package com.Blockgration.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.model.FlightModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Ksf on 10/13/2016.
 */
public class FlightDetailAdapter extends BaseAdapter {
    private Context context;
    private List<FlightModel> flightArray;
    private ViewHolder viewHolder;


    public FlightDetailAdapter(Context context, List<FlightModel> flightArray) {
        this.context = context;
        this.flightArray = flightArray;

    }

    @Override
    public int getCount() {
        return flightArray.size();
    }

    @Override
    public Object getItem(int index) {
        return flightArray.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int currentPosition = position;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_flight_details, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.tvFlightDetailDepAirport = (TextView) convertView.findViewById(R.id.tvFlightDetailDepAirport);
            viewHolder.tvFlightDetailDepTime = (TextView) convertView.findViewById(R.id.tvFlightDetailDepTime);
            viewHolder.tvFlightDetailDep = (TextView) convertView.findViewById(R.id.tvFlightDetailDep);
            viewHolder.tvFlightDetailDuration = (TextView) convertView.findViewById(R.id.tvFlightDetailDuration);
            viewHolder.tvFlightDetailArrival = (TextView) convertView.findViewById(R.id.tvFlightDetailArrival);
            viewHolder.tvFlightDetailArrivalTime = (TextView) convertView.findViewById(R.id.tvFlightDetailArrivalTime);
            viewHolder.tvFlightDetailArrivalAirport = (TextView) convertView.findViewById(R.id.tvFlightDetailArrivalAirport);

            viewHolder.tvFlightDetailArrivalDate = (TextView) convertView.findViewById(R.id.tvFlightDetailArrivalDate);
            viewHolder.tvFlightDetailDepDate = (TextView) convertView.findViewById(R.id.tvFlightDetailDepDate);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        viewHolder.tvFlightDetailDepAirport.setText(flightArray.get(position).getFlightDepartureAirportName().trim());

        viewHolder.tvFlightDetailDep.setText(flightArray.get(position).getDepAirportCode());

//        String totalDuration = compareDate(flightArray.get(position).getDepTime(), flightArray.get(position).getArrivalTime());
        viewHolder.tvFlightDetailDuration.setText(flightArray.get(position).getFlightDuration());

        viewHolder.tvFlightDetailArrival.setText(flightArray.get(position).getArrivalAirportCode());
        viewHolder.tvFlightDetailArrivalAirport.setText(flightArray.get(position).getFlightArrivalAirportName().trim());

//        String[] dateTimeSplitDep = flightArray.get(position).getDepTime().split("T");
//        String[] timeSplitDep = dateTimeSplitDep[1].split(":");
        viewHolder.tvFlightDetailDepTime.setText(flightArray.get(position).getDepTime());
//        String[] dateTimeSplitArr = flightArray.get(position).getArrivalTime().split("T");
//        String[] timeSplitArr = dateTimeSplitArr[1].split(":");
        viewHolder.tvFlightDetailArrivalTime.setText(flightArray.get(position).getArrivalTime());

        viewHolder.tvFlightDetailArrivalDate.setText(flightArray.get(position).getFlightArrivalTimeZone());
        viewHolder.tvFlightDetailDepDate.setText(flightArray.get(position).getFlightDepartureTimeZone());



        return convertView;
    }


    static class ViewHolder {
        TextView tvFlightDetailDepAirport;
        TextView tvFlightDetailDepTime;
        TextView tvFlightDetailDep;
        TextView tvFlightDetailDuration;
        TextView tvFlightDetailArrival;
        TextView tvFlightDetailArrivalTime;
        TextView tvFlightDetailArrivalDate;
        TextView tvFlightDetailDepDate;

        TextView tvFlightDetailArrivalAirport;

    }

    private String compareDate(String depTime, String arrTime) {
        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(depTime);
            d2 = format.parse(arrTime);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
            if (diffDays >= 1) {
                return diffDays + " day " + diffHours + " hrs" + diffMinutes + " mins";
            } else if (diffHours >= 1) {
                return diffHours + " hrs " + diffMinutes + " mins";
            } else {
                return diffMinutes + " mins";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
