package com.Blockgration.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.giftcard.GiftCardListActivity;
import com.Blockgration.custom.CustomTermAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.model.SmartMobileAllOffersModel;

import java.util.List;



public class SmartMobAllOffersAdapter extends RecyclerView.Adapter<SmartMobAllOffersAdapter.RecyclerViewHolders> {

    private List<SmartMobileAllOffersModel> itemList;
    private Context context;
    String categoryDS;


    public SmartMobAllOffersAdapter(Context context, List<SmartMobileAllOffersModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_sm_all_offers, null);
        return new RecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, int position) {
        AQuery aq = new AQuery(context);
        if (itemList.get(position).getLogo_url() != null && !itemList.get(position).getLogo_url().isEmpty()) {
            aq.id(holder.ivSmartMobile).image(itemList.get(position).getLogo_url(), true, true);
            //aq.id(holder.tvDescription).(itemList.get(position.getCardDescription()),true,true);
            Log.i("AQ", String.valueOf(aq));
        }

        holder.tvSmartMobile.setText(itemList.get(position).getTitle());
//        GiftCardCatModel item = itemList.get(position);
//
//        DescriptionFragment f1 = new DescriptionFragment();
//        Bundle bundle = new Bundle();
//        //String transId = itemList.get(i).getCardDescription();
//        bundle.putString("Model", categoryDS);
//        f1.setArguments(bundle);






//        holder.tvDescription.setText(String.valueOf(itemList.get(position).getCardDescription()));
//        holder.tvTerms.setText(String.valueOf(itemList.get(position).getCardTerms()));

    }


    @Override
    public int getItemCount() {
        return this.itemList.size();
    }



    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView ivSmartMobile;
        public TextView tvSmartMobile;
        public Button btnTerms;
        public TextView tvDescription,tvTerms;


        public RecyclerViewHolders(View itemView) {
            super(itemView);
            tvSmartMobile = (TextView) itemView.findViewById(R.id.tvSmartMobile);
            ivSmartMobile = (ImageView) itemView.findViewById(R.id.ivSmartMobile);
            ivSmartMobile.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int i = getAdapterPosition();
            if (view.getId() == R.id.ivSmartMobile) {
                String id = itemList.get(i).getOffer_key();
                CustomToast.showMessage(context,id);
                /*Intent giftCardItemIntent = new Intent(context, GiftCardListActivity.class);
                giftCardItemIntent.putExtra("Model", itemList.get(i));
                context.startActivity(giftCardItemIntent);
                ((Activity)context).finish();*/
            } else if (view.getId() == R.id.btnTerms) {
//                showSettingsAlert(itemList.get(i).getCardTerms());

            }
            /*Bundle args = new Bundle();
            args.putString("Model", categoryDS);
            //set Fragmentclass Arguments
            DescriptionFragment fragobj = new DescriptionFragment();
            fragobj.setArguments(args);

            Log.d("LOGTAG2", String.valueOf(args));
            Log.d("LOGTAG3", String.valueOf(fragobj));
*/
//            DescriptionFragment fragment = new DescriptionFragment();
//            Bundle bundle = new Bundle();
//            bundle.putString("Model", categoryDS);
//            Log.i("BUNDLE", bundle.toString());
//            Log.i("BUNDLE1", categoryDS);
//            fragment.setArguments(bundle);
//            Log.i("BUNDLE2", fragment.toString());

//            DescriptionFragment f1 = new DescriptionFragment();
//            Bundle bundle = new Bundle();
//            //String transId = itemList.get(i).getCardDescription();
//            bundle.putString("Model", categoryDS);
//            f1.setArguments(bundle);
//            Log.i("BUNDLE1", bundle.toString());
//            Log.i("BUNDLE2", categoryDS);
//


        }
    }

    public void showSettingsAlert(String terms) {
        CustomTermAlertDialog customTermAlertDialog = new CustomTermAlertDialog(context, "Terms & Conditions", terms);
        customTermAlertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });

        customTermAlertDialog.show();
    }


}

