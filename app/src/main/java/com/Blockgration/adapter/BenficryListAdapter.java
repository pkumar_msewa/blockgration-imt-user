package com.Blockgration.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.Blockgration.Userwallet.R;
import com.Blockgration.model.BenfercyModel;

import java.util.ArrayList;

/**
 * Created by Amrutha-PC on 28-Feb-17.
 */

public class BenficryListAdapter extends BaseAdapter {
    ArrayList<BenfercyModel> countryModels;
    private Context context;


    public BenficryListAdapter(Context context, ArrayList<BenfercyModel> countryModels) {
        this.context = context;
        this.countryModels = countryModels;
    }


    @Override
    public int getCount() {

        return countryModels.size();
    }

    @Override
    public Object getItem(int i) {
        return countryModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return countryModels.indexOf(getItem(i));

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        View convertView = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            convertView = mInflater.inflate(R.layout.list_item, viewGroup, false);
            holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            convertView = view;
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtTitle.setText(countryModels.get(i).getFirstname());
        holder.imageView.setVisibility(View.GONE);
        return convertView;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
    }

}
