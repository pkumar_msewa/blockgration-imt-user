package com.Blockgration.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.flightinneracitivty.FlightPriceOneWayActivity;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.FlightListModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by Ksf on 10/13/2016.
 */
public class FlightListAdapter extends RecyclerView.Adapter<FlightListAdapter.RecyclerViewHolders> {

    private Context context;
    private List<FlightListModel> flightArray;
    private String destinationCode, sourceCode, dateOfJourney;
    private int adultNo, childNo, infantNo;
    private String flightClass;

    private String jsonToSend = "";
    private String jsonFareToSend = "";


    public FlightListAdapter(Context context, List<FlightListModel> flightArray, String sourceCode, String destinationCode, String dateOfJourney,int adultNo, int childNo,int infantNo, String flightClass, String jsonToSend, String jsonFareToSend) {
        this.context = context;
        this.flightArray = flightArray;
        this.sourceCode = sourceCode;
        this.destinationCode = destinationCode;
        this.dateOfJourney = dateOfJourney;
        this.flightClass = flightClass;
        this.adultNo = adultNo;
        this.childNo = childNo;
        this.infantNo = infantNo;
        this.jsonFareToSend = jsonFareToSend;
        this.jsonToSend = jsonToSend;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_flight_list, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders viewHolder, int position) {
        String totalDuration;
        final int currentPosition = position;
        if (flightArray.get(position).getFlightListArray().size() == 1) {
            viewHolder.tvFlightVia.setVisibility(View.GONE);
            viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName());

            viewHolder.tvFlightDepTime.setText(flightArray.get(position).getFlightListArray().get(0).getDepTime());

            viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get(0).getArrivalTime());

//            totalDuration = compareDate(flightArray.get(position).getFlightListArray().get(0).getDepTime(), flightArray.get(position).getFlightListArray().get(0).getArrivalTime());
            viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getFlightDuration());
            viewHolder.tvFlightRule.setText(flightArray.get(position).getFlightListArray().get(0).getFlightRule());
            viewHolder.tvFightType.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        } else {
            viewHolder.tvFlightVia.setVisibility(View.VISIBLE);
            viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName());
            viewHolder.tvFlightDepTime.setText(flightArray.get(position).getFlightListArray().get(0).getDepTime());
            viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get(0).getArrivalTime());


            //Checking connectivity
            if (flightArray.get(position).getFlightListArray().size() == 2) {
                //Stoppage
                viewHolder.tvFlightVia.setText("via " + flightArray.get(position).getFlightListArray().get(0).getArrivalAirportCode());
                //Time
                totalDuration = compareDate(flightArray.get(position).getFlightListArray().get(0).getFlightArrivalTimeZone()+"T"+flightArray.get(position).getFlightListArray().get(0).getDepTime(), flightArray.get(position).getFlightListArray().get(1).getFlightArrivalTimeZone()+"T"+flightArray.get(position).getFlightListArray().get(1).getArrivalTime());
                viewHolder.tvFlightTotalTime.setText(totalDuration);

            } else if (flightArray.get(position).getFlightListArray().size() == 3) {
                viewHolder.tvFlightVia.setText("via " + flightArray.get(position).getFlightListArray().get(0).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(1).getArrivalAirportCode());

                //Time
                totalDuration = compareDate(flightArray.get(position).getFlightListArray().get(0).getFlightArrivalTimeZone()+"T"+flightArray.get(position).getFlightListArray().get(0).getDepTime(),flightArray.get(position).getFlightListArray().get(2).getFlightArrivalTimeZone()+"T"+flightArray.get(position).getFlightListArray().get(2).getArrivalTime());
                viewHolder.tvFlightTotalTime.setText(totalDuration);

            } else if (flightArray.get(position).getFlightListArray().size() == 4) {
                viewHolder.tvFlightVia.setText("via " + flightArray.get(position).getFlightListArray().get(0).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(1).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(2).getArrivalAirportCode());

                //Time
                totalDuration = compareDate(flightArray.get(position).getFlightListArray().get(0).getFlightArrivalTimeZone()+"T"+flightArray.get(position).getFlightListArray().get(0).getDepTime(),flightArray.get(position).getFlightListArray().get(3).getFlightArrivalTimeZone()+"T"+ flightArray.get(position).getFlightListArray().get(3).getArrivalTime());
                viewHolder.tvFlightTotalTime.setText(totalDuration);
            } else if (flightArray.get(position).getFlightListArray().size() == 5) {
                viewHolder.tvFlightVia.setText("via " + flightArray.get(position).getFlightListArray().get(0).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(1).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(2).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(3).getArrivalAirportCode());

                //Time
                totalDuration = compareDate(flightArray.get(position).getFlightListArray().get(0).getFlightArrivalTimeZone()+"T"+flightArray.get(position).getFlightListArray().get(0).getDepTime(), flightArray.get(position).getFlightListArray().get(4).getFlightArrivalTimeZone()+"T"+flightArray.get(position).getFlightListArray().get(4).getArrivalTime());
                viewHolder.tvFlightTotalTime.setText(totalDuration);
            } else {
                viewHolder.tvFlightVia.setText("with " + (flightArray.get(position).getFlightListArray().size() - 1) + " Stops");
                //Time
                totalDuration = compareDate(flightArray.get(position).getFlightListArray().get(0).getFlightArrivalTimeZone()+"T"+flightArray.get(position).getFlightListArray().get(0).getDepTime(), flightArray.get(position).getFlightListArray().get(position).getFlightArrivalTimeZone()+"T"+flightArray.get(position).getFlightListArray().get(flightArray.get(position).getFlightListArray().size() - 1).getArrivalTime());
                viewHolder.tvFlightTotalTime.setText(totalDuration);
            }

            viewHolder.tvFlightRule.setText(flightArray.get(position).getFlightListArray().get(0).getFlightRule());
            viewHolder.tvFightType.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        }

        viewHolder.ivFightList.setImageResource(AppMetadata.getFLightImage(flightArray.get(position).getFlightListArray().get(0).getFlightCode().trim()));
        viewHolder.tvFlightFare.setText(context.getResources().getString(R.string.rupease) + " " + flightArray.get(position).getFlightNetFare());

        viewHolder.llFlightListMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent flightDetailIntent = new Intent(context, FlightPriceOneWayActivity.class);
                flightDetailIntent.putExtra("FlightArray", flightArray.get(currentPosition));
                flightDetailIntent.putExtra("sourceCode", sourceCode);
                flightDetailIntent.putExtra("destinationCode", destinationCode);
                flightDetailIntent.putExtra("dateOfJourney", dateOfJourney);

                flightDetailIntent.putExtra("adultNo", adultNo);
                flightDetailIntent.putExtra("childNo", childNo);
                flightDetailIntent.putExtra("infantNo", infantNo);
                flightDetailIntent.putExtra("flightClass",flightClass);
                flightDetailIntent.putExtra("jsonToSend",jsonToSend);
                flightDetailIntent.putExtra("jsonFareToSend",jsonFareToSend);


                flightDetailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(flightDetailIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.flightArray.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        public TextView tvFlightListName;
        public TextView tvFlightArrivalTime;
        public TextView tvFlightDepTime;
        public TextView tvFlightFare;
        public TextView tvFlightTotalTime;
        public TextView tvFightType;
        public TextView tvFlightRule;
        public TextView tvFlightVia;
        public LinearLayout llFlightListMain;
        public ImageView ivFightList;

        public RecyclerViewHolders(View convertView) {
            super(convertView);
            tvFlightListName = (TextView) convertView.findViewById(R.id.tvFightListName);
            tvFlightArrivalTime = (TextView) convertView.findViewById(R.id.tvFlightArrivalTime);
            tvFlightDepTime = (TextView) convertView.findViewById(R.id.tvFlightDepTime);
            tvFlightFare = (TextView) convertView.findViewById(R.id.tvFlightFare);
            tvFlightTotalTime = (TextView) convertView.findViewById(R.id.tvFlightTotalTime);
            llFlightListMain = (LinearLayout) convertView.findViewById(R.id.llFlightListMain);
            tvFightType = (TextView) convertView.findViewById(R.id.tvFightType);
            tvFlightRule = (TextView) convertView.findViewById(R.id.tvFlightRule);
            tvFlightVia = (TextView) convertView.findViewById(R.id.tvFlightVia);
            ivFightList = (ImageView) convertView.findViewById(R.id.ivFightList);
        }
    }

    private String compareDate(String depTime, String arrTime) {
        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy'T'HH:mm");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(depTime);
            d2 = format.parse(arrTime);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
            if (diffDays >= 1) {
                return diffDays + " day " + diffHours + " hrs" + diffMinutes + " mins";
            } else {
                return diffHours + " hrs " + diffMinutes + " mins";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}

