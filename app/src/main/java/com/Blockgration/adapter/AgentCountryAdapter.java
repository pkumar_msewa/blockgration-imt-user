package com.Blockgration.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.model.AgentModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Amrutha-PC on 28-Feb-17.
 */

public class AgentCountryAdapter extends BaseAdapter {
    ArrayList<AgentModel> countryModels;
    private Context context;


    public AgentCountryAdapter(Context context, ArrayList<AgentModel> countryModels) {
        this.context = context;
        this.countryModels = countryModels;
    }


    @Override
    public int getCount() {

        return countryModels.size();
    }

    @Override
    public Object getItem(int i) {
        return countryModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return countryModels.indexOf(getItem(i));

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        View convertView = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            convertView = mInflater.inflate(R.layout.list_agent, viewGroup, false);
            holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.tvAgentTittle);
            holder.imageView = (ImageView) convertView.findViewById(R.id.ivAgent);
            convertView.setTag(holder);
        } else {
            convertView = view;
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtTitle.setText(countryModels.get(i).getAgentName());
        if (i == 0) {
            Picasso.with(context).load(R.drawable.fgm).into(holder.imageView);

        } else if (i == 1) {
            Picasso.with(context).load(R.drawable.businesslogofgm).into(holder.imageView);
        }

        Log.i("Remittance", countryModels.get(i).getAgentLogo());

        return convertView;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
    }

}
