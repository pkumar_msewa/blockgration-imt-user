package com.Blockgration.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.Blockgration.model.CountryModel;

import java.util.List;


public class AddCurrencySpinnerAdapter extends ArrayAdapter<CountryModel> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<CountryModel> values;

    public AddCurrencySpinnerAdapter(Context context, int textViewResourceId, List<CountryModel> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public int getCount(){
        return values.size();
    }

    public long getItemId(int position){
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setPadding(16,8,8,8);
        label.setTextSize(16);
        label.setText(values.get(position).getCurrencyCode());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setPadding(16,8,8,8);
        label.setTextSize(16);
        label.setText(values.get(position).getCurrencyCode());
        return label;
    }



}
	


