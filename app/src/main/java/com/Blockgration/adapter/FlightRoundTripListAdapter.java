package com.Blockgration.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.FlightListModel;

import java.util.List;


/**
 * Created by Kashif-PC on 11/26/2016.
 */
public class FlightRoundTripListAdapter extends RecyclerView.Adapter<FlightRoundTripListAdapter.RecyclerViewHolders> {

    private Context context;
    private List<FlightListModel> flightArray;
    private String destinationCode, sourceCode, dateOfJourney;
    private int adultNo, childNo, infantNo;

    private int selectedPosition;

    public FlightRoundTripListAdapter(Context context, List<FlightListModel> flightArray, String sourceCode, String destinationCode, String dateOfJourney, int adultNo, int childNo, int infantNo) {
        this.context = context;
        this.flightArray = flightArray;
        this.sourceCode = sourceCode;
        this.destinationCode = destinationCode;
        this.dateOfJourney = dateOfJourney;

        this.adultNo = adultNo;
        this.childNo = childNo;
        this.infantNo = infantNo;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_roundtrip_flight_list, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders viewHolder, int position) {
        String totalDuration;
        final int currentPosition = position;
        if (flightArray.get(position).getFlightListArray().size() == 1) {
            viewHolder.tvFlightVia.setVisibility(View.GONE);
            viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName());
            viewHolder.tvFlightDepTime.setText(flightArray.get(position).getFlightListArray().get(0).getDepTime());
            viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get(0).getArrivalTime());
            viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getFlightDuration());
            viewHolder.tvFlightRule.setText(flightArray.get(position).getFlightListArray().get(0).getFlightRule());
            viewHolder.tvFightType.setText(flightArray.get(position).getFlightListArray().get(0).getFlightCode() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        } else {
            viewHolder.tvFlightVia.setVisibility(View.VISIBLE);
            viewHolder.tvFlightListName.setText(flightArray.get(position).getFlightListArray().get(0).getFlightName());
            viewHolder.tvFlightDepTime.setText(flightArray.get(position).getFlightListArray().get(0).getDepTime());

            viewHolder.tvFlightArrivalTime.setText(flightArray.get(position).getFlightListArray().get(0).getArrivalTime());


            //Checking connectivity
            if (flightArray.get(position).getFlightListArray().size() == 2) {
                //Stoppage
                viewHolder.tvFlightVia.setText("via " + flightArray.get(position).getFlightListArray().get(0).getArrivalAirportCode());
                viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getFlightDuration());

            } else if (flightArray.get(position).getFlightListArray().size() == 3) {
                viewHolder.tvFlightVia.setText("via " + flightArray.get(position).getFlightListArray().get(0).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(1).getArrivalAirportCode());
                viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getFlightDuration());

            } else if (flightArray.get(position).getFlightListArray().size() == 4) {
                viewHolder.tvFlightVia.setText("via " + flightArray.get(position).getFlightListArray().get(0).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(1).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(2).getArrivalAirportCode());
                viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getFlightDuration());
            } else if (flightArray.get(position).getFlightListArray().size() == 5) {
                viewHolder.tvFlightVia.setText("via " + flightArray.get(position).getFlightListArray().get(0).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(1).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(2).getArrivalAirportCode() + ", " + flightArray.get(position).getFlightListArray().get(3).getArrivalAirportCode());
                viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getFlightDuration());
            } else {
                viewHolder.tvFlightVia.setText("with " + (flightArray.get(position).getFlightListArray().size() - 1) + " Stops");
                viewHolder.tvFlightTotalTime.setText(flightArray.get(position).getFlightListArray().get(0).getFlightDuration());
            }


            viewHolder.tvFlightRule.setText(flightArray.get(position).getFlightListArray().get(0).getFlightRule());
            viewHolder.tvFightType.setText(flightArray.get(position).getFlightListArray().get(0).getFlightCode() + " " + flightArray.get(position).getFlightListArray().get(0).getFlightNo());
        }


        viewHolder.ivFightList.setImageResource(AppMetadata.getFLightImage(flightArray.get(position).getFlightListArray().get(0).getFlightCode()));
        viewHolder.tvFlightFare.setText(context.getResources().getString(R.string.rupease) + " " + flightArray.get(position).getFlightNetFare());


        if (isSelected(position)) {
            viewHolder.llFlightListMain.setSelected(true);
        } else {
            viewHolder.llFlightListMain.setSelected(false);
        }

//        sendUpdate(flightArray.get(currentPosition).getFlightListArray().get(0).getTripType(),flightArray.get(currentPosition).getFlightNetFare());

        viewHolder.llFlightListMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = currentPosition;
                if (view.isSelected()) {
                    view.setSelected(false);
                } else {
                    view.setSelected(true);
                }
                sendUpdate(flightArray.get(currentPosition).getFlightListArray().get(0).getTripType(),flightArray.get(currentPosition).getFlightNetFare());
                notifyDataSetChanged();

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.flightArray.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        public TextView tvFlightListName;
        public TextView tvFlightArrivalTime;
        public TextView tvFlightDepTime;
        public TextView tvFlightFare;
        public TextView tvFlightTotalTime;
        public TextView tvFightType;
        public TextView tvFlightRule;
        public TextView tvFlightVia;
        public LinearLayout llFlightListMain;
        public ImageView ivFightList;

        public RecyclerViewHolders(View convertView) {
            super(convertView);
            tvFlightListName = (TextView) convertView.findViewById(R.id.tvFightListName);
            tvFlightArrivalTime = (TextView) convertView.findViewById(R.id.tvFlightArrivalTime);
            tvFlightDepTime = (TextView) convertView.findViewById(R.id.tvFlightDepTime);
            tvFlightFare = (TextView) convertView.findViewById(R.id.tvFlightFare);
            tvFlightTotalTime = (TextView) convertView.findViewById(R.id.tvFlightTotalTime);
            llFlightListMain = (LinearLayout) convertView.findViewById(R.id.llFlightListMain);
            tvFightType = (TextView) convertView.findViewById(R.id.tvFightType);
            tvFlightRule = (TextView) convertView.findViewById(R.id.tvFlightRule);
            tvFlightVia = (TextView) convertView.findViewById(R.id.tvFlightVia);
            ivFightList = (ImageView) convertView.findViewById(R.id.ivFightList);
        }
    }



    private void sendUpdate(int tripType, double price){
        Intent intent = new Intent("flight-price");
        intent.putExtra("tripType", tripType);
        intent.putExtra("price-update", price);
        intent.putExtra("flightArray",flightArray.get(selectedPosition));
        intent.putExtra("position",selectedPosition);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


    private boolean isSelected(int position) {
        if (position != selectedPosition) {
            return false;
        } else {
            return true;
        }
    }
}
