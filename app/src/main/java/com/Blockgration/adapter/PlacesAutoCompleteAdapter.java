package com.Blockgration.adapter;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;


import com.Blockgration.model.AgentCityModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

    private ArrayList<AgentCityModel> resultList;

    private Context mContext;
    private int mResource;
    private JSONObject jsonObject;
    private String s;
    private AgentAPI mPlaceAPI = new AgentAPI();

    public PlacesAutoCompleteAdapter(Context context, int resource, JSONObject jsonObject, String s) {
        super(context, resource);

        this.mContext = context;
        this.mResource = resource;
        this.jsonObject = jsonObject;
        this.s = s;
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        Log.i("VALUE", String.valueOf(resultList.size()));
        return resultList.size();
    }

    @Override
    public String getItem(int position) {
        return resultList.get(position).getAgent_Name();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    try {
                        jsonObject.put("city_name", constraint.toString());
                        resultList = mPlaceAPI.autocomplete(jsonObject, mContext);
                        Log.i("value", String.valueOf(resultList.size()));
                        for (AgentCityModel model : resultList) {
                            filterResults.values = model.getAgent_Name();
                            filterResults.count = resultList.size();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

    public ArrayList<AgentCityModel> getArrayStringValue() {
        return resultList;
    }
}