package com.Blockgration.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.RetrieveDetailsActivity;
import com.Blockgration.model.CurrentAccountModel;
import com.Blockgration.model.StatementModel;
import com.orm.query.Select;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ksf on 5/8/2016.
 */
public class ReceiptAdapter extends BaseAdapter {

    private ViewHolder viewHolder;
    private Context context;
    private ArrayList<StatementModel> statementList;
    private List<CurrentAccountModel> currentAccountArray;
    private String ccc ;

    public ReceiptAdapter(Context context, ArrayList<StatementModel> statementList) {
        this.context = context;
        this.statementList = statementList;
    }

    @Override
    public int getCount() {
        return statementList.size();
    }

    @Override
    public Object getItem(int position) {
        return statementList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parentView) {
        final int currentPosition = position;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_receipt_news, null);
            viewHolder = new ViewHolder();
            viewHolder.tvReceiptDate = (TextView) convertView.findViewById(R.id.tvReceiptDate);
            viewHolder.tvReceiptCurrentBalance = (TextView) convertView.findViewById(R.id.tvReceiptCurrentBalance);
            viewHolder.tvReceiptAmount = (TextView) convertView.findViewById(R.id.tvReceiptAmount);
            viewHolder.tvReceiptDetails = (TextView) convertView.findViewById(R.id.tvReceiptDetails);
            viewHolder.tvReceiptStatus = (TextView) convertView.findViewById(R.id.tvReceiptStatus);
            viewHolder.tvReceiptTnxId = (TextView) convertView.findViewById(R.id.tvReceiptTnxId);
            currentAccountArray = Select.from(CurrentAccountModel.class).list();
            viewHolder.tvReceiptAuthRef = (TextView) convertView.findViewById(R.id.tvReceiptAuthRef);
            viewHolder.tvReceiptReteriveRef = (TextView) convertView.findViewById(R.id.tvReceiptReteriveRef);
            viewHolder.tvReteriveRefDetails = (TextView) convertView.findViewById(R.id.tvReteriveRefDetails);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ccc = currentAccountArray.get(0).getCurrencyCode();

        viewHolder.tvReceiptDetails.setText(statementList.get(position).getDescription());
        viewHolder.tvReceiptTnxId.setText("Ref no. " + statementList.get(position).getRefNo());
        viewHolder.tvReceiptCurrentBalance.setText("Wallet Balance: " + ccc + " " + statementList.get(position).getCurrentBalance());
//        viewHolder.tvReceiptCurrentBalance.setText("Wallet Balance: " + context.getResources().getString(R.string.rupease) + " " + statementList.get(position).getCurrentBalance());

        //MVISA
        viewHolder.tvReceiptAuthRef.setText("Auth Ref no. " + statementList.get(position).getAuthRefNo());
        viewHolder.tvReceiptReteriveRef.setText("Retrieve Ref no. " + statementList.get(position).getRetrievRefNo());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm" + " aaa");
        String dateString = dateFormat.format(new Date(Long.parseLong(statementList.get(position).getDateTime())));

        viewHolder.tvReceiptDate.setText(dateString);

        viewHolder.tvReceiptStatus.setText(statementList.get(position).getServiceStatus());
        if (statementList.get(position).isDebited()) {
            viewHolder.tvReceiptAmount.setText("Debit\n" + ccc+ " " + statementList.get(position).getAmountPaid());
        } else {
            viewHolder.tvReceiptAmount.setText("Credit\n" + ccc + statementList.get(position).getAmountPaid());
        }

//        if(statementList.get(position).getsCode().equals("SMR_CA")){
//            viewHolder.tvReteriveRefDetails.setVisibility(View.VISIBLE);
//            viewHolder.tvReteriveRefDetails.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent i = new Intent(context,RetrieveDetailsActivity.class);
//                    i.putExtra("rrn",statementList.get(position).getRetrievRefNo());
//                    context.startActivity(i);
//                }
//            });
//        }


        if (statementList.get(position).getRetrievRefNo().isEmpty() || statementList.get(position).getRetrievRefNo().equals("null")) {
            viewHolder.tvReceiptReteriveRef.setVisibility(View.GONE);
        } else {
            viewHolder.tvReceiptReteriveRef.setVisibility(View.VISIBLE);
        }
        if (statementList.get(position).getAuthRefNo().isEmpty() || statementList.get(position).getAuthRefNo().equals("null")) {
            viewHolder.tvReceiptAuthRef.setVisibility(View.GONE);
        } else {
            viewHolder.tvReceiptAuthRef.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    static class ViewHolder {
        TextView tvReceiptDate, tvReceiptCurrentBalance, tvReceiptAmount, tvReceiptStatus, tvReceiptDetails;
        TextView tvReceiptTnxId, tvReceiptReteriveRef, tvReceiptAuthRef, tvReteriveRefDetails;

    }
}
