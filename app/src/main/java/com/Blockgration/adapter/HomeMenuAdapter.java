package com.Blockgration.adapter;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

//import com.movieapp.in.activity.HomePageActivity;
import com.movieapp.in.activity.HomePageActivity;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.MainMenuDetailActivity;
import com.Blockgration.Userwallet.activity.MerchantPaymentActivity;
import com.Blockgration.Userwallet.activity.ScanToPAyActivity;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.metadata.AppMetadata;
import com.Blockgration.model.MainMenuModel;
import com.serviceOntario.in.activity.Home;

import java.util.ArrayList;


/**
 * Created by Ksf on 4/14/2016.
 */
public class HomeMenuAdapter extends RecyclerView.Adapter<HomeMenuAdapter.RecyclerViewHolders> {
    private ArrayList<MainMenuModel> itemList;
    private Context context;

    public HomeMenuAdapter(Context context, ArrayList<MainMenuModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_menu, null);
        return new RecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, int position) {
        holder.tvMenuItems.setText(itemList.get(position).getMenuTitle());
        holder.ivMenuItems.setImageResource(itemList.get(position).getMenuImage());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvMenuItems;
        public ImageView ivMenuItems;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvMenuItems = (TextView) itemView.findViewById(R.id.tvMenuItems);
            ivMenuItems = (ImageView) itemView.findViewById(R.id.ivMenuItems);

            if (!PreferenceManager.getDefaultSharedPreferences(context).getBoolean("preSwitchTheme", false)) {
                tvMenuItems.setTextColor(context.getResources().getColor(R.color.dark_text));
            } else {
                tvMenuItems.setTextColor(context.getResources().getColor(R.color.white_text));
            }
        }

        @Override
        public void onClick(View view) {
            int i = getAdapterPosition();
            Intent menuIntent = new Intent(context, MainMenuDetailActivity.class);
//            if (i == 0) {
//                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "MobileTopUp");
//                context.startActivity(menuIntent);
//            } else
            if (i == 0) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "BillPayment");
                context.startActivity(menuIntent);
            } else if (i == 1) {
                Intent intent = new Intent(context, ScanToPAyActivity.class);
                context.startActivity(intent);
            }
//            else if (i == 3) {
//                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "ExchangeMoney");
//                context.startActivity(menuIntent);
//            }
//            else if (i == 4) {
//                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "sendmoney");
//                context.startActivity(menuIntent);
//                CustomToast.showMessage(context, "Coming Soon");
//            }
            else if (i == 2) {
//                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Travel");
//                context.startActivity(menuIntent);
                CustomToast.showMessage(context, "Coming Soon");
            } else if (i == 3) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "sendmoney");
                context.startActivity(menuIntent);
            } else if (i == 4) {
                context.startActivity(new Intent(context, MerchantPaymentActivity.class));
//                CustomToast.showMessage(context, "Coming Soon");
            } else if (i == 5) {
                Intent intent = new Intent(context, HomePageActivity.class);
                context.startActivity(intent);
            } else if (i == 6) {
                Intent intent = new Intent(context, Home.class);
                context.startActivity(intent);
            } else if (i == 7) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Shopping");
                context.startActivity(menuIntent);
            } else if (i == 8) {
//                CustomToast.showMessage(context, "Coming Soon");
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "Gift Cards");
                context.startActivity(menuIntent);
            } else if (i == 9) {
                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "ChatBot");
                context.startActivity(menuIntent);
            }
//            else if (i == 10) {
//                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "SmartMobile");
//                context.startActivity(menuIntent);
//            }
//            else if (i == 10) {
//                CustomToast.showMessage(context, "Coming Soon");
//            } else if (i == 11) {
//
//            } else if (i == 12) {
//                CustomToast.showMessage(context, "Coming Soon");
//            } else if (i == 14) {
//                menuIntent.putExtra(AppMetadata.FRAGMENT_TYPE, "enter");
//                context.startActivity(menuIntent);
//                CustomToast.showMessage(context, "Coming Soon");
//            } else if (i == 13) {
//                Intent intent = new Intent(context, Home.class);
//                context.startActivity(intent);
//            } else if (i == 8) {
//                Intent intent = new Intent(context, HomePageActivity.class);
//                context.startActivity(intent);
//            }
            else {
                CustomToast.showMessage(context, "Coming Soon");
            }

        }
    }
}
