package com.Blockgration.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.Blockgration.Userwallet.R;
import com.Blockgration.Userwallet.activity.SmOfferDetailsActivity;
import com.Blockgration.custom.CustomTermAlertDialog;
import com.Blockgration.custom.CustomToast;
import com.Blockgration.model.CurrentAccountModel;
import com.Blockgration.model.SmartMobileAllOffersModel;
import com.orm.query.Select;
import com.squareup.picasso.Picasso;

import java.util.List;


public class SmartMobAllOffersAdapterNew extends BaseAdapter {

    private List<SmartMobileAllOffersModel> itemList;
    private Context context;
    private ViewHolder viewHolder;


    public SmartMobAllOffersAdapterNew(Context context, List<SmartMobileAllOffersModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }


    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.adapter_sm_all_offers, null);
            viewHolder = new SmartMobAllOffersAdapterNew.ViewHolder();
            viewHolder.tvSmartMobile = (TextView) convertView.findViewById(R.id.tvSmartMobile);
            viewHolder.ivSmartMobile = (ImageView) convertView.findViewById(R.id.ivSmartMobile);
//            viewHolder.btnTerms = (Button) convertView.findViewById(R.id.btnTerms);
            viewHolder.cvOffer = (CardView) convertView.findViewById(R.id.cvOffer);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SmartMobAllOffersAdapterNew.ViewHolder) convertView.getTag();
        }

        viewHolder.tvSmartMobile.setText(itemList.get(position).getTitle());

        AQuery aq = new AQuery(context);
        Log.i("URL", itemList.get(position).getLogo_url());
//        aq.id(viewHolder.ivSmartMobile).image(itemList.get(position).getLogo_url().replaceAll("\\\\", ""), true, true).background(R.drawable.loading_image);
        Picasso.with(context).load(itemList.get(position).getLogo_url()).placeholder(R.drawable.loading_image).into(viewHolder.ivSmartMobile);

/*        viewHolder.btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CustomToast.showMessage(context, "" + itemList.get(position).getOffer_key());
                Intent i = new Intent(context, SmOfferDetailsActivity.class);
                i.putExtra("offer_key", itemList.get(position).getOffer_key());
                context.startActivity(i);
            }
        });*/
        viewHolder.cvOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CustomToast.showMessage(context, "" + itemList.get(position).getOffer_key());
                Intent i = new Intent(context, SmOfferDetailsActivity.class);
                i.putExtra("offer_key", itemList.get(position).getOffer_key());
                context.startActivity(i);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        TextView tvSmartMobile;
        ImageView ivSmartMobile;
        //        Button btnTerms;
        CardView cvOffer;
    }


}

