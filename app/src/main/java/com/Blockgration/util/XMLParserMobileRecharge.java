package com.Blockgration.util;

import android.content.Context;

import com.Blockgration.model.MobileRechargeModel;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;


/**
 * Created by admin on 7/27/2015.
 */
public class XMLParserMobileRecharge {

    private String merchantName,merchantStreet;

//    // TODO XML Deal Child Node
    public static final String KEY_DEALS = "Deals";
    public static final String KEY_DEAL = "Deal";
    public static final String KEY_DEAL_ID = "DealID";
    public static final String KEY_DEAL_TITLE = "DealTitle";
    public static final String KEY_DEAL_SHORT_TITLE = "DealShortTitle";
    public static final String KEY_DEAL_Body = "DealBody";
    public static final String KEY_DEAL_RESTRICTIONS = "DealRestrictions";
    public static final String KEY_MERCHANTS = "Merchants";
    public static final String KEY_MERCHANT = "Merchant";
    public static final String KEY_MERCHANT_NAME = "MerchantName";
    public static final String KEY_MERCHANT_STREET = "MerchantStreet";
    public static final String KEY_MERCHANT_CITY = "MerchantCity";
    public static final String KEY_MERCHANT_PROVINCE = "MerchantProvince";
    public static final String KEY_MERCHANT_POSTAL_CODE = "MerchantPostalCode";
    public static final String KEY_MERCHANT_PHONE = "MerchantPhone";
    public static final String KEY_DEAL_CATEGORIES = "DealCategories";
    public static final String KEY_CATEGORY = "Category";
    public static final String KEY_DEAL_PUBLISH_DATE = "DealPublishDate";
    public static final String KEY_DEAL_START_DATE = "DealStartDate";
    public static final String KEY_DEAL_EXPIRES_DATE = "DealExpiresDate";
    public static final String KEY_DEAL_URI = "DealURL";
    public static final String KEY_DEAL_IMAGE_URL = "DealImageURL";
    public static final String KEY_DEAL_SAVINGS = "DealSavings";


    private static XMLParserUtils parser = new XMLParserUtils();
    private static String assetString;

    private MobileRechargeModel myCouponsModel;

    public ArrayList<MobileRechargeModel> getData(Context context) {
        String xml = parser.getXmlFromUrl("https://qa.valuetopup.com/posaservice/servicemanager.asmx?WSDL");
        Document doc = parser.getDomElement(xml);
        return getDealOnFromDocument(doc);
    }


    public ArrayList<MobileRechargeModel> getDealOnFromDocument(Document doc) {
        ArrayList<MobileRechargeModel> datas = new ArrayList<>();
        ArrayList<MobileRechargeModel> dealMerchants = new ArrayList<>();
        try {
            NodeList deals = doc.getElementsByTagName(KEY_DEALS);
            for (int i = 0; i < deals.getLength(); i++) {
                NodeList nl = doc.getElementsByTagName(KEY_DEAL);
                for (int j = 0; j < nl.getLength(); j++) {
                    Element e = (Element) nl.item(j);
                    String dealID = parser.getValueGroupOn(e, KEY_DEAL_ID);
                    String DealTitle = parser.getValueGroupOn(e, KEY_DEAL_TITLE);
                    String dealShortTitle = parser.getValueGroupOn(e, KEY_DEAL_SHORT_TITLE);
                    String dealBody = parser.getValueGroupOn(e, KEY_DEAL_Body);
                    String dealRestrictions = parser.getValueGroupOn(e, KEY_DEAL_RESTRICTIONS);
                    String merchants = parser.getValueGroupOn(e, KEY_MERCHANTS);

                    for (int k = 0; k < merchants.length(); k++) {
                        final String merchant = parser.getValueGroupOn(e, KEY_MERCHANT);
                        merchantName = parser.getValueGroupOn(e, KEY_MERCHANT_NAME);
                        merchantStreet = parser.getValueGroupOn(e, KEY_MERCHANT_STREET);
                        final String merchantCity = parser.getValueGroupOn(e, KEY_MERCHANT_CITY);
                        final String merchantProvince = parser.getValueGroupOn(e, KEY_MERCHANT_PROVINCE);
                        final String merchantPostalCode = parser.getValueGroupOn(e, KEY_MERCHANT_POSTAL_CODE);
                        final String merchantPhone = parser.getValueGroupOn(e, KEY_MERCHANT_PHONE);

                    }
                    String dealCategories = parser.getValueGroupOn(e, KEY_DEAL_CATEGORIES);
                    for (int s = 0; s < dealCategories.length(); s++) {
                        String category = parser.getValueGroupOn(e, KEY_CATEGORY);
                    }
                    String dealPublishDate = parser.getValueGroupOn(e, KEY_DEAL_PUBLISH_DATE);
                    String dealStartDate = parser.getValueGroupOn(e, KEY_DEAL_START_DATE);
                    String dealExpiresDate = parser.getValueGroupOn(e, KEY_DEAL_EXPIRES_DATE);
                    String dealURL = parser.getValueGroupOn(e, KEY_DEAL_URI);
                    String dealImageURL = parser.getValueGroupOn(e, KEY_DEAL_IMAGE_URL);
                    String dealSavings = parser.getValueGroupOn(e, KEY_DEAL_SAVINGS);

                    myCouponsModel = new MobileRechargeModel();
                    myCouponsModel.save();
                    datas.add(myCouponsModel);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return datas;
    }

}


