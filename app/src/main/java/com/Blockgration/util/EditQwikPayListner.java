package com.Blockgration.util;

/**
 * Created by Kashif-PC on 12/5/2016.
 */
public interface EditQwikPayListner {

    void onEditCompleted();
    void onEditError();
    void onSessionInvalid();
}
