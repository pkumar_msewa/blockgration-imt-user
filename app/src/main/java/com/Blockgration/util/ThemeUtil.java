package com.Blockgration.util;

import android.app.Activity;
import android.content.Intent;

import com.Blockgration.Userwallet.R;

/**
 * Created by Ksf on 8/12/2016.
 */
public class ThemeUtil {
    private static int cTheme;
    public final static int LIGHT = 0;
    public final static int DARK = 1;

    public static void changeToTheme(Activity activity, int theme) {
        cTheme = theme;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
    }

    public static void onActivityCreateSetTheme(Activity activity)  {
        switch (cTheme) {
            default:
            case LIGHT:
                activity.setTheme(R.style.AppThemeLight);
                break;

            case DARK:
                activity.setTheme(R.style.AppThemeDark);
                break;
        }
    }
}
