package com.Blockgration.util;

import android.text.TextUtils;
import android.util.Patterns;

import com.Blockgration.Userwallet.R;


public class PayingDetailsValidation {

    private PayingDetailsValidation() {
    }

    public static CheckLog checkPayQwikId(String firstName) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(firstName)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkFirstName(String firstName) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(firstName)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }


    public static CheckLog checkLastName(String lastName) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(lastName)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkPinCode(String pincode) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(pincode)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else if (pincode.length() != 6) {
            checkLog.msg = R.string.error_pin_six_digit;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkCCV(String ccv) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(ccv)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        }
//		if (pincode.length() != 6) {
//			checkLog.msg = R.string.error_pin_six_digit;
//			checkLog.isValid = false;
//			return checkLog;
//		}
        else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkEmail(String email) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(email)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            checkLog.msg = R.string.error_invalid_email;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkPassword(String password) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(password)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else if (password.length() < 6) {
            checkLog.msg = R.string.error_invalid_password;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkRePassword(String password, String rePassword) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(rePassword)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else if (!(rePassword.equals(password))) {
            checkLog.msg = R.string.error_mismatch_password;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkCountry(String country) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(country)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkPhone(String phone) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(phone)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkAmount(String amount) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(amount)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        }else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkAmountBelow1000(String amount) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(amount)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else if (Double.parseDouble(amount) == 0) {
            checkLog.msg = R.string.error_invalid_amount;
            checkLog.isValid = false;
            return checkLog;
        } else if (Double.parseDouble(amount) > 1000) {
            checkLog.msg = R.string.error_invalid_amount_1000;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkAmountBelow5000(String amount) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(amount)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else if (Double.parseDouble(amount) == 0) {
            checkLog.msg = R.string.error_invalid_amount;
            checkLog.isValid = false;
            return checkLog;
        } else if (Double.parseDouble(amount) > 5000) {
            checkLog.msg = R.string.error_invalid_amount_5000;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkAmountBelow10000(String amount) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(amount)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else if (Double.parseDouble(amount) == 0) {
            checkLog.msg = R.string.error_invalid_amount;
            checkLog.isValid = false;
            return checkLog;
        } else if (Double.parseDouble(amount) > 10000) {
            checkLog.msg = R.string.error_invalid_amount_10000;
            checkLog.isValid = false;
            return checkLog;
        } else if (Double.parseDouble(amount) < 10) {
            checkLog.msg = R.string.error_invalid_amount_10;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkGasCustomerAc(String acno) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(acno)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkElectricityCustomerAc(String acno) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(acno)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkElectricityUnit(String acno) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(acno)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkElecricityArea(String acno) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(acno)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else if (acno.length() != 2) {
            checkLog.msg = R.string.error_field_2_digit;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }

    }

    public static CheckLog checkArea(String acno) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(acno)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }

    }

    public static CheckLog checkLandlineCustomerAc(String acno) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(acno)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkLandlineNo(String acno) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(acno)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkDataCardNo(String acno) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(acno)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkInsurancePolicyNo(String acno) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(acno)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkMobile(String phone) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(phone)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkMobileTenDigit(String phone) {
        CheckLog checkLog = new CheckLog();
        int phonenumber = phone.length();
        double min = 6000000000.0;
        double max = 10000000000.0;
        if (TextUtils.isEmpty(phone)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else if (phonenumber < 10) {
            checkLog.msg = R.string.error_enter_ten_digit;
            return checkLog;
        } else if (!((Double.parseDouble(phone) >= min) && (Double.parseDouble(phone) < max))) {
            checkLog.msg = R.string.error_enter_valid_mob_number;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkMessage(String message) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(message)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkOTP(String otp) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(otp)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkAddress(String address) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(address)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkState(String state) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(state)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkCity(String city) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(city)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkZipCode(String zipCode) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(zipCode)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkIFSCCode(String ifscCode) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(ifscCode)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }

    public static CheckLog checkBankAccountNumber(String accountNumber) {
        CheckLog checkLog = new CheckLog();
        if (TextUtils.isEmpty(accountNumber)) {
            checkLog.msg = R.string.error_field_required;
            checkLog.isValid = false;
            return checkLog;
        } else {
            checkLog.isValid = true;
            return checkLog;
        }
    }


}
