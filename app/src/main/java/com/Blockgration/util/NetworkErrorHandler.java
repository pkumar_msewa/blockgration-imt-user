package com.Blockgration.util;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.Blockgration.Userwallet.R;


public class NetworkErrorHandler {

    public static String getMessage(VolleyError error, Context context) {
        NetworkResponse networkResponse = error.networkResponse;
        if (error instanceof AuthFailureError) {

            return  context.getResources().getString(R.string.server_error_auth_error);
        } else if (error instanceof NoConnectionError || error instanceof NetworkError) {
            return context.getResources().getString(R.string.server_error_no_connection);
        } else if (error instanceof ParseError) {
            return  context.getResources().getString(R.string.server_error_parse_error);
        } else if (error instanceof ServerError) {

            return  context.getResources().getString(R.string.server_error_server_error);
        } else if (error instanceof TimeoutError) {
            return  context.getResources().getString(R.string.server_error_timeout);
        } else {
            return "Unknown error";
        }

    }


}
