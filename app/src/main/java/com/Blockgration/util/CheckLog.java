package com.Blockgration.util;


import com.Blockgration.Userwallet.R;

public class CheckLog {

	public boolean isValid;
	public int msg;

	/*
	 * By default isValid is false and error is null
	 */
	public CheckLog() {
		isValid = false;
		msg = R.string.error_unknown;
	}
}
