package com.serviceOntario.uitl;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.serviceOntario.in.R;


/**
 * Created by Prajun Adhikary on 12/1/2015.
 */
public class DialogUtils {
    public static Dialog launchCustomDialog(Context context) {
        Dialog dialogLoading = new Dialog(context);
        dialogLoading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogLoading.setContentView(R.layout.dialog_loading_spinner);
        dialogLoading.setCancelable(false);
        dialogLoading.show();
        return dialogLoading;
    }
}
