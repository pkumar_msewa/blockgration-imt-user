package com.serviceOntario.uitl;

public class LogCat {

	private static final boolean showLog = true;

	public static void print(String message) {
		if (showLog) {
			if (message != null) {
				System.out.println(message);
			}
		}
	}

    public static void error(String message) {
        if (showLog) {
            if (message != null) {
                System.err.print(message);
            }
        }
    }

}
