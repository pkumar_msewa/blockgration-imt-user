package com.serviceOntario.uitl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {

    /*
    Generic JSON Keys
     */
    public static final String KEY_RESPONSE_CODE = "Response Code";
    public static final String KEY_RESPONSE_MESSAGE = "Response Message";

    /*
    Currency Exchange JSON Keys
     */
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_STATUS = "status_activity";
    public static final String KEY_DATA = "data";
    public static final String KEY_EXCHANGE_RATE = "exchangeRate";
    public static final String KEY_FEE = "fee";

    /*
    Account JSON Keys
     */
    public static final String KEY_CURRENCY = "Currency";
    public static final String KEY_ID = "Id";

    /*
    Group statement JSON Keys
     */
    public final static String KEY_DESCRIPTION = "description";
    public final static String KEY_TRANSACTION_DATE = "transactionDate";
    public final static String KEY_TRANSACTION_STATUS = "transactionStatus";
    public final static String KEY_TRANSACTION_FAVOURITE = "favourite";
    public final static String KEY_COMPLETED_DATE = "completedDate";
    public final static String KEY_AMOUNT = "amount";
    public final static String KEY_DEBIT = "debit";
    public final static String KEY_SERVICE_TRANSACTION_ID = "serviceTransactionId";
    public final static String KEY_SERVICE_TRS_UNIQUE_ID = "serviceTrsUniqueId";
    public final static String KEY_PROCESSED_BY = "processedBy";
    public final static String KEY_FAILURE_MESSAGE = "failureMessage";
    public final static String KEY_REVERTED_BY = "revertedBy";
    public final static String KEY_MERCHANT_NAME = "merchantName";
    public final static String KEY_SERVICE_NAME = "serviceName";
    public final static String KEY_TYPE = "type";
    public final static String KEY_PROPERTIES = "properties";
    public final static String KEY_TRANSACTIONS = "transactions";

    /*
    Login Response JSON Keys
     */
    public final static String KEY_SESSION_ID = "Session Id";
    public final static String KEY_REWARD_POINTS = "Reward Points";
    public final static String KEY_NAME = "Name";
    public final static String KEY_ID_P = "id";
    public final static String KEY_USER_ID = "userId";
    public final static String KEY_USER_NAME = "userName";
    public final static String KEY_REQUEST_DATE = "requestDate";
    public final static String KEY_ACCOUNT_ID = "accountId";
    public final static String KEY_ACCOUNT_TYPE = "accountType";
    public final static String KEY_ACTIVE_STATUS = "activeStatus";
    public final static String KEY_BALANCE = "balance";
    public final static String KEY_BLOCK_STATUS = "blockStatus";

    /*
    QR Code Scanner Result
     */
    public final static String KEY_SERVICE_CODE = "serviceCode";
    public final static String KEY_SERVICE_TYPE = "serviceType";
    public final static String KEY_PRODUCT_AMOUNT = "productAmount";
    public final static String KEY_PRODUCT_ID = "productId";
    public final static String KEY_TAX_AMOUNT = "taxAmount";
    public final static String KEY_PRODUCT_SERVICE_CHARGE = "productServiceCharge";
    public final static String KEY_PRODUCT_DELIVERY_CHARGE = "productDeliveryCharge";
    public final static String KEY_NARRATION = "narration";
    public final static String KEY_SERVICE_ID = "serviceId";
    public final static String KEY_UNIQUE_ID = "uniqueId";


	public static String getStringForKey(JSONObject jsonObj, String key) {
		try {
			String str = jsonObj.getString(key);
			return str;
		} catch (JSONException e) {
			e.printStackTrace();
			LogCat.error("JSON Exception for KEY :: " + key);
		}
		return null;
	}

	public static int getIntForKey(JSONObject jsonObj, String key) {
		try {
			int str = jsonObj.getInt(key);
			return str;
		} catch (JSONException e) {
			e.printStackTrace();
			LogCat.error("JSON Exception for KEY :: " + key);
		}
		return 0;
	}

    public static boolean getBooleanForKey(JSONObject jsonObj, String key) {
        try {
            boolean str = jsonObj.getBoolean(key);
            return str;
        } catch (JSONException e) {
            e.printStackTrace();
            LogCat.error("JSON Exception for KEY :: " + key);
        }
        return false;
    }

	public static double getDoubleForKey(JSONObject jsonObj, String key) {
		try {
			double str = jsonObj.getDouble(key);
			return str;
		} catch (JSONException e) {
			e.printStackTrace();
			LogCat.error("JSON Exception for KEY :: " + key);
		}
		return 0.0;
	}

	public static JSONObject getJSONObjectForKey(JSONObject jsonObj, String key) {
		try {
			JSONObject str = jsonObj.getJSONObject(key);
			return str;
		} catch (JSONException e) {
			e.printStackTrace();
			LogCat.error("JSON Exception for KEY :: " + key);
		}
		return null;
	}

    public static Object getObjectForKey(JSONObject jsonObj, String key) {
        try {
            Object str = jsonObj.get(key);
            return str;
        } catch (JSONException e) {
            e.printStackTrace();
            LogCat.error("JSON Exception for KEY :: " + key);
        }
        return null;
    }

	public static JSONArray getJSONArrayForKey(JSONObject jsonObj, String key) {
		try {
			JSONArray str = jsonObj.getJSONArray(key);
			return str;
		} catch (JSONException e) {
			e.printStackTrace();
			LogCat.error("JSON Exception for KEY :: " + key);
		}
		return null;
	}

	public static JSONArray getJSONArrayFormString(String str) {
		try {
			JSONArray jsonArray = new JSONArray(str);
			return jsonArray;
		} catch (JSONException e) {
			e.printStackTrace();
			LogCat.error("JSON Exception for STRING :: " + str);
		}
		return null;
	}

	public static JSONObject getJSONObjectFormString(String str) {
		try {
			JSONObject jsonObject = new JSONObject(str);
			return jsonObject;
		} catch (JSONException e) {
			e.printStackTrace();
			LogCat.error("JSON Exception for STRING :: " + str);
		}
		return null;
	}

	public static JSONObject getJSONObjectFromJSONArray(JSONObject array, int index) {
		try {
			return (JSONObject) array.get(String.valueOf(index));
		} catch (JSONException e) {
			e.printStackTrace();
			LogCat.error("JSON Exception for INDEX :: " + index);
		}
		return null;
	}

}
