package com.serviceOntario.uitl;


import com.serviceOntario.in.R;

public class CheckLog {

	public boolean isValid;
	public int msg;

	/*
	 * By default isValid is false and error is null
	 */
	public CheckLog() {
		isValid = false;
		msg = R.string.error_field_required;
	}
}
