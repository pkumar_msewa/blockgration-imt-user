package com.serviceOntario.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 11/6/2015.
 */
public class DTOHomeProduct implements Parcelable {

    private String productName;
    private int productImage;
    private String productDesc;

    public DTOHomeProduct() {
        this.productName = "";
        this.productImage = 0;
        this.productDesc = "";
    }

    public DTOHomeProduct(String productName, int productImage) {
        this.productName = productName;
        this.productImage = productImage;
        this.productDesc = productDesc;
    }

    protected DTOHomeProduct(Parcel in) {
        productName = in.readString();
        productImage = in.readInt();
        productDesc = in.readString();
    }

    public static final Creator<DTOHomeProduct> CREATOR = new Creator<DTOHomeProduct>() {
        @Override
        public DTOHomeProduct createFromParcel(Parcel in) {
            return new DTOHomeProduct(in);
        }

        @Override
        public DTOHomeProduct[] newArray(int size) {
            return new DTOHomeProduct[size];
        }
    };

    public String getProductPrice() {
        return productDesc;
    }

    public void setProductPrice(String productPrice) {
        this.productDesc = productPrice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductImage() {
        return productImage;
    }

    public void setProductImage(int productImage) {
        this.productImage = productImage;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(productName);
        dest.writeInt(productImage);
        dest.writeString(productDesc);
    }

}
