package com.serviceOntario.dto;


public class HomeMenu {

	private int icon;
	private String title;
	private String subTitle;
	private Class<?> activity;

	public HomeMenu(int icon, String title, String subTitle, Class<?> activity) {
		this.icon = icon;
		this.title = title;
		this.subTitle = subTitle;
		this.activity = activity;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public Class<?> getActivity() {
		return activity;
	}

	public void setActivity(Class<?> activity) {
		this.activity = activity;
	}

}
