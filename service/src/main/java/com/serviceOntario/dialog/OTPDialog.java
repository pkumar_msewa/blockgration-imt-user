package com.serviceOntario.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.serviceOntario.in.R;
import com.serviceOntario.apiUrls.SOAPIUrls;
import com.serviceOntario.requestHandler.TaskListener;
import com.serviceOntario.uitl.CheckLog;
import com.serviceOntario.uitl.DialogUtils;
import com.serviceOntario.uitl.JSONParser;
import com.serviceOntario.uitl.LogCat;
import com.serviceOntario.uitl.Validation;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by admin on 3/28/2016.
 */
public class OTPDialog {
    Context context;
    private TextView tvTitle;
    private TextView tvDetails;
    private TextView tvDescriptions;
    private FragmentManager supportFragmentManager;
    TaskListener taskListener;
    private TextInputLayout tillOTPNumber;
    private EditText etOTPNumber;
    private Dialog pDialog;
    private RequestQueue requestQueue;
    private boolean valid;
    private String OTP;
    private View focusView;

    public OTPDialog(Context context, final FragmentManager supportFragmentManager, TaskListener taskListener) {
        this.context = context;
        this.supportFragmentManager = supportFragmentManager;
        this.taskListener = taskListener;
    }

    public void show(final String TITLE, String MESSAGE, final String NOTE) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_otp);
        dialog.setCancelable(false);
        requestQueue = Volley.newRequestQueue(context);

        tillOTPNumber = (TextInputLayout) dialog.findViewById(R.id.tillOTPNumber);
        etOTPNumber = (EditText) dialog.findViewById(R.id.etOTPNumber);


        Button btnClose = (Button) dialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button btnResend = (Button) dialog.findViewById(R.id.resend);

        Button btnSubmit = (Button) dialog.findViewById(R.id.submit);


        btnResend.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             pDialog = DialogUtils.launchCustomDialog(context);
                                             pDialog.show();

                                             StringRequest stringRequest = new StringRequest(Request.Method.POST, SOAPIUrls.URL_RESND_OTP, new Response.Listener<String>() {
                                                 @Override
                                                 public void onResponse(String s) {
                                                     pDialog.cancel();
                                                     pDialog.dismiss();

                                                     try {
                                                         LogCat.print("RESPONSE" + s);
                                                         JSONObject jsonObject = new JSONObject(s);
                                                         String code = JSONParser.getStringForKey(jsonObject, "code");
                                                         String message = JSONParser.getStringForKey(jsonObject, "message");
                                                         if (code != null) {
                                                             if (code.equals("S00")) {
                                                                 pDialog.dismiss();

                                                             } else {
                                                                 Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                                             }
                                                         }
                                                     } catch (JSONException e) {
                                                         e.printStackTrace();
                                                     }

                                                 }
                                             }, new Response.ErrorListener() {
                                                 @Override
                                                 public void onErrorResponse(VolleyError volleyError) {

                                                     LogCat.print("ERROE" + volleyError);
                                                     if (volleyError.networkResponse == null) {
                                                         if (volleyError.getClass().equals(TimeoutError.class)) {
                                                             Toast.makeText(context,
                                                                     "Timeout error!",
                                                                     Toast.LENGTH_LONG).show();
                                                         }
                                                     }
                                                     pDialog.dismiss();
                                                     pDialog.cancel();
                                                     pDialog.dismiss();
                                                 }
                                             }) {
                                                 @Override
                                                 public Map<String, String> getHeaders() throws AuthFailureError {
                                                     HashMap<String, String> hashMap = new HashMap<String, String>();
                                                     hashMap.put("MobileNumber", NOTE);
                                                     LogCat.print("HASH" + hashMap);
                                                     return hashMap;
                                                 }
                                             };
                                             int socketTimeout = 60000;
                                             RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                                             stringRequest.setRetryPolicy(policy);
                                             requestQueue.add(stringRequest);


                                         }
                                     }

        );


        btnSubmit.setOnClickListener(new View.OnClickListener()

                                     {
                                         @Override
                                         public void onClick(View v) {
                                             pDialog = DialogUtils.launchCustomDialog(context);
                                             pDialog.show();
                                             StringRequest stringRequest = new StringRequest(Request.Method.POST, SOAPIUrls.URL_QTP, new Response.Listener<String>() {
                                                 @Override
                                                 public void onResponse(String s) {
                                                     pDialog.cancel();
                                                     pDialog.dismiss();

                                                     try {
                                                         LogCat.print("RESPONSE" + s);
                                                         JSONObject jsonObject = new JSONObject(s);
                                                         String code = JSONParser.getStringForKey(jsonObject, "code");
                                                         String message = JSONParser.getStringForKey(jsonObject, "message");
                                                         String details = JSONParser.getStringForKey(jsonObject, "details");
                                                         Toast.makeText(context, details, Toast.LENGTH_LONG).show();
                                                         if (code != null) {
                                                             if (code.equals("S00")) {
                                                                 pDialog.dismiss();
                                                                 dialog.dismiss();

                                                                 SucessDialog sucessDialog = new SucessDialog(context, supportFragmentManager, null);
                                                                 sucessDialog.show(TITLE, details, "Register");

                                                             } else {
                                                                 SucessDialog sucessDialog = new SucessDialog(context, null, null);
                                                                 sucessDialog.show(TITLE, details, "OK");
                                                             }
                                                         }
                                                     } catch (JSONException e) {
                                                         e.printStackTrace();
                                                     }

                                                 }
                                             }, new Response.ErrorListener() {
                                                 @Override
                                                 public void onErrorResponse(VolleyError volleyError) {

                                                     LogCat.print("ERROE" + volleyError);
                                                     SucessDialog sucessDialog = new SucessDialog(context, null, null);
                                                     sucessDialog.show(TITLE, "Server error, please try again.", "ok");
                                                     if (volleyError.networkResponse == null) {
                                                         if (volleyError.getClass().equals(TimeoutError.class)) {
                                                             Toast.makeText(context,
                                                                     "Timeout error!",
                                                                     Toast.LENGTH_LONG).show();
                                                         }
                                                     }
                                                     pDialog.dismiss();
                                                     pDialog.cancel();
                                                     pDialog.dismiss();
                                                 }
                                             }) {
                                                 @Override
                                                 public Map<String, String> getParams() throws AuthFailureError {
                                                     HashMap<String, String> hashMap = new HashMap<String, String>();
                                                     hashMap.put("mobileNumber", NOTE);
                                                     hashMap.put("key", etOTPNumber.getText().toString());
                                                     LogCat.print("HASH" + hashMap);
                                                     return hashMap;
                                                 }
                                             };
                                             int socketTimeout = 60000;
                                             RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                                             stringRequest.setRetryPolicy(policy);
                                             requestQueue.add(stringRequest);
                                         }


                                     }

        );
        dialog.show();
    }

    public boolean valid() {
        etOTPNumber.setError(null);
        valid = true;
        OTP = etOTPNumber.getText().toString();
        checkOTP(etOTPNumber, OTP);
        return valid;
    }

    private void checkOTP(EditText etOTPNumber, String otp) {
        CheckLog checkLog = Validation.checkOTP(otp);
        if (!checkLog.isValid) {
            etOTPNumber.setError(context.getString(checkLog.msg));
            focusView = etOTPNumber;
            valid = false;
        }
    }


}
