package com.serviceOntario.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.serviceOntario.in.R;
import com.serviceOntario.requestHandler.TaskListener;


/**
 * Created by admin on 3/28/2016.
 */
public class SucessDialog {
    Context context;
    private TextView tvTitle;
    private TextView tvDetails;
    private TextView tvDescriptions;
    private FragmentManager supportFragmentManager;
    TaskListener taskListener;

    public SucessDialog(Context context, final FragmentManager supportFragmentManager, TaskListener taskListener) {
        this.context = context;
        this.supportFragmentManager = supportFragmentManager;
        this.taskListener = taskListener;
    }

    public void show(String TITLE, String MESSAGE, String NOTE) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_no_internet);
        dialog.setCancelable(false);

        tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        tvDetails = (TextView) dialog.findViewById(R.id.tvDialogDetails);
        tvDescriptions = (TextView) dialog.findViewById(R.id.tvDialogDescription);

        tvTitle.setText(TITLE);
        tvDescriptions.setText(MESSAGE);

        Button btnCancel = (Button) dialog.findViewById(R.id.btnDialogCancel);
        btnCancel.setVisibility(View.VISIBLE);

        Button btnSettings = (Button) dialog.findViewById(R.id.btnDialogStart);

        if (NOTE.equals("Register")) {
            btnCancel.setText("LOGIN");
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    ((Activity) context).finish();
                }
            });
            btnSettings.setVisibility(View.GONE);
        } else {
            btnCancel.setText(NOTE);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btnSettings.setVisibility(View.GONE);
        }
        dialog.show();
    }
}
