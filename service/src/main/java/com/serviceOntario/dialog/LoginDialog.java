package com.serviceOntario.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.serviceOntario.in.R;
import com.serviceOntario.apiUrls.SOAPIUrls;
import com.serviceOntario.in.activity.MainActivity;
import com.serviceOntario.in.activity.monerispayment.MonerisWebViewActivity;
import com.serviceOntario.session.SessionPreferences;
import com.serviceOntario.uitl.CheckLog;
import com.serviceOntario.uitl.DialogUtils;
import com.serviceOntario.uitl.JSONParser;
import com.serviceOntario.uitl.LogCat;
import com.serviceOntario.uitl.Validation;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 4/30/2016.
 */
public class LoginDialog {
    private Context context;
    private EditText etMobileNumber;
    private EditText etPassword;
    private RequestQueue requestQueue;
    private Dialog pDialog;
    private boolean valid;
    private String Amount;
    private View focusView;
    private Button btndialoglogin, btndialogReg;
    private String password, mobileNumber;
    private FragmentManager supportFragmentManager;

    private SessionPreferences sessionPreferences;
    private TextInputLayout tillpassword, tillmobileNumber;

    public LoginDialog(Context context, String amount, FragmentManager supportFragmentManager) {
        this.context = context;
        this.Amount = amount;
        this.supportFragmentManager = supportFragmentManager;
    }

    public void showDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.logindialog);
        sessionPreferences = new SessionPreferences(context);

        etPassword = (EditText) dialog.findViewById(R.id.etPassword);
        etMobileNumber = (EditText) dialog.findViewById(R.id.etMobileNumber);
        tillmobileNumber = (TextInputLayout) dialog.findViewById(R.id.tillmobileNumber);
        tillpassword = (TextInputLayout) dialog.findViewById(R.id.tillpassword);
        requestQueue = Volley.newRequestQueue(context);
        btndialogReg = (Button) dialog.findViewById(R.id.btndialogReg);
        btndialoglogin = (Button) dialog.findViewById(R.id.btndialoglogin);
        btndialogReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        etMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tillmobileNumber.setErrorEnabled(true);
                tillmobileNumber.setError("Enter correct Service Ontario Mobile Number");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etMobileNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tillmobileNumber.setError(null);
                }
            }
        });
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tillpassword.setErrorEnabled(true);
                tillpassword.setError("Enter password minimum 6 digits number");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tillpassword.setError(null);
                }
            }
        });

        btndialoglogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validForm()) {
                    if (isNetworkAvaliable(context)) {
                        pDialog = DialogUtils.launchCustomDialog(context);
                        pDialog.show();
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, SOAPIUrls.URL_QP_LOGIN, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                pDialog.cancel();
                                pDialog.dismiss();

                                try {
                                    LogCat.print("RESPONSE" + s);
                                    JSONObject jsonObject = new JSONObject(s);
                                    String code = JSONParser.getStringForKey(jsonObject, "code");
                                    String message = JSONParser.getStringForKey(jsonObject, "message");
                                    if (code != null) {
                                        if (code.equals("S00")) {
                                            pDialog.dismiss();
                                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                            String details = JSONParser.getStringForKey(jsonObject, "details");
                                            JSONObject jsonObjectFormString = JSONParser.getJSONObjectFormString(details);
                                            assert jsonObjectFormString != null;
                                            for (int i = 0; i < jsonObjectFormString.length(); i++) {
                                                JSONObject userDetail = JSONParser.getJSONObjectForKey(jsonObjectFormString, "userDetail");
                                                LogCat.print("Userdetails" + userDetail);
                                                SessionPreferences.PQSession pqSession = new SessionPreferences.PQSession();
                                                String sessionId = JSONParser.getStringForKey(jsonObjectFormString, "sessionId");

                                                for (int j = 0; j < userDetail.length(); j++) {
                                                    String firstName = JSONParser.getStringForKey(userDetail, "firstName");
                                                    String middleName = JSONParser.getStringForKey(userDetail, "middleName");
                                                    String lastName = JSONParser.getStringForKey(userDetail, "lastName");
                                                    String address = JSONParser.getStringForKey(userDetail, "address");
                                                    String contactNo = JSONParser.getStringForKey(userDetail, "contactNo");
                                                    String email = JSONParser.getStringForKey(userDetail, "email");
                                                    pqSession.setFirstName(firstName);
                                                    pqSession.setMiddleName(middleName);
                                                    pqSession.setEmail(email);
                                                    pqSession.setLastName(lastName);
                                                    pqSession.setAddress(address);
                                                    pqSession.setMobile(contactNo);
                                                    pqSession.setBalance(null);
                                                    pqSession.setSessionId(sessionId);
                                                    dialog.dismiss();
                                                    sessionPreferences.clearSession();
                                                    sessionPreferences.UserDetails(pqSession);
                                                    Intent intent = new Intent(context, MainActivity.class);
                                                    intent.putExtra(MonerisWebViewActivity.EXTRA_AMOUNT, Amount);
                                                    ((Activity) context).startActivity(intent);
                                                }

                                            }
                                        } else {
                                            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {

                                LogCat.print("ERROE" + volleyError);
                                if (volleyError.networkResponse == null) {
                                    if (volleyError.getClass().equals(TimeoutError.class)) {
                                        Toast.makeText(context,
                                                "Timeout error!",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                                pDialog.dismiss();
                                pDialog.cancel();
                                pDialog.dismiss();
                            }
                        }) {
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> hashMap = new HashMap<String, String>();
                                hashMap.put("username", mobileNumber);
                                hashMap.put("password", password);
                                LogCat.print("HASH" + hashMap);
                                return hashMap;
                            }
                        };
                        int socketTimeout = 60000;
                        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                        stringRequest.setRetryPolicy(policy);
                        requestQueue.add(stringRequest);
                    } else {
                        Toast.makeText(context, "Please Turn on Internet connection", Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });
        dialog.show();


    }

    public static boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    private boolean validForm() {
        etPassword.setError(null);
        etMobileNumber.setError(null);
        valid = true;

        password = etPassword.getText().toString();
        mobileNumber = etMobileNumber.getText().toString();

        checkUserMobileNumber(mobileNumber, tillmobileNumber);
        checkUserPassword(password, tillpassword);
        return valid;
    }

    private void checkUserMobileNumber(String mobileNumber, TextInputLayout tillmobileNumber) {
        CheckLog checkLog = Validation.checkMobile(mobileNumber);
        if (!checkLog.isValid) {
            tillmobileNumber.setErrorEnabled(true);
            tillmobileNumber.setError(context.getString(checkLog.msg));
            focusView = tillmobileNumber;
            valid = false;
        }
    }

    private void checkUserPassword(String password, TextInputLayout tilpassword) {
        CheckLog checkLog = Validation.checkPassword(password);
        if (!checkLog.isValid) {
            tilpassword.setError(Html.fromHtml(context.getString(checkLog.msg)));
            focusView = tilpassword;
            valid = false;
        }
    }

}
