package com.serviceOntario.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.serviceOntario.in.R;


public class NoInternetDialog {

	private Context context;

	private TextView tvTitle;
	private TextView tvDetails;
	private TextView tvDescriptions;

	private static final String TITLE = "NO INTERNET CONNECTION";
	private static final String MESSAGE = "Please connect to internet and try again.";
	private static final String NOTE = "You can go to setting and turn on the internet connection.";

	public NoInternetDialog(Context context) {
		this.context = context;
	}

	public void showDialog() {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.dialog_no_internet);
		dialog.setCancelable(false);

		tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
		tvDetails = (TextView) dialog.findViewById(R.id.tvDialogDetails);
		tvDescriptions = (TextView) dialog.findViewById(R.id.tvDialogDescription);

		tvTitle.setText(TITLE);
		tvDescriptions.setText(MESSAGE);
		tvDetails.setText(NOTE);

		Button btnCancel = (Button) dialog.findViewById(R.id.btnDialogCancel);
		btnCancel.setText("Cancel");
        btnCancel.setVisibility(View.VISIBLE);

		Button btnSettings = (Button) dialog.findViewById(R.id.btnDialogStart);
		btnSettings.setText("Settings");

		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		btnSettings.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				context.startActivity(new Intent(Settings.ACTION_SETTINGS));
				dialog.dismiss();
			}
		});
		dialog.show();
	}

}
