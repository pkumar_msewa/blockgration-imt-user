package com.serviceOntario.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.serviceOntario.in.R;
import com.serviceOntario.in.activity.PaymentActivity;
import com.serviceOntario.in.activity.monerispayment.MonerisWebViewActivity;
import com.serviceOntario.uitl.CheckLog;
import com.serviceOntario.uitl.Validation;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by admin on 4/30/2016.
 */
public class LoadMoneyDialog {
    private Context context;
    private TextView tvTitle;
    private TextView tvDescriptions;
    private EditText etAmount;
    private RequestQueue requestQueue;
    private Dialog pDialog;
    private boolean valid;
    private String Amount;
    private EditText focusView;
    private RadioGroup radioGroup;
    private RadioButton radioSexButton;

    public LoadMoneyDialog(Context context) {
        this.context = context;
    }

    public void showDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogLayout = inflater.inflate(R.layout.loadmoneydialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogLayout);
        radioGroup = (RadioGroup) dialogLayout.findViewById(R.id.radioGroup);

        tvTitle = (TextView) dialogLayout.findViewById(R.id.tvTitle);
        tvDescriptions = (TextView) dialogLayout.findViewById(R.id.tvDialogDescription);
        etAmount = (EditText) dialogLayout.findViewById(R.id.tvDialogDetails);
        requestQueue = Volley.newRequestQueue(context);

        tvTitle.setText("PAYMENT");

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                int selectedId = radioGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioSexButton = (RadioButton) dialogLayout.findViewById(selectedId);
                if (isNetworkAvaliable(context)) {
                    if (radioSexButton.getText().toString().equals("Moneris Payment")) {

                        Intent i = new Intent(context, MonerisWebViewActivity.class);
                        i.putExtra(MonerisWebViewActivity.EXTRA_AMOUNT, "10");
                        ((Activity) context).startActivity(i);
                    } else {
                        Intent intent = new Intent(context, PaymentActivity.class);
                        ((Activity) context).startActivity(intent);
                    }
                } else {
                    NoInternetDialog noInternetDialog = new NoInternetDialog(context);
                    noInternetDialog.showDialog();
                }
            }


        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 0, 10, 0);
        nbutton.setLayoutParams(params);
        nbutton.setBackgroundColor(context.getResources().getColor(R.color.pq_blue));
        nbutton.setTextColor(context.getResources().getColor(R.color.pq_white));
        Button pbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setBackgroundColor(context.getResources().getColor(R.color.pq_blue));
        pbutton.setTextColor(context.getResources().getColor(R.color.pq_white));
        pbutton.setLayoutParams(params);


    }

    public static boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    private boolean validForm() {
        etAmount.setError(null);
        valid = true;

        Amount = etAmount.getText().toString();

        checkUserAmount(Amount, etAmount);
        return valid;
    }

    private void checkUserAmount(String password, EditText tilpassword) {
        CheckLog checkLog = Validation.checkAmount(password);
        if (!checkLog.isValid) {
            tilpassword.setError(Html.fromHtml(context.getString(checkLog.msg)));
            focusView = tilpassword;
            valid = false;
        }
    }

}
