package com.serviceOntario.session;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by Prajun Adhikary on 10/26/2015.
 */
public class SessionPreferences {

    private static final String PREF_LANGUAGE = "com.serviceOntario.in.language";
    private static final String PREF_SESSION = "com.serviceOntario.in.session";
    private static final String PREF_SESSION_LAST_NAME = "com.serviceOntario.in.session.lastName";
    private static final String PREF_SESSION_MIDDLE_NAME = "com.serviceOntario.in.session.middlename";
    private static final String PREF_SESSION_FIRST_NAME = "com.serviceOntario.in.session.name";
    private static final String PREF_SESSION_EMAIL = "com.serviceOntario.in.session.email";
    private static final String PREF_SESSION_BALANCE = "com.serviceOntario.in.session.balance";
    private static final String PREF_SESSION_REWARD = "com.serviceOntario.in.session.reward";
    private static final String PREF_SESSION_MOBILE = "com.serviceOntario.in.session.mobile";
    private static final String PREF_SESSION_ADDRESS = "com.serviceOntario.in.address";
    private static final String PREF_SESSION_ID = "com.serviceOntario.in.session.id";
    private static final String PREF_SESSION_VALID = "com.serviceOntario.in.session.valid";
    private static final String PREF_SESSION_DATE_OF_BIRTH = "com.serviceOntario.in.session.dateofbirth";
    private static final String PREF_SESSION_GENDER = "com.serviceOntario.in.session.gender";
    private static final String PREF_SESSION_LICENCE = "com.serviceOntario.in.session.licence";


    private volatile static PQSession instance;
    private SharedPreferences sharedPreferences;

    public SessionPreferences(Context context) {
        this.sharedPreferences = context.getSharedPreferences(PREF_SESSION, Context.MODE_PRIVATE);
    }

    public static String getPrefSessionDateOfBirth() {
        return PREF_SESSION_DATE_OF_BIRTH;
    }

    public static String getPrefSessionGender() {
        return PREF_SESSION_GENDER;
    }

    public static String getPrefSessionLicence() {
        return PREF_SESSION_LICENCE;
    }

    public static String getPrefLanguage() {
        return PREF_LANGUAGE;
    }

    public static String getPrefSession() {
        return PREF_SESSION;
    }

    public static String getPrefSessionLastName() {
        return PREF_SESSION_LAST_NAME;
    }

    public static String getPrefSessionMiddleName() {
        return PREF_SESSION_MIDDLE_NAME;
    }

    public static String getPrefSessionFirstName() {
        return PREF_SESSION_FIRST_NAME;
    }

    public static String getPrefSessionEmail() {
        return PREF_SESSION_EMAIL;
    }

    public static String getPrefSessionBalance() {
        return PREF_SESSION_BALANCE;
    }

    public static String getPrefSessionReward() {
        return PREF_SESSION_REWARD;
    }

    public static String getPrefSessionMobile() {
        return PREF_SESSION_MOBILE;
    }

    public static String getPrefSessionAddress() {
        return PREF_SESSION_ADDRESS;
    }

    public static String getPrefSessionId() {
        return PREF_SESSION_ID;
    }

    public static String getPrefSessionValid() {
        return PREF_SESSION_VALID;
    }

    public void UserDetails(PQSession pqSession) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_SESSION_ID, pqSession.getSessionId());
        editor.putString(PREF_SESSION_FIRST_NAME, pqSession.getFirstName());
        editor.putString(PREF_SESSION_MIDDLE_NAME, pqSession.getMiddleName());
        editor.putString(PREF_SESSION_LAST_NAME, pqSession.getLastName());
        editor.putString(PREF_SESSION_MOBILE, pqSession.getMobile());
        editor.putString(PREF_SESSION_EMAIL, pqSession.getEmail());
        editor.putString(PREF_SESSION_ADDRESS, pqSession.getAddress());
        editor.putString(PREF_SESSION_BALANCE, pqSession.getBalance());
        editor.putString(PREF_SESSION_GENDER, pqSession.getGender());
        editor.putString(PREF_SESSION_DATE_OF_BIRTH, pqSession.getDateofbirth());
        editor.putString(PREF_SESSION_LICENCE, pqSession.getLienceNo());

        editor.commit();
    }


    public void language(String lang) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_LANGUAGE, lang);
        editor.commit();
    }

    public String getLanguage() {
        return sharedPreferences.getString(PREF_LANGUAGE, null);
    }

    public void clearLanguage() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_LANGUAGE, null);
        editor.commit();
    }


    public PQSession getUserDetails() {
        PQSession pqSession = new PQSession();
        pqSession.setSessionId(sharedPreferences.getString(PREF_SESSION_ID, null));
        pqSession.setMobile(sharedPreferences.getString(PREF_SESSION_MOBILE, null));
        pqSession.setEmail(sharedPreferences.getString(PREF_SESSION_EMAIL, null));
        pqSession.setFirstName(sharedPreferences.getString(PREF_SESSION_FIRST_NAME, null));
        pqSession.setMiddleName(sharedPreferences.getString(PREF_SESSION_MIDDLE_NAME, null));
        pqSession.setLastName(sharedPreferences.getString(PREF_SESSION_LAST_NAME, null));
        pqSession.setBalance(sharedPreferences.getString(PREF_SESSION_BALANCE, null));
        pqSession.setLienceNo(sharedPreferences.getString(PREF_SESSION_LICENCE, null));
        pqSession.setGender(sharedPreferences.getString(PREF_SESSION_GENDER, null));
        pqSession.setDateofbirth(sharedPreferences.getString(PREF_SESSION_DATE_OF_BIRTH, null));
        pqSession.setAddress(sharedPreferences.getString(PREF_SESSION_ADDRESS, null));
        return pqSession;
    }


    public void clearSession() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_SESSION_ID, null);
        editor.putString(PREF_SESSION_FIRST_NAME, null);
        editor.putString(PREF_SESSION_MIDDLE_NAME, null);
        editor.putString(PREF_SESSION_LAST_NAME, null);
        editor.putString(PREF_SESSION_MOBILE, null);
        editor.putString(PREF_SESSION_EMAIL, null);
        editor.putString(PREF_SESSION_ADDRESS, null);
        editor.putString(PREF_SESSION_BALANCE, null);
        editor.putString(PREF_SESSION_DATE_OF_BIRTH, null);
        editor.putString(PREF_SESSION_GENDER, null);
        editor.putString(PREF_SESSION_LICENCE, null);
        editor.commit();
    }

    // Singleton
    public static class PQSession {
//        "userDetail": {
//        "username": "1000000002",
//                "firstName": "Prajun",
//                "middleName": "P",
//                "lastName": "Adhikary",
//                "address": "Koramangala",
//                "contactNo": "1000000002",
//                "userType": "User",
//                "authority": "ROLE_USER,ROLE_AUTHENTICATED",
//                "emailStatus": "Active",
//                "mobileStatus": "Active",
//                "email": "padhikary@msewa.com2"
//    },
//            "sessionId": "gfdrl3ge5w3avrghwhuxlnhr",
//            "accountDetail": {
//        "id": 7,
//                "created": 1459337641000,
//                "lastModified": null,
//                "version": 0,
//                "balance": 938,
//                "accountNumber": "PQ2",
//                "accountType": {
//            "id": 2,
//                    "created": 1459337637000,
//                    "lastModified": null,
//                    "version": 0,
//                    "name": "Non - KYC",
//                    "description": "Unverified Account",
//                    "code": "NONKYC",
//                    "monthlyLimit": 10000,
//                    "dailyLimit": 5000,
//                    "balanceLimit": 10000,
//                    "new": false
//        },
//        "new": false
//    }
//}

        private String firstName;
        private String middleName;
        private String lastName;
        private String address;
        private String mobile;
        private String Dateofbirth;
        private String lienceNo;
        private String gender;

        private String email;
        private String sessionId;
        private String balance;


        public PQSession() {

        }


        public static PQSession getInstance() {
            if (instance == null) {
                synchronized (SessionPreferences.class) {
                    if (instance == null) {
                        instance = new PQSession();
                    }
                }
            }
            return instance;
        }

        public String getDateofbirth() {
            return Dateofbirth;
        }

        public void setDateofbirth(String dateofbirth) {
            Dateofbirth = dateofbirth;
        }

        public String getLienceNo() {
            return lienceNo;
        }

        public void setLienceNo(String lienceNo) {
            this.lienceNo = lienceNo;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }


        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getSessionId() {
            return sessionId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }
    }
}
