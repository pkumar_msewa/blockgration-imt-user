package com.serviceOntario.in.fragment.login;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.serviceOntario.in.R;
import com.serviceOntario.apiUrls.SOAPIUrls;
import com.serviceOntario.in.activity.MainActivity;
import com.serviceOntario.in.activity.registration.RegistrationActivity;
import com.serviceOntario.session.SessionPreferences;
import com.serviceOntario.uitl.CheckLog;
import com.serviceOntario.uitl.DialogUtils;
import com.serviceOntario.uitl.JSONParser;
import com.serviceOntario.uitl.LogCat;
import com.serviceOntario.uitl.Validation;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 4/26/2016.
 */
public class LoginFragment extends Fragment {
    private View rootView;
    private EditText etpassword;
    private TextInputLayout tilpassword;
    private EditText etmobilenumber;
    private Button btnsubmit;
    private TextInputLayout tillmobileNumber;
    private Dialog pDialog;
    private RequestQueue requestQueue;
    private boolean valid;
    private String userMobileNumber;
    private String password;
    private SessionPreferences sessionPreferences;

    private View focusView;
    private Button btnRegister;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_login, container, false);
        tillmobileNumber = (TextInputLayout) rootView.findViewById(R.id.tillmobileNumber);
        requestQueue = Volley.newRequestQueue(getActivity());
        sessionPreferences = new SessionPreferences(getActivity());
        etmobilenumber = (EditText) rootView.findViewById(R.id.etmobilenumber);
        tilpassword = (TextInputLayout) rootView.findViewById(R.id.tilpassword);
        etpassword = (EditText) rootView.findViewById(R.id.etpassword);
        btnRegister = (Button) rootView.findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), RegistrationActivity.class);
                startActivity(i);
            }
        });
        btnsubmit = (Button) rootView.findViewById(R.id.btnsubmit);
        etmobilenumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tillmobileNumber.setErrorEnabled(true);
                tillmobileNumber.setError("Enter correct  Mobile Number");

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etmobilenumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tillmobileNumber.setError(null);
                }
            }
        });
        etpassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tilpassword.setError(null);
                }
            }
        });
        etpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilpassword.setErrorEnabled(true);
                tilpassword.setError("Enter password minimum 6 digits number");

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validForm()) {
                    if ((isNetworkAvaliable(getActivity()))) {
                        pDialog = DialogUtils.launchCustomDialog(getActivity());
                        pDialog.show();
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, SOAPIUrls.URL_QP_LOGIN, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                pDialog.cancel();
                                pDialog.dismiss();

                                try {

                                    LogCat.print("RESPONSE" + s);
                                    JSONObject jsonObject = new JSONObject(s);
                                    String code = JSONParser.getStringForKey(jsonObject, "code");
                                    String message = JSONParser.getStringForKey(jsonObject, "message");
                                    if (code != null) {
                                        if (code.equals("S00")) {
                                            pDialog.dismiss();
                                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                            String details = JSONParser.getStringForKey(jsonObject, "details");
                                            JSONObject jsonObjectFormString = JSONParser.getJSONObjectFormString(details);
                                            assert jsonObjectFormString != null;
                                            for (int i = 0; i < jsonObjectFormString.length(); i++) {
                                                JSONObject userDetail = JSONParser.getJSONObjectForKey(jsonObjectFormString, "userDetail");
                                                SessionPreferences.PQSession pqSession = new SessionPreferences.PQSession();
                                                String sessionId = JSONParser.getStringForKey(jsonObjectFormString, "sessionId");
                                                for (int j = 0; j < userDetail.length(); j++) {
                                                    String firstName = JSONParser.getStringForKey(userDetail, "firstName");
                                                    String middleName = JSONParser.getStringForKey(userDetail, "middleName");
                                                    String lastName = JSONParser.getStringForKey(userDetail, "lastName");
                                                    String address = JSONParser.getStringForKey(userDetail, "address");
                                                    String contactNo = JSONParser.getStringForKey(userDetail, "contactNo");
                                                    String email = JSONParser.getStringForKey(userDetail, "email");
                                                    String dateOfBirth = JSONParser.getStringForKey(userDetail, "dateOfBirth");
                                                    String gender = JSONParser.getStringForKey(userDetail, "gender");
                                                    String licenceNo = JSONParser.getStringForKey(userDetail, "licenceNo");
                                                    pqSession.setFirstName(firstName);
                                                    pqSession.setMiddleName(middleName);
                                                    pqSession.setEmail(email);
                                                    pqSession.setLastName(lastName);
                                                    pqSession.setAddress(address);
                                                    pqSession.setMobile(contactNo);
                                                    pqSession.setBalance(null);
                                                    pqSession.setSessionId(sessionId);
                                                    pqSession.setDateofbirth(dateOfBirth);
                                                    pqSession.setGender(gender);
                                                    pqSession.setLienceNo(licenceNo);
                                                    sessionPreferences.UserDetails(pqSession);
                                                    getActivity().finish();
                                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                                    startActivity(intent);
                                                }

                                            }
                                        } else {
                                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {

                                LogCat.print("ERROE" + volleyError);
                                if (volleyError.networkResponse == null) {
                                    if (volleyError.getClass().equals(TimeoutError.class)) {
                                        Toast.makeText(getActivity(),
                                                "Timeout error!",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                                pDialog.dismiss();
                                pDialog.cancel();
                                pDialog.dismiss();
                            }
                        }) {
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> hashMap = new HashMap<String, String>();
                                hashMap.put("username", userMobileNumber);
                                hashMap.put("password", password);
                                LogCat.print("HASH" + hashMap);
                                return hashMap;
                            }
                        };
                        int socketTimeout = 60000;
                        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                        stringRequest.setRetryPolicy(policy);
                        requestQueue.add(stringRequest);
                    } else {
                        Toast.makeText(getActivity(), "Please Turn on Internet connection", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
        return rootView;
    }

    public static boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public boolean validForm() {
        etmobilenumber.setError(null);
        etpassword.setError(null);
        valid = true;

        userMobileNumber = etmobilenumber.getText().toString();
        password = etpassword.getText().toString();

        checkuserMobileNumber(userMobileNumber, tillmobileNumber);
        checkpassword(password, tilpassword);


        return valid;
    }

    private void checkpassword(String password, TextInputLayout tilpassword) {
        CheckLog checkLog = Validation.checkPassword(password);
        if (!checkLog.isValid) {
            tilpassword.setErrorEnabled(true);
            tilpassword.setError(getString(checkLog.msg));
            focusView = tilpassword;
            valid = false;
        }
    }

    private void checkuserMobileNumber(String userMobileNumber, TextInputLayout tilpassword) {
        CheckLog checkLog = Validation.checkTenDigitNumber(userMobileNumber);
        if (!checkLog.isValid) {
            tilpassword.setErrorEnabled(true);
            tilpassword.setError(getString(checkLog.msg));
            focusView = tilpassword;
            valid = false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
//            String message=data.getStringExtra("MESSAGE");
//            textView1.setText(message);

        }
    }

    public void onBackPressed() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setMessage("Are you sure you want to exit QwikrPay Application?");
        alertBuilder.setCancelable(true);
        alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                getActivity().finish();
            }
        });
        alertBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
    }
}
