package com.serviceOntario.in.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.serviceOntario.in.R;
import com.serviceOntario.adapter.GridMenuAdapter;
import com.serviceOntario.dto.HomeMenu;
import com.serviceOntario.in.activity.AllDetailsActivity;
import com.serviceOntario.session.SessionPreferences;
import com.serviceOntario.uitl.LogCat;

import java.util.ArrayList;

/**
 * Created by admin on 4/25/2016.
 */
public class HomePageFragment extends Fragment {
    private View rootView;
    private GridView gridView;
    private GridMenuAdapter menuAdapter;
    private SessionPreferences sharedPreferences;
    private ArrayList<HomeMenu> homeMenu;

    //    String[] fransProductName = {"Conducteurs", "Véhicules", "Santé", "Identification et certificats", "Service Ontario", "Maison, terrains et biens meubles", "Agents de sécurité et enquêteurs privés", "Plein air", "Éducation et formation", "Emplacement", "Entreprise", "Gouvernement"};
    int[] Image = {R.drawable.drivers, R.drawable.vechical, R.drawable.health, R.drawable.certicate, R.drawable.icon, R.drawable.home, R.drawable.secq, R.drawable.outdoor, R.drawable.eduction, R.drawable.location_icon, R.drawable.briefcase, R.drawable.goverment};
    private ImageView tel_icon, msg_icon;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_home_page, container, false);
        LogCat.print("value" + getResources().getString(R.string.driver));
        String[] productName = {Html.fromHtml(getResources().getString(R.string.driver)).toString(), getResources().getString(R.string.Vechical), getResources().getString(R.string.Health), getResources().getString(R.string.Identification_and_Certificates), "Service Ontario", getResources().getString(R.string.Home), getResources().getString(R.string.Security_guards_and_private), getResources().getString(R.string.Outdoor), getResources().getString(R.string.Education_and_training), getResources().getString(R.string.Location), getResources().getString(R.string.Business), getResources().getString(R.string.Government)};
        gridView = (GridView) rootView.findViewById(R.id.gridView);
        homeMenu = new ArrayList<>();
        tel_icon = (ImageView) rootView.findViewById(R.id.tel_icon);
        msg_icon = (ImageView) rootView.findViewById(R.id.msg_icon);
        tel_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AllDetailsActivity.class);
                intent.putExtra(AllDetailsActivity.Extradetails, 13);
                startActivity(intent);
            }
        });
        msg_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AllDetailsActivity.class);
                intent.putExtra(AllDetailsActivity.Extradetails, 13);
                startActivity(intent);
            }
        });
        sharedPreferences = new SessionPreferences(getActivity());

        for (int i = 0; i < Image.length; i++) {
            homeMenu.add(new HomeMenu(Image[i], productName[i], null, null));
            menuAdapter = new GridMenuAdapter(getActivity(), homeMenu, getActivity().getSupportFragmentManager());
            gridView.setAdapter(menuAdapter);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        menuAdapter.notifyDataSetChanged();
    }
}
