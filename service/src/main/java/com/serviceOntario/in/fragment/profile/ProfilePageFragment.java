package com.serviceOntario.in.fragment.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.serviceOntario.in.R;
import com.serviceOntario.session.SessionPreferences;

/**
 * Created by admin on 5/3/2016.
 */
public class ProfilePageFragment extends Fragment {
    private View rootView;
    private TextView tvProfileName;
    private TextView tvProfileNumber;
    private TextView tvProfileEmail;
    private SessionPreferences sessionPreferences;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.profile_fragment, container, false);
        tvProfileName = (TextView) rootView.findViewById(R.id.tvProfileName);
        tvProfileNumber = (TextView) rootView.findViewById(R.id.tvProfileNumber);
        tvProfileEmail = (TextView) rootView.findViewById(R.id.tvProfileEmail);
        sessionPreferences = new SessionPreferences(getActivity());
        if (sessionPreferences.getUserDetails().getFirstName() != null && !sessionPreferences.getUserDetails().getFirstName().isEmpty()) {
            tvProfileName.setText(sessionPreferences.getUserDetails().getFirstName() + " " + sessionPreferences.getUserDetails().getMiddleName() + " " + sessionPreferences.getUserDetails().getLastName());
        }
        if (sessionPreferences.getUserDetails().getMobile() != null && !sessionPreferences.getUserDetails().getMobile().isEmpty()) {
            tvProfileNumber.setText(sessionPreferences.getUserDetails().getMobile());
        }
        if (sessionPreferences.getUserDetails().getEmail() != null && !sessionPreferences.getUserDetails().getEmail().isEmpty()) {
            tvProfileEmail.setText(sessionPreferences.getUserDetails().getEmail());
        }

        return rootView;
    }

}
