package com.serviceOntario.in.activity.status;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.serviceOntario.in.R;
import com.serviceOntario.in.activity.MainActivity;

/**
 * Created by admin on 5/2/2016.
 */
public class StatusActivity extends AppCompatActivity {

    public static final String EXTRA_STATUS = "status";
    private TextView tvOtherDetails;
    private TextView tvTransactionStatus;
    private TextView tvCongratulationTransactionStatus;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.status_activity);
        tvCongratulationTransactionStatus = (TextView) findViewById(R.id.tvCongratulationTransactionStatus);
        tvTransactionStatus = (TextView) findViewById(R.id.tvTransactionStatus);
        tvOtherDetails = (TextView) findViewById(R.id.tvOtherDetails);
        final String transactionStatus = getIntent().getStringExtra(EXTRA_STATUS);

        if (transactionStatus != null && transactionStatus.length() != 0) {
            if (transactionStatus.equals("Transaction Successful!")) {
                tvCongratulationTransactionStatus.setText("CONGRATULATIONS!");
                tvTransactionStatus.setText(transactionStatus);
            } else if (transactionStatus.equals("Payment Successful!")) {
                tvCongratulationTransactionStatus.setText("CONGRATULATIONS!");
                tvTransactionStatus.setText(transactionStatus);
            } else if (transactionStatus.equals("Transaction Cancelled!")) {
                tvCongratulationTransactionStatus.setText("OOPPS!!");
                tvTransactionStatus.setText(transactionStatus);
            } else if (transactionStatus.equals("Transaction Declined!")) {
                tvCongratulationTransactionStatus.setText("OOPPS!!");
                tvTransactionStatus.setText(transactionStatus);
            } else {
                tvCongratulationTransactionStatus.setText("OOPPS!!");
                tvTransactionStatus.setText("Sorry for the inconvenience.\nPlease try again");
            }
        }

        Button btnTransactionOK = (Button) findViewById(R.id.btnTransactionOK);

        assert btnTransactionOK != null;
        btnTransactionOK.setOnClickListener(new View.OnClickListener()

                                            {
                                                @Override
                                                public void onClick(View v) {
                                                    if (tvCongratulationTransactionStatus.getText().equals("CONGRATULATIONS!")) {
                                                        Intent i = new Intent(StatusActivity.this, MainActivity.class);
                                                        startActivity(i);

                                                    } else {
                                                        finish();
                                                    }
                                                }
                                            }

        );
    }
}
