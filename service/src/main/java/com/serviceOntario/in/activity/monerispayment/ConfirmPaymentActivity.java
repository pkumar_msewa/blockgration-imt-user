package com.serviceOntario.in.activity.monerispayment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.serviceOntario.in.R;
import com.serviceOntario.apiUrls.SOAPIUrls;
import com.serviceOntario.in.activity.status.StatusActivity;
import com.serviceOntario.session.SessionPreferences;
import com.serviceOntario.uitl.DialogUtils;
import com.serviceOntario.uitl.JSONParser;
import com.serviceOntario.uitl.LogCat;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ConfirmPaymentActivity extends Activity {

    private static final String amount = "100";

    private Button btnPay;
    private Dialog pDialog;
    private SessionPreferences sessionPreferences;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_);
        requestQueue = Volley.newRequestQueue(ConfirmPaymentActivity.this);
        sessionPreferences = new SessionPreferences(ConfirmPaymentActivity.this);

        btnPay = (Button) findViewById(R.id.btnPay);
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptPayment(amount);
            }
        });
    }

    private void attemptPayment(final String amount) {
        // fgmtest.firstglobalmoney.com:8034/Ontario/Api/v1/User/Licence/ProcessPQTransaction
        // amount in BODY
        // sessionId in HEADER
        pDialog = DialogUtils.launchCustomDialog(ConfirmPaymentActivity.this);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SOAPIUrls.URL_TRANSITION, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                pDialog.cancel();
                pDialog.dismiss();

                try {
                    LogCat.print("RESPONSE" + s);
                    JSONObject jsonObject = new JSONObject(s);
                    String code = JSONParser.getStringForKey(jsonObject, "code");
                    String message = JSONParser.getStringForKey(jsonObject, "message");
                    String details = JSONParser.getStringForKey(jsonObject, "details");
                    if (code != null) {
                        if (code.equals("S00")) {
                            Intent i = new Intent(ConfirmPaymentActivity.this, StatusActivity.class);
                            i.putExtra(StatusActivity.EXTRA_STATUS, "Payment Successful!");
                            startActivity(i);
                            finish();

                        } else {
                            Toast.makeText(ConfirmPaymentActivity.this, details, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                LogCat.print("ERROE" + volleyError);
//                if (volleyError.networkResponse == null) {
//                    if (volleyError.getClass().equals(TimeoutError.class)) {
//
//                    }
//                }
                pDialog.dismiss();
                pDialog.cancel();
                pDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("amount", amount);
                LogCat.print("HASH" + hashMap);
                return hashMap;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("sessionId", sessionPreferences.getUserDetails().getSessionId());
                Log.i("Transaction session id",sessionPreferences.getUserDetails().getSessionId());
                LogCat.print("HASH" + hashMap);
                return hashMap;
            }

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
}

