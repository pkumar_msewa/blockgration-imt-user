package com.serviceOntario.in.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;

import com.serviceOntario.in.R;
import com.serviceOntario.in.activity.mpin.MPINActivity;
import com.serviceOntario.in.activity.mpin.MPINPreference;
import com.serviceOntario.session.SessionPreferences;
import com.serviceOntario.uitl.LogCat;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by admin on 4/27/2016.
 */
public class SplashPageActivity extends AppCompatActivity {

    private ImageView ivspview;
    private int mColorCode;
    private Bitmap mFinalBitmap;
    private SessionPreferences sessionPreferences;
    private MPINPreference mpinPreference;
    private EditText etone, ettwo, etthree, etfoure;
    private RequestQueue requestQueue;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashpage);
        ivspview = (ImageView) findViewById(R.id.ivspview);
        sessionPreferences = new SessionPreferences(SplashPageActivity.this);
        mpinPreference = new MPINPreference(SplashPageActivity.this);
        requestQueue = Volley.newRequestQueue(SplashPageActivity.this);

//        String strColorCode = "#000000";
//        if (strColorCode != null && strColorCode.length() > 0 &&
//                strColorCode.length() == 7) {
//            mColorCode = Color.parseColor(strColorCode);
//
////            Get the image to be changed from the drawable, drawable-xhdpi, drawable-hdpi,etc folder.
//            Drawable sourceDrawable = getResources().getDrawable(R.drawable.logowithtagline);
////
////        Convert drawable in to bitmap
//            Bitmap sourceBitmap = DrawableChange.convertDrawableToBitmap(sourceDrawable);
////
////        Pass the bitmap and color code to change the icon color dynamically.
////
//            mFinalBitmap = DrawableChange.changeImageColor(sourceBitmap, mColorCode);
////
//            ivspview.setImageBitmap(mFinalBitmap);
//        }
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      if (sessionPreferences.getUserDetails().getSessionId() != null && !sessionPreferences.getUserDetails().getSessionId().isEmpty()) {
                                          LogCat.print("SESSIONID" + sessionPreferences.getUserDetails().getSessionId());
                                          if (mpinPreference.isMPINSet()) {
                                              startActivity(new Intent(SplashPageActivity.this, MPINActivity.class));
                                          } else {
                                              startActivity(new Intent(SplashPageActivity.this, MainActivity.class));
                                          }
                                          finish();
                                      } else {
                                          startActivity(new Intent(SplashPageActivity.this, MainActivity.class));
                                          finish();
                                      }
                                  }
                              }
                );
            }
        };
        thread.start();
    }


}
