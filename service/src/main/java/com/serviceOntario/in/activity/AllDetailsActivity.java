package com.serviceOntario.in.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.serviceOntario.in.R;
import com.serviceOntario.adapter.detailsAdapter;
import com.serviceOntario.dto.DTOHomeProduct;
import com.serviceOntario.session.SessionPreferences;

import java.util.ArrayList;

public class AllDetailsActivity extends AppCompatActivity {

    public static String Extradetails = "number";
    String[] title = {"Drivers", "Vehicles", "Health", "Identification and certificates", "Service Ontario", "Home,land and personal", "Security guards and private", "Outdoor", "Education and training", "Location", "Business", "Government", "ss", "ss"};
    public static String[] driver = {"Ontario Driver's Licence", "Driver Licence Renewal", "Enhanced driver's licence", "Get an accessible parking permit", "Renew, replace or change an accessible parking permit", "Driver records", "Change address: driver's licence", "First-time driver: get a licence", "Renew a driver's licence: outside Ontario", "Exchange out-of-province or out-of-country driver's licence", "Replace a driver's licence: outside Ontario", "Renew a G driver's licence: 80 years and over", "Sign up for email renewal reminders"};
    public static String[] Vehicles = {"Vehicle Licence Plate Renewal", "Vehicle registration: plate/permit", "Licence plate sticker renewal", "Personalized licence plates", "Used vehicle information package (UVIP)", "Buy/sell a used vehicle in Ontario", "Register a farm vehicle", "Vehicle records", "Special permit", "Temporary licence plate sticker", "Change address: vehicle permit", "Licence plate sticker renewal: outside Ontario", "Get vintage licence plates for classic cars", "Get, renew or replace a garage licence", "Sign up for email renewal reminders"};
    public static String[] Health = {"Change of Information", "Registration for Ontario Health Insurance Coverage", "Change of Address", "Replacement Declaration", "Health card renewal", "Switch to a photo health card", "First-time health card", "Change address: health card", "Organ and tissue donation registration"};
    public static String[] Home_land_and_personal_property = {"Change your address", "Land registration", "Register or search for a personal property lien"};
    public static String[] Identification_and_certificates = {"Ontario Photo Card", "Newborn registration (4-in-1 Newborn Bundle)", "Birth certificate", "Marriage certificate", "Death certificate", "Change your last name", "Change a child's name", "Change name (for adults)", "Sex designation change",};
    public static String[] Security_guards_and_private_investigators = {"Apply for an individual licence", "Apply for an agency sole proprietorship licence", "Apply for an agency partnership licence", "Apply for an agency corporation licence", "Register as an employer"};
    public static String[] Outdoor = {"Outdoors Card", "Fishing licence", "Hunting licence"};
    public static String[] Education_and_training = {"Register a Private Career College in Ontario (for colleges)", "Search for registered Private Career Colleges in Ontario (for students)", "OSAP: Ontario Student Assistance Program"};
    //french
    public static String[] driverFrance = {"Demande de permis de conduire  de l'Ontario", "Demande de renouvellement du permis de conduire", "Permis de conduire Plus", "Obtenir un permis de stationnement accessible", "Renouveler, remplacer ou modifier les renseignements sur un permis de stationnement accessible", "Dossier de conducteur", "Changer l’adresse sur le permis de conduire", "Conducteurs novices : obtenir un permis", "Échanger un permis de conduire d’une autre province ou d’un autre pays", "Renouveler un permis de conduire pendant un séjour à l’extérieur de l’Ontario", "Remplacer un permis de conduire pendant un séjour à l’extérieur de l’Ontario", "Renouvellement du permis de conduire : personnes âgées de 80 ans ou plus", "S’inscrire aux rappels par courriel"};

    public static String[] Outdoor_french = {"Carte Plein air", "Permis de pêche", "Permis de chasse"};

    public static String[] Education_and_training_french = {"Inscrire un collège privé d’enseignement professionnel en Ontario (collèges)", "Trouver un collège privé d’enseignement professionnel en Ontario (étudiants)", "RAFEO : Régime d’aide financière aux étudiantes et étudiants de l’Ontario"};
    public static String[] Vehicles_french = {"Demande de renouvellement d'immatriculation (plaque) de vehicule", "Demande d'immatriculation d'un vehicule", "Renouveler une vignette d’immatriculation", "Plaques d’immatriculation personnalisée", "Trousse d’information sur les véhicules d’occasion", "Acheter ou vente un véhicule d’occasion ", " Faire immatriculer un véhicule agricole", " Dossier de véhicule", " Permis spécial ", " Vignette d’immatriculation temporaire", " Changer l’adresse sur un certificat d’immatriculation", " Renouveler ou remplacer une vignette d’immatriculation pendant un séjour à l’extérieur de l’Ontario", " Plaques d’immatriculation de collection pour voitures anciennes", " Obtenir, renouveler ou remplacer un permis de garage ", " S’inscrire aux rappels par courriel"};
    public static String[] Identification_and_certificates_french = {"Carte-photo de l’Ontario", "Enregistrer une naissance (service 4 en 1)", "Certificat de naissance", "Certificat de mariage", "Certificat de décès", "Changement de nom de famille", "Changer le nom d'un enfant", "Changement de nom personnes adultes", "Modifier la désignation du sexe"};
    public static String[] Home_land_and_personal_property_french = {"Changement d’adresse", "Enregistrement immobilier", "Enregistrer une sûreté ou rechercher un privilège"};
    public static String[] Health_french = {"Changement d'information", "Inscription pour la couverture d'assurance santé de l'Ontario ", "Changement d'adresse", "Déclaration de remplacement", " Renouveler la carte Santé", " Passez à la carte Santé avec photo", " Première carte Santé", " Changer l’adresse sur la carte Santé", " S'inscrire au don d’organes et de tissus humains"};
    public static String[] Security_guards_and_private_investigators_french = {" Obtenir un permis d’agent ou d’enquêteur", " Obtenir un permis d’agence (entreprise individuelle)", " Obtenir un permis d’agence (société de personnes)", " Obtenir un permis d’agence (sociétés par actions)", " S’inscrire comme employeur1)"};
    String[] fransProductName = {"Conducteurs", "Véhicules", "Santé", "Identification et certificats", "Service Ontario", "Maison, terrains et biens meubles", "Agents de sécurité et enquêteurs privés", "Plein air", "Éducation et formation", "Emplacement", "Entreprise", "Gouvernement"};
    private ImageView ivmvelmage1;
    private TextView tvActionBarText;
    private Button btnVideo;
    private ListView listView;
    private TextView titles;
    private ImageView commingsoon;
    private Bitmap mFinalBitmap;
    private int mColorCode;
    private SessionPreferences sessionPreferences;
    private ImageView back;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alldetails);
        int name = getIntent().getIntExtra(AllDetailsActivity.Extradetails, 0);
        listView = (ListView) findViewById(R.id.listView);
        titles = (TextView) findViewById(R.id.title);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        commingsoon = (ImageView) findViewById(R.id.commingsoon);
        sessionPreferences = new SessionPreferences(AllDetailsActivity.this);
        ArrayList<DTOHomeProduct> dtoHomeProducts = new ArrayList<>();
//        if (sessionPreferences.getLanguage() != null && !sessionPreferences.getLanguage().isEmpty()) {
//            if (sessionPreferences.getLanguage().equals("ENGLISH")) {
                titles.setText(title[name]);
//            } else {
//                titles.setText(fransProductName[name]);
//            }
//        }
        listView.setVisibility(View.VISIBLE);
        commingsoon.setVisibility(View.GONE);
        if (name == 0) {
            for (int j = 0; j < driver.length; j++) {
//                if (sessionPreferences.getLanguage().equals("ENGLISH")) {
                    dtoHomeProducts.add(new DTOHomeProduct(driver[j], 0));
//                } else {
//                    dtoHomeProducts.add(new DTOHomeProduct(driverFrance[j], 0));
//                }
                detailsAdapter detailsAdapter = new detailsAdapter(AllDetailsActivity.this, dtoHomeProducts);
                listView.setAdapter(detailsAdapter);
            }
        }
        if (name == 1) {
            for (int j = 0; j < Vehicles.length; j++) {
//                if (sessionPreferences.getLanguage().equals("ENGLISH")) {
                    dtoHomeProducts.add(new DTOHomeProduct(Vehicles[j], 0));
////                } else {
//                    dtoHomeProducts.add(new DTOHomeProduct(Vehicles_french[j], 0));
//                }
                detailsAdapter detailsAdapter = new detailsAdapter(AllDetailsActivity.this, dtoHomeProducts);
                listView.setAdapter(detailsAdapter);
            }
        }
        if (name == 3) {
            for (int j = 0; j < Identification_and_certificates.length; j++) {
//                if (sessionPreferences.getLanguage().equals("ENGLISH")) {
                    dtoHomeProducts.add(new DTOHomeProduct(Identification_and_certificates[j], 0));
//                } else {
//                    dtoHomeProducts.add(new DTOHomeProduct(Identification_and_certificates_french[j], 0));
//                }
                detailsAdapter detailsAdapter = new detailsAdapter(AllDetailsActivity.this, dtoHomeProducts);
                listView.setAdapter(detailsAdapter);
            }
        }
        if (name == 2) {
            for (int j = 0; j < Health.length; j++) {
//                if (sessionPreferences.getLanguage().equals("ENGLISH")) {
                    dtoHomeProducts.add(new DTOHomeProduct(Health[j], 0));
//                } else {
//                    dtoHomeProducts.add(new DTOHomeProduct(Health_french[j], 0));
//                }
                detailsAdapter detailsAdapter = new detailsAdapter(AllDetailsActivity.this, dtoHomeProducts);
                listView.setAdapter(detailsAdapter);
            }
        }
        if (name == 5) {
            for (int j = 0; j < Home_land_and_personal_property.length; j++) {
//                if (sessionPreferences.getLanguage().equals("ENGLISH")) {
                    dtoHomeProducts.add(new DTOHomeProduct(Home_land_and_personal_property[j], 0));
//                } else {
                    dtoHomeProducts.add(new DTOHomeProduct(Home_land_and_personal_property_french[j], 0));
//                }
                detailsAdapter detailsAdapter = new detailsAdapter(AllDetailsActivity.this, dtoHomeProducts);
                listView.setAdapter(detailsAdapter);
            }
        }
        if (name == 6) {
            for (int j = 0; j < Security_guards_and_private_investigators.length; j++) {
//                if (sessionPreferences.getLanguage().equals("ENGLISH")) {
//                    dtoHomeProducts.add(new DTOHomeProduct(Security_guards_and_private_investigators[j], 0));
//                } else {
                    dtoHomeProducts.add(new DTOHomeProduct(Security_guards_and_private_investigators[j], 0));

//                }
                detailsAdapter detailsAdapter = new detailsAdapter(AllDetailsActivity.this, dtoHomeProducts);
                listView.setAdapter(detailsAdapter);
            }
        }
        if (name == 7) {
            for (int j = 0; j < Outdoor.length; j++) {
//                if (sessionPreferences.getLanguage().equals("ENGLISH")) {
                    dtoHomeProducts.add(new DTOHomeProduct(Outdoor[j], 0));
//                } else {
//                    dtoHomeProducts.add(new DTOHomeProduct(Outdoor_french[j], 0));
//                }
                detailsAdapter detailsAdapter = new detailsAdapter(AllDetailsActivity.this, dtoHomeProducts);
                listView.setAdapter(detailsAdapter);
            }
        }
        if (name == 8) {
            for (int j = 0; j < Education_and_training.length; j++) {
//                if (sessionPreferences.getLanguage().equals("ENGLISH")) {
                    dtoHomeProducts.add(new DTOHomeProduct(Education_and_training[j], 0));
//                } else {
//                    dtoHomeProducts.add(new DTOHomeProduct(Education_and_training_french[j], 0));
//                }
                detailsAdapter detailsAdapter = new detailsAdapter(AllDetailsActivity.this, dtoHomeProducts);
                listView.setAdapter(detailsAdapter);
            }
        }
        if (name == 9) {
            commingsoon.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }

        if (name == 11) {
            commingsoon.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            String strColorCode = "#006B3F";
//
        }

        if (name == 10) {
            commingsoon.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }
        if (name == 12) {
            commingsoon.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }
        if (name == 13) {
            commingsoon.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }


    }
}

