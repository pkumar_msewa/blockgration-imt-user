package com.serviceOntario.in.activity.mpin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.serviceOntario.in.R;
import com.serviceOntario.in.activity.MainActivity;

public class MPINActivity extends Activity {

    private MPINPreference mpinPreference;

    private EditText etMPIN;
    private Button btnMPINProceed;

    private String mpin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mpin);

        mpinPreference = new MPINPreference(MPINActivity.this);

        etMPIN = (EditText) findViewById(R.id.etMPIN);
        btnMPINProceed = (Button) findViewById(R.id.btnMPINProceed);
        btnMPINProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpin = etMPIN.getText().toString();
                if (mpin != null && mpin.length() != 0) {
                    if (mpinPreference.checkMPIN(mpin)) {
                        // TODO Show main activity
                        Intent i = new Intent(MPINActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        // TODO Show error
                        etMPIN.setError("MPIN not matched");
                        etMPIN.requestFocus();
                    }
                } else {
                    etMPIN.setError("Required");
                    etMPIN.requestFocus();
                }
            }
        });
    }

}
