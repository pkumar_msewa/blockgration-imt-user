package com.serviceOntario.in.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.serviceOntario.in.R;
import com.serviceOntario.in.activity.monerispayment.ConfirmPaymentActivity;

/**
 * Created by admin on 3/15/2016.
 */
public class PaymentActivity extends AppCompatActivity {

    private ImageView backarrow;
    private Button btnPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment);
        backarrow = (ImageView) findViewById(R.id.back);
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnPay = (Button) findViewById(R.id.btnPay);
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PaymentActivity.this, ConfirmPaymentActivity.class));

            }
        });

    }
}
