package com.serviceOntario.in.activity.monerispayment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.serviceOntario.in.R;
import com.serviceOntario.apiUrls.SOAPIUrls;
import com.serviceOntario.in.activity.status.StatusActivity;
import com.serviceOntario.session.SessionPreferences;
import com.serviceOntario.uitl.LogCat;

import org.apache.http.util.EncodingUtils;

/**
 * Created by admin on 4/30/2016.
 */
public class MonerisWebViewActivity extends AppCompatActivity {

    public static final String EXTRA_AMOUNT = "amount";
    private android.webkit.WebView wbview;
    static String response = null;
    private ImageView back;
    private SessionPreferences sessionPreferences;
    private Dialog dialog;
    private String sessionId;
    private String amount;
    private ProgressBar progressBar;
    private FrameLayout frame;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loadmoney);
        sessionPreferences = new SessionPreferences(MonerisWebViewActivity.this);
        sessionId = sessionPreferences.getUserDetails().getSessionId();
        amount = getIntent().getStringExtra(EXTRA_AMOUNT);
        frame = (FrameLayout) findViewById(R.id.frame);

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        class MyJavaScriptInterface {
            @JavascriptInterface
            public void processHTML(String html) {
                // process the html as needed by the app
                // TODO Process the HTML
                LogCat.print("RESPONSE::::::::::::::::::::::::::::::::");
                LogCat.print(html);
                LogCat.print("::::::::::::::::::::::::::::::::::::::::");

                String status = null;
                if (html.indexOf("Source : ERROR") != -1) {
                    status = "Transaction Declined!";
                } else {
                    status = "Status Not Known!";
                }
                LogCat.print("Status :: " + status);
                // Start Activity with status_activity put in extra
                final Intent intent = new Intent(getApplicationContext(), StatusActivity.class);
                intent.putExtra(StatusActivity.EXTRA_STATUS, status);
                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            startActivity(intent);
                            finish();
                        }
                    }
                };
                timer.start();
            }
        }

        wbview = (android.webkit.WebView) findViewById(R.id.webview);
        wbview.getSettings().setJavaScriptEnabled(true);
        wbview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");

        wbview.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(android.webkit.WebView view, String url) {
                super.onPageFinished(wbview, url);
                frame.setVisibility(View.GONE);
                LogCat.print("URL :: " + url);
                if (url.indexOf("cancelTXN=Cancel+Transaction") != -1) {
                    wbview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                }
            }

            @Override
            public void onReceivedError(android.webkit.WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getApplicationContext(), "Oh no!" + description, Toast.LENGTH_SHORT).show();
                LogCat.print("ERROR" + errorCode);
                frame.setVisibility(View.GONE);


            }

            @Override
            public void onPageStarted(android.webkit.WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);


            }
        });


        wbview.getSettings().setLoadsImagesAutomatically(true);
        wbview.getSettings().setPluginState(WebSettings.PluginState.ON);
        wbview.getSettings().setAllowFileAccess(true);
        wbview.getSettings().setLoadWithOverviewMode(true);
        wbview.getSettings().setUseWideViewPort(true);
        wbview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        String DESKTOP_USERAGENT = wbview.getSettings().getUserAgentString();
        DESKTOP_USERAGENT = DESKTOP_USERAGENT.replace("Mobile ", "");
        wbview.getSettings().setUserAgentString(DESKTOP_USERAGENT);
        String params = "amount=" + amount + "&sessionId=" + sessionId;
        LogCat.print("Values" + params.toString());
        wbview.postUrl(SOAPIUrls.URL_QR_MONERIS, EncodingUtils.getBytes(params, "BASE64"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
