package com.serviceOntario.in.activity.mpin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.serviceOntario.in.R;
import com.serviceOntario.in.activity.MainActivity;

public class ChangeMPINActivity extends AppCompatActivity {

    private EditText etoldMPIN, etnewMPIN, etconMPIN;
    private Button btnMPINProceed;
    private String oldMpin, newMpin, confirmMpin;

    private MPINPreference mpinPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_mpin);
        etoldMPIN = (EditText) findViewById(R.id.etoldMPIN);
        etnewMPIN = (EditText) findViewById(R.id.etnewMPIN);
        etconMPIN = (EditText) findViewById(R.id.etconMPIN);
        mpinPreference = new MPINPreference(ChangeMPINActivity.this);
        btnMPINProceed = (Button) findViewById(R.id.btnMPINProceed);
        btnMPINProceed.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  if (etoldMPIN.getText().toString() != null && etnewMPIN.getText().toString() != null && etconMPIN.getText().toString() != null) {
                                                      if (etnewMPIN.getText().toString().equals(etconMPIN.getText().toString())) {
                                                          if (mpinPreference.checkMPIN(etoldMPIN.getText().toString())) {
                                                              // TODO Show main activity
                                                              mpinPreference.clearMPIN();
                                                              mpinPreference.setMPIN(etnewMPIN.getText().toString());
                                                              Intent i = new Intent(ChangeMPINActivity.this, MainActivity.class);
                                                              startActivity(i);
                                                              finish();
                                                          } else {
                                                              // TODO Show error
                                                              etnewMPIN.setError("MPIN not matched");
                                                              etnewMPIN.requestFocus();
                                                          }
                                                      } else {
                                                          Toast.makeText(ChangeMPINActivity.this, "Mpin Mismatch", Toast.LENGTH_LONG).show();
                                                      }
                                                  } else {
                                                      etoldMPIN.setError("Required");
                                                      etnewMPIN.setError("Required");
                                                      etconMPIN.setError("Required");
                                                      etconMPIN.requestFocus();
                                                      etnewMPIN.requestFocus();
                                                      etoldMPIN.requestFocus();
                                                  }
                                              }
                                          }


        );

    }
}
