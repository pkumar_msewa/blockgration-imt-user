package com.serviceOntario.in.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.serviceOntario.in.R;
import com.serviceOntario.dialog.LoadMoneyDialog;
import com.serviceOntario.uitl.LogCat;

import java.io.File;
import java.util.Calendar;

/**
 * Created by admin on 4/16/2016.
 */
public class ChangeInformationActivity extends AppCompatActivity {
    public static final String EXTRADETAILS = "name";
    private Button upload;
    private static final int PICK_IMAGE = 1;
    private static final int SELECT_PICTURE = 2;
    private boolean valid;
    private View focusView;
    private ImageButton ib;
    private EditText date;
    private Calendar cal;
    private int day;
    private TextInputLayout tilldate;
    private int month;
    private int year;
    private String imageFilePath;
    private TextView tvImageUrl;
    private ImageView backarrow;
    private TextView title;
    private Button btnPayment;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_information);
        upload = (Button) findViewById(R.id.upload);
        title = (TextView) findViewById(R.id.title);
        date = (EditText) findViewById(R.id.date);

        btnPayment = (Button) findViewById(R.id.btnPayment);
        title.setText(getIntent().getStringExtra(ChangeInformationActivity.EXTRADETAILS));
        tvImageUrl = (TextView) findViewById(R.id.tvImageUrl);
        backarrow = (ImageView) findViewById(R.id.back);
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 2);
            }
        });
        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadMoneyDialog loadMoneyDialog = new LoadMoneyDialog(ChangeInformationActivity.this);
                loadMoneyDialog.showDialog();
            }
        });
        ib = (ImageButton) findViewById(R.id.imageButton1);
        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(0);
            }
        });



    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // TODO http://developer.android.com/guide/topics/providers/document-provider.html
        LogCat.print("REPONSE CODE" + resultCode);
        LogCat.print("REQUESTCODE CODE" + requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                imageFilePath = getRealPathFromURI(selectedImageUri);
                tvImageUrl.setText(imageFilePath);
                File file = new File(imageFilePath);
            }

        }
        if (resultCode == -1) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                imageFilePath = getRealPathFromURI(selectedImageUri);
                File file = new File(imageFilePath);
                tvImageUrl.setText(imageFilePath);
            }

        }
        if (resultCode == Activity.RESULT_CANCELED) {
            if (requestCode == PICK_IMAGE) {
                Uri selectedImageUri = data.getData();
                imageFilePath = getRealPathFromURI(selectedImageUri);
                File file = new File(imageFilePath);
                tvImageUrl.setText(imageFilePath);
                LogCat.print("IMAGE PATH" + imageFilePath);
            }

        }

    }

    private String getRealPathFromURI(Uri contentURI) {
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, year, month, day);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            date.setText(selectedDay + " - " + (selectedMonth + 1) + " - "
                    + selectedYear);
        }
    };

}
