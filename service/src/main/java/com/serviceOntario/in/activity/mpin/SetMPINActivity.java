package com.serviceOntario.in.activity.mpin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.serviceOntario.in.R;
import com.serviceOntario.in.activity.MainActivity;

public class SetMPINActivity extends AppCompatActivity {

    private MPINPreference mpinPreference;

    private EditText etMPIN;
    private Button btnMPINProceed;
    private String mpin,cmip;
    private EditText etconform;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_mpin);
        mpinPreference = new MPINPreference(SetMPINActivity.this);

        etMPIN = (EditText) findViewById(R.id.etMPIN);
        etconform=(EditText)findViewById(R.id.etconform);
        btnMPINProceed = (Button) findViewById(R.id.btnMPINProceed);
        btnMPINProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpin = etMPIN.getText().toString();
                cmip = etconform.getText().toString();
                if (mpin != null && mpin.length() != 0 ) {
                    if (cmip != null && cmip.length() != 0 ) {
                        if (cmip.equalsIgnoreCase(mpin)) {
                            mpinPreference.setMPIN(mpin);
                            Toast.makeText(getApplicationContext(), "MPIN set :: " + mpinPreference.getMPIN(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(SetMPINActivity.this, MainActivity.class));
                            finish();
                        } else {
                            etconform.setError("Confirm MPIN did not match");
                            etconform.requestFocus();
                        }
                    } else {
                        etconform.setError("Confirm MPIN required");
                        etconform.requestFocus();
                    }
                } else {
                    etMPIN.setError("MPIN Required");
                    etMPIN.requestFocus();
                }
            }
        });
    }
}
