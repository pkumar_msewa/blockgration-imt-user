package com.serviceOntario.in.activity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.serviceOntario.in.R;
import com.serviceOntario.dialog.LoadMoneyDialog;
import com.serviceOntario.uitl.LogCat;
import com.serviceOntario.uitl.DrawableChange;


/**
 * Created by admin on 4/16/2016.
 */
public class VehicleRegistrationActivity extends AppCompatActivity {
    public static final String EXTRADETAILS = "name";
    private ImageView back;
    private Bitmap mFinalBitmap;
    private int mColorCode;
    private ImageView backarrow;
    private TextView title;
    private Button btnPayment;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_registration);
        back = (ImageView) findViewById(R.id.back);
        String strColorCode = "#" + Integer.toHexString(ContextCompat.getColor(VehicleRegistrationActivity.this, R.color.pq_white));
        LogCat.print("COLOR" + strColorCode);
        backarrow = (ImageView) findViewById(R.id.back);
        title = (TextView) findViewById(R.id.title);
        btnPayment = (Button) findViewById(R.id.btnPayment);
        title.setText(getIntent().getStringExtra(ChangeInformationActivity.EXTRADETAILS));
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadMoneyDialog loadMoneyDialog = new LoadMoneyDialog(VehicleRegistrationActivity.this);
                loadMoneyDialog.showDialog();
            }
        });

        if (strColorCode != null && strColorCode.length() > 0 &&
                strColorCode.length() == 7) {
            mColorCode = Color.parseColor(strColorCode);

            //Get the image to be changed from the drawable, drawable-xhdpi, drawable-hdpi,etc folder.
            Drawable sourceDrawable = getResources().getDrawable(R.drawable.backarrow);

            //Convert drawable in to bitmap
            Bitmap sourceBitmap = DrawableChange.convertDrawableToBitmap(sourceDrawable);

            //Pass the bitmap and color code to change the icon color dynamically.

            mFinalBitmap = DrawableChange.changeImageColor(sourceBitmap, mColorCode);

            back.setImageBitmap(mFinalBitmap);
        }


    }
}
