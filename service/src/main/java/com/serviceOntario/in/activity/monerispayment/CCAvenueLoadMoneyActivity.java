package com.serviceOntario.in.activity.monerispayment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.serviceOntario.in.R;
import com.serviceOntario.in.activity.status.StatusActivity;
import com.serviceOntario.uitl.AesCryptUtil;

public class CCAvenueLoadMoneyActivity extends AppCompatActivity {
  public static final String EXTRA_AMOUNT = "amount";
  private String amountToLoad;
  private Toolbar toolbar;
  private FrameLayout frame;
  private ImageView back;

  @SuppressLint({"SetJavaScriptEnabled"})
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.service_loadmoney);

    this.toolbar = ((Toolbar) findViewById(R.id.toolbar));
    setSupportActionBar(this.toolbar);

    this.amountToLoad = getIntent().getStringExtra("amount");
    this.back = ((ImageView) findViewById(R.id.back));
    this.back.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        CCAvenueLoadMoneyActivity.this.finish();
      }
    });
    this.frame = ((FrameLayout) findViewById(R.id.frame));
    final WebView webview = (WebView) findViewById(R.id.webview);
    webview.getSettings().setJavaScriptEnabled(true);
    if (webview != null) {
      webview.setWebViewClient(new WebViewClient() {
        @SuppressLint("WrongConstant")
        public void onPageFinished(WebView view, String url) {
          super.onPageFinished(webview, url);
          CCAvenueLoadMoneyActivity.this.frame.setVisibility(View.GONE);
          if (url.indexOf("ccavenueTranstionFailure") != -1) {
            Intent intent = new Intent(CCAvenueLoadMoneyActivity.this.getApplicationContext(), StatusActivity.class);
            intent.putExtra("status", "Transaction Declined!");
            CCAvenueLoadMoneyActivity.this.startActivity(intent);
            webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
          }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
          Toast.makeText(CCAvenueLoadMoneyActivity.this.getApplicationContext(), "Oh no!" + description, Toast.LENGTH_LONG).show();
          CCAvenueLoadMoneyActivity.this.frame.setVisibility(View.VISIBLE);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
          super.onPageStarted(view, url, favicon);
        }

      });
    }

    String key = "promo_code=&integration_type=iframe_normal&billing_state=Tamilnadu&order_id=8553926329&delivery_name=PayQwik&billing_email=care@payqwik.in&customer_identifier=8553926329&currency=INR&delivery_state=Tamilnadu&delivery_tel=8553926329&amount=" + this.amountToLoad
      .replaceAll("[-+.^:,$]", "") +
      "&billing_country=India&delivery_country=India&successURL=" + "success" + "&billing_tel=" + "8553926329" + "&delivery_city=Chennai&merchant_id=47281&redirect_url=http://119.18.52.107/ccavenueTranstionSuccessfull&cancel_url=http://119.18.52.107/ccavenueTranstionFailure&payqwikBal=0.00&billing_name=PayQwik&delivery_address=India Land Building&delivery_zip=600058&failureURL=http://wap.krayons.mobi/PayQuickRes&billing_zip=600058&sessionId=54225&billing_city=Chennai&merchant_param1=" + "8553926329" + "&language=EN&merchant_param5=" + "ASDFFDWWEWFFSDS12452" + "&mobileNumber=" + "8553926329" + "&merchant_param4=" + "jakka.santosh@gmail.com" + "&merchant_param3=" + "santosh" + "&merchant_param2=" + "2323134" + "&billing_address=India Land Building";

    System.out.println("KEYSS" + key);
    AesCryptUtil aesUtil = new AesCryptUtil("3FB85A32035FD55667BAC54C51463BE3");
    String encRequest = aesUtil.encrypt(key);
    System.out.println("VALES" + encRequest);
    webview.getSettings().setLoadsImagesAutomatically(true);
    webview.getSettings().setPluginState(WebSettings.PluginState.ON);
    webview.getSettings().setAllowFileAccess(true);
    webview.getSettings().setLoadWithOverviewMode(true);
    webview.getSettings().setUseWideViewPort(false);
    webview.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
    webview.loadUrl("https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction&merchant_id=47281&encRequest=" + encRequest + "&access_code=AVXL02BI72CL24LXLC");
  }

  public void onBackPressed() {
    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
    alertBuilder.setMessage("Are you sure you want to cancel the transaction?");
    alertBuilder.setCancelable(true);
    alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        CCAvenueLoadMoneyActivity.this.finish();
      }
    });
    alertBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.cancel();
      }
    });
    AlertDialog alertDialog = alertBuilder.create();
    alertDialog.show();
  }
}
