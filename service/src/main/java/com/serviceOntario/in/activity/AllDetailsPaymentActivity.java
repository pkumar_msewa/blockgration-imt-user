package com.serviceOntario.in.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.serviceOntario.in.R;

public class AllDetailsPaymentActivity extends AppCompatActivity {

    public static String Extradetails = "details";
    private TextView title, desctitle, desc, cost, costdesc, payment, paymentdesc;
    public static String[] driver = {"Driver's licence renewal", "Enhanced driver's licence", "Get an accessible parking permit", "Renew, replace or change an accessible parking permit", "Driver records", "Change address: driver's licence", "First-time driver: get a licence", "Renew a driver's licence: outside Ontario", "Exchange out-of-province or out-of-country driver's licence", "Replace a driver's licence: outside Ontario", "Renew a G driver's licence: 80 years and over", "Sign up for email renewal reminders"};
    public static String[] Vehicles = {"Licence plate sticker renewal", "Vehicle registration: plate/permit", "Personalized licence plates", "Used vehicle information package (UVIP)", "Buy/sell a used vehicle in Ontario", "Register a farm vehicle", "Vehicle records", "Special permit", "Temporary licence plate sticker", "Change address: vehicle permit", "Licence plate sticker renewal: outside Ontario", "Get vintage licence plates for classic cars", "Get, renew or replace a garage licence", "Sign up for email renewal reminders"};
    public static String[] Health = {"Health card renewal", "Switch to a photo health card", "First-time health card", "Change address: health card", "Organ and tissue donation registration"};
    public static String[] Home_land_and_personal_property = {"Change your address", "Land registration", "Register or search for a personal property lien"};
    public static String[] Identification_and_certificates = {"Ontario Photo Card", "Newborn registration (4-in-1 Newborn Bundle)", "Birth certificate", "Marriage certificate", "Death certificate", "Change your last name", "Change a child's name", "Change name (for adults)", "Sex designation change",};
    public static String[] Security_guards_and_private_investigators = {"Apply for an individual licence", "Apply for an agency sole proprietorship licence", "Apply for an agency partnership licence", "Apply for an agency corporation licence", "Register as an employer"};
    public static String[] Outdoor = {"Outdoors Card", "Fishing licence", "Hunting licence"};
    public static String[] Education_and_training = {"Register a Private Career College in Ontario (for colleges)", "Search for registered Private Career Colleges in Ontario (for students)", "OSAP: Ontario Student Assistance Program"};
    private Button pay;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alldetailspayment);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title = (TextView) findViewById(R.id.title);
        title.setText(getIntent().getStringExtra(AllDetailsPaymentActivity.Extradetails));
        pay = (Button) findViewById(R.id.btnpay);
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AllDetailsPaymentActivity.this, PaymentActivity.class);
                startActivity(intent);

            }
        });

    }

}
