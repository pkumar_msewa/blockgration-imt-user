package com.serviceOntario.in.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.serviceOntario.in.R;
import com.serviceOntario.dialog.LoadMoneyDialog;
import com.serviceOntario.session.SessionPreferences;
import com.serviceOntario.uitl.LogCat;

import java.io.File;

/**
 * Created by admin on 4/16/2016.
 */
public class DriverLicenceRenewalApplicationActivity extends AppCompatActivity {

    public static final String EXTRADETAILS = "name";
    private Button uploadImage;
    private static final int PICK_IMAGE = 1;
    private static final int SELECT_PICTURE = 2;
    private boolean valid;
    private View focusView;
    private Button btnUploadImage;
    private String imageFilePath;
    private File file;
    private TextView tvImageUrl;
    private ImageView backarrow;
    private TextView title;
    private Button btnPayment;
    private EditText etNameAddress, etDriverLienceNumber, etDateOFBirth;
    private SessionPreferences sessionPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_licence_renewal_application);
        sessionPreferences = new SessionPreferences(DriverLicenceRenewalApplicationActivity.this);
        tvImageUrl = (TextView) findViewById(R.id.tvImageUrl);
        uploadImage = (Button) findViewById(R.id.uploadImage);
        btnPayment = (Button) findViewById(R.id.btnPayment);
        backarrow = (ImageView) findViewById(R.id.back);
        title = (TextView) findViewById(R.id.title);

        etNameAddress = (EditText) findViewById(R.id.etNameAddress);
        etNameAddress.setFocusable(false);
        etDriverLienceNumber = (EditText) findViewById(R.id.etDriverLienceNumber);
        etDateOFBirth = (EditText) findViewById(R.id.etDateOFBirth);
        etNameAddress.setText(sessionPreferences.getUserDetails().getFirstName() + " " + sessionPreferences.getUserDetails().getLastName() + " " + sessionPreferences.getUserDetails().getAddress());
        etDriverLienceNumber.setText(sessionPreferences.getUserDetails().getLienceNo());
        etDateOFBirth.setText(sessionPreferences.getUserDetails().getDateofbirth());

        title.setText(getIntent().getStringExtra(ChangeInformationActivity.EXTRADETAILS));
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 2);
            }
        });
        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadMoneyDialog loadMoneyDialog = new LoadMoneyDialog(DriverLicenceRenewalApplicationActivity.this);
                loadMoneyDialog.showDialog();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // TODO http://developer.android.com/guide/topics/providers/document-provider.html
        LogCat.print("REQUESTCODE CODE" + requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                imageFilePath = getRealPathFromURI(selectedImageUri);
                tvImageUrl.setText(imageFilePath);
                File file = new File(imageFilePath);
            }

        }
        if (resultCode == -1) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                imageFilePath = getRealPathFromURI(selectedImageUri);
                File file = new File(imageFilePath);
                tvImageUrl.setText(imageFilePath);
            }

        }
        if (resultCode == Activity.RESULT_CANCELED) {
            if (requestCode == PICK_IMAGE) {
                Uri selectedImageUri = data.getData();
                imageFilePath = getRealPathFromURI(selectedImageUri);
                File file = new File(imageFilePath);
                tvImageUrl.setText(imageFilePath);
                LogCat.print("IMAGE PATH" + imageFilePath);
            }

        }

    }

    private String getRealPathFromURI(Uri contentURI) {
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }
}
