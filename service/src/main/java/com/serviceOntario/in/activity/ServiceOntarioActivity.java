package com.serviceOntario.in.activity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.serviceOntario.in.R;
import com.serviceOntario.uitl.DrawableChange;

/**
 * Created by admin on 4/26/2016.
 */
public class ServiceOntarioActivity extends AppCompatActivity {
    private ImageView imageView3;
    private int mColorCode;
    private Bitmap mFinalBitmap;
    private ImageView back;
    private Button btnPayment;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_ontario);
        imageView3 = (ImageView) findViewById(R.id.imageView3);
        back = (ImageView) findViewById(R.id.back);
        btnPayment = (Button) findViewById(R.id.btnPayment);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        String strColorCode = "#006633";
        if (strColorCode != null && strColorCode.length() > 0 &&
                strColorCode.length() == 7) {
            mColorCode = Color.parseColor(strColorCode);

//            Get the image to be changed from the drawable, drawable-xhdpi, drawable-hdpi,etc folder.
            Drawable sourceDrawable = getResources().getDrawable(R.drawable.logowithtagline);
//
//        Convert drawable in to bitmap
            Bitmap sourceBitmap = DrawableChange.convertDrawableToBitmap(sourceDrawable);
//
//
            mFinalBitmap = DrawableChange.changeImageColor(sourceBitmap, mColorCode);
//
            imageView3.setImageBitmap(mFinalBitmap);
        }
    }


}
