package com.serviceOntario.in.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.serviceOntario.in.R;
import com.serviceOntario.in.R.id;
import com.serviceOntario.in.R.layout;
import com.serviceOntario.in.fragment.HomePageFragment;
import com.serviceOntario.session.SessionPreferences;
import com.serviceOntario.session.SessionPreferences.PQSession;

public class Home extends AppCompatActivity
{
  private ImageView back;
  public static final String EXTRA_SESSION = "amount";
  public static final String EXTRA_NAME = "name";
  private SessionPreferences sessionPreferences;

  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_service_main);
    this.back = ((ImageView)findViewById(R.id.back));
    this.back.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View v) {
        Home.this.finish();
      }
    });
    this.sessionPreferences = new SessionPreferences(this);
    SessionPreferences.PQSession pqSession = new SessionPreferences.PQSession();
    pqSession.setFirstName(getIntent().getStringExtra("name"));
    pqSession.setSessionId(getIntent().getStringExtra("amount"));
    this.sessionPreferences.UserDetails(pqSession);

    getSupportFragmentManager().beginTransaction().add(R.id.frame, new HomePageFragment()).commit();
  }
}
