package com.serviceOntario.in.activity.registration;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.serviceOntario.in.R;
import com.serviceOntario.apiUrls.SOAPIUrls;
import com.serviceOntario.dialog.OTPDialog;
import com.serviceOntario.dialog.SucessDialog;
import com.serviceOntario.uitl.CheckLog;
import com.serviceOntario.uitl.DialogUtils;
import com.serviceOntario.uitl.JSONParser;
import com.serviceOntario.uitl.LogCat;
import com.serviceOntario.uitl.UserEmailFetcher;
import com.serviceOntario.uitl.Validation;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {

    private Button btnsubmit;
    private EditText etMobileNumber;
    private EditText etEmailId;
    private EditText etfirstname;
    private EditText etpassword;
    private EditText etconpassword;
    private String MobileNumber;
    private String Email;
    private String firstName;
    private String password;
    private String conForm;
    private View focusView;
    private ImageButton ib;
    private Calendar cal;
    private int day;
    private int month;
    private int year;
    private CheckBox ckFemale;
    private RequestQueue requestQueue;
    private boolean valid;
    private EditText etlastname;
    private String lastName;
    private Dialog ringProgressDialog;
    private ProgressDialog pDialog;
    private TextInputLayout tillmobileNumber, tilemailId, tillFirstName, tillLastName, tillPassword, tillconformpassword;
    private ImageView back;
    private EditText etLinenceNo;
    private TextInputLayout tillDriverLinence;
    private RadioGroup radioGender;
    private RadioButton radioSexButton;
    private String gender;
    private String linence;
    private EditText date;
    private String dates;
    private EditText etMiddleName;
    private TextInputLayout tillmiddleName;
    private String middleName;
    private TextInputLayout tilldate;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        pDialog = new ProgressDialog(getApplicationContext());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        date = (EditText) findViewById(R.id.date);
        requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        etEmailId = (EditText) findViewById(R.id.etEmailId);
        etfirstname = (EditText) findViewById(R.id.etfirstname);
        etlastname = (EditText) findViewById(R.id.etlastname);
        etpassword = (EditText) findViewById(R.id.etpassword);
        etconpassword = (EditText) findViewById(R.id.etconpassword);
        etLinenceNo = (EditText) findViewById(R.id.etLinenceNo);
        btnsubmit = (Button) findViewById(R.id.btnsubmit);
        etMiddleName = (EditText) findViewById(R.id.etMiddleName);
        tillmiddleName = (TextInputLayout) findViewById(R.id.tillmiddleName);

        radioGender = (RadioGroup) findViewById(R.id.radioGender);

        // TextInputLayout and edittext add focus change listener
        tillmobileNumber = (TextInputLayout) findViewById(R.id.tillmobileNumber);
        tilemailId = (TextInputLayout) findViewById(R.id.tilemailId);
        tillFirstName = (TextInputLayout) findViewById(R.id.tillFirstName);
        tillLastName = (TextInputLayout) findViewById(R.id.tillLastName);
        tillPassword = (TextInputLayout) findViewById(R.id.tillPassword);
        tillconformpassword = (TextInputLayout) findViewById(R.id.tillconformpassword);
        tillDriverLinence = (TextInputLayout) findViewById(R.id.tillDriverLinence);
        tilldate = (TextInputLayout) findViewById(R.id.tilldate);

        ib = (ImageButton) findViewById(R.id.imageButton1);
        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(0);
            }
        });

        date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilldate.setErrorEnabled(true);
                tilldate.setError("Enter date format YYYY-MM-DD");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tilldate.setError(null);
                }

            }
        });
        etLinenceNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tillDriverLinence.setErrorEnabled(true);
                tillDriverLinence.setError("Enter licence number");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etMiddleName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tillmiddleName.setErrorEnabled(true);
                tillmiddleName.setError("Enter Middle Name");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etMiddleName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tillmiddleName.setError(null);
                }

            }
        });
        etLinenceNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tillDriverLinence.setErrorEnabled(true);
                tillDriverLinence.setError("Enter licence number");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etLinenceNo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tillDriverLinence.setError(null);
                }

            }
        });

        etMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tillmobileNumber.setErrorEnabled(true);
                tillmobileNumber.setError("Enter 10 digits mobile number");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etMobileNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tillmobileNumber.setError(null);
                }

            }
        });
        etEmailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilemailId.setError("Enter Valid Email Id");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etEmailId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tilemailId.setError(null);
                }
            }
        });
        etfirstname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tillFirstName.setError("Enter First Name");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etfirstname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tillFirstName.setError(null);
                }

            }
        });
        etlastname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tillLastName.setError("Enter Last Name");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etlastname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tillLastName.setError(null);
                }
            }
        });
        etpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tillPassword.setError("Please Enter minimum 6 digits password");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etpassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tillPassword.setError(null);
                }

            }
        });
        etconpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tillconformpassword.setError("Please Enter minimum 6 digits password ");

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etconpassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    tillconformpassword.setError(null);
                }
            }
        });
        etEmailId.setText(UserEmailFetcher.getEmail(RegistrationActivity.this));

        btnsubmit.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             if (isNetworkAvaliable(RegistrationActivity.this)) {
                                                 if (validform()) {
                                                     ringProgressDialog = DialogUtils.launchCustomDialog(RegistrationActivity.this);
                                                     ringProgressDialog.show();
                                                     StringRequest stringRequest = new StringRequest(Request.Method.POST, SOAPIUrls.URL_QP_REGISTER, new com.android.volley.Response.Listener<String>() {
                                                         @Override
                                                         public void onResponse(String s) {
                                                             ringProgressDialog.cancel();
                                                             pDialog.dismiss();

                                                             try {
                                                                 LogCat.print("RESPONSE" + s);
                                                                 JSONObject jsonObject = new JSONObject(s);

                                                                 String status = JSONParser.getStringForKey(jsonObject, "status");
                                                                 String code = JSONParser.getStringForKey(jsonObject, "code");
                                                                 String message = JSONParser.getStringForKey(jsonObject, "message");
                                                                 String details = JSONParser.getStringForKey(jsonObject, "details");
                                                                 if (code != null) {
                                                                     if (code.equals("S00")) {
                                                                         OTPDialog otpDialog = new OTPDialog(RegistrationActivity.this, getSupportFragmentManager(), null);
                                                                         otpDialog.show("Hi " + firstName, details, etMobileNumber.getText().toString());
                                                                     } else if (code.equals("F04")) {
                                                                         if (status.equals("Bad Request")) {
                                                                             OTPDialog otpDialog = new OTPDialog(RegistrationActivity.this, getSupportFragmentManager(), null);
                                                                             otpDialog.show("Hi " + firstName, "Username Already Exist", etMobileNumber.getText().toString());
                                                                         } else {
                                                                             OTPDialog otpDialog = new OTPDialog(RegistrationActivity.this, getSupportFragmentManager(), null);
                                                                             otpDialog.show("Hi " + firstName, details, etMobileNumber.getText().toString());
                                                                         }

                                                                     } else {
                                                                         SucessDialog sucessDialog = new SucessDialog(RegistrationActivity.this, null, null);
                                                                         sucessDialog.show("Hi " + firstName, details, "OK");
                                                                     }
                                                                 }
                                                             } catch (
                                                                     JSONException e
                                                                     )

                                                             {
                                                                 e.printStackTrace();
                                                             }

                                                         }
                                                     }, new com.android.volley.Response.ErrorListener() {
                                                         @Override
                                                         public void onErrorResponse(VolleyError volleyError) {

                                                             ringProgressDialog.cancel();
                                                             pDialog.dismiss();

                                                             LogCat.print("ERROE" + volleyError.getMessage());
                                                             if (volleyError.networkResponse == null) {
                                                                 if (volleyError.getClass().equals(TimeoutError.class)) {
                                                                     Toast.makeText(getApplicationContext(),
                                                                             "Timeout error!",
                                                                             Toast.LENGTH_LONG).show();
                                                                 }
                                                             }

                                                         }
                                                     }) {

                                                         @Override
                                                         public Map<String, String> getParams() throws AuthFailureError {
                                                             HashMap<String, String> hashMap = new HashMap<String, String>();
                                                             hashMap.put("firstName", firstName);
                                                             hashMap.put("lastName", lastName);
                                                             hashMap.put("email", Email);
                                                             hashMap.put("contactNo", MobileNumber);
                                                             hashMap.put("password", password);
                                                             hashMap.put("confirmPassword", conForm);
                                                             hashMap.put("gender", gender);
                                                             hashMap.put("middleName", "");
                                                             hashMap.put("dateOfBirth", dates);
                                                             hashMap.put("licenceNo", linence);
                                                             hashMap.put("username", MobileNumber);
                                                             LogCat.print("HASH" + hashMap);
                                                             return hashMap;
                                                         }
                                                     };
                                                     int socketTimeout = 60000;
                                                     RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                                                     stringRequest.setRetryPolicy(policy);
                                                     requestQueue.add(stringRequest);
                                                 }
                                             } else

                                             {
                                                 Toast.makeText(getApplicationContext(), "Please Turn on Internet connection", Toast.LENGTH_SHORT).show();
                                             }

                                         }
                                     }

        );

    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        DatePickerDialog dpDialog = new DatePickerDialog(this, datePickerListener, year, month, day);
        DatePicker datePicker = dpDialog.getDatePicker();

        Calendar calendar = Calendar.getInstance();
        //get the current day
        datePicker.setMaxDate(calendar.getTimeInMillis());//set the current day as the max date
        return dpDialog;

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            date.setText(selectedYear + "-" + (selectedMonth + 1) + "-" + selectedDay);
        }
    };

    public static boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED);
    }

    public boolean validform() {

        etMobileNumber.setError(null);
        etEmailId.setError(null);
        etfirstname.setError(null);
        date.setError(null);
        etlastname.setError(null);
        etpassword.setError(null);
        etconpassword.setError(null);
        etLinenceNo.setError(null);


        int selectedId = radioGender.getCheckedRadioButtonId();

        // find the radiobutton by returned id
        radioSexButton = (RadioButton) findViewById(selectedId);


        valid = true;

        MobileNumber = etMobileNumber.getText().toString();
        Email = etEmailId.getText().toString();
        firstName = etfirstname.getText().toString();
        lastName = etlastname.getText().toString();
        password = etpassword.getText().toString();
        conForm = etconpassword.getText().toString();
        gender = radioSexButton.getText().toString();
        linence = etLinenceNo.getText().toString();
        dates = date.getText().toString();
        checkdate(dates, tilldate);
        checklience(linence, tillDriverLinence);
        checkmobilenumber(MobileNumber, tillmobileNumber);
        checkEmail(Email, tilemailId);
        checkFirstName(firstName, tillFirstName);
        checkLastName(lastName, tillLastName);
        checkPassword(password, tillPassword);
        checkconfompassword(password, tillconformpassword, conForm);
        return valid;
    }

    private void checkdate(String dates, TextInputLayout tilldate) {
        CheckLog checkLog = Validation.checkFirstName(dates);
        if (!checkLog.isValid) {
            tilldate.isErrorEnabled();
            tilldate.setError(getString(checkLog.msg));
            focusView = tilldate;
            valid = false;
        }
    }

    private void checklience(String linence, TextInputLayout etLinenceNo) {
        CheckLog checkLog = Validation.checkFirstName(linence);
        if (!checkLog.isValid) {
            etlastname.setError(getString(checkLog.msg));
            focusView = etlastname;
            valid = false;
        }
    }

    private void checkLastName(String lastName, TextInputLayout etlastname) {
        CheckLog checkLog = Validation.checkFirstName(lastName);
        if (!checkLog.isValid) {
            etlastname.setError(getString(checkLog.msg));
            focusView = etlastname;
            valid = false;
        }
    }

    private void checkconfompassword(String mobileNumber, TextInputLayout etMobileNumber, String conForm) {
        CheckLog checkLog = Validation.checkRePassword(mobileNumber, conForm);
        if (!checkLog.isValid) {
            etMobileNumber.setError(getString(checkLog.msg));
            focusView = etMobileNumber;
            valid = false;
        }
    }

    private void checkPassword(String mobileNumber, TextInputLayout etMobileNumber) {
        CheckLog checkLog = Validation.checkPassword(mobileNumber);
        if (!checkLog.isValid) {
            etMobileNumber.setError(getString(checkLog.msg));
            focusView = etMobileNumber;
            valid = false;
        }
    }

    private void checkFirstName(String mobileNumber, TextInputLayout etMobileNumber) {
        CheckLog checkLog = Validation.checkFirstName(mobileNumber);
        if (!checkLog.isValid) {
            etMobileNumber.setError(getString(checkLog.msg));
            focusView = etMobileNumber;
            valid = false;
        }
    }

    private void checkEmail(String mobileNumber, TextInputLayout etMobileNumber) {
        CheckLog checkLog = Validation.checkEmail(mobileNumber);
        if (!checkLog.isValid) {
            etMobileNumber.setError(getString(checkLog.msg));
            focusView = etMobileNumber;
            valid = false;
        }
    }

    private void checkmobilenumber(String mobileNumber, TextInputLayout etMobileNumber) {
        CheckLog checkLog = Validation.checkTenDigitNumber(mobileNumber);
        if (!checkLog.isValid) {
            etMobileNumber.isErrorEnabled();
            etMobileNumber.setError(getString(checkLog.msg));
            focusView = etMobileNumber;
            valid = false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        setResult(2, intent);
        finish();//finishing activity
    }
}
