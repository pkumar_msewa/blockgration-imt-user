package com.serviceOntario.in.activity.mpin;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by DELL on 04-05-2016.
 */
public class MPINPreference {

    public static final String PREF = "com.jakka.pref";
    public static final String KEY_MPIN = "com.jakka.pref.key.mpin";

    private SharedPreferences sharedpreferences;

    public MPINPreference(Context context) {
        sharedpreferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
    }

    public boolean checkMPIN(String mpin) {
        String storedMPIN = sharedpreferences.getString(KEY_MPIN, "");
        if (storedMPIN.equalsIgnoreCase(mpin)) {
            return true;
        } else {
            return false;
        }
    }

    public void setMPIN(String mpin) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(KEY_MPIN, mpin);
        editor.commit();
    }

    public String getMPIN() {
       return sharedpreferences.getString(KEY_MPIN, "");
    }

    public void clearMPIN() {
        setMPIN("");
    }

    public boolean isMPINSet() {
        String storedMPIN = sharedpreferences.getString(KEY_MPIN, "");
        if (storedMPIN.length() == 0) {
            return false;
        } else {
            return true;
        }
    }
}
