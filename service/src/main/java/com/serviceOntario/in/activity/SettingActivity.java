package com.serviceOntario.in.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.serviceOntario.in.R;
import com.serviceOntario.in.activity.mpin.ChangeMPINActivity;
import com.serviceOntario.in.activity.mpin.SetMPINActivity;

public class SettingActivity extends AppCompatActivity {

    private Button btnSetMPIN;
    private Button btnChagneMPIN;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSetMPIN = (Button) findViewById(R.id.btnSetMPIN);
        btnSetMPIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, SetMPINActivity.class));
            }
        });
        btnChagneMPIN = (Button) findViewById(R.id.btnChagneMPIN);
        btnChagneMPIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, ChangeMPINActivity.class));
            }
        });
    }
}
