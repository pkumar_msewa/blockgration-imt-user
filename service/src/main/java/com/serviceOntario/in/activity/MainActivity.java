package com.serviceOntario.in.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.serviceOntario.in.R;
import com.serviceOntario.apiUrls.SOAPIUrls;
import com.serviceOntario.in.activity.mpin.MPINPreference;
import com.serviceOntario.in.fragment.HomePageFragment;
import com.serviceOntario.in.fragment.login.LoginFragment;
import com.serviceOntario.in.fragment.profile.ProfilePageFragment;
import com.serviceOntario.session.SessionPreferences;
import com.serviceOntario.uitl.DialogUtils;
import com.serviceOntario.uitl.JSONParser;
import com.serviceOntario.uitl.LogCat;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private View navigationHeaderView;
    private ImageView ivProfilePicture;
    private TextView tvProfileNumber;
    private TextView tvProfileName;
    private TextView tvProfileEmail;
    private SessionPreferences sessionPreferences;
    private MPINPreference mpinPreference;
    private RelativeLayout header;
    private ImageView logo;
    private RequestQueue requestQueue;
    private Dialog pDialog;
    private SessionPreferences.PQSession pqSession;
    private Locale myLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pqSession = new SessionPreferences.PQSession();
        sessionPreferences = new SessionPreferences(MainActivity.this);
        mpinPreference = new MPINPreference(MainActivity.this);
        getSupportFragmentManager().beginTransaction().add(R.id.frame, new HomePageFragment()).commit();

        requestQueue = Volley.newRequestQueue(MainActivity.this);
        sessionPreferences = new SessionPreferences(MainActivity.this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        if (sessionPreferences.getLanguage() != null && !sessionPreferences.getLanguage().isEmpty()) {
            if (sessionPreferences.getLanguage().equals("FRANCAIS")) {
                String languageToLoad = "fr"; // your language
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());
            } else if (sessionPreferences.getLanguage().equals("ENGLISH")) {
                String languageToLoad = "en"; // your language
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());
            }
        } else {
            sessionPreferences.clearLanguage();
            sessionPreferences.language("ENGLISH");
        }
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationHeaderView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        if (sessionPreferences.getUserDetails().getSessionId() != null && !sessionPreferences.getUserDetails().getSessionId().isEmpty()) {
            logo = (ImageView) navigationHeaderView.findViewById(R.id.logo);
            ivProfilePicture = (CircleImageView) navigationHeaderView.findViewById(R.id.ivProfilePicture);
            tvProfileNumber = (TextView) navigationHeaderView.findViewById(R.id.tvProfileNumber);
            tvProfileName = (TextView) navigationHeaderView.findViewById(R.id.tvProfileName);
            tvProfileEmail = (TextView) navigationHeaderView.findViewById(R.id.tvProfileEmail);
            Picasso.with(MainActivity.this).load("https://www.versioneye.com/java/com.squareup.picasso:picasso/2.3.4").placeholder(R.mipmap.image).into(ivProfilePicture);
            logo.setVisibility(View.GONE);
            ivProfilePicture.setVisibility(View.VISIBLE);
            tvProfileNumber.setVisibility(View.VISIBLE);
            tvProfileName.setVisibility(View.VISIBLE);
            tvProfileEmail.setVisibility(View.VISIBLE);
            tvProfileEmail.setText(sessionPreferences.getUserDetails().getEmail());
            tvProfileName.setText(sessionPreferences.getUserDetails().getFirstName() + " " + sessionPreferences.getUserDetails().getMiddleName() + " " + sessionPreferences.getUserDetails().getLastName());
            tvProfileNumber.setText(sessionPreferences.getUserDetails().getMobile());
            navigationView.getMenu().getItem(5).setTitle(getResources().getString(R.string.logout)).setVisible(true);
            navigationView.getMenu().getItem(3).setVisible(true).setTitle(getResources().getString(R.string.Language));
            navigationView.getMenu().getItem(4).setVisible(true).setTitle(getResources().getString(R.string.More));
            navigationView.getMenu().getItem(2).setVisible(true).setTitle(getResources().getString(R.string.action_settings));
            navigationView.getMenu().getItem(1).setVisible(true).setTitle(R.string.action_profile);
            navigationView.getMenu().getItem(0).setTitle(getResources().getString(R.string.action_home));

        } else {
            logo = (ImageView) navigationHeaderView.findViewById(R.id.logo);
            header = (RelativeLayout) navigationHeaderView.findViewById(R.id.header);
            header.setBackgroundResource(R.color.pq_black);
            ivProfilePicture = (CircleImageView) navigationHeaderView.findViewById(R.id.ivProfilePicture);
            tvProfileNumber = (TextView) navigationHeaderView.findViewById(R.id.tvProfileNumber);
            tvProfileName = (TextView) navigationHeaderView.findViewById(R.id.tvProfileName);
            tvProfileEmail = (TextView) navigationHeaderView.findViewById(R.id.tvProfileEmail);
            Picasso.with(MainActivity.this).load("https://www.versioneye.com/java/com.squareup.picasso:picasso/2.3.4").placeholder(R.drawable.logowithtagline).resize(100, 100).into(ivProfilePicture);
            ivProfilePicture.setVisibility(View.GONE);
            logo.setVisibility(View.VISIBLE);
            navigationView.getMenu().getItem(2).setVisible(false);
            navigationView.getMenu().getItem(3).setVisible(false);
            navigationView.getMenu().getItem(1).setVisible(false);
            navigationView.getMenu().getItem(4).setVisible(false);
            navigationView.getMenu().getItem(0).setTitle(getResources().getString(R.string.action_home));
            navigationView.getMenu().getItem(5).setTitle(getResources().getString(R.string.Login));
        }
        navigationView.setNavigationItemSelectedListener(this);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            alertBuilder.setMessage("Are you sure you want to exit Service Ontario Application?");
            alertBuilder.setCancelable(true);
            alertBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ActivityCompat.finishAffinity(MainActivity.this);
                }
            });
            alertBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frame, new HomePageFragment()).commit();
        } else if (id == R.id.nav_camera) {
            // Handle the camera action
            if (item.getTitle().equals("Login")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, new LoginFragment()).commit();
            } else if (item.getTitle().equals("Connexion")) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, new LoginFragment()).commit();
            } else {
                if ((isNetworkAvaliable(MainActivity.this))) {
                    pDialog = DialogUtils.launchCustomDialog(MainActivity.this);
                    pDialog.show();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, SOAPIUrls.URL_QR_LOGOUT, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            pDialog.cancel();
                            pDialog.dismiss();

                            try {
                                LogCat.print("RESPONSE" + s);
                                JSONObject jsonObject = new JSONObject(s);
                                String code = JSONParser.getStringForKey(jsonObject, "code");
                                String message = JSONParser.getStringForKey(jsonObject, "message");
                                if (code != null) {
                                    if (code.equals("S00")) {
                                        pDialog.dismiss();
                                        sessionPreferences.clearSession();
                                        mpinPreference.clearMPIN();
                                        Intent intent = getIntent();
                                        startActivity(intent);
                                    } else {
                                        assert message != null;
                                        if (message.equals("Invalid Session")) {
                                            getSupportFragmentManager().beginTransaction().replace(R.id.frame, new LoginFragment()).commit();
                                        }
                                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            LogCat.print("ERROE" + volleyError);
                            if (volleyError.networkResponse == null) {
                                if (volleyError.getClass().equals(TimeoutError.class)) {
                                    Toast.makeText(MainActivity.this,
                                            "Timeout error!",
                                            Toast.LENGTH_LONG).show();

                                }
                            }
                            pDialog.dismiss();
                            pDialog.cancel();
                            pDialog.dismiss();
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> hashMap = new HashMap<String, String>();
                            hashMap.put("sessionId", sessionPreferences.getUserDetails().getSessionId());
                            LogCat.print("HASH" + hashMap);
                            return hashMap;
                        }
                    };
                    int socketTimeout = 60000;
                    RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                    stringRequest.setRetryPolicy(policy);
                    requestQueue.add(stringRequest);
                } else {
                    Toast.makeText(MainActivity.this, "Please Turn on Internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (id == R.id.nav_gallery) {
            Intent intent = new Intent(MainActivity.this, SettingActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_manage) {
            Intent intent = new Intent(MainActivity.this, AllDetailsActivity.class);
            intent.putExtra(AllDetailsActivity.Extradetails, 13);
            startActivity(intent);
        } else if (id == R.id.nav_slideshow) {
            View view = getLayoutInflater().inflate(R.layout.bottomselectlanguage, null);
            ImageView imageView6 = (ImageView) view.findViewById(R.id.imageView6);
            final TextView english = (TextView) view.findViewById(R.id.english);
            final TextView france = (TextView) view.findViewById(R.id.france);
            final Dialog mBottomSheetDialog = new Dialog(MainActivity.this, R.style.MaterialDialogSheet);
            mBottomSheetDialog.setContentView(view);
            mBottomSheetDialog.setCancelable(true);
            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
            mBottomSheetDialog.show();
            imageView6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBottomSheetDialog.dismiss();
                }
            });
            english.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sessionPreferences.clearLanguage();
                    sessionPreferences.language(english.getText().toString());
                    String languageToLoad = "en"; // your language
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    Intent intent = getIntent();
                    startActivity(intent);
                    getSupportFragmentManager().beginTransaction().add(R.id.frame, new HomePageFragment()).commit();
                    mBottomSheetDialog.dismiss();
                }
            });
            france.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sessionPreferences.clearLanguage();
                    sessionPreferences.language(france.getText().toString());
                    String languageToLoad = "fr"; // your language
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config,
                            getBaseContext().getResources().getDisplayMetrics());
                    getSupportFragmentManager().beginTransaction().add(R.id.frame, new HomePageFragment()).commit();
                    Intent intent = getIntent();
                    startActivity(intent);
                    mBottomSheetDialog.dismiss();
                }
            });

        } else if (id == R.id.nav_profile) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frame, new ProfilePageFragment()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public static boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

}
