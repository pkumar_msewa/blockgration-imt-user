package com.serviceOntario.in.activity.monerispayment;

import android.content.Intent;
import android.graphics.Bitmap;
import com.serviceOntario.in.R;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.serviceOntario.in.R.id;
import com.serviceOntario.in.R.layout;
import com.serviceOntario.uitl.ServiceUtility;
import java.io.PrintStream;
import org.apache.http.util.EncodingUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class LoadMoneyEBSActivity extends AppCompatActivity
{
  public static final String EXTRA = "amount";
  public static final String Name = "name";
  public static final String SESSION = "session";
  private String amountToLoad;
  private String orderId;
  private Toolbar toolbar;
  private ImageButton ivBackBtn;
  private WebView webview;
  private RequestQueue rq;
  private LinearLayout llLoadMoneyWebViewLoading;
  private FrameLayout flWebView;
  private String status = null;
  private FrameLayout frame;
  private ImageView back;
  private String sessionId;
  private String name;

  protected void onCreate(@Nullable Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.service_loadmoney);
    this.toolbar = ((Toolbar)findViewById(R.id.toolbar));
    setSupportActionBar(this.toolbar);
    this.frame = ((FrameLayout)findViewById(R.id.frame));
    this.webview = ((WebView)findViewById(R.id.webview));
    this.back = ((ImageView)findViewById(R.id.back));
    this.sessionId = getIntent().getStringExtra("session");
    this.name = getIntent().getStringExtra("name");
    this.rq = Volley.newRequestQueue(getApplicationContext());
    this.back.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View v) {
        LoadMoneyEBSActivity.this.finish();
      }
    });
    System.out.println("VALUEE" + this.name + this.sessionId);

    this.amountToLoad = getIntent().getStringExtra("amount");
    this.orderId = String.valueOf(System.currentTimeMillis());
    renderView();
  }

  public void renderView()
  {
    this.webview.getSettings().setJavaScriptEnabled(true);
    this.webview.addJavascriptInterface(new Object()
    {
      @JavascriptInterface
      public void processHTML(String html)
      {
        try
        {
          String result = Html.fromHtml(html).toString();
          Log.i("Transaction Response", result);

          JSONObject jsonResponse = new JSONObject(result);
          boolean success = jsonResponse.getBoolean("success");

          if (success)
            LoadMoneyEBSActivity.this.status = "Congratulations!\nTransaction Successful!";
          else {
            LoadMoneyEBSActivity.this.status = "Transaction Failed!\nPlease try again";
          }

          if (LoadMoneyEBSActivity.this.status != null) {
            LoadMoneyEBSActivity.this.runOnUiThread(new Runnable()
            {
              public void run() {
                LoadMoneyEBSActivity.this.webview.loadData("", "text/html", "UTF-8");
              }
            });
            LoadMoneyEBSActivity.this.sendRefresh();
          }
        }
        catch (JSONException e) {
          e.printStackTrace();
        }
      }
    }
    , "HTMLOUT");

    this.webview.setWebViewClient(new WebViewClient()
    {
      public void onPageFinished(WebView view, String url) {
        super.onPageFinished(LoadMoneyEBSActivity.this.webview, url);
        WebView v = view;
        if (url.indexOf("/LoadMoney/Redirect") != -1) {
          LoadMoneyEBSActivity.this.webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
        }

        LoadMoneyEBSActivity.this.frame.setVisibility(8);
      }

      public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
      {
        Log.i("Page Received error", "Page Received error");
        Toast.makeText(LoadMoneyEBSActivity.this.getApplicationContext(), "Oh no!" + description, 0).show();
      }

      public void onPageStarted(WebView view, String url, Bitmap favicon)
      {
        super.onPageStarted(view, url, favicon);
        Log.i("Page Started", "Page Started");
      }
    });
    StringBuffer params = new StringBuffer();

    params.append(ServiceUtility.addToPostParams("sessionId", this.sessionId));
    params.append(ServiceUtility.addToPostParams("channel", "0"));
    params.append(ServiceUtility.addToPostParams("accountId", "20696"));
    params.append(ServiceUtility.addToPostParams("referenceNo", "lkdfjsldkfjsdf"));
    params.append(ServiceUtility.addToPostParams("amount", this.amountToLoad));
    params.append(ServiceUtility.addToPostParams("mode", "TEST"));
    params.append(ServiceUtility.addToPostParams("currency", "INR"));
    params.append(ServiceUtility.addToPostParams("description", "jdslkfsd"));
    params.append(ServiceUtility.addToPostParams("returnUrl", "http://fgmtest.firstglobalmoney.com:8034/Api/v1/User/Website/en/LoadMoney/Redirect"));

    params.append(ServiceUtility.addToPostParams("name", this.name));
    params.append(ServiceUtility.addToPostParams("address", "lfjks"));
    params.append(ServiceUtility.addToPostParams("city", "fdsf"));
    params.append(ServiceUtility.addToPostParams("state", "fds"));
    params.append(ServiceUtility.addToPostParams("country", "IND"));
    params.append(ServiceUtility.addToPostParams("postalCode", "dsff"));
    params.append(ServiceUtility.addToPostParams("phone", "1000000000"));
    params.append(ServiceUtility.addToPostParams("email", "fsdf@fgd.com"));

    params.append(ServiceUtility.addToPostParams("shipName", "fdsf"));
    params.append(ServiceUtility.addToPostParams("shipAddress", "fsdf"));
    params.append(ServiceUtility.addToPostParams("shipCity", "sdf"));
    params.append(ServiceUtility.addToPostParams("shipState", "sdf"));
    params.append(ServiceUtility.addToPostParams("shipCountry", "IND"));
    params.append(ServiceUtility.addToPostParams("shipPostalCode", "387773"));
    params.append(ServiceUtility.addToPostParams("shipPhone", "1000000000"));

    String vPostParams = params.substring(0, params.length() - 1);
    this.webview.postUrl("http://66.207.206.54:8034/Api/v1/User/Android/en/LoadMoney/Process", EncodingUtils.getBytes(vPostParams, "base64"));
  }

  private void sendRefresh()
  {
    Intent intent = new Intent("setting-change");
    intent.putExtra("updates", "1");
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }
}
