package com.serviceOntario.apiUrls;

/**
 * Created by admin on 10/28/2015.
 */
public class SOAPIUrls {
    public static final String URL_QP_LOGIN = "http://66.207.206.54:8034/Ontario/Api/v1/Login";
    public static final String URL_QR_LOGOUT = "http://66.207.206.54:8034/Ontario/Api/v1/Logout";
    public static final String URL_QR_MONERIS = "http://66.207.206.54:8034/Ontario/Api/v1/User/LoadMoney/Process/";
    public static final String URL_QP_REGISTER = "http://66.207.206.54:8034/Ontario/Api/v1/Register";
    public static final String URL_QTP = "http://66.207.206.54:8034/Ontario/Api/v1/Activate/Mobile";
    public static final String URL_TRANSITION = "http://66.207.206.54:8034/Ontario/Api/v1/User/Licence/ProcessPQTransaction";
    public static final String URL_GET_USER_DETAILS = "http://66.207.206.54:8034/Ontario/Api/v1/User/GetUserDetails";
    public static final String URL_RESND_OTP = "http://66.207.206.54:8034/Ontario/Api/v1/Resend/Mobile/OTP";
}
