package com.serviceOntario.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.serviceOntario.dto.DTOHomeProduct;
import com.serviceOntario.in.activity.AllDetailsPaymentActivity;
import com.serviceOntario.in.activity.ChangeInformationActivity;
import com.serviceOntario.in.activity.ChangeofAddressActivity;
import com.serviceOntario.in.activity.DriverLicenceRenewalApplicationActivity;
import com.serviceOntario.in.activity.OntarioDriverLicenceActivity;
import com.serviceOntario.in.activity.OntarioPhotoCard_OntarioResidentsonlyActivity;
import com.serviceOntario.in.activity.RegistrationHealthInsuranceCoverageActivity;
import com.serviceOntario.in.activity.ReplacementDeclarationActivity;
import com.serviceOntario.in.activity.VehicleLicencePlateRenewalApplicationActivity;
import com.serviceOntario.in.activity.VehicleRegistrationActivity;
import com.serviceOntario.in.R;

import java.util.ArrayList;

public class detailsAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private Context context;
    ArrayList<DTOHomeProduct> products;
    private static int s;
    private ViewHolder holder;

    public detailsAdapter(Context context, ArrayList<DTOHomeProduct> dtoHomeProducts) {
        this.context = context;
        this.products = dtoHomeProducts;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return products.indexOf(position);
    }

    private class ViewHolder {
        ImageView img, ivadd;
        TextView tvfoodtitle, tvfoodprice;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_deatils_items_products, null);
            holder = new ViewHolder();
            holder.tvfoodtitle = (TextView) convertView.findViewById(R.id.tvtitle);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        final DTOHomeProduct dtoHomeProduct = products.get(position);
        holder.tvfoodtitle.setText(dtoHomeProduct.getProductName());
        holder.tvfoodtitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    if (dtoHomeProduct.getProductName().equals("Changement d'information")) {
                        Intent intent = new Intent(context, ChangeInformationActivity.class);
                        intent.putExtra(ChangeInformationActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    } else if (dtoHomeProduct.getProductName().equals("Change of Information")) {
                        Intent intent = new Intent(context, ChangeInformationActivity.class);
                        intent.putExtra(ChangeInformationActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    } else if (dtoHomeProduct.getProductName().equals("Ontario Driver's Licence")) {
                        Intent intent = new Intent(context, OntarioDriverLicenceActivity.class);
                        intent.putExtra(OntarioDriverLicenceActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    } else if (dtoHomeProduct.getProductName().equals("Demande de permis de conduire  de l'Ontario")) {
                        Intent intent = new Intent(context, OntarioDriverLicenceActivity.class);
                        intent.putExtra(OntarioDriverLicenceActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    } else if (dtoHomeProduct.getProductName().equals("Demande de renouvellement d'immatriculation (plaque) de vehicule")) {
                        Intent intent = new Intent(context, VehicleLicencePlateRenewalApplicationActivity.class);
                        intent.putExtra(VehicleLicencePlateRenewalApplicationActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);

                    } else if (dtoHomeProduct.getProductName().equals("Vehicle Licence Plate Renewal")) {
                        Intent intent = new Intent(context, VehicleLicencePlateRenewalApplicationActivity.class);
                        intent.putExtra(VehicleLicencePlateRenewalApplicationActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);

                    } else if (dtoHomeProduct.getProductName().equals("Ontario Photo Card")) {
                        Intent intent = new Intent(context, OntarioPhotoCard_OntarioResidentsonlyActivity.class);
                        intent.putExtra(OntarioPhotoCard_OntarioResidentsonlyActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);

                    } else if (dtoHomeProduct.getProductName().equals("Carte-photo de l’Ontario")) {
                        Intent intent = new Intent(context, OntarioPhotoCard_OntarioResidentsonlyActivity.class);
                        intent.putExtra(OntarioPhotoCard_OntarioResidentsonlyActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);

                    } else {
                        Intent intent = new Intent(context, AllDetailsPaymentActivity.class);
                        intent.putExtra(AllDetailsPaymentActivity.Extradetails, dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    }
                } else if (position == 1) {
                    if (dtoHomeProduct.getProductName().equals("Registration for Ontario Health Insurance Coverage")) {
                        Intent intent = new Intent(context, RegistrationHealthInsuranceCoverageActivity.class);
                        intent.putExtra(RegistrationHealthInsuranceCoverageActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);

                    } else if (dtoHomeProduct.getProductName().equals("Inscription pour la couverture d'assurance santé de l'Ontario ")) {
                        Intent intent = new Intent(context, RegistrationHealthInsuranceCoverageActivity.class);
                        intent.putExtra(RegistrationHealthInsuranceCoverageActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);

                    } else if (dtoHomeProduct.getProductName().equals("Demande d'immatriculation d'un vehicule")) {
                        Intent intent = new Intent(context, VehicleRegistrationActivity.class);
                        intent.putExtra(VehicleRegistrationActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    } else if (dtoHomeProduct.getProductName().equals("Vehicle registration: plate/permit")) {
                        Intent intent = new Intent(context, VehicleRegistrationActivity.class);
                        intent.putExtra(VehicleRegistrationActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    } else if (dtoHomeProduct.getProductName().equals("Demande de renouvellement du permis de conduire")) {
                        Intent intent = new Intent(context, DriverLicenceRenewalApplicationActivity.class);
                        intent.putExtra(DriverLicenceRenewalApplicationActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    } else if (dtoHomeProduct.getProductName().equals("Driver Licence Renewal")) {
                        Intent intent = new Intent(context, DriverLicenceRenewalApplicationActivity.class);
                        intent.putExtra(DriverLicenceRenewalApplicationActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, AllDetailsPaymentActivity.class);
                        intent.putExtra(AllDetailsPaymentActivity.Extradetails, dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    }
                } else if (position == 2) {
                    if (dtoHomeProduct.getProductName().equals("Changement d'adresse")) {
                        Intent intent = new Intent(context, ChangeofAddressActivity.class);
                        intent.putExtra(ChangeofAddressActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);

                    } else if (dtoHomeProduct.getProductName().equals("Change of Address")) {
                        Intent intent = new Intent(context, ChangeofAddressActivity.class);
                        intent.putExtra(ChangeofAddressActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);

                    } else {
                        Intent intent = new Intent(context, AllDetailsPaymentActivity.class);
                        intent.putExtra(AllDetailsPaymentActivity.Extradetails, dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    }
                } else if (position == 3) {
                    if (dtoHomeProduct.getProductName().equals("Déclaration de remplacement")) {
                        Intent intent = new Intent(context, ReplacementDeclarationActivity.class);
                        intent.putExtra(ReplacementDeclarationActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);

                    } else if (dtoHomeProduct.getProductName().equals("Replacement Declaration")) {
                        Intent intent = new Intent(context, ReplacementDeclarationActivity.class);
                        intent.putExtra(ReplacementDeclarationActivity.EXTRADETAILS,dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);

                    } else {
                        Intent intent = new Intent(context, AllDetailsPaymentActivity.class);
                        intent.putExtra(AllDetailsPaymentActivity.Extradetails, dtoHomeProduct.getProductName());
                        ((Activity) context).startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(context, AllDetailsPaymentActivity.class);
                    intent.putExtra(AllDetailsPaymentActivity.Extradetails, dtoHomeProduct.getProductName());
                    ((Activity) context).startActivity(intent);
                }

            }
        });
        return convertView;
    }


}
