package com.serviceOntario.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.serviceOntario.in.R;
import com.serviceOntario.dto.HomeMenu;
import com.serviceOntario.in.activity.AllDetailsActivity;
import com.serviceOntario.in.activity.ServiceOntarioActivity;
import com.serviceOntario.in.fragment.login.LoginFragment;
import com.serviceOntario.session.SessionPreferences;

import java.util.ArrayList;

public class GridMenuAdapter extends BaseAdapter {

    private ArrayList<HomeMenu> menuList;
    private ViewHolder viewHolder;
    private Context context;
    private int mColorCode;
    private Bitmap mFinalBitmap;
    private SessionPreferences sessionPreferences;
    private String value;
    private FragmentManager supportFragmentManager;

    public GridMenuAdapter(Context context, ArrayList<HomeMenu> homeMenu, FragmentManager supportFragmentManager) {
        this.context = context;
        this.menuList = homeMenu;
        this.supportFragmentManager = supportFragmentManager;
        sessionPreferences = new SessionPreferences(context);

    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Override
    public Object getItem(int position) {
        return menuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_home, null);
            viewHolder = new ViewHolder();
            viewHolder.ivIcon = (ImageView) convertView.findViewById(R.id.ivIcon);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            viewHolder.linear = (LinearLayout) convertView.findViewById(R.id.linear);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvTitle.setText(menuList.get(position).getTitle());
        try{
            viewHolder.ivIcon.setImageResource(menuList.get(position).getIcon());
        }
        catch (OutOfMemoryError e){
            e.printStackTrace();
        }
        viewHolder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (sessionPreferences.getUserDetails().getSessionId() != null && !sessionPreferences.getUserDetails().getSessionId().isEmpty()) {
                    if (position == 4) {
                        Intent intent = new Intent(context, ServiceOntarioActivity.class);
                        ((Activity) context).startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, AllDetailsActivity.class);
                        intent.putExtra(AllDetailsActivity.Extradetails, position);
                        ((Activity) context).startActivity(intent);
                    }
//                } else {
//                    supportFragmentManager.beginTransaction().replace(R.id.frame, new LoginFragment()).commit();
//                }
            }
        });
////        String strColorCode = "#006B3F";
//                String strColorCode = "#000000";
//        if (strColorCode != null && strColorCode.length() > 0 &&
//                strColorCode.length() == 7) {
//            mColorCode = Color.parseColor(strColorCode);
//
////            Get the image to be changed from the drawable, drawable-xhdpi, drawable-hdpi,etc folder.
//            Drawable sourceDrawable = context.getResources().getDrawable(menuList.get(position).getIcon());
////
////        Convert drawable in to bitmap
//            Bitmap sourceBitmap = DrawableChange.convertDrawableToBitmap(sourceDrawable);
////
////        Pass the bitmap and color code to change the icon color dynamically.
////
//            mFinalBitmap = DrawableChange.changeImageColor(sourceBitmap, mColorCode);
////
//            viewHolder.ivIcon.setImageBitmap(mFinalBitmap);
//        }
        return convertView;
    }

    static class ViewHolder {
        ImageView ivIcon;
        TextView tvTitle;
        LinearLayout linear;
    }

}
