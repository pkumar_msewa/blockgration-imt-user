package com.serviceOntario.custom;

import android.content.Context;
import android.util.AttributeSet;

public class GridView extends android.widget.GridView {

	public static int heightSpec = 0;

	public GridView(Context context) {
		super(context);
	}

	public GridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public GridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		if (heightSpec == 0) {
//			if (getLayoutParams().height == LayoutParams.WRAP_CONTENT) {
//				heightSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
//			} else {
//				heightSpec = heightMeasureSpec;
//			}
//		}
		super.onMeasure(widthMeasureSpec, heightSpec);
	}
}
