package com.serviceOntario.requestHandler;

public interface TaskListener {

	public void taskCompleted(Response response);
}
